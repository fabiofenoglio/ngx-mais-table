/*
 * Public API Surface of ngx-mais-table
 */

export * from './lib/services';
export * from './lib/mais-table.component';
export * from './lib/mais-table.module';
export * from './lib/model';
