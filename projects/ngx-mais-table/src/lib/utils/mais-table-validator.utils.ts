import { IMaisTableColumn, IMaisTableConfiguration } from '../model';
import { MaisTableFormatterHelper } from './mais-table-formatter.utils';

export abstract class MaisTableValidatorHelper {

  public static validateColumnsSpecification(columns: IMaisTableColumn[], config: IMaisTableConfiguration) {
    const err = MaisTableValidatorHelper._validateColumnsSpecification(columns, config);
    if (err) {
      throw new Error(err);
    }
    return null;
  }

  private static _validateColumnsSpecification(columns: IMaisTableColumn[], config: IMaisTableConfiguration) {
    if (!columns || !columns.length) {
      return 'A non-empty list of columns must be provided';
    }

    for (const col of columns) {
      if ( !col.name ) {
        return 'Columns need to have a non-empty name.';
      }
    }

    for (const col of columns) {
      if ( (MaisTableFormatterHelper.isFilterable(col, config) || MaisTableFormatterHelper.isHideable(col, config) )
      && !MaisTableFormatterHelper.hasLabel(col, config) ) {
        return 'Columns to be filtered or toggled must have either a labelKey or a label. ' +
          'To hide a column label use the showLabelInTable property. Offending column is [' + col.name + ']';
      }
    }

    if (columns.filter(c => !MaisTableFormatterHelper.isHideable(c, config)).length < 1) {
      return 'At least one of the columns must be not hideable: set canHide=false on primary or relevant column.';
    }

    return null;
  }
}
