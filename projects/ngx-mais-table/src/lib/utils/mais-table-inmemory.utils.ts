import { IMaisTableColumn, IMaisTablePageResponse, IMaisTablePageRequest, MaisTableSortDirection } from '../model';
import { MaisTableFormatterHelper } from './mais-table-formatter.utils';

export abstract class MaisTableInMemoryHelper {

  public static fetchInMemory(
    data: any[],
    request: IMaisTablePageRequest,
    sortColumn?: IMaisTableColumn | null,
    filteringColumns?: IMaisTableColumn[],
    locale?: string ): IMaisTablePageResponse {
    const output: IMaisTablePageResponse = {
      content: [],
      size: 0,
      totalElements: 0,
      totalPages: 0
    };

    if (!data || !data.length) {
      return output;
    }

    // apply filtering
    let filtered = data;
    if (request.query && request.query.length && request.query.trim().length
      && request.queryFields && request.queryFields.length) {

      const cleanedSearchQuery = request.query.trim().toLowerCase();
      filtered = filtered.filter(candidateRow => {
        let allowed = false;
        if (filteringColumns) {
          for (const filteringColumn of filteringColumns) {
            const extractedValue = MaisTableFormatterHelper.extractValue(candidateRow, filteringColumn, locale);
            if (extractedValue) {
              const extractedValueStr = extractedValue + '';
              if (extractedValueStr.toLowerCase().indexOf(cleanedSearchQuery) !== -1) {
                allowed = true;
                break;
              }
            }
          }
        }
        return allowed;
      });
    }

    // apply pagination
    let startingIndex;
    let endingIndex;

    if (request && request.page != null && request.size != null &&
      (typeof request.page !== 'undefined') && (typeof request.size !== 'undefined') && (request.page === 0 || request.page > 0)) {
      startingIndex = request.page * request.size;
      endingIndex = (request.page + 1) * request.size;
      if (startingIndex >= filtered.length ) {
        return output;
      }

      if (endingIndex > filtered.length ) {
        endingIndex = filtered.length;
      }
    } else {
      startingIndex = 0;
      endingIndex = filtered.length;
    }

    if (request.sort && request.sort.length && request.sort[0]) {
      const sortDirection: number = request.sort[0].direction === MaisTableSortDirection.DESCENDING ? -1 : +1;

      if (sortColumn) {
        filtered.sort((c1, c2) => {
          const v1 = MaisTableFormatterHelper.extractValue(c1, sortColumn, locale) || '';
          const v2 = MaisTableFormatterHelper.extractValue(c2, sortColumn, locale) || '';
          const v1n = (v1 === null || v1 === undefined || Number.isNaN(v1));
          const v2n = (v2 === null || v2 === undefined || Number.isNaN(v2));
          if (v1n && v2n) {
            return 0;
          } else if (v1n && !v2n) {
            return -1 * sortDirection;
          } else if (!v1n && v2n) {
            return 1 * sortDirection;
          } else {
            return (v1 > v2 ? 1 : v1 < v2 ? -1 : 0)  * sortDirection;
          }
        });
      }
    }

    const fetchedPage = filtered.slice(startingIndex, endingIndex);
    output.content = fetchedPage;
    output.size = fetchedPage.length;
    output.totalElements = filtered.length;
    if (request.size) {
      output.totalPages = Math.ceil(filtered.length);
    } else {
      output.totalPages = 1;
    }
    return output;
  }
}
