import {
  MaisTableFormatter,
  IMaisTableColumn,
  IMaisTableFormatSpecification,
  IMaisTableFormatProvider,
  IMaisTableConfiguration
} from '../model';
import { LowerCasePipe, UpperCasePipe, DatePipe, CurrencyPipe } from '@angular/common';

// @dynamic
export abstract class MaisTableFormatterHelper {
  private static lowercasePipe = new LowerCasePipe();
  private static uppercasePipe = new UpperCasePipe();

  private static transformMap = {
    UPPERCASE: (raw: any, locale: string, args: any) => MaisTableFormatterHelper.uppercasePipe.transform(raw),
    LOWERCASE: (raw: any, locale: string, args: any) => MaisTableFormatterHelper.lowercasePipe.transform(raw),
    DATE: (raw: any, locale: string, args: any) => {
      return new DatePipe(locale).transform(raw, args);
    },
    CURRENCY: (raw: any, locale: string, args: any) => {
      // signature: currencyCode?: string, display?: string | boolean, digitsInfo?: string, locale?: string
      if (!args) {
        args = {};
      }
      return new CurrencyPipe(locale).transform(raw, args.currencyCode, args.display, args.digitsInfo, locale);
    }
  };

  public static getPropertyValue(object: any, field: string): string | null {
    if (object === null || object === undefined) {
      return null;
    }
    if (field.indexOf('.') === -1) {
      return object[field];
    }
    const splitted = field.split('.');
    const subObject = object[splitted[0]];
    return this.getPropertyValue(subObject, splitted.slice(1).join('.'));
  }

  public static format(raw: any, formatterSpec: IMaisTableFormatSpecification, locale?: string) {
    if (!raw) {
      return raw;
    }

    const transformFn = MaisTableFormatterHelper.transformMap[formatterSpec.formatter];
    if (!transformFn) {
      throw new Error('Unknown formatter: ' + formatterSpec.formatter);
    }

    return transformFn(raw, locale || 'en', formatterSpec.arguments);
  }

  public static extractValue(row: any, column: IMaisTableColumn, locale?: string) {
    if (!row) {
      return null;
    }
    let val: any;
    if (column.valueExtractor) {
      val = column.valueExtractor(row);
    } else if (column.field) {
      val = MaisTableFormatterHelper.getPropertyValue(row, column.field);
    } else {
      val = undefined;
    }

    if (column.formatters) {
      for (const formatter of (Array.isArray(column.formatters) ? column.formatters : [column.formatters])) {
        if ( (formatter as any).formatter ) {
          val = MaisTableFormatterHelper.format(val, (formatter as IMaisTableFormatSpecification), locale);
        } else if ( (formatter as any).format ) {
          val = (formatter as IMaisTableFormatProvider).format(val);
        } else {
          val = MaisTableFormatterHelper.format(val, { formatter: (formatter as MaisTableFormatter), arguments: null }, locale);
        }
      }
    }

    return val;
  }

  public static isDefaultVisibleColumn(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return (column.defaultVisible === true || column.defaultVisible === false ?
      column.defaultVisible : config?.columns?.defaultIsDefaulView) ||
        !MaisTableFormatterHelper.isHideable(column, config);
  }

  public static isDefaultFilterColumn(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return column.defaultFilter === true || column.defaultFilter === false ? column.defaultFilter : config?.columns?.defaultIsDefaultFilter;
  }

  public static isHideable(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean | undefined {
    return column.canHide === true || column.canHide === false ? column.canHide : config?.columns?.defaultCanHide;
  }

  public static isSortable(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return column.canSort === true || column.canSort === false ? column.canSort : config?.columns?.defaultCanSort;
  }

  public static isFilterable(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return (column.canFilter === true || column.canFilter === false ? column.canFilter : config?.columns?.defaultCanFilter) &&
      (column.serverField || column.field);
  }

  public static hasLabel(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return (column.labelKey || column.label);
  }

  public static isLabelVisibleInTable(column: IMaisTableColumn, config: IMaisTableConfiguration) {
    return column.showLabelInTable === true || column.showLabelInTable === false ?
      column.showLabelInTable : config?.columns?.defaultShowLabelInTable;
  }

}
