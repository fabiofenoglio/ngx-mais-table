import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTableModule } from '@angular/material/table';
import { MaisTableComponent } from './mais-table.component';

@NgModule({
  declarations: [
    MaisTableComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbModule,
    TranslateModule,
    CdkTableModule,
    MatTableModule,
    DragDropModule,
  ],
  exports: [
    MaisTableComponent
  ],
  providers: [
  ]
})
export class MaisTableModule { }
