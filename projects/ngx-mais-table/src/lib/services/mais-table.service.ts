import { Injectable } from '@angular/core';
import { MaisTableLogger, MaisTableBrowserHelper, IMaisTableLogAdapter } from '../utils';
import {
  MaisTableActionActivationCondition,
  MaisTablePaginationMethod,
  MaisTableRefreshStrategy,
  IMaisTableConfiguration
} from '../model';
import { MaisTableLoggerService } from './mais-table-logger.service';

@Injectable({providedIn: 'root'})
export class MaisTableService {

  private logger: MaisTableLogger;
  private configuration: IMaisTableConfiguration;

  constructor(private maisTableLoggerService: MaisTableLoggerService) {
    this.logger = new MaisTableLogger('MaisTableService');
    this.logger.trace('building service');
    this.configuration = this.buildDefaultConfiguration();

    this.logger.debug('initializing MAIS table component for browser', MaisTableBrowserHelper.getBrowser());
    this.maisTableLoggerService.withLogAdapter(null);
  }

  public configure(config: IMaisTableConfiguration): MaisTableService {
    Object.assign(this.configuration, config);
    return this;
  }

  public withLogAdapter(logAdapter: IMaisTableLogAdapter): MaisTableService {
    this.maisTableLoggerService.withLogAdapter(logAdapter);
    return this;
  }

  public getConfiguration(): IMaisTableConfiguration {
    return this.configuration;
  }

  private buildDefaultConfiguration(): IMaisTableConfiguration {
    return {
      currentSchemaVersion: 'DEFAULT',
      refresh: {
        defaultStrategy: MaisTableRefreshStrategy.NONE,
        defaultInterval: 60000,
        defaultOnPushInBackground: true,
        defaultOnTickInBackground: true
      },
      rowSelection: {
        enabledByDefault: false,
        selectAllEnabledByDefault: true,
        multipleSelectionEnabledByDefault: true
      },
      columnToggling: {
        enabledByDefault: true,
        showFixedColumns: true
      },
      filtering: {
        enabledByDefault: true,
        autoReloadOnQueryClear: true,
        autoReloadOnQueryChange: true,
        autoReloadTimeout: 300,
        showUnfilterableColumns: true
      },
      contextFiltering: {
        enabledByDefault: true
      },
      pagination: {
        enabledByDefault: true,
        pageSizeSelectionEnabledByDefault: true,
        defaultPaginationMode: MaisTablePaginationMethod.CLIENT,
        defaultPageSize: 5,
        defaultPossiblePageSizes: [5, 10, 25, 50]
      },
      actions: {
        contextActionsEnabledByDefault: true,
        headerActionsEnabledByDefault: true,
        defaultHeaderActionActivationCondition: MaisTableActionActivationCondition.ALWAYS,
        defaultContextActionActivationCondition: MaisTableActionActivationCondition.MULTIPLE_SELECTION
      },
      columns: {
        defaultShowLabelInTable: true,
        defaultCanSort: false,
        defaultCanHide: true,
        defaultCanFilter: true,
        defaultIsDefaultFilter: true,
        defaultIsDefaulView: true
      },
      rowExpansion: {
        enabledByDefault: false,
        multipleExpansionEnabledByDefault: false
      },
      dragAndDrop: {
        dragEnabledByDefault: false,
        dropEnabledByDefault: false
      },
      itemTracking: {
        enabledByDefault: false
      },
      storePersistence: {
        enabledByDefault: true
      }
    };
  }
}
