import { Injectable } from '@angular/core';
import { IMaisTableLogAdapter } from '../utils/mais-table-log-adapter';
import { MaisTableEmbeddedLogger } from '../utils/mais-table-embedded-logger';

@Injectable({providedIn: 'root'})
export class MaisTableLoggerService {

  private static embeddedLogger: IMaisTableLogAdapter = new MaisTableEmbeddedLogger();
  private static logAdapter: IMaisTableLogAdapter | null = null;

  constructor() {
  }

  public static getConfiguredLogAdapter(): IMaisTableLogAdapter {
    return MaisTableLoggerService.logAdapter || MaisTableLoggerService.embeddedLogger;
  }

  public withLogAdapter(logAdapter: IMaisTableLogAdapter | null) {
    MaisTableLoggerService.logAdapter = logAdapter;
  }

}
