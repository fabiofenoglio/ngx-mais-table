
export interface IMaisTableAction {
  // nome interno della action. OBBLIGATORIO
  name: string;

  // etichetta da mostrare nell'header della tabella
  label?: string;

  // chiave per ottenere l'etichetta da mostrare nell'header da i18n
  labelKey?: string;

  // classe bootstrap da associare al button
  displayClass?: string;

  // classi ulteriori da associare al button
  additionalClasses?: string;

  // condizione di attivazione. Se non specificato il default e' secondo ACTION_DEFAULTS
  activationCondition?: MaisTableActionActivationCondition;
}

export enum MaisTableActionActivationCondition {
  NEVER = 'NEVER',
  ALWAYS = 'ALWAYS',
  SINGLE_SELECTION = 'SINGLE_SELECTION',
  MULTIPLE_SELECTION = 'MULTIPLE_SELECTION',
  NO_SELECTION = 'NO_SELECTION',
  DYNAMIC = 'DYNAMIC'
}
