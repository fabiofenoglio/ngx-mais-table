import { IMaisTableColumn } from './mais-table-column.model';
import { IMaisTableAction } from './mais-table-action.model';
import { Observable } from 'rxjs';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { MaisTableComponent } from '../mais-table.component';

export enum MaisTableSortDirection {
  ASCENDING = 'ASC',
  DESCENDING = 'DESC'
}

export interface IMaisTableSort {
  property: string | null;
  direction?: MaisTableSortDirection;
}

export interface IMaisTablePageRequest {
  page?: number | null;
  size?: number | null;
  sort?: (IMaisTableSort | null)[];
  query?: string | null;
  queryFields?: (string | null)[];
  filters?: (string | null)[];
}

export interface IMaisTablePageResponse {
  content: any[]; // Returns the page content
  number?: number; // Returns the number of the current Slice.
  numberOfElements?: number; // Returns the number of elements currently on this Slice.
  size?: number; // Returns the size of the Slice.
  sort?: IMaisTableSort[]; // Returns the sorting parameters for the Slice.
  totalElements?: number; // Returns the total amount of elements.
  totalPages?: number; // Returns the number of total pages.
}

export enum MaisTablePaginationMethod {
  CLIENT = 'CLIENT',
  SERVER = 'SERVER'
}

export enum MaisTableFormatter {
  UPPERCASE = 'UPPERCASE',
  LOWERCASE = 'LOWERCASE',
  DATE = 'DATE',
  CURRENCY = 'CURRENCY'
}

export enum MaisTableRefreshStrategy {
  ON_PUSH = 'ON_PUSH',
  TIMED = 'TIMED',
  NONE = 'NONE'
}

export interface IMaisTableFormatProvider {
  format: (input: any) => any;
}

export interface IMaisTableFormatSpecification {
  formatter: MaisTableFormatter;
  arguments: any;
}

export interface IMaisTableSortingSpecification {
  column: IMaisTableColumn | null;
  direction: MaisTableSortDirection | null;
}

export interface IMaisTableActionDispatchingContext {
  selectedItems: any[];
  action: IMaisTableAction;
}

// tslint:disable-next-line: no-empty-interface
export interface IMaisTableActionDispatchingResult {
}

export interface IMaisTableStatusSnapshot {
  schemaVersion: string;
  orderColumn?: string | null;
  orderColumnDirection?: string;
  query?: string | null;
  queryColumns?: string[];
  visibleColumns?: string[];
  contextFilters?: string[];
  currentPage?: number;
  pageSize?: number;
  checkedItems?: any[];
  expandedItems?: any[];
}

export interface IMaisTablePersistableStatusSnapshot {
  schemaVersion: string;
  orderColumn?: string | null;
  orderColumnDirection?: string;
  query?: string | null;
  queryColumns?: string[];
  visibleColumns?: string[];
  contextFilters?: string[];
  currentPage?: number;
  pageSize?: number;
  checkedItemIdentifiers?: any[];
  expandedItemIdentifiers?: any[];
}

export interface IMaisTableStoreAdapterSaveContext {
  status: IMaisTablePersistableStatusSnapshot | null;
}

export interface IMaisTableStoreAdapterLoadContext {
  status: IMaisTablePersistableStatusSnapshot | null;
}

export interface IMaisTableStoreAdapter {
  save: (payload: IMaisTableStoreAdapterSaveContext | null) => Observable<boolean>;
  load: () => Observable<IMaisTablePersistableStatusSnapshot>;
}

export interface IMaisTableIdentifierProvider {
  extract: (input: any) => any;
}

export interface IMaisTableItemDraggedContext {
  item: any;
  event: CdkDragDrop<any>;
  fromDataSnapshot: any[];
  toDataSnapshot: any[];
  fromComponent: MaisTableComponent;
  toComponent: MaisTableComponent;
}

export interface IMaisTableItemDroppedContext {
  item: any;
  event: CdkDragDrop<any>;
  fromDataSnapshot: any[] | null;
  toDataSnapshot: any[];
  fromComponent: MaisTableComponent | null;
  toComponent: MaisTableComponent | null;
}

export interface IMaisTablePushRefreshRequest {
  event?: any;
  inBackground?: boolean;
}

export enum MaisTableReloadReason {
  INTERNAL = 'INTERNAL',
  INTERVAL = 'INTERVAL',
  USER = 'USER',
  PUSH = 'PUSH',
  EXTERNAL = 'EXTERNAL'
}

export interface IMaisTableReloadContext {
  reason: MaisTableReloadReason;
  inBackground?: boolean;
  withStatusSnapshot?: IMaisTablePersistableStatusSnapshot | null;
  pushRequest?: IMaisTablePushRefreshRequest | null;
}

export enum MaisTableColumnSize {
  XXS = 'xxs',
  XS = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  XL = 'xl',
  XXL = 'xxl',
  DEFAULT = 'default'
}
