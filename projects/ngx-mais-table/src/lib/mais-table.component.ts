import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  ContentChild,
  TemplateRef,
  ChangeDetectorRef,
  NgZone,
  OnChanges,
  SimpleChanges,
  AfterContentInit,
  ViewChild
} from '@angular/core';
import {
  MaisTableSortDirection,
  IMaisTablePageRequest,
  IMaisTablePageResponse,
  IMaisTableColumn,
  MaisTablePaginationMethod,
  IMaisTableSortingSpecification,
  IMaisTableAction,
  IMaisTableActionDispatchingContext,
  MaisTableActionActivationCondition,
  IMaisTableContextFilter,
  IMaisTableStatusSnapshot,
  IMaisTableStoreAdapter,
  IMaisTableIdentifierProvider,
  IMaisTableItemDraggedContext,
  IMaisTableItemDroppedContext,
  MaisTableRefreshStrategy,
  IMaisTablePushRefreshRequest,
  IMaisTableReloadContext,
  MaisTableReloadReason,
  IMaisTablePersistableStatusSnapshot
} from './model';
import {
  MaisTableFormatterHelper,
  MaisTableInMemoryHelper,
  MaisTableValidatorHelper,
  MaisTableBrowserHelper,
  MaisTableLogger
} from './utils';
import { MaisTableRegistryService, MaisTableService } from './services';

import { Observable, Subject, Subscriber, throwError, Subscription, timer } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { CdkDragDrop, CdkDrag } from '@angular/cdk/drag-drop';
import { animate, state, style, transition, trigger } from '@angular/animations';
import $ from 'jquery';
import { MatTable } from '@angular/material/table';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mais-table',
  templateUrl: './mais-table.component.html',
  styleUrls: ['./mais-table.component.scss', './mais-table-porting.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MaisTableComponent implements OnInit, OnDestroy, OnChanges, AfterContentInit {

  private static counter = 0;

  // input parameters
  @Input() data: any[] | Observable<any> | Subject<any>;
  @Input() dataProvider: ((input: IMaisTablePageRequest, context?: IMaisTableReloadContext)
    => Observable<IMaisTablePageResponse>) | null = null;
  @Input() columns: IMaisTableColumn[];
  @Input() defaultSortingColumn: string | null = null;
  @Input() defaultSortingDirection: string | null = null;
  @Input() defaultPageSize: number | null = null;
  @Input() possiblePageSize: number[] | null = null;
  @Input() tableId: string;
  @Input() dropConnectedTo: string;
  @Input() paginationMode: MaisTablePaginationMethod;
  @Input() enablePagination: boolean;
  @Input() enableSelection: boolean;
  @Input() enableContextFiltering: boolean;
  @Input() enableSelectAll: boolean;
  @Input() enableMultiSelect: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableColumnsSelection: boolean;
  @Input() enableContextActions: boolean;
  @Input() enableHeaderActions: boolean;
  @Input() enableRowExpansion: boolean;
  @Input() enablePageSizeSelect: boolean;
  @Input() enableMultipleRowExpansion: boolean;
  @Input() enableItemTracking: boolean;
  @Input() enableStorePersistence: boolean;
  @Input() enableDrag: boolean;
  @Input() enableDrop: boolean;
  @Input() contextActions: IMaisTableAction[];
  @Input() headerActions: IMaisTableAction[];
  @Input() contextFilters: IMaisTableContextFilter[];
  @Input() contextFilteringPrompt: string;
  @Input() actionStatusProvider: (action: IMaisTableAction, status: IMaisTableStatusSnapshot | null) => boolean;
  @Input() expandableStatusProvider: (row: any, status: IMaisTableStatusSnapshot | null) => boolean;
  @Input() storeAdapter: IMaisTableStoreAdapter;
  @Input() itemIdentifier: string | IMaisTableIdentifierProvider;
  @Input() refreshStrategy: MaisTableRefreshStrategy | MaisTableRefreshStrategy[];
  @Input() refreshInterval: number;
  @Input() refreshEmitter: Observable<IMaisTablePushRefreshRequest>;
  @Input() refreshIntervalInBackground: boolean;
  @Input() refreshOnPushInBackground: boolean;

  // output events
  @Output() pageChange = new EventEmitter<number>();
  @Output() sortChange = new EventEmitter<IMaisTableSortingSpecification>();
  @Output() selectionChange = new EventEmitter<any[]>();
  @Output() filteringColumnsChange = new EventEmitter<IMaisTableColumn[]>();
  @Output() visibleColumnsChange = new EventEmitter<IMaisTableColumn[]>();
  @Output() contextFiltersChange = new EventEmitter<IMaisTableContextFilter[]>();
  @Output() statusChange = new EventEmitter<IMaisTableStatusSnapshot>();
  @Output() itemDragged = new EventEmitter<IMaisTableItemDraggedContext>();
  @Output() itemDropped = new EventEmitter<IMaisTableItemDroppedContext>();
  @Output() action = new EventEmitter<IMaisTableActionDispatchingContext>();

  // input template
  @ContentChild('cellTemplate') cellTemplate: TemplateRef<any>;
  @ContentChild('actionsCaptionTemplate') actionsCaptionTemplate: TemplateRef<any>;
  @ContentChild('rowDetailTemplate') rowDetailTemplate: TemplateRef<any>;
  @ContentChild('dragTemplate') dragTemplate: TemplateRef<any>;
  @ContentChild('dropTemplate') dropTemplate: TemplateRef<any>;

  @ViewChild('#renderedMatTable') table: MatTable<any>;

  private isIE = MaisTableBrowserHelper.isIE();

  private logger: MaisTableLogger;
  private registrationId: string;
  private statusSnapshot: IMaisTableStatusSnapshot | null = null;
  private persistableStatusSnapshot: IMaisTableStatusSnapshot | null = null;
  private uuid: string;
  forceReRender = false;
  private initialized = false;

  // local data
  private clientDataSnapshot: any[];
  dataSnapshot: any[];

  private selectedPageIndex: number;
  private selectedPageSize: number | null;

  private selectedSortColumn: IMaisTableColumn | null;
  private selectedSortDirection: MaisTableSortDirection;

  selectedSearchQuery: string | null;
  private lastFetchedSearchQuery: string | null;

  // selected items with checkbox
  private checkedItems: any[];

  // selected columns for filtering
  private checkedColumnsForFiltering: IMaisTableColumn[];

  // selected columns for visualization
  private checkedColumnsForVisualization: IMaisTableColumn[];

  // selected custom filters
  private checkedContextFilters: IMaisTableContextFilter[];

  // expanded rows
  private expandedItems: any[];

  // for server pagination
  private fetchedPageCount: number | null;
  private fetchedResultNumber: number | null;
  fetching = false;
  showFetching = false;

  private refreshEmitterSubscription: Subscription | null = null;
  private refreshIntervalTimer: Observable<number> | null;
  private refreshIntervalSubscription: Subscription | null = null;

  // MatTable adapter
  matTableDataObservable: Subject<any[]> | null = null;
  expandedElement: any = null;

  // lifecycle hooks

  constructor(
    private translateService: TranslateService,
    private configurationService: MaisTableService,
    private registry: MaisTableRegistryService,
    private cdr: ChangeDetectorRef,
    private ngZone: NgZone ) {

    this.uuid = 'MTCMP-' + (++MaisTableComponent.counter) + '-' + Math.round(Math.random() * 100000);
    this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
    this.logger.trace('building component');

    this.matTableDataObservable = new Subject<any[]>();
  }

  ngOnInit() {
    this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
    this.logger.trace('initializing component');

    this.registrationId = this.registry.register(this.currentTableId, this);

    // input validation
    MaisTableValidatorHelper.validateColumnsSpecification(this.columns, this.configurationService.getConfiguration());

    this.clientDataSnapshot = [];
    this.dataSnapshot = [];
    this.fetchedPageCount = 0;
    this.fetchedResultNumber = 0;
    this.selectedPageIndex = 0;
    this.selectedSearchQuery = null;
    this.selectedPageSize = null;
    this.expandedItems = [];
    this.checkedItems = [];
    this.checkedContextFilters = [];
    this.checkedColumnsForFiltering = this.filterableColumns.filter(
      c => MaisTableFormatterHelper.isDefaultFilterColumn(c, this.configurationService.getConfiguration()));
    this.checkedColumnsForVisualization = this.columns.filter(
      c => MaisTableFormatterHelper.isDefaultVisibleColumn(c, this.configurationService.getConfiguration()));
    this.statusSnapshot = this.buildStatusSnapshot();
    this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();

    if (this.dataProvider) {
      // input is a provider function
      if (this.data) {
        throw new Error('MaisTable can\'t be provided both data and dataProvider');
      }

    } else if (this.data instanceof Observable || this.data instanceof Subject) {
      // input is observable
      this.data.subscribe(dataSnapshot => {
        this.handleInputDataObservableEmission(dataSnapshot);
      });
    } else {
      // input is data array
      this.clientDataSnapshot = this.data;
    }

    // LOAD STATUS from store if possible
    let activationObservable: Observable<IMaisTablePersistableStatusSnapshot | null>;
    if (this.storePersistenceEnabledAndPossible) {
      activationObservable = this.loadStatusFromStore();
    } else {
      activationObservable = new Observable(subscriber => {
        subscriber.next(null);
        subscriber.complete();
      });
    }

    activationObservable.subscribe((status: IMaisTablePersistableStatusSnapshot) => {
      if (status) {
        this.logger.trace('restored status snapshot from storage', status);
        this.applyStatusSnapshot(status);
      }

      this.reload({reason: MaisTableReloadReason.INTERNAL, withStatusSnapshot: status}).subscribe(yes => {
        this.completeInitialization();
      }, nope => {
        this.completeInitialization();
      });

    }, failure => {
      this.logger.error('restoring status from store failed', failure);
      this.reload({reason: MaisTableReloadReason.INTERNAL}).subscribe(yes => {
        this.completeInitialization();
      }, nope => {
        this.completeInitialization();
      });
    });
  }

  ngAfterContentInit() {
    this.logger.debug('after content init');
  }

  private handleInputDataObservableEmission(dataSnapshot: any[]) {
    this.logger.debug('data snapshot emitted from input data');
    this.clientDataSnapshot = dataSnapshot;
    this.reload({reason: MaisTableReloadReason.EXTERNAL});
  }

  private completeInitialization() {
    this.initialized = true;

    this.statusChange.pipe(debounceTime(200)).subscribe(statusSnapshot => {
      this.persistStatusToStore().subscribe();
    });

    if (this.refreshEmitter) {
      this.handleRefreshEmitterChange(this.refreshEmitter);
    }
    if (this.refreshInterval) {
      this.handleRefreshIntervalChange(this.refreshInterval);
    }
  }

  ngOnDestroy() {
    this.logger.trace('destroying component');
    if (this.registrationId) {
      this.registry.unregister(this.currentTableId, this.registrationId);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.refreshEmitter) {
      this.handleRefreshEmitterChange(changes.refreshEmitter.currentValue);
    }
    if (changes.refreshInterval) {
      this.handleRefreshIntervalChange(changes.refreshInterval.currentValue);
    }
    if (changes.refreshStrategy && !changes.refreshStrategy.firstChange) {
      this.logger.warn('REFRESH STRATEGY CHANGED WHILE RUNNING. YOU SURE ABOUT THIS?', changes);
      this.handleRefreshEmitterChange(this.refreshEmitter);
      this.handleRefreshIntervalChange(this.refreshInterval);
    }
  }

  private handleRefreshEmitterChange(newValue: Observable<IMaisTablePushRefreshRequest>) {
    if (this.refreshEmitterSubscription) {
      this.refreshEmitterSubscription.unsubscribe();
    }
    if (newValue) {
      this.refreshEmitterSubscription = newValue.subscribe(event => {
        this.logger.trace('received refresh push request', event);
        if (this.dragInProgress) {
          this.logger.warn('refresh from emitter ignored because user is dragging elements');
        } else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.ON_PUSH) === -1) {
          this.logger.warn('refresh from emitter ignored because refresh strategies are ' + this.currentRefreshStrategies +
            '. Why are you pushing to this component?');
        } else if (!this.initialized) {
          this.logger.warn('refresh from emitter ignored because the component is not fully initialized');
        } else if (this.fetching) {
          this.logger.warn('refresh from emitter ignored because the component is fetching already');
        } else {
          this.logger.debug('launching reload following push request');
          this.reload({
            reason: MaisTableReloadReason.PUSH,
            pushRequest: event,
            inBackground: event.inBackground === true || event.inBackground === false ?
              event.inBackground : this.currentRefreshOnPushInBackground
          });
        }
      });
    }
  }

  private handleRefreshIntervalChange(newValue: number) {
    if (this.refreshIntervalSubscription) {
      this.refreshIntervalSubscription.unsubscribe();
    }
    if (this.refreshIntervalTimer) {
      this.refreshIntervalTimer = null;
    }
    if (newValue) {
      this.refreshIntervalTimer = timer(newValue, newValue);
      this.refreshIntervalSubscription = this.refreshIntervalTimer.subscribe(tick => {
        this.logger.trace('emitted refresh tick request');
        if (this.dragInProgress) {
          this.logger.warn('refresh from emitter ignored because user is dragging elements');
        } else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.TIMED) === -1) {
          this.logger.warn('refresh from tick ignored because refresh strategies are ' + this.currentRefreshStrategies +
            '. Why is this component emitting ticks ?');
        } else if (!this.initialized) {
          this.logger.warn('refresh from tick ignored because the component is not fully initialized');
        } else if (this.fetching) {
          this.logger.warn('refresh from tick ignored because the component is fetching already');
        } else {
          this.logger.debug('launching reload following tick');
          this.reload({
            reason: MaisTableReloadReason.INTERVAL,
            inBackground: this.currentRefreshIntervalInBackground
          });
        }
      });
    }
  }

  // public methods

  public refresh(background?: boolean): Observable<void> {
    if (!this.initialized) {
      return throwError('table component is still initializing');
    }
    return this.reload({reason: MaisTableReloadReason.EXTERNAL, inBackground: background});
  }

  public getDataSnapshot(): any[] {
    return this.dataSnapshot;
  }

  public getStatusSnapshot(): IMaisTableStatusSnapshot | null {
    return this.statusSnapshot;
  }

  public loadStatus(status: IMaisTablePersistableStatusSnapshot) {
    if (!this.initialized) {
      return throwError('table component is still initializing');
    }
    this.logger.trace('loading status snapshot from external caller', status);
    this.applyStatusSnapshot(status);
    this.reload({reason: MaisTableReloadReason.EXTERNAL, withStatusSnapshot: status});
  }

  private applyStatusSnapshot(status: IMaisTablePersistableStatusSnapshot) {
   if (!status || !status.schemaVersion) {
     this.logger.warn('not restoring status because it is malformed');
     return;
   }

   if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
    this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
      status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
    return;
   }

   if (status.orderColumn) {
    this.selectedSortColumn = this.columns.find(c => this.isSortable(c) && c.name === status.orderColumn) || null;
   }

   if (status.orderColumnDirection) {
     this.selectedSortDirection = (status.orderColumnDirection === MaisTableSortDirection.DESCENDING) ?
      MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
   }

   if (this.currentEnableFiltering && status.query) {
     this.selectedSearchQuery = status.query.trim();
   }

   if (this.currentEnableFiltering && status.queryColumns && status.queryColumns.length) {
    this.checkedColumnsForFiltering = this.filterableColumns.filter(c => status?.queryColumns?.indexOf(c.name) !== -1);
   }

   if (this.currentEnableColumnsSelection && status.visibleColumns && status.visibleColumns.length) {
    this.checkedColumnsForVisualization = this.columns.filter(c => !this.isHideable(c) || status?.visibleColumns?.indexOf(c.name) !== -1);
   }

   if (this.currentEnableContextFiltering && status.contextFilters && status.contextFilters.length) {
     this.checkedContextFilters = this.contextFilters.filter(f => status?.contextFilters?.indexOf(f.name) !== -1);
   }

   if (this.currentEnablePagination && status.currentPage || status.currentPage === 0) {
     this.selectedPageIndex = status.currentPage;
   }

   if (this.currentEnablePagination && status.pageSize) {
     this.selectedPageSize = this.currentPossiblePageSizes.find(s => status.pageSize === s) || null;
   }

   this.statusSnapshot = this.buildStatusSnapshot();
   this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
  }

  private applyStatusSnapshotPostFetch(status: IMaisTablePersistableStatusSnapshot) {
   if (!status || !status.schemaVersion) {
     this.logger.warn('not restoring status because it is malformed');
     return;
   }

   if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
    this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
      status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
    return;
   }

   if (this.currentEnableSelection && this.itemTrackingEnabledAndPossible
      && status.checkedItemIdentifiers && status.checkedItemIdentifiers.length) {

    this.checkedItems = this.dataSnapshot.filter(data => {
      const id = this.getItemIdentifier(data);
      return id && status?.checkedItemIdentifiers?.indexOf(id) !== -1;
    });
   }

   if (this.currentEnableRowExpansion && this.itemTrackingEnabledAndPossible
      && status.expandedItemIdentifiers && status.expandedItemIdentifiers.length) {

    this.expandedItems = this.dataSnapshot.filter(data => {
      const id = this.getItemIdentifier(data);
      return id && status?.expandedItemIdentifiers?.indexOf(id) !== -1 && this.isExpandable(data);
    });
   }

   this.statusSnapshot = this.buildStatusSnapshot();
   this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
  }

  private reload( context: IMaisTableReloadContext ): Observable<void> {
    this.fetching = true;
    this.showFetching = !context.inBackground;
    const obs: Observable<void> = new Observable<void>(subscriber => {
      try {
        this.reloadInObservable(subscriber, context);
      } catch (e) {
        subscriber.error(e);
        subscriber.complete();
      }
    });

    obs.subscribe(success => {
      this.logger.trace('async reload success');
      this.fetching = false;
      if (!context.inBackground) {
        this.showFetching = false;
      }

      this.handleMatTableDataSnapshotChanged();
    }, failure => {
      this.logger.trace('async reload failed', failure);
      this.fetching = false;
      if (!context.inBackground) {
        this.showFetching = false;
      }

      this.handleMatTableDataSnapshotChanged();
    });
    return obs;
  }

  private reloadInObservable(tracker: Subscriber<void>, context: IMaisTableReloadContext) {
    const withSnapshot: IMaisTablePersistableStatusSnapshot | null = context.withStatusSnapshot || null;

    const pageRequest = this.buildPageRequest();

    this.logger.debug('reloading table data', pageRequest);

    // clear checked items
    if (!context.inBackground) {
      this.checkedItems = [];
      this.expandedItems = [];

      this.statusChanged();
    }
    this.lastFetchedSearchQuery = pageRequest.query || null;

    if (this.dataProvider) {
      // call data provider
      this.logger.trace('reload has been called, fetching data from provided function');
      this.logger.trace('page request for data fetch is', pageRequest);

      this.dataProvider(pageRequest, context).subscribe((response: IMaisTablePageResponse) => {
        this.logger.trace('fetching data completed successfully');

        if (!this.dragInProgress) {
          if (this.paginationMode === MaisTablePaginationMethod.SERVER) {
            this.parseResponseWithServerPagination(response);
          } else {
            this.parseResponseWithClientPagination(response.content, pageRequest);
          }
          if (withSnapshot) {
            this.applyStatusSnapshotPostFetch(withSnapshot);
          }
        } else {
          this.logger.warn('data fetch aborted because user is dragging things');
        }

        tracker.next();
        tracker.complete();
      }, failure => {
        this.logger.error('error fetching data from provider function', failure);

        if (!this.dragInProgress) {
          this.dataSnapshot = [];
        } else {
          this.logger.warn('data fetch aborted because user is dragging things');
        }

        tracker.error(failure);
        tracker.complete();
      });

    } else {
      // data is not provided on request and is in clientDataSnapshot
      this.logger.trace('reload has been called on locally fetched data');

      if (!this.dragInProgress) {
        this.parseResponseWithClientPagination(this.clientDataSnapshot, pageRequest);
        if (withSnapshot) {
          this.applyStatusSnapshotPostFetch(withSnapshot);
        }
      } else {
        this.logger.warn('data fetch aborted because user is dragging things');
      }

      tracker.next();
      tracker.complete();
    }
  }

  private parseResponseWithServerPagination(response: IMaisTablePageResponse) {
    this.dataSnapshot = response.content;

    if (this.currentEnablePagination) {
      if (response.totalPages) {
        this.fetchedPageCount = response.totalPages;
      } else {
        throw new Error('data from server did not contain required totalPages field');
      }

      if (response.totalElements) {
        this.fetchedResultNumber = response.totalElements;
      } else {
        throw new Error('data from server did not contain required totalElements field');
      }
    }
  }

  private parseResponseWithClientPagination(data: any[], request: IMaisTablePageRequest) {
    this.logger.trace('applying in-memory fetching, paginating, ordering and filtering');
    const inMemoryResponse = MaisTableInMemoryHelper.fetchInMemory(
      data,
      request,
      this.currentSortColumn,
      this.checkedColumnsForFiltering,
      this.getCurrentLocale() );

    this.dataSnapshot = inMemoryResponse.content;
    this.fetchedResultNumber = typeof inMemoryResponse.totalElements === 'undefined' ? null : inMemoryResponse.totalElements;
    this.fetchedPageCount = typeof inMemoryResponse.totalPages === 'undefined' ? null : inMemoryResponse.totalPages;
  }

  private buildPageRequest(): IMaisTablePageRequest {
    const output: IMaisTablePageRequest = {
      page: this.currentEnablePagination ? this.currentPageIndex : null,
      size: this.currentEnablePagination ? this.currentPageSize : null,
      sort: [],
      query: null,
      queryFields: [],
      filters: []
    };

    if (this.currentEnableFiltering && this.searchQueryActive) {
      output.query = this.currentSearchQuery;
      output.queryFields = this.checkedColumnsForFiltering.map(column => column.serverField || column.field || null );
    }

    if (this.currentEnableContextFiltering && this.checkedContextFilters.length) {
      for (const filter of this.checkedContextFilters) {
        output.filters?.push(filter.name || null);
      }
    }

    const sortColumn = this.currentSortColumn;
    const sortDirection = this.currentSortDirection;
    if (sortColumn) {
      output.sort?.push({
        property: sortColumn.serverField || sortColumn.field || null,
        direction: sortDirection || MaisTableSortDirection.ASCENDING
      });
    }

    return output;
  }

  private setPage(index: number) {
    this.selectedPageIndex = index;
    this.statusChanged();
    this.emitPageChanged();
  }

  get currentRefreshOnPushInBackground(): boolean {
    if (this.refreshOnPushInBackground === true || this.refreshOnPushInBackground === false) {
      return this.refreshOnPushInBackground;
    } else {
      return this.configurationService.getConfiguration().refresh?.defaultOnPushInBackground!;
    }
  }

  get currentRefreshIntervalInBackground(): boolean {
    if (this.refreshIntervalInBackground === true || this.refreshIntervalInBackground === false) {
      return this.refreshIntervalInBackground;
    } else {
      return this.configurationService.getConfiguration().refresh?.defaultOnTickInBackground!;
    }
  }

  get currentRefreshStrategies(): MaisTableRefreshStrategy[] {
    if (this.refreshStrategy) {
      if (Array.isArray(this.refreshStrategy) && this.refreshStrategy.length) {
        return this.refreshStrategy;
      } else {
        return [this.refreshStrategy as MaisTableRefreshStrategy];
      }
    }
    return [this.configurationService.getConfiguration().refresh?.defaultStrategy!];
  }

  get currentRefreshInterval(): number {
    return this.refreshInterval || this.configurationService.getConfiguration().refresh?.defaultInterval!;
  }

  get currentTableId(): string {
    return this.tableId || this.uuid;
  }

  get currentEnableDrag(): boolean {
    if (this.enableDrag === true || this.enableDrag === false) {
      return this.enableDrag;
    } else {
      return this.configurationService.getConfiguration().dragAndDrop?.dragEnabledByDefault!;
    }
  }

  get currentEnableDrop(): boolean {
    if (this.enableDrop === true || this.enableDrop === false) {
      return this.enableDrop;
    } else {
      return this.configurationService.getConfiguration().dragAndDrop?.dropEnabledByDefault!;
    }
  }

  get currentEnablePagination(): boolean {
    if (this.enablePagination === true || this.enablePagination === false) {
      return this.enablePagination;
    } else {
      return this.configurationService.getConfiguration().pagination?.enabledByDefault!;
    }
  }

  get currentEnableItemTracking(): boolean {
    if (this.enableItemTracking === true || this.enableItemTracking === false) {
      return this.enableItemTracking;
    } else {
      return this.configurationService.getConfiguration().itemTracking?.enabledByDefault!;
    }
  }

  get currentEnableStorePersistence(): boolean {
    if (this.enableStorePersistence === true || this.enableStorePersistence === false) {
      return this.enableStorePersistence;
    } else {
      return this.configurationService.getConfiguration().storePersistence?.enabledByDefault!;
    }
  }

  get currentEnablePageSizeSelect(): boolean {
    if (this.enablePageSizeSelect === true || this.enablePageSizeSelect === false) {
      return this.enablePageSizeSelect;
    } else {
      return this.configurationService.getConfiguration().pagination?.pageSizeSelectionEnabledByDefault!;
    }
  }

  get currentEnableMultipleRowExpansion(): boolean {
    if (this.enableMultipleRowExpansion === true || this.enableMultipleRowExpansion === false) {
      return this.enableMultipleRowExpansion;
    } else {
      return this.configurationService.getConfiguration().rowExpansion?.multipleExpansionEnabledByDefault!;
    }
  }

  get currentEnableRowExpansion(): boolean {
    if (this.enableRowExpansion === true || this.enableRowExpansion === false) {
      return this.enableRowExpansion;
    } else {
      return this.configurationService.getConfiguration().rowExpansion?.enabledByDefault!;
    }
  }

  get currentEnableHeaderActions(): boolean {
    if (this.enableHeaderActions === true || this.enableHeaderActions === false) {
      return this.enableHeaderActions;
    } else {
      return this.configurationService.getConfiguration().actions?.headerActionsEnabledByDefault!;
    }
  }

  get currentEnableContextActions(): boolean {
    if (this.enableContextActions === true || this.enableContextActions === false) {
      return this.enableContextActions;
    } else {
      return this.configurationService.getConfiguration().actions?.contextActionsEnabledByDefault!;
    }
  }

  get currentEnableColumnsSelection(): boolean {
    if (this.enableColumnsSelection === true || this.enableColumnsSelection === false) {
      return this.enableColumnsSelection;
    } else {
      return this.configurationService.getConfiguration().columnToggling?.enabledByDefault!;
    }
  }

  get currentEnableFiltering(): boolean {
    if (this.enableFiltering === true || this.enableFiltering === false) {
      return this.enableFiltering;
    } else {
      return this.configurationService.getConfiguration().filtering?.enabledByDefault!;
    }
  }

  get currentEnableMultiSelect(): boolean {
    if (this.enableMultiSelect === true || this.enableMultiSelect === false) {
      return this.enableMultiSelect;
    } else {
      return this.configurationService.getConfiguration().rowSelection?.multipleSelectionEnabledByDefault!;
    }
  }

  get currentEnableSelectAll(): boolean {
    if (this.enableSelectAll === true || this.enableSelectAll === false) {
      return this.enableSelectAll;
    } else {
      return this.configurationService.getConfiguration().rowSelection?.selectAllEnabledByDefault!;
    }
  }

  get currentEnableContextFiltering(): boolean {
    if (this.enableContextFiltering === true || this.enableContextFiltering === false) {
      return this.enableContextFiltering;
    } else {
      return this.configurationService.getConfiguration().contextFiltering?.enabledByDefault!;
    }
  }

  get currentEnableSelection(): boolean {
    if (this.enableSelection === true || this.enableSelection === false) {
      return this.enableSelection;
    } else {
      return this.configurationService.getConfiguration().rowSelection?.enabledByDefault!;
    }
  }

  get currentPaginationMode(): MaisTablePaginationMethod {
    if (this.paginationMode) {
      return this.paginationMode;
    } else {
      return this.configurationService.getConfiguration().pagination?.defaultPaginationMode!;
    }
  }

  get itemTrackingEnabledAndPossible(): boolean {
    return !!this.itemIdentifier && this.currentEnableItemTracking;
  }

  get storePersistenceEnabledAndPossible(): boolean {
    return this.storeAdapter && this.currentEnableStorePersistence;
  }

  get rowExpansionEnabledAndPossible(): boolean {
    return this.currentEnableRowExpansion;
  }

  get pageSizeSelectEnabledAndPossible(): boolean {
    return this.currentPossiblePageSizes && this.currentPossiblePageSizes.length > 0 && this.currentEnablePageSizeSelect;
  }

  get contextFilteringEnabledAndPossible(): boolean {
    return this.contextFilters && this.contextFilters.length > 0 && this.currentEnableContextFiltering;
  }

  get contextActionsEnabledAndPossible(): boolean {
    return this.contextActions && this.contextActions.length > 0 && this.currentEnableContextActions;
  }

  get headerActionsEnabledAndPossible(): boolean {
    return this.headerActions && this.headerActions.length > 0 && this.currentEnableHeaderActions;
  }

  get columnSelectionPossibleAndAllowed(): boolean {
    return this.currentEnableColumnsSelection && this.columns.length > 0 && this.hideableColumns.length > 0;
  }

  get filteringPossibleAndAllowed(): boolean {
    return this.currentEnableFiltering && this.filterableColumns.length > 0;
  }

  get currentPossiblePageSizes(): number[] {
    if (this.possiblePageSize && this.possiblePageSize.length) {
      return this.possiblePageSize;
    } else {
      return this.configurationService.getConfiguration().pagination?.defaultPossiblePageSizes!;
    }
  }

  get currentSearchQuery(): string | null {
    if (this.selectedSearchQuery) {
      return this.selectedSearchQuery;
    } else {
      return null;
    }
  }

  get currentPageCount(): number {
    return this.fetchedPageCount || 0;
  }

  get currentResultNumber(): number {
    return this.fetchedResultNumber || 0;
  }

  get currentPageSize(): number {
    const def = this.configurationService.getConfiguration().pagination?.defaultPageSize!;
    if (this.selectedPageSize) {
      return this.selectedPageSize;
    } else if (this.defaultPageSize) {
      return this.defaultPageSize;
    } else if (
        this.currentPossiblePageSizes.length &&
        def &&
        this.currentPossiblePageSizes.indexOf(def) !== -1) {
        return def;
    } else if (this.currentPossiblePageSizes.length) {
      return this.currentPossiblePageSizes[0];
    } else if (def) {
      return def;
    } else {
      return 10;
    }
  }

  get currentPageIndex(): number {
    if (this.selectedPageIndex) {
      return this.selectedPageIndex;
    } else {
      return 0;
    }
  }

  get currentSortColumn(): IMaisTableColumn | null {
    if (this.selectedSortColumn) {
      // todo log if sorting column disappears
      return this.visibleColumns.find(c => c.name === this.selectedSortColumn?.name) || null;
    } else if (this.defaultSortingColumn) {
      const foundColumn = this.visibleColumns.find(c => c.name === this.defaultSortingColumn);
      if (foundColumn) {
        return foundColumn;
      } else {
        // find first sortable column?
        return this.visibleColumns.find(column =>
          MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
      }
    } else {
      // find first sortable column?
      return this.visibleColumns.find(column =>
        MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
    }
  }

  get currentSortDirection(): MaisTableSortDirection {
    if (this.selectedSortDirection) {
      return this.selectedSortDirection;
    } else if (this.defaultSortingDirection) {
      return this.defaultSortingDirection === MaisTableSortDirection.DESCENDING ?
      MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
    } else {
      // return DEFAULT
      return MaisTableSortDirection.ASCENDING;
    }
  }

  get currentData(): any[] {
    return this.dataSnapshot || [];
  }

  get enumPages(): (number | { skip: boolean; })[] {
    const rangeStart = 1;
    const rangeEnd = 1;
    const rangeSelected = 1;

    const pages = [];
    const pageNum = this.currentPageCount;
    const currentIndex = this.currentPageIndex;
    let isSkipping = false;

    for (let i = 0; i < pageNum; i ++) {
      if (Math.abs(i - currentIndex) <= (rangeSelected) || i <= (rangeStart - 1) || i >= (pageNum - rangeEnd)) {
        isSkipping = false;
        pages.push(i);
      } else {
        if (!isSkipping) {
          pages.push({
            skip: true
          });
          isSkipping = true;
        }
      }
    }
    return pages;
  }

  get activeColumnsCount(): number {
    let output = this.visibleColumns.length;
    if (this.currentEnableSelection) {
      output ++;
    }
    if (this.contextFilteringEnabledAndPossible) {
      output ++;
    }
    if (this.rowExpansionEnabledAndPossible) {
      output ++;
    }
    return output;
  }

  get noResults(): boolean {
    if (this.paginationMode === MaisTablePaginationMethod.CLIENT) {
      return !this.currentData.length;
    } else {
      return !this.dataSnapshot.length;
    }
  }

  get hideableColumns(): IMaisTableColumn[] {
    return this.columns.filter(c => MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
  }

  get visibleColumns(): IMaisTableColumn[] {
    return this.columns.filter(c => this.checkedColumnsForVisualization.indexOf(c) !== -1);
  }

  get currentContextFilters(): IMaisTableContextFilter[] {
    return this.contextFilters || [];
  }

  get currentHeaderActions(): IMaisTableAction[] {
    return this.headerActions || [];
  }

  get currentActions(): IMaisTableAction[] {
    return this.contextActions || [];
  }

  get hasActions(): boolean {
    return this.currentActions.length > 0;
  }

  get searchQueryNeedApply(): boolean {
    return (this.currentSearchQuery || '') !== (this.lastFetchedSearchQuery || '');
  }

  get searchQueryActive(): boolean {
    if (!this.currentSearchQuery) {
      return false;
    }
    if (!this.checkedColumnsForFiltering.length) {
      return false;
    }
    return true;
  }

  get searchQueryMalformed(): boolean {
    if (!this.currentSearchQuery) {
      return false;
    }
    if (!this.checkedColumnsForFiltering.length) {
      return true;
    }
    return false;
  }

  get filterableColumns(): IMaisTableColumn[] {
    return this.columns.filter(c => this.isFilterable(c));
  }

  get anyButtonActionsAllowed(): boolean {
    return !!this.contextActions.find(a => this.isContextActionAllowed(a));
  }

  get anyHeaderActionsAllowed(): boolean {
    return !!this.headerActions.find(a => this.isHeaderActionAllowed(a));
  }

  get configColumnVisibilityShowFixedColumns() {
    return this.configurationService.getConfiguration().columnToggling?.showFixedColumns;
  }

  get configFilteringShowUnfilterableColumns() {
    return this.configurationService.getConfiguration().filtering?.showUnfilterableColumns;
  }

  // drag and drop support

  acceptDropPredicate = (item: CdkDrag<any>) => {
    return this.acceptDrop;
  }

  get acceptDrop() {
    return this.currentEnableDrop;
  }

  get acceptDrag() {
    return this.currentEnableDrag;
  }

  get referencedTable(): MaisTableComponent | null {
    const v = this.getReferencedTable();
    return v;
  }

  getReferencedTable(): MaisTableComponent | null {
    if (this.dropConnectedTo && this.dropConnectedTo.length) {
      const connectedToList: string[] = (Array.isArray(this.dropConnectedTo)) ? this.dropConnectedTo : [this.dropConnectedTo];
      const found = [];
      for (const connectedToToken of connectedToList) {
        const registeredList: any[] = this.registry.get(connectedToToken);
        if (registeredList && registeredList.length) {
          for (const regComp of registeredList) {
            found.push(regComp);
          }
        }
      }
      if (found.length < 1) {
        return null;
      } else if (found.length > 1) {
        this.logger.error('MULTIPLE TABLE REFERENCED STILL ACTIVE', found);
        return null;
      } else {
        return found[0].component;
      }
    } else {
      return this;
    }
  }

  // user actions

  handleItemDragged(event: CdkDragDrop<any[]>, from: MaisTableComponent, to: MaisTableComponent) {
    this.logger.debug('item dragged from table ' + this.currentTableId);
    this.itemDragged.emit({
      item: event.item.data,
      event,
      fromDataSnapshot: from.getDataSnapshot(),
      toDataSnapshot: to.getDataSnapshot(),
      fromComponent: from,
      toComponent: to
    });
  }

  handleItemDropped(event: CdkDragDrop<any[]>) {
    this.logger.debug('DROP EVENT FROM TABLE ' + this.currentTableId, event);
    const droppedSource: MaisTableComponent | null = this.registry.getSingle(event.previousContainer.id);
    const droppedTarget: MaisTableComponent | null = this.registry.getSingle(event.container.id);

    if (!droppedTarget) {
      this.logger.warn('NO DROP TARGET FOUND');
      return;
    }

    if (!droppedTarget.acceptDrop) {
      this.logger.debug('skipping drop on container with acceptDrop = false');
      return;
    }

    if (droppedSource) {
      droppedSource.handleItemDragged(event, droppedSource, droppedTarget);
    }

    this.logger.debug('item dropped on table ' + droppedTarget.currentTableId);
    this.itemDropped.emit({
      item: event.item.data,
      event,
      fromDataSnapshot: droppedSource ? droppedSource.getDataSnapshot() : null,
      toDataSnapshot: droppedTarget.getDataSnapshot(),
      fromComponent: droppedSource,
      toComponent: droppedTarget
    });

    // I'm sorry
    this.cdr.detectChanges();
    this.ngZone.run(() => {
      this.dataSnapshot = this.dataSnapshot.map(o => o);
      this.cdr.detectChanges();
      setTimeout(() => this.forceReRender = true);
      setTimeout(() => this.forceReRender = false);
      this.handleMatTableDataSnapshotChanged();
    });
  }

  applySelectedFilter() {
    this.setPage(0);
    this.statusChanged();
    this.reload({reason: MaisTableReloadReason.USER});
  }

  clickOnRowExpansion(item: any) {
    if (!this.rowExpansionEnabledAndPossible) {
      return;
    }
    if (!this.isExpandable(item)) {
      return;
    }

    if (this.isExpanded(item)) {
      this.expandedItems.splice(this.expandedItems.indexOf(item), 1);
    } else {
      if (!this.currentEnableMultipleRowExpansion) {
        this.expandedItems = [];
      }
      this.expandedItems.push(item);
    }

    this.handleMatTableDataSnapshotChanged();
    this.statusChanged();
  }

  clickOnContextAction(action: IMaisTableAction) {
    if (!this.currentEnableContextActions) {
      this.logger.warn('button actions are disabled');
      return;
    }
    if (!this.isContextActionAllowed(action)) {
      return;
    }

    const dispatchContext: IMaisTableActionDispatchingContext = {
      action,
      selectedItems: this.checkedItems
    };

    this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);

    this.action.emit(dispatchContext);
  }

  clickOnPageSize(pageSize: number) {
    this.selectedPageSize = pageSize;
    this.setPage(0);
    this.statusChanged();
    this.reload({reason: MaisTableReloadReason.USER});
  }

  clickOnHeaderAction(action: IMaisTableAction) {
    if (!this.currentEnableHeaderActions) {
      this.logger.warn('button actions are disabled');
      return;
    }
    if (!this.isHeaderActionAllowed(action)) {
      return;
    }

    const dispatchContext: IMaisTableActionDispatchingContext = {
      action,
      selectedItems: this.checkedItems
    };

    this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);

    this.action.emit(dispatchContext);
  }

  searchQueryFocusOut() {
    const comp = this;
    setTimeout(() => {
      if ((comp.selectedSearchQuery || '') !== (comp.lastFetchedSearchQuery || '')) {
        if (comp.selectedSearchQuery) {
          if (this.configurationService.getConfiguration().filtering?.autoReloadOnQueryChange) {
            comp.applySelectedFilter();
          } else {
            // comp.selectedSearchQuery = comp.lastFetchedSearchQuery;
          }
        } else {
          if (this.configurationService.getConfiguration().filtering?.autoReloadOnQueryClear) {
            comp.applySelectedFilter();
          }
        }
      }
    }, this.configurationService.getConfiguration().filtering?.autoReloadTimeout);
  }

  switchToPage(pageIndex: number, context?: IMaisTableReloadContext) {
    if (this.currentPageIndex === pageIndex) {
      return;
    }
    if (!context) {
      context = {reason: MaisTableReloadReason.USER};
    }

    this.setPage(pageIndex);
    this.statusChanged();
    this.reload(context);
  }

  clickOnColumn(column: IMaisTableColumn) {
    if (!MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) {
      return;
    }

    const sortColumn = this.currentSortColumn;
    const sortDirection = this.currentSortDirection;

    if (sortColumn && sortColumn.name === column.name) {
      this.selectedSortDirection = (sortDirection === MaisTableSortDirection.DESCENDING ?
        MaisTableSortDirection.ASCENDING : MaisTableSortDirection.DESCENDING);
    } else {
      this.selectedSortColumn = column;
      this.selectedSortDirection = MaisTableSortDirection.ASCENDING;
    }

    this.emitSortChanged();

    // forza ritorno alla prima pagina
    this.setPage(0);
    this.statusChanged();
    this.reload({reason: MaisTableReloadReason.USER});
  }

  // parsing functions

  getItemIdentifier(item: any) {
    if (this.itemTrackingEnabledAndPossible) {
      if ((this.itemIdentifier as any).extract) {
        return (this.itemIdentifier as IMaisTableIdentifierProvider).extract(item);
      } else {
        return MaisTableFormatterHelper.getPropertyValue(item, this.itemIdentifier as string);
      }
    } else {
      return null;
    }
  }

  getCurrentLocale(): string {
    return this.translateService.getDefaultLang();
  }

  extractValue(row: any, column: IMaisTableColumn) {
    return MaisTableFormatterHelper.extractValue(row, column, this.getCurrentLocale());
  }

  resolveLabel(column: IMaisTableColumn | IMaisTableAction | IMaisTableContextFilter) {
    return this.resolveLabelOrFixed(column.labelKey, column.label);
  }

  resolveLabelOrFixed(key: string | null | undefined, fixed:  string | null | undefined) {
    if (key) {
      return this.translateService.instant(key);
    } else if (fixed) {
      return fixed;
    } else {
      return null;
    }
  }

  isCurrentSortingColumn(column: IMaisTableColumn, direction = null) {
    const sortColumn = this.currentSortColumn;
    if (sortColumn && column && column.name === sortColumn.name) {
      if (!direction) {
        return true;
      }
      return direction === this.currentSortDirection;
    } else {
      return false;
    }
  }

  isDefaultVisibleColumn(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.isDefaultVisibleColumn(column, this.configurationService.getConfiguration());
  }

  isDefaultFilterColumn(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.isDefaultFilterColumn(column, this.configurationService.getConfiguration());
  }

  isExpandable(row: any): boolean {
    return this.currentEnableRowExpansion &&
      (!this.expandableStatusProvider || this.expandableStatusProvider(row, this.statusSnapshot));
  }

  isHideable(column: IMaisTableColumn): boolean | undefined {
    return MaisTableFormatterHelper.isHideable(column, this.configurationService.getConfiguration());
  }

  isSortable(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration());
  }

  isFilterable(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.isFilterable(column, this.configurationService.getConfiguration());
  }

  hasLabel(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.hasLabel(column, this.configurationService.getConfiguration());
  }

  isLabelVisibleInTable(column: IMaisTableColumn) {
    return MaisTableFormatterHelper.isLabelVisibleInTable(column, this.configurationService.getConfiguration());
  }

  isContextActionAllowed(action: IMaisTableAction): boolean | undefined {
    return this.isActionAllowed(action, this.configurationService.getConfiguration().actions?.defaultContextActionActivationCondition);
  }

  isHeaderActionAllowed(action: IMaisTableAction): boolean | undefined {
    return this.isActionAllowed(action, this.configurationService.getConfiguration().actions?.defaultHeaderActionActivationCondition);
  }

  isActionAllowed(action: IMaisTableAction, def: MaisTableActionActivationCondition | undefined) {
    const actCond = action.activationCondition || def;
    if (actCond === MaisTableActionActivationCondition.ALWAYS) {
      return true;
    } else if (actCond === MaisTableActionActivationCondition.NEVER) {
      return false;
    } else if (actCond === MaisTableActionActivationCondition.DYNAMIC) {
      // delega decisione a provider esterno
      if (!this.actionStatusProvider) {
        this.logger.error('Action with dynamic enabling but no actionStatusProvider provided');
        return false;
      }

      return this.actionStatusProvider(action, this.statusSnapshot || null);
    }

    const numSelected = this.checkedItems.length;
    if (actCond === MaisTableActionActivationCondition.SINGLE_SELECTION) {
      return numSelected === 1;
    } else if (actCond === MaisTableActionActivationCondition.MULTIPLE_SELECTION) {
      return numSelected > 0;
    } else if (actCond === MaisTableActionActivationCondition.NO_SELECTION) {
      return numSelected < 1;
    } else {
      throw new Error('Unknown action activation condition: ' + actCond);
    }
  }

  // gestione delle righe espanse

  isExpanded(item: any) {
    return this.expandedItems.indexOf(item) !== -1;
  }

  // checkbox select per items

  isChecked(item: any) {
    return this.checkedItems.indexOf(item) !== -1;
  }

  get allChecked(): boolean {
    const dataItems = this.currentData;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedItems.indexOf(item) === -1) {
        return false;
      }
    }
    return true;
  }

  get anyChecked(): boolean {
    const dataItems = this.currentData;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedItems.indexOf(item) !== -1) {
        return true;
      }
    }
    return false;
  }

  get noneChecked(): boolean {
    return !this.anyChecked;
  }

  toggleChecked(item: any) {
    if (this.isChecked(item)) {
      this.checkedItems.splice(this.checkedItems.indexOf(item), 1);
    } else {
      if (!this.currentEnableMultiSelect) {
        this.checkedItems = [];
      }
      this.checkedItems.push(item);
    }

    this.statusChanged();
    this.emitSelectionChanged();
  }

  toggleAllChecked() {
    if (!this.currentEnableSelectAll) {
      return;
    }
    if (!this.currentEnableMultiSelect) {
      return;
    }

    if (this.allChecked) {
      this.checkedItems = [];
    } else {
      this.checkedItems = [];
      for (const el of this.currentData) {
        this.checkedItems.push(el);
      }
    }

    this.statusChanged();
    this.emitSelectionChanged();
  }

  // checkbox select per filtering columns

  isColumnCheckedForFiltering(item: IMaisTableColumn) {
    return this.checkedColumnsForFiltering.indexOf(item) !== -1;
  }

  get allFilteringColumnsChecked(): boolean {
    const dataItems = this.filterableColumns;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedColumnsForFiltering.indexOf(item) === -1) {
        return false;
      }
    }
    return true;
  }

  get anyFilteringColumnsChecked(): boolean {
    const dataItems = this.filterableColumns;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedColumnsForFiltering.indexOf(item) !== -1) {
        return true;
      }
    }
    return false;
  }

  get noFilteringColumnshecked(): boolean {
    return !this.anyFilteringColumnsChecked;
  }

  toggleFilteringColumnChecked(item: IMaisTableColumn) {
    if (!this.currentEnableFiltering) {
      return;
    }

    if (!this.isFilterable(item)) {
      return;
    }

    if (this.isColumnCheckedForFiltering(item)) {
      this.checkedColumnsForFiltering.splice(this.checkedColumnsForFiltering.indexOf(item), 1);
    } else {
      this.checkedColumnsForFiltering.push(item);
    }

    if (this.selectedSearchQuery) {
      this.logger.trace('search query columns changed with active search query, reloading');
      this.applySelectedFilter();
    }

    this.statusChanged();
    this.emitFilteringColumnsSelectionChanged();
  }

  toggleAllFilteringColumnsChecked() {
    if (!this.currentEnableFiltering) {
      return;
    }

    if (this.allFilteringColumnsChecked) {
      this.checkedColumnsForFiltering = [];
    } else {
      this.checkedColumnsForFiltering = [];
      for (const el of this.filterableColumns) {
        this.checkedColumnsForFiltering.push(el);
      }
    }

    if (this.selectedSearchQuery) {
      this.logger.trace('search query columns changed with active search query, reloading');
      this.applySelectedFilter();
    }

    this.statusChanged();
    this.emitFilteringColumnsSelectionChanged();
  }

  // checkbox select per visible columns

  isColumnCheckedForVisualization(item: IMaisTableColumn) {
    return this.checkedColumnsForVisualization.indexOf(item) !== -1;
  }

  get allVisibleColumnsChecked(): boolean {
    const dataItems = this.hideableColumns;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedColumnsForVisualization.indexOf(item) === -1) {
        return false;
      }
    }
    return true;
  }

  get anyVisibleColumnsChecked(): boolean {
    const dataItems = this.hideableColumns;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedColumnsForVisualization.indexOf(item) !== -1) {
        return true;
      }
    }
    return false;
  }

  get noVisibleColumnshecked(): boolean {
    return !this.anyVisibleColumnsChecked;
  }

  toggleVisibleColumnChecked(item: IMaisTableColumn) {
    if (!MaisTableFormatterHelper.isHideable(item, this.configurationService.getConfiguration()) || !this.currentEnableColumnsSelection) {
      return;
    }

    if (this.isColumnCheckedForVisualization(item)) {
      this.checkedColumnsForVisualization.splice(this.checkedColumnsForVisualization.indexOf(item), 1);
    } else {
      this.checkedColumnsForVisualization.push(item);
    }

    this.statusChanged();
    this.emitVisibleColumnsSelectionChanged();
  }

  toggleAllVisibleColumnsChecked() {
    if (!this.currentEnableColumnsSelection) {
      return;
    }

    if (this.allVisibleColumnsChecked) {
      this.checkedColumnsForVisualization = this.columns.filter(c =>
        !MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
    } else {
      this.checkedColumnsForVisualization = [];
      for (const el of this.columns) {
        this.checkedColumnsForVisualization.push(el);
      }
    }

    this.statusChanged();
    this.emitVisibleColumnsSelectionChanged();
  }

  // checkbox select per filtri custom

  isContextFilterChecked(item: IMaisTableContextFilter) {
    return this.checkedContextFilters.indexOf(item) !== -1;
  }

  get allContextFilterChecked(): boolean {
    const dataItems = this.currentContextFilters;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedContextFilters.indexOf(item) === -1) {
        return false;
      }
    }
    return true;
  }

  get anyContextFilterChecked(): boolean {
    const dataItems = this.currentContextFilters;
    if (!dataItems.length) {
      return false;
    }
    for (const item of dataItems) {
      if (this.checkedContextFilters.indexOf(item) !== -1) {
        return true;
      }
    }
    return false;
  }

  get noContextFiltersChecked(): boolean {
    return !this.anyContextFilterChecked;
  }

  toggleContextFilterChecked(item: IMaisTableContextFilter) {
    if (!this.currentEnableContextFiltering) {
      return;
    }

    if (this.isContextFilterChecked(item)) {
      this.checkedContextFilters.splice(this.checkedContextFilters.indexOf(item), 1);
    } else {
      this.checkedContextFilters.push(item);
      if (item.group) {
        this.checkedContextFilters = this.checkedContextFilters.filter(o => {
          return (o.name === item.name) || (!o.group) || (o.group !== item.group);
        });
      }
    }

    this.emitContextFiltersSelectionChanged();

    this.setPage(0);
    this.statusChanged();
    this.reload({reason: MaisTableReloadReason.USER});
  }

  private buildStatusSnapshot(): IMaisTableStatusSnapshot {
    return {
      schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
      orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
      orderColumnDirection: this.currentSortDirection || null,
      query: this.currentSearchQuery,
      queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
      visibleColumns: this.visibleColumns.map(c => c.name),
      contextFilters: this.checkedContextFilters.map(c => c.name),
      currentPage: this.currentPageIndex,
      pageSize: this.currentPageSize,
      checkedItems: !this.currentEnableSelection ? [] : this.checkedItems.map(v => v),
      expandedItems:  !this.currentEnableRowExpansion ? [] : this.expandedItems.map(v => v)
    };
  }

  private buildPersistableStatusSnapshot(): IMaisTablePersistableStatusSnapshot {
    return {
      schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
      orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
      orderColumnDirection: this.currentSortDirection || null,
      query: this.currentSearchQuery,
      queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
      visibleColumns: this.visibleColumns.map(c => c.name),
      contextFilters: this.checkedContextFilters.map(c => c.name),
      currentPage: this.currentPageIndex,
      pageSize: this.currentPageSize,
      checkedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
        this.checkedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v),
      expandedItemIdentifiers:  !this.itemTrackingEnabledAndPossible ? [] :
        this.expandedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v)
    };
  }

  private statusChanged() {
    this.statusSnapshot = this.buildStatusSnapshot();
    this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    this.logger.trace('table status changed', this.persistableStatusSnapshot);

    this.emitStatusChanged();
  }

  private persistStatusToStore(): Observable<void> {
    if (!this.statusSnapshot) {
      return throwError('No status to save');
    }
    return new Observable( subscriber => {
      if (this.storePersistenceEnabledAndPossible) {
        this.logger.trace('passing status to persistence store');
        if (this.storeAdapter.save) {
          this.storeAdapter.save({
            status: this.persistableStatusSnapshot
          }).subscribe(result => {
            this.logger.trace('saved status snapshot to persistence store');
            subscriber.next();
            subscriber.complete();
          }, failure => {
            this.logger.warn('failed to save status snapshot to persistence store', failure);
            subscriber.error(failure);
            subscriber.complete();
          });
        } else {
          subscriber.error('No save function in store adapter');
          subscriber.complete();
        }
      }
      subscriber.next();
      subscriber.complete();
    });
  }

  private loadStatusFromStore(): Observable<IMaisTablePersistableStatusSnapshot | null> {
    return new Observable( subscriber => {
      if (this.storePersistenceEnabledAndPossible) {
        this.logger.trace('fetching status from persistence store');
        if (this.storeAdapter.load) {
          this.storeAdapter.load().subscribe(result => {
            this.logger.trace('fetched status snapshot from persistence store');
            subscriber.next(result);
            subscriber.complete();
          }, failure => {
            subscriber.error(failure);
            this.logger.warn('failed to fetch status snapshot from persistence store', failure);
            subscriber.complete();
          });
        } else {
          subscriber.error('No load function in store adapter');
          subscriber.complete();
        }
      }
      subscriber.next(null);
      subscriber.complete();
    });
  }

  get dragInProgress(): boolean {
    return (
      $('.cdk-drag-preview:visible').length +
      $('.cdk-drag-placeholder:visible').length +
      $('.cdk-drop-list-dragging:visible').length +
      $('.cdk-drop-list-receiving:visible').length
    ) > 0;
  }

  // event emitters

  private emitSelectionChanged() {
    this.selectionChange.emit(this.checkedItems);
  }

  private emitSortChanged() {
    this.sortChange.emit({
      column: this.currentSortColumn,
      direction: this.currentSortDirection
    });
  }

  private emitPageChanged() {
    this.pageChange.emit(this.selectedPageIndex);
  }

  private emitFilteringColumnsSelectionChanged() {
    this.filteringColumnsChange.emit(this.checkedColumnsForFiltering);
  }

  private emitVisibleColumnsSelectionChanged() {
    this.visibleColumnsChange.emit(this.checkedColumnsForVisualization);
  }

  private emitContextFiltersSelectionChanged() {
    this.contextFiltersChange.emit(this.checkedContextFilters);
  }

  private emitStatusChanged() {
    if (this.statusSnapshot) {
      this.statusChange.emit(this.statusSnapshot);
    }
  }

  // MatTable adapters
  get matTableData(): any[] {
    return this.dataSnapshot;
  }

  get matTableVisibleColumnDefs(): string[] {
    const o = [];
    if (this.rowExpansionEnabledAndPossible) {
      o.push('internal___col_row_expansion');
    }
    if (this.currentEnableSelection) {
      o.push('internal___col_row_selection');
    }
    if (this.contextFilteringEnabledAndPossible) {
      o.push('internal___col_context_filtering');
    }
    this.visibleColumns.map(c => c.name).forEach(c => o.push(c));
    return o;
  }

  get matTableVisibleColumns(): IMaisTableColumn[] {
    return this.visibleColumns;
  }

  handleMatTableDataSnapshotChanged() {
    const rows: any[] = [];

    this.dataSnapshot.forEach(element => {
      const detailRow = { ___detailRowContent: true, ___parentRow: element, ...element };
      element.___detailRow = detailRow;
      rows.push(element, detailRow);
    });

    // TODO : BUILD STATIC ROW WITH DATA EXTRACTORS!
    this.logger.trace('emitting static rows for mat-table ' + this.currentTableId, rows);

    setTimeout(() => {
      this.matTableDataObservable?.next(rows);
    });
  }

  isMatTableExpansionDetailRow = (i: number, row: any) => row.hasOwnProperty('___detailRowContent');

  isMatTableExpanded = (row: any, limit = false) => {
    if (!limit && row.___detailRow) {
      if (this.isMatTableExpanded(row.___detailRow, true)) {
        return true;
      }
    }
    if (!limit && row.___parentRow) {
      if (this.isMatTableExpanded(row.___parentRow, true)) {
        return true;
      }
    }
    if (!this.rowExpansionEnabledAndPossible) {
      return false;
    }
    if (!this.isExpandable(row)) {
      return false;
    }
    if (!this.isExpanded(row)) {
      return false;
    }
    return true;
  }

  get compatibilityModeForMainTable(): boolean {
    if (!this.isIE) {
      return false;
    } else if (this.currentEnableDrop) {
      return true;
    } else {
      return true;
    }
  }

  get compatibilityModeForDropDowns(): boolean {
    return true;

    if (!this.isIE) {
      return false;
    } else {
      return true;
    }
  }

}
