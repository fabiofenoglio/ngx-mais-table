import { MaisTableFormatterHelper } from './mais-table-formatter.utils';
export class MaisTableValidatorHelper {
    static validateColumnsSpecification(columns, config) {
        const err = MaisTableValidatorHelper._validateColumnsSpecification(columns, config);
        if (err) {
            throw new Error(err);
        }
        return null;
    }
    static _validateColumnsSpecification(columns, config) {
        if (!columns || !columns.length) {
            return 'A non-empty list of columns must be provided';
        }
        for (const col of columns) {
            if (!col.name) {
                return 'Columns need to have a non-empty name.';
            }
        }
        for (const col of columns) {
            if ((MaisTableFormatterHelper.isFilterable(col, config) || MaisTableFormatterHelper.isHideable(col, config))
                && !MaisTableFormatterHelper.hasLabel(col, config)) {
                return 'Columns to be filtered or toggled must have either a labelKey or a label. ' +
                    'To hide a column label use the showLabelInTable property. Offending column is [' + col.name + ']';
            }
        }
        if (columns.filter(c => !MaisTableFormatterHelper.isHideable(c, config)).length < 1) {
            return 'At least one of the columns must be not hideable: set canHide=false on primary or relevant column.';
        }
        return null;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS12YWxpZGF0b3IudXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLXZhbGlkYXRvci51dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV4RSxNQUFNLE9BQWdCLHdCQUF3QjtJQUVyQyxNQUFNLENBQUMsNEJBQTRCLENBQUMsT0FBMkIsRUFBRSxNQUErQjtRQUNyRyxNQUFNLEdBQUcsR0FBRyx3QkFBd0IsQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDcEYsSUFBSSxHQUFHLEVBQUU7WUFDUCxNQUFNLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRU8sTUFBTSxDQUFDLDZCQUE2QixDQUFDLE9BQTJCLEVBQUUsTUFBK0I7UUFDdkcsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsT0FBTyw4Q0FBOEMsQ0FBQztTQUN2RDtRQUVELEtBQUssTUFBTSxHQUFHLElBQUksT0FBTyxFQUFFO1lBQ3pCLElBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFHO2dCQUNmLE9BQU8sd0NBQXdDLENBQUM7YUFDakQ7U0FDRjtRQUVELEtBQUssTUFBTSxHQUFHLElBQUksT0FBTyxFQUFFO1lBQ3pCLElBQUssQ0FBQyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxJQUFJLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUU7bUJBQzNHLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsRUFBRztnQkFDbkQsT0FBTyw0RUFBNEU7b0JBQ2pGLGlGQUFpRixHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO2FBQ3RHO1NBQ0Y7UUFFRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25GLE9BQU8sb0dBQW9HLENBQUM7U0FDN0c7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNYWlzVGFibGVDb2x1bW4sIElNYWlzVGFibGVDb25maWd1cmF0aW9uIH0gZnJvbSAnLi4vbW9kZWwnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIgfSBmcm9tICcuL21haXMtdGFibGUtZm9ybWF0dGVyLnV0aWxzJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBNYWlzVGFibGVWYWxpZGF0b3JIZWxwZXIge1xyXG5cclxuICBwdWJsaWMgc3RhdGljIHZhbGlkYXRlQ29sdW1uc1NwZWNpZmljYXRpb24oY29sdW1uczogSU1haXNUYWJsZUNvbHVtbltdLCBjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKSB7XHJcbiAgICBjb25zdCBlcnIgPSBNYWlzVGFibGVWYWxpZGF0b3JIZWxwZXIuX3ZhbGlkYXRlQ29sdW1uc1NwZWNpZmljYXRpb24oY29sdW1ucywgY29uZmlnKTtcclxuICAgIGlmIChlcnIpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbnVsbDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIF92YWxpZGF0ZUNvbHVtbnNTcGVjaWZpY2F0aW9uKGNvbHVtbnM6IElNYWlzVGFibGVDb2x1bW5bXSwgY29uZmlnOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbikge1xyXG4gICAgaWYgKCFjb2x1bW5zIHx8ICFjb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gJ0Egbm9uLWVtcHR5IGxpc3Qgb2YgY29sdW1ucyBtdXN0IGJlIHByb3ZpZGVkJztcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGNvbnN0IGNvbCBvZiBjb2x1bW5zKSB7XHJcbiAgICAgIGlmICggIWNvbC5uYW1lICkge1xyXG4gICAgICAgIHJldHVybiAnQ29sdW1ucyBuZWVkIHRvIGhhdmUgYSBub24tZW1wdHkgbmFtZS4nO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChjb25zdCBjb2wgb2YgY29sdW1ucykge1xyXG4gICAgICBpZiAoIChNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNGaWx0ZXJhYmxlKGNvbCwgY29uZmlnKSB8fCBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjb2wsIGNvbmZpZykgKVxyXG4gICAgICAmJiAhTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmhhc0xhYmVsKGNvbCwgY29uZmlnKSApIHtcclxuICAgICAgICByZXR1cm4gJ0NvbHVtbnMgdG8gYmUgZmlsdGVyZWQgb3IgdG9nZ2xlZCBtdXN0IGhhdmUgZWl0aGVyIGEgbGFiZWxLZXkgb3IgYSBsYWJlbC4gJyArXHJcbiAgICAgICAgICAnVG8gaGlkZSBhIGNvbHVtbiBsYWJlbCB1c2UgdGhlIHNob3dMYWJlbEluVGFibGUgcHJvcGVydHkuIE9mZmVuZGluZyBjb2x1bW4gaXMgWycgKyBjb2wubmFtZSArICddJztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjb2x1bW5zLmZpbHRlcihjID0+ICFNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjLCBjb25maWcpKS5sZW5ndGggPCAxKSB7XHJcbiAgICAgIHJldHVybiAnQXQgbGVhc3Qgb25lIG9mIHRoZSBjb2x1bW5zIG11c3QgYmUgbm90IGhpZGVhYmxlOiBzZXQgY2FuSGlkZT1mYWxzZSBvbiBwcmltYXJ5IG9yIHJlbGV2YW50IGNvbHVtbi4nO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxufVxyXG4iXX0=