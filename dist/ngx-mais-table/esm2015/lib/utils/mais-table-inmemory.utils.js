import { MaisTableSortDirection } from '../model';
import { MaisTableFormatterHelper } from './mais-table-formatter.utils';
export class MaisTableInMemoryHelper {
    static fetchInMemory(data, request, sortColumn, filteringColumns, locale) {
        const output = {
            content: [],
            size: 0,
            totalElements: 0,
            totalPages: 0
        };
        if (!data || !data.length) {
            return output;
        }
        // apply filtering
        let filtered = data;
        if (request.query && request.query.length && request.query.trim().length
            && request.queryFields && request.queryFields.length) {
            const cleanedSearchQuery = request.query.trim().toLowerCase();
            filtered = filtered.filter(candidateRow => {
                let allowed = false;
                if (filteringColumns) {
                    for (const filteringColumn of filteringColumns) {
                        const extractedValue = MaisTableFormatterHelper.extractValue(candidateRow, filteringColumn, locale);
                        if (extractedValue) {
                            const extractedValueStr = extractedValue + '';
                            if (extractedValueStr.toLowerCase().indexOf(cleanedSearchQuery) !== -1) {
                                allowed = true;
                                break;
                            }
                        }
                    }
                }
                return allowed;
            });
        }
        // apply pagination
        let startingIndex;
        let endingIndex;
        if (request && request.page != null && request.size != null &&
            (typeof request.page !== 'undefined') && (typeof request.size !== 'undefined') && (request.page === 0 || request.page > 0)) {
            startingIndex = request.page * request.size;
            endingIndex = (request.page + 1) * request.size;
            if (startingIndex >= filtered.length) {
                return output;
            }
            if (endingIndex > filtered.length) {
                endingIndex = filtered.length;
            }
        }
        else {
            startingIndex = 0;
            endingIndex = filtered.length;
        }
        if (request.sort && request.sort.length && request.sort[0]) {
            const sortDirection = request.sort[0].direction === MaisTableSortDirection.DESCENDING ? -1 : +1;
            if (sortColumn) {
                filtered.sort((c1, c2) => {
                    const v1 = MaisTableFormatterHelper.extractValue(c1, sortColumn, locale) || '';
                    const v2 = MaisTableFormatterHelper.extractValue(c2, sortColumn, locale) || '';
                    const v1n = (v1 === null || v1 === undefined || Number.isNaN(v1));
                    const v2n = (v2 === null || v2 === undefined || Number.isNaN(v2));
                    if (v1n && v2n) {
                        return 0;
                    }
                    else if (v1n && !v2n) {
                        return -1 * sortDirection;
                    }
                    else if (!v1n && v2n) {
                        return 1 * sortDirection;
                    }
                    else {
                        return (v1 > v2 ? 1 : v1 < v2 ? -1 : 0) * sortDirection;
                    }
                });
            }
        }
        const fetchedPage = filtered.slice(startingIndex, endingIndex);
        output.content = fetchedPage;
        output.size = fetchedPage.length;
        output.totalElements = filtered.length;
        if (request.size) {
            output.totalPages = Math.ceil(filtered.length);
        }
        else {
            output.totalPages = 1;
        }
        return output;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1pbm1lbW9yeS51dGlscy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYWlzLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL21haXMtdGFibGUtaW5tZW1vcnkudXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFtRSxzQkFBc0IsRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUNuSCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV4RSxNQUFNLE9BQWdCLHVCQUF1QjtJQUVwQyxNQUFNLENBQUMsYUFBYSxDQUN6QixJQUFXLEVBQ1gsT0FBOEIsRUFDOUIsVUFBb0MsRUFDcEMsZ0JBQXFDLEVBQ3JDLE1BQWU7UUFDZixNQUFNLE1BQU0sR0FBMkI7WUFDckMsT0FBTyxFQUFFLEVBQUU7WUFDWCxJQUFJLEVBQUUsQ0FBQztZQUNQLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFVBQVUsRUFBRSxDQUFDO1NBQ2QsQ0FBQztRQUVGLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3pCLE9BQU8sTUFBTSxDQUFDO1NBQ2Y7UUFFRCxrQkFBa0I7UUFDbEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLE1BQU07ZUFDbkUsT0FBTyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUV0RCxNQUFNLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUQsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3hDLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDcEIsSUFBSSxnQkFBZ0IsRUFBRTtvQkFDcEIsS0FBSyxNQUFNLGVBQWUsSUFBSSxnQkFBZ0IsRUFBRTt3QkFDOUMsTUFBTSxjQUFjLEdBQUcsd0JBQXdCLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ3BHLElBQUksY0FBYyxFQUFFOzRCQUNsQixNQUFNLGlCQUFpQixHQUFHLGNBQWMsR0FBRyxFQUFFLENBQUM7NEJBQzlDLElBQUksaUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0NBQ3RFLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0NBQ2YsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtpQkFDRjtnQkFDRCxPQUFPLE9BQU8sQ0FBQztZQUNqQixDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsbUJBQW1CO1FBQ25CLElBQUksYUFBYSxDQUFDO1FBQ2xCLElBQUksV0FBVyxDQUFDO1FBRWhCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSTtZQUN6RCxDQUFDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDNUgsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUM1QyxXQUFXLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDaEQsSUFBSSxhQUFhLElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRztnQkFDckMsT0FBTyxNQUFNLENBQUM7YUFDZjtZQUVELElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUc7Z0JBQ2xDLFdBQVcsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO2FBQy9CO1NBQ0Y7YUFBTTtZQUNMLGFBQWEsR0FBRyxDQUFDLENBQUM7WUFDbEIsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7U0FDL0I7UUFFRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUMxRCxNQUFNLGFBQWEsR0FBVyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV4RyxJQUFJLFVBQVUsRUFBRTtnQkFDZCxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFO29CQUN2QixNQUFNLEVBQUUsR0FBRyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQy9FLE1BQU0sRUFBRSxHQUFHLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDL0UsTUFBTSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEtBQUssSUFBSSxJQUFJLEVBQUUsS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNsRSxNQUFNLEdBQUcsR0FBRyxDQUFDLEVBQUUsS0FBSyxJQUFJLElBQUksRUFBRSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2xFLElBQUksR0FBRyxJQUFJLEdBQUcsRUFBRTt3QkFDZCxPQUFPLENBQUMsQ0FBQztxQkFDVjt5QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDdEIsT0FBTyxDQUFDLENBQUMsR0FBRyxhQUFhLENBQUM7cUJBQzNCO3lCQUFNLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxFQUFFO3dCQUN0QixPQUFPLENBQUMsR0FBRyxhQUFhLENBQUM7cUJBQzFCO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBSSxhQUFhLENBQUM7cUJBQzFEO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjtRQUVELE1BQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9ELE1BQU0sQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztRQUNqQyxNQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDdkMsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDaEQ7YUFBTTtZQUNMLE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU1haXNUYWJsZUNvbHVtbiwgSU1haXNUYWJsZVBhZ2VSZXNwb25zZSwgSU1haXNUYWJsZVBhZ2VSZXF1ZXN0LCBNYWlzVGFibGVTb3J0RGlyZWN0aW9uIH0gZnJvbSAnLi4vbW9kZWwnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIgfSBmcm9tICcuL21haXMtdGFibGUtZm9ybWF0dGVyLnV0aWxzJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBNYWlzVGFibGVJbk1lbW9yeUhlbHBlciB7XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgZmV0Y2hJbk1lbW9yeShcclxuICAgIGRhdGE6IGFueVtdLFxyXG4gICAgcmVxdWVzdDogSU1haXNUYWJsZVBhZ2VSZXF1ZXN0LFxyXG4gICAgc29ydENvbHVtbj86IElNYWlzVGFibGVDb2x1bW4gfCBudWxsLFxyXG4gICAgZmlsdGVyaW5nQ29sdW1ucz86IElNYWlzVGFibGVDb2x1bW5bXSxcclxuICAgIGxvY2FsZT86IHN0cmluZyApOiBJTWFpc1RhYmxlUGFnZVJlc3BvbnNlIHtcclxuICAgIGNvbnN0IG91dHB1dDogSU1haXNUYWJsZVBhZ2VSZXNwb25zZSA9IHtcclxuICAgICAgY29udGVudDogW10sXHJcbiAgICAgIHNpemU6IDAsXHJcbiAgICAgIHRvdGFsRWxlbWVudHM6IDAsXHJcbiAgICAgIHRvdGFsUGFnZXM6IDBcclxuICAgIH07XHJcblxyXG4gICAgaWYgKCFkYXRhIHx8ICFkYXRhLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gb3V0cHV0O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGFwcGx5IGZpbHRlcmluZ1xyXG4gICAgbGV0IGZpbHRlcmVkID0gZGF0YTtcclxuICAgIGlmIChyZXF1ZXN0LnF1ZXJ5ICYmIHJlcXVlc3QucXVlcnkubGVuZ3RoICYmIHJlcXVlc3QucXVlcnkudHJpbSgpLmxlbmd0aFxyXG4gICAgICAmJiByZXF1ZXN0LnF1ZXJ5RmllbGRzICYmIHJlcXVlc3QucXVlcnlGaWVsZHMubGVuZ3RoKSB7XHJcblxyXG4gICAgICBjb25zdCBjbGVhbmVkU2VhcmNoUXVlcnkgPSByZXF1ZXN0LnF1ZXJ5LnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICBmaWx0ZXJlZCA9IGZpbHRlcmVkLmZpbHRlcihjYW5kaWRhdGVSb3cgPT4ge1xyXG4gICAgICAgIGxldCBhbGxvd2VkID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKGZpbHRlcmluZ0NvbHVtbnMpIHtcclxuICAgICAgICAgIGZvciAoY29uc3QgZmlsdGVyaW5nQ29sdW1uIG9mIGZpbHRlcmluZ0NvbHVtbnMpIHtcclxuICAgICAgICAgICAgY29uc3QgZXh0cmFjdGVkVmFsdWUgPSBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuZXh0cmFjdFZhbHVlKGNhbmRpZGF0ZVJvdywgZmlsdGVyaW5nQ29sdW1uLCBsb2NhbGUpO1xyXG4gICAgICAgICAgICBpZiAoZXh0cmFjdGVkVmFsdWUpIHtcclxuICAgICAgICAgICAgICBjb25zdCBleHRyYWN0ZWRWYWx1ZVN0ciA9IGV4dHJhY3RlZFZhbHVlICsgJyc7XHJcbiAgICAgICAgICAgICAgaWYgKGV4dHJhY3RlZFZhbHVlU3RyLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihjbGVhbmVkU2VhcmNoUXVlcnkpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgYWxsb3dlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFsbG93ZWQ7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGFwcGx5IHBhZ2luYXRpb25cclxuICAgIGxldCBzdGFydGluZ0luZGV4O1xyXG4gICAgbGV0IGVuZGluZ0luZGV4O1xyXG5cclxuICAgIGlmIChyZXF1ZXN0ICYmIHJlcXVlc3QucGFnZSAhPSBudWxsICYmIHJlcXVlc3Quc2l6ZSAhPSBudWxsICYmXHJcbiAgICAgICh0eXBlb2YgcmVxdWVzdC5wYWdlICE9PSAndW5kZWZpbmVkJykgJiYgKHR5cGVvZiByZXF1ZXN0LnNpemUgIT09ICd1bmRlZmluZWQnKSAmJiAocmVxdWVzdC5wYWdlID09PSAwIHx8IHJlcXVlc3QucGFnZSA+IDApKSB7XHJcbiAgICAgIHN0YXJ0aW5nSW5kZXggPSByZXF1ZXN0LnBhZ2UgKiByZXF1ZXN0LnNpemU7XHJcbiAgICAgIGVuZGluZ0luZGV4ID0gKHJlcXVlc3QucGFnZSArIDEpICogcmVxdWVzdC5zaXplO1xyXG4gICAgICBpZiAoc3RhcnRpbmdJbmRleCA+PSBmaWx0ZXJlZC5sZW5ndGggKSB7XHJcbiAgICAgICAgcmV0dXJuIG91dHB1dDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGVuZGluZ0luZGV4ID4gZmlsdGVyZWQubGVuZ3RoICkge1xyXG4gICAgICAgIGVuZGluZ0luZGV4ID0gZmlsdGVyZWQubGVuZ3RoO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzdGFydGluZ0luZGV4ID0gMDtcclxuICAgICAgZW5kaW5nSW5kZXggPSBmaWx0ZXJlZC5sZW5ndGg7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHJlcXVlc3Quc29ydCAmJiByZXF1ZXN0LnNvcnQubGVuZ3RoICYmIHJlcXVlc3Quc29ydFswXSkge1xyXG4gICAgICBjb25zdCBzb3J0RGlyZWN0aW9uOiBudW1iZXIgPSByZXF1ZXN0LnNvcnRbMF0uZGlyZWN0aW9uID09PSBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLkRFU0NFTkRJTkcgPyAtMSA6ICsxO1xyXG5cclxuICAgICAgaWYgKHNvcnRDb2x1bW4pIHtcclxuICAgICAgICBmaWx0ZXJlZC5zb3J0KChjMSwgYzIpID0+IHtcclxuICAgICAgICAgIGNvbnN0IHYxID0gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmV4dHJhY3RWYWx1ZShjMSwgc29ydENvbHVtbiwgbG9jYWxlKSB8fCAnJztcclxuICAgICAgICAgIGNvbnN0IHYyID0gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmV4dHJhY3RWYWx1ZShjMiwgc29ydENvbHVtbiwgbG9jYWxlKSB8fCAnJztcclxuICAgICAgICAgIGNvbnN0IHYxbiA9ICh2MSA9PT0gbnVsbCB8fCB2MSA9PT0gdW5kZWZpbmVkIHx8IE51bWJlci5pc05hTih2MSkpO1xyXG4gICAgICAgICAgY29uc3QgdjJuID0gKHYyID09PSBudWxsIHx8IHYyID09PSB1bmRlZmluZWQgfHwgTnVtYmVyLmlzTmFOKHYyKSk7XHJcbiAgICAgICAgICBpZiAodjFuICYmIHYybikge1xyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICAgIH0gZWxzZSBpZiAodjFuICYmICF2Mm4pIHtcclxuICAgICAgICAgICAgcmV0dXJuIC0xICogc29ydERpcmVjdGlvbjtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoIXYxbiAmJiB2Mm4pIHtcclxuICAgICAgICAgICAgcmV0dXJuIDEgKiBzb3J0RGlyZWN0aW9uO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICh2MSA+IHYyID8gMSA6IHYxIDwgdjIgPyAtMSA6IDApICAqIHNvcnREaXJlY3Rpb247XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBmZXRjaGVkUGFnZSA9IGZpbHRlcmVkLnNsaWNlKHN0YXJ0aW5nSW5kZXgsIGVuZGluZ0luZGV4KTtcclxuICAgIG91dHB1dC5jb250ZW50ID0gZmV0Y2hlZFBhZ2U7XHJcbiAgICBvdXRwdXQuc2l6ZSA9IGZldGNoZWRQYWdlLmxlbmd0aDtcclxuICAgIG91dHB1dC50b3RhbEVsZW1lbnRzID0gZmlsdGVyZWQubGVuZ3RoO1xyXG4gICAgaWYgKHJlcXVlc3Quc2l6ZSkge1xyXG4gICAgICBvdXRwdXQudG90YWxQYWdlcyA9IE1hdGguY2VpbChmaWx0ZXJlZC5sZW5ndGgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgb3V0cHV0LnRvdGFsUGFnZXMgPSAxO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG91dHB1dDtcclxuICB9XHJcbn1cclxuIl19