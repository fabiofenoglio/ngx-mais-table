export class MaisTableEmbeddedLogger {
    trace(message, ...additional) {
        console.log('[trace] ' + message, ...additional);
    }
    debug(message, ...additional) {
        console.log('[trace] ' + message, ...additional);
    }
    info(message, ...additional) {
        console.log('[INFO] ' + message, ...additional);
    }
    log(message, ...additional) {
        console.log('[INFO] ' + message, ...additional);
    }
    warn(message, ...additional) {
        console.warn('[WARN] ' + message, ...additional);
    }
    error(message, ...additional) {
        console.error('[ERROR] ' + message, ...additional);
    }
    fatal(message, ...additional) {
        console.error('[FATAL] ' + message, ...additional);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1lbWJlZGRlZC1sb2dnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLWVtYmVkZGVkLWxvZ2dlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8sdUJBQXVCO0lBRWxDLEtBQUssQ0FBQyxPQUFZLEVBQUUsR0FBRyxVQUFpQjtRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxPQUFPLEVBQUUsR0FBRyxVQUFVLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsS0FBSyxDQUFDLE9BQVksRUFBRSxHQUFHLFVBQWlCO1FBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLE9BQU8sRUFBRSxHQUFHLFVBQVUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxJQUFJLENBQUMsT0FBWSxFQUFFLEdBQUcsVUFBaUI7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsT0FBTyxFQUFFLEdBQUcsVUFBVSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELEdBQUcsQ0FBQyxPQUFZLEVBQUUsR0FBRyxVQUFpQjtRQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxPQUFPLEVBQUUsR0FBRyxVQUFVLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQsSUFBSSxDQUFDLE9BQVksRUFBRSxHQUFHLFVBQWlCO1FBQ3JDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sRUFBRSxHQUFHLFVBQVUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxLQUFLLENBQUMsT0FBWSxFQUFFLEdBQUcsVUFBaUI7UUFDdEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsT0FBTyxFQUFFLEdBQUcsVUFBVSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELEtBQUssQ0FBQyxPQUFZLEVBQUUsR0FBRyxVQUFpQjtRQUN0QyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxPQUFPLEVBQUUsR0FBRyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTWFpc1RhYmxlTG9nQWRhcHRlciB9IGZyb20gJy4vbWFpcy10YWJsZS1sb2ctYWRhcHRlcic7XHJcblxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlRW1iZWRkZWRMb2dnZXIgaW1wbGVtZW50cyBJTWFpc1RhYmxlTG9nQWRhcHRlciB7XHJcblxyXG4gIHRyYWNlKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdbdHJhY2VdICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGRlYnVnKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdbdHJhY2VdICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGluZm8obWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ1tJTkZPXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG5cclxuICBsb2cobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ1tJTkZPXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG5cclxuICB3YXJuKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUud2FybignW1dBUk5dICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGVycm9yKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoJ1tFUlJPUl0gJyArIG1lc3NhZ2UsIC4uLmFkZGl0aW9uYWwpO1xyXG4gIH1cclxuXHJcbiAgZmF0YWwobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5lcnJvcignW0ZBVEFMXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG59XHJcblxyXG4iXX0=