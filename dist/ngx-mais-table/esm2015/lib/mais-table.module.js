import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTableModule } from '@angular/material/table';
import { MaisTableComponent } from './mais-table.component';
import * as i0 from "@angular/core";
export class MaisTableModule {
}
/** @nocollapse */ MaisTableModule.ɵmod = i0.ɵɵdefineNgModule({ type: MaisTableModule });
/** @nocollapse */ MaisTableModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MaisTableModule_Factory(t) { return new (t || MaisTableModule)(); }, providers: [], imports: [[
            CommonModule,
            FontAwesomeModule,
            NgbModule,
            TranslateModule,
            CdkTableModule,
            MatTableModule,
            DragDropModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MaisTableModule, { declarations: [MaisTableComponent], imports: [CommonModule,
        FontAwesomeModule,
        NgbModule,
        TranslateModule,
        CdkTableModule,
        MatTableModule,
        DragDropModule], exports: [MaisTableComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    MaisTableComponent,
                ],
                imports: [
                    CommonModule,
                    FontAwesomeModule,
                    NgbModule,
                    TranslateModule,
                    CdkTableModule,
                    MatTableModule,
                    DragDropModule,
                ],
                exports: [
                    MaisTableComponent
                ],
                providers: []
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9tYWlzLXRhYmxlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOztBQXFCNUQsTUFBTSxPQUFPLGVBQWU7O21EQUFmLGVBQWU7NkdBQWYsZUFBZSxtQkFIZixFQUNWLFlBYlE7WUFDUCxZQUFZO1lBQ1osaUJBQWlCO1lBQ2pCLFNBQVM7WUFDVCxlQUFlO1lBQ2YsY0FBYztZQUNkLGNBQWM7WUFDZCxjQUFjO1NBQ2Y7d0ZBT1UsZUFBZSxtQkFqQnhCLGtCQUFrQixhQUdsQixZQUFZO1FBQ1osaUJBQWlCO1FBQ2pCLFNBQVM7UUFDVCxlQUFlO1FBQ2YsY0FBYztRQUNkLGNBQWM7UUFDZCxjQUFjLGFBR2Qsa0JBQWtCO2tEQUtULGVBQWU7Y0FuQjNCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1osa0JBQWtCO2lCQUNuQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixpQkFBaUI7b0JBQ2pCLFNBQVM7b0JBQ1QsZUFBZTtvQkFDZixjQUFjO29CQUNkLGNBQWM7b0JBQ2QsY0FBYztpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1Asa0JBQWtCO2lCQUNuQjtnQkFDRCxTQUFTLEVBQUUsRUFDVjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRm9udEF3ZXNvbWVNb2R1bGUgfSBmcm9tICdAZm9ydGF3ZXNvbWUvYW5ndWxhci1mb250YXdlc29tZSc7XHJcbmltcG9ydCB7IE5nYk1vZHVsZSB9IGZyb20gJ0BuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IERyYWdEcm9wTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XHJcbmltcG9ydCB7IENka1RhYmxlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3RhYmxlJztcclxuaW1wb3J0IHsgTWF0VGFibGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IE1haXNUYWJsZUNvbXBvbmVudCB9IGZyb20gJy4vbWFpcy10YWJsZS5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIE1haXNUYWJsZUNvbXBvbmVudCxcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvbnRBd2Vzb21lTW9kdWxlLFxyXG4gICAgTmdiTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgQ2RrVGFibGVNb2R1bGUsXHJcbiAgICBNYXRUYWJsZU1vZHVsZSxcclxuICAgIERyYWdEcm9wTW9kdWxlLFxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgTWFpc1RhYmxlQ29tcG9uZW50XHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYWlzVGFibGVNb2R1bGUgeyB9XHJcbiJdfQ==