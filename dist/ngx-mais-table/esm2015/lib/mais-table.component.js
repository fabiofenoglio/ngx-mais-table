import { Component, Input, Output, EventEmitter, ContentChild, TemplateRef, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { MaisTableSortDirection, MaisTablePaginationMethod, MaisTableActionActivationCondition, MaisTableRefreshStrategy, MaisTableReloadReason } from './model';
import { MaisTableFormatterHelper, MaisTableInMemoryHelper, MaisTableValidatorHelper, MaisTableBrowserHelper, MaisTableLogger } from './utils';
import { MaisTableRegistryService, MaisTableService } from './services';
import { Observable, Subject, throwError, timer } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import $ from 'jquery';
import { MatTable } from '@angular/material/table';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "./services";
import * as i3 from "@angular/common";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "@fortawesome/angular-fontawesome";
import * as i6 from "@angular/cdk/drag-drop";
import * as i7 from "@angular/material/table";
const _c0 = ["cellTemplate"];
const _c1 = ["actionsCaptionTemplate"];
const _c2 = ["rowDetailTemplate"];
const _c3 = ["dragTemplate"];
const _c4 = ["dropTemplate"];
const _c5 = ["#renderedMatTable"];
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r20 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template_input_change_0_listener($event) { i0.ɵɵrestoreView(_r20); const column_r14 = i0.ɵɵnextContext(2).$implicit; const ctx_r18 = i0.ɵɵnextContext(4); ctx_r18.toggleFilteringColumnChecked(column_r14); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r14 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r16 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("checked", ctx_r16.isColumnCheckedForFiltering(column_r14));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 31);
} if (rf & 2) {
    i0.ɵɵproperty("checked", false)("disabled", true);
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 20);
    i0.ɵɵelementStart(1, "div", 22);
    i0.ɵɵelementStart(2, "div", 23);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template, 1, 1, "input", 29);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template, 1, 2, "input", 30);
    i0.ɵɵelementStart(5, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template_span_click_5_listener($event) { i0.ɵɵrestoreView(_r24); const column_r14 = i0.ɵɵnextContext().$implicit; const ctx_r22 = i0.ɵɵnextContext(4); ctx_r22.toggleFilteringColumnChecked(column_r14); return $event.stopPropagation(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r14 = i0.ɵɵnextContext().$implicit;
    const ctx_r15 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r15.isFilterable(column_r14));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r15.isFilterable(column_r14));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r15.isFilterable(column_r14));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r15.resolveLabel(column_r14), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template, 7, 5, "button", 28);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r14 = ctx.$implicit;
    const ctx_r13 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r13.hasLabel(column_r14) && (ctx_r13.isFilterable(column_r14) || ctx_r13.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    const _r27 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 15);
    i0.ɵɵelementStart(2, "button", 16);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_button_click_2_listener() { i0.ɵɵrestoreView(_r27); const ctx_r26 = i0.ɵɵnextContext(3); return ctx_r26.applySelectedFilter(); });
    i0.ɵɵelement(3, "i", 17);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "button", 18);
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "div", 20);
    i0.ɵɵelementStart(7, "h6", 21);
    i0.ɵɵtext(8);
    i0.ɵɵpipe(9, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "button", 20);
    i0.ɵɵelementStart(11, "div", 22);
    i0.ɵɵelementStart(12, "div", 23);
    i0.ɵɵelementStart(13, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_Template_input_change_13_listener($event) { i0.ɵɵrestoreView(_r27); const ctx_r28 = i0.ɵɵnextContext(3); ctx_r28.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(14, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_span_click_14_listener($event) { i0.ɵɵrestoreView(_r27); const ctx_r29 = i0.ɵɵnextContext(3); ctx_r29.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵtext(15);
    i0.ɵɵpipe(16, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(17, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(18, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r11 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("btn-black", !ctx_r11.searchQueryNeedApply || !ctx_r11.searchQueryActive)("btn-primary", ctx_r11.searchQueryNeedApply && ctx_r11.searchQueryActive)("border-primary", ctx_r11.searchQueryActive && !ctx_r11.searchQueryMalformed)("border-warning", ctx_r11.searchQueryMalformed);
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("border-primary", ctx_r11.searchQueryActive && !ctx_r11.searchQueryMalformed)("border-warning", ctx_r11.searchQueryMalformed)("btn-black", !(ctx_r11.selectedSearchQuery && ctx_r11.noFilteringColumnshecked))("btn-warning", ctx_r11.selectedSearchQuery && ctx_r11.noFilteringColumnshecked);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(9, 21, "table.common.messages.pick_filtering_columns"));
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("checked", ctx_r11.allFilteringColumnsChecked);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(16, 23, "table.common.messages.pick_all_filtering_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r11.columns);
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r40 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r40); const column_r33 = i0.ɵɵnextContext().$implicit; const ctx_r38 = i0.ɵɵnextContext(4); return ctx_r38.toggleFilteringColumnChecked(column_r33); });
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r33 = i0.ɵɵnextContext().$implicit;
    const ctx_r34 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r34.isFilterable(column_r33));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r34.isFilterable(column_r33) && ctx_r34.isColumnCheckedForFiltering(column_r33));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r34.isFilterable(column_r33) && !ctx_r34.isColumnCheckedForFiltering(column_r33));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r34.isFilterable(column_r33));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r34.resolveLabel(column_r33), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template, 5, 6, "button", 35);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r33 = ctx.$implicit;
    const ctx_r32 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r32.hasLabel(column_r33) && (ctx_r32.isFilterable(column_r33) || ctx_r32.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_9_Template(rf, ctx) { if (rf & 1) {
    const _r43 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 15);
    i0.ɵɵelementStart(2, "button", 16);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_2_listener() { i0.ɵɵrestoreView(_r43); const ctx_r42 = i0.ɵɵnextContext(3); return ctx_r42.applySelectedFilter(); });
    i0.ɵɵelement(3, "i", 17);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "button", 18);
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "div", 20);
    i0.ɵɵelementStart(7, "h6", 21);
    i0.ɵɵtext(8);
    i0.ɵɵpipe(9, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_10_listener() { i0.ɵɵrestoreView(_r43); const ctx_r44 = i0.ɵɵnextContext(3); return ctx_r44.toggleAllFilteringColumnsChecked(); });
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(12, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(13);
    i0.ɵɵpipe(14, "translate");
    i0.ɵɵelement(15, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(16, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r12 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("btn-black", !ctx_r12.searchQueryNeedApply || !ctx_r12.searchQueryActive)("btn-primary", ctx_r12.searchQueryNeedApply && ctx_r12.searchQueryActive)("border-primary", ctx_r12.searchQueryActive && !ctx_r12.searchQueryMalformed)("border-warning", ctx_r12.searchQueryMalformed);
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("border-primary", ctx_r12.searchQueryActive && !ctx_r12.searchQueryMalformed)("border-warning", ctx_r12.searchQueryMalformed)("btn-black", !(ctx_r12.selectedSearchQuery && ctx_r12.noFilteringColumnshecked))("btn-warning", ctx_r12.selectedSearchQuery && ctx_r12.noFilteringColumnshecked);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(9, 22, "table.common.messages.pick_filtering_columns"));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r12.allFilteringColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r12.allFilteringColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(14, 24, "table.common.messages.pick_all_filtering_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r12.columns);
} }
function MaisTableComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r46 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "form", 12);
    i0.ɵɵlistener("submit", function MaisTableComponent_div_1_div_2_Template_form_submit_1_listener() { i0.ɵɵrestoreView(_r46); const ctx_r45 = i0.ɵɵnextContext(2); return ctx_r45.applySelectedFilter(); });
    i0.ɵɵelementStart(2, "div");
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 13);
    i0.ɵɵelementStart(6, "input", 14);
    i0.ɵɵlistener("ngModelChange", function MaisTableComponent_div_1_div_2_Template_input_ngModelChange_6_listener($event) { i0.ɵɵrestoreView(_r46); const ctx_r47 = i0.ɵɵnextContext(2); return ctx_r47.selectedSearchQuery = $event; })("focusout", function MaisTableComponent_div_1_div_2_Template_input_focusout_6_listener() { i0.ɵɵrestoreView(_r46); const ctx_r48 = i0.ɵɵnextContext(2); return ctx_r48.searchQueryFocusOut(); });
    i0.ɵɵpipe(7, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_div_1_div_2_ng_container_8_Template, 19, 25, "ng-container", 2);
    i0.ɵɵtemplate(9, MaisTableComponent_div_1_div_2_ng_container_9_Template, 17, 26, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(4, 9, "table.common.messages.filter_prompt"));
    i0.ɵɵadvance(3);
    i0.ɵɵclassProp("border-primary", ctx_r4.searchQueryActive && !ctx_r4.searchQueryMalformed)("border-warning", ctx_r4.searchQueryMalformed);
    i0.ɵɵpropertyInterpolate("placeholder", i0.ɵɵpipeBind1(7, 11, "table.common.messages.filter_placeholder"));
    i0.ɵɵproperty("ngModel", ctx_r4.selectedSearchQuery);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r4.compatibilityModeForDropDowns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r58 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template_input_change_0_listener($event) { i0.ɵɵrestoreView(_r58); const column_r52 = i0.ɵɵnextContext(2).$implicit; const ctx_r56 = i0.ɵɵnextContext(4); ctx_r56.toggleVisibleColumnChecked(column_r52); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r52 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r54 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("checked", ctx_r54.isColumnCheckedForVisualization(column_r52));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 31);
} if (rf & 2) {
    i0.ɵɵproperty("checked", true)("disabled", true);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r62 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 20);
    i0.ɵɵelementStart(1, "div", 22);
    i0.ɵɵelementStart(2, "div", 23);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template, 1, 1, "input", 29);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template, 1, 2, "input", 30);
    i0.ɵɵelementStart(5, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template_span_click_5_listener($event) { i0.ɵɵrestoreView(_r62); const column_r52 = i0.ɵɵnextContext().$implicit; const ctx_r60 = i0.ɵɵnextContext(4); ctx_r60.toggleVisibleColumnChecked(column_r52); return $event.stopPropagation(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r52 = i0.ɵɵnextContext().$implicit;
    const ctx_r53 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r53.isHideable(column_r52));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r53.isHideable(column_r52));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r53.isHideable(column_r52));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r53.resolveLabel(column_r52), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template, 7, 5, "button", 28);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r52 = ctx.$implicit;
    const ctx_r51 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r51.hasLabel(column_r52) && (ctx_r51.isHideable(column_r52) || ctx_r51.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    const _r65 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "button", 38);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "h6", 39);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "button", 20);
    i0.ɵɵelementStart(10, "div", 22);
    i0.ɵɵelementStart(11, "div", 23);
    i0.ɵɵelementStart(12, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_Template_input_change_12_listener($event) { i0.ɵɵrestoreView(_r65); const ctx_r64 = i0.ɵɵnextContext(3); ctx_r64.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_Template_span_click_13_listener($event) { i0.ɵɵrestoreView(_r65); const ctx_r66 = i0.ɵɵnextContext(3); ctx_r66.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵtext(14);
    i0.ɵɵpipe(15, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(16, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(17, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r49 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 6, "table.common.messages.add_remove_columns"), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(8, 8, "table.common.messages.select_columns_prompt"), " ");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("checked", ctx_r49.allVisibleColumnsChecked);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(15, 10, "table.common.messages.pick_all_visible_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r49.columns);
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r77 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r77); const column_r70 = i0.ɵɵnextContext().$implicit; const ctx_r75 = i0.ɵɵnextContext(4); return ctx_r75.toggleVisibleColumnChecked(column_r70); });
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r70 = i0.ɵɵnextContext().$implicit;
    const ctx_r71 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r71.isHideable(column_r70));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r71.isHideable(column_r70) && ctx_r71.isColumnCheckedForVisualization(column_r70));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r71.isHideable(column_r70) && !ctx_r71.isColumnCheckedForVisualization(column_r70));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r71.isHideable(column_r70));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r71.resolveLabel(column_r70), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template, 5, 6, "button", 35);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r70 = ctx.$implicit;
    const ctx_r69 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r69.hasLabel(column_r70) && (ctx_r69.isHideable(column_r70) || ctx_r69.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    const _r80 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "button", 38);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "h6", 39);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_Template_button_click_9_listener() { i0.ɵɵrestoreView(_r80); const ctx_r79 = i0.ɵɵnextContext(3); return ctx_r79.toggleAllVisibleColumnsChecked(); });
    i0.ɵɵtemplate(10, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(12);
    i0.ɵɵpipe(13, "translate");
    i0.ɵɵelement(14, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(15, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r50 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 7, "table.common.messages.add_remove_columns"), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(8, 9, "table.common.messages.select_columns_prompt"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r50.allVisibleColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r50.allVisibleColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(13, 11, "table.common.messages.pick_all_visible_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r50.columns);
} }
function MaisTableComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 36);
    i0.ɵɵelementStart(1, "div");
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_Template, 18, 12, "ng-container", 2);
    i0.ɵɵtemplate(5, MaisTableComponent_div_1_div_3_ng_container_5_Template, 16, 13, "ng-container", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 3, "table.common.messages.customize_view_prompt"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r5.compatibilityModeForDropDowns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r5.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 36);
} }
function MaisTableComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 11);
} }
function MaisTableComponent_div_1_ng_template_8_Template(rf, ctx) { }
function MaisTableComponent_div_1_ng_container_10_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r84 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 41);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_ng_container_10_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r84); const action_r82 = ctx.$implicit; const ctx_r83 = i0.ɵɵnextContext(3); return ctx_r83.clickOnContextAction(action_r82); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const action_r82 = ctx.$implicit;
    const ctx_r81 = i0.ɵɵnextContext(3);
    i0.ɵɵclassMapInterpolate2("btn btn-", action_r82.displayClass || "light", " ", action_r82.additionalClasses || "", " mr-1");
    i0.ɵɵproperty("disabled", !ctx_r81.isContextActionAllowed(action_r82));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r81.resolveLabel(action_r82), " ");
} }
function MaisTableComponent_div_1_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_ng_container_10_button_1_Template, 2, 6, "button", 40);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r9.headerActions);
} }
function MaisTableComponent_div_1_div_11_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r88 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_11_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r88); const action_r86 = ctx.$implicit; const ctx_r87 = i0.ɵɵnextContext(3); return ctx_r87.clickOnContextAction(action_r86); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const action_r86 = ctx.$implicit;
    const ctx_r85 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("disabled", !ctx_r85.isContextActionAllowed(action_r86));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r85.resolveLabel(action_r86), " ");
} }
function MaisTableComponent_div_1_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 42);
    i0.ɵɵelementStart(1, "button", 43);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 44);
    i0.ɵɵelementStart(5, "div", 45);
    i0.ɵɵelementStart(6, "h6", 21);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, MaisTableComponent_div_1_div_11_button_9_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("disabled", !ctx_r10.anyButtonActionsAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r10.currentActions);
} }
const _c6 = function () { return {}; };
function MaisTableComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵelementStart(1, "div", 5);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_2_Template, 10, 13, "div", 6);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_Template, 6, 5, "div", 7);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_4_Template, 1, 0, "div", 7);
    i0.ɵɵtemplate(5, MaisTableComponent_div_1_div_5_Template, 1, 0, "div", 6);
    i0.ɵɵelementStart(6, "div", 8);
    i0.ɵɵelementStart(7, "div");
    i0.ɵɵtemplate(8, MaisTableComponent_div_1_ng_template_8_Template, 0, 0, "ng-template", 9);
    i0.ɵɵtext(9, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(10, MaisTableComponent_div_1_ng_container_10_Template, 2, 1, "ng-container", 2);
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_11_Template, 10, 8, "div", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r0.filteringPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.columnSelectionPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r0.columnSelectionPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r0.filteringPossibleAndAllowed);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r0.actionsCaptionTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction0(8, _c6));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r0.headerActionsEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.contextActionsEnabledAndPossible);
} }
function MaisTableComponent_ng_container_2_th_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 54);
} }
function MaisTableComponent_ng_container_2_th_9_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r98 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_9_span_2_Template_input_change_1_listener() { i0.ɵɵrestoreView(_r98); const ctx_r97 = i0.ɵɵnextContext(3); return ctx_r97.toggleAllChecked(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r96 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r96.allChecked);
} }
function MaisTableComponent_ng_container_2_th_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 54);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_th_9_span_2_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r90 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r90.currentEnableSelectAll && ctx_r90.currentEnableMultiSelect && !ctx_r90.noResults);
} }
function MaisTableComponent_ng_container_2_th_10_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r99 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, ctx_r99.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_2_th_10_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_2_th_10_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r104 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 20);
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_10_div_7_Template_input_change_1_listener($event) { i0.ɵɵrestoreView(_r104); const filter_r102 = ctx.$implicit; const ctx_r103 = i0.ɵɵnextContext(3); ctx_r103.toggleContextFilterChecked(filter_r102); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "span", 59);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_th_10_div_7_Template_span_click_2_listener($event) { i0.ɵɵrestoreView(_r104); const filter_r102 = ctx.$implicit; const ctx_r105 = i0.ɵɵnextContext(3); ctx_r105.toggleContextFilterChecked(filter_r102); return $event.stopPropagation(); });
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const filter_r102 = ctx.$implicit;
    const ctx_r101 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r101.isContextFilterChecked(filter_r102));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r101.resolveLabel(filter_r102), " ");
} }
function MaisTableComponent_ng_container_2_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 55);
    i0.ɵɵelementStart(1, "div", 56);
    i0.ɵɵelementStart(2, "div", 57);
    i0.ɵɵelementStart(3, "div", 19);
    i0.ɵɵelementStart(4, "h6", 39);
    i0.ɵɵtemplate(5, MaisTableComponent_ng_container_2_th_10_span_5_Template, 3, 3, "span", 2);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_2_th_10_span_6_Template, 3, 3, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_2_th_10_div_7_Template, 4, 2, "div", 58);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r91 = i0.ɵɵnextContext(2);
    i0.ɵɵclassProp("border-primary", ctx_r91.anyContextFilterChecked);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r91.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r91.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r91.currentContextFilters);
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r113 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r113); const column_r106 = i0.ɵɵnextContext(2).$implicit; const ctx_r111 = i0.ɵɵnextContext(2); return ctx_r111.clickOnColumn(column_r106); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r116 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r116); const column_r106 = i0.ɵɵnextContext(2).$implicit; const ctx_r114 = i0.ɵɵnextContext(2); return ctx_r114.clickOnColumn(column_r106); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template, 3, 0, "span", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template, 3, 0, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r106 = i0.ɵɵnextContext().$implicit;
    const ctx_r107 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r107.isCurrentSortingColumn(column_r106, "ASC"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r107.isCurrentSortingColumn(column_r106, "DESC"));
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r106 = i0.ɵɵnextContext().$implicit;
    const ctx_r108 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r108.resolveLabel(column_r106), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_11_Template(rf, ctx) { if (rf & 1) {
    const _r120 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "th", 60);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_Template_th_click_1_listener() { i0.ɵɵrestoreView(_r120); const column_r106 = ctx.$implicit; const ctx_r119 = i0.ɵɵnextContext(2); return ctx_r119.clickOnColumn(column_r106); });
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_Template, 3, 2, "span", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_ng_container_11_span_3_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r106 = ctx.$implicit;
    const ctx_r92 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("order-up", ctx_r92.isCurrentSortingColumn(column_r106, "ASC"))("order-down", ctx_r92.isCurrentSortingColumn(column_r106, "DESC"))("border-primary", ctx_r92.isCurrentSortingColumn(column_r106))("clickable", ctx_r92.isSortable(column_r106));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r92.isCurrentSortingColumn(column_r106));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r92.isLabelVisibleInTable(column_r106) && ctx_r92.hasLabel(column_r106));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    const _r134 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r134); const row_r122 = i0.ɵɵnextContext(2).$implicit; const ctx_r132 = i0.ɵɵnextContext(3); return ctx_r132.clickOnRowExpansion(row_r122); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    const _r137 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r137); const row_r122 = i0.ɵɵnextContext(2).$implicit; const ctx_r135 = i0.ɵɵnextContext(3); return ctx_r135.clickOnRowExpansion(row_r122); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 69);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r123 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r123.isExpanded(row_r122) && ctx_r123.isExpandable(row_r122));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r123.isExpanded(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template(rf, ctx) { if (rf & 1) {
    const _r141 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "th", 69);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵelementStart(2, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template_input_change_2_listener() { i0.ɵɵrestoreView(_r141); const row_r122 = i0.ɵɵnextContext().$implicit; const ctx_r139 = i0.ɵɵnextContext(3); return ctx_r139.toggleChecked(row_r122); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r124 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("checked", ctx_r124.isChecked(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "td");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template(rf, ctx) { }
const _c7 = function (a0, a1, a2) { return { row: a0, column: a1, value: a2 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r143 = i0.ɵɵnextContext().$implicit;
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r144 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r144.cellTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction3(2, _c7, row_r122, column_r143, ctx_r144.extractValue(row_r122, column_r143)));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r143 = i0.ɵɵnextContext().$implicit;
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r145 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r145.extractValue(row_r122, column_r143), " ");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "td");
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template, 2, 6, "div", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template, 2, 1, "div", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r143 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", column_r143.applyTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !column_r143.applyTemplate);
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template(rf, ctx) { }
const _c8 = function (a0) { return { row: a0 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 72);
    i0.ɵɵelement(1, "td");
    i0.ɵɵelementStart(2, "td", 73);
    i0.ɵɵelementStart(3, "div", 74);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r127 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("hidden", !ctx_r127.referencedTable.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵattribute("colspan", ctx_r127.activeColumnsCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r127.referencedTable.dropTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(4, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 75);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r128 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r128.dragTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(2, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 76);
    i0.ɵɵelement(1, "td");
    i0.ɵɵelementStart(2, "td");
    i0.ɵɵelementStart(3, "div", 77);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = i0.ɵɵnextContext().$implicit;
    const ctx_r129 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(2);
    i0.ɵɵattribute("colspan", ctx_r129.activeColumnsCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r129.rowDetailTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(3, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tr", 64);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template, 3, 2, "th", 65);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template, 3, 1, "th", 65);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template, 1, 0, "td", 2);
    i0.ɵɵtemplate(5, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template, 4, 2, "ng-container", 27);
    i0.ɵɵelementStart(6, "td", 50);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template, 5, 6, "tr", 66);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "td", 50);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template, 2, 4, "div", 67);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template, 5, 5, "tr", 68);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const row_r122 = ctx.$implicit;
    const ctx_r121 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("row-selected", ctx_r121.isChecked(row_r122));
    i0.ɵɵproperty("cdkDragData", row_r122)("cdkDragDisabled", !ctx_r121.acceptDrag);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r121.rowExpansionEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r121.currentEnableSelection);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r121.contextFilteringEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r121.visibleColumns);
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r121.rowExpansionEnabledAndPossible && ctx_r121.isExpanded(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tbody");
    i0.ɵɵelement(1, "tr", 63);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template, 11, 9, "ng-container", 27);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r93 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r93.currentData);
} }
function MaisTableComponent_ng_container_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tbody");
    i0.ɵɵelementStart(2, "tr", 78);
    i0.ɵɵelementStart(3, "td", 79);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r94 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵattribute("colspan", ctx_r94.activeColumnsCount);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(5, 2, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tbody");
    i0.ɵɵelementStart(2, "tr", 80);
    i0.ɵɵelementStart(3, "td", 79);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r95 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵattribute("colspan", ctx_r95.activeColumnsCount);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(5, 2, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    const _r158 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 48);
    i0.ɵɵelementStart(2, "table", 49);
    i0.ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_2_Template_table_cdkDropListDropped_2_listener($event) { i0.ɵɵrestoreView(_r158); const ctx_r157 = i0.ɵɵnextContext(); return ctx_r157.handleItemDropped($event); });
    i0.ɵɵelementStart(3, "caption", 50);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "thead", 51);
    i0.ɵɵelementStart(7, "tr");
    i0.ɵɵtemplate(8, MaisTableComponent_ng_container_2_th_8_Template, 1, 0, "th", 52);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_2_th_9_Template, 3, 1, "th", 52);
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_2_th_10_Template, 8, 6, "th", 53);
    i0.ɵɵtemplate(11, MaisTableComponent_ng_container_2_ng_container_11_Template, 4, 10, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(12, MaisTableComponent_ng_container_2_tbody_12_Template, 3, 1, "tbody", 2);
    i0.ɵɵtemplate(13, MaisTableComponent_ng_container_2_ng_container_13_Template, 6, 4, "ng-container", 2);
    i0.ɵɵtemplate(14, MaisTableComponent_ng_container_2_ng_container_14_Template, 6, 4, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("nodrop", !ctx_r1.acceptDrop)("acceptdrop", ctx_r1.acceptDrop);
    i0.ɵɵpropertyInterpolate("id", ctx_r1.currentTableId);
    i0.ɵɵproperty("cdkDropListConnectedTo", ctx_r1.dropConnectedTo)("cdkDropListData", ctx_r1.currentData)("cdkDropListEnterPredicate", ctx_r1.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r1.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(5, 17, "table.common.accessibility.caption"));
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngIf", ctx_r1.rowExpansionEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.currentEnableSelection);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.contextFilteringEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r1.visibleColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r1.showFetching && !ctx_r1.forceReRender);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r1.showFetching && ctx_r1.noResults);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.showFetching);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-header-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    const _r180 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r180); const row_r175 = i0.ɵɵnextContext().$implicit; const ctx_r178 = i0.ɵɵnextContext(4); return ctx_r178.clickOnRowExpansion(row_r175); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    const _r183 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r183); const row_r175 = i0.ɵɵnextContext().$implicit; const ctx_r181 = i0.ɵɵnextContext(4); return ctx_r181.clickOnRowExpansion(row_r175); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell", 93);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r175 = ctx.$implicit;
    const ctx_r165 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r165.isExpanded(row_r175) && ctx_r165.isExpandable(row_r175));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r165.isExpanded(row_r175));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r186 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template_input_change_1_listener() { i0.ɵɵrestoreView(_r186); const ctx_r185 = i0.ɵɵnextContext(5); return ctx_r185.toggleAllChecked(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r184 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r184.allChecked);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-header-cell", 93);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r166 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r166.currentEnableSelectAll && ctx_r166.currentEnableMultiSelect && !ctx_r166.noResults);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template(rf, ctx) { if (rf & 1) {
    const _r189 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-cell", 93);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵelementStart(2, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template_input_change_2_listener() { i0.ɵɵrestoreView(_r189); const row_r187 = ctx.$implicit; const ctx_r188 = i0.ɵɵnextContext(4); return ctx_r188.toggleChecked(row_r187); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r187 = ctx.$implicit;
    const ctx_r167 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("checked", ctx_r167.isChecked(row_r187));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r190 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, ctx_r190.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r195 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 20);
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_input_change_1_listener($event) { i0.ɵɵrestoreView(_r195); const filter_r193 = ctx.$implicit; const ctx_r194 = i0.ɵɵnextContext(5); ctx_r194.toggleContextFilterChecked(filter_r193); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "span", 59);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_span_click_2_listener($event) { i0.ɵɵrestoreView(_r195); const filter_r193 = ctx.$implicit; const ctx_r196 = i0.ɵɵnextContext(5); ctx_r196.toggleContextFilterChecked(filter_r193); return $event.stopPropagation(); });
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const filter_r193 = ctx.$implicit;
    const ctx_r192 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r192.isContextFilterChecked(filter_r193));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r192.resolveLabel(filter_r193), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-header-cell", 93);
    i0.ɵɵelementStart(1, "div", 94);
    i0.ɵɵelementStart(2, "div", 56);
    i0.ɵɵelementStart(3, "div", 57);
    i0.ɵɵelementStart(4, "div", 19);
    i0.ɵɵelementStart(5, "h6", 39);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template, 3, 3, "span", 2);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template, 3, 3, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template, 4, 2, "div", 58);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r168 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("border-primary", ctx_r168.anyContextFilterChecked);
    i0.ɵɵproperty("autoClose", "outside")("container", "body");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r168.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r168.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r168.currentContextFilters);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell");
    i0.ɵɵelementStart(1, "div", 77);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const detail_r198 = ctx.$implicit;
    const ctx_r170 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r170.rowDetailTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(2, _c8, detail_r198.___parentRow));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r209 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r209); const column_r200 = i0.ɵɵnextContext(3).$implicit; const ctx_r207 = i0.ɵɵnextContext(4); return ctx_r207.clickOnColumn(column_r200); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r212 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r212); const column_r200 = i0.ɵɵnextContext(3).$implicit; const ctx_r210 = i0.ɵɵnextContext(4); return ctx_r210.clickOnColumn(column_r200); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template, 3, 0, "span", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template, 3, 0, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r203 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r203.isCurrentSortingColumn(column_r200, "ASC"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r203.isCurrentSortingColumn(column_r200, "DESC"));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = i0.ɵɵnextContext(2).$implicit;
    const ctx_r204 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r204.resolveLabel(column_r200), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template(rf, ctx) { if (rf & 1) {
    const _r217 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-header-cell");
    i0.ɵɵelementStart(1, "div", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template_div_click_1_listener() { i0.ɵɵrestoreView(_r217); const column_r200 = i0.ɵɵnextContext().$implicit; const ctx_r215 = i0.ɵɵnextContext(4); return ctx_r215.clickOnColumn(column_r200); });
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template, 3, 2, "span", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = i0.ɵɵnextContext().$implicit;
    const ctx_r201 = i0.ɵɵnextContext(4);
    i0.ɵɵclassMapInterpolate1("maissize-", column_r200.size || "default", "");
    i0.ɵɵadvance(1);
    i0.ɵɵclassMap(column_r200.headerDisplayClass || "");
    i0.ɵɵclassProp("order-up", ctx_r201.isCurrentSortingColumn(column_r200, "ASC"))("order-down", ctx_r201.isCurrentSortingColumn(column_r200, "DESC"))("border-primary", ctx_r201.isCurrentSortingColumn(column_r200))("clickable", ctx_r201.isSortable(column_r200));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r201.isCurrentSortingColumn(column_r200));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r201.isLabelVisibleInTable(column_r200) && ctx_r201.hasLabel(column_r200));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r219 = i0.ɵɵnextContext().$implicit;
    const column_r200 = i0.ɵɵnextContext().$implicit;
    const ctx_r220 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r220.cellTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction3(2, _c7, row_r219, column_r200, ctx_r220.extractValue(row_r219, column_r200)));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r219 = i0.ɵɵnextContext().$implicit;
    const column_r200 = i0.ɵɵnextContext().$implicit;
    const ctx_r221 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r221.extractValue(row_r219, column_r200), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template, 2, 6, "div", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template, 2, 1, "div", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵclassMapInterpolate2("maissize-", column_r200.size || "default", " ", column_r200.cellDisplayClass || "", "");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", column_r200.applyTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !column_r200.applyTemplate);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0, 85);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template, 4, 16, "mat-header-cell", 95);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template, 3, 6, "mat-cell", 96);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r200 = ctx.$implicit;
    i0.ɵɵproperty("matColumnDef", column_r200.name);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-header-row");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-row", 97);
} if (rf & 2) {
    const row_r228 = ctx.$implicit;
    const ctx_r173 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("expanded", ctx_r173.isMatTableExpanded(row_r228))("row-selected", ctx_r173.isChecked(row_r228));
    i0.ɵɵproperty("cdkDragData", row_r228)("cdkDragDisabled", !ctx_r173.acceptDrag);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-row", 98);
} if (rf & 2) {
    const row_r229 = ctx.$implicit;
    const ctx_r174 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("@detailExpand", ctx_r174.isMatTableExpanded(row_r229) ? "expanded" : "collapsed");
} }
const _c9 = function () { return ["internal___col_row_detail"]; };
function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template(rf, ctx) { if (rf & 1) {
    const _r231 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-table", 83, 84);
    i0.ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template_mat_table_cdkDropListDropped_0_listener($event) { i0.ɵɵrestoreView(_r231); const ctx_r230 = i0.ɵɵnextContext(3); return ctx_r230.handleItemDropped($event); });
    i0.ɵɵelementContainerStart(2, 85);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template, 1, 0, "mat-header-cell", 86);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template, 3, 2, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(5, 85);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template, 3, 1, "mat-header-cell", 86);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template, 3, 1, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(8, 85);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template, 9, 7, "mat-header-cell", 86);
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template, 1, 0, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(11, 85);
    i0.ɵɵtemplate(12, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template, 3, 4, "mat-cell", 88);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵtemplate(13, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template, 3, 1, "ng-container", 89);
    i0.ɵɵtemplate(14, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template, 1, 0, "mat-header-row", 90);
    i0.ɵɵtemplate(15, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template, 1, 6, "mat-row", 91);
    i0.ɵɵtemplate(16, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template, 1, 1, "mat-row", 92);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r160 = i0.ɵɵnextContext(3);
    i0.ɵɵclassProp("nodrop", !ctx_r160.acceptDrop)("acceptdrop", ctx_r160.acceptDrop);
    i0.ɵɵpropertyInterpolate("id", ctx_r160.currentTableId);
    i0.ɵɵproperty("dataSource", ctx_r160.matTableDataObservable)("cdkDropListConnectedTo", ctx_r160.dropConnectedTo)("cdkDropListData", ctx_r160.currentData)("cdkDropListEnterPredicate", ctx_r160.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r160.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_expansion");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_selection");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_context_filtering");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_detail");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r160.matTableVisibleColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matHeaderRowDef", ctx_r160.matTableVisibleColumnDefs);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", ctx_r160.matTableVisibleColumnDefs);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", i0.ɵɵpureFunction0(19, _c9))("matRowDefWhen", ctx_r160.isMatTableExpansionDetailRow);
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 78);
    i0.ɵɵelementStart(2, "p", 99);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 1, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 80);
    i0.ɵɵelementStart(2, "p", 99);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 1, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 48);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_Template, 17, 20, "mat-table", 82);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_ng_container_2_Template, 5, 3, "ng-container", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_ng_container_3_Template, 5, 3, "ng-container", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r159 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r159.showFetching && !ctx_r159.forceReRender);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r159.showFetching && ctx_r159.noResults);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r159.showFetching);
} }
function MaisTableComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_Template, 4, 3, "div", 81);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.matTableDataObservable);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 113);
    i0.ɵɵtext(1, "(current)");
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template(rf, ctx) { if (rf & 1) {
    const _r244 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 106);
    i0.ɵɵelementStart(1, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template_a_click_1_listener() { i0.ɵɵrestoreView(_r244); const page_r238 = i0.ɵɵnextContext().$implicit; const ctx_r242 = i0.ɵɵnextContext(3); return ctx_r242.switchToPage(page_r238); });
    i0.ɵɵtext(2);
    i0.ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template, 2, 0, "span", 112);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const page_r238 = i0.ɵɵnextContext().$implicit;
    const ctx_r239 = i0.ɵɵnextContext(3);
    i0.ɵɵclassProp("active", page_r238 === ctx_r239.currentPageIndex);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", page_r238 + 1, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", page_r238 === ctx_r239.currentPageIndex);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 114);
    i0.ɵɵelementStart(1, "a", 115);
    i0.ɵɵtext(2, " ... ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template, 4, 4, "li", 110);
    i0.ɵɵtemplate(2, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template, 3, 0, "li", 111);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const page_r238 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !page_r238.skip);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", page_r238.skip);
} }
function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r249 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r249); const pageSize_r247 = ctx.$implicit; const ctx_r248 = i0.ɵɵnextContext(4); return ctx_r248.clickOnPageSize(pageSize_r247); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const pageSize_r247 = ctx.$implicit;
    const ctx_r246 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("disabled", pageSize_r247 === ctx_r246.currentPageSize);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", pageSize_r247, " ");
} }
const _c10 = function (a0) { return { current: a0 }; };
function MaisTableComponent_div_4_ng_container_3_span_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 116);
    i0.ɵɵelementStart(1, "div");
    i0.ɵɵtext(2, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "button", 117);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "span", 44);
    i0.ɵɵtemplate(7, MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r237 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind2(5, 2, "table.common.messages.page_size_button", i0.ɵɵpureFunction1(5, _c10, ctx_r237.currentPageSize)));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r237.currentPossiblePageSizes);
} }
const _c11 = function (a0, a1) { return { totalElements: a0, totalPages: a1 }; };
function MaisTableComponent_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    const _r251 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 102);
    i0.ɵɵelementStart(2, "div", 103);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "nav", 104);
    i0.ɵɵelementStart(6, "ul", 105);
    i0.ɵɵelementStart(7, "li", 106);
    i0.ɵɵelementStart(8, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_8_listener() { i0.ɵɵrestoreView(_r251); const ctx_r250 = i0.ɵɵnextContext(2); return ctx_r250.switchToPage(0); });
    i0.ɵɵtext(9);
    i0.ɵɵpipe(10, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(11, "li", 106);
    i0.ɵɵelementStart(12, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_12_listener() { i0.ɵɵrestoreView(_r251); const ctx_r252 = i0.ɵɵnextContext(2); return ctx_r252.switchToPage(ctx_r252.currentPageIndex - 1); });
    i0.ɵɵtext(13);
    i0.ɵɵpipe(14, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(15, MaisTableComponent_div_4_ng_container_3_ng_container_15_Template, 3, 2, "ng-container", 27);
    i0.ɵɵelementStart(16, "li", 106);
    i0.ɵɵelementStart(17, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_17_listener() { i0.ɵɵrestoreView(_r251); const ctx_r253 = i0.ɵɵnextContext(2); return ctx_r253.switchToPage(ctx_r253.currentPageIndex + 1); });
    i0.ɵɵtext(18);
    i0.ɵɵpipe(19, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(20, "li", 106);
    i0.ɵɵelementStart(21, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_21_listener() { i0.ɵɵrestoreView(_r251); const ctx_r254 = i0.ɵɵnextContext(2); return ctx_r254.switchToPage(ctx_r254.currentPageCount - 1); });
    i0.ɵɵtext(22);
    i0.ɵɵpipe(23, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(24, "div", 108);
    i0.ɵɵtemplate(25, MaisTableComponent_div_4_ng_container_3_span_25_Template, 8, 7, "span", 109);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r232 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind2(4, 15, "table.common.pagination.total_elements", i0.ɵɵpureFunction2(26, _c11, ctx_r232.currentResultNumber, ctx_r232.currentPageCount)), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵclassProp("disabled", !(ctx_r232.currentPageIndex > 0));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(10, 18, "table.common.pagination.first_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("disabled", !(ctx_r232.currentPageIndex > 0));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(14, 20, "table.common.pagination.previous_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r232.enumPages);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("disabled", ctx_r232.currentPageIndex >= ctx_r232.currentPageCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(19, 22, "table.common.pagination.next_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("disabled", ctx_r232.currentPageIndex >= ctx_r232.currentPageCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(23, 24, "table.common.pagination.last_page"));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r232.pageSizeSelectEnabledAndPossible);
} }
function MaisTableComponent_div_4_ng_template_6_Template(rf, ctx) { }
function MaisTableComponent_div_4_ng_container_8_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r258 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 41);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_8_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r258); const action_r256 = ctx.$implicit; const ctx_r257 = i0.ɵɵnextContext(3); return ctx_r257.clickOnContextAction(action_r256); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const action_r256 = ctx.$implicit;
    const ctx_r255 = i0.ɵɵnextContext(3);
    i0.ɵɵclassMapInterpolate2("btn btn-", action_r256.displayClass || "light", " ", action_r256.additionalClasses || "", " mr-1");
    i0.ɵɵproperty("disabled", !ctx_r255.isContextActionAllowed(action_r256));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r255.resolveLabel(action_r256), " ");
} }
function MaisTableComponent_div_4_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_8_button_1_Template, 2, 6, "button", 40);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r234 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r234.headerActions);
} }
function MaisTableComponent_div_4_div_9_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r262 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_div_9_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r262); const action_r260 = ctx.$implicit; const ctx_r261 = i0.ɵɵnextContext(3); return ctx_r261.clickOnContextAction(action_r260); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const action_r260 = ctx.$implicit;
    const ctx_r259 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("disabled", !ctx_r259.isContextActionAllowed(action_r260));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r259.resolveLabel(action_r260), " ");
} }
function MaisTableComponent_div_4_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 42);
    i0.ɵɵelementStart(1, "button", 43);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 44);
    i0.ɵɵelementStart(5, "div", 45);
    i0.ɵɵelementStart(6, "h6", 21);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, MaisTableComponent_div_4_div_9_button_9_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r235 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("disabled", !ctx_r235.anyButtonActionsAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r235.currentActions);
} }
function MaisTableComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 100);
    i0.ɵɵelementStart(1, "div", 5);
    i0.ɵɵelementStart(2, "div", 101);
    i0.ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_Template, 26, 29, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 8);
    i0.ɵɵelementStart(5, "div");
    i0.ɵɵtemplate(6, MaisTableComponent_div_4_ng_template_6_Template, 0, 0, "ng-template", 9);
    i0.ɵɵtext(7, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_div_4_ng_container_8_Template, 2, 1, "ng-container", 2);
    i0.ɵɵtemplate(9, MaisTableComponent_div_4_div_9_Template, 10, 8, "div", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r3.currentResultNumber > 0 && ctx_r3.currentEnablePagination);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r3.actionsCaptionTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction0(5, _c6));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r3.headerActionsEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r3.contextActionsEnabledAndPossible);
} }
export class MaisTableComponent {
    // lifecycle hooks
    constructor(translateService, configurationService, registry, cdr, ngZone) {
        this.translateService = translateService;
        this.configurationService = configurationService;
        this.registry = registry;
        this.cdr = cdr;
        this.ngZone = ngZone;
        this.dataProvider = null;
        this.defaultSortingColumn = null;
        this.defaultSortingDirection = null;
        this.defaultPageSize = null;
        this.possiblePageSize = null;
        // output events
        this.pageChange = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.selectionChange = new EventEmitter();
        this.filteringColumnsChange = new EventEmitter();
        this.visibleColumnsChange = new EventEmitter();
        this.contextFiltersChange = new EventEmitter();
        this.statusChange = new EventEmitter();
        this.itemDragged = new EventEmitter();
        this.itemDropped = new EventEmitter();
        this.action = new EventEmitter();
        this.isIE = MaisTableBrowserHelper.isIE();
        this.statusSnapshot = null;
        this.persistableStatusSnapshot = null;
        this.forceReRender = false;
        this.initialized = false;
        this.fetching = false;
        this.showFetching = false;
        this.refreshEmitterSubscription = null;
        this.refreshIntervalSubscription = null;
        // MatTable adapter
        this.matTableDataObservable = null;
        this.expandedElement = null;
        // drag and drop support
        this.acceptDropPredicate = (item) => {
            return this.acceptDrop;
        };
        this.isMatTableExpansionDetailRow = (i, row) => row.hasOwnProperty('___detailRowContent');
        this.isMatTableExpanded = (row, limit = false) => {
            if (!limit && row.___detailRow) {
                if (this.isMatTableExpanded(row.___detailRow, true)) {
                    return true;
                }
            }
            if (!limit && row.___parentRow) {
                if (this.isMatTableExpanded(row.___parentRow, true)) {
                    return true;
                }
            }
            if (!this.rowExpansionEnabledAndPossible) {
                return false;
            }
            if (!this.isExpandable(row)) {
                return false;
            }
            if (!this.isExpanded(row)) {
                return false;
            }
            return true;
        };
        this.uuid = 'MTCMP-' + (++MaisTableComponent.counter) + '-' + Math.round(Math.random() * 100000);
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('building component');
        this.matTableDataObservable = new Subject();
    }
    ngOnInit() {
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('initializing component');
        this.registrationId = this.registry.register(this.currentTableId, this);
        // input validation
        MaisTableValidatorHelper.validateColumnsSpecification(this.columns, this.configurationService.getConfiguration());
        this.clientDataSnapshot = [];
        this.dataSnapshot = [];
        this.fetchedPageCount = 0;
        this.fetchedResultNumber = 0;
        this.selectedPageIndex = 0;
        this.selectedSearchQuery = null;
        this.selectedPageSize = null;
        this.expandedItems = [];
        this.checkedItems = [];
        this.checkedContextFilters = [];
        this.checkedColumnsForFiltering = this.filterableColumns.filter(c => MaisTableFormatterHelper.isDefaultFilterColumn(c, this.configurationService.getConfiguration()));
        this.checkedColumnsForVisualization = this.columns.filter(c => MaisTableFormatterHelper.isDefaultVisibleColumn(c, this.configurationService.getConfiguration()));
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        if (this.dataProvider) {
            // input is a provider function
            if (this.data) {
                throw new Error('MaisTable can\'t be provided both data and dataProvider');
            }
        }
        else if (this.data instanceof Observable || this.data instanceof Subject) {
            // input is observable
            this.data.subscribe(dataSnapshot => {
                this.handleInputDataObservableEmission(dataSnapshot);
            });
        }
        else {
            // input is data array
            this.clientDataSnapshot = this.data;
        }
        // LOAD STATUS from store if possible
        let activationObservable;
        if (this.storePersistenceEnabledAndPossible) {
            activationObservable = this.loadStatusFromStore();
        }
        else {
            activationObservable = new Observable(subscriber => {
                subscriber.next(null);
                subscriber.complete();
            });
        }
        activationObservable.subscribe((status) => {
            if (status) {
                this.logger.trace('restored status snapshot from storage', status);
                this.applyStatusSnapshot(status);
            }
            this.reload({ reason: MaisTableReloadReason.INTERNAL, withStatusSnapshot: status }).subscribe(yes => {
                this.completeInitialization();
            }, nope => {
                this.completeInitialization();
            });
        }, failure => {
            this.logger.error('restoring status from store failed', failure);
            this.reload({ reason: MaisTableReloadReason.INTERNAL }).subscribe(yes => {
                this.completeInitialization();
            }, nope => {
                this.completeInitialization();
            });
        });
    }
    ngAfterContentInit() {
        this.logger.debug('after content init');
    }
    handleInputDataObservableEmission(dataSnapshot) {
        this.logger.debug('data snapshot emitted from input data');
        this.clientDataSnapshot = dataSnapshot;
        this.reload({ reason: MaisTableReloadReason.EXTERNAL });
    }
    completeInitialization() {
        this.initialized = true;
        this.statusChange.pipe(debounceTime(200)).subscribe(statusSnapshot => {
            this.persistStatusToStore().subscribe();
        });
        if (this.refreshEmitter) {
            this.handleRefreshEmitterChange(this.refreshEmitter);
        }
        if (this.refreshInterval) {
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    }
    ngOnDestroy() {
        this.logger.trace('destroying component');
        if (this.registrationId) {
            this.registry.unregister(this.currentTableId, this.registrationId);
        }
    }
    ngOnChanges(changes) {
        if (changes.refreshEmitter) {
            this.handleRefreshEmitterChange(changes.refreshEmitter.currentValue);
        }
        if (changes.refreshInterval) {
            this.handleRefreshIntervalChange(changes.refreshInterval.currentValue);
        }
        if (changes.refreshStrategy && !changes.refreshStrategy.firstChange) {
            this.logger.warn('REFRESH STRATEGY CHANGED WHILE RUNNING. YOU SURE ABOUT THIS?', changes);
            this.handleRefreshEmitterChange(this.refreshEmitter);
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    }
    handleRefreshEmitterChange(newValue) {
        if (this.refreshEmitterSubscription) {
            this.refreshEmitterSubscription.unsubscribe();
        }
        if (newValue) {
            this.refreshEmitterSubscription = newValue.subscribe(event => {
                this.logger.trace('received refresh push request', event);
                if (this.dragInProgress) {
                    this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.ON_PUSH) === -1) {
                    this.logger.warn('refresh from emitter ignored because refresh strategies are ' + this.currentRefreshStrategies +
                        '. Why are you pushing to this component?');
                }
                else if (!this.initialized) {
                    this.logger.warn('refresh from emitter ignored because the component is not fully initialized');
                }
                else if (this.fetching) {
                    this.logger.warn('refresh from emitter ignored because the component is fetching already');
                }
                else {
                    this.logger.debug('launching reload following push request');
                    this.reload({
                        reason: MaisTableReloadReason.PUSH,
                        pushRequest: event,
                        inBackground: event.inBackground === true || event.inBackground === false ?
                            event.inBackground : this.currentRefreshOnPushInBackground
                    });
                }
            });
        }
    }
    handleRefreshIntervalChange(newValue) {
        if (this.refreshIntervalSubscription) {
            this.refreshIntervalSubscription.unsubscribe();
        }
        if (this.refreshIntervalTimer) {
            this.refreshIntervalTimer = null;
        }
        if (newValue) {
            this.refreshIntervalTimer = timer(newValue, newValue);
            this.refreshIntervalSubscription = this.refreshIntervalTimer.subscribe(tick => {
                this.logger.trace('emitted refresh tick request');
                if (this.dragInProgress) {
                    this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.TIMED) === -1) {
                    this.logger.warn('refresh from tick ignored because refresh strategies are ' + this.currentRefreshStrategies +
                        '. Why is this component emitting ticks ?');
                }
                else if (!this.initialized) {
                    this.logger.warn('refresh from tick ignored because the component is not fully initialized');
                }
                else if (this.fetching) {
                    this.logger.warn('refresh from tick ignored because the component is fetching already');
                }
                else {
                    this.logger.debug('launching reload following tick');
                    this.reload({
                        reason: MaisTableReloadReason.INTERVAL,
                        inBackground: this.currentRefreshIntervalInBackground
                    });
                }
            });
        }
    }
    // public methods
    refresh(background) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        return this.reload({ reason: MaisTableReloadReason.EXTERNAL, inBackground: background });
    }
    getDataSnapshot() {
        return this.dataSnapshot;
    }
    getStatusSnapshot() {
        return this.statusSnapshot;
    }
    loadStatus(status) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        this.logger.trace('loading status snapshot from external caller', status);
        this.applyStatusSnapshot(status);
        this.reload({ reason: MaisTableReloadReason.EXTERNAL, withStatusSnapshot: status });
    }
    applyStatusSnapshot(status) {
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (status.orderColumn) {
            this.selectedSortColumn = this.columns.find(c => this.isSortable(c) && c.name === status.orderColumn) || null;
        }
        if (status.orderColumnDirection) {
            this.selectedSortDirection = (status.orderColumnDirection === MaisTableSortDirection.DESCENDING) ?
                MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
        }
        if (this.currentEnableFiltering && status.query) {
            this.selectedSearchQuery = status.query.trim();
        }
        if (this.currentEnableFiltering && status.queryColumns && status.queryColumns.length) {
            this.checkedColumnsForFiltering = this.filterableColumns.filter(c => { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.queryColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableColumnsSelection && status.visibleColumns && status.visibleColumns.length) {
            this.checkedColumnsForVisualization = this.columns.filter(c => { var _a, _b; return !this.isHideable(c) || ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.visibleColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableContextFiltering && status.contextFilters && status.contextFilters.length) {
            this.checkedContextFilters = this.contextFilters.filter(f => { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.contextFilters) === null || _b === void 0 ? void 0 : _b.indexOf(f.name)) !== -1; });
        }
        if (this.currentEnablePagination && status.currentPage || status.currentPage === 0) {
            this.selectedPageIndex = status.currentPage;
        }
        if (this.currentEnablePagination && status.pageSize) {
            this.selectedPageSize = this.currentPossiblePageSizes.find(s => status.pageSize === s) || null;
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    }
    applyStatusSnapshotPostFetch(status) {
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (this.currentEnableSelection && this.itemTrackingEnabledAndPossible
            && status.checkedItemIdentifiers && status.checkedItemIdentifiers.length) {
            this.checkedItems = this.dataSnapshot.filter(data => {
                var _a, _b;
                const id = this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.checkedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1;
            });
        }
        if (this.currentEnableRowExpansion && this.itemTrackingEnabledAndPossible
            && status.expandedItemIdentifiers && status.expandedItemIdentifiers.length) {
            this.expandedItems = this.dataSnapshot.filter(data => {
                var _a, _b;
                const id = this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.expandedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1 && this.isExpandable(data);
            });
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    }
    reload(context) {
        this.fetching = true;
        this.showFetching = !context.inBackground;
        const obs = new Observable(subscriber => {
            try {
                this.reloadInObservable(subscriber, context);
            }
            catch (e) {
                subscriber.error(e);
                subscriber.complete();
            }
        });
        obs.subscribe(success => {
            this.logger.trace('async reload success');
            this.fetching = false;
            if (!context.inBackground) {
                this.showFetching = false;
            }
            this.handleMatTableDataSnapshotChanged();
        }, failure => {
            this.logger.trace('async reload failed', failure);
            this.fetching = false;
            if (!context.inBackground) {
                this.showFetching = false;
            }
            this.handleMatTableDataSnapshotChanged();
        });
        return obs;
    }
    reloadInObservable(tracker, context) {
        const withSnapshot = context.withStatusSnapshot || null;
        const pageRequest = this.buildPageRequest();
        this.logger.debug('reloading table data', pageRequest);
        // clear checked items
        if (!context.inBackground) {
            this.checkedItems = [];
            this.expandedItems = [];
            this.statusChanged();
        }
        this.lastFetchedSearchQuery = pageRequest.query || null;
        if (this.dataProvider) {
            // call data provider
            this.logger.trace('reload has been called, fetching data from provided function');
            this.logger.trace('page request for data fetch is', pageRequest);
            this.dataProvider(pageRequest, context).subscribe((response) => {
                this.logger.trace('fetching data completed successfully');
                if (!this.dragInProgress) {
                    if (this.paginationMode === MaisTablePaginationMethod.SERVER) {
                        this.parseResponseWithServerPagination(response);
                    }
                    else {
                        this.parseResponseWithClientPagination(response.content, pageRequest);
                    }
                    if (withSnapshot) {
                        this.applyStatusSnapshotPostFetch(withSnapshot);
                    }
                }
                else {
                    this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.next();
                tracker.complete();
            }, failure => {
                this.logger.error('error fetching data from provider function', failure);
                if (!this.dragInProgress) {
                    this.dataSnapshot = [];
                }
                else {
                    this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.error(failure);
                tracker.complete();
            });
        }
        else {
            // data is not provided on request and is in clientDataSnapshot
            this.logger.trace('reload has been called on locally fetched data');
            if (!this.dragInProgress) {
                this.parseResponseWithClientPagination(this.clientDataSnapshot, pageRequest);
                if (withSnapshot) {
                    this.applyStatusSnapshotPostFetch(withSnapshot);
                }
            }
            else {
                this.logger.warn('data fetch aborted because user is dragging things');
            }
            tracker.next();
            tracker.complete();
        }
    }
    parseResponseWithServerPagination(response) {
        this.dataSnapshot = response.content;
        if (this.currentEnablePagination) {
            if (response.totalPages) {
                this.fetchedPageCount = response.totalPages;
            }
            else {
                throw new Error('data from server did not contain required totalPages field');
            }
            if (response.totalElements) {
                this.fetchedResultNumber = response.totalElements;
            }
            else {
                throw new Error('data from server did not contain required totalElements field');
            }
        }
    }
    parseResponseWithClientPagination(data, request) {
        this.logger.trace('applying in-memory fetching, paginating, ordering and filtering');
        const inMemoryResponse = MaisTableInMemoryHelper.fetchInMemory(data, request, this.currentSortColumn, this.checkedColumnsForFiltering, this.getCurrentLocale());
        this.dataSnapshot = inMemoryResponse.content;
        this.fetchedResultNumber = typeof inMemoryResponse.totalElements === 'undefined' ? null : inMemoryResponse.totalElements;
        this.fetchedPageCount = typeof inMemoryResponse.totalPages === 'undefined' ? null : inMemoryResponse.totalPages;
    }
    buildPageRequest() {
        var _a, _b;
        const output = {
            page: this.currentEnablePagination ? this.currentPageIndex : null,
            size: this.currentEnablePagination ? this.currentPageSize : null,
            sort: [],
            query: null,
            queryFields: [],
            filters: []
        };
        if (this.currentEnableFiltering && this.searchQueryActive) {
            output.query = this.currentSearchQuery;
            output.queryFields = this.checkedColumnsForFiltering.map(column => column.serverField || column.field || null);
        }
        if (this.currentEnableContextFiltering && this.checkedContextFilters.length) {
            for (const filter of this.checkedContextFilters) {
                (_a = output.filters) === null || _a === void 0 ? void 0 : _a.push(filter.name || null);
            }
        }
        const sortColumn = this.currentSortColumn;
        const sortDirection = this.currentSortDirection;
        if (sortColumn) {
            (_b = output.sort) === null || _b === void 0 ? void 0 : _b.push({
                property: sortColumn.serverField || sortColumn.field || null,
                direction: sortDirection || MaisTableSortDirection.ASCENDING
            });
        }
        return output;
    }
    setPage(index) {
        this.selectedPageIndex = index;
        this.statusChanged();
        this.emitPageChanged();
    }
    get currentRefreshOnPushInBackground() {
        var _a;
        if (this.refreshOnPushInBackground === true || this.refreshOnPushInBackground === false) {
            return this.refreshOnPushInBackground;
        }
        else {
            return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnPushInBackground;
        }
    }
    get currentRefreshIntervalInBackground() {
        var _a;
        if (this.refreshIntervalInBackground === true || this.refreshIntervalInBackground === false) {
            return this.refreshIntervalInBackground;
        }
        else {
            return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnTickInBackground;
        }
    }
    get currentRefreshStrategies() {
        var _a;
        if (this.refreshStrategy) {
            if (Array.isArray(this.refreshStrategy) && this.refreshStrategy.length) {
                return this.refreshStrategy;
            }
            else {
                return [this.refreshStrategy];
            }
        }
        return [(_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultStrategy];
    }
    get currentRefreshInterval() {
        var _a;
        return this.refreshInterval || ((_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultInterval);
    }
    get currentTableId() {
        return this.tableId || this.uuid;
    }
    get currentEnableDrag() {
        var _a;
        if (this.enableDrag === true || this.enableDrag === false) {
            return this.enableDrag;
        }
        else {
            return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dragEnabledByDefault;
        }
    }
    get currentEnableDrop() {
        var _a;
        if (this.enableDrop === true || this.enableDrop === false) {
            return this.enableDrop;
        }
        else {
            return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dropEnabledByDefault;
        }
    }
    get currentEnablePagination() {
        var _a;
        if (this.enablePagination === true || this.enablePagination === false) {
            return this.enablePagination;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableItemTracking() {
        var _a;
        if (this.enableItemTracking === true || this.enableItemTracking === false) {
            return this.enableItemTracking;
        }
        else {
            return (_a = this.configurationService.getConfiguration().itemTracking) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableStorePersistence() {
        var _a;
        if (this.enableStorePersistence === true || this.enableStorePersistence === false) {
            return this.enableStorePersistence;
        }
        else {
            return (_a = this.configurationService.getConfiguration().storePersistence) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnablePageSizeSelect() {
        var _a;
        if (this.enablePageSizeSelect === true || this.enablePageSizeSelect === false) {
            return this.enablePageSizeSelect;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.pageSizeSelectionEnabledByDefault;
        }
    }
    get currentEnableMultipleRowExpansion() {
        var _a;
        if (this.enableMultipleRowExpansion === true || this.enableMultipleRowExpansion === false) {
            return this.enableMultipleRowExpansion;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.multipleExpansionEnabledByDefault;
        }
    }
    get currentEnableRowExpansion() {
        var _a;
        if (this.enableRowExpansion === true || this.enableRowExpansion === false) {
            return this.enableRowExpansion;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableHeaderActions() {
        var _a;
        if (this.enableHeaderActions === true || this.enableHeaderActions === false) {
            return this.enableHeaderActions;
        }
        else {
            return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.headerActionsEnabledByDefault;
        }
    }
    get currentEnableContextActions() {
        var _a;
        if (this.enableContextActions === true || this.enableContextActions === false) {
            return this.enableContextActions;
        }
        else {
            return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.contextActionsEnabledByDefault;
        }
    }
    get currentEnableColumnsSelection() {
        var _a;
        if (this.enableColumnsSelection === true || this.enableColumnsSelection === false) {
            return this.enableColumnsSelection;
        }
        else {
            return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableFiltering() {
        var _a;
        if (this.enableFiltering === true || this.enableFiltering === false) {
            return this.enableFiltering;
        }
        else {
            return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableMultiSelect() {
        var _a;
        if (this.enableMultiSelect === true || this.enableMultiSelect === false) {
            return this.enableMultiSelect;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.multipleSelectionEnabledByDefault;
        }
    }
    get currentEnableSelectAll() {
        var _a;
        if (this.enableSelectAll === true || this.enableSelectAll === false) {
            return this.enableSelectAll;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.selectAllEnabledByDefault;
        }
    }
    get currentEnableContextFiltering() {
        var _a;
        if (this.enableContextFiltering === true || this.enableContextFiltering === false) {
            return this.enableContextFiltering;
        }
        else {
            return (_a = this.configurationService.getConfiguration().contextFiltering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableSelection() {
        var _a;
        if (this.enableSelection === true || this.enableSelection === false) {
            return this.enableSelection;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentPaginationMode() {
        var _a;
        if (this.paginationMode) {
            return this.paginationMode;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPaginationMode;
        }
    }
    get itemTrackingEnabledAndPossible() {
        return !!this.itemIdentifier && this.currentEnableItemTracking;
    }
    get storePersistenceEnabledAndPossible() {
        return this.storeAdapter && this.currentEnableStorePersistence;
    }
    get rowExpansionEnabledAndPossible() {
        return this.currentEnableRowExpansion;
    }
    get pageSizeSelectEnabledAndPossible() {
        return this.currentPossiblePageSizes && this.currentPossiblePageSizes.length > 0 && this.currentEnablePageSizeSelect;
    }
    get contextFilteringEnabledAndPossible() {
        return this.contextFilters && this.contextFilters.length > 0 && this.currentEnableContextFiltering;
    }
    get contextActionsEnabledAndPossible() {
        return this.contextActions && this.contextActions.length > 0 && this.currentEnableContextActions;
    }
    get headerActionsEnabledAndPossible() {
        return this.headerActions && this.headerActions.length > 0 && this.currentEnableHeaderActions;
    }
    get columnSelectionPossibleAndAllowed() {
        return this.currentEnableColumnsSelection && this.columns.length > 0 && this.hideableColumns.length > 0;
    }
    get filteringPossibleAndAllowed() {
        return this.currentEnableFiltering && this.filterableColumns.length > 0;
    }
    get currentPossiblePageSizes() {
        var _a;
        if (this.possiblePageSize && this.possiblePageSize.length) {
            return this.possiblePageSize;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPossiblePageSizes;
        }
    }
    get currentSearchQuery() {
        if (this.selectedSearchQuery) {
            return this.selectedSearchQuery;
        }
        else {
            return null;
        }
    }
    get currentPageCount() {
        return this.fetchedPageCount || 0;
    }
    get currentResultNumber() {
        return this.fetchedResultNumber || 0;
    }
    get currentPageSize() {
        var _a;
        const def = (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPageSize;
        if (this.selectedPageSize) {
            return this.selectedPageSize;
        }
        else if (this.defaultPageSize) {
            return this.defaultPageSize;
        }
        else if (this.currentPossiblePageSizes.length &&
            def &&
            this.currentPossiblePageSizes.indexOf(def) !== -1) {
            return def;
        }
        else if (this.currentPossiblePageSizes.length) {
            return this.currentPossiblePageSizes[0];
        }
        else if (def) {
            return def;
        }
        else {
            return 10;
        }
    }
    get currentPageIndex() {
        if (this.selectedPageIndex) {
            return this.selectedPageIndex;
        }
        else {
            return 0;
        }
    }
    get currentSortColumn() {
        if (this.selectedSortColumn) {
            // todo log if sorting column disappears
            return this.visibleColumns.find(c => { var _a; return c.name === ((_a = this.selectedSortColumn) === null || _a === void 0 ? void 0 : _a.name); }) || null;
        }
        else if (this.defaultSortingColumn) {
            const foundColumn = this.visibleColumns.find(c => c.name === this.defaultSortingColumn);
            if (foundColumn) {
                return foundColumn;
            }
            else {
                // find first sortable column?
                return this.visibleColumns.find(column => MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
            }
        }
        else {
            // find first sortable column?
            return this.visibleColumns.find(column => MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
        }
    }
    get currentSortDirection() {
        if (this.selectedSortDirection) {
            return this.selectedSortDirection;
        }
        else if (this.defaultSortingDirection) {
            return this.defaultSortingDirection === MaisTableSortDirection.DESCENDING ?
                MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
        }
        else {
            // return DEFAULT
            return MaisTableSortDirection.ASCENDING;
        }
    }
    get currentData() {
        return this.dataSnapshot || [];
    }
    get enumPages() {
        const rangeStart = 1;
        const rangeEnd = 1;
        const rangeSelected = 1;
        const pages = [];
        const pageNum = this.currentPageCount;
        const currentIndex = this.currentPageIndex;
        let isSkipping = false;
        for (let i = 0; i < pageNum; i++) {
            if (Math.abs(i - currentIndex) <= (rangeSelected) || i <= (rangeStart - 1) || i >= (pageNum - rangeEnd)) {
                isSkipping = false;
                pages.push(i);
            }
            else {
                if (!isSkipping) {
                    pages.push({
                        skip: true
                    });
                    isSkipping = true;
                }
            }
        }
        return pages;
    }
    get activeColumnsCount() {
        let output = this.visibleColumns.length;
        if (this.currentEnableSelection) {
            output++;
        }
        if (this.contextFilteringEnabledAndPossible) {
            output++;
        }
        if (this.rowExpansionEnabledAndPossible) {
            output++;
        }
        return output;
    }
    get noResults() {
        if (this.paginationMode === MaisTablePaginationMethod.CLIENT) {
            return !this.currentData.length;
        }
        else {
            return !this.dataSnapshot.length;
        }
    }
    get hideableColumns() {
        return this.columns.filter(c => MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
    }
    get visibleColumns() {
        return this.columns.filter(c => this.checkedColumnsForVisualization.indexOf(c) !== -1);
    }
    get currentContextFilters() {
        return this.contextFilters || [];
    }
    get currentHeaderActions() {
        return this.headerActions || [];
    }
    get currentActions() {
        return this.contextActions || [];
    }
    get hasActions() {
        return this.currentActions.length > 0;
    }
    get searchQueryNeedApply() {
        return (this.currentSearchQuery || '') !== (this.lastFetchedSearchQuery || '');
    }
    get searchQueryActive() {
        if (!this.currentSearchQuery) {
            return false;
        }
        if (!this.checkedColumnsForFiltering.length) {
            return false;
        }
        return true;
    }
    get searchQueryMalformed() {
        if (!this.currentSearchQuery) {
            return false;
        }
        if (!this.checkedColumnsForFiltering.length) {
            return true;
        }
        return false;
    }
    get filterableColumns() {
        return this.columns.filter(c => this.isFilterable(c));
    }
    get anyButtonActionsAllowed() {
        return !!this.contextActions.find(a => this.isContextActionAllowed(a));
    }
    get anyHeaderActionsAllowed() {
        return !!this.headerActions.find(a => this.isHeaderActionAllowed(a));
    }
    get configColumnVisibilityShowFixedColumns() {
        var _a;
        return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.showFixedColumns;
    }
    get configFilteringShowUnfilterableColumns() {
        var _a;
        return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.showUnfilterableColumns;
    }
    get acceptDrop() {
        return this.currentEnableDrop;
    }
    get acceptDrag() {
        return this.currentEnableDrag;
    }
    get referencedTable() {
        const v = this.getReferencedTable();
        return v;
    }
    getReferencedTable() {
        if (this.dropConnectedTo && this.dropConnectedTo.length) {
            const connectedToList = (Array.isArray(this.dropConnectedTo)) ? this.dropConnectedTo : [this.dropConnectedTo];
            const found = [];
            for (const connectedToToken of connectedToList) {
                const registeredList = this.registry.get(connectedToToken);
                if (registeredList && registeredList.length) {
                    for (const regComp of registeredList) {
                        found.push(regComp);
                    }
                }
            }
            if (found.length < 1) {
                return null;
            }
            else if (found.length > 1) {
                this.logger.error('MULTIPLE TABLE REFERENCED STILL ACTIVE', found);
                return null;
            }
            else {
                return found[0].component;
            }
        }
        else {
            return this;
        }
    }
    // user actions
    handleItemDragged(event, from, to) {
        this.logger.debug('item dragged from table ' + this.currentTableId);
        this.itemDragged.emit({
            item: event.item.data,
            event,
            fromDataSnapshot: from.getDataSnapshot(),
            toDataSnapshot: to.getDataSnapshot(),
            fromComponent: from,
            toComponent: to
        });
    }
    handleItemDropped(event) {
        this.logger.debug('DROP EVENT FROM TABLE ' + this.currentTableId, event);
        const droppedSource = this.registry.getSingle(event.previousContainer.id);
        const droppedTarget = this.registry.getSingle(event.container.id);
        if (!droppedTarget) {
            this.logger.warn('NO DROP TARGET FOUND');
            return;
        }
        if (!droppedTarget.acceptDrop) {
            this.logger.debug('skipping drop on container with acceptDrop = false');
            return;
        }
        if (droppedSource) {
            droppedSource.handleItemDragged(event, droppedSource, droppedTarget);
        }
        this.logger.debug('item dropped on table ' + droppedTarget.currentTableId);
        this.itemDropped.emit({
            item: event.item.data,
            event,
            fromDataSnapshot: droppedSource ? droppedSource.getDataSnapshot() : null,
            toDataSnapshot: droppedTarget.getDataSnapshot(),
            fromComponent: droppedSource,
            toComponent: droppedTarget
        });
        // I'm sorry
        this.cdr.detectChanges();
        this.ngZone.run(() => {
            this.dataSnapshot = this.dataSnapshot.map(o => o);
            this.cdr.detectChanges();
            setTimeout(() => this.forceReRender = true);
            setTimeout(() => this.forceReRender = false);
            this.handleMatTableDataSnapshotChanged();
        });
    }
    applySelectedFilter() {
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    clickOnRowExpansion(item) {
        if (!this.rowExpansionEnabledAndPossible) {
            return;
        }
        if (!this.isExpandable(item)) {
            return;
        }
        if (this.isExpanded(item)) {
            this.expandedItems.splice(this.expandedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultipleRowExpansion) {
                this.expandedItems = [];
            }
            this.expandedItems.push(item);
        }
        this.handleMatTableDataSnapshotChanged();
        this.statusChanged();
    }
    clickOnContextAction(action) {
        if (!this.currentEnableContextActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isContextActionAllowed(action)) {
            return;
        }
        const dispatchContext = {
            action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    }
    clickOnPageSize(pageSize) {
        this.selectedPageSize = pageSize;
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    clickOnHeaderAction(action) {
        if (!this.currentEnableHeaderActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isHeaderActionAllowed(action)) {
            return;
        }
        const dispatchContext = {
            action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    }
    searchQueryFocusOut() {
        var _a;
        const comp = this;
        setTimeout(() => {
            var _a, _b;
            if ((comp.selectedSearchQuery || '') !== (comp.lastFetchedSearchQuery || '')) {
                if (comp.selectedSearchQuery) {
                    if ((_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadOnQueryChange) {
                        comp.applySelectedFilter();
                    }
                    else {
                        // comp.selectedSearchQuery = comp.lastFetchedSearchQuery;
                    }
                }
                else {
                    if ((_b = this.configurationService.getConfiguration().filtering) === null || _b === void 0 ? void 0 : _b.autoReloadOnQueryClear) {
                        comp.applySelectedFilter();
                    }
                }
            }
        }, (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadTimeout);
    }
    switchToPage(pageIndex, context) {
        if (this.currentPageIndex === pageIndex) {
            return;
        }
        if (!context) {
            context = { reason: MaisTableReloadReason.USER };
        }
        this.setPage(pageIndex);
        this.statusChanged();
        this.reload(context);
    }
    clickOnColumn(column) {
        if (!MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) {
            return;
        }
        const sortColumn = this.currentSortColumn;
        const sortDirection = this.currentSortDirection;
        if (sortColumn && sortColumn.name === column.name) {
            this.selectedSortDirection = (sortDirection === MaisTableSortDirection.DESCENDING ?
                MaisTableSortDirection.ASCENDING : MaisTableSortDirection.DESCENDING);
        }
        else {
            this.selectedSortColumn = column;
            this.selectedSortDirection = MaisTableSortDirection.ASCENDING;
        }
        this.emitSortChanged();
        // forza ritorno alla prima pagina
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    // parsing functions
    getItemIdentifier(item) {
        if (this.itemTrackingEnabledAndPossible) {
            if (this.itemIdentifier.extract) {
                return this.itemIdentifier.extract(item);
            }
            else {
                return MaisTableFormatterHelper.getPropertyValue(item, this.itemIdentifier);
            }
        }
        else {
            return null;
        }
    }
    getCurrentLocale() {
        return this.translateService.getDefaultLang();
    }
    extractValue(row, column) {
        return MaisTableFormatterHelper.extractValue(row, column, this.getCurrentLocale());
    }
    resolveLabel(column) {
        return this.resolveLabelOrFixed(column.labelKey, column.label);
    }
    resolveLabelOrFixed(key, fixed) {
        if (key) {
            return this.translateService.instant(key);
        }
        else if (fixed) {
            return fixed;
        }
        else {
            return null;
        }
    }
    isCurrentSortingColumn(column, direction = null) {
        const sortColumn = this.currentSortColumn;
        if (sortColumn && column && column.name === sortColumn.name) {
            if (!direction) {
                return true;
            }
            return direction === this.currentSortDirection;
        }
        else {
            return false;
        }
    }
    isDefaultVisibleColumn(column) {
        return MaisTableFormatterHelper.isDefaultVisibleColumn(column, this.configurationService.getConfiguration());
    }
    isDefaultFilterColumn(column) {
        return MaisTableFormatterHelper.isDefaultFilterColumn(column, this.configurationService.getConfiguration());
    }
    isExpandable(row) {
        return this.currentEnableRowExpansion &&
            (!this.expandableStatusProvider || this.expandableStatusProvider(row, this.statusSnapshot));
    }
    isHideable(column) {
        return MaisTableFormatterHelper.isHideable(column, this.configurationService.getConfiguration());
    }
    isSortable(column) {
        return MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration());
    }
    isFilterable(column) {
        return MaisTableFormatterHelper.isFilterable(column, this.configurationService.getConfiguration());
    }
    hasLabel(column) {
        return MaisTableFormatterHelper.hasLabel(column, this.configurationService.getConfiguration());
    }
    isLabelVisibleInTable(column) {
        return MaisTableFormatterHelper.isLabelVisibleInTable(column, this.configurationService.getConfiguration());
    }
    isContextActionAllowed(action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultContextActionActivationCondition);
    }
    isHeaderActionAllowed(action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultHeaderActionActivationCondition);
    }
    isActionAllowed(action, def) {
        const actCond = action.activationCondition || def;
        if (actCond === MaisTableActionActivationCondition.ALWAYS) {
            return true;
        }
        else if (actCond === MaisTableActionActivationCondition.NEVER) {
            return false;
        }
        else if (actCond === MaisTableActionActivationCondition.DYNAMIC) {
            // delega decisione a provider esterno
            if (!this.actionStatusProvider) {
                this.logger.error('Action with dynamic enabling but no actionStatusProvider provided');
                return false;
            }
            return this.actionStatusProvider(action, this.statusSnapshot || null);
        }
        const numSelected = this.checkedItems.length;
        if (actCond === MaisTableActionActivationCondition.SINGLE_SELECTION) {
            return numSelected === 1;
        }
        else if (actCond === MaisTableActionActivationCondition.MULTIPLE_SELECTION) {
            return numSelected > 0;
        }
        else if (actCond === MaisTableActionActivationCondition.NO_SELECTION) {
            return numSelected < 1;
        }
        else {
            throw new Error('Unknown action activation condition: ' + actCond);
        }
    }
    // gestione delle righe espanse
    isExpanded(item) {
        return this.expandedItems.indexOf(item) !== -1;
    }
    // checkbox select per items
    isChecked(item) {
        return this.checkedItems.indexOf(item) !== -1;
    }
    get allChecked() {
        const dataItems = this.currentData;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedItems.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyChecked() {
        const dataItems = this.currentData;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedItems.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noneChecked() {
        return !this.anyChecked;
    }
    toggleChecked(item) {
        if (this.isChecked(item)) {
            this.checkedItems.splice(this.checkedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultiSelect) {
                this.checkedItems = [];
            }
            this.checkedItems.push(item);
        }
        this.statusChanged();
        this.emitSelectionChanged();
    }
    toggleAllChecked() {
        if (!this.currentEnableSelectAll) {
            return;
        }
        if (!this.currentEnableMultiSelect) {
            return;
        }
        if (this.allChecked) {
            this.checkedItems = [];
        }
        else {
            this.checkedItems = [];
            for (const el of this.currentData) {
                this.checkedItems.push(el);
            }
        }
        this.statusChanged();
        this.emitSelectionChanged();
    }
    // checkbox select per filtering columns
    isColumnCheckedForFiltering(item) {
        return this.checkedColumnsForFiltering.indexOf(item) !== -1;
    }
    get allFilteringColumnsChecked() {
        const dataItems = this.filterableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForFiltering.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyFilteringColumnsChecked() {
        const dataItems = this.filterableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForFiltering.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noFilteringColumnshecked() {
        return !this.anyFilteringColumnsChecked;
    }
    toggleFilteringColumnChecked(item) {
        if (!this.currentEnableFiltering) {
            return;
        }
        if (!this.isFilterable(item)) {
            return;
        }
        if (this.isColumnCheckedForFiltering(item)) {
            this.checkedColumnsForFiltering.splice(this.checkedColumnsForFiltering.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForFiltering.push(item);
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    }
    toggleAllFilteringColumnsChecked() {
        if (!this.currentEnableFiltering) {
            return;
        }
        if (this.allFilteringColumnsChecked) {
            this.checkedColumnsForFiltering = [];
        }
        else {
            this.checkedColumnsForFiltering = [];
            for (const el of this.filterableColumns) {
                this.checkedColumnsForFiltering.push(el);
            }
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    }
    // checkbox select per visible columns
    isColumnCheckedForVisualization(item) {
        return this.checkedColumnsForVisualization.indexOf(item) !== -1;
    }
    get allVisibleColumnsChecked() {
        const dataItems = this.hideableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForVisualization.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyVisibleColumnsChecked() {
        const dataItems = this.hideableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForVisualization.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noVisibleColumnshecked() {
        return !this.anyVisibleColumnsChecked;
    }
    toggleVisibleColumnChecked(item) {
        if (!MaisTableFormatterHelper.isHideable(item, this.configurationService.getConfiguration()) || !this.currentEnableColumnsSelection) {
            return;
        }
        if (this.isColumnCheckedForVisualization(item)) {
            this.checkedColumnsForVisualization.splice(this.checkedColumnsForVisualization.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForVisualization.push(item);
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    }
    toggleAllVisibleColumnsChecked() {
        if (!this.currentEnableColumnsSelection) {
            return;
        }
        if (this.allVisibleColumnsChecked) {
            this.checkedColumnsForVisualization = this.columns.filter(c => !MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
        }
        else {
            this.checkedColumnsForVisualization = [];
            for (const el of this.columns) {
                this.checkedColumnsForVisualization.push(el);
            }
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    }
    // checkbox select per filtri custom
    isContextFilterChecked(item) {
        return this.checkedContextFilters.indexOf(item) !== -1;
    }
    get allContextFilterChecked() {
        const dataItems = this.currentContextFilters;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedContextFilters.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyContextFilterChecked() {
        const dataItems = this.currentContextFilters;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedContextFilters.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noContextFiltersChecked() {
        return !this.anyContextFilterChecked;
    }
    toggleContextFilterChecked(item) {
        if (!this.currentEnableContextFiltering) {
            return;
        }
        if (this.isContextFilterChecked(item)) {
            this.checkedContextFilters.splice(this.checkedContextFilters.indexOf(item), 1);
        }
        else {
            this.checkedContextFilters.push(item);
            if (item.group) {
                this.checkedContextFilters = this.checkedContextFilters.filter(o => {
                    return (o.name === item.name) || (!o.group) || (o.group !== item.group);
                });
            }
        }
        this.emitContextFiltersSelectionChanged();
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    buildStatusSnapshot() {
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
            visibleColumns: this.visibleColumns.map(c => c.name),
            contextFilters: this.checkedContextFilters.map(c => c.name),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItems: !this.currentEnableSelection ? [] : this.checkedItems.map(v => v),
            expandedItems: !this.currentEnableRowExpansion ? [] : this.expandedItems.map(v => v)
        };
    }
    buildPersistableStatusSnapshot() {
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
            visibleColumns: this.visibleColumns.map(c => c.name),
            contextFilters: this.checkedContextFilters.map(c => c.name),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.checkedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v),
            expandedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.expandedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v)
        };
    }
    statusChanged() {
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        this.logger.trace('table status changed', this.persistableStatusSnapshot);
        this.emitStatusChanged();
    }
    persistStatusToStore() {
        if (!this.statusSnapshot) {
            return throwError('No status to save');
        }
        return new Observable(subscriber => {
            if (this.storePersistenceEnabledAndPossible) {
                this.logger.trace('passing status to persistence store');
                if (this.storeAdapter.save) {
                    this.storeAdapter.save({
                        status: this.persistableStatusSnapshot
                    }).subscribe(result => {
                        this.logger.trace('saved status snapshot to persistence store');
                        subscriber.next();
                        subscriber.complete();
                    }, failure => {
                        this.logger.warn('failed to save status snapshot to persistence store', failure);
                        subscriber.error(failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No save function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next();
            subscriber.complete();
        });
    }
    loadStatusFromStore() {
        return new Observable(subscriber => {
            if (this.storePersistenceEnabledAndPossible) {
                this.logger.trace('fetching status from persistence store');
                if (this.storeAdapter.load) {
                    this.storeAdapter.load().subscribe(result => {
                        this.logger.trace('fetched status snapshot from persistence store');
                        subscriber.next(result);
                        subscriber.complete();
                    }, failure => {
                        subscriber.error(failure);
                        this.logger.warn('failed to fetch status snapshot from persistence store', failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No load function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next(null);
            subscriber.complete();
        });
    }
    get dragInProgress() {
        return ($('.cdk-drag-preview:visible').length +
            $('.cdk-drag-placeholder:visible').length +
            $('.cdk-drop-list-dragging:visible').length +
            $('.cdk-drop-list-receiving:visible').length) > 0;
    }
    // event emitters
    emitSelectionChanged() {
        this.selectionChange.emit(this.checkedItems);
    }
    emitSortChanged() {
        this.sortChange.emit({
            column: this.currentSortColumn,
            direction: this.currentSortDirection
        });
    }
    emitPageChanged() {
        this.pageChange.emit(this.selectedPageIndex);
    }
    emitFilteringColumnsSelectionChanged() {
        this.filteringColumnsChange.emit(this.checkedColumnsForFiltering);
    }
    emitVisibleColumnsSelectionChanged() {
        this.visibleColumnsChange.emit(this.checkedColumnsForVisualization);
    }
    emitContextFiltersSelectionChanged() {
        this.contextFiltersChange.emit(this.checkedContextFilters);
    }
    emitStatusChanged() {
        if (this.statusSnapshot) {
            this.statusChange.emit(this.statusSnapshot);
        }
    }
    // MatTable adapters
    get matTableData() {
        return this.dataSnapshot;
    }
    get matTableVisibleColumnDefs() {
        const o = [];
        if (this.rowExpansionEnabledAndPossible) {
            o.push('internal___col_row_expansion');
        }
        if (this.currentEnableSelection) {
            o.push('internal___col_row_selection');
        }
        if (this.contextFilteringEnabledAndPossible) {
            o.push('internal___col_context_filtering');
        }
        this.visibleColumns.map(c => c.name).forEach(c => o.push(c));
        return o;
    }
    get matTableVisibleColumns() {
        return this.visibleColumns;
    }
    handleMatTableDataSnapshotChanged() {
        const rows = [];
        this.dataSnapshot.forEach(element => {
            const detailRow = Object.assign({ ___detailRowContent: true, ___parentRow: element }, element);
            element.___detailRow = detailRow;
            rows.push(element, detailRow);
        });
        // TODO : BUILD STATIC ROW WITH DATA EXTRACTORS!
        this.logger.trace('emitting static rows for mat-table ' + this.currentTableId, rows);
        setTimeout(() => {
            var _a;
            (_a = this.matTableDataObservable) === null || _a === void 0 ? void 0 : _a.next(rows);
        });
    }
    get compatibilityModeForMainTable() {
        if (!this.isIE) {
            return false;
        }
        else if (this.currentEnableDrop) {
            return true;
        }
        else {
            return true;
        }
    }
    get compatibilityModeForDropDowns() {
        return true;
        if (!this.isIE) {
            return false;
        }
        else {
            return true;
        }
    }
}
MaisTableComponent.counter = 0;
/** @nocollapse */ MaisTableComponent.ɵfac = function MaisTableComponent_Factory(t) { return new (t || MaisTableComponent)(i0.ɵɵdirectiveInject(i1.TranslateService), i0.ɵɵdirectiveInject(i2.MaisTableService), i0.ɵɵdirectiveInject(i2.MaisTableRegistryService), i0.ɵɵdirectiveInject(i0.ChangeDetectorRef), i0.ɵɵdirectiveInject(i0.NgZone)); };
/** @nocollapse */ MaisTableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MaisTableComponent, selectors: [["mais-table"]], contentQueries: function MaisTableComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
        i0.ɵɵcontentQuery(dirIndex, _c0, true);
        i0.ɵɵcontentQuery(dirIndex, _c1, true);
        i0.ɵɵcontentQuery(dirIndex, _c2, true);
        i0.ɵɵcontentQuery(dirIndex, _c3, true);
        i0.ɵɵcontentQuery(dirIndex, _c4, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.cellTemplate = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.actionsCaptionTemplate = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.rowDetailTemplate = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.dragTemplate = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.dropTemplate = _t.first);
    } }, viewQuery: function MaisTableComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c5, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.table = _t.first);
    } }, inputs: { data: "data", dataProvider: "dataProvider", columns: "columns", defaultSortingColumn: "defaultSortingColumn", defaultSortingDirection: "defaultSortingDirection", defaultPageSize: "defaultPageSize", possiblePageSize: "possiblePageSize", tableId: "tableId", dropConnectedTo: "dropConnectedTo", paginationMode: "paginationMode", enablePagination: "enablePagination", enableSelection: "enableSelection", enableContextFiltering: "enableContextFiltering", enableSelectAll: "enableSelectAll", enableMultiSelect: "enableMultiSelect", enableFiltering: "enableFiltering", enableColumnsSelection: "enableColumnsSelection", enableContextActions: "enableContextActions", enableHeaderActions: "enableHeaderActions", enableRowExpansion: "enableRowExpansion", enablePageSizeSelect: "enablePageSizeSelect", enableMultipleRowExpansion: "enableMultipleRowExpansion", enableItemTracking: "enableItemTracking", enableStorePersistence: "enableStorePersistence", enableDrag: "enableDrag", enableDrop: "enableDrop", contextActions: "contextActions", headerActions: "headerActions", contextFilters: "contextFilters", contextFilteringPrompt: "contextFilteringPrompt", actionStatusProvider: "actionStatusProvider", expandableStatusProvider: "expandableStatusProvider", storeAdapter: "storeAdapter", itemIdentifier: "itemIdentifier", refreshStrategy: "refreshStrategy", refreshInterval: "refreshInterval", refreshEmitter: "refreshEmitter", refreshIntervalInBackground: "refreshIntervalInBackground", refreshOnPushInBackground: "refreshOnPushInBackground" }, outputs: { pageChange: "pageChange", sortChange: "sortChange", selectionChange: "selectionChange", filteringColumnsChange: "filteringColumnsChange", visibleColumnsChange: "visibleColumnsChange", contextFiltersChange: "contextFiltersChange", statusChange: "statusChange", itemDragged: "itemDragged", itemDropped: "itemDropped", action: "action" }, features: [i0.ɵɵNgOnChangesFeature()], decls: 5, vars: 4, consts: [[1, "mais-table"], ["class", "container-full px-4 py-5 table-actions table-actions--header", 4, "ngIf"], [4, "ngIf"], ["class", "container-full px-4 py-4 table-actions table-actions--footer", 4, "ngIf"], [1, "container-full", "px-4", "py-5", "table-actions", "table-actions--header"], [1, "row"], ["class", "col-4", 4, "ngIf"], ["class", "col-3", 4, "ngIf"], [1, "col-5", "text-right"], [3, "ngTemplateOutlet", "ngTemplateOutletContext"], ["class", "btn-group", "ngbDropdown", "", 4, "ngIf"], [1, "col-4"], [3, "submit"], [1, "input-group"], ["type", "text", "name", "selectedSearchQuery", "aria-label", "Search", 1, "form-control", 3, "placeholder", "ngModel", "ngModelChange", "focusout"], ["ngbDropdown", "", 1, "input-group-append", 3, "autoClose"], ["type", "submit", 1, "btn", 3, "click"], [1, "fas", "fa-search"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 1, "dropdown-item"], [1, "dropdown-header"], [1, "form-group", "clickable"], [1, "form-check"], ["type", "checkbox", 3, "checked", "change"], [3, "click"], [1, "dropdown-divider"], [4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", "class", "dropdown-item", 3, "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "change", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled"], ["ngbDropdownItem", "", 3, "click"], [3, "icon", 4, "ngIf"], [3, "icon"], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngIf"], [1, "col-3"], ["ngbDropdown", "", 1, "btn-group", 3, "autoClose"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"], ["ngbDropdownItem", "", 1, "dropdown-header"], ["type", "button", 3, "class", "disabled", "click", 4, "ngFor", "ngForOf"], ["type", "button", 3, "disabled", "click"], ["ngbDropdown", "", 1, "btn-group"], ["ngbDropdownToggle", "", 1, "btn", "btn-light", "dropdown-toggle", 3, "disabled"], ["ngbDropdownMenu", ""], ["ngbDropdownItem", ""], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "table-responsive"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], [1, "d-none"], [1, ""], ["scope", "row", "class", "small-as-possible", 4, "ngIf"], ["scope", "row", "style", "width: 1%;", "ngbDropdown", "", 3, "border-primary", "autoClose", 4, "ngIf"], ["scope", "row", 1, "small-as-possible"], ["scope", "row", "ngbDropdown", "", 2, "width", "1%", 3, "autoClose"], ["ngbDropdownToggle", "", 1, "form-check", "clickable"], [1, "dropdown", "show", "d-inline-block", "float-right"], ["class", "dropdown-item", "ngbDropdownItem", "", 4, "ngFor", "ngForOf"], [1, "clickable", 3, "click"], ["scope", "col", 3, "click"], ["type", "button", 1, "order-btn", "btn", "btn-none", 3, "click"], [1, "fas", "fa-long-arrow-alt-up"], [1, "start-row-hook", "d-none"], ["cdkDrag", "", 1, "data-row", 3, "cdkDragData", "cdkDragDisabled"], ["scope", "row", 4, "ngIf"], ["class", "drop-container", 3, "hidden", 4, "cdkDragPlaceholder"], ["class", "drag-container", 4, "cdkDragPreview"], ["class", "detail-row", 4, "ngIf"], ["scope", "row"], ["class", "clickable", 3, "icon", "click", 4, "ngIf"], [1, "clickable", 3, "icon", "click"], [1, "drop-container", 3, "hidden"], [1, "drop-cell"], [1, "drop-container"], [1, "drag-container"], [1, "detail-row"], [1, "row-expansion-container"], [1, "message-row", "no-results-row"], [1, "text-center"], [1, "message-row", "fetching-row"], ["class", "table-responsive", 4, "ngIf"], ["class", "table table-dark table-hover", "cdkDropList", "", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "nodrop", "acceptdrop", "cdkDropListDropped", 4, "ngIf"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], ["renderedMatTable", ""], [3, "matColumnDef"], ["class", "maissize-min", 4, "matHeaderCellDef"], ["class", "maissize-min", 4, "matCellDef"], [4, "matCellDef"], [3, "matColumnDef", 4, "ngFor", "ngForOf"], [4, "matHeaderRowDef"], ["class", "element-row data-row", "cdkDrag", "", 3, "expanded", "row-selected", "cdkDragData", "cdkDragDisabled", 4, "matRowDef", "matRowDefColumns"], ["style", "overflow: hidden", 4, "matRowDef", "matRowDefColumns", "matRowDefWhen"], [1, "maissize-min"], ["ngbDropdown", "", 3, "autoClose", "container"], [3, "class", 4, "matHeaderCellDef"], [3, "class", 4, "matCellDef"], ["cdkDrag", "", 1, "element-row", "data-row", 3, "cdkDragData", "cdkDragDisabled"], [2, "overflow", "hidden"], [1, "alert", "alert-primary", "text-center"], [1, "container-full", "px-4", "py-4", "table-actions", "table-actions--footer"], [1, "col-7"], [1, "inline-block", "inline-block-bottom"], [1, "pagination-caption"], ["aria-label", "pagination", 1, "pagination-links"], [1, "pagination"], [1, "page-item", "clickable"], [1, "page-link", 3, "click"], [1, "inline-block", "inline-block-bottom", "pl-2"], ["ngbDropdown", "", 4, "ngIf"], ["class", "page-item clickable", 3, "active", 4, "ngIf"], ["class", "page-item", 4, "ngIf"], ["class", "sr-only", 4, "ngIf"], [1, "sr-only"], [1, "page-item"], [1, "page-link"], ["ngbDropdown", ""], ["ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"]], template: function MaisTableComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, MaisTableComponent_div_1_Template, 12, 9, "div", 1);
        i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_Template, 15, 19, "ng-container", 2);
        i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_Template, 2, 1, "ng-container", 2);
        i0.ɵɵtemplate(4, MaisTableComponent_div_4_Template, 10, 6, "div", 3);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.columnSelectionPossibleAndAllowed || ctx.filteringPossibleAndAllowed || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.compatibilityModeForMainTable);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.compatibilityModeForMainTable);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.currentEnablePagination || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
    } }, directives: [i3.NgIf, i3.NgTemplateOutlet, i4.NgbDropdown, i4.NgbDropdownToggle, i4.NgbDropdownMenu, i4.NgbDropdownItem, i3.NgForOf, i5.FaIconComponent, i6.CdkDropList, i6.CdkDrag, i6.CdkDragPlaceholder, i6.CdkDragPreview, i7.MatTable, i7.MatColumnDef, i7.MatHeaderCellDef, i7.MatCellDef, i7.MatHeaderRowDef, i7.MatRowDef, i7.MatHeaderCell, i7.MatCell, i7.MatHeaderRow, i7.MatRow], pipes: [i1.TranslatePipe], styles: [".message-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding:2em}.clickable[_ngcontent-%COMP%]{cursor:pointer}.inline-block[_ngcontent-%COMP%]{display:inline-block}.inline-block-bottom[_ngcontent-%COMP%]{vertical-align:bottom}.small-as-possible[_ngcontent-%COMP%]{width:1%}.detail-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:2em}.row-selected[_ngcontent-%COMP%]{background-color:#bc001622}.row-selected[_ngcontent-%COMP%]:hover{background-color:#77464755!important}.drag-container[_ngcontent-%COMP%], .drop-container[_ngcontent-%COMP%]{min-height:60px;padding:1em;background:#77464755;border:3px dotted #25161755}.nodrop[_ngcontent-%COMP%]   .drop-cell[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-row[_ngcontent-%COMP%]{display:none!important}.cdk-drop-list-dragging.acceptdrop[_ngcontent-%COMP%]   td[_ngcontent-%COMP%], th[_ngcontent-%COMP%]{border:none}.mat-header-cell[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{padding:0;margin:0}.mat-cell.maissize-min[_ngcontent-%COMP%], .mat-cell.maissize-xxs[_ngcontent-%COMP%], .mat-header-cell.maissize-min[_ngcontent-%COMP%], .mat-header-cell.maissize-xxs[_ngcontent-%COMP%]{flex:0 0 2%;min-width:3em}.mat-cell.maissize-xs[_ngcontent-%COMP%], .mat-header-cell.maissize-xs[_ngcontent-%COMP%]{flex:0 0 3%;min-width:5em}.mat-cell.maissize-s[_ngcontent-%COMP%], .mat-header-cell.maissize-s[_ngcontent-%COMP%]{flex:0 0 3%;min-width:8em}.mat-cell.maissize-m[_ngcontent-%COMP%], .mat-header-cell.maissize-m[_ngcontent-%COMP%]{flex:0 0 3%;min-width:12em}.mat-cell.maissize-l[_ngcontent-%COMP%], .mat-header-cell.maissize-l[_ngcontent-%COMP%]{flex:0 0 3%;min-width:18em}.mat-cell.maissize-xl[_ngcontent-%COMP%], .mat-header-cell.maissize-xl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:25em}.mat-cell.maissize-xxl[_ngcontent-%COMP%], .mat-header-cell.maissize-xxl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:30em}.mat-table[_ngcontent-%COMP%]{border-collapse:collapse}.mat-header-row[_ngcontent-%COMP%], .mat-row[_ngcontent-%COMP%], .mat-table[_ngcontent-%COMP%]{border-color:gray}", "mat-table[_ngcontent-%COMP%]{display:block}mat-header-row[_ngcontent-%COMP%]{min-height:56px}mat-footer-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{min-height:48px}mat-footer-row[_ngcontent-%COMP%], mat-header-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{display:flex;border-width:0 0 1px;border-style:solid;align-items:center;box-sizing:border-box}mat-footer-row[_ngcontent-%COMP%]::after, mat-header-row[_ngcontent-%COMP%]::after, mat-row[_ngcontent-%COMP%]::after{display:inline-block;min-height:inherit;content:\"\"}mat-cell[_ngcontent-%COMP%]:first-of-type, mat-footer-cell[_ngcontent-%COMP%]:first-of-type, mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}mat-cell[_ngcontent-%COMP%]:last-of-type, mat-footer-cell[_ngcontent-%COMP%]:last-of-type, mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}mat-cell[_ngcontent-%COMP%], mat-footer-cell[_ngcontent-%COMP%], mat-header-cell[_ngcontent-%COMP%]{flex:1;display:flex;align-items:center;overflow:hidden;word-wrap:break-word;min-height:inherit}table.mat-table[_ngcontent-%COMP%]{border-spacing:0}tr.mat-header-row[_ngcontent-%COMP%]{height:56px}tr.mat-footer-row[_ngcontent-%COMP%], tr.mat-row[_ngcontent-%COMP%]{height:48px}th.mat-header-cell[_ngcontent-%COMP%]{text-align:left}[dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]{text-align:right}td.mat-cell[_ngcontent-%COMP%], td.mat-footer-cell[_ngcontent-%COMP%], th.mat-header-cell[_ngcontent-%COMP%]{padding:0;border-bottom-width:1px;border-bottom-style:solid}td.mat-cell[_ngcontent-%COMP%]:first-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}td.mat-cell[_ngcontent-%COMP%]:last-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}"], data: { animation: [
            trigger('detailExpand', [
                state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                state('expanded', style({ height: '*', visibility: 'visible' })),
                transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
            ]),
        ] } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableComponent, [{
        type: Component,
        args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mais-table',
                templateUrl: './mais-table.component.html',
                styleUrls: ['./mais-table.component.scss', './mais-table-porting.css'],
                animations: [
                    trigger('detailExpand', [
                        state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                        state('expanded', style({ height: '*', visibility: 'visible' })),
                        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                    ]),
                ],
            }]
    }], function () { return [{ type: i1.TranslateService }, { type: i2.MaisTableService }, { type: i2.MaisTableRegistryService }, { type: i0.ChangeDetectorRef }, { type: i0.NgZone }]; }, { data: [{
            type: Input
        }], dataProvider: [{
            type: Input
        }], columns: [{
            type: Input
        }], defaultSortingColumn: [{
            type: Input
        }], defaultSortingDirection: [{
            type: Input
        }], defaultPageSize: [{
            type: Input
        }], possiblePageSize: [{
            type: Input
        }], tableId: [{
            type: Input
        }], dropConnectedTo: [{
            type: Input
        }], paginationMode: [{
            type: Input
        }], enablePagination: [{
            type: Input
        }], enableSelection: [{
            type: Input
        }], enableContextFiltering: [{
            type: Input
        }], enableSelectAll: [{
            type: Input
        }], enableMultiSelect: [{
            type: Input
        }], enableFiltering: [{
            type: Input
        }], enableColumnsSelection: [{
            type: Input
        }], enableContextActions: [{
            type: Input
        }], enableHeaderActions: [{
            type: Input
        }], enableRowExpansion: [{
            type: Input
        }], enablePageSizeSelect: [{
            type: Input
        }], enableMultipleRowExpansion: [{
            type: Input
        }], enableItemTracking: [{
            type: Input
        }], enableStorePersistence: [{
            type: Input
        }], enableDrag: [{
            type: Input
        }], enableDrop: [{
            type: Input
        }], contextActions: [{
            type: Input
        }], headerActions: [{
            type: Input
        }], contextFilters: [{
            type: Input
        }], contextFilteringPrompt: [{
            type: Input
        }], actionStatusProvider: [{
            type: Input
        }], expandableStatusProvider: [{
            type: Input
        }], storeAdapter: [{
            type: Input
        }], itemIdentifier: [{
            type: Input
        }], refreshStrategy: [{
            type: Input
        }], refreshInterval: [{
            type: Input
        }], refreshEmitter: [{
            type: Input
        }], refreshIntervalInBackground: [{
            type: Input
        }], refreshOnPushInBackground: [{
            type: Input
        }], pageChange: [{
            type: Output
        }], sortChange: [{
            type: Output
        }], selectionChange: [{
            type: Output
        }], filteringColumnsChange: [{
            type: Output
        }], visibleColumnsChange: [{
            type: Output
        }], contextFiltersChange: [{
            type: Output
        }], statusChange: [{
            type: Output
        }], itemDragged: [{
            type: Output
        }], itemDropped: [{
            type: Output
        }], action: [{
            type: Output
        }], cellTemplate: [{
            type: ContentChild,
            args: ['cellTemplate']
        }], actionsCaptionTemplate: [{
            type: ContentChild,
            args: ['actionsCaptionTemplate']
        }], rowDetailTemplate: [{
            type: ContentChild,
            args: ['rowDetailTemplate']
        }], dragTemplate: [{
            type: ContentChild,
            args: ['dragTemplate']
        }], dropTemplate: [{
            type: ContentChild,
            args: ['dropTemplate']
        }], table: [{
            type: ViewChild,
            args: ['#renderedMatTable']
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9tYWlzLXRhYmxlLmNvbXBvbmVudC50cyIsImxpYi9tYWlzLXRhYmxlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsS0FBSyxFQUNMLE1BQU0sRUFDTixZQUFZLEVBQ1osWUFBWSxFQUNaLFdBQVcsRUFDWCxpQkFBaUIsRUFDakIsTUFBTSxFQUlOLFNBQVMsRUFDVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQ0wsc0JBQXNCLEVBSXRCLHlCQUF5QixFQUl6QixrQ0FBa0MsRUFPbEMsd0JBQXdCLEVBR3hCLHFCQUFxQixFQUV0QixNQUFNLFNBQVMsQ0FBQztBQUNqQixPQUFPLEVBQ0wsd0JBQXdCLEVBQ3hCLHVCQUF1QixFQUN2Qix3QkFBd0IsRUFDeEIsc0JBQXNCLEVBQ3RCLGVBQWUsRUFDaEIsTUFBTSxTQUFTLENBQUM7QUFDakIsT0FBTyxFQUFFLHdCQUF3QixFQUFFLGdCQUFnQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBRXhFLE9BQU8sRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFjLFVBQVUsRUFBZ0IsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM5QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUV2RCxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUN2QixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0seUJBQXlCLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDTTdCLGlDQUVBO0lBREUsbVVBQWdELHdCQUF3QixJQUFFO0lBRDVFLGlCQUVBOzs7O0lBRnVCLHlFQUErQzs7O0lBRXRFLDRCQUNBOztJQUR1QiwrQkFBaUIsa0JBQUE7Ozs7SUFMOUMsa0NBQ0U7SUFBQSwrQkFDRTtJQUFBLCtCQUNFO0lBQUEsNEhBRUE7SUFBQSw0SEFDQTtJQUFBLGdDQUNFO0lBREssdVRBQStDLHdCQUF3QixJQUFFO0lBQzlFLFlBQ0Y7SUFBQSxpQkFBTztJQUNULGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBUzs7OztJQVhxQyw2REFBd0M7SUFHVCxlQUE0QjtJQUE1Qix1REFBNEI7SUFFMUQsZUFBNkI7SUFBN0Isd0RBQTZCO0lBRXBFLGVBQ0Y7SUFERSxpRUFDRjs7O0lBVE4sNkJBQ0E7SUFBQSxxSEFDRTtJQVdGLDBCQUFlOzs7O0lBWndFLGVBQTRGO0lBQTVGLDJJQUE0Rjs7OztJQWxDdkwsNkJBQ0E7SUFBQSwrQkFDRTtJQUFBLGtDQU1HO0lBRGEsd05BQStCO0lBQzVDLHdCQUE2QjtJQUNoQyxpQkFBUztJQUNULDZCQU1TO0lBQ1QsK0JBQ0U7SUFBQSwrQkFDRTtJQUFBLDhCQUE0QjtJQUFBLFlBQWdFOztJQUFBLGlCQUFLO0lBQ25HLGlCQUFNO0lBQ04sbUNBQ0U7SUFBQSxnQ0FDRTtJQUFBLGdDQUNFO0lBQUEsa0NBRUE7SUFERSwyT0FBOEMsd0JBQXdCLElBQUU7SUFEMUUsaUJBRUE7SUFBQSxpQ0FDRTtJQURLLHdPQUE2Qyx3QkFBd0IsSUFBRTtJQUM1RSxhQUNGOztJQUFBLGlCQUFPO0lBQ1QsaUJBQU07SUFDUixpQkFBTTtJQUNOLDJCQUFvQztJQUN0QyxpQkFBUztJQUNULG1IQUNBO0lBYUYsaUJBQU07SUFDUixpQkFBTTtJQUNOLDBCQUFlOzs7SUFoRDZCLGVBQXVCO0lBQXZCLHFDQUF1QjtJQUUvRCxlQUErRDtJQUEvRCx3RkFBK0QsMEVBQUEsOEVBQUEsZ0RBQUE7SUFRL0QsZUFBbUU7SUFBbkUsNEZBQW1FLGdEQUFBLGlGQUFBLGdGQUFBO0lBUXJDLGVBQWdFO0lBQWhFLDJGQUFnRTtJQUtqRSxlQUFzQztJQUF0Qyw0REFBc0M7SUFHM0QsZUFDRjtJQURFLDJHQUNGO0lBS1EsZUFBOEI7SUFBOUIseUNBQThCOzs7SUF5QzFDLDhCQUE4RTs7SUFBckUscUNBQXVCOzs7SUFDaEMsOEJBQXlFOztJQUFoRSwrQkFBaUI7OztJQU8xQiw4QkFBK0c7O0lBQXRHLHFDQUF1Qjs7O0lBQ2hDLDhCQUEwRzs7SUFBakcsK0JBQWlCOzs7SUFDMUIsOEJBQW1FOztJQUExRCwrQkFBaUI7Ozs7SUFKNUIsa0NBRUU7SUFGK0QscVRBQThDO0lBRTdHLGdJQUFxRztJQUNyRyxnSUFBZ0c7SUFDaEcsZ0lBQXlEO0lBQ3pELFlBQ0Y7SUFBQSxpQkFBUzs7OztJQU5lLDZEQUF3QztJQUU3QixlQUFtRTtJQUFuRSwwR0FBbUU7SUFDekUsZUFBb0U7SUFBcEUsMkdBQW9FO0lBQ3BFLGVBQTZCO0lBQTdCLHdEQUE2QjtJQUN4RCxlQUNGO0lBREUsaUVBQ0Y7OztJQVBBLDZCQUNBO0lBQUEscUhBRUU7SUFLRiwwQkFBZTs7OztJQU5iLGVBQTRGO0lBQTVGLDJJQUE0Rjs7OztJQTdCbEcsNkJBQ0E7SUFBQSwrQkFDRTtJQUFBLGtDQU1HO0lBRGEsd05BQStCO0lBQzVDLHdCQUE2QjtJQUNoQyxpQkFBUztJQUNULDZCQU1TO0lBQ1QsK0JBQ0U7SUFBQSwrQkFDRTtJQUFBLDhCQUE0QjtJQUFBLFlBQWdFOztJQUFBLGlCQUFLO0lBQ25HLGlCQUFNO0lBQ04sbUNBQ0U7SUFEc0Isc09BQTRDO0lBQ2xFLHlHQUFvRTtJQUNwRSx5R0FBK0Q7SUFDL0QsYUFDQTs7SUFBQSwyQkFBb0M7SUFDdEMsaUJBQVM7SUFDVCxtSEFDQTtJQVFGLGlCQUFNO0lBQ1IsaUJBQU07SUFDTiwwQkFBZTs7O0lBckM2QixlQUF1QjtJQUF2QixxQ0FBdUI7SUFFL0QsZUFBK0Q7SUFBL0Qsd0ZBQStELDBFQUFBLDhFQUFBLGdEQUFBO0lBUS9ELGVBQW1FO0lBQW5FLDRGQUFtRSxnREFBQSxpRkFBQSxnRkFBQTtJQVFyQyxlQUFnRTtJQUFoRSwyRkFBZ0U7SUFHM0QsZUFBa0M7SUFBbEMseURBQWtDO0lBQ3hDLGVBQW1DO0lBQW5DLDBEQUFtQztJQUM5RCxlQUNBO0lBREEsMkdBQ0E7SUFFWSxlQUE4QjtJQUE5Qix5Q0FBOEI7Ozs7SUEvRnRELCtCQUNFO0lBQUEsZ0NBQ0U7SUFESSx5TUFBZ0M7SUFDcEMsMkJBQUs7SUFBQSxZQUF1RDs7SUFBQSxpQkFBTTtJQUNsRSwrQkFDRTtJQUFBLGlDQVdBO0lBSkUscU9BQWlDLGlNQUFBOztJQVBuQyxpQkFXQTtJQUNBLG1HQUNBO0lBbURBLG1HQUNBO0lBc0NGLGlCQUFNO0lBQ1IsaUJBQU87SUFDVCxpQkFBTTs7O0lBM0dHLGVBQXVEO0lBQXZELGlGQUF1RDtJQUl4RCxlQUFtRTtJQUFuRSwwRkFBbUUsK0NBQUE7SUFHbkUsMEdBQTBFO0lBRTFFLG9EQUFpQztJQUtyQixlQUFzQztJQUF0Qyw0REFBc0M7SUFvRHRDLGVBQXFDO0lBQXJDLDJEQUFxQzs7OztJQXVFM0MsaUNBRUE7SUFERSxpVUFBOEMsd0JBQXdCLElBQUU7SUFEMUUsaUJBRUE7Ozs7SUFGdUIsNkVBQW1EOzs7SUFFMUUsNEJBQ0E7O0lBRHVCLDhCQUFnQixrQkFBQTs7OztJQUw3QyxrQ0FDRTtJQUFBLCtCQUNFO0lBQUEsK0JBQ0U7SUFBQSw0SEFFQTtJQUFBLDRIQUNBO0lBQUEsZ0NBQ0U7SUFESSxxVEFBNkMsd0JBQXdCLElBQUU7SUFDM0UsWUFDRjtJQUFBLGlCQUFPO0lBQ1QsaUJBQU07SUFDUixpQkFBTTtJQUNSLGlCQUFTOzs7O0lBWHFDLDJEQUFzQztJQUdILGVBQTBCO0lBQTFCLHFEQUEwQjtJQUU3RCxlQUEyQjtJQUEzQixzREFBMkI7SUFFakUsZUFDRjtJQURFLGlFQUNGOzs7SUFUTiw2QkFDQTtJQUFBLHFIQUNFO0lBV0YsMEJBQWU7Ozs7SUFac0UsZUFBMEY7SUFBMUYseUlBQTBGOzs7O0lBdEJyTCw2QkFDQTtJQUFBLCtCQUNJO0lBQUEsa0NBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFTO0lBQ1QsK0JBQ0U7SUFBQSw4QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQUs7SUFDTCxrQ0FDRTtJQUFBLGdDQUNFO0lBQUEsZ0NBQ0U7SUFBQSxrQ0FFQTtJQURFLHlPQUE0Qyx3QkFBd0IsSUFBRTtJQUR4RSxpQkFFQTtJQUFBLGlDQUNFO0lBREssc09BQTJDLHdCQUF3QixJQUFFO0lBQzFFLGFBQ0Y7O0lBQUEsaUJBQU87SUFDVCxpQkFBTTtJQUNSLGlCQUFNO0lBQ04sMkJBQW9DO0lBQ3RDLGlCQUFTO0lBQ1QsbUhBQ0E7SUFhRixpQkFBTTtJQUNWLGlCQUFNO0lBQ04sMEJBQWU7OztJQXBDb0IsZUFBdUI7SUFBdkIscUNBQXVCO0lBRXBELGVBQ0Y7SUFERSxpR0FDRjtJQUdJLGVBQ0Y7SUFERSxvR0FDRjtJQUk2QixlQUFvQztJQUFwQywwREFBb0M7SUFHekQsZUFDRjtJQURFLHlHQUNGO0lBS1EsZUFBOEI7SUFBOUIseUNBQThCOzs7SUE2QnhDLDhCQUE0RTs7SUFBbkUscUNBQXVCOzs7SUFDaEMsOEJBQXVFOztJQUE5RCwrQkFBaUI7OztJQU94Qiw4QkFBaUg7O0lBQXhHLHFDQUF1Qjs7O0lBQ2hDLDhCQUE0Rzs7SUFBbkcsK0JBQWlCOzs7SUFDMUIsOEJBQXVFOztJQUE5RCxxQ0FBdUI7Ozs7SUFKcEMsa0NBRUk7SUFEcUMsbVRBQTRDO0lBQ2pGLGdJQUF1RztJQUN2RyxnSUFBa0c7SUFDbEcsZ0lBQTZEO0lBQzdELFlBQ0o7SUFBQSxpQkFBUzs7OztJQUxQLDJEQUFzQztJQUNILGVBQXFFO0lBQXJFLDRHQUFxRTtJQUMzRSxlQUFzRTtJQUF0RSw2R0FBc0U7SUFDaEUsZUFBMkI7SUFBM0Isc0RBQTJCO0lBQzVELGVBQ0o7SUFESSxpRUFDSjs7O0lBUEEsNkJBQ0E7SUFBQSxxSEFFSTtJQUtKLDBCQUFlOzs7O0lBUFMsZUFBMEY7SUFBMUYseUlBQTBGOzs7O0lBaEIxSCw2QkFDRTtJQUFBLCtCQUNJO0lBQUEsa0NBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFTO0lBQ1QsK0JBQ0U7SUFBQSw4QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQUs7SUFDTCxrQ0FDRTtJQURzQixtT0FBMEM7SUFDaEUseUdBQWtFO0lBQ2xFLHlHQUE2RDtJQUM3RCxhQUNBOztJQUFBLDJCQUFvQztJQUN0QyxpQkFBUztJQUNULG1IQUNBO0lBUUYsaUJBQU07SUFDVixpQkFBTTtJQUNOLDBCQUFlOzs7SUF6Qm9CLGVBQXVCO0lBQXZCLHFDQUF1QjtJQUVwRCxlQUNGO0lBREUsaUdBQ0Y7SUFHSSxlQUNGO0lBREUsb0dBQ0Y7SUFFbUMsZUFBZ0M7SUFBaEMsdURBQWdDO0lBQ3RDLGVBQWlDO0lBQWpDLHdEQUFpQztJQUM1RCxlQUNBO0lBREEseUdBQ0E7SUFFWSxlQUE4QjtJQUE5Qix5Q0FBOEI7OztJQTNEdEQsK0JBQ0U7SUFBQSwyQkFBSztJQUFBLFlBQStEOztJQUFBLGlCQUFNO0lBRzFFLG1HQUNBO0lBdUNBLG1HQUNFO0lBMkJKLGlCQUFNOzs7SUF2RUMsZUFBK0Q7SUFBL0QseUZBQStEO0lBR3RELGVBQXNDO0lBQXRDLDREQUFzQztJQXdDdEMsZUFBcUM7SUFBckMsMkRBQXFDOzs7SUE2QnJELDBCQUVNOzs7SUFDTiwwQkFFTTs7Ozs7SUFRSixrQ0FHRTtJQUZBLHlRQUFzQztJQUV0QyxZQUNGO0lBQUEsaUJBQVM7Ozs7SUFKYSwySEFBOEY7SUFDM0Usc0VBQTRDO0lBRW5GLGVBQ0Y7SUFERSxpRUFDRjs7O0lBTEEsNkJBQ0E7SUFBQSxnR0FHRTtJQUVGLDBCQUFlOzs7SUFMc0csZUFBb0M7SUFBcEMsOENBQW9DOzs7O0lBYXJKLGtDQUNFO0lBRDRELGdRQUFzQztJQUNsRyxZQUNGO0lBQUEsaUJBQVM7Ozs7SUFGNEYsc0VBQTRDO0lBQy9JLGVBQ0Y7SUFERSxpRUFDRjs7O0lBUkosK0JBQ0U7SUFBQSxrQ0FBc0c7SUFBQSxZQUF3RDs7SUFBQSxpQkFBUztJQUN2SywrQkFDRTtJQUFBLCtCQUNFO0lBQUEsOEJBQTRCO0lBQUEsWUFBcUQ7O0lBQUEsaUJBQUs7SUFDeEYsaUJBQU07SUFDTix1RkFDRTtJQUVKLGlCQUFNO0lBQ1IsaUJBQU07OztJQVQwQyxlQUFxQztJQUFyQywyREFBcUM7SUFBbUIsZUFBd0Q7SUFBeEQsa0ZBQXdEO0lBRzlILGVBQXFEO0lBQXJELCtFQUFxRDtJQUUzRCxlQUFxQztJQUFyQyxnREFBcUM7Ozs7SUFyTnZFLDhCQUdFO0lBQUEsOEJBQ0U7SUFBQSwyRUFDRTtJQTZHRix5RUFDRTtJQXdFRix5RUFDRTtJQUVGLHlFQUNFO0lBRUYsOEJBQ0U7SUFBQSwyQkFDRTtJQUFBLHlGQUF5RjtJQUN6Rix3QkFDRjtJQUFBLGlCQUFNO0lBRU4sNkZBQ0E7SUFPQSw2RUFDRTtJQVVKLGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBTTs7O0lBeE5pQixlQUFtQztJQUFuQyx5REFBbUM7SUE4R25DLGVBQXlDO0lBQXpDLCtEQUF5QztJQXlFekMsZUFBMEM7SUFBMUMsZ0VBQTBDO0lBRzFDLGVBQW9DO0lBQXBDLDBEQUFvQztJQUt0QyxlQUEyQztJQUEzQyxnRUFBMkMsdURBQUE7SUFJNUMsZUFBdUM7SUFBdkMsNkRBQXVDO0lBUWxCLGVBQXdDO0lBQXhDLDhEQUF3Qzs7O0lBZ0N6RSx5QkFFSzs7OztJQUdELDRCQUNFO0lBQUEsaUNBQ0Y7SUFEZ0Qsc05BQTZCO0lBQTNFLGlCQUNGO0lBQUEsaUJBQU87OztJQURrQixlQUFzQjtJQUF0Qiw0Q0FBc0I7OztJQUhuRCw4QkFDRTtJQUFBLCtCQUNFO0lBQUEseUZBQ0U7SUFFSixpQkFBTTtJQUNSLGlCQUFLOzs7SUFKSyxlQUF3RTtJQUF4RSwrR0FBd0U7OztJQVdwRSw0QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQU87OztJQURMLGVBQ0Y7SUFERSxxRkFDRjs7O0lBQ0EsNEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFPOztJQURMLGVBQ0Y7SUFERSxvR0FDRjs7OztJQUVGLCtCQUNFO0lBQUEsaUNBRUU7SUFEQSxxUkFBOEMsd0JBQXdCLElBQUU7SUFEMUUsaUJBRUU7SUFBQSxnQ0FDQTtJQUR3QixrUkFBNkMsd0JBQXdCLElBQUU7SUFDL0YsWUFDQTtJQUFBLGlCQUFPO0lBQ1gsaUJBQU07Ozs7SUFMbUIsZUFBMEM7SUFBMUMsc0VBQTBDO0lBRy9ELGVBQ0E7SUFEQSxtRUFDQTs7O0lBbEJoQiw4QkFFSTtJQUFBLCtCQUNFO0lBQUEsK0JBQ0k7SUFBQSwrQkFDRTtJQUFBLDhCQUNFO0lBQUEsMEZBQ0U7SUFFRiwwRkFDRTtJQUVKLGlCQUFLO0lBQ0wseUZBQ0U7SUFNSixpQkFBTTtJQUNWLGlCQUFNO0lBQ1IsaUJBQU07SUFDVixpQkFBSzs7O0lBdEJILGlFQUFnRDtJQUFhLHFDQUF1QjtJQUtsRSxlQUE4QjtJQUE5QixxREFBOEI7SUFHOUIsZUFBK0I7SUFBL0Isc0RBQStCO0lBSUksZUFBNEM7SUFBNUMsdURBQTRDOzs7O0lBb0I3Riw0QkFDRTtJQUFBLGtDQUFxRjtJQUFoQyxxU0FBK0I7SUFBQyx3QkFBd0M7SUFBQSxpQkFBUztJQUN4SSxpQkFBTzs7OztJQUNQLDRCQUNFO0lBQUEsa0NBQXFGO0lBQWhDLHFTQUErQjtJQUFDLHdCQUF3QztJQUFBLGlCQUFTO0lBQ3hJLGlCQUFPOzs7SUFOVCw0QkFDRTtJQUFBLDJHQUNFO0lBRUYsMkdBQ0U7SUFFSixpQkFBTzs7OztJQU5DLGVBQTZDO0lBQTdDLDBFQUE2QztJQUc3QyxlQUE4QztJQUE5QywyRUFBOEM7OztJQUl0RCw0QkFDRTtJQUFBLFlBQ0Y7SUFBQSxpQkFBTzs7OztJQURMLGVBQ0Y7SUFERSxtRUFDRjs7OztJQWxCSiw2QkFDRTtJQUFBLDhCQU9FO0lBRkEsbVFBQStCO0lBRS9CLG9HQUNFO0lBT0Ysb0dBQ0U7SUFFSixpQkFBSztJQUNQLDBCQUFlOzs7O0lBbEJYLGVBQXdEO0lBQXhELDhFQUF3RCxtRUFBQSwrREFBQSw4Q0FBQTtJQU1sRCxlQUFzQztJQUF0QyxrRUFBc0M7SUFRdEMsZUFBeUQ7SUFBekQsa0dBQXlEOzs7O0lBb0JqRSxtQ0FBbUo7SUFBN0MsK1NBQWtDO0lBQUMsaUJBQVU7O0lBQXhILDBDQUE0Qjs7OztJQUN2RCxtQ0FBMkg7SUFBN0MsK1NBQWtDO0lBQUMsaUJBQVU7O0lBQWhHLHdDQUEwQjs7O0lBRnZELDhCQUNFO0lBQUEsd0hBQXlJO0lBQ3pJLHdIQUFpSDtJQUNuSCxpQkFBSzs7OztJQUZxRCxlQUE2QztJQUE3Qyx3RkFBNkM7SUFDL0MsZUFBdUI7SUFBdkIsb0RBQXVCOzs7O0lBRS9FLDhCQUNFO0lBQUEsK0JBQ0U7SUFBQSxpQ0FDRjtJQURvRCw4UkFBNkI7SUFBL0UsaUJBQ0Y7SUFBQSxpQkFBTTtJQUNSLGlCQUFLOzs7O0lBRnNCLGVBQTBCO0lBQTFCLHNEQUEwQjs7O0lBR3JELHFCQUVLOzs7OztJQUdILDJCQUNFO0lBQUEsK0lBR0E7SUFDRixpQkFBTTs7Ozs7SUFIQSxlQUFpQztJQUFqQyx3REFBaUMsNEhBQUE7OztJQUl2QywyQkFDRTtJQUFBLFlBQ0Y7SUFBQSxpQkFBTTs7Ozs7SUFESixlQUNGO0lBREUsNkVBQ0Y7OztJQVZGLDZCQUNBO0lBQUEsMEJBQ0U7SUFBQSx5SEFDRTtJQUtGLHlIQUNFO0lBRUosaUJBQUs7SUFDTCwwQkFBZTs7O0lBVlIsZUFBNEI7SUFBNUIsZ0RBQTRCO0lBTTVCLGVBQTZCO0lBQTdCLGlEQUE2Qjs7Ozs7SUFNbEMsOEJBQ0U7SUFBQSxxQkFBUztJQUNULDhCQUNFO0lBQUEsK0JBQ0U7SUFBQSwrSEFHQTtJQUNGLGlCQUFNO0lBQ1IsaUJBQUs7SUFDUCxpQkFBSzs7OztJQVYwQyw2REFBc0M7SUFFL0UsZUFBdUM7SUFBdkMsMERBQXVDO0lBR25DLGVBQWlEO0lBQWpELHdFQUFpRCxpRUFBQTs7OztJQVEzRCwrQkFDRTtJQUFBLGdJQUdBO0lBQ0YsaUJBQU07Ozs7SUFIRixlQUFpQztJQUFqQyx3REFBaUMsaUVBQUE7Ozs7SUFNekMsOEJBQ0U7SUFBQSxxQkFBUztJQUNULDBCQUNFO0lBQUEsK0JBQ0U7SUFBQSxnSUFHQTtJQUNGLGlCQUFNO0lBQ1IsaUJBQUs7SUFDUCxpQkFBSzs7OztJQVJDLGVBQXVDO0lBQXZDLDBEQUF1QztJQUduQyxlQUFzQztJQUF0Qyw2REFBc0MsaUVBQUE7OztJQTVEaEQsNkJBQ0E7SUFBQSw4QkFPRTtJQUFBLHlHQUNFO0lBR0YseUdBQ0U7SUFJRix3R0FDRTtJQUVGLDZIQUNBO0lBWUEsOEJBQ0U7SUFBQSx5R0FDRTtJQVVKLGlCQUFLO0lBQ0wsOEJBQ0U7SUFBQSwyR0FDRTtJQUtKLGlCQUFLO0lBQ1AsaUJBQUs7SUFDTCwyR0FDRTtJQVVGLDBCQUFlOzs7O0lBaEViLGVBQXFDO0lBQXJDLDREQUFxQztJQUdyQyxzQ0FBbUIseUNBQUE7SUFHSCxlQUFzQztJQUF0Qyw4REFBc0M7SUFJdEMsZUFBOEI7SUFBOUIsc0RBQThCO0lBSzFDLGVBQTBDO0lBQTFDLGtFQUEwQztJQUdoQyxlQUFxQztJQUFyQyxpREFBcUM7SUFtQ2pELGVBQXlEO0lBQXpELCtGQUF5RDs7O0lBM0QvRCw2QkFHRTtJQUFBLHlCQUF1QztJQUN2QywrR0FDQTtJQWtFRixpQkFBUTs7O0lBbkVRLGVBQStCO0lBQS9CLDZDQUErQjs7O0lBb0UvQyw2QkFDQTtJQUFBLDZCQUNFO0lBQUEsOEJBQ0U7SUFBQSw4QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQUs7SUFDUCxpQkFBSztJQUNQLGlCQUFRO0lBQ1IsMEJBQWU7OztJQUxQLGVBQW1DO0lBQW5DLHFEQUFtQztJQUNyQyxlQUNGO0lBREUseUZBQ0Y7OztJQUlKLDZCQUNBO0lBQUEsNkJBQ0U7SUFBQSw4QkFDRTtJQUFBLDhCQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBSztJQUNQLGlCQUFLO0lBQ1AsaUJBQVE7SUFDUiwwQkFBZTs7O0lBTFAsZUFBbUM7SUFBbkMscURBQW1DO0lBQ3JDLGVBQ0Y7SUFERSx1RkFDRjs7OztJQS9KUiw2QkFDQTtJQUFBLCtCQUNFO0lBQUEsaUNBV0U7SUFQQSxpUEFBZ0Q7SUFPaEQsbUNBQXdCO0lBQUEsWUFBc0Q7O0lBQUEsaUJBQVU7SUFDeEYsaUNBQ0U7SUFBQSwwQkFDRTtJQUFBLGlGQUNFO0lBRUYsaUZBQ0U7SUFNRixtRkFFSTtJQXNCSix3R0FDRTtJQW9CSixpQkFBSztJQUNQLGlCQUFRO0lBQ1Isd0ZBR0U7SUFxRUYsc0dBQ0E7SUFRQSxzR0FDQTtJQVFGLGlCQUFRO0lBQ1YsaUJBQU07SUFDTiwwQkFBZTs7O0lBM0pYLGVBQTRCO0lBQTVCLDRDQUE0QixpQ0FBQTtJQU41QixxREFBdUI7SUFDdkIsK0RBQTBDLHVDQUFBLHlEQUFBLGtEQUFBO0lBUWxCLGVBQXNEO0lBQXRELGlGQUFzRDtJQUcxRCxlQUFzQztJQUF0Qyw0REFBc0M7SUFHdEMsZUFBOEI7SUFBOUIsb0RBQThCO0lBTzlCLGVBQTBDO0lBQTFDLGdFQUEwQztJQXdCNUMsZUFBcUM7SUFBckMsK0NBQXFDO0lBd0JyRCxlQUF1QztJQUF2QyxvRUFBdUM7SUF1RTNCLGVBQWtDO0lBQWxDLCtEQUFrQztJQVNsQyxlQUFvQjtJQUFwQiwwQ0FBb0I7OztJQWtDaEMsc0NBRWtCOzs7O0lBRWhCLG1DQUFtSjtJQUE3Qyw4U0FBa0M7SUFBQyxpQkFBVTs7SUFBeEgsMENBQTRCOzs7O0lBQ3ZELG1DQUEySDtJQUE3Qyw4U0FBa0M7SUFBQyxpQkFBVTs7SUFBaEcsd0NBQTBCOzs7SUFGdkQsb0NBQ0U7SUFBQSx3SEFBeUk7SUFDekksd0hBQWlIO0lBQ25ILGlCQUFXOzs7O0lBRitDLGVBQTZDO0lBQTdDLHdGQUE2QztJQUMvQyxlQUF1QjtJQUF2QixvREFBdUI7Ozs7SUFNM0UsNEJBQ0U7SUFBQSxpQ0FDRjtJQURnRCx3UEFBNkI7SUFBM0UsaUJBQ0Y7SUFBQSxpQkFBTzs7O0lBRGtCLGVBQXNCO0lBQXRCLDZDQUFzQjs7O0lBSG5ELDJDQUNFO0lBQUEsK0JBQ0U7SUFBQSx3SEFDRTtJQUVKLGlCQUFNO0lBQ1IsaUJBQWtCOzs7SUFKUixlQUF3RTtJQUF4RSxrSEFBd0U7Ozs7SUFLbEYsb0NBQ0U7SUFBQSwrQkFDRTtJQUFBLGlDQUNGO0lBRG9ELCtRQUE2QjtJQUEvRSxpQkFDRjtJQUFBLGlCQUFNO0lBQ1IsaUJBQVc7Ozs7SUFGZ0IsZUFBMEI7SUFBMUIsc0RBQTBCOzs7SUFnQnZDLDRCQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBTzs7O0lBREwsZUFDRjtJQURFLHNGQUNGOzs7SUFDQSw0QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQU87O0lBREwsZUFDRjtJQURFLG9HQUNGOzs7O0lBRUYsK0JBQ0U7SUFBQSxpQ0FFRTtJQURBLG1UQUE4Qyx3QkFBd0IsSUFBRTtJQUQxRSxpQkFFRTtJQUFBLGdDQUNBO0lBRHdCLGdUQUE2Qyx3QkFBd0IsSUFBRTtJQUMvRixZQUNBO0lBQUEsaUJBQU87SUFDWCxpQkFBTTs7OztJQUxtQixlQUEwQztJQUExQyxzRUFBMEM7SUFHL0QsZUFDQTtJQURBLG1FQUNBOzs7SUF2QmhCLDJDQUNFO0lBQUEsK0JBTUU7SUFBQSwrQkFDRTtJQUFBLCtCQUNJO0lBQUEsK0JBQ0U7SUFBQSw4QkFDRTtJQUFBLHdIQUNFO0lBRUYsd0hBQ0U7SUFFSixpQkFBSztJQUNMLHVIQUNFO0lBTUosaUJBQU07SUFDVixpQkFBTTtJQUNSLGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBa0I7OztJQTNCZCxlQUFnRDtJQUFoRCxrRUFBZ0Q7SUFFaEQscUNBQXVCLHFCQUFBO0lBT1AsZUFBOEI7SUFBOUIsc0RBQThCO0lBRzlCLGVBQStCO0lBQS9CLHVEQUErQjtJQUlJLGVBQTRDO0lBQTVDLHdEQUE0Qzs7O0lBWW5HLCtCQUVXOzs7O0lBR1gsZ0NBQ0U7SUFBQSwrQkFDRTtJQUFBLGdJQUdBO0lBQ0YsaUJBQU07SUFDUixpQkFBVzs7OztJQUpILGVBQXNDO0lBQXRDLDZEQUFzQyxpRkFBQTs7OztJQWdCeEMsNEJBQ0U7SUFBQSxrQ0FBcUY7SUFBaEMseVVBQStCO0lBQUMsd0JBQXdDO0lBQUEsaUJBQVM7SUFDeEksaUJBQU87Ozs7SUFDUCw0QkFDRTtJQUFBLGtDQUFxRjtJQUFoQyx5VUFBK0I7SUFBQyx3QkFBd0M7SUFBQSxpQkFBUztJQUN4SSxpQkFBTzs7O0lBTlQsNEJBQ0U7SUFBQSwrSUFDRTtJQUVGLCtJQUNFO0lBRUosaUJBQU87Ozs7SUFOQyxlQUE2QztJQUE3QywwRUFBNkM7SUFHN0MsZUFBOEM7SUFBOUMsMkVBQThDOzs7SUFJdEQsNEJBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQU87Ozs7SUFETCxlQUNGO0lBREUsbUVBQ0Y7Ozs7SUFsQkosdUNBQ0U7SUFBQSwrQkFPRTtJQURBLHVUQUErQjtJQUMvQix3SUFDRTtJQU9GLHdJQUNFO0lBRUosaUJBQU07SUFDUixpQkFBa0I7Ozs7SUFwQmlCLHlFQUErQztJQUU5RSxlQUE2QztJQUE3QyxtREFBNkM7SUFDN0MsK0VBQXdELG9FQUFBLGdFQUFBLCtDQUFBO0lBS2xELGVBQXNDO0lBQXRDLG1FQUFzQztJQVF0QyxlQUF5RDtJQUF6RCxvR0FBeUQ7Ozs7SUFNakUsMkJBQ0U7SUFBQSxxSkFHQTtJQUNGLGlCQUFNOzs7OztJQUhBLGVBQWlDO0lBQWpDLHdEQUFpQyw0SEFBQTs7O0lBSXZDLDJCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFNOzs7OztJQURKLGVBQ0Y7SUFERSw2RUFDRjs7O0lBVEYsZ0NBQ0U7SUFBQSwrSEFDRTtJQUtGLCtIQUNFO0lBRUosaUJBQVc7OztJQVZxQixrSEFBbUY7SUFDNUcsZUFBNEI7SUFBNUIsZ0RBQTRCO0lBTTVCLGVBQTZCO0lBQTdCLGlEQUE2Qjs7O0lBN0J0QyxpQ0FDRTtJQUFBLDhJQUNFO0lBb0JGLCtIQUNFO0lBVUosMEJBQWU7OztJQWpDRCwrQ0FBNEI7OztJQW9DMUMsaUNBQThFOzs7SUFDOUUsOEJBT1c7Ozs7SUFMVCxpRUFBMEMsOENBQUE7SUFHMUMsc0NBQW1CLHlDQUFBOzs7SUFHckIsOEJBR1U7Ozs7SUFGTixnR0FBb0U7Ozs7O0lBbEkxRSx5Q0FlRTtJQVJBLHdRQUFnRDtJQVNoRCxpQ0FDRTtJQUFBLDZIQUNFO0lBRUYsK0dBQ0U7SUFHSiwwQkFBZTtJQUNmLGlDQUNFO0lBQUEsNkhBQ0U7SUFNRiwrR0FDRTtJQUlKLDBCQUFlO0lBQ2YsaUNBQ0U7SUFBQSw2SEFDRTtJQTZCRixpSEFDRTtJQUVKLDBCQUFlO0lBQ2Ysa0NBQ0U7SUFBQSxpSEFDRTtJQU9KLDBCQUFlO0lBQ2YseUhBQ0U7SUFtQ0YsNkhBQTZEO0lBQzdELCtHQU9DO0lBQ0QsK0dBR0E7SUFDRixpQkFBWTs7O0lBMUhWLDhDQUE0QixtQ0FBQTtJQU41Qix1REFBdUI7SUFIdkIsNERBQXFDLG9EQUFBLHlDQUFBLDJEQUFBLG9EQUFBO0lBY3ZCLGVBQStDO0lBQS9DLDZEQUErQztJQVMvQyxlQUErQztJQUEvQyw2REFBK0M7SUFjL0MsZUFBbUQ7SUFBbkQsaUVBQW1EO0lBbUNuRCxlQUE0QztJQUE1QywwREFBNEM7SUFVZixlQUE2QztJQUE3Qyx5REFBNkM7SUFvQ3hFLGVBQTRDO0lBQTVDLG9FQUE0QztJQUNuRCxlQUF5RDtJQUF6RCxxRUFBeUQ7SUFRekQsZUFBZ0c7SUFBaEcsOERBQWdHLHdEQUFBOzs7SUFNM0csNkJBQ0E7SUFBQSwrQkFDRTtJQUFBLDZCQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBSTtJQUNOLGlCQUFNO0lBQ04sMEJBQWU7O0lBSFgsZUFDRjtJQURFLHlGQUNGOzs7SUFHRiw2QkFDQTtJQUFBLCtCQUNFO0lBQUEsNkJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFJO0lBQ04saUJBQU07SUFDTiwwQkFBZTs7SUFIWCxlQUNGO0lBREUsdUZBQ0Y7OztJQXBKSiwrQkFFRTtJQUFBLHVHQWVFO0lBd0hGLDBHQUNBO0lBTUEsMEdBQ0E7SUFPRixpQkFBTTs7O0lBeklGLGVBQXVDO0lBQXZDLHdFQUF1QztJQTBIM0IsZUFBa0M7SUFBbEMsbUVBQWtDO0lBT2xDLGVBQW9CO0lBQXBCLDRDQUFvQjs7O0lBakpwQyw2QkFDQTtJQUFBLG1GQUVFO0lBdUpGLDBCQUFlOzs7SUF6SmUsZUFBOEI7SUFBOUIsb0RBQThCOzs7SUFpTDFDLGlDQUF3RDtJQUFBLHlCQUFTO0lBQUEsaUJBQU87Ozs7SUFINUUsK0JBQ0U7SUFBQSw4QkFDRTtJQURtQix1UkFBNEI7SUFDL0MsWUFDQTtJQUFBLGlIQUF3RDtJQUMxRCxpQkFBSTtJQUNOLGlCQUFLOzs7O0lBTDJCLGlFQUEwQztJQUV0RSxlQUNBO0lBREEsOENBQ0E7SUFBc0IsZUFBaUM7SUFBakMsOERBQWlDOzs7SUFHM0QsK0JBQ0U7SUFBQSw4QkFDRTtJQUFBLHFCQUNGO0lBQUEsaUJBQUk7SUFDTixpQkFBSzs7O0lBWEwsNkJBQ0E7SUFBQSx3R0FDRTtJQUtGLHdHQUNFO0lBSUYsMEJBQWU7OztJQVg0RCxlQUFrQjtJQUFsQixzQ0FBa0I7SUFNdkUsZUFBaUI7SUFBakIscUNBQWlCOzs7O0lBc0J2QyxrQ0FDRTtJQUR3RSxvUkFBbUM7SUFDM0csWUFDRjtJQUFBLGlCQUFTOzs7O0lBRnFHLHFFQUF5QztJQUNySixlQUNGO0lBREUsOENBQ0Y7Ozs7SUFSSixpQ0FDRTtJQUFBLDJCQUNFO0lBQUEsd0JBQ0Y7SUFBQSxpQkFBTTtJQUNOLG1DQUFnRTtJQUFBLFlBQXFGOztJQUFBLGlCQUFTO0lBQzlKLGdDQUNFO0lBQUEsdUdBQ0U7SUFFSixpQkFBTztJQUNULGlCQUFPOzs7SUFOMkQsZUFBcUY7SUFBckYsMklBQXFGO0lBRTNILGVBQWlEO0lBQWpELDJEQUFpRDs7Ozs7SUExQy9FLDZCQUNBO0lBQUEsZ0NBQ0U7SUFBQSxnQ0FDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQU07SUFDTixnQ0FDRTtJQUFBLCtCQUNFO0lBQUEsK0JBQ0U7SUFBQSw4QkFBK0M7SUFBMUIsb01BQXNCLENBQUMsS0FBRTtJQUFDLFlBQXNEOztJQUFBLGlCQUFJO0lBQzNHLGlCQUFLO0lBQ0wsZ0NBQ0U7SUFBQSwrQkFBa0U7SUFBN0MsaU9BQXlDLENBQUMsS0FBRTtJQUFDLGFBQXlEOztJQUFBLGlCQUFJO0lBQ2pJLGlCQUFLO0lBQ0wsNkdBQ0E7SUFZQSxnQ0FDRTtJQUFBLCtCQUFrRTtJQUE3QyxpT0FBeUMsQ0FBQyxLQUFFO0lBQUMsYUFBcUQ7O0lBQUEsaUJBQUk7SUFDN0gsaUJBQUs7SUFDTCxnQ0FDRTtJQUFBLCtCQUFrRTtJQUE3QyxpT0FBeUMsQ0FBQyxLQUFFO0lBQUMsYUFBcUQ7O0lBQUEsaUJBQUk7SUFDN0gsaUJBQUs7SUFDUCxpQkFBSztJQUNQLGlCQUFNO0lBQ1IsaUJBQU07SUFDTixpQ0FDRTtJQUFBLDhGQUNFO0lBVUosaUJBQU07SUFDTiwwQkFBZTs7O0lBN0NYLGVBQ0Y7SUFERSx1TEFDRjtJQUdvQyxlQUEwQztJQUExQyw0REFBMEM7SUFDekIsZUFBc0Q7SUFBdEQsa0ZBQXNEO0lBRXZFLGVBQTBDO0lBQTFDLDREQUEwQztJQUNOLGVBQXlEO0lBQXpELHFGQUF5RDtJQUUvRyxlQUE4QjtJQUE5Qiw0Q0FBOEI7SUFhWixlQUE2RDtJQUE3RCxzRkFBNkQ7SUFDekIsZUFBcUQ7SUFBckQsaUZBQXFEO0lBRXpGLGVBQTZEO0lBQTdELHNGQUE2RDtJQUN6QixlQUFxRDtJQUFyRCxpRkFBcUQ7SUFNM0csZUFBd0M7SUFBeEMsZ0VBQXdDOzs7OztJQXFCMUQsa0NBR0E7SUFGQSw2UUFBc0M7SUFFdEMsWUFDRjtJQUFBLGlCQUFTOzs7O0lBSmUsNkhBQThGO0lBQzdFLHdFQUE0QztJQUVuRixlQUNGO0lBREUsbUVBQ0Y7OztJQUxBLDZCQUNFO0lBQUEsK0ZBR0E7SUFFRiwwQkFBZTs7O0lBTHdHLGVBQW9DO0lBQXBDLGdEQUFvQzs7OztJQWF2SixrQ0FDRTtJQUQ0RCxvUUFBc0M7SUFDbEcsWUFDRjtJQUFBLGlCQUFTOzs7O0lBRjRGLHdFQUE0QztJQUMvSSxlQUNGO0lBREUsbUVBQ0Y7OztJQVJKLCtCQUNFO0lBQUEsa0NBQXNHO0lBQUEsWUFBd0Q7O0lBQUEsaUJBQVM7SUFDdkssK0JBQ0U7SUFBQSwrQkFDRTtJQUFBLDhCQUE0QjtJQUFBLFlBQXFEOztJQUFBLGlCQUFLO0lBQ3hGLGlCQUFNO0lBQ04sc0ZBQ0U7SUFFSixpQkFBTTtJQUNSLGlCQUFNOzs7SUFUMEMsZUFBcUM7SUFBckMsNERBQXFDO0lBQW1CLGVBQXdEO0lBQXhELGtGQUF3RDtJQUc5SCxlQUFxRDtJQUFyRCwrRUFBcUQ7SUFFM0QsZUFBcUM7SUFBckMsaURBQXFDOzs7SUEzRXpFLGdDQUdFO0lBQUEsOEJBQ0k7SUFBQSxnQ0FDRTtJQUFBLDZGQUNBO0lBZ0RGLGlCQUFNO0lBQ04sOEJBQ0U7SUFBQSwyQkFDRTtJQUFBLHlGQUF5RjtJQUN6Rix3QkFDRjtJQUFBLGlCQUFNO0lBRU4sMkZBQ0U7SUFPRiwyRUFDRTtJQVVKLGlCQUFNO0lBQ1YsaUJBQU07SUFDUixpQkFBTTs7O0lBN0VnQixlQUEwRDtJQUExRCx1RkFBMEQ7SUFvRHpELGVBQTJDO0lBQTNDLGdFQUEyQyx1REFBQTtJQUk1QyxlQUF1QztJQUF2Qyw2REFBdUM7SUFRbEIsZUFBd0M7SUFBeEMsOERBQXdDOztBRHRpQnJGLE1BQU0sT0FBTyxrQkFBa0I7SUF1SDdCLGtCQUFrQjtJQUVsQixZQUNVLGdCQUFrQyxFQUNsQyxvQkFBc0MsRUFDdEMsUUFBa0MsRUFDbEMsR0FBc0IsRUFDdEIsTUFBYztRQUpkLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFrQjtRQUN0QyxhQUFRLEdBQVIsUUFBUSxDQUEwQjtRQUNsQyxRQUFHLEdBQUgsR0FBRyxDQUFtQjtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBeEhmLGlCQUFZLEdBQzZCLElBQUksQ0FBQztRQUU5Qyx5QkFBb0IsR0FBa0IsSUFBSSxDQUFDO1FBQzNDLDRCQUF1QixHQUFrQixJQUFJLENBQUM7UUFDOUMsb0JBQWUsR0FBa0IsSUFBSSxDQUFDO1FBQ3RDLHFCQUFnQixHQUFvQixJQUFJLENBQUM7UUFrQ2xELGdCQUFnQjtRQUNOLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBQ3hDLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBa0MsQ0FBQztRQUNoRSxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7UUFDNUMsMkJBQXNCLEdBQUcsSUFBSSxZQUFZLEVBQXNCLENBQUM7UUFDaEUseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQXNCLENBQUM7UUFDOUQseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQTZCLENBQUM7UUFDckUsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBNEIsQ0FBQztRQUM1RCxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFnQyxDQUFDO1FBQy9ELGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQWdDLENBQUM7UUFDL0QsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFzQyxDQUFDO1FBV2xFLFNBQUksR0FBRyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUlyQyxtQkFBYyxHQUFvQyxJQUFJLENBQUM7UUFDdkQsOEJBQXlCLEdBQW9DLElBQUksQ0FBQztRQUUxRSxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUNkLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBaUM1QixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRWIsK0JBQTBCLEdBQXdCLElBQUksQ0FBQztRQUV2RCxnQ0FBMkIsR0FBd0IsSUFBSSxDQUFDO1FBRWhFLG1CQUFtQjtRQUNuQiwyQkFBc0IsR0FBMEIsSUFBSSxDQUFDO1FBQ3JELG9CQUFlLEdBQVEsSUFBSSxDQUFDO1FBNDNCNUIsd0JBQXdCO1FBRXhCLHdCQUFtQixHQUFHLENBQUMsSUFBa0IsRUFBRSxFQUFFO1lBQzNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixDQUFDLENBQUE7UUF3eUJELGlDQUE0QixHQUFHLENBQUMsQ0FBUyxFQUFFLEdBQVEsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBRWxHLHVCQUFrQixHQUFHLENBQUMsR0FBUSxFQUFFLEtBQUssR0FBRyxLQUFLLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ25ELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFDRCxJQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ25ELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLDhCQUE4QixFQUFFO2dCQUN4QyxPQUFPLEtBQUssQ0FBQzthQUNkO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDekIsT0FBTyxLQUFLLENBQUM7YUFDZDtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFBO1FBcHJEQyxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsR0FBRyxDQUFDLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFlLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFFeEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksT0FBTyxFQUFTLENBQUM7SUFDckQsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksZUFBZSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBRTVDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV4RSxtQkFBbUI7UUFDbkIsd0JBQXdCLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1FBRWxILElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQzdELENBQUMsQ0FBQyxFQUFFLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4RyxJQUFJLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQ3ZELENBQUMsQ0FBQyxFQUFFLENBQUMsd0JBQXdCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN6RyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2pELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztRQUV2RSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsK0JBQStCO1lBQy9CLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDYixNQUFNLElBQUksS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7YUFDNUU7U0FFRjthQUFNLElBQUksSUFBSSxDQUFDLElBQUksWUFBWSxVQUFVLElBQUksSUFBSSxDQUFDLElBQUksWUFBWSxPQUFPLEVBQUU7WUFDMUUsc0JBQXNCO1lBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsaUNBQWlDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdkQsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsc0JBQXNCO1lBQ3RCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3JDO1FBRUQscUNBQXFDO1FBQ3JDLElBQUksb0JBQTRFLENBQUM7UUFDakYsSUFBSSxJQUFJLENBQUMsa0NBQWtDLEVBQUU7WUFDM0Msb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDbkQ7YUFBTTtZQUNMLG9CQUFvQixHQUFHLElBQUksVUFBVSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUNqRCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QixVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQTJDLEVBQUUsRUFBRTtZQUM3RSxJQUFJLE1BQU0sRUFBRTtnQkFDVixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDbkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2xDO1lBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2hHLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ2hDLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDUixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVMLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRTtZQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG9DQUFvQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2pFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsUUFBUSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3BFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ2hDLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDUixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGtCQUFrQjtRQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFTyxpQ0FBaUMsQ0FBQyxZQUFtQjtRQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxZQUFZLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTyxzQkFBc0I7UUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQ25FLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzFDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDdEQ7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUMxQyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDcEU7SUFDSCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLGNBQWMsRUFBRTtZQUMxQixJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN0RTtRQUNELElBQUksT0FBTyxDQUFDLGVBQWUsRUFBRTtZQUMzQixJQUFJLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN4RTtRQUNELElBQUksT0FBTyxDQUFDLGVBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFO1lBQ25FLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhEQUE4RCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzFGLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7SUFFTywwQkFBMEIsQ0FBQyxRQUFrRDtRQUNuRixJQUFJLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUNuQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDL0M7UUFDRCxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQywwQkFBMEIsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO2lCQUNwRjtxQkFBTSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3pGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhEQUE4RCxHQUFHLElBQUksQ0FBQyx3QkFBd0I7d0JBQzdHLDBDQUEwQyxDQUFDLENBQUM7aUJBQy9DO3FCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyw2RUFBNkUsQ0FBQyxDQUFDO2lCQUNqRztxQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHdFQUF3RSxDQUFDLENBQUM7aUJBQzVGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7b0JBQzdELElBQUksQ0FBQyxNQUFNLENBQUM7d0JBQ1YsTUFBTSxFQUFFLHFCQUFxQixDQUFDLElBQUk7d0JBQ2xDLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixZQUFZLEVBQUUsS0FBSyxDQUFDLFlBQVksS0FBSyxJQUFJLElBQUksS0FBSyxDQUFDLFlBQVksS0FBSyxLQUFLLENBQUMsQ0FBQzs0QkFDekUsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdDQUFnQztxQkFDN0QsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTywyQkFBMkIsQ0FBQyxRQUFnQjtRQUNsRCxJQUFJLElBQUksQ0FBQywyQkFBMkIsRUFBRTtZQUNwQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDaEQ7UUFDRCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUM3QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO2lCQUNwRjtxQkFBTSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDJEQUEyRCxHQUFHLElBQUksQ0FBQyx3QkFBd0I7d0JBQzFHLDBDQUEwQyxDQUFDLENBQUM7aUJBQy9DO3FCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwRUFBMEUsQ0FBQyxDQUFDO2lCQUM5RjtxQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFFQUFxRSxDQUFDLENBQUM7aUJBQ3pGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7b0JBQ3JELElBQUksQ0FBQyxNQUFNLENBQUM7d0JBQ1YsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVE7d0JBQ3RDLFlBQVksRUFBRSxJQUFJLENBQUMsa0NBQWtDO3FCQUN0RCxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELGlCQUFpQjtJQUVWLE9BQU8sQ0FBQyxVQUFvQjtRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFPLFVBQVUsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1NBQzVEO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRU0sZUFBZTtRQUNwQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQztJQUVNLGlCQUFpQjtRQUN0QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDN0IsQ0FBQztJQUVNLFVBQVUsQ0FBQyxNQUEyQztRQUMzRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFPLFVBQVUsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1NBQzVEO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsOENBQThDLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsUUFBUSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUVPLG1CQUFtQixDQUFDLE1BQTJDO1FBQ3RFLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDakUsT0FBTztTQUNSO1FBRUQsSUFBSSxNQUFNLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLG9CQUFvQixFQUFFO1lBQy9GLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1FQUFtRTtnQkFDbEYsTUFBTSxDQUFDLGFBQWEsR0FBRyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNqSSxPQUFPO1NBQ1A7UUFFRCxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUM7U0FDOUc7UUFFRCxJQUFJLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtZQUMvQixJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEtBQUssc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDakcsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDdkU7UUFFRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO1lBQy9DLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2hEO1FBRUQsSUFBSSxJQUFJLENBQUMsc0JBQXNCLElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRixJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxlQUFDLE9BQUEsYUFBQSxNQUFNLDBDQUFFLFlBQVksMENBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU0sQ0FBQyxDQUFDLENBQUEsRUFBQSxDQUFDLENBQUM7U0FDbkg7UUFFRCxJQUFJLElBQUksQ0FBQyw2QkFBNkIsSUFBSSxNQUFNLENBQUMsY0FBYyxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFO1lBQ2hHLElBQUksQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxlQUFDLE9BQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLGFBQUEsTUFBTSwwQ0FBRSxjQUFjLDBDQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFNLENBQUMsQ0FBQyxDQUFBLEVBQUEsQ0FBQyxDQUFDO1NBQ3RJO1FBRUQsSUFBSSxJQUFJLENBQUMsNkJBQTZCLElBQUksTUFBTSxDQUFDLGNBQWMsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRTtZQUMvRixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsZUFBQyxPQUFBLGFBQUEsTUFBTSwwQ0FBRSxjQUFjLDBDQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFNLENBQUMsQ0FBQyxDQUFBLEVBQUEsQ0FBQyxDQUFDO1NBQzlHO1FBRUQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtZQUNsRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDbkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztTQUNoRztRQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDakQsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO0lBQ3hFLENBQUM7SUFFTyw0QkFBNEIsQ0FBQyxNQUEyQztRQUMvRSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRTtZQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ2pFLE9BQU87U0FDUjtRQUVELElBQUksTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRTtZQUMvRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtRUFBbUU7Z0JBQ2xGLE1BQU0sQ0FBQyxhQUFhLEdBQUcsNEJBQTRCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDakksT0FBTztTQUNQO1FBRUQsSUFBSSxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLDhCQUE4QjtlQUNoRSxNQUFNLENBQUMsc0JBQXNCLElBQUksTUFBTSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRTtZQUU1RSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFOztnQkFDbEQsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsSUFBSSxhQUFBLE1BQU0sMENBQUUsc0JBQXNCLDBDQUFFLE9BQU8sQ0FBQyxFQUFFLE9BQU0sQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxDQUFDLENBQUM7U0FDSDtRQUVELElBQUksSUFBSSxDQUFDLHlCQUF5QixJQUFJLElBQUksQ0FBQyw4QkFBOEI7ZUFDbkUsTUFBTSxDQUFDLHVCQUF1QixJQUFJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUU7WUFFOUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTs7Z0JBQ25ELE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxFQUFFLElBQUksYUFBQSxNQUFNLDBDQUFFLHVCQUF1QiwwQ0FBRSxPQUFPLENBQUMsRUFBRSxPQUFNLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUYsQ0FBQyxDQUFDLENBQUM7U0FDSDtRQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDakQsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO0lBQ3hFLENBQUM7SUFFTyxNQUFNLENBQUUsT0FBZ0M7UUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7UUFDMUMsTUFBTSxHQUFHLEdBQXFCLElBQUksVUFBVSxDQUFPLFVBQVUsQ0FBQyxFQUFFO1lBQzlELElBQUk7Z0JBQ0YsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUM5QztZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFO2dCQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzthQUMzQjtZQUVELElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQzNDLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRTtZQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHFCQUFxQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFO2dCQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzthQUMzQjtZQUVELElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRU8sa0JBQWtCLENBQUMsT0FBeUIsRUFBRSxPQUFnQztRQUNwRixNQUFNLFlBQVksR0FBK0MsT0FBTyxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQztRQUVwRyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUU1QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUV2RCxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUU7WUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFFeEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFdBQVcsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO1FBRXhELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixxQkFBcUI7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsOERBQThELENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUVqRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFnQyxFQUFFLEVBQUU7Z0JBQ3JGLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Z0JBRTFELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUN4QixJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUsseUJBQXlCLENBQUMsTUFBTSxFQUFFO3dCQUM1RCxJQUFJLENBQUMsaUNBQWlDLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ2xEO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO3FCQUN2RTtvQkFDRCxJQUFJLFlBQVksRUFBRTt3QkFDaEIsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUNqRDtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2lCQUN4RTtnQkFFRCxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2YsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3JCLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyw0Q0FBNEMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFFekUsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO2lCQUN4QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2lCQUN4RTtnQkFFRCxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7U0FFSjthQUFNO1lBQ0wsK0RBQStEO1lBQy9ELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7WUFFcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQzdFLElBQUksWUFBWSxFQUFFO29CQUNoQixJQUFJLENBQUMsNEJBQTRCLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ2pEO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN4RTtZQUVELE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNmLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNwQjtJQUNILENBQUM7SUFFTyxpQ0FBaUMsQ0FBQyxRQUFnQztRQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFFckMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxRQUFRLENBQUMsVUFBVSxFQUFFO2dCQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQzthQUM3QztpQkFBTTtnQkFDTCxNQUFNLElBQUksS0FBSyxDQUFDLDREQUE0RCxDQUFDLENBQUM7YUFDL0U7WUFFRCxJQUFJLFFBQVEsQ0FBQyxhQUFhLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO2FBQ25EO2lCQUFNO2dCQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsK0RBQStELENBQUMsQ0FBQzthQUNsRjtTQUNGO0lBQ0gsQ0FBQztJQUVPLGlDQUFpQyxDQUFDLElBQVcsRUFBRSxPQUE4QjtRQUNuRixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO1FBQ3JGLE1BQU0sZ0JBQWdCLEdBQUcsdUJBQXVCLENBQUMsYUFBYSxDQUM1RCxJQUFJLEVBQ0osT0FBTyxFQUNQLElBQUksQ0FBQyxpQkFBaUIsRUFDdEIsSUFBSSxDQUFDLDBCQUEwQixFQUMvQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBRSxDQUFDO1FBRTVCLElBQUksQ0FBQyxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO1FBQzdDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFPLGdCQUFnQixDQUFDLGFBQWEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDO1FBQ3pILElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLGdCQUFnQixDQUFDLFVBQVUsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO0lBQ2xILENBQUM7SUFFTyxnQkFBZ0I7O1FBQ3RCLE1BQU0sTUFBTSxHQUEwQjtZQUNwQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDakUsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUNoRSxJQUFJLEVBQUUsRUFBRTtZQUNSLEtBQUssRUFBRSxJQUFJO1lBQ1gsV0FBVyxFQUFFLEVBQUU7WUFDZixPQUFPLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFFRixJQUFJLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDekQsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBRSxDQUFDO1NBQ2pIO1FBRUQsSUFBSSxJQUFJLENBQUMsNkJBQTZCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRTtZQUMzRSxLQUFLLE1BQU0sTUFBTSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtnQkFDL0MsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7YUFDM0M7U0FDRjtRQUVELE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUMxQyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7UUFDaEQsSUFBSSxVQUFVLEVBQUU7WUFDZCxNQUFBLE1BQU0sQ0FBQyxJQUFJLDBDQUFFLElBQUksQ0FBQztnQkFDaEIsUUFBUSxFQUFFLFVBQVUsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLEtBQUssSUFBSSxJQUFJO2dCQUM1RCxTQUFTLEVBQUUsYUFBYSxJQUFJLHNCQUFzQixDQUFDLFNBQVM7YUFDN0QsRUFBRTtTQUNKO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVPLE9BQU8sQ0FBQyxLQUFhO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQsSUFBSSxnQ0FBZ0M7O1FBQ2xDLElBQUksSUFBSSxDQUFDLHlCQUF5QixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssS0FBSyxFQUFFO1lBQ3ZGLE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDO1NBQ3ZDO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsT0FBTywwQ0FBRSx5QkFBMEIsQ0FBQztTQUN6RjtJQUNILENBQUM7SUFFRCxJQUFJLGtDQUFrQzs7UUFDcEMsSUFBSSxJQUFJLENBQUMsMkJBQTJCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQywyQkFBMkIsS0FBSyxLQUFLLEVBQUU7WUFDM0YsT0FBTyxJQUFJLENBQUMsMkJBQTJCLENBQUM7U0FDekM7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHlCQUEwQixDQUFDO1NBQ3pGO0lBQ0gsQ0FBQztJQUVELElBQUksd0JBQXdCOztRQUMxQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRTtnQkFDdEUsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO2FBQzdCO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBMkMsQ0FBQyxDQUFDO2FBQzNEO1NBQ0Y7UUFDRCxPQUFPLENBQUMsTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLGVBQWdCLENBQUMsQ0FBQztJQUNsRixDQUFDO0lBRUQsSUFBSSxzQkFBc0I7O1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGVBQWUsS0FBSSxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE9BQU8sMENBQUUsZUFBZ0IsQ0FBQSxDQUFDO0lBQ3hHLENBQUM7SUFFRCxJQUFJLGNBQWM7UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVELElBQUksaUJBQWlCOztRQUNuQixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO1lBQ3pELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztTQUN4QjthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFdBQVcsMENBQUUsb0JBQXFCLENBQUM7U0FDeEY7SUFDSCxDQUFDO0lBRUQsSUFBSSxpQkFBaUI7O1FBQ25CLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7WUFDekQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3hCO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsV0FBVywwQ0FBRSxvQkFBcUIsQ0FBQztTQUN4RjtJQUNILENBQUM7SUFFRCxJQUFJLHVCQUF1Qjs7UUFDekIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxLQUFLLEVBQUU7WUFDckUsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7U0FDOUI7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxVQUFVLDBDQUFFLGdCQUFpQixDQUFDO1NBQ25GO0lBQ0gsQ0FBQztJQUVELElBQUkseUJBQXlCOztRQUMzQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGtCQUFrQixLQUFLLEtBQUssRUFBRTtZQUN6RSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztTQUNoQzthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFlBQVksMENBQUUsZ0JBQWlCLENBQUM7U0FDckY7SUFDSCxDQUFDO0lBRUQsSUFBSSw2QkFBNkI7O1FBQy9CLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssS0FBSyxFQUFFO1lBQ2pGLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO1NBQ3BDO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsZ0JBQWdCLDBDQUFFLGdCQUFpQixDQUFDO1NBQ3pGO0lBQ0gsQ0FBQztJQUVELElBQUksMkJBQTJCOztRQUM3QixJQUFJLElBQUksQ0FBQyxvQkFBb0IsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLG9CQUFvQixLQUFLLEtBQUssRUFBRTtZQUM3RSxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztTQUNsQzthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFVBQVUsMENBQUUsaUNBQWtDLENBQUM7U0FDcEc7SUFDSCxDQUFDO0lBRUQsSUFBSSxpQ0FBaUM7O1FBQ25DLElBQUksSUFBSSxDQUFDLDBCQUEwQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsMEJBQTBCLEtBQUssS0FBSyxFQUFFO1lBQ3pGLE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDO1NBQ3hDO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsWUFBWSwwQ0FBRSxpQ0FBa0MsQ0FBQztTQUN0RztJQUNILENBQUM7SUFFRCxJQUFJLHlCQUF5Qjs7UUFDM0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxLQUFLLEVBQUU7WUFDekUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7U0FDaEM7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLGdCQUFpQixDQUFDO1NBQ3JGO0lBQ0gsQ0FBQztJQUVELElBQUksMEJBQTBCOztRQUM1QixJQUFJLElBQUksQ0FBQyxtQkFBbUIsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLG1CQUFtQixLQUFLLEtBQUssRUFBRTtZQUMzRSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUNqQzthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE9BQU8sMENBQUUsNkJBQThCLENBQUM7U0FDN0Y7SUFDSCxDQUFDO0lBRUQsSUFBSSwyQkFBMkI7O1FBQzdCLElBQUksSUFBSSxDQUFDLG9CQUFvQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsb0JBQW9CLEtBQUssS0FBSyxFQUFFO1lBQzdFLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDO1NBQ2xDO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsT0FBTywwQ0FBRSw4QkFBK0IsQ0FBQztTQUM5RjtJQUNILENBQUM7SUFFRCxJQUFJLDZCQUE2Qjs7UUFDL0IsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxLQUFLLEVBQUU7WUFDakYsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUM7U0FDcEM7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxjQUFjLDBDQUFFLGdCQUFpQixDQUFDO1NBQ3ZGO0lBQ0gsQ0FBQztJQUVELElBQUksc0JBQXNCOztRQUN4QixJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssS0FBSyxFQUFFO1lBQ25FLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztTQUM3QjthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsMENBQUUsZ0JBQWlCLENBQUM7U0FDbEY7SUFDSCxDQUFDO0lBRUQsSUFBSSx3QkFBd0I7O1FBQzFCLElBQUksSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssS0FBSyxFQUFFO1lBQ3ZFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO1NBQy9CO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsWUFBWSwwQ0FBRSxpQ0FBa0MsQ0FBQztTQUN0RztJQUNILENBQUM7SUFFRCxJQUFJLHNCQUFzQjs7UUFDeEIsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssRUFBRTtZQUNuRSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDN0I7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLHlCQUEwQixDQUFDO1NBQzlGO0lBQ0gsQ0FBQztJQUVELElBQUksNkJBQTZCOztRQUMvQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLEtBQUssRUFBRTtZQUNqRixPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQztTQUNwQzthQUFNO1lBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLGdCQUFnQiwwQ0FBRSxnQkFBaUIsQ0FBQztTQUN6RjtJQUNILENBQUM7SUFFRCxJQUFJLHNCQUFzQjs7UUFDeEIsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssRUFBRTtZQUNuRSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDN0I7YUFBTTtZQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLGdCQUFpQixDQUFDO1NBQ3JGO0lBQ0gsQ0FBQztJQUVELElBQUkscUJBQXFCOztRQUN2QixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1NBQzVCO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsVUFBVSwwQ0FBRSxxQkFBc0IsQ0FBQztTQUN4RjtJQUNILENBQUM7SUFFRCxJQUFJLDhCQUE4QjtRQUNoQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyx5QkFBeUIsQ0FBQztJQUNqRSxDQUFDO0lBRUQsSUFBSSxrQ0FBa0M7UUFDcEMsT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyw2QkFBNkIsQ0FBQztJQUNqRSxDQUFDO0lBRUQsSUFBSSw4QkFBOEI7UUFDaEMsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUM7SUFDeEMsQ0FBQztJQUVELElBQUksZ0NBQWdDO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQywyQkFBMkIsQ0FBQztJQUN2SCxDQUFDO0lBRUQsSUFBSSxrQ0FBa0M7UUFDcEMsT0FBTyxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsNkJBQTZCLENBQUM7SUFDckcsQ0FBQztJQUVELElBQUksZ0NBQWdDO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLDJCQUEyQixDQUFDO0lBQ25HLENBQUM7SUFFRCxJQUFJLCtCQUErQjtRQUNqQyxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQywwQkFBMEIsQ0FBQztJQUNoRyxDQUFDO0lBRUQsSUFBSSxpQ0FBaUM7UUFDbkMsT0FBTyxJQUFJLENBQUMsNkJBQTZCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMxRyxDQUFDO0lBRUQsSUFBSSwyQkFBMkI7UUFDN0IsT0FBTyxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELElBQUksd0JBQXdCOztRQUMxQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQ3pELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO1NBQzlCO2FBQU07WUFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsVUFBVSwwQ0FBRSx3QkFBeUIsQ0FBQztTQUMzRjtJQUNILENBQUM7SUFFRCxJQUFJLGtCQUFrQjtRQUNwQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM1QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUNqQzthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxJQUFJLGdCQUFnQjtRQUNsQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELElBQUksbUJBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixJQUFJLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsSUFBSSxlQUFlOztRQUNqQixNQUFNLEdBQUcsR0FBRyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFVBQVUsMENBQUUsZUFBZ0IsQ0FBQztRQUN0RixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztTQUM5QjthQUFNLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUMvQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDN0I7YUFBTSxJQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNO1lBQ3BDLEdBQUc7WUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ25ELE9BQU8sR0FBRyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUU7WUFDL0MsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekM7YUFBTSxJQUFJLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxDQUFDO1NBQ1o7YUFBTTtZQUNMLE9BQU8sRUFBRSxDQUFDO1NBQ1g7SUFDSCxDQUFDO0lBRUQsSUFBSSxnQkFBZ0I7UUFDbEIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7U0FDL0I7YUFBTTtZQUNMLE9BQU8sQ0FBQyxDQUFDO1NBQ1Y7SUFDSCxDQUFDO0lBRUQsSUFBSSxpQkFBaUI7UUFDbkIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0Isd0NBQXdDO1lBQ3hDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsV0FBQyxPQUFBLENBQUMsQ0FBQyxJQUFJLFlBQUssSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJLENBQUEsQ0FBQSxFQUFBLENBQUMsSUFBSSxJQUFJLENBQUM7U0FDeEY7YUFBTSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUNwQyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDeEYsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsT0FBTyxXQUFXLENBQUM7YUFDcEI7aUJBQU07Z0JBQ0wsOEJBQThCO2dCQUM5QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQ3ZDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQzthQUN0RztTQUNGO2FBQU07WUFDTCw4QkFBOEI7WUFDOUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUN2Qyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7U0FDdEc7SUFDSCxDQUFDO0lBRUQsSUFBSSxvQkFBb0I7UUFDdEIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7U0FDbkM7YUFBTSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyx1QkFBdUIsS0FBSyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDM0Usc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDdEU7YUFBTTtZQUNMLGlCQUFpQjtZQUNqQixPQUFPLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztTQUN6QztJQUNILENBQUM7SUFFRCxJQUFJLFdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRCxJQUFJLFNBQVM7UUFDWCxNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDckIsTUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLE1BQU0sYUFBYSxHQUFHLENBQUMsQ0FBQztRQUV4QixNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDakIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ3RDLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUMzQyxJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFFdkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sRUFBRSxDQUFDLEVBQUcsRUFBRTtZQUNqQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsRUFBRTtnQkFDdkcsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNmO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ2YsS0FBSyxDQUFDLElBQUksQ0FBQzt3QkFDVCxJQUFJLEVBQUUsSUFBSTtxQkFDWCxDQUFDLENBQUM7b0JBQ0gsVUFBVSxHQUFHLElBQUksQ0FBQztpQkFDbkI7YUFDRjtTQUNGO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsSUFBSSxrQkFBa0I7UUFDcEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFDeEMsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDL0IsTUFBTSxFQUFHLENBQUM7U0FDWDtRQUNELElBQUksSUFBSSxDQUFDLGtDQUFrQyxFQUFFO1lBQzNDLE1BQU0sRUFBRyxDQUFDO1NBQ1g7UUFDRCxJQUFJLElBQUksQ0FBQyw4QkFBOEIsRUFBRTtZQUN2QyxNQUFNLEVBQUcsQ0FBQztTQUNYO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVELElBQUksU0FBUztRQUNYLElBQUksSUFBSSxDQUFDLGNBQWMsS0FBSyx5QkFBeUIsQ0FBQyxNQUFNLEVBQUU7WUFDNUQsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1NBQ2pDO2FBQU07WUFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7U0FDbEM7SUFDSCxDQUFDO0lBRUQsSUFBSSxlQUFlO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN4SCxDQUFDO0lBRUQsSUFBSSxjQUFjO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELElBQUkscUJBQXFCO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLGNBQWMsSUFBSSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVELElBQUksb0JBQW9CO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUVELElBQUksY0FBYztRQUNoQixPQUFPLElBQUksQ0FBQyxjQUFjLElBQUksRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsSUFBSSxvQkFBb0I7UUFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNqRixDQUFDO0lBRUQsSUFBSSxpQkFBaUI7UUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLEVBQUU7WUFDM0MsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELElBQUksb0JBQW9CO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDNUIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsTUFBTSxFQUFFO1lBQzNDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxJQUFJLGlCQUFpQjtRQUNuQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxJQUFJLHVCQUF1QjtRQUN6QixPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxJQUFJLHVCQUF1QjtRQUN6QixPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCxJQUFJLHNDQUFzQzs7UUFDeEMsYUFBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxjQUFjLDBDQUFFLGdCQUFnQixDQUFDO0lBQ3ZGLENBQUM7SUFFRCxJQUFJLHNDQUFzQzs7UUFDeEMsYUFBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLDBDQUFFLHVCQUF1QixDQUFDO0lBQ3pGLENBQUM7SUFRRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztJQUNoQyxDQUFDO0lBRUQsSUFBSSxVQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDaEMsQ0FBQztJQUVELElBQUksZUFBZTtRQUNqQixNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNwQyxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO1lBQ3ZELE1BQU0sZUFBZSxHQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDeEgsTUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLEtBQUssTUFBTSxnQkFBZ0IsSUFBSSxlQUFlLEVBQUU7Z0JBQzlDLE1BQU0sY0FBYyxHQUFVLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2xFLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7b0JBQzNDLEtBQUssTUFBTSxPQUFPLElBQUksY0FBYyxFQUFFO3dCQUNwQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNyQjtpQkFDRjthQUNGO1lBQ0QsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDcEIsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDbkUsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDM0I7U0FDRjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxlQUFlO0lBRWYsaUJBQWlCLENBQUMsS0FBeUIsRUFBRSxJQUF3QixFQUFFLEVBQXNCO1FBQzNGLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztZQUNwQixJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ3JCLEtBQUs7WUFDTCxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hDLGNBQWMsRUFBRSxFQUFFLENBQUMsZUFBZSxFQUFFO1lBQ3BDLGFBQWEsRUFBRSxJQUFJO1lBQ25CLFdBQVcsRUFBRSxFQUFFO1NBQ2hCLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUF5QjtRQUN6QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pFLE1BQU0sYUFBYSxHQUE4QixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckcsTUFBTSxhQUFhLEdBQThCLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFN0YsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNsQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ3pDLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFO1lBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7WUFDeEUsT0FBTztTQUNSO1FBRUQsSUFBSSxhQUFhLEVBQUU7WUFDakIsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxhQUFhLEVBQUUsYUFBYSxDQUFDLENBQUM7U0FDdEU7UUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDM0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7WUFDcEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSTtZQUNyQixLQUFLO1lBQ0wsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDeEUsY0FBYyxFQUFFLGFBQWEsQ0FBQyxlQUFlLEVBQUU7WUFDL0MsYUFBYSxFQUFFLGFBQWE7WUFDNUIsV0FBVyxFQUFFLGFBQWE7U0FDM0IsQ0FBQyxDQUFDO1FBRUgsWUFBWTtRQUNaLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3pCLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzVDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG1CQUFtQjtRQUNqQixJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLElBQUksRUFBQyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELG1CQUFtQixDQUFDLElBQVM7UUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRTtZQUN4QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM1QixPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEU7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsaUNBQWlDLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2FBQ3pCO1lBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDL0I7UUFFRCxJQUFJLENBQUMsaUNBQWlDLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELG9CQUFvQixDQUFDLE1BQXdCO1FBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEVBQUU7WUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUNoRCxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3hDLE9BQU87U0FDUjtRQUVELE1BQU0sZUFBZSxHQUF1QztZQUMxRCxNQUFNO1lBQ04sYUFBYSxFQUFFLElBQUksQ0FBQyxZQUFZO1NBQ2pDLENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLGVBQWUsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUUxRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZUFBZSxDQUFDLFFBQWdCO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxNQUF3QjtRQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDaEQsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN2QyxPQUFPO1NBQ1I7UUFFRCxNQUFNLGVBQWUsR0FBdUM7WUFDMUQsTUFBTTtZQUNOLGFBQWEsRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNqQyxDQUFDO1FBRUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxlQUFlLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFFMUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELG1CQUFtQjs7UUFDakIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7O1lBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxFQUFFLENBQUMsRUFBRTtnQkFDNUUsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7b0JBQzVCLFVBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsU0FBUywwQ0FBRSx1QkFBdUIsRUFBRTt3QkFDbkYsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7cUJBQzVCO3lCQUFNO3dCQUNMLDBEQUEwRDtxQkFDM0Q7aUJBQ0Y7cUJBQU07b0JBQ0wsVUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLDBDQUFFLHNCQUFzQixFQUFFO3dCQUNsRixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztxQkFDNUI7aUJBQ0Y7YUFDRjtRQUNILENBQUMsUUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLDBDQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELFlBQVksQ0FBQyxTQUFpQixFQUFFLE9BQWlDO1FBQy9ELElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFNBQVMsRUFBRTtZQUN2QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ1osT0FBTyxHQUFHLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLElBQUksRUFBQyxDQUFDO1NBQ2hEO1FBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBRUQsYUFBYSxDQUFDLE1BQXdCO1FBQ3BDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEVBQUU7WUFDOUYsT0FBTztTQUNSO1FBRUQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQzFDLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztRQUVoRCxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDakQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMsYUFBYSxLQUFLLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNqRixzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3pFO2FBQU07WUFDTCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsTUFBTSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDL0Q7UUFFRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFdkIsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsb0JBQW9CO0lBRXBCLGlCQUFpQixDQUFDLElBQVM7UUFDekIsSUFBSSxJQUFJLENBQUMsOEJBQThCLEVBQUU7WUFDdkMsSUFBSyxJQUFJLENBQUMsY0FBc0IsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hDLE9BQVEsSUFBSSxDQUFDLGNBQStDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVFO2lCQUFNO2dCQUNMLE9BQU8sd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUF3QixDQUFDLENBQUM7YUFDdkY7U0FDRjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNoRCxDQUFDO0lBRUQsWUFBWSxDQUFDLEdBQVEsRUFBRSxNQUF3QjtRQUM3QyxPQUFPLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVELFlBQVksQ0FBQyxNQUFxRTtRQUNoRixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsbUJBQW1CLENBQUMsR0FBOEIsRUFBRSxLQUFpQztRQUNuRixJQUFJLEdBQUcsRUFBRTtZQUNQLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMzQzthQUFNLElBQUksS0FBSyxFQUFFO1lBQ2hCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsTUFBd0IsRUFBRSxTQUFTLEdBQUcsSUFBSTtRQUMvRCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDMUMsSUFBSSxVQUFVLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDLElBQUksRUFBRTtZQUMzRCxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLFNBQVMsS0FBSyxJQUFJLENBQUMsb0JBQW9CLENBQUM7U0FDaEQ7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsTUFBd0I7UUFDN0MsT0FBTyx3QkFBd0IsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUMvRyxDQUFDO0lBRUQscUJBQXFCLENBQUMsTUFBd0I7UUFDNUMsT0FBTyx3QkFBd0IsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUM5RyxDQUFDO0lBRUQsWUFBWSxDQUFDLEdBQVE7UUFDbkIsT0FBTyxJQUFJLENBQUMseUJBQXlCO1lBQ25DLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQsVUFBVSxDQUFDLE1BQXdCO1FBQ2pDLE9BQU8sd0JBQXdCLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLENBQUM7SUFFRCxVQUFVLENBQUMsTUFBd0I7UUFDakMsT0FBTyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDbkcsQ0FBQztJQUVELFlBQVksQ0FBQyxNQUF3QjtRQUNuQyxPQUFPLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUNyRyxDQUFDO0lBRUQsUUFBUSxDQUFDLE1BQXdCO1FBQy9CLE9BQU8sd0JBQXdCLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxNQUF3QjtRQUM1QyxPQUFPLHdCQUF3QixDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO0lBQzlHLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxNQUF3Qjs7UUFDN0MsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sUUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHVDQUF1QyxDQUFDLENBQUM7SUFDckksQ0FBQztJQUVELHFCQUFxQixDQUFDLE1BQXdCOztRQUM1QyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxRQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE9BQU8sMENBQUUsc0NBQXNDLENBQUMsQ0FBQztJQUNwSSxDQUFDO0lBRUQsZUFBZSxDQUFDLE1BQXdCLEVBQUUsR0FBbUQ7UUFDM0YsTUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixJQUFJLEdBQUcsQ0FBQztRQUNsRCxJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsQ0FBQyxNQUFNLEVBQUU7WUFDekQsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNLElBQUksT0FBTyxLQUFLLGtDQUFrQyxDQUFDLEtBQUssRUFBRTtZQUMvRCxPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU0sSUFBSSxPQUFPLEtBQUssa0NBQWtDLENBQUMsT0FBTyxFQUFFO1lBQ2pFLHNDQUFzQztZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxtRUFBbUUsQ0FBQyxDQUFDO2dCQUN2RixPQUFPLEtBQUssQ0FBQzthQUNkO1lBRUQsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUM7U0FDdkU7UUFFRCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQztRQUM3QyxJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRSxPQUFPLFdBQVcsS0FBSyxDQUFDLENBQUM7U0FDMUI7YUFBTSxJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsQ0FBQyxrQkFBa0IsRUFBRTtZQUM1RSxPQUFPLFdBQVcsR0FBRyxDQUFDLENBQUM7U0FDeEI7YUFBTSxJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsQ0FBQyxZQUFZLEVBQUU7WUFDdEUsT0FBTyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1NBQ3hCO2FBQU07WUFDTCxNQUFNLElBQUksS0FBSyxDQUFDLHVDQUF1QyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1NBQ3BFO0lBQ0gsQ0FBQztJQUVELCtCQUErQjtJQUUvQixVQUFVLENBQUMsSUFBUztRQUNsQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCw0QkFBNEI7SUFFNUIsU0FBUyxDQUFDLElBQVM7UUFDakIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsSUFBSSxVQUFVO1FBQ1osTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsS0FBSyxNQUFNLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDMUMsT0FBTyxLQUFLLENBQUM7YUFDZDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsSUFBSSxVQUFVO1FBQ1osTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsS0FBSyxNQUFNLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDMUMsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsSUFBSSxXQUFXO1FBQ2IsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDMUIsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFTO1FBQ3JCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM5RDthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7YUFDeEI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUNoQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2xDLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztTQUN4QjthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdkIsS0FBSyxNQUFNLEVBQUUsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM1QjtTQUNGO1FBRUQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFRCx3Q0FBd0M7SUFFeEMsMkJBQTJCLENBQUMsSUFBc0I7UUFDaEQsT0FBTyxJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxJQUFJLDBCQUEwQjtRQUM1QixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDckIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELEtBQUssTUFBTSxJQUFJLElBQUksU0FBUyxFQUFFO1lBQzVCLElBQUksSUFBSSxDQUFDLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDeEQsT0FBTyxLQUFLLENBQUM7YUFDZDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsSUFBSSwwQkFBMEI7UUFDNUIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxLQUFLLE1BQU0sSUFBSSxJQUFJLFNBQVMsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3hELE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELElBQUksd0JBQXdCO1FBQzFCLE9BQU8sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUM7SUFDMUMsQ0FBQztJQUVELDRCQUE0QixDQUFDLElBQXNCO1FBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDaEMsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDNUIsT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDMUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzFGO2FBQU07WUFDTCxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsa0VBQWtFLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0NBQW9DLEVBQUUsQ0FBQztJQUM5QyxDQUFDO0lBRUQsZ0NBQWdDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDaEMsT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDbkMsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztTQUN0QzthQUFNO1lBQ0wsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztZQUNyQyxLQUFLLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMxQztTQUNGO1FBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsa0VBQWtFLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0NBQW9DLEVBQUUsQ0FBQztJQUM5QyxDQUFDO0lBRUQsc0NBQXNDO0lBRXRDLCtCQUErQixDQUFDLElBQXNCO1FBQ3BELE9BQU8sSUFBSSxDQUFDLDhCQUE4QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRUQsSUFBSSx3QkFBd0I7UUFDMUIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsS0FBSyxNQUFNLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUM1RCxPQUFPLEtBQUssQ0FBQzthQUNkO1NBQ0Y7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxJQUFJLHdCQUF3QjtRQUMxQixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxLQUFLLE1BQU0sSUFBSSxJQUFJLFNBQVMsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzVELE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELElBQUksc0JBQXNCO1FBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUM7SUFDeEMsQ0FBQztJQUVELDBCQUEwQixDQUFDLElBQXNCO1FBQy9DLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEVBQUU7WUFDbkksT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDOUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2xHO2FBQU07WUFDTCxJQUFJLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hEO1FBRUQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCw4QkFBOEI7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyw2QkFBNkIsRUFBRTtZQUN2QyxPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNqQyxJQUFJLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FDNUQsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMxRjthQUFNO1lBQ0wsSUFBSSxDQUFDLDhCQUE4QixHQUFHLEVBQUUsQ0FBQztZQUN6QyxLQUFLLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDOUM7U0FDRjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsa0NBQWtDLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsb0NBQW9DO0lBRXBDLHNCQUFzQixDQUFDLElBQTZCO1FBQ2xELE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsSUFBSSx1QkFBdUI7UUFDekIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDO1FBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxLQUFLLE1BQU0sSUFBSSxJQUFJLFNBQVMsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ25ELE9BQU8sS0FBSyxDQUFDO2FBQ2Q7U0FDRjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELElBQUksdUJBQXVCO1FBQ3pCLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsS0FBSyxNQUFNLElBQUksSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNuRCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxJQUFJLHVCQUF1QjtRQUN6QixPQUFPLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDO0lBQ3ZDLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxJQUE2QjtRQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFO1lBQ3ZDLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNoRjthQUFNO1lBQ0wsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ2pFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFFLENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjtRQUVELElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDO1FBRTFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU8sbUJBQW1CO1FBQ3pCLE9BQU87WUFDTCxhQUFhLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsb0JBQW9CLElBQUksTUFBTTtZQUMxRixXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ3hFLG9CQUFvQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxJQUFJO1lBQ3ZELEtBQUssRUFBRSxJQUFJLENBQUMsa0JBQWtCO1lBQzlCLFlBQVksRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUM5RCxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3BELGNBQWMsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUMzRCxXQUFXLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtZQUNsQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDOUIsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQy9FLGFBQWEsRUFBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN0RixDQUFDO0lBQ0osQ0FBQztJQUVPLDhCQUE4QjtRQUNwQyxPQUFPO1lBQ0wsYUFBYSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLG9CQUFvQixJQUFJLE1BQU07WUFDMUYsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN4RSxvQkFBb0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSTtZQUN2RCxLQUFLLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtZQUM5QixZQUFZLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDOUQsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNwRCxjQUFjLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDM0QsV0FBVyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDbEMsUUFBUSxFQUFFLElBQUksQ0FBQyxlQUFlO1lBQzlCLHNCQUFzQixFQUFFLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDakUsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlFLHVCQUF1QixFQUFHLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hGLENBQUM7SUFDSixDQUFDO0lBRU8sYUFBYTtRQUNuQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2pELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztRQUN2RSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUUxRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU8sb0JBQW9CO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3hCLE9BQU8sVUFBVSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDeEM7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFFLFVBQVUsQ0FBQyxFQUFFO1lBQ2xDLElBQUksSUFBSSxDQUFDLGtDQUFrQyxFQUFFO2dCQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO2dCQUN6RCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFO29CQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQzt3QkFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyx5QkFBeUI7cUJBQ3ZDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7d0JBQ2hFLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDbEIsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscURBQXFELEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ2pGLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzFCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO29CQUN0RCxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0Y7WUFDRCxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbEIsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLG1CQUFtQjtRQUN6QixPQUFPLElBQUksVUFBVSxDQUFFLFVBQVUsQ0FBQyxFQUFFO1lBQ2xDLElBQUksSUFBSSxDQUFDLGtDQUFrQyxFQUFFO2dCQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFO29CQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQzt3QkFDcEUsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDeEIsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ1gsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsd0RBQXdELEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ3BGLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO29CQUN0RCxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0Y7WUFDRCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFJLGNBQWM7UUFDaEIsT0FBTyxDQUNMLENBQUMsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLE1BQU07WUFDckMsQ0FBQyxDQUFDLCtCQUErQixDQUFDLENBQUMsTUFBTTtZQUN6QyxDQUFDLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxNQUFNO1lBQzNDLENBQUMsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLE1BQU0sQ0FDN0MsR0FBRyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRUQsaUJBQWlCO0lBRVQsb0JBQW9CO1FBQzFCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU8sZUFBZTtRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNuQixNQUFNLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtZQUM5QixTQUFTLEVBQUUsSUFBSSxDQUFDLG9CQUFvQjtTQUNyQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sZUFBZTtRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU8sb0NBQW9DO1FBQzFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVPLGtDQUFrQztRQUN4QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFTyxrQ0FBa0M7UUFDeEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRU8saUJBQWlCO1FBQ3ZCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDN0M7SUFDSCxDQUFDO0lBRUQsb0JBQW9CO0lBQ3BCLElBQUksWUFBWTtRQUNkLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDO0lBRUQsSUFBSSx5QkFBeUI7UUFDM0IsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2IsSUFBSSxJQUFJLENBQUMsOEJBQThCLEVBQUU7WUFDdkMsQ0FBQyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDL0IsQ0FBQyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxJQUFJLENBQUMsa0NBQWtDLEVBQUU7WUFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdELE9BQU8sQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELElBQUksc0JBQXNCO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDO0lBRUQsaUNBQWlDO1FBQy9CLE1BQU0sSUFBSSxHQUFVLEVBQUUsQ0FBQztRQUV2QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNsQyxNQUFNLFNBQVMsbUJBQUssbUJBQW1CLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxPQUFPLElBQUssT0FBTyxDQUFFLENBQUM7WUFDbkYsT0FBTyxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7WUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxnREFBZ0Q7UUFDaEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMscUNBQXFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVyRixVQUFVLENBQUMsR0FBRyxFQUFFOztZQUNkLE1BQUEsSUFBSSxDQUFDLHNCQUFzQiwwQ0FBRSxJQUFJLENBQUMsSUFBSSxFQUFFO1FBQzFDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQTJCRCxJQUFJLDZCQUE2QjtRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNkLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUNqQyxPQUFPLElBQUksQ0FBQztTQUNiO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELElBQUksNkJBQTZCO1FBQy9CLE9BQU8sSUFBSSxDQUFDO1FBRVosSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDZCxPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7QUF0MERjLDBCQUFPLEdBQUcsQ0FBQyxDQUFDO29GQUZoQixrQkFBa0I7dURBQWxCLGtCQUFrQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ3BFL0IsOEJBRUU7UUFBQSxvRUFHRTtRQTRORix1RkFDQTtRQXVLQSxxRkFDQTtRQTJKQSxvRUFHRTtRQWlGSixpQkFBTTs7UUF0bkJGLGVBQStJO1FBQS9JLDhLQUErSTtRQThObkksZUFBc0M7UUFBdEMseURBQXNDO1FBd0t0QyxlQUFxQztRQUFyQyx3REFBcUM7UUE2SmpELGVBQXNHO1FBQXRHLGlJQUFzRztnNktEMWU1RjtZQUNWLE9BQU8sQ0FBQyxjQUFjLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRixLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hFLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FBQzthQUN0RixDQUFDO1NBQ0g7a0RBRVUsa0JBQWtCO2NBYjlCLFNBQVM7ZUFBQztnQkFDVCwrQ0FBK0M7Z0JBQy9DLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixXQUFXLEVBQUUsNkJBQTZCO2dCQUMxQyxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsRUFBRSwwQkFBMEIsQ0FBQztnQkFDdEUsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxjQUFjLEVBQUU7d0JBQ3RCLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO3dCQUNsRixLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7d0JBQ2hFLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FBQztxQkFDdEYsQ0FBQztpQkFDSDthQUNGOztrQkFNRSxLQUFLOztrQkFDTCxLQUFLOztrQkFFTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFHTCxNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFDTixNQUFNOztrQkFHTixZQUFZO21CQUFDLGNBQWM7O2tCQUMzQixZQUFZO21CQUFDLHdCQUF3Qjs7a0JBQ3JDLFlBQVk7bUJBQUMsbUJBQW1COztrQkFDaEMsWUFBWTttQkFBQyxjQUFjOztrQkFDM0IsWUFBWTttQkFBQyxjQUFjOztrQkFFM0IsU0FBUzttQkFBQyxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgT25EZXN0cm95LFxyXG4gIElucHV0LFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgQ29udGVudENoaWxkLFxyXG4gIFRlbXBsYXRlUmVmLFxyXG4gIENoYW5nZURldGVjdG9yUmVmLFxyXG4gIE5nWm9uZSxcclxuICBPbkNoYW5nZXMsXHJcbiAgU2ltcGxlQ2hhbmdlcyxcclxuICBBZnRlckNvbnRlbnRJbml0LFxyXG4gIFZpZXdDaGlsZFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIE1haXNUYWJsZVNvcnREaXJlY3Rpb24sXHJcbiAgSU1haXNUYWJsZVBhZ2VSZXF1ZXN0LFxyXG4gIElNYWlzVGFibGVQYWdlUmVzcG9uc2UsXHJcbiAgSU1haXNUYWJsZUNvbHVtbixcclxuICBNYWlzVGFibGVQYWdpbmF0aW9uTWV0aG9kLFxyXG4gIElNYWlzVGFibGVTb3J0aW5nU3BlY2lmaWNhdGlvbixcclxuICBJTWFpc1RhYmxlQWN0aW9uLFxyXG4gIElNYWlzVGFibGVBY3Rpb25EaXNwYXRjaGluZ0NvbnRleHQsXHJcbiAgTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbixcclxuICBJTWFpc1RhYmxlQ29udGV4dEZpbHRlcixcclxuICBJTWFpc1RhYmxlU3RhdHVzU25hcHNob3QsXHJcbiAgSU1haXNUYWJsZVN0b3JlQWRhcHRlcixcclxuICBJTWFpc1RhYmxlSWRlbnRpZmllclByb3ZpZGVyLFxyXG4gIElNYWlzVGFibGVJdGVtRHJhZ2dlZENvbnRleHQsXHJcbiAgSU1haXNUYWJsZUl0ZW1Ecm9wcGVkQ29udGV4dCxcclxuICBNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3ksXHJcbiAgSU1haXNUYWJsZVB1c2hSZWZyZXNoUmVxdWVzdCxcclxuICBJTWFpc1RhYmxlUmVsb2FkQ29udGV4dCxcclxuICBNYWlzVGFibGVSZWxvYWRSZWFzb24sXHJcbiAgSU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3RcclxufSBmcm9tICcuL21vZGVsJztcclxuaW1wb3J0IHtcclxuICBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIsXHJcbiAgTWFpc1RhYmxlSW5NZW1vcnlIZWxwZXIsXHJcbiAgTWFpc1RhYmxlVmFsaWRhdG9ySGVscGVyLFxyXG4gIE1haXNUYWJsZUJyb3dzZXJIZWxwZXIsXHJcbiAgTWFpc1RhYmxlTG9nZ2VyXHJcbn0gZnJvbSAnLi91dGlscyc7XHJcbmltcG9ydCB7IE1haXNUYWJsZVJlZ2lzdHJ5U2VydmljZSwgTWFpc1RhYmxlU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMnO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCwgU3Vic2NyaWJlciwgdGhyb3dFcnJvciwgU3Vic2NyaXB0aW9uLCB0aW1lciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHsgQ2RrRHJhZ0Ryb3AsIENka0RyYWcgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcclxuaW1wb3J0IHsgYW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IE1hdFRhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBjb21wb25lbnQtc2VsZWN0b3JcclxuICBzZWxlY3RvcjogJ21haXMtdGFibGUnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9tYWlzLXRhYmxlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9tYWlzLXRhYmxlLmNvbXBvbmVudC5zY3NzJywgJy4vbWFpcy10YWJsZS1wb3J0aW5nLmNzcyddLFxyXG4gIGFuaW1hdGlvbnM6IFtcclxuICAgIHRyaWdnZXIoJ2RldGFpbEV4cGFuZCcsIFtcclxuICAgICAgc3RhdGUoJ2NvbGxhcHNlZCcsIHN0eWxlKHsgaGVpZ2h0OiAnMHB4JywgbWluSGVpZ2h0OiAnMCcsIHZpc2liaWxpdHk6ICdoaWRkZW4nIH0pKSxcclxuICAgICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyBoZWlnaHQ6ICcqJywgdmlzaWJpbGl0eTogJ3Zpc2libGUnIH0pKSxcclxuICAgICAgdHJhbnNpdGlvbignZXhwYW5kZWQgPD0+IGNvbGxhcHNlZCcsIGFuaW1hdGUoJzIyNW1zIGN1YmljLWJlemllcigwLjQsIDAuMCwgMC4yLCAxKScpKSxcclxuICAgIF0pLFxyXG4gIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYWlzVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzLCBBZnRlckNvbnRlbnRJbml0IHtcclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgY291bnRlciA9IDA7XHJcblxyXG4gIC8vIGlucHV0IHBhcmFtZXRlcnNcclxuICBASW5wdXQoKSBkYXRhOiBhbnlbXSB8IE9ic2VydmFibGU8YW55PiB8IFN1YmplY3Q8YW55PjtcclxuICBASW5wdXQoKSBkYXRhUHJvdmlkZXI6ICgoaW5wdXQ6IElNYWlzVGFibGVQYWdlUmVxdWVzdCwgY29udGV4dD86IElNYWlzVGFibGVSZWxvYWRDb250ZXh0KVxyXG4gICAgPT4gT2JzZXJ2YWJsZTxJTWFpc1RhYmxlUGFnZVJlc3BvbnNlPikgfCBudWxsID0gbnVsbDtcclxuICBASW5wdXQoKSBjb2x1bW5zOiBJTWFpc1RhYmxlQ29sdW1uW107XHJcbiAgQElucHV0KCkgZGVmYXVsdFNvcnRpbmdDb2x1bW46IHN0cmluZyB8IG51bGwgPSBudWxsO1xyXG4gIEBJbnB1dCgpIGRlZmF1bHRTb3J0aW5nRGlyZWN0aW9uOiBzdHJpbmcgfCBudWxsID0gbnVsbDtcclxuICBASW5wdXQoKSBkZWZhdWx0UGFnZVNpemU6IG51bWJlciB8IG51bGwgPSBudWxsO1xyXG4gIEBJbnB1dCgpIHBvc3NpYmxlUGFnZVNpemU6IG51bWJlcltdIHwgbnVsbCA9IG51bGw7XHJcbiAgQElucHV0KCkgdGFibGVJZDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGRyb3BDb25uZWN0ZWRUbzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHBhZ2luYXRpb25Nb2RlOiBNYWlzVGFibGVQYWdpbmF0aW9uTWV0aG9kO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVBhZ2luYXRpb246IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlU2VsZWN0aW9uOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZUNvbnRleHRGaWx0ZXJpbmc6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlU2VsZWN0QWxsOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZU11bHRpU2VsZWN0OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZUZpbHRlcmluZzogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVDb2x1bW5zU2VsZWN0aW9uOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZUNvbnRleHRBY3Rpb25zOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZUhlYWRlckFjdGlvbnM6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlUm93RXhwYW5zaW9uOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVBhZ2VTaXplU2VsZWN0OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZU11bHRpcGxlUm93RXhwYW5zaW9uOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZUl0ZW1UcmFja2luZzogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVTdG9yZVBlcnNpc3RlbmNlOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZURyYWc6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlRHJvcDogYm9vbGVhbjtcclxuICBASW5wdXQoKSBjb250ZXh0QWN0aW9uczogSU1haXNUYWJsZUFjdGlvbltdO1xyXG4gIEBJbnB1dCgpIGhlYWRlckFjdGlvbnM6IElNYWlzVGFibGVBY3Rpb25bXTtcclxuICBASW5wdXQoKSBjb250ZXh0RmlsdGVyczogSU1haXNUYWJsZUNvbnRleHRGaWx0ZXJbXTtcclxuICBASW5wdXQoKSBjb250ZXh0RmlsdGVyaW5nUHJvbXB0OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgYWN0aW9uU3RhdHVzUHJvdmlkZXI6IChhY3Rpb246IElNYWlzVGFibGVBY3Rpb24sIHN0YXR1czogSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbCkgPT4gYm9vbGVhbjtcclxuICBASW5wdXQoKSBleHBhbmRhYmxlU3RhdHVzUHJvdmlkZXI6IChyb3c6IGFueSwgc3RhdHVzOiBJTWFpc1RhYmxlU3RhdHVzU25hcHNob3QgfCBudWxsKSA9PiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIHN0b3JlQWRhcHRlcjogSU1haXNUYWJsZVN0b3JlQWRhcHRlcjtcclxuICBASW5wdXQoKSBpdGVtSWRlbnRpZmllcjogc3RyaW5nIHwgSU1haXNUYWJsZUlkZW50aWZpZXJQcm92aWRlcjtcclxuICBASW5wdXQoKSByZWZyZXNoU3RyYXRlZ3k6IE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneSB8IE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneVtdO1xyXG4gIEBJbnB1dCgpIHJlZnJlc2hJbnRlcnZhbDogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIHJlZnJlc2hFbWl0dGVyOiBPYnNlcnZhYmxlPElNYWlzVGFibGVQdXNoUmVmcmVzaFJlcXVlc3Q+O1xyXG4gIEBJbnB1dCgpIHJlZnJlc2hJbnRlcnZhbEluQmFja2dyb3VuZDogYm9vbGVhbjtcclxuICBASW5wdXQoKSByZWZyZXNoT25QdXNoSW5CYWNrZ3JvdW5kOiBib29sZWFuO1xyXG5cclxuICAvLyBvdXRwdXQgZXZlbnRzXHJcbiAgQE91dHB1dCgpIHBhZ2VDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPG51bWJlcj4oKTtcclxuICBAT3V0cHV0KCkgc29ydENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8SU1haXNUYWJsZVNvcnRpbmdTcGVjaWZpY2F0aW9uPigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3Rpb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gIEBPdXRwdXQoKSBmaWx0ZXJpbmdDb2x1bW5zQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlQ29sdW1uW10+KCk7XHJcbiAgQE91dHB1dCgpIHZpc2libGVDb2x1bW5zQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlQ29sdW1uW10+KCk7XHJcbiAgQE91dHB1dCgpIGNvbnRleHRGaWx0ZXJzQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlQ29udGV4dEZpbHRlcltdPigpO1xyXG4gIEBPdXRwdXQoKSBzdGF0dXNDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPElNYWlzVGFibGVTdGF0dXNTbmFwc2hvdD4oKTtcclxuICBAT3V0cHV0KCkgaXRlbURyYWdnZWQgPSBuZXcgRXZlbnRFbWl0dGVyPElNYWlzVGFibGVJdGVtRHJhZ2dlZENvbnRleHQ+KCk7XHJcbiAgQE91dHB1dCgpIGl0ZW1Ecm9wcGVkID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlSXRlbURyb3BwZWRDb250ZXh0PigpO1xyXG4gIEBPdXRwdXQoKSBhY3Rpb24gPSBuZXcgRXZlbnRFbWl0dGVyPElNYWlzVGFibGVBY3Rpb25EaXNwYXRjaGluZ0NvbnRleHQ+KCk7XHJcblxyXG4gIC8vIGlucHV0IHRlbXBsYXRlXHJcbiAgQENvbnRlbnRDaGlsZCgnY2VsbFRlbXBsYXRlJykgY2VsbFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gIEBDb250ZW50Q2hpbGQoJ2FjdGlvbnNDYXB0aW9uVGVtcGxhdGUnKSBhY3Rpb25zQ2FwdGlvblRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gIEBDb250ZW50Q2hpbGQoJ3Jvd0RldGFpbFRlbXBsYXRlJykgcm93RGV0YWlsVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XHJcbiAgQENvbnRlbnRDaGlsZCgnZHJhZ1RlbXBsYXRlJykgZHJhZ1RlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gIEBDb250ZW50Q2hpbGQoJ2Ryb3BUZW1wbGF0ZScpIGRyb3BUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuXHJcbiAgQFZpZXdDaGlsZCgnI3JlbmRlcmVkTWF0VGFibGUnKSB0YWJsZTogTWF0VGFibGU8YW55PjtcclxuXHJcbiAgcHJpdmF0ZSBpc0lFID0gTWFpc1RhYmxlQnJvd3NlckhlbHBlci5pc0lFKCk7XHJcblxyXG4gIHByaXZhdGUgbG9nZ2VyOiBNYWlzVGFibGVMb2dnZXI7XHJcbiAgcHJpdmF0ZSByZWdpc3RyYXRpb25JZDogc3RyaW5nO1xyXG4gIHByaXZhdGUgc3RhdHVzU25hcHNob3Q6IElNYWlzVGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGwgPSBudWxsO1xyXG4gIHByaXZhdGUgcGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdDogSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbCA9IG51bGw7XHJcbiAgcHJpdmF0ZSB1dWlkOiBzdHJpbmc7XHJcbiAgZm9yY2VSZVJlbmRlciA9IGZhbHNlO1xyXG4gIHByaXZhdGUgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuXHJcbiAgLy8gbG9jYWwgZGF0YVxyXG4gIHByaXZhdGUgY2xpZW50RGF0YVNuYXBzaG90OiBhbnlbXTtcclxuICBkYXRhU25hcHNob3Q6IGFueVtdO1xyXG5cclxuICBwcml2YXRlIHNlbGVjdGVkUGFnZUluZGV4OiBudW1iZXI7XHJcbiAgcHJpdmF0ZSBzZWxlY3RlZFBhZ2VTaXplOiBudW1iZXIgfCBudWxsO1xyXG5cclxuICBwcml2YXRlIHNlbGVjdGVkU29ydENvbHVtbjogSU1haXNUYWJsZUNvbHVtbiB8IG51bGw7XHJcbiAgcHJpdmF0ZSBzZWxlY3RlZFNvcnREaXJlY3Rpb246IE1haXNUYWJsZVNvcnREaXJlY3Rpb247XHJcblxyXG4gIHNlbGVjdGVkU2VhcmNoUXVlcnk6IHN0cmluZyB8IG51bGw7XHJcbiAgcHJpdmF0ZSBsYXN0RmV0Y2hlZFNlYXJjaFF1ZXJ5OiBzdHJpbmcgfCBudWxsO1xyXG5cclxuICAvLyBzZWxlY3RlZCBpdGVtcyB3aXRoIGNoZWNrYm94XHJcbiAgcHJpdmF0ZSBjaGVja2VkSXRlbXM6IGFueVtdO1xyXG5cclxuICAvLyBzZWxlY3RlZCBjb2x1bW5zIGZvciBmaWx0ZXJpbmdcclxuICBwcml2YXRlIGNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nOiBJTWFpc1RhYmxlQ29sdW1uW107XHJcblxyXG4gIC8vIHNlbGVjdGVkIGNvbHVtbnMgZm9yIHZpc3VhbGl6YXRpb25cclxuICBwcml2YXRlIGNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbjogSU1haXNUYWJsZUNvbHVtbltdO1xyXG5cclxuICAvLyBzZWxlY3RlZCBjdXN0b20gZmlsdGVyc1xyXG4gIHByaXZhdGUgY2hlY2tlZENvbnRleHRGaWx0ZXJzOiBJTWFpc1RhYmxlQ29udGV4dEZpbHRlcltdO1xyXG5cclxuICAvLyBleHBhbmRlZCByb3dzXHJcbiAgcHJpdmF0ZSBleHBhbmRlZEl0ZW1zOiBhbnlbXTtcclxuXHJcbiAgLy8gZm9yIHNlcnZlciBwYWdpbmF0aW9uXHJcbiAgcHJpdmF0ZSBmZXRjaGVkUGFnZUNvdW50OiBudW1iZXIgfCBudWxsO1xyXG4gIHByaXZhdGUgZmV0Y2hlZFJlc3VsdE51bWJlcjogbnVtYmVyIHwgbnVsbDtcclxuICBmZXRjaGluZyA9IGZhbHNlO1xyXG4gIHNob3dGZXRjaGluZyA9IGZhbHNlO1xyXG5cclxuICBwcml2YXRlIHJlZnJlc2hFbWl0dGVyU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb24gfCBudWxsID0gbnVsbDtcclxuICBwcml2YXRlIHJlZnJlc2hJbnRlcnZhbFRpbWVyOiBPYnNlcnZhYmxlPG51bWJlcj4gfCBudWxsO1xyXG4gIHByaXZhdGUgcmVmcmVzaEludGVydmFsU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb24gfCBudWxsID0gbnVsbDtcclxuXHJcbiAgLy8gTWF0VGFibGUgYWRhcHRlclxyXG4gIG1hdFRhYmxlRGF0YU9ic2VydmFibGU6IFN1YmplY3Q8YW55W10+IHwgbnVsbCA9IG51bGw7XHJcbiAgZXhwYW5kZWRFbGVtZW50OiBhbnkgPSBudWxsO1xyXG5cclxuICAvLyBsaWZlY3ljbGUgaG9va3NcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHRyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNvbmZpZ3VyYXRpb25TZXJ2aWNlOiBNYWlzVGFibGVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByZWdpc3RyeTogTWFpc1RhYmxlUmVnaXN0cnlTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjZHI6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSApIHtcclxuXHJcbiAgICB0aGlzLnV1aWQgPSAnTVRDTVAtJyArICgrK01haXNUYWJsZUNvbXBvbmVudC5jb3VudGVyKSArICctJyArIE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDEwMDAwMCk7XHJcbiAgICB0aGlzLmxvZ2dlciA9IG5ldyBNYWlzVGFibGVMb2dnZXIoJ01haXNUYWJsZUNvbXBvbmVudF8nICsgdGhpcy5jdXJyZW50VGFibGVJZCk7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnYnVpbGRpbmcgY29tcG9uZW50Jyk7XHJcblxyXG4gICAgdGhpcy5tYXRUYWJsZURhdGFPYnNlcnZhYmxlID0gbmV3IFN1YmplY3Q8YW55W10+KCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMubG9nZ2VyID0gbmV3IE1haXNUYWJsZUxvZ2dlcignTWFpc1RhYmxlQ29tcG9uZW50XycgKyB0aGlzLmN1cnJlbnRUYWJsZUlkKTtcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdpbml0aWFsaXppbmcgY29tcG9uZW50Jyk7XHJcblxyXG4gICAgdGhpcy5yZWdpc3RyYXRpb25JZCA9IHRoaXMucmVnaXN0cnkucmVnaXN0ZXIodGhpcy5jdXJyZW50VGFibGVJZCwgdGhpcyk7XHJcblxyXG4gICAgLy8gaW5wdXQgdmFsaWRhdGlvblxyXG4gICAgTWFpc1RhYmxlVmFsaWRhdG9ySGVscGVyLnZhbGlkYXRlQ29sdW1uc1NwZWNpZmljYXRpb24odGhpcy5jb2x1bW5zLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSk7XHJcblxyXG4gICAgdGhpcy5jbGllbnREYXRhU25hcHNob3QgPSBbXTtcclxuICAgIHRoaXMuZGF0YVNuYXBzaG90ID0gW107XHJcbiAgICB0aGlzLmZldGNoZWRQYWdlQ291bnQgPSAwO1xyXG4gICAgdGhpcy5mZXRjaGVkUmVzdWx0TnVtYmVyID0gMDtcclxuICAgIHRoaXMuc2VsZWN0ZWRQYWdlSW5kZXggPSAwO1xyXG4gICAgdGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5ID0gbnVsbDtcclxuICAgIHRoaXMuc2VsZWN0ZWRQYWdlU2l6ZSA9IG51bGw7XHJcbiAgICB0aGlzLmV4cGFuZGVkSXRlbXMgPSBbXTtcclxuICAgIHRoaXMuY2hlY2tlZEl0ZW1zID0gW107XHJcbiAgICB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycyA9IFtdO1xyXG4gICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZyA9IHRoaXMuZmlsdGVyYWJsZUNvbHVtbnMuZmlsdGVyKFxyXG4gICAgICBjID0+IE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc0RlZmF1bHRGaWx0ZXJDb2x1bW4oYywgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKTtcclxuICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uID0gdGhpcy5jb2x1bW5zLmZpbHRlcihcclxuICAgICAgYyA9PiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNEZWZhdWx0VmlzaWJsZUNvbHVtbihjLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSkpO1xyXG4gICAgdGhpcy5zdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRTdGF0dXNTbmFwc2hvdCgpO1xyXG4gICAgdGhpcy5wZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90ID0gdGhpcy5idWlsZFBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QoKTtcclxuXHJcbiAgICBpZiAodGhpcy5kYXRhUHJvdmlkZXIpIHtcclxuICAgICAgLy8gaW5wdXQgaXMgYSBwcm92aWRlciBmdW5jdGlvblxyXG4gICAgICBpZiAodGhpcy5kYXRhKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNYWlzVGFibGUgY2FuXFwndCBiZSBwcm92aWRlZCBib3RoIGRhdGEgYW5kIGRhdGFQcm92aWRlcicpO1xyXG4gICAgICB9XHJcblxyXG4gICAgfSBlbHNlIGlmICh0aGlzLmRhdGEgaW5zdGFuY2VvZiBPYnNlcnZhYmxlIHx8IHRoaXMuZGF0YSBpbnN0YW5jZW9mIFN1YmplY3QpIHtcclxuICAgICAgLy8gaW5wdXQgaXMgb2JzZXJ2YWJsZVxyXG4gICAgICB0aGlzLmRhdGEuc3Vic2NyaWJlKGRhdGFTbmFwc2hvdCA9PiB7XHJcbiAgICAgICAgdGhpcy5oYW5kbGVJbnB1dERhdGFPYnNlcnZhYmxlRW1pc3Npb24oZGF0YVNuYXBzaG90KTtcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBpbnB1dCBpcyBkYXRhIGFycmF5XHJcbiAgICAgIHRoaXMuY2xpZW50RGF0YVNuYXBzaG90ID0gdGhpcy5kYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIExPQUQgU1RBVFVTIGZyb20gc3RvcmUgaWYgcG9zc2libGVcclxuICAgIGxldCBhY3RpdmF0aW9uT2JzZXJ2YWJsZTogT2JzZXJ2YWJsZTxJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGw+O1xyXG4gICAgaWYgKHRoaXMuc3RvcmVQZXJzaXN0ZW5jZUVuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICBhY3RpdmF0aW9uT2JzZXJ2YWJsZSA9IHRoaXMubG9hZFN0YXR1c0Zyb21TdG9yZSgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgYWN0aXZhdGlvbk9ic2VydmFibGUgPSBuZXcgT2JzZXJ2YWJsZShzdWJzY3JpYmVyID0+IHtcclxuICAgICAgICBzdWJzY3JpYmVyLm5leHQobnVsbCk7XHJcbiAgICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhY3RpdmF0aW9uT2JzZXJ2YWJsZS5zdWJzY3JpYmUoKHN0YXR1czogSU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QpID0+IHtcclxuICAgICAgaWYgKHN0YXR1cykge1xyXG4gICAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdyZXN0b3JlZCBzdGF0dXMgc25hcHNob3QgZnJvbSBzdG9yYWdlJywgc3RhdHVzKTtcclxuICAgICAgICB0aGlzLmFwcGx5U3RhdHVzU25hcHNob3Qoc3RhdHVzKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5yZWxvYWQoe3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLklOVEVSTkFMLCB3aXRoU3RhdHVzU25hcHNob3Q6IHN0YXR1c30pLnN1YnNjcmliZSh5ZXMgPT4ge1xyXG4gICAgICAgIHRoaXMuY29tcGxldGVJbml0aWFsaXphdGlvbigpO1xyXG4gICAgICB9LCBub3BlID0+IHtcclxuICAgICAgICB0aGlzLmNvbXBsZXRlSW5pdGlhbGl6YXRpb24oKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfSwgZmFpbHVyZSA9PiB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLmVycm9yKCdyZXN0b3Jpbmcgc3RhdHVzIGZyb20gc3RvcmUgZmFpbGVkJywgZmFpbHVyZSk7XHJcbiAgICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5JTlRFUk5BTH0pLnN1YnNjcmliZSh5ZXMgPT4ge1xyXG4gICAgICAgIHRoaXMuY29tcGxldGVJbml0aWFsaXphdGlvbigpO1xyXG4gICAgICB9LCBub3BlID0+IHtcclxuICAgICAgICB0aGlzLmNvbXBsZXRlSW5pdGlhbGl6YXRpb24oKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdhZnRlciBjb250ZW50IGluaXQnKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlSW5wdXREYXRhT2JzZXJ2YWJsZUVtaXNzaW9uKGRhdGFTbmFwc2hvdDogYW55W10pIHtcclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdkYXRhIHNuYXBzaG90IGVtaXR0ZWQgZnJvbSBpbnB1dCBkYXRhJyk7XHJcbiAgICB0aGlzLmNsaWVudERhdGFTbmFwc2hvdCA9IGRhdGFTbmFwc2hvdDtcclxuICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5FWFRFUk5BTH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjb21wbGV0ZUluaXRpYWxpemF0aW9uKCkge1xyXG4gICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XHJcblxyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2UucGlwZShkZWJvdW5jZVRpbWUoMjAwKSkuc3Vic2NyaWJlKHN0YXR1c1NuYXBzaG90ID0+IHtcclxuICAgICAgdGhpcy5wZXJzaXN0U3RhdHVzVG9TdG9yZSgpLnN1YnNjcmliZSgpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucmVmcmVzaEVtaXR0ZXIpIHtcclxuICAgICAgdGhpcy5oYW5kbGVSZWZyZXNoRW1pdHRlckNoYW5nZSh0aGlzLnJlZnJlc2hFbWl0dGVyKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnJlZnJlc2hJbnRlcnZhbCkge1xyXG4gICAgICB0aGlzLmhhbmRsZVJlZnJlc2hJbnRlcnZhbENoYW5nZSh0aGlzLnJlZnJlc2hJbnRlcnZhbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdkZXN0cm95aW5nIGNvbXBvbmVudCcpO1xyXG4gICAgaWYgKHRoaXMucmVnaXN0cmF0aW9uSWQpIHtcclxuICAgICAgdGhpcy5yZWdpc3RyeS51bnJlZ2lzdGVyKHRoaXMuY3VycmVudFRhYmxlSWQsIHRoaXMucmVnaXN0cmF0aW9uSWQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMucmVmcmVzaEVtaXR0ZXIpIHtcclxuICAgICAgdGhpcy5oYW5kbGVSZWZyZXNoRW1pdHRlckNoYW5nZShjaGFuZ2VzLnJlZnJlc2hFbWl0dGVyLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhbmdlcy5yZWZyZXNoSW50ZXJ2YWwpIHtcclxuICAgICAgdGhpcy5oYW5kbGVSZWZyZXNoSW50ZXJ2YWxDaGFuZ2UoY2hhbmdlcy5yZWZyZXNoSW50ZXJ2YWwuY3VycmVudFZhbHVlKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzLnJlZnJlc2hTdHJhdGVneSAmJiAhY2hhbmdlcy5yZWZyZXNoU3RyYXRlZ3kuZmlyc3RDaGFuZ2UpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignUkVGUkVTSCBTVFJBVEVHWSBDSEFOR0VEIFdISUxFIFJVTk5JTkcuIFlPVSBTVVJFIEFCT1VUIFRISVM/JywgY2hhbmdlcyk7XHJcbiAgICAgIHRoaXMuaGFuZGxlUmVmcmVzaEVtaXR0ZXJDaGFuZ2UodGhpcy5yZWZyZXNoRW1pdHRlcik7XHJcbiAgICAgIHRoaXMuaGFuZGxlUmVmcmVzaEludGVydmFsQ2hhbmdlKHRoaXMucmVmcmVzaEludGVydmFsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlUmVmcmVzaEVtaXR0ZXJDaGFuZ2UobmV3VmFsdWU6IE9ic2VydmFibGU8SU1haXNUYWJsZVB1c2hSZWZyZXNoUmVxdWVzdD4pIHtcclxuICAgIGlmICh0aGlzLnJlZnJlc2hFbWl0dGVyU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmVmcmVzaEVtaXR0ZXJTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIGlmIChuZXdWYWx1ZSkge1xyXG4gICAgICB0aGlzLnJlZnJlc2hFbWl0dGVyU3Vic2NyaXB0aW9uID0gbmV3VmFsdWUuc3Vic2NyaWJlKGV2ZW50ID0+IHtcclxuICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgncmVjZWl2ZWQgcmVmcmVzaCBwdXNoIHJlcXVlc3QnLCBldmVudCk7XHJcbiAgICAgICAgaWYgKHRoaXMuZHJhZ0luUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ3JlZnJlc2ggZnJvbSBlbWl0dGVyIGlnbm9yZWQgYmVjYXVzZSB1c2VyIGlzIGRyYWdnaW5nIGVsZW1lbnRzJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmN1cnJlbnRSZWZyZXNoU3RyYXRlZ2llcy5pbmRleE9mKE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneS5PTl9QVVNIKSA9PT0gLTEpIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ3JlZnJlc2ggZnJvbSBlbWl0dGVyIGlnbm9yZWQgYmVjYXVzZSByZWZyZXNoIHN0cmF0ZWdpZXMgYXJlICcgKyB0aGlzLmN1cnJlbnRSZWZyZXNoU3RyYXRlZ2llcyArXHJcbiAgICAgICAgICAgICcuIFdoeSBhcmUgeW91IHB1c2hpbmcgdG8gdGhpcyBjb21wb25lbnQ/Jyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5pbml0aWFsaXplZCkge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybigncmVmcmVzaCBmcm9tIGVtaXR0ZXIgaWdub3JlZCBiZWNhdXNlIHRoZSBjb21wb25lbnQgaXMgbm90IGZ1bGx5IGluaXRpYWxpemVkJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZldGNoaW5nKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gZW1pdHRlciBpZ25vcmVkIGJlY2F1c2UgdGhlIGNvbXBvbmVudCBpcyBmZXRjaGluZyBhbHJlYWR5Jyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdsYXVuY2hpbmcgcmVsb2FkIGZvbGxvd2luZyBwdXNoIHJlcXVlc3QnKTtcclxuICAgICAgICAgIHRoaXMucmVsb2FkKHtcclxuICAgICAgICAgICAgcmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uUFVTSCxcclxuICAgICAgICAgICAgcHVzaFJlcXVlc3Q6IGV2ZW50LFxyXG4gICAgICAgICAgICBpbkJhY2tncm91bmQ6IGV2ZW50LmluQmFja2dyb3VuZCA9PT0gdHJ1ZSB8fCBldmVudC5pbkJhY2tncm91bmQgPT09IGZhbHNlID9cclxuICAgICAgICAgICAgICBldmVudC5pbkJhY2tncm91bmQgOiB0aGlzLmN1cnJlbnRSZWZyZXNoT25QdXNoSW5CYWNrZ3JvdW5kXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVSZWZyZXNoSW50ZXJ2YWxDaGFuZ2UobmV3VmFsdWU6IG51bWJlcikge1xyXG4gICAgaWYgKHRoaXMucmVmcmVzaEludGVydmFsU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmVmcmVzaEludGVydmFsU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5yZWZyZXNoSW50ZXJ2YWxUaW1lcikge1xyXG4gICAgICB0aGlzLnJlZnJlc2hJbnRlcnZhbFRpbWVyID0gbnVsbDtcclxuICAgIH1cclxuICAgIGlmIChuZXdWYWx1ZSkge1xyXG4gICAgICB0aGlzLnJlZnJlc2hJbnRlcnZhbFRpbWVyID0gdGltZXIobmV3VmFsdWUsIG5ld1ZhbHVlKTtcclxuICAgICAgdGhpcy5yZWZyZXNoSW50ZXJ2YWxTdWJzY3JpcHRpb24gPSB0aGlzLnJlZnJlc2hJbnRlcnZhbFRpbWVyLnN1YnNjcmliZSh0aWNrID0+IHtcclxuICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgnZW1pdHRlZCByZWZyZXNoIHRpY2sgcmVxdWVzdCcpO1xyXG4gICAgICAgIGlmICh0aGlzLmRyYWdJblByb2dyZXNzKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gZW1pdHRlciBpZ25vcmVkIGJlY2F1c2UgdXNlciBpcyBkcmFnZ2luZyBlbGVtZW50cycpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50UmVmcmVzaFN0cmF0ZWdpZXMuaW5kZXhPZihNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3kuVElNRUQpID09PSAtMSkge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybigncmVmcmVzaCBmcm9tIHRpY2sgaWdub3JlZCBiZWNhdXNlIHJlZnJlc2ggc3RyYXRlZ2llcyBhcmUgJyArIHRoaXMuY3VycmVudFJlZnJlc2hTdHJhdGVnaWVzICtcclxuICAgICAgICAgICAgJy4gV2h5IGlzIHRoaXMgY29tcG9uZW50IGVtaXR0aW5nIHRpY2tzID8nKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmluaXRpYWxpemVkKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gdGljayBpZ25vcmVkIGJlY2F1c2UgdGhlIGNvbXBvbmVudCBpcyBub3QgZnVsbHkgaW5pdGlhbGl6ZWQnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZmV0Y2hpbmcpIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ3JlZnJlc2ggZnJvbSB0aWNrIGlnbm9yZWQgYmVjYXVzZSB0aGUgY29tcG9uZW50IGlzIGZldGNoaW5nIGFscmVhZHknKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIuZGVidWcoJ2xhdW5jaGluZyByZWxvYWQgZm9sbG93aW5nIHRpY2snKTtcclxuICAgICAgICAgIHRoaXMucmVsb2FkKHtcclxuICAgICAgICAgICAgcmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uSU5URVJWQUwsXHJcbiAgICAgICAgICAgIGluQmFja2dyb3VuZDogdGhpcy5jdXJyZW50UmVmcmVzaEludGVydmFsSW5CYWNrZ3JvdW5kXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gcHVibGljIG1ldGhvZHNcclxuXHJcbiAgcHVibGljIHJlZnJlc2goYmFja2dyb3VuZD86IGJvb2xlYW4pOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgIGlmICghdGhpcy5pbml0aWFsaXplZCkge1xyXG4gICAgICByZXR1cm4gdGhyb3dFcnJvcigndGFibGUgY29tcG9uZW50IGlzIHN0aWxsIGluaXRpYWxpemluZycpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5FWFRFUk5BTCwgaW5CYWNrZ3JvdW5kOiBiYWNrZ3JvdW5kfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0RGF0YVNuYXBzaG90KCk6IGFueVtdIHtcclxuICAgIHJldHVybiB0aGlzLmRhdGFTbmFwc2hvdDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTdGF0dXNTbmFwc2hvdCgpOiBJTWFpc1RhYmxlU3RhdHVzU25hcHNob3QgfCBudWxsIHtcclxuICAgIHJldHVybiB0aGlzLnN0YXR1c1NuYXBzaG90O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGxvYWRTdGF0dXMoc3RhdHVzOiBJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCkge1xyXG4gICAgaWYgKCF0aGlzLmluaXRpYWxpemVkKSB7XHJcbiAgICAgIHJldHVybiB0aHJvd0Vycm9yKCd0YWJsZSBjb21wb25lbnQgaXMgc3RpbGwgaW5pdGlhbGl6aW5nJyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnbG9hZGluZyBzdGF0dXMgc25hcHNob3QgZnJvbSBleHRlcm5hbCBjYWxsZXInLCBzdGF0dXMpO1xyXG4gICAgdGhpcy5hcHBseVN0YXR1c1NuYXBzaG90KHN0YXR1cyk7XHJcbiAgICB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uRVhURVJOQUwsIHdpdGhTdGF0dXNTbmFwc2hvdDogc3RhdHVzfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFwcGx5U3RhdHVzU25hcHNob3Qoc3RhdHVzOiBJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCkge1xyXG4gICBpZiAoIXN0YXR1cyB8fCAhc3RhdHVzLnNjaGVtYVZlcnNpb24pIHtcclxuICAgICB0aGlzLmxvZ2dlci53YXJuKCdub3QgcmVzdG9yaW5nIHN0YXR1cyBiZWNhdXNlIGl0IGlzIG1hbGZvcm1lZCcpO1xyXG4gICAgIHJldHVybjtcclxuICAgfVxyXG5cclxuICAgaWYgKHN0YXR1cy5zY2hlbWFWZXJzaW9uICE9PSB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jdXJyZW50U2NoZW1hVmVyc2lvbikge1xyXG4gICAgdGhpcy5sb2dnZXIud2Fybignbm90IHJlc3RvcmluZyBzdGF0dXMgYmVjYXVzZSBpdCBpcyBvYnNvbGV0ZSAoc25hcHNob3QgaXMgdmVyc2lvbiAnICtcclxuICAgICAgc3RhdHVzLnNjaGVtYVZlcnNpb24gKyAnIHdoaWxlIGN1cnJlbnQgdmVyc2lvbiBpcyAnICsgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY3VycmVudFNjaGVtYVZlcnNpb24gKyAnKScpO1xyXG4gICAgcmV0dXJuO1xyXG4gICB9XHJcblxyXG4gICBpZiAoc3RhdHVzLm9yZGVyQ29sdW1uKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkU29ydENvbHVtbiA9IHRoaXMuY29sdW1ucy5maW5kKGMgPT4gdGhpcy5pc1NvcnRhYmxlKGMpICYmIGMubmFtZSA9PT0gc3RhdHVzLm9yZGVyQ29sdW1uKSB8fCBudWxsO1xyXG4gICB9XHJcblxyXG4gICBpZiAoc3RhdHVzLm9yZGVyQ29sdW1uRGlyZWN0aW9uKSB7XHJcbiAgICAgdGhpcy5zZWxlY3RlZFNvcnREaXJlY3Rpb24gPSAoc3RhdHVzLm9yZGVyQ29sdW1uRGlyZWN0aW9uID09PSBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLkRFU0NFTkRJTkcpID9cclxuICAgICAgTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HIDogTWFpc1RhYmxlU29ydERpcmVjdGlvbi5BU0NFTkRJTkc7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVGaWx0ZXJpbmcgJiYgc3RhdHVzLnF1ZXJ5KSB7XHJcbiAgICAgdGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5ID0gc3RhdHVzLnF1ZXJ5LnRyaW0oKTtcclxuICAgfVxyXG5cclxuICAgaWYgKHRoaXMuY3VycmVudEVuYWJsZUZpbHRlcmluZyAmJiBzdGF0dXMucXVlcnlDb2x1bW5zICYmIHN0YXR1cy5xdWVyeUNvbHVtbnMubGVuZ3RoKSB7XHJcbiAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nID0gdGhpcy5maWx0ZXJhYmxlQ29sdW1ucy5maWx0ZXIoYyA9PiBzdGF0dXM/LnF1ZXJ5Q29sdW1ucz8uaW5kZXhPZihjLm5hbWUpICE9PSAtMSk7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVDb2x1bW5zU2VsZWN0aW9uICYmIHN0YXR1cy52aXNpYmxlQ29sdW1ucyAmJiBzdGF0dXMudmlzaWJsZUNvbHVtbnMubGVuZ3RoKSB7XHJcbiAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbiA9IHRoaXMuY29sdW1ucy5maWx0ZXIoYyA9PiAhdGhpcy5pc0hpZGVhYmxlKGMpIHx8IHN0YXR1cz8udmlzaWJsZUNvbHVtbnM/LmluZGV4T2YoYy5uYW1lKSAhPT0gLTEpO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlQ29udGV4dEZpbHRlcmluZyAmJiBzdGF0dXMuY29udGV4dEZpbHRlcnMgJiYgc3RhdHVzLmNvbnRleHRGaWx0ZXJzLmxlbmd0aCkge1xyXG4gICAgIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzID0gdGhpcy5jb250ZXh0RmlsdGVycy5maWx0ZXIoZiA9PiBzdGF0dXM/LmNvbnRleHRGaWx0ZXJzPy5pbmRleE9mKGYubmFtZSkgIT09IC0xKTtcclxuICAgfVxyXG5cclxuICAgaWYgKHRoaXMuY3VycmVudEVuYWJsZVBhZ2luYXRpb24gJiYgc3RhdHVzLmN1cnJlbnRQYWdlIHx8IHN0YXR1cy5jdXJyZW50UGFnZSA9PT0gMCkge1xyXG4gICAgIHRoaXMuc2VsZWN0ZWRQYWdlSW5kZXggPSBzdGF0dXMuY3VycmVudFBhZ2U7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVQYWdpbmF0aW9uICYmIHN0YXR1cy5wYWdlU2l6ZSkge1xyXG4gICAgIHRoaXMuc2VsZWN0ZWRQYWdlU2l6ZSA9IHRoaXMuY3VycmVudFBvc3NpYmxlUGFnZVNpemVzLmZpbmQocyA9PiBzdGF0dXMucGFnZVNpemUgPT09IHMpIHx8IG51bGw7XHJcbiAgIH1cclxuXHJcbiAgIHRoaXMuc3RhdHVzU25hcHNob3QgPSB0aGlzLmJ1aWxkU3RhdHVzU25hcHNob3QoKTtcclxuICAgdGhpcy5wZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90ID0gdGhpcy5idWlsZFBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYXBwbHlTdGF0dXNTbmFwc2hvdFBvc3RGZXRjaChzdGF0dXM6IElNYWlzVGFibGVQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KSB7XHJcbiAgIGlmICghc3RhdHVzIHx8ICFzdGF0dXMuc2NoZW1hVmVyc2lvbikge1xyXG4gICAgIHRoaXMubG9nZ2VyLndhcm4oJ25vdCByZXN0b3Jpbmcgc3RhdHVzIGJlY2F1c2UgaXQgaXMgbWFsZm9ybWVkJyk7XHJcbiAgICAgcmV0dXJuO1xyXG4gICB9XHJcblxyXG4gICBpZiAoc3RhdHVzLnNjaGVtYVZlcnNpb24gIT09IHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmN1cnJlbnRTY2hlbWFWZXJzaW9uKSB7XHJcbiAgICB0aGlzLmxvZ2dlci53YXJuKCdub3QgcmVzdG9yaW5nIHN0YXR1cyBiZWNhdXNlIGl0IGlzIG9ic29sZXRlIChzbmFwc2hvdCBpcyB2ZXJzaW9uICcgK1xyXG4gICAgICBzdGF0dXMuc2NoZW1hVmVyc2lvbiArICcgd2hpbGUgY3VycmVudCB2ZXJzaW9uIGlzICcgKyB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jdXJyZW50U2NoZW1hVmVyc2lvbiArICcpJyk7XHJcbiAgICByZXR1cm47XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVTZWxlY3Rpb24gJiYgdGhpcy5pdGVtVHJhY2tpbmdFbmFibGVkQW5kUG9zc2libGVcclxuICAgICAgJiYgc3RhdHVzLmNoZWNrZWRJdGVtSWRlbnRpZmllcnMgJiYgc3RhdHVzLmNoZWNrZWRJdGVtSWRlbnRpZmllcnMubGVuZ3RoKSB7XHJcblxyXG4gICAgdGhpcy5jaGVja2VkSXRlbXMgPSB0aGlzLmRhdGFTbmFwc2hvdC5maWx0ZXIoZGF0YSA9PiB7XHJcbiAgICAgIGNvbnN0IGlkID0gdGhpcy5nZXRJdGVtSWRlbnRpZmllcihkYXRhKTtcclxuICAgICAgcmV0dXJuIGlkICYmIHN0YXR1cz8uY2hlY2tlZEl0ZW1JZGVudGlmaWVycz8uaW5kZXhPZihpZCkgIT09IC0xO1xyXG4gICAgfSk7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVSb3dFeHBhbnNpb24gJiYgdGhpcy5pdGVtVHJhY2tpbmdFbmFibGVkQW5kUG9zc2libGVcclxuICAgICAgJiYgc3RhdHVzLmV4cGFuZGVkSXRlbUlkZW50aWZpZXJzICYmIHN0YXR1cy5leHBhbmRlZEl0ZW1JZGVudGlmaWVycy5sZW5ndGgpIHtcclxuXHJcbiAgICB0aGlzLmV4cGFuZGVkSXRlbXMgPSB0aGlzLmRhdGFTbmFwc2hvdC5maWx0ZXIoZGF0YSA9PiB7XHJcbiAgICAgIGNvbnN0IGlkID0gdGhpcy5nZXRJdGVtSWRlbnRpZmllcihkYXRhKTtcclxuICAgICAgcmV0dXJuIGlkICYmIHN0YXR1cz8uZXhwYW5kZWRJdGVtSWRlbnRpZmllcnM/LmluZGV4T2YoaWQpICE9PSAtMSAmJiB0aGlzLmlzRXhwYW5kYWJsZShkYXRhKTtcclxuICAgIH0pO1xyXG4gICB9XHJcblxyXG4gICB0aGlzLnN0YXR1c1NuYXBzaG90ID0gdGhpcy5idWlsZFN0YXR1c1NuYXBzaG90KCk7XHJcbiAgIHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlbG9hZCggY29udGV4dDogSU1haXNUYWJsZVJlbG9hZENvbnRleHQgKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICB0aGlzLmZldGNoaW5nID0gdHJ1ZTtcclxuICAgIHRoaXMuc2hvd0ZldGNoaW5nID0gIWNvbnRleHQuaW5CYWNrZ3JvdW5kO1xyXG4gICAgY29uc3Qgb2JzOiBPYnNlcnZhYmxlPHZvaWQ+ID0gbmV3IE9ic2VydmFibGU8dm9pZD4oc3Vic2NyaWJlciA9PiB7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWRJbk9ic2VydmFibGUoc3Vic2NyaWJlciwgY29udGV4dCk7XHJcbiAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICBzdWJzY3JpYmVyLmVycm9yKGUpO1xyXG4gICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgb2JzLnN1YnNjcmliZShzdWNjZXNzID0+IHtcclxuICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ2FzeW5jIHJlbG9hZCBzdWNjZXNzJyk7XHJcbiAgICAgIHRoaXMuZmV0Y2hpbmcgPSBmYWxzZTtcclxuICAgICAgaWYgKCFjb250ZXh0LmluQmFja2dyb3VuZCkge1xyXG4gICAgICAgIHRoaXMuc2hvd0ZldGNoaW5nID0gZmFsc2U7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuaGFuZGxlTWF0VGFibGVEYXRhU25hcHNob3RDaGFuZ2VkKCk7XHJcbiAgICB9LCBmYWlsdXJlID0+IHtcclxuICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ2FzeW5jIHJlbG9hZCBmYWlsZWQnLCBmYWlsdXJlKTtcclxuICAgICAgdGhpcy5mZXRjaGluZyA9IGZhbHNlO1xyXG4gICAgICBpZiAoIWNvbnRleHQuaW5CYWNrZ3JvdW5kKSB7XHJcbiAgICAgICAgdGhpcy5zaG93RmV0Y2hpbmcgPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5oYW5kbGVNYXRUYWJsZURhdGFTbmFwc2hvdENoYW5nZWQoKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIG9icztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVsb2FkSW5PYnNlcnZhYmxlKHRyYWNrZXI6IFN1YnNjcmliZXI8dm9pZD4sIGNvbnRleHQ6IElNYWlzVGFibGVSZWxvYWRDb250ZXh0KSB7XHJcbiAgICBjb25zdCB3aXRoU25hcHNob3Q6IElNYWlzVGFibGVQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbCA9IGNvbnRleHQud2l0aFN0YXR1c1NuYXBzaG90IHx8IG51bGw7XHJcblxyXG4gICAgY29uc3QgcGFnZVJlcXVlc3QgPSB0aGlzLmJ1aWxkUGFnZVJlcXVlc3QoKTtcclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygncmVsb2FkaW5nIHRhYmxlIGRhdGEnLCBwYWdlUmVxdWVzdCk7XHJcblxyXG4gICAgLy8gY2xlYXIgY2hlY2tlZCBpdGVtc1xyXG4gICAgaWYgKCFjb250ZXh0LmluQmFja2dyb3VuZCkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRJdGVtcyA9IFtdO1xyXG4gICAgICB0aGlzLmV4cGFuZGVkSXRlbXMgPSBbXTtcclxuXHJcbiAgICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5sYXN0RmV0Y2hlZFNlYXJjaFF1ZXJ5ID0gcGFnZVJlcXVlc3QucXVlcnkgfHwgbnVsbDtcclxuXHJcbiAgICBpZiAodGhpcy5kYXRhUHJvdmlkZXIpIHtcclxuICAgICAgLy8gY2FsbCBkYXRhIHByb3ZpZGVyXHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdyZWxvYWQgaGFzIGJlZW4gY2FsbGVkLCBmZXRjaGluZyBkYXRhIGZyb20gcHJvdmlkZWQgZnVuY3Rpb24nKTtcclxuICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3BhZ2UgcmVxdWVzdCBmb3IgZGF0YSBmZXRjaCBpcycsIHBhZ2VSZXF1ZXN0KTtcclxuXHJcbiAgICAgIHRoaXMuZGF0YVByb3ZpZGVyKHBhZ2VSZXF1ZXN0LCBjb250ZXh0KS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBJTWFpc1RhYmxlUGFnZVJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ2ZldGNoaW5nIGRhdGEgY29tcGxldGVkIHN1Y2Nlc3NmdWxseScpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuZHJhZ0luUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgIGlmICh0aGlzLnBhZ2luYXRpb25Nb2RlID09PSBNYWlzVGFibGVQYWdpbmF0aW9uTWV0aG9kLlNFUlZFUikge1xyXG4gICAgICAgICAgICB0aGlzLnBhcnNlUmVzcG9uc2VXaXRoU2VydmVyUGFnaW5hdGlvbihyZXNwb25zZSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnBhcnNlUmVzcG9uc2VXaXRoQ2xpZW50UGFnaW5hdGlvbihyZXNwb25zZS5jb250ZW50LCBwYWdlUmVxdWVzdCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAod2l0aFNuYXBzaG90KSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwbHlTdGF0dXNTbmFwc2hvdFBvc3RGZXRjaCh3aXRoU25hcHNob3QpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdkYXRhIGZldGNoIGFib3J0ZWQgYmVjYXVzZSB1c2VyIGlzIGRyYWdnaW5nIHRoaW5ncycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdHJhY2tlci5uZXh0KCk7XHJcbiAgICAgICAgdHJhY2tlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBmYWlsdXJlID0+IHtcclxuICAgICAgICB0aGlzLmxvZ2dlci5lcnJvcignZXJyb3IgZmV0Y2hpbmcgZGF0YSBmcm9tIHByb3ZpZGVyIGZ1bmN0aW9uJywgZmFpbHVyZSk7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5kcmFnSW5Qcm9ncmVzcykge1xyXG4gICAgICAgICAgdGhpcy5kYXRhU25hcHNob3QgPSBbXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybignZGF0YSBmZXRjaCBhYm9ydGVkIGJlY2F1c2UgdXNlciBpcyBkcmFnZ2luZyB0aGluZ3MnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRyYWNrZXIuZXJyb3IoZmFpbHVyZSk7XHJcbiAgICAgICAgdHJhY2tlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBkYXRhIGlzIG5vdCBwcm92aWRlZCBvbiByZXF1ZXN0IGFuZCBpcyBpbiBjbGllbnREYXRhU25hcHNob3RcclxuICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3JlbG9hZCBoYXMgYmVlbiBjYWxsZWQgb24gbG9jYWxseSBmZXRjaGVkIGRhdGEnKTtcclxuXHJcbiAgICAgIGlmICghdGhpcy5kcmFnSW5Qcm9ncmVzcykge1xyXG4gICAgICAgIHRoaXMucGFyc2VSZXNwb25zZVdpdGhDbGllbnRQYWdpbmF0aW9uKHRoaXMuY2xpZW50RGF0YVNuYXBzaG90LCBwYWdlUmVxdWVzdCk7XHJcbiAgICAgICAgaWYgKHdpdGhTbmFwc2hvdCkge1xyXG4gICAgICAgICAgdGhpcy5hcHBseVN0YXR1c1NuYXBzaG90UG9zdEZldGNoKHdpdGhTbmFwc2hvdCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ2RhdGEgZmV0Y2ggYWJvcnRlZCBiZWNhdXNlIHVzZXIgaXMgZHJhZ2dpbmcgdGhpbmdzJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRyYWNrZXIubmV4dCgpO1xyXG4gICAgICB0cmFja2VyLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHBhcnNlUmVzcG9uc2VXaXRoU2VydmVyUGFnaW5hdGlvbihyZXNwb25zZTogSU1haXNUYWJsZVBhZ2VSZXNwb25zZSkge1xyXG4gICAgdGhpcy5kYXRhU25hcHNob3QgPSByZXNwb25zZS5jb250ZW50O1xyXG5cclxuICAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVQYWdpbmF0aW9uKSB7XHJcbiAgICAgIGlmIChyZXNwb25zZS50b3RhbFBhZ2VzKSB7XHJcbiAgICAgICAgdGhpcy5mZXRjaGVkUGFnZUNvdW50ID0gcmVzcG9uc2UudG90YWxQYWdlcztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ2RhdGEgZnJvbSBzZXJ2ZXIgZGlkIG5vdCBjb250YWluIHJlcXVpcmVkIHRvdGFsUGFnZXMgZmllbGQnKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHJlc3BvbnNlLnRvdGFsRWxlbWVudHMpIHtcclxuICAgICAgICB0aGlzLmZldGNoZWRSZXN1bHROdW1iZXIgPSByZXNwb25zZS50b3RhbEVsZW1lbnRzO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignZGF0YSBmcm9tIHNlcnZlciBkaWQgbm90IGNvbnRhaW4gcmVxdWlyZWQgdG90YWxFbGVtZW50cyBmaWVsZCcpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHBhcnNlUmVzcG9uc2VXaXRoQ2xpZW50UGFnaW5hdGlvbihkYXRhOiBhbnlbXSwgcmVxdWVzdDogSU1haXNUYWJsZVBhZ2VSZXF1ZXN0KSB7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnYXBwbHlpbmcgaW4tbWVtb3J5IGZldGNoaW5nLCBwYWdpbmF0aW5nLCBvcmRlcmluZyBhbmQgZmlsdGVyaW5nJyk7XHJcbiAgICBjb25zdCBpbk1lbW9yeVJlc3BvbnNlID0gTWFpc1RhYmxlSW5NZW1vcnlIZWxwZXIuZmV0Y2hJbk1lbW9yeShcclxuICAgICAgZGF0YSxcclxuICAgICAgcmVxdWVzdCxcclxuICAgICAgdGhpcy5jdXJyZW50U29ydENvbHVtbixcclxuICAgICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZyxcclxuICAgICAgdGhpcy5nZXRDdXJyZW50TG9jYWxlKCkgKTtcclxuXHJcbiAgICB0aGlzLmRhdGFTbmFwc2hvdCA9IGluTWVtb3J5UmVzcG9uc2UuY29udGVudDtcclxuICAgIHRoaXMuZmV0Y2hlZFJlc3VsdE51bWJlciA9IHR5cGVvZiBpbk1lbW9yeVJlc3BvbnNlLnRvdGFsRWxlbWVudHMgPT09ICd1bmRlZmluZWQnID8gbnVsbCA6IGluTWVtb3J5UmVzcG9uc2UudG90YWxFbGVtZW50cztcclxuICAgIHRoaXMuZmV0Y2hlZFBhZ2VDb3VudCA9IHR5cGVvZiBpbk1lbW9yeVJlc3BvbnNlLnRvdGFsUGFnZXMgPT09ICd1bmRlZmluZWQnID8gbnVsbCA6IGluTWVtb3J5UmVzcG9uc2UudG90YWxQYWdlcztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYnVpbGRQYWdlUmVxdWVzdCgpOiBJTWFpc1RhYmxlUGFnZVJlcXVlc3Qge1xyXG4gICAgY29uc3Qgb3V0cHV0OiBJTWFpc1RhYmxlUGFnZVJlcXVlc3QgPSB7XHJcbiAgICAgIHBhZ2U6IHRoaXMuY3VycmVudEVuYWJsZVBhZ2luYXRpb24gPyB0aGlzLmN1cnJlbnRQYWdlSW5kZXggOiBudWxsLFxyXG4gICAgICBzaXplOiB0aGlzLmN1cnJlbnRFbmFibGVQYWdpbmF0aW9uID8gdGhpcy5jdXJyZW50UGFnZVNpemUgOiBudWxsLFxyXG4gICAgICBzb3J0OiBbXSxcclxuICAgICAgcXVlcnk6IG51bGwsXHJcbiAgICAgIHF1ZXJ5RmllbGRzOiBbXSxcclxuICAgICAgZmlsdGVyczogW11cclxuICAgIH07XHJcblxyXG4gICAgaWYgKHRoaXMuY3VycmVudEVuYWJsZUZpbHRlcmluZyAmJiB0aGlzLnNlYXJjaFF1ZXJ5QWN0aXZlKSB7XHJcbiAgICAgIG91dHB1dC5xdWVyeSA9IHRoaXMuY3VycmVudFNlYXJjaFF1ZXJ5O1xyXG4gICAgICBvdXRwdXQucXVlcnlGaWVsZHMgPSB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLm1hcChjb2x1bW4gPT4gY29sdW1uLnNlcnZlckZpZWxkIHx8IGNvbHVtbi5maWVsZCB8fCBudWxsICk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuY3VycmVudEVuYWJsZUNvbnRleHRGaWx0ZXJpbmcgJiYgdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMubGVuZ3RoKSB7XHJcbiAgICAgIGZvciAoY29uc3QgZmlsdGVyIG9mIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzKSB7XHJcbiAgICAgICAgb3V0cHV0LmZpbHRlcnM/LnB1c2goZmlsdGVyLm5hbWUgfHwgbnVsbCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzb3J0Q29sdW1uID0gdGhpcy5jdXJyZW50U29ydENvbHVtbjtcclxuICAgIGNvbnN0IHNvcnREaXJlY3Rpb24gPSB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uO1xyXG4gICAgaWYgKHNvcnRDb2x1bW4pIHtcclxuICAgICAgb3V0cHV0LnNvcnQ/LnB1c2goe1xyXG4gICAgICAgIHByb3BlcnR5OiBzb3J0Q29sdW1uLnNlcnZlckZpZWxkIHx8IHNvcnRDb2x1bW4uZmllbGQgfHwgbnVsbCxcclxuICAgICAgICBkaXJlY3Rpb246IHNvcnREaXJlY3Rpb24gfHwgTWFpc1RhYmxlU29ydERpcmVjdGlvbi5BU0NFTkRJTkdcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG91dHB1dDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0UGFnZShpbmRleDogbnVtYmVyKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkUGFnZUluZGV4ID0gaW5kZXg7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMuZW1pdFBhZ2VDaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFJlZnJlc2hPblB1c2hJbkJhY2tncm91bmQoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5yZWZyZXNoT25QdXNoSW5CYWNrZ3JvdW5kID09PSB0cnVlIHx8IHRoaXMucmVmcmVzaE9uUHVzaEluQmFja2dyb3VuZCA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMucmVmcmVzaE9uUHVzaEluQmFja2dyb3VuZDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yZWZyZXNoPy5kZWZhdWx0T25QdXNoSW5CYWNrZ3JvdW5kITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UmVmcmVzaEludGVydmFsSW5CYWNrZ3JvdW5kKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMucmVmcmVzaEludGVydmFsSW5CYWNrZ3JvdW5kID09PSB0cnVlIHx8IHRoaXMucmVmcmVzaEludGVydmFsSW5CYWNrZ3JvdW5kID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5yZWZyZXNoSW50ZXJ2YWxJbkJhY2tncm91bmQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucmVmcmVzaD8uZGVmYXVsdE9uVGlja0luQmFja2dyb3VuZCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFJlZnJlc2hTdHJhdGVnaWVzKCk6IE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneVtdIHtcclxuICAgIGlmICh0aGlzLnJlZnJlc2hTdHJhdGVneSkge1xyXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheSh0aGlzLnJlZnJlc2hTdHJhdGVneSkgJiYgdGhpcy5yZWZyZXNoU3RyYXRlZ3kubGVuZ3RoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVmcmVzaFN0cmF0ZWd5O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBbdGhpcy5yZWZyZXNoU3RyYXRlZ3kgYXMgTWFpc1RhYmxlUmVmcmVzaFN0cmF0ZWd5XTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIFt0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yZWZyZXNoPy5kZWZhdWx0U3RyYXRlZ3khXTtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UmVmcmVzaEludGVydmFsKCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5yZWZyZXNoSW50ZXJ2YWwgfHwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucmVmcmVzaD8uZGVmYXVsdEludGVydmFsITtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50VGFibGVJZCgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMudGFibGVJZCB8fCB0aGlzLnV1aWQ7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZURyYWcoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVEcmFnID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlRHJhZyA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlRHJhZztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5kcmFnQW5kRHJvcD8uZHJhZ0VuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVEcm9wKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlRHJvcCA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZURyb3AgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZURyb3A7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuZHJhZ0FuZERyb3A/LmRyb3BFbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlUGFnaW5hdGlvbigpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVBhZ2luYXRpb24gPT09IHRydWUgfHwgdGhpcy5lbmFibGVQYWdpbmF0aW9uID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVQYWdpbmF0aW9uO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnBhZ2luYXRpb24/LmVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVJdGVtVHJhY2tpbmcoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVJdGVtVHJhY2tpbmcgPT09IHRydWUgfHwgdGhpcy5lbmFibGVJdGVtVHJhY2tpbmcgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZUl0ZW1UcmFja2luZztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5pdGVtVHJhY2tpbmc/LmVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVTdG9yZVBlcnNpc3RlbmNlKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlU3RvcmVQZXJzaXN0ZW5jZSA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZVN0b3JlUGVyc2lzdGVuY2UgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZVN0b3JlUGVyc2lzdGVuY2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuc3RvcmVQZXJzaXN0ZW5jZT8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZVBhZ2VTaXplU2VsZWN0KCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlUGFnZVNpemVTZWxlY3QgPT09IHRydWUgfHwgdGhpcy5lbmFibGVQYWdlU2l6ZVNlbGVjdCA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlUGFnZVNpemVTZWxlY3Q7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucGFnaW5hdGlvbj8ucGFnZVNpemVTZWxlY3Rpb25FbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlTXVsdGlwbGVSb3dFeHBhbnNpb24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVNdWx0aXBsZVJvd0V4cGFuc2lvbiA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZU11bHRpcGxlUm93RXhwYW5zaW9uID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVNdWx0aXBsZVJvd0V4cGFuc2lvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yb3dFeHBhbnNpb24/Lm11bHRpcGxlRXhwYW5zaW9uRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZVJvd0V4cGFuc2lvbigpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVJvd0V4cGFuc2lvbiA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZVJvd0V4cGFuc2lvbiA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlUm93RXhwYW5zaW9uO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnJvd0V4cGFuc2lvbj8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZUhlYWRlckFjdGlvbnMoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVIZWFkZXJBY3Rpb25zID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlSGVhZGVyQWN0aW9ucyA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlSGVhZGVyQWN0aW9ucztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5hY3Rpb25zPy5oZWFkZXJBY3Rpb25zRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZUNvbnRleHRBY3Rpb25zKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlQ29udGV4dEFjdGlvbnMgPT09IHRydWUgfHwgdGhpcy5lbmFibGVDb250ZXh0QWN0aW9ucyA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlQ29udGV4dEFjdGlvbnM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuYWN0aW9ucz8uY29udGV4dEFjdGlvbnNFbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlQ29sdW1uc1NlbGVjdGlvbigpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZUNvbHVtbnNTZWxlY3Rpb24gPT09IHRydWUgfHwgdGhpcy5lbmFibGVDb2x1bW5zU2VsZWN0aW9uID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVDb2x1bW5zU2VsZWN0aW9uO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmNvbHVtblRvZ2dsaW5nPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlRmlsdGVyaW5nKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlRmlsdGVyaW5nID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlRmlsdGVyaW5nID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVGaWx0ZXJpbmc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuZmlsdGVyaW5nPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlTXVsdGlTZWxlY3QoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVNdWx0aVNlbGVjdCA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZU11bHRpU2VsZWN0ID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVNdWx0aVNlbGVjdDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yb3dTZWxlY3Rpb24/Lm11bHRpcGxlU2VsZWN0aW9uRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZVNlbGVjdEFsbCgpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVNlbGVjdEFsbCA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZVNlbGVjdEFsbCA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlU2VsZWN0QWxsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnJvd1NlbGVjdGlvbj8uc2VsZWN0QWxsRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZUNvbnRleHRGaWx0ZXJpbmcoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVDb250ZXh0RmlsdGVyaW5nID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlQ29udGV4dEZpbHRlcmluZyA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlQ29udGV4dEZpbHRlcmluZztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jb250ZXh0RmlsdGVyaW5nPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlU2VsZWN0aW9uKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlU2VsZWN0aW9uID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlU2VsZWN0aW9uID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVTZWxlY3Rpb247XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucm93U2VsZWN0aW9uPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UGFnaW5hdGlvbk1vZGUoKTogTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZCB7XHJcbiAgICBpZiAodGhpcy5wYWdpbmF0aW9uTW9kZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5wYWdpbmF0aW9uTW9kZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5wYWdpbmF0aW9uPy5kZWZhdWx0UGFnaW5hdGlvbk1vZGUhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGl0ZW1UcmFja2luZ0VuYWJsZWRBbmRQb3NzaWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhIXRoaXMuaXRlbUlkZW50aWZpZXIgJiYgdGhpcy5jdXJyZW50RW5hYmxlSXRlbVRyYWNraW5nO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHN0b3JlUGVyc2lzdGVuY2VFbmFibGVkQW5kUG9zc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdG9yZUFkYXB0ZXIgJiYgdGhpcy5jdXJyZW50RW5hYmxlU3RvcmVQZXJzaXN0ZW5jZTtcclxuICB9XHJcblxyXG4gIGdldCByb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RW5hYmxlUm93RXhwYW5zaW9uO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHBhZ2VTaXplU2VsZWN0RW5hYmxlZEFuZFBvc3NpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudFBvc3NpYmxlUGFnZVNpemVzICYmIHRoaXMuY3VycmVudFBvc3NpYmxlUGFnZVNpemVzLmxlbmd0aCA+IDAgJiYgdGhpcy5jdXJyZW50RW5hYmxlUGFnZVNpemVTZWxlY3Q7XHJcbiAgfVxyXG5cclxuICBnZXQgY29udGV4dEZpbHRlcmluZ0VuYWJsZWRBbmRQb3NzaWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmNvbnRleHRGaWx0ZXJzICYmIHRoaXMuY29udGV4dEZpbHRlcnMubGVuZ3RoID4gMCAmJiB0aGlzLmN1cnJlbnRFbmFibGVDb250ZXh0RmlsdGVyaW5nO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbnRleHRBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dEFjdGlvbnMgJiYgdGhpcy5jb250ZXh0QWN0aW9ucy5sZW5ndGggPiAwICYmIHRoaXMuY3VycmVudEVuYWJsZUNvbnRleHRBY3Rpb25zO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGhlYWRlckFjdGlvbnNFbmFibGVkQW5kUG9zc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5oZWFkZXJBY3Rpb25zICYmIHRoaXMuaGVhZGVyQWN0aW9ucy5sZW5ndGggPiAwICYmIHRoaXMuY3VycmVudEVuYWJsZUhlYWRlckFjdGlvbnM7XHJcbiAgfVxyXG5cclxuICBnZXQgY29sdW1uU2VsZWN0aW9uUG9zc2libGVBbmRBbGxvd2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudEVuYWJsZUNvbHVtbnNTZWxlY3Rpb24gJiYgdGhpcy5jb2x1bW5zLmxlbmd0aCA+IDAgJiYgdGhpcy5oaWRlYWJsZUNvbHVtbnMubGVuZ3RoID4gMDtcclxuICB9XHJcblxyXG4gIGdldCBmaWx0ZXJpbmdQb3NzaWJsZUFuZEFsbG93ZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RW5hYmxlRmlsdGVyaW5nICYmIHRoaXMuZmlsdGVyYWJsZUNvbHVtbnMubGVuZ3RoID4gMDtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UG9zc2libGVQYWdlU2l6ZXMoKTogbnVtYmVyW10ge1xyXG4gICAgaWYgKHRoaXMucG9zc2libGVQYWdlU2l6ZSAmJiB0aGlzLnBvc3NpYmxlUGFnZVNpemUubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnBvc3NpYmxlUGFnZVNpemU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucGFnaW5hdGlvbj8uZGVmYXVsdFBvc3NpYmxlUGFnZVNpemVzITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50U2VhcmNoUXVlcnkoKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkU2VhcmNoUXVlcnk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UGFnZUNvdW50KCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5mZXRjaGVkUGFnZUNvdW50IHx8IDA7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFJlc3VsdE51bWJlcigpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMuZmV0Y2hlZFJlc3VsdE51bWJlciB8fCAwO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRQYWdlU2l6ZSgpOiBudW1iZXIge1xyXG4gICAgY29uc3QgZGVmID0gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucGFnaW5hdGlvbj8uZGVmYXVsdFBhZ2VTaXplITtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkUGFnZVNpemUpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRQYWdlU2l6ZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5kZWZhdWx0UGFnZVNpemUpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdFBhZ2VTaXplO1xyXG4gICAgfSBlbHNlIGlmIChcclxuICAgICAgICB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplcy5sZW5ndGggJiZcclxuICAgICAgICBkZWYgJiZcclxuICAgICAgICB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplcy5pbmRleE9mKGRlZikgIT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIGRlZjtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50UG9zc2libGVQYWdlU2l6ZXMubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplc1swXTtcclxuICAgIH0gZWxzZSBpZiAoZGVmKSB7XHJcbiAgICAgIHJldHVybiBkZWY7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gMTA7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFBhZ2VJbmRleCgpOiBudW1iZXIge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRQYWdlSW5kZXgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRQYWdlSW5kZXg7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gMDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50U29ydENvbHVtbigpOiBJTWFpc1RhYmxlQ29sdW1uIHwgbnVsbCB7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFNvcnRDb2x1bW4pIHtcclxuICAgICAgLy8gdG9kbyBsb2cgaWYgc29ydGluZyBjb2x1bW4gZGlzYXBwZWFyc1xyXG4gICAgICByZXR1cm4gdGhpcy52aXNpYmxlQ29sdW1ucy5maW5kKGMgPT4gYy5uYW1lID09PSB0aGlzLnNlbGVjdGVkU29ydENvbHVtbj8ubmFtZSkgfHwgbnVsbDtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5kZWZhdWx0U29ydGluZ0NvbHVtbikge1xyXG4gICAgICBjb25zdCBmb3VuZENvbHVtbiA9IHRoaXMudmlzaWJsZUNvbHVtbnMuZmluZChjID0+IGMubmFtZSA9PT0gdGhpcy5kZWZhdWx0U29ydGluZ0NvbHVtbik7XHJcbiAgICAgIGlmIChmb3VuZENvbHVtbikge1xyXG4gICAgICAgIHJldHVybiBmb3VuZENvbHVtbjtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBmaW5kIGZpcnN0IHNvcnRhYmxlIGNvbHVtbj9cclxuICAgICAgICByZXR1cm4gdGhpcy52aXNpYmxlQ29sdW1ucy5maW5kKGNvbHVtbiA9PlxyXG4gICAgICAgICAgTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzU29ydGFibGUoY29sdW1uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSkpIHx8IG51bGw7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIGZpbmQgZmlyc3Qgc29ydGFibGUgY29sdW1uP1xyXG4gICAgICByZXR1cm4gdGhpcy52aXNpYmxlQ29sdW1ucy5maW5kKGNvbHVtbiA9PlxyXG4gICAgICAgIE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc1NvcnRhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKSB8fCBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRTb3J0RGlyZWN0aW9uKCk6IE1haXNUYWJsZVNvcnREaXJlY3Rpb24ge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTb3J0RGlyZWN0aW9uKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkU29ydERpcmVjdGlvbjtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5kZWZhdWx0U29ydGluZ0RpcmVjdGlvbikge1xyXG4gICAgICByZXR1cm4gdGhpcy5kZWZhdWx0U29ydGluZ0RpcmVjdGlvbiA9PT0gTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HID9cclxuICAgICAgTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HIDogTWFpc1RhYmxlU29ydERpcmVjdGlvbi5BU0NFTkRJTkc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyByZXR1cm4gREVGQVVMVFxyXG4gICAgICByZXR1cm4gTWFpc1RhYmxlU29ydERpcmVjdGlvbi5BU0NFTkRJTkc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudERhdGEoKTogYW55W10ge1xyXG4gICAgcmV0dXJuIHRoaXMuZGF0YVNuYXBzaG90IHx8IFtdO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGVudW1QYWdlcygpOiAobnVtYmVyIHwgeyBza2lwOiBib29sZWFuOyB9KVtdIHtcclxuICAgIGNvbnN0IHJhbmdlU3RhcnQgPSAxO1xyXG4gICAgY29uc3QgcmFuZ2VFbmQgPSAxO1xyXG4gICAgY29uc3QgcmFuZ2VTZWxlY3RlZCA9IDE7XHJcblxyXG4gICAgY29uc3QgcGFnZXMgPSBbXTtcclxuICAgIGNvbnN0IHBhZ2VOdW0gPSB0aGlzLmN1cnJlbnRQYWdlQ291bnQ7XHJcbiAgICBjb25zdCBjdXJyZW50SW5kZXggPSB0aGlzLmN1cnJlbnRQYWdlSW5kZXg7XHJcbiAgICBsZXQgaXNTa2lwcGluZyA9IGZhbHNlO1xyXG5cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFnZU51bTsgaSArKykge1xyXG4gICAgICBpZiAoTWF0aC5hYnMoaSAtIGN1cnJlbnRJbmRleCkgPD0gKHJhbmdlU2VsZWN0ZWQpIHx8IGkgPD0gKHJhbmdlU3RhcnQgLSAxKSB8fCBpID49IChwYWdlTnVtIC0gcmFuZ2VFbmQpKSB7XHJcbiAgICAgICAgaXNTa2lwcGluZyA9IGZhbHNlO1xyXG4gICAgICAgIHBhZ2VzLnB1c2goaSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCFpc1NraXBwaW5nKSB7XHJcbiAgICAgICAgICBwYWdlcy5wdXNoKHtcclxuICAgICAgICAgICAgc2tpcDogdHJ1ZVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpc1NraXBwaW5nID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBwYWdlcztcclxuICB9XHJcblxyXG4gIGdldCBhY3RpdmVDb2x1bW5zQ291bnQoKTogbnVtYmVyIHtcclxuICAgIGxldCBvdXRwdXQgPSB0aGlzLnZpc2libGVDb2x1bW5zLmxlbmd0aDtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVTZWxlY3Rpb24pIHtcclxuICAgICAgb3V0cHV0ICsrO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuY29udGV4dEZpbHRlcmluZ0VuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICBvdXRwdXQgKys7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5yb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgb3V0cHV0ICsrO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG91dHB1dDtcclxuICB9XHJcblxyXG4gIGdldCBub1Jlc3VsdHMoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5wYWdpbmF0aW9uTW9kZSA9PT0gTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZC5DTElFTlQpIHtcclxuICAgICAgcmV0dXJuICF0aGlzLmN1cnJlbnREYXRhLmxlbmd0aDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiAhdGhpcy5kYXRhU25hcHNob3QubGVuZ3RoO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGhpZGVhYmxlQ29sdW1ucygpOiBJTWFpc1RhYmxlQ29sdW1uW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sdW1ucy5maWx0ZXIoYyA9PiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHZpc2libGVDb2x1bW5zKCk6IElNYWlzVGFibGVDb2x1bW5bXSB7XHJcbiAgICByZXR1cm4gdGhpcy5jb2x1bW5zLmZpbHRlcihjID0+IHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLmluZGV4T2YoYykgIT09IC0xKTtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50Q29udGV4dEZpbHRlcnMoKTogSU1haXNUYWJsZUNvbnRleHRGaWx0ZXJbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5jb250ZXh0RmlsdGVycyB8fCBbXTtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50SGVhZGVyQWN0aW9ucygpOiBJTWFpc1RhYmxlQWN0aW9uW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuaGVhZGVyQWN0aW9ucyB8fCBbXTtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50QWN0aW9ucygpOiBJTWFpc1RhYmxlQWN0aW9uW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dEFjdGlvbnMgfHwgW107XHJcbiAgfVxyXG5cclxuICBnZXQgaGFzQWN0aW9ucygpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmN1cnJlbnRBY3Rpb25zLmxlbmd0aCA+IDA7XHJcbiAgfVxyXG5cclxuICBnZXQgc2VhcmNoUXVlcnlOZWVkQXBwbHkoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gKHRoaXMuY3VycmVudFNlYXJjaFF1ZXJ5IHx8ICcnKSAhPT0gKHRoaXMubGFzdEZldGNoZWRTZWFyY2hRdWVyeSB8fCAnJyk7XHJcbiAgfVxyXG5cclxuICBnZXQgc2VhcmNoUXVlcnlBY3RpdmUoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudFNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXQgc2VhcmNoUXVlcnlNYWxmb3JtZWQoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudFNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBnZXQgZmlsdGVyYWJsZUNvbHVtbnMoKTogSU1haXNUYWJsZUNvbHVtbltdIHtcclxuICAgIHJldHVybiB0aGlzLmNvbHVtbnMuZmlsdGVyKGMgPT4gdGhpcy5pc0ZpbHRlcmFibGUoYykpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFueUJ1dHRvbkFjdGlvbnNBbGxvd2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEhdGhpcy5jb250ZXh0QWN0aW9ucy5maW5kKGEgPT4gdGhpcy5pc0NvbnRleHRBY3Rpb25BbGxvd2VkKGEpKTtcclxuICB9XHJcblxyXG4gIGdldCBhbnlIZWFkZXJBY3Rpb25zQWxsb3dlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhIXRoaXMuaGVhZGVyQWN0aW9ucy5maW5kKGEgPT4gdGhpcy5pc0hlYWRlckFjdGlvbkFsbG93ZWQoYSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbmZpZ0NvbHVtblZpc2liaWxpdHlTaG93Rml4ZWRDb2x1bW5zKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmNvbHVtblRvZ2dsaW5nPy5zaG93Rml4ZWRDb2x1bW5zO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbmZpZ0ZpbHRlcmluZ1Nob3dVbmZpbHRlcmFibGVDb2x1bW5zKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmZpbHRlcmluZz8uc2hvd1VuZmlsdGVyYWJsZUNvbHVtbnM7XHJcbiAgfVxyXG5cclxuICAvLyBkcmFnIGFuZCBkcm9wIHN1cHBvcnRcclxuXHJcbiAgYWNjZXB0RHJvcFByZWRpY2F0ZSA9IChpdGVtOiBDZGtEcmFnPGFueT4pID0+IHtcclxuICAgIHJldHVybiB0aGlzLmFjY2VwdERyb3A7XHJcbiAgfVxyXG5cclxuICBnZXQgYWNjZXB0RHJvcCgpIHtcclxuICAgIHJldHVybiB0aGlzLmN1cnJlbnRFbmFibGVEcm9wO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFjY2VwdERyYWcoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RW5hYmxlRHJhZztcclxuICB9XHJcblxyXG4gIGdldCByZWZlcmVuY2VkVGFibGUoKTogTWFpc1RhYmxlQ29tcG9uZW50IHwgbnVsbCB7XHJcbiAgICBjb25zdCB2ID0gdGhpcy5nZXRSZWZlcmVuY2VkVGFibGUoKTtcclxuICAgIHJldHVybiB2O1xyXG4gIH1cclxuXHJcbiAgZ2V0UmVmZXJlbmNlZFRhYmxlKCk6IE1haXNUYWJsZUNvbXBvbmVudCB8IG51bGwge1xyXG4gICAgaWYgKHRoaXMuZHJvcENvbm5lY3RlZFRvICYmIHRoaXMuZHJvcENvbm5lY3RlZFRvLmxlbmd0aCkge1xyXG4gICAgICBjb25zdCBjb25uZWN0ZWRUb0xpc3Q6IHN0cmluZ1tdID0gKEFycmF5LmlzQXJyYXkodGhpcy5kcm9wQ29ubmVjdGVkVG8pKSA/IHRoaXMuZHJvcENvbm5lY3RlZFRvIDogW3RoaXMuZHJvcENvbm5lY3RlZFRvXTtcclxuICAgICAgY29uc3QgZm91bmQgPSBbXTtcclxuICAgICAgZm9yIChjb25zdCBjb25uZWN0ZWRUb1Rva2VuIG9mIGNvbm5lY3RlZFRvTGlzdCkge1xyXG4gICAgICAgIGNvbnN0IHJlZ2lzdGVyZWRMaXN0OiBhbnlbXSA9IHRoaXMucmVnaXN0cnkuZ2V0KGNvbm5lY3RlZFRvVG9rZW4pO1xyXG4gICAgICAgIGlmIChyZWdpc3RlcmVkTGlzdCAmJiByZWdpc3RlcmVkTGlzdC5sZW5ndGgpIHtcclxuICAgICAgICAgIGZvciAoY29uc3QgcmVnQ29tcCBvZiByZWdpc3RlcmVkTGlzdCkge1xyXG4gICAgICAgICAgICBmb3VuZC5wdXNoKHJlZ0NvbXApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoZm91bmQubGVuZ3RoIDwgMSkge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGZvdW5kLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci5lcnJvcignTVVMVElQTEUgVEFCTEUgUkVGRVJFTkNFRCBTVElMTCBBQ1RJVkUnLCBmb3VuZCk7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIGZvdW5kWzBdLmNvbXBvbmVudDtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyB1c2VyIGFjdGlvbnNcclxuXHJcbiAgaGFuZGxlSXRlbURyYWdnZWQoZXZlbnQ6IENka0RyYWdEcm9wPGFueVtdPiwgZnJvbTogTWFpc1RhYmxlQ29tcG9uZW50LCB0bzogTWFpc1RhYmxlQ29tcG9uZW50KSB7XHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnaXRlbSBkcmFnZ2VkIGZyb20gdGFibGUgJyArIHRoaXMuY3VycmVudFRhYmxlSWQpO1xyXG4gICAgdGhpcy5pdGVtRHJhZ2dlZC5lbWl0KHtcclxuICAgICAgaXRlbTogZXZlbnQuaXRlbS5kYXRhLFxyXG4gICAgICBldmVudCxcclxuICAgICAgZnJvbURhdGFTbmFwc2hvdDogZnJvbS5nZXREYXRhU25hcHNob3QoKSxcclxuICAgICAgdG9EYXRhU25hcHNob3Q6IHRvLmdldERhdGFTbmFwc2hvdCgpLFxyXG4gICAgICBmcm9tQ29tcG9uZW50OiBmcm9tLFxyXG4gICAgICB0b0NvbXBvbmVudDogdG9cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlSXRlbURyb3BwZWQoZXZlbnQ6IENka0RyYWdEcm9wPGFueVtdPikge1xyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ0RST1AgRVZFTlQgRlJPTSBUQUJMRSAnICsgdGhpcy5jdXJyZW50VGFibGVJZCwgZXZlbnQpO1xyXG4gICAgY29uc3QgZHJvcHBlZFNvdXJjZTogTWFpc1RhYmxlQ29tcG9uZW50IHwgbnVsbCA9IHRoaXMucmVnaXN0cnkuZ2V0U2luZ2xlKGV2ZW50LnByZXZpb3VzQ29udGFpbmVyLmlkKTtcclxuICAgIGNvbnN0IGRyb3BwZWRUYXJnZXQ6IE1haXNUYWJsZUNvbXBvbmVudCB8IG51bGwgPSB0aGlzLnJlZ2lzdHJ5LmdldFNpbmdsZShldmVudC5jb250YWluZXIuaWQpO1xyXG5cclxuICAgIGlmICghZHJvcHBlZFRhcmdldCkge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdOTyBEUk9QIFRBUkdFVCBGT1VORCcpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCFkcm9wcGVkVGFyZ2V0LmFjY2VwdERyb3ApIHtcclxuICAgICAgdGhpcy5sb2dnZXIuZGVidWcoJ3NraXBwaW5nIGRyb3Agb24gY29udGFpbmVyIHdpdGggYWNjZXB0RHJvcCA9IGZhbHNlJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoZHJvcHBlZFNvdXJjZSkge1xyXG4gICAgICBkcm9wcGVkU291cmNlLmhhbmRsZUl0ZW1EcmFnZ2VkKGV2ZW50LCBkcm9wcGVkU291cmNlLCBkcm9wcGVkVGFyZ2V0KTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnaXRlbSBkcm9wcGVkIG9uIHRhYmxlICcgKyBkcm9wcGVkVGFyZ2V0LmN1cnJlbnRUYWJsZUlkKTtcclxuICAgIHRoaXMuaXRlbURyb3BwZWQuZW1pdCh7XHJcbiAgICAgIGl0ZW06IGV2ZW50Lml0ZW0uZGF0YSxcclxuICAgICAgZXZlbnQsXHJcbiAgICAgIGZyb21EYXRhU25hcHNob3Q6IGRyb3BwZWRTb3VyY2UgPyBkcm9wcGVkU291cmNlLmdldERhdGFTbmFwc2hvdCgpIDogbnVsbCxcclxuICAgICAgdG9EYXRhU25hcHNob3Q6IGRyb3BwZWRUYXJnZXQuZ2V0RGF0YVNuYXBzaG90KCksXHJcbiAgICAgIGZyb21Db21wb25lbnQ6IGRyb3BwZWRTb3VyY2UsXHJcbiAgICAgIHRvQ29tcG9uZW50OiBkcm9wcGVkVGFyZ2V0XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBJJ20gc29ycnlcclxuICAgIHRoaXMuY2RyLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgIHRoaXMuZGF0YVNuYXBzaG90ID0gdGhpcy5kYXRhU25hcHNob3QubWFwKG8gPT4gbyk7XHJcbiAgICAgIHRoaXMuY2RyLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmZvcmNlUmVSZW5kZXIgPSB0cnVlKTtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmZvcmNlUmVSZW5kZXIgPSBmYWxzZSk7XHJcbiAgICAgIHRoaXMuaGFuZGxlTWF0VGFibGVEYXRhU25hcHNob3RDaGFuZ2VkKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFwcGx5U2VsZWN0ZWRGaWx0ZXIoKSB7XHJcbiAgICB0aGlzLnNldFBhZ2UoMCk7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5VU0VSfSk7XHJcbiAgfVxyXG5cclxuICBjbGlja09uUm93RXhwYW5zaW9uKGl0ZW06IGFueSkge1xyXG4gICAgaWYgKCF0aGlzLnJvd0V4cGFuc2lvbkVuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuaXNFeHBhbmRhYmxlKGl0ZW0pKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5pc0V4cGFuZGVkKGl0ZW0pKSB7XHJcbiAgICAgIHRoaXMuZXhwYW5kZWRJdGVtcy5zcGxpY2UodGhpcy5leHBhbmRlZEl0ZW1zLmluZGV4T2YoaXRlbSksIDEpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVNdWx0aXBsZVJvd0V4cGFuc2lvbikge1xyXG4gICAgICAgIHRoaXMuZXhwYW5kZWRJdGVtcyA9IFtdO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuZXhwYW5kZWRJdGVtcy5wdXNoKGl0ZW0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuaGFuZGxlTWF0VGFibGVEYXRhU25hcHNob3RDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIGNsaWNrT25Db250ZXh0QWN0aW9uKGFjdGlvbjogSU1haXNUYWJsZUFjdGlvbikge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVDb250ZXh0QWN0aW9ucykge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdidXR0b24gYWN0aW9ucyBhcmUgZGlzYWJsZWQnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLmlzQ29udGV4dEFjdGlvbkFsbG93ZWQoYWN0aW9uKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgZGlzcGF0Y2hDb250ZXh0OiBJTWFpc1RhYmxlQWN0aW9uRGlzcGF0Y2hpbmdDb250ZXh0ID0ge1xyXG4gICAgICBhY3Rpb24sXHJcbiAgICAgIHNlbGVjdGVkSXRlbXM6IHRoaXMuY2hlY2tlZEl0ZW1zXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdkaXNwYXRjaGluZyBhY3Rpb24gJyArIGFjdGlvbi5uYW1lICsgJyB3aXRoIHBheWxvYWQnLCBkaXNwYXRjaENvbnRleHQpO1xyXG5cclxuICAgIHRoaXMuYWN0aW9uLmVtaXQoZGlzcGF0Y2hDb250ZXh0KTtcclxuICB9XHJcblxyXG4gIGNsaWNrT25QYWdlU2l6ZShwYWdlU2l6ZTogbnVtYmVyKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkUGFnZVNpemUgPSBwYWdlU2l6ZTtcclxuICAgIHRoaXMuc2V0UGFnZSgwKTtcclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5yZWxvYWQoe3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLlVTRVJ9KTtcclxuICB9XHJcblxyXG4gIGNsaWNrT25IZWFkZXJBY3Rpb24oYWN0aW9uOiBJTWFpc1RhYmxlQWN0aW9uKSB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudEVuYWJsZUhlYWRlckFjdGlvbnMpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignYnV0dG9uIGFjdGlvbnMgYXJlIGRpc2FibGVkJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5pc0hlYWRlckFjdGlvbkFsbG93ZWQoYWN0aW9uKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgZGlzcGF0Y2hDb250ZXh0OiBJTWFpc1RhYmxlQWN0aW9uRGlzcGF0Y2hpbmdDb250ZXh0ID0ge1xyXG4gICAgICBhY3Rpb24sXHJcbiAgICAgIHNlbGVjdGVkSXRlbXM6IHRoaXMuY2hlY2tlZEl0ZW1zXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdkaXNwYXRjaGluZyBhY3Rpb24gJyArIGFjdGlvbi5uYW1lICsgJyB3aXRoIHBheWxvYWQnLCBkaXNwYXRjaENvbnRleHQpO1xyXG5cclxuICAgIHRoaXMuYWN0aW9uLmVtaXQoZGlzcGF0Y2hDb250ZXh0KTtcclxuICB9XHJcblxyXG4gIHNlYXJjaFF1ZXJ5Rm9jdXNPdXQoKSB7XHJcbiAgICBjb25zdCBjb21wID0gdGhpcztcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICBpZiAoKGNvbXAuc2VsZWN0ZWRTZWFyY2hRdWVyeSB8fCAnJykgIT09IChjb21wLmxhc3RGZXRjaGVkU2VhcmNoUXVlcnkgfHwgJycpKSB7XHJcbiAgICAgICAgaWYgKGNvbXAuc2VsZWN0ZWRTZWFyY2hRdWVyeSkge1xyXG4gICAgICAgICAgaWYgKHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmZpbHRlcmluZz8uYXV0b1JlbG9hZE9uUXVlcnlDaGFuZ2UpIHtcclxuICAgICAgICAgICAgY29tcC5hcHBseVNlbGVjdGVkRmlsdGVyKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBjb21wLnNlbGVjdGVkU2VhcmNoUXVlcnkgPSBjb21wLmxhc3RGZXRjaGVkU2VhcmNoUXVlcnk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmICh0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5maWx0ZXJpbmc/LmF1dG9SZWxvYWRPblF1ZXJ5Q2xlYXIpIHtcclxuICAgICAgICAgICAgY29tcC5hcHBseVNlbGVjdGVkRmlsdGVyKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5maWx0ZXJpbmc/LmF1dG9SZWxvYWRUaW1lb3V0KTtcclxuICB9XHJcblxyXG4gIHN3aXRjaFRvUGFnZShwYWdlSW5kZXg6IG51bWJlciwgY29udGV4dD86IElNYWlzVGFibGVSZWxvYWRDb250ZXh0KSB7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50UGFnZUluZGV4ID09PSBwYWdlSW5kZXgpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCFjb250ZXh0KSB7XHJcbiAgICAgIGNvbnRleHQgPSB7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uVVNFUn07XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zZXRQYWdlKHBhZ2VJbmRleCk7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMucmVsb2FkKGNvbnRleHQpO1xyXG4gIH1cclxuXHJcbiAgY2xpY2tPbkNvbHVtbihjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIGlmICghTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzU29ydGFibGUoY29sdW1uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSkpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHNvcnRDb2x1bW4gPSB0aGlzLmN1cnJlbnRTb3J0Q29sdW1uO1xyXG4gICAgY29uc3Qgc29ydERpcmVjdGlvbiA9IHRoaXMuY3VycmVudFNvcnREaXJlY3Rpb247XHJcblxyXG4gICAgaWYgKHNvcnRDb2x1bW4gJiYgc29ydENvbHVtbi5uYW1lID09PSBjb2x1bW4ubmFtZSkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU29ydERpcmVjdGlvbiA9IChzb3J0RGlyZWN0aW9uID09PSBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLkRFU0NFTkRJTkcgP1xyXG4gICAgICAgIE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HIDogTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRTb3J0Q29sdW1uID0gY29sdW1uO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU29ydERpcmVjdGlvbiA9IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZW1pdFNvcnRDaGFuZ2VkKCk7XHJcblxyXG4gICAgLy8gZm9yemEgcml0b3JubyBhbGxhIHByaW1hIHBhZ2luYVxyXG4gICAgdGhpcy5zZXRQYWdlKDApO1xyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uVVNFUn0pO1xyXG4gIH1cclxuXHJcbiAgLy8gcGFyc2luZyBmdW5jdGlvbnNcclxuXHJcbiAgZ2V0SXRlbUlkZW50aWZpZXIoaXRlbTogYW55KSB7XHJcbiAgICBpZiAodGhpcy5pdGVtVHJhY2tpbmdFbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgaWYgKCh0aGlzLml0ZW1JZGVudGlmaWVyIGFzIGFueSkuZXh0cmFjdCkge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5pdGVtSWRlbnRpZmllciBhcyBJTWFpc1RhYmxlSWRlbnRpZmllclByb3ZpZGVyKS5leHRyYWN0KGl0ZW0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuZ2V0UHJvcGVydHlWYWx1ZShpdGVtLCB0aGlzLml0ZW1JZGVudGlmaWVyIGFzIHN0cmluZyk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0Q3VycmVudExvY2FsZSgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlU2VydmljZS5nZXREZWZhdWx0TGFuZygpO1xyXG4gIH1cclxuXHJcbiAgZXh0cmFjdFZhbHVlKHJvdzogYW55LCBjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuZXh0cmFjdFZhbHVlKHJvdywgY29sdW1uLCB0aGlzLmdldEN1cnJlbnRMb2NhbGUoKSk7XHJcbiAgfVxyXG5cclxuICByZXNvbHZlTGFiZWwoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uIHwgSU1haXNUYWJsZUFjdGlvbiB8IElNYWlzVGFibGVDb250ZXh0RmlsdGVyKSB7XHJcbiAgICByZXR1cm4gdGhpcy5yZXNvbHZlTGFiZWxPckZpeGVkKGNvbHVtbi5sYWJlbEtleSwgY29sdW1uLmxhYmVsKTtcclxuICB9XHJcblxyXG4gIHJlc29sdmVMYWJlbE9yRml4ZWQoa2V5OiBzdHJpbmcgfCBudWxsIHwgdW5kZWZpbmVkLCBmaXhlZDogIHN0cmluZyB8IG51bGwgfCB1bmRlZmluZWQpIHtcclxuICAgIGlmIChrZXkpIHtcclxuICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlU2VydmljZS5pbnN0YW50KGtleSk7XHJcbiAgICB9IGVsc2UgaWYgKGZpeGVkKSB7XHJcbiAgICAgIHJldHVybiBmaXhlZDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW46IElNYWlzVGFibGVDb2x1bW4sIGRpcmVjdGlvbiA9IG51bGwpIHtcclxuICAgIGNvbnN0IHNvcnRDb2x1bW4gPSB0aGlzLmN1cnJlbnRTb3J0Q29sdW1uO1xyXG4gICAgaWYgKHNvcnRDb2x1bW4gJiYgY29sdW1uICYmIGNvbHVtbi5uYW1lID09PSBzb3J0Q29sdW1uLm5hbWUpIHtcclxuICAgICAgaWYgKCFkaXJlY3Rpb24pIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gZGlyZWN0aW9uID09PSB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaXNEZWZhdWx0VmlzaWJsZUNvbHVtbihjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNEZWZhdWx0VmlzaWJsZUNvbHVtbihjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGlzRGVmYXVsdEZpbHRlckNvbHVtbihjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNEZWZhdWx0RmlsdGVyQ29sdW1uKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG4gIH1cclxuXHJcbiAgaXNFeHBhbmRhYmxlKHJvdzogYW55KTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RW5hYmxlUm93RXhwYW5zaW9uICYmXHJcbiAgICAgICghdGhpcy5leHBhbmRhYmxlU3RhdHVzUHJvdmlkZXIgfHwgdGhpcy5leHBhbmRhYmxlU3RhdHVzUHJvdmlkZXIocm93LCB0aGlzLnN0YXR1c1NuYXBzaG90KSk7XHJcbiAgfVxyXG5cclxuICBpc0hpZGVhYmxlKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbik6IGJvb2xlYW4gfCB1bmRlZmluZWQge1xyXG4gICAgcmV0dXJuIE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc0hpZGVhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG4gIH1cclxuXHJcbiAgaXNTb3J0YWJsZShjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNTb3J0YWJsZShjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGlzRmlsdGVyYWJsZShjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNGaWx0ZXJhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG4gIH1cclxuXHJcbiAgaGFzTGFiZWwoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmhhc0xhYmVsKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG4gIH1cclxuXHJcbiAgaXNMYWJlbFZpc2libGVJblRhYmxlKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbikge1xyXG4gICAgcmV0dXJuIE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc0xhYmVsVmlzaWJsZUluVGFibGUoY29sdW1uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSk7XHJcbiAgfVxyXG5cclxuICBpc0NvbnRleHRBY3Rpb25BbGxvd2VkKGFjdGlvbjogSU1haXNUYWJsZUFjdGlvbik6IGJvb2xlYW4gfCB1bmRlZmluZWQge1xyXG4gICAgcmV0dXJuIHRoaXMuaXNBY3Rpb25BbGxvd2VkKGFjdGlvbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuYWN0aW9ucz8uZGVmYXVsdENvbnRleHRBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uKTtcclxuICB9XHJcblxyXG4gIGlzSGVhZGVyQWN0aW9uQWxsb3dlZChhY3Rpb246IElNYWlzVGFibGVBY3Rpb24pOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcclxuICAgIHJldHVybiB0aGlzLmlzQWN0aW9uQWxsb3dlZChhY3Rpb24sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmFjdGlvbnM/LmRlZmF1bHRIZWFkZXJBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uKTtcclxuICB9XHJcblxyXG4gIGlzQWN0aW9uQWxsb3dlZChhY3Rpb246IElNYWlzVGFibGVBY3Rpb24sIGRlZjogTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbiB8IHVuZGVmaW5lZCkge1xyXG4gICAgY29uc3QgYWN0Q29uZCA9IGFjdGlvbi5hY3RpdmF0aW9uQ29uZGl0aW9uIHx8IGRlZjtcclxuICAgIGlmIChhY3RDb25kID09PSBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLkFMV0FZUykge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoYWN0Q29uZCA9PT0gTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5ORVZFUikge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9IGVsc2UgaWYgKGFjdENvbmQgPT09IE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24uRFlOQU1JQykge1xyXG4gICAgICAvLyBkZWxlZ2EgZGVjaXNpb25lIGEgcHJvdmlkZXIgZXN0ZXJub1xyXG4gICAgICBpZiAoIXRoaXMuYWN0aW9uU3RhdHVzUHJvdmlkZXIpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci5lcnJvcignQWN0aW9uIHdpdGggZHluYW1pYyBlbmFibGluZyBidXQgbm8gYWN0aW9uU3RhdHVzUHJvdmlkZXIgcHJvdmlkZWQnKTtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiB0aGlzLmFjdGlvblN0YXR1c1Byb3ZpZGVyKGFjdGlvbiwgdGhpcy5zdGF0dXNTbmFwc2hvdCB8fCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBudW1TZWxlY3RlZCA9IHRoaXMuY2hlY2tlZEl0ZW1zLmxlbmd0aDtcclxuICAgIGlmIChhY3RDb25kID09PSBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLlNJTkdMRV9TRUxFQ1RJT04pIHtcclxuICAgICAgcmV0dXJuIG51bVNlbGVjdGVkID09PSAxO1xyXG4gICAgfSBlbHNlIGlmIChhY3RDb25kID09PSBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLk1VTFRJUExFX1NFTEVDVElPTikge1xyXG4gICAgICByZXR1cm4gbnVtU2VsZWN0ZWQgPiAwO1xyXG4gICAgfSBlbHNlIGlmIChhY3RDb25kID09PSBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLk5PX1NFTEVDVElPTikge1xyXG4gICAgICByZXR1cm4gbnVtU2VsZWN0ZWQgPCAxO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbmtub3duIGFjdGlvbiBhY3RpdmF0aW9uIGNvbmRpdGlvbjogJyArIGFjdENvbmQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gZ2VzdGlvbmUgZGVsbGUgcmlnaGUgZXNwYW5zZVxyXG5cclxuICBpc0V4cGFuZGVkKGl0ZW06IGFueSkge1xyXG4gICAgcmV0dXJuIHRoaXMuZXhwYW5kZWRJdGVtcy5pbmRleE9mKGl0ZW0pICE9PSAtMTtcclxuICB9XHJcblxyXG4gIC8vIGNoZWNrYm94IHNlbGVjdCBwZXIgaXRlbXNcclxuXHJcbiAgaXNDaGVja2VkKGl0ZW06IGFueSkge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hlY2tlZEl0ZW1zLmluZGV4T2YoaXRlbSkgIT09IC0xO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFsbENoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBkYXRhSXRlbXMgPSB0aGlzLmN1cnJlbnREYXRhO1xyXG4gICAgaWYgKCFkYXRhSXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBkYXRhSXRlbXMpIHtcclxuICAgICAgaWYgKHRoaXMuY2hlY2tlZEl0ZW1zLmluZGV4T2YoaXRlbSkgPT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldCBhbnlDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5jdXJyZW50RGF0YTtcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRJdGVtcy5pbmRleE9mKGl0ZW0pICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBnZXQgbm9uZUNoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gIXRoaXMuYW55Q2hlY2tlZDtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUNoZWNrZWQoaXRlbTogYW55KSB7XHJcbiAgICBpZiAodGhpcy5pc0NoZWNrZWQoaXRlbSkpIHtcclxuICAgICAgdGhpcy5jaGVja2VkSXRlbXMuc3BsaWNlKHRoaXMuY2hlY2tlZEl0ZW1zLmluZGV4T2YoaXRlbSksIDEpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVNdWx0aVNlbGVjdCkge1xyXG4gICAgICAgIHRoaXMuY2hlY2tlZEl0ZW1zID0gW107XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jaGVja2VkSXRlbXMucHVzaChpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMuZW1pdFNlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUFsbENoZWNrZWQoKSB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudEVuYWJsZVNlbGVjdEFsbCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudEVuYWJsZU11bHRpU2VsZWN0KSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5hbGxDaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZEl0ZW1zID0gW107XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRJdGVtcyA9IFtdO1xyXG4gICAgICBmb3IgKGNvbnN0IGVsIG9mIHRoaXMuY3VycmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLmNoZWNrZWRJdGVtcy5wdXNoKGVsKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5lbWl0U2VsZWN0aW9uQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gY2hlY2tib3ggc2VsZWN0IHBlciBmaWx0ZXJpbmcgY29sdW1uc1xyXG5cclxuICBpc0NvbHVtbkNoZWNrZWRGb3JGaWx0ZXJpbmcoaXRlbTogSU1haXNUYWJsZUNvbHVtbikge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcuaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbiAgfVxyXG5cclxuICBnZXQgYWxsRmlsdGVyaW5nQ29sdW1uc0NoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBkYXRhSXRlbXMgPSB0aGlzLmZpbHRlcmFibGVDb2x1bW5zO1xyXG4gICAgaWYgKCFkYXRhSXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBkYXRhSXRlbXMpIHtcclxuICAgICAgaWYgKHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcuaW5kZXhPZihpdGVtKSA9PT0gLTEpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFueUZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5maWx0ZXJhYmxlQ29sdW1ucztcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLmluZGV4T2YoaXRlbSkgIT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGdldCBub0ZpbHRlcmluZ0NvbHVtbnNoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gIXRoaXMuYW55RmlsdGVyaW5nQ29sdW1uc0NoZWNrZWQ7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVGaWx0ZXJpbmdDb2x1bW5DaGVja2VkKGl0ZW06IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlRmlsdGVyaW5nKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXRoaXMuaXNGaWx0ZXJhYmxlKGl0ZW0pKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5pc0NvbHVtbkNoZWNrZWRGb3JGaWx0ZXJpbmcoaXRlbSkpIHtcclxuICAgICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5zcGxpY2UodGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5pbmRleE9mKGl0ZW0pLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcucHVzaChpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdzZWFyY2ggcXVlcnkgY29sdW1ucyBjaGFuZ2VkIHdpdGggYWN0aXZlIHNlYXJjaCBxdWVyeSwgcmVsb2FkaW5nJyk7XHJcbiAgICAgIHRoaXMuYXBwbHlTZWxlY3RlZEZpbHRlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5lbWl0RmlsdGVyaW5nQ29sdW1uc1NlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKCkge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVGaWx0ZXJpbmcpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcgPSBbXTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcgPSBbXTtcclxuICAgICAgZm9yIChjb25zdCBlbCBvZiB0aGlzLmZpbHRlcmFibGVDb2x1bW5zKSB7XHJcbiAgICAgICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5wdXNoKGVsKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnNlbGVjdGVkU2VhcmNoUXVlcnkpIHtcclxuICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3NlYXJjaCBxdWVyeSBjb2x1bW5zIGNoYW5nZWQgd2l0aCBhY3RpdmUgc2VhcmNoIHF1ZXJ5LCByZWxvYWRpbmcnKTtcclxuICAgICAgdGhpcy5hcHBseVNlbGVjdGVkRmlsdGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLmVtaXRGaWx0ZXJpbmdDb2x1bW5zU2VsZWN0aW9uQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gY2hlY2tib3ggc2VsZWN0IHBlciB2aXNpYmxlIGNvbHVtbnNcclxuXHJcbiAgaXNDb2x1bW5DaGVja2VkRm9yVmlzdWFsaXphdGlvbihpdGVtOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gdGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24uaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbiAgfVxyXG5cclxuICBnZXQgYWxsVmlzaWJsZUNvbHVtbnNDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5oaWRlYWJsZUNvbHVtbnM7XHJcbiAgICBpZiAoIWRhdGFJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGFJdGVtcykge1xyXG4gICAgICBpZiAodGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24uaW5kZXhPZihpdGVtKSA9PT0gLTEpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFueVZpc2libGVDb2x1bW5zQ2hlY2tlZCgpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IGRhdGFJdGVtcyA9IHRoaXMuaGlkZWFibGVDb2x1bW5zO1xyXG4gICAgaWYgKCFkYXRhSXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBkYXRhSXRlbXMpIHtcclxuICAgICAgaWYgKHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLmluZGV4T2YoaXRlbSkgIT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGdldCBub1Zpc2libGVDb2x1bW5zaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICF0aGlzLmFueVZpc2libGVDb2x1bW5zQ2hlY2tlZDtcclxuICB9XHJcblxyXG4gIHRvZ2dsZVZpc2libGVDb2x1bW5DaGVja2VkKGl0ZW06IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIGlmICghTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzSGlkZWFibGUoaXRlbSwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpIHx8ICF0aGlzLmN1cnJlbnRFbmFibGVDb2x1bW5zU2VsZWN0aW9uKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5pc0NvbHVtbkNoZWNrZWRGb3JWaXN1YWxpemF0aW9uKGl0ZW0pKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLnNwbGljZSh0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5pbmRleE9mKGl0ZW0pLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLnB1c2goaXRlbSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLmVtaXRWaXNpYmxlQ29sdW1uc1NlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCgpIHtcclxuICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlQ29sdW1uc1NlbGVjdGlvbikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuYWxsVmlzaWJsZUNvbHVtbnNDaGVja2VkKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uID0gdGhpcy5jb2x1bW5zLmZpbHRlcihjID0+XHJcbiAgICAgICAgIU1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc0hpZGVhYmxlKGMsIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbiA9IFtdO1xyXG4gICAgICBmb3IgKGNvbnN0IGVsIG9mIHRoaXMuY29sdW1ucykge1xyXG4gICAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLnB1c2goZWwpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLmVtaXRWaXNpYmxlQ29sdW1uc1NlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIC8vIGNoZWNrYm94IHNlbGVjdCBwZXIgZmlsdHJpIGN1c3RvbVxyXG5cclxuICBpc0NvbnRleHRGaWx0ZXJDaGVja2VkKGl0ZW06IElNYWlzVGFibGVDb250ZXh0RmlsdGVyKSB7XHJcbiAgICByZXR1cm4gdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMuaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbiAgfVxyXG5cclxuICBnZXQgYWxsQ29udGV4dEZpbHRlckNoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBkYXRhSXRlbXMgPSB0aGlzLmN1cnJlbnRDb250ZXh0RmlsdGVycztcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5pbmRleE9mKGl0ZW0pID09PSAtMSkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXQgYW55Q29udGV4dEZpbHRlckNoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBkYXRhSXRlbXMgPSB0aGlzLmN1cnJlbnRDb250ZXh0RmlsdGVycztcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5pbmRleE9mKGl0ZW0pICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBnZXQgbm9Db250ZXh0RmlsdGVyc0NoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gIXRoaXMuYW55Q29udGV4dEZpbHRlckNoZWNrZWQ7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVDb250ZXh0RmlsdGVyQ2hlY2tlZChpdGVtOiBJTWFpc1RhYmxlQ29udGV4dEZpbHRlcikge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVDb250ZXh0RmlsdGVyaW5nKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5pc0NvbnRleHRGaWx0ZXJDaGVja2VkKGl0ZW0pKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzLnNwbGljZSh0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5pbmRleE9mKGl0ZW0pLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzLnB1c2goaXRlbSk7XHJcbiAgICAgIGlmIChpdGVtLmdyb3VwKSB7XHJcbiAgICAgICAgdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMgPSB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5maWx0ZXIobyA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gKG8ubmFtZSA9PT0gaXRlbS5uYW1lKSB8fCAoIW8uZ3JvdXApIHx8IChvLmdyb3VwICE9PSBpdGVtLmdyb3VwKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZW1pdENvbnRleHRGaWx0ZXJzU2VsZWN0aW9uQ2hhbmdlZCgpO1xyXG5cclxuICAgIHRoaXMuc2V0UGFnZSgwKTtcclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5yZWxvYWQoe3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLlVTRVJ9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYnVpbGRTdGF0dXNTbmFwc2hvdCgpOiBJTWFpc1RhYmxlU3RhdHVzU25hcHNob3Qge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgc2NoZW1hVmVyc2lvbjogdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY3VycmVudFNjaGVtYVZlcnNpb24gfHwgJ05PTkUnLFxyXG4gICAgICBvcmRlckNvbHVtbjogdGhpcy5jdXJyZW50U29ydENvbHVtbiA/IHRoaXMuY3VycmVudFNvcnRDb2x1bW4ubmFtZSA6IG51bGwsXHJcbiAgICAgIG9yZGVyQ29sdW1uRGlyZWN0aW9uOiB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uIHx8IG51bGwsXHJcbiAgICAgIHF1ZXJ5OiB0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeSxcclxuICAgICAgcXVlcnlDb2x1bW5zOiB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLm1hcChjID0+IGMubmFtZSksXHJcbiAgICAgIHZpc2libGVDb2x1bW5zOiB0aGlzLnZpc2libGVDb2x1bW5zLm1hcChjID0+IGMubmFtZSksXHJcbiAgICAgIGNvbnRleHRGaWx0ZXJzOiB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5tYXAoYyA9PiBjLm5hbWUpLFxyXG4gICAgICBjdXJyZW50UGFnZTogdGhpcy5jdXJyZW50UGFnZUluZGV4LFxyXG4gICAgICBwYWdlU2l6ZTogdGhpcy5jdXJyZW50UGFnZVNpemUsXHJcbiAgICAgIGNoZWNrZWRJdGVtczogIXRoaXMuY3VycmVudEVuYWJsZVNlbGVjdGlvbiA/IFtdIDogdGhpcy5jaGVja2VkSXRlbXMubWFwKHYgPT4gdiksXHJcbiAgICAgIGV4cGFuZGVkSXRlbXM6ICAhdGhpcy5jdXJyZW50RW5hYmxlUm93RXhwYW5zaW9uID8gW10gOiB0aGlzLmV4cGFuZGVkSXRlbXMubWFwKHYgPT4gdilcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1aWxkUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCgpOiBJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBzY2hlbWFWZXJzaW9uOiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jdXJyZW50U2NoZW1hVmVyc2lvbiB8fCAnTk9ORScsXHJcbiAgICAgIG9yZGVyQ29sdW1uOiB0aGlzLmN1cnJlbnRTb3J0Q29sdW1uID8gdGhpcy5jdXJyZW50U29ydENvbHVtbi5uYW1lIDogbnVsbCxcclxuICAgICAgb3JkZXJDb2x1bW5EaXJlY3Rpb246IHRoaXMuY3VycmVudFNvcnREaXJlY3Rpb24gfHwgbnVsbCxcclxuICAgICAgcXVlcnk6IHRoaXMuY3VycmVudFNlYXJjaFF1ZXJ5LFxyXG4gICAgICBxdWVyeUNvbHVtbnM6IHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcubWFwKGMgPT4gYy5uYW1lKSxcclxuICAgICAgdmlzaWJsZUNvbHVtbnM6IHRoaXMudmlzaWJsZUNvbHVtbnMubWFwKGMgPT4gYy5uYW1lKSxcclxuICAgICAgY29udGV4dEZpbHRlcnM6IHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzLm1hcChjID0+IGMubmFtZSksXHJcbiAgICAgIGN1cnJlbnRQYWdlOiB0aGlzLmN1cnJlbnRQYWdlSW5kZXgsXHJcbiAgICAgIHBhZ2VTaXplOiB0aGlzLmN1cnJlbnRQYWdlU2l6ZSxcclxuICAgICAgY2hlY2tlZEl0ZW1JZGVudGlmaWVyczogIXRoaXMuaXRlbVRyYWNraW5nRW5hYmxlZEFuZFBvc3NpYmxlID8gW10gOlxyXG4gICAgICAgIHRoaXMuY2hlY2tlZEl0ZW1zLm1hcChpdGVtID0+IHRoaXMuZ2V0SXRlbUlkZW50aWZpZXIoaXRlbSkpLmZpbHRlcih2ID0+ICEhdiksXHJcbiAgICAgIGV4cGFuZGVkSXRlbUlkZW50aWZpZXJzOiAgIXRoaXMuaXRlbVRyYWNraW5nRW5hYmxlZEFuZFBvc3NpYmxlID8gW10gOlxyXG4gICAgICAgIHRoaXMuZXhwYW5kZWRJdGVtcy5tYXAoaXRlbSA9PiB0aGlzLmdldEl0ZW1JZGVudGlmaWVyKGl0ZW0pKS5maWx0ZXIodiA9PiAhIXYpXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdGF0dXNDaGFuZ2VkKCkge1xyXG4gICAgdGhpcy5zdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRTdGF0dXNTbmFwc2hvdCgpO1xyXG4gICAgdGhpcy5wZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90ID0gdGhpcy5idWlsZFBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QoKTtcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCd0YWJsZSBzdGF0dXMgY2hhbmdlZCcsIHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCk7XHJcblxyXG4gICAgdGhpcy5lbWl0U3RhdHVzQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwZXJzaXN0U3RhdHVzVG9TdG9yZSgpOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgIGlmICghdGhpcy5zdGF0dXNTbmFwc2hvdCkge1xyXG4gICAgICByZXR1cm4gdGhyb3dFcnJvcignTm8gc3RhdHVzIHRvIHNhdmUnKTtcclxuICAgIH1cclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSggc3Vic2NyaWJlciA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnN0b3JlUGVyc2lzdGVuY2VFbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgncGFzc2luZyBzdGF0dXMgdG8gcGVyc2lzdGVuY2Ugc3RvcmUnKTtcclxuICAgICAgICBpZiAodGhpcy5zdG9yZUFkYXB0ZXIuc2F2ZSkge1xyXG4gICAgICAgICAgdGhpcy5zdG9yZUFkYXB0ZXIuc2F2ZSh7XHJcbiAgICAgICAgICAgIHN0YXR1czogdGhpcy5wZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90XHJcbiAgICAgICAgICB9KS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3NhdmVkIHN0YXR1cyBzbmFwc2hvdCB0byBwZXJzaXN0ZW5jZSBzdG9yZScpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLm5leHQoKTtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZmFpbHVyZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ2ZhaWxlZCB0byBzYXZlIHN0YXR1cyBzbmFwc2hvdCB0byBwZXJzaXN0ZW5jZSBzdG9yZScsIGZhaWx1cmUpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLmVycm9yKGZhaWx1cmUpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgc3Vic2NyaWJlci5lcnJvcignTm8gc2F2ZSBmdW5jdGlvbiBpbiBzdG9yZSBhZGFwdGVyJyk7XHJcbiAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHN1YnNjcmliZXIubmV4dCgpO1xyXG4gICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbG9hZFN0YXR1c0Zyb21TdG9yZSgpOiBPYnNlcnZhYmxlPElNYWlzVGFibGVQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbD4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKCBzdWJzY3JpYmVyID0+IHtcclxuICAgICAgaWYgKHRoaXMuc3RvcmVQZXJzaXN0ZW5jZUVuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdmZXRjaGluZyBzdGF0dXMgZnJvbSBwZXJzaXN0ZW5jZSBzdG9yZScpO1xyXG4gICAgICAgIGlmICh0aGlzLnN0b3JlQWRhcHRlci5sb2FkKSB7XHJcbiAgICAgICAgICB0aGlzLnN0b3JlQWRhcHRlci5sb2FkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdmZXRjaGVkIHN0YXR1cyBzbmFwc2hvdCBmcm9tIHBlcnNpc3RlbmNlIHN0b3JlJyk7XHJcbiAgICAgICAgICAgIHN1YnNjcmliZXIubmV4dChyZXN1bHQpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBmYWlsdXJlID0+IHtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5lcnJvcihmYWlsdXJlKTtcclxuICAgICAgICAgICAgdGhpcy5sb2dnZXIud2FybignZmFpbGVkIHRvIGZldGNoIHN0YXR1cyBzbmFwc2hvdCBmcm9tIHBlcnNpc3RlbmNlIHN0b3JlJywgZmFpbHVyZSk7XHJcbiAgICAgICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBzdWJzY3JpYmVyLmVycm9yKCdObyBsb2FkIGZ1bmN0aW9uIGluIHN0b3JlIGFkYXB0ZXInKTtcclxuICAgICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgc3Vic2NyaWJlci5uZXh0KG51bGwpO1xyXG4gICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldCBkcmFnSW5Qcm9ncmVzcygpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICQoJy5jZGstZHJhZy1wcmV2aWV3OnZpc2libGUnKS5sZW5ndGggK1xyXG4gICAgICAkKCcuY2RrLWRyYWctcGxhY2Vob2xkZXI6dmlzaWJsZScpLmxlbmd0aCArXHJcbiAgICAgICQoJy5jZGstZHJvcC1saXN0LWRyYWdnaW5nOnZpc2libGUnKS5sZW5ndGggK1xyXG4gICAgICAkKCcuY2RrLWRyb3AtbGlzdC1yZWNlaXZpbmc6dmlzaWJsZScpLmxlbmd0aFxyXG4gICAgKSA+IDA7XHJcbiAgfVxyXG5cclxuICAvLyBldmVudCBlbWl0dGVyc1xyXG5cclxuICBwcml2YXRlIGVtaXRTZWxlY3Rpb25DaGFuZ2VkKCkge1xyXG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UuZW1pdCh0aGlzLmNoZWNrZWRJdGVtcyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGVtaXRTb3J0Q2hhbmdlZCgpIHtcclxuICAgIHRoaXMuc29ydENoYW5nZS5lbWl0KHtcclxuICAgICAgY29sdW1uOiB0aGlzLmN1cnJlbnRTb3J0Q29sdW1uLFxyXG4gICAgICBkaXJlY3Rpb246IHRoaXMuY3VycmVudFNvcnREaXJlY3Rpb25cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbWl0UGFnZUNoYW5nZWQoKSB7XHJcbiAgICB0aGlzLnBhZ2VDaGFuZ2UuZW1pdCh0aGlzLnNlbGVjdGVkUGFnZUluZGV4KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW1pdEZpbHRlcmluZ0NvbHVtbnNTZWxlY3Rpb25DaGFuZ2VkKCkge1xyXG4gICAgdGhpcy5maWx0ZXJpbmdDb2x1bW5zQ2hhbmdlLmVtaXQodGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGVtaXRWaXNpYmxlQ29sdW1uc1NlbGVjdGlvbkNoYW5nZWQoKSB7XHJcbiAgICB0aGlzLnZpc2libGVDb2x1bW5zQ2hhbmdlLmVtaXQodGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbWl0Q29udGV4dEZpbHRlcnNTZWxlY3Rpb25DaGFuZ2VkKCkge1xyXG4gICAgdGhpcy5jb250ZXh0RmlsdGVyc0NoYW5nZS5lbWl0KHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW1pdFN0YXR1c0NoYW5nZWQoKSB7XHJcbiAgICBpZiAodGhpcy5zdGF0dXNTbmFwc2hvdCkge1xyXG4gICAgICB0aGlzLnN0YXR1c0NoYW5nZS5lbWl0KHRoaXMuc3RhdHVzU25hcHNob3QpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gTWF0VGFibGUgYWRhcHRlcnNcclxuICBnZXQgbWF0VGFibGVEYXRhKCk6IGFueVtdIHtcclxuICAgIHJldHVybiB0aGlzLmRhdGFTbmFwc2hvdDtcclxuICB9XHJcblxyXG4gIGdldCBtYXRUYWJsZVZpc2libGVDb2x1bW5EZWZzKCk6IHN0cmluZ1tdIHtcclxuICAgIGNvbnN0IG8gPSBbXTtcclxuICAgIGlmICh0aGlzLnJvd0V4cGFuc2lvbkVuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICBvLnB1c2goJ2ludGVybmFsX19fY29sX3Jvd19leHBhbnNpb24nKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVTZWxlY3Rpb24pIHtcclxuICAgICAgby5wdXNoKCdpbnRlcm5hbF9fX2NvbF9yb3dfc2VsZWN0aW9uJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jb250ZXh0RmlsdGVyaW5nRW5hYmxlZEFuZFBvc3NpYmxlKSB7XHJcbiAgICAgIG8ucHVzaCgnaW50ZXJuYWxfX19jb2xfY29udGV4dF9maWx0ZXJpbmcnKTtcclxuICAgIH1cclxuICAgIHRoaXMudmlzaWJsZUNvbHVtbnMubWFwKGMgPT4gYy5uYW1lKS5mb3JFYWNoKGMgPT4gby5wdXNoKGMpKTtcclxuICAgIHJldHVybiBvO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG1hdFRhYmxlVmlzaWJsZUNvbHVtbnMoKTogSU1haXNUYWJsZUNvbHVtbltdIHtcclxuICAgIHJldHVybiB0aGlzLnZpc2libGVDb2x1bW5zO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlTWF0VGFibGVEYXRhU25hcHNob3RDaGFuZ2VkKCkge1xyXG4gICAgY29uc3Qgcm93czogYW55W10gPSBbXTtcclxuXHJcbiAgICB0aGlzLmRhdGFTbmFwc2hvdC5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICBjb25zdCBkZXRhaWxSb3cgPSB7IF9fX2RldGFpbFJvd0NvbnRlbnQ6IHRydWUsIF9fX3BhcmVudFJvdzogZWxlbWVudCwgLi4uZWxlbWVudCB9O1xyXG4gICAgICBlbGVtZW50Ll9fX2RldGFpbFJvdyA9IGRldGFpbFJvdztcclxuICAgICAgcm93cy5wdXNoKGVsZW1lbnQsIGRldGFpbFJvdyk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBUT0RPIDogQlVJTEQgU1RBVElDIFJPVyBXSVRIIERBVEEgRVhUUkFDVE9SUyFcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdlbWl0dGluZyBzdGF0aWMgcm93cyBmb3IgbWF0LXRhYmxlICcgKyB0aGlzLmN1cnJlbnRUYWJsZUlkLCByb3dzKTtcclxuXHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgdGhpcy5tYXRUYWJsZURhdGFPYnNlcnZhYmxlPy5uZXh0KHJvd3MpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBpc01hdFRhYmxlRXhwYW5zaW9uRGV0YWlsUm93ID0gKGk6IG51bWJlciwgcm93OiBhbnkpID0+IHJvdy5oYXNPd25Qcm9wZXJ0eSgnX19fZGV0YWlsUm93Q29udGVudCcpO1xyXG5cclxuICBpc01hdFRhYmxlRXhwYW5kZWQgPSAocm93OiBhbnksIGxpbWl0ID0gZmFsc2UpID0+IHtcclxuICAgIGlmICghbGltaXQgJiYgcm93Ll9fX2RldGFpbFJvdykge1xyXG4gICAgICBpZiAodGhpcy5pc01hdFRhYmxlRXhwYW5kZWQocm93Ll9fX2RldGFpbFJvdywgdHJ1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKCFsaW1pdCAmJiByb3cuX19fcGFyZW50Um93KSB7XHJcbiAgICAgIGlmICh0aGlzLmlzTWF0VGFibGVFeHBhbmRlZChyb3cuX19fcGFyZW50Um93LCB0cnVlKSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMucm93RXhwYW5zaW9uRW5hYmxlZEFuZFBvc3NpYmxlKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5pc0V4cGFuZGFibGUocm93KSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuaXNFeHBhbmRlZChyb3cpKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbXBhdGliaWxpdHlNb2RlRm9yTWFpblRhYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKCF0aGlzLmlzSUUpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmN1cnJlbnRFbmFibGVEcm9wKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY29tcGF0aWJpbGl0eU1vZGVGb3JEcm9wRG93bnMoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuXHJcbiAgICBpZiAoIXRoaXMuaXNJRSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJtYWlzLXRhYmxlXCI+XHJcblxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXItZnVsbCBweC00IHB5LTUgdGFibGUtYWN0aW9ucyB0YWJsZS1hY3Rpb25zLS1oZWFkZXJcIlxyXG4gICAgKm5nSWY9XCJjb2x1bW5TZWxlY3Rpb25Qb3NzaWJsZUFuZEFsbG93ZWQgfHwgZmlsdGVyaW5nUG9zc2libGVBbmRBbGxvd2VkIHx8IGhlYWRlckFjdGlvbnNFbmFibGVkQW5kUG9zc2libGUgfHwgY29udGV4dEFjdGlvbnNFbmFibGVkQW5kUG9zc2libGVcIlxyXG4gID5cclxuICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbC00XCIgKm5nSWY9XCJmaWx0ZXJpbmdQb3NzaWJsZUFuZEFsbG93ZWRcIj5cclxuICAgICAgICA8Zm9ybSAoc3VibWl0KT1cImFwcGx5U2VsZWN0ZWRGaWx0ZXIoKVwiPlxyXG4gICAgICAgICAgPGRpdj57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmZpbHRlcl9wcm9tcHQnIHwgdHJhbnNsYXRlIH19PC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj5cclxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cInNlYXJjaFF1ZXJ5QWN0aXZlICYmICFzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci13YXJuaW5nXT1cInNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICBuYW1lPVwic2VsZWN0ZWRTZWFyY2hRdWVyeVwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmZpbHRlcl9wbGFjZWhvbGRlcicgfCB0cmFuc2xhdGUgfX1cIlxyXG4gICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJTZWFyY2hcIlxyXG4gICAgICAgICAgICAgIFsobmdNb2RlbCldPVwic2VsZWN0ZWRTZWFyY2hRdWVyeVwiXHJcbiAgICAgICAgICAgICAgKGZvY3Vzb3V0KT1cInNlYXJjaFF1ZXJ5Rm9jdXNPdXQoKVwiXHJcbiAgICAgICAgICAgID5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gRFJPUERPV04gSU4gV0VCS0lUIE1PREUgLS0+XHJcbiAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCIhY29tcGF0aWJpbGl0eU1vZGVGb3JEcm9wRG93bnNcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwLWFwcGVuZFwiIG5nYkRyb3Bkb3duIFthdXRvQ2xvc2VdPVwiJ291dHNpZGUnXCI+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0blwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYnRuLWJsYWNrXT1cIiFzZWFyY2hRdWVyeU5lZWRBcHBseSB8fCAhc2VhcmNoUXVlcnlBY3RpdmVcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi1wcmltYXJ5XT1cInNlYXJjaFF1ZXJ5TmVlZEFwcGx5ICYmIHNlYXJjaFF1ZXJ5QWN0aXZlXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItcHJpbWFyeV09XCJzZWFyY2hRdWVyeUFjdGl2ZSAmJiAhc2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci13YXJuaW5nXT1cInNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIiAoY2xpY2spPVwiYXBwbHlTZWxlY3RlZEZpbHRlcigpXCJcclxuICAgICAgICAgICAgICAgID48aSBjbGFzcz1cImZhcyBmYS1zZWFyY2hcIj48L2k+XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tYmxhY2tcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cInNlYXJjaFF1ZXJ5QWN0aXZlICYmICFzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXdhcm5pbmddPVwic2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi1ibGFja109XCIhKHNlbGVjdGVkU2VhcmNoUXVlcnkgJiYgbm9GaWx0ZXJpbmdDb2x1bW5zaGVja2VkKVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYnRuLXdhcm5pbmddPVwic2VsZWN0ZWRTZWFyY2hRdWVyeSAmJiBub0ZpbHRlcmluZ0NvbHVtbnNoZWNrZWRcIlxyXG4gICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiIG5nYkRyb3Bkb3duVG9nZ2xlPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25NZW51IGNsYXNzPVwiZHJvcGRvd24tbWVudVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bkl0ZW0gY2xhc3M9XCJkcm9wZG93bi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxoNiBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiPnt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMucGlja19maWx0ZXJpbmdfY29sdW1ucycgfCB0cmFuc2xhdGUgfX08L2g2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgY2xpY2thYmxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJhbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwidG9nZ2xlQWxsRmlsdGVyaW5nQ29sdW1uc0NoZWNrZWQoKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAgKGNsaWNrKT1cInRvZ2dsZUFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKCk7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2FsbF9maWx0ZXJpbmdfY29sdW1ucycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1kaXZpZGVyXCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiBjb2x1bW5zXCI+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBbY2xhc3MuZGlzYWJsZWRdPVwiIWlzRmlsdGVyYWJsZShjb2x1bW4pXCIgKm5nSWY9XCJoYXNMYWJlbChjb2x1bW4pICYmIChpc0ZpbHRlcmFibGUoY29sdW1uKSB8fCBjb25maWdGaWx0ZXJpbmdTaG93VW5maWx0ZXJhYmxlQ29sdW1ucylcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgY2xpY2thYmxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJpc0NvbHVtbkNoZWNrZWRGb3JGaWx0ZXJpbmcoY29sdW1uKVwiICpuZ0lmPVwiaXNGaWx0ZXJhYmxlKGNvbHVtbilcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZUZpbHRlcmluZ0NvbHVtbkNoZWNrZWQoY29sdW1uKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiZmFsc2VcIiAqbmdJZj1cIiFpc0ZpbHRlcmFibGUoY29sdW1uKVwiIFtkaXNhYmxlZF09XCJ0cnVlXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICAoY2xpY2spPVwidG9nZ2xlRmlsdGVyaW5nQ29sdW1uQ2hlY2tlZChjb2x1bW4pOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChjb2x1bW4pIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBEUk9QRE9XTiBJTiBJRTExIENPTVBBVElCSUxJVFkgTU9ERSAtLT5cclxuICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImNvbXBhdGliaWxpdHlNb2RlRm9yRHJvcERvd25zXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1hcHBlbmRcIiBuZ2JEcm9wZG93biBbYXV0b0Nsb3NlXT1cIidvdXRzaWRlJ1wiPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG5cIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi1ibGFja109XCIhc2VhcmNoUXVlcnlOZWVkQXBwbHkgfHwgIXNlYXJjaFF1ZXJ5QWN0aXZlXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4tcHJpbWFyeV09XCJzZWFyY2hRdWVyeU5lZWRBcHBseSAmJiBzZWFyY2hRdWVyeUFjdGl2ZVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwic2VhcmNoUXVlcnlBY3RpdmUgJiYgIXNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItd2FybmluZ109XCJzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCIgKGNsaWNrKT1cImFwcGx5U2VsZWN0ZWRGaWx0ZXIoKVwiXHJcbiAgICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJmYXMgZmEtc2VhcmNoXCI+PC9pPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWJsYWNrXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItcHJpbWFyeV09XCJzZWFyY2hRdWVyeUFjdGl2ZSAmJiAhc2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci13YXJuaW5nXT1cInNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4tYmxhY2tdPVwiIShzZWxlY3RlZFNlYXJjaFF1ZXJ5ICYmIG5vRmlsdGVyaW5nQ29sdW1uc2hlY2tlZClcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi13YXJuaW5nXT1cInNlbGVjdGVkU2VhcmNoUXVlcnkgJiYgbm9GaWx0ZXJpbmdDb2x1bW5zaGVja2VkXCJcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIiBuZ2JEcm9wZG93blRvZ2dsZT5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudSBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfZmlsdGVyaW5nX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19PC9oNj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gKGNsaWNrKT1cInRvZ2dsZUFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKClcIj5cclxuICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ2NoZWNrLXNxdWFyZSdcIiAqbmdJZj1cImFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInc3F1YXJlJ1wiICpuZ0lmPVwiIWFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfYWxsX2ZpbHRlcmluZ19jb2x1bW5zJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGl2aWRlclwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgY29sdW1uc1wiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gW2NsYXNzLmRpc2FibGVkXT1cIiFpc0ZpbHRlcmFibGUoY29sdW1uKVwiIChjbGljayk9XCJ0b2dnbGVGaWx0ZXJpbmdDb2x1bW5DaGVja2VkKGNvbHVtbilcIlxyXG4gICAgICAgICAgICAgICAgICAqbmdJZj1cImhhc0xhYmVsKGNvbHVtbikgJiYgKGlzRmlsdGVyYWJsZShjb2x1bW4pIHx8IGNvbmZpZ0ZpbHRlcmluZ1Nob3dVbmZpbHRlcmFibGVDb2x1bW5zKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInY2hlY2stc3F1YXJlJ1wiICpuZ0lmPVwiaXNGaWx0ZXJhYmxlKGNvbHVtbikgJiYgaXNDb2x1bW5DaGVja2VkRm9yRmlsdGVyaW5nKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidzcXVhcmUnXCIgKm5nSWY9XCJpc0ZpbHRlcmFibGUoY29sdW1uKSAmJiAhaXNDb2x1bW5DaGVja2VkRm9yRmlsdGVyaW5nKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidzcXVhcmUnXCIgKm5nSWY9XCIhaXNGaWx0ZXJhYmxlKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChjb2x1bW4pIH19XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Zvcm0+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLTNcIiAqbmdJZj1cImNvbHVtblNlbGVjdGlvblBvc3NpYmxlQW5kQWxsb3dlZFwiPlxyXG4gICAgICAgIDxkaXY+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5jdXN0b21pemVfdmlld19wcm9tcHQnIHwgdHJhbnNsYXRlIH19PC9kaXY+XHJcblxyXG4gICAgICAgIDwhLS0gRFJPUERPV04gSU4gV0VCS0lUIE1PREUgLS0+XHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cIiFjb21wYXRpYmlsaXR5TW9kZUZvckRyb3BEb3duc1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJidG4tZ3JvdXBcIiBuZ2JEcm9wZG93biBbYXV0b0Nsb3NlXT1cIidvdXRzaWRlJ1wiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tYmxhY2sgZHJvcGRvd24tdG9nZ2xlXCIgbmdiRHJvcGRvd25Ub2dnbGU+XHJcbiAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5hZGRfcmVtb3ZlX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudSBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIj5cclxuICAgICAgICAgICAgICA8aDYgbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnNlbGVjdF9jb2x1bW5zX3Byb21wdCcgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICA8L2g2PlxyXG4gICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgY2xpY2thYmxlXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZUFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCgpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiAgKGNsaWNrKT1cInRvZ2dsZUFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCgpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfYWxsX3Zpc2libGVfY29sdW1ucycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGl2aWRlclwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiBjb2x1bW5zXCI+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gY2xhc3M9XCJkcm9wZG93bi1pdGVtXCIgW2NsYXNzLmRpc2FibGVkXT1cIiFpc0hpZGVhYmxlKGNvbHVtbilcIiAqbmdJZj1cImhhc0xhYmVsKGNvbHVtbikgJiYgKGlzSGlkZWFibGUoY29sdW1uKSB8fCBjb25maWdDb2x1bW5WaXNpYmlsaXR5U2hvd0ZpeGVkQ29sdW1ucylcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNsaWNrYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVja1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJpc0NvbHVtbkNoZWNrZWRGb3JWaXN1YWxpemF0aW9uKGNvbHVtbilcIiAqbmdJZj1cImlzSGlkZWFibGUoY29sdW1uKVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZVZpc2libGVDb2x1bW5DaGVja2VkKGNvbHVtbik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJ0cnVlXCIgKm5nSWY9XCIhaXNIaWRlYWJsZShjb2x1bW4pXCIgW2Rpc2FibGVkXT1cInRydWVcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIChjbGljayk9XCJ0b2dnbGVWaXNpYmxlQ29sdW1uQ2hlY2tlZChjb2x1bW4pOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoY29sdW1uKSB9fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgICAgIDwhLS0gRFJPUERPV04gSU4gSUUxMSBDT01QQVRJQklMSVRZIE1PREUgLS0+XHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImNvbXBhdGliaWxpdHlNb2RlRm9yRHJvcERvd25zXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwXCIgbmdiRHJvcGRvd24gW2F1dG9DbG9zZV09XCInb3V0c2lkZSdcIj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tYmxhY2sgZHJvcGRvd24tdG9nZ2xlXCIgbmdiRHJvcGRvd25Ub2dnbGU+XHJcbiAgICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmFkZF9yZW1vdmVfY29sdW1ucycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudSBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxoNiBuZ2JEcm9wZG93bkl0ZW0gY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5zZWxlY3RfY29sdW1uc19wcm9tcHQnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICA8L2g2PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gKGNsaWNrKT1cInRvZ2dsZUFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCgpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidjaGVjay1zcXVhcmUnXCIgKm5nSWY9XCJhbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWRcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidzcXVhcmUnXCIgKm5nSWY9XCIhYWxsVmlzaWJsZUNvbHVtbnNDaGVja2VkXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfYWxsX3Zpc2libGVfY29sdW1ucycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWRpdmlkZXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgY29sdW1uIG9mIGNvbHVtbnNcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtICpuZ0lmPVwiaGFzTGFiZWwoY29sdW1uKSAmJiAoaXNIaWRlYWJsZShjb2x1bW4pIHx8IGNvbmZpZ0NvbHVtblZpc2liaWxpdHlTaG93Rml4ZWRDb2x1bW5zKVwiXHJcbiAgICAgICAgICAgICAgICAgIFtjbGFzcy5kaXNhYmxlZF09XCIhaXNIaWRlYWJsZShjb2x1bW4pXCIgKGNsaWNrKT1cInRvZ2dsZVZpc2libGVDb2x1bW5DaGVja2VkKGNvbHVtbilcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInY2hlY2stc3F1YXJlJ1wiICpuZ0lmPVwiaXNIaWRlYWJsZShjb2x1bW4pICYmIGlzQ29sdW1uQ2hlY2tlZEZvclZpc3VhbGl6YXRpb24oY29sdW1uKVwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInc3F1YXJlJ1wiICpuZ0lmPVwiaXNIaWRlYWJsZShjb2x1bW4pICYmICFpc0NvbHVtbkNoZWNrZWRGb3JWaXN1YWxpemF0aW9uKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ2NoZWNrLXNxdWFyZSdcIiAqbmdJZj1cIiFpc0hpZGVhYmxlKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGNvbHVtbikgfX1cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtM1wiICpuZ0lmPVwiIWNvbHVtblNlbGVjdGlvblBvc3NpYmxlQW5kQWxsb3dlZFwiPlxyXG4gICAgICAgIDwhLS0gRklMTEVSIC0tPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbC00XCIgKm5nSWY9XCIhZmlsdGVyaW5nUG9zc2libGVBbmRBbGxvd2VkXCI+XHJcbiAgICAgICAgPCEtLSBGSUxMRVIgLS0+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLTUgdGV4dC1yaWdodFwiPlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwiYWN0aW9uc0NhcHRpb25UZW1wbGF0ZVwiIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7fVwiPiA8L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCJoZWFkZXJBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlXCI+XHJcbiAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXt7IGFjdGlvbi5kaXNwbGF5Q2xhc3MgfHwgJ2xpZ2h0JyB9fSB7eyBhY3Rpb24uYWRkaXRpb25hbENsYXNzZXMgfHwgJycgfX0gbXItMVwiICpuZ0Zvcj1cImxldCBhY3Rpb24gb2YgaGVhZGVyQWN0aW9uc1wiXHJcbiAgICAgICAgICAoY2xpY2spPVwiY2xpY2tPbkNvbnRleHRBY3Rpb24oYWN0aW9uKVwiIFtkaXNhYmxlZF09XCIhaXNDb250ZXh0QWN0aW9uQWxsb3dlZChhY3Rpb24pXCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoYWN0aW9uKSB9fVxyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwXCIgbmdiRHJvcGRvd24gKm5nSWY9XCJjb250ZXh0QWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tbGlnaHQgZHJvcGRvd24tdG9nZ2xlXCIgW2Rpc2FibGVkXT1cIiFhbnlCdXR0b25BY3Rpb25zQWxsb3dlZFwiIG5nYkRyb3Bkb3duVG9nZ2xlPnt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuYWN0aW9uc19idXR0b24nIHwgdHJhbnNsYXRlIH19PC9idXR0b24+XHJcbiAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudT5cclxuICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bkl0ZW0+XHJcbiAgICAgICAgICAgICAgPGg2IGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCI+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2FjdGlvbicgfCB0cmFuc2xhdGUgfX08L2g2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gKm5nRm9yPVwibGV0IGFjdGlvbiBvZiBjdXJyZW50QWN0aW9uc1wiIChjbGljayk9XCJjbGlja09uQ29udGV4dEFjdGlvbihhY3Rpb24pXCIgW2Rpc2FibGVkXT1cIiFpc0NvbnRleHRBY3Rpb25BbGxvd2VkKGFjdGlvbilcIj5cclxuICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoYWN0aW9uKSB9fVxyXG4gICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG5cclxuICA8IS0tIE1BSU4gVEFCTEUgLSBXRUJLSVQgTU9ERSAtLT5cclxuICA8bmctY29udGFpbmVyICpuZ0lmPVwiIWNvbXBhdGliaWxpdHlNb2RlRm9yTWFpblRhYmxlXCI+XHJcbiAgPGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIj5cclxuICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLWRhcmsgdGFibGUtaG92ZXJcIlxyXG4gICAgICBjZGtEcm9wTGlzdFxyXG4gICAgICBpZD1cInt7Y3VycmVudFRhYmxlSWR9fVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdENvbm5lY3RlZFRvXT1cImRyb3BDb25uZWN0ZWRUb1wiXHJcbiAgICAgIChjZGtEcm9wTGlzdERyb3BwZWQpPVwiaGFuZGxlSXRlbURyb3BwZWQoJGV2ZW50KVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdERhdGFdPVwiY3VycmVudERhdGFcIlxyXG4gICAgICBbY2RrRHJvcExpc3RFbnRlclByZWRpY2F0ZV09XCJhY2NlcHREcm9wUHJlZGljYXRlXCJcclxuICAgICAgW2Nka0Ryb3BMaXN0U29ydGluZ0Rpc2FibGVkXT1cIiFhY2NlcHREcm9wXCJcclxuICAgICAgW2NsYXNzLm5vZHJvcF09XCIhYWNjZXB0RHJvcFwiXHJcbiAgICAgIFtjbGFzcy5hY2NlcHRkcm9wXT1cImFjY2VwdERyb3BcIlxyXG4gICAgPlxyXG4gICAgICA8Y2FwdGlvbiBjbGFzcz1cImQtbm9uZVwiPnt7ICd0YWJsZS5jb21tb24uYWNjZXNzaWJpbGl0eS5jYXB0aW9uJyB8IHRyYW5zbGF0ZSB9fTwvY2FwdGlvbj5cclxuICAgICAgPHRoZWFkIGNsYXNzPVwiXCI+XHJcbiAgICAgICAgPHRyPlxyXG4gICAgICAgICAgPHRoIHNjb3BlPVwicm93XCIgKm5nSWY9XCJyb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGVcIiBjbGFzcz1cInNtYWxsLWFzLXBvc3NpYmxlXCI+XHJcbiAgICAgICAgICAgIDwhLS0gZW1wdHkgaGVhZGVyIGZvciByb3cgZXhwYW5zaW9uIC0tPlxyXG4gICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDx0aCBzY29wZT1cInJvd1wiICpuZ0lmPVwiY3VycmVudEVuYWJsZVNlbGVjdGlvblwiIGNsYXNzPVwic21hbGwtYXMtcG9zc2libGVcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImN1cnJlbnRFbmFibGVTZWxlY3RBbGwgJiYgY3VycmVudEVuYWJsZU11bHRpU2VsZWN0ICYmICFub1Jlc3VsdHNcIj5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJhbGxDaGVja2VkXCIgKGNoYW5nZSk9XCJ0b2dnbGVBbGxDaGVja2VkKClcIiAvPlxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPHRoIHNjb3BlPVwicm93XCIgKm5nSWY9XCJjb250ZXh0RmlsdGVyaW5nRW5hYmxlZEFuZFBvc3NpYmxlXCIgc3R5bGU9XCJ3aWR0aDogMSU7XCJcclxuICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cImFueUNvbnRleHRGaWx0ZXJDaGVja2VkXCIgbmdiRHJvcGRvd24gW2F1dG9DbG9zZV09XCInb3V0c2lkZSdcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVjayBjbGlja2FibGVcIiBuZ2JEcm9wZG93blRvZ2dsZT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93biBzaG93IGQtaW5saW5lLWJsb2NrIGZsb2F0LXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIiBuZ2JEcm9wZG93bk1lbnU+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIiBuZ2JEcm9wZG93bkl0ZW0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiY29udGV4dEZpbHRlcmluZ1Byb21wdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGNvbnRleHRGaWx0ZXJpbmdQcm9tcHQgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cIiFjb250ZXh0RmlsdGVyaW5nUHJvbXB0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5jb250ZXh0X2ZpbHRlcl9wcm9tcHQnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvaDY+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24taXRlbVwiIG5nYkRyb3Bkb3duSXRlbSAqbmdGb3I9XCJsZXQgZmlsdGVyIG9mIGN1cnJlbnRDb250ZXh0RmlsdGVyc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiaXNDb250ZXh0RmlsdGVyQ2hlY2tlZChmaWx0ZXIpXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZUNvbnRleHRGaWx0ZXJDaGVja2VkKGZpbHRlcik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiY2xpY2thYmxlXCIgKGNsaWNrKT1cInRvZ2dsZUNvbnRleHRGaWx0ZXJDaGVja2VkKGZpbHRlcik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoZmlsdGVyKSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgY29sdW1uIG9mIHZpc2libGVDb2x1bW5zXCI+XHJcbiAgICAgICAgICAgIDx0aCBzY29wZT1cImNvbFwiXHJcbiAgICAgICAgICAgICAgW2NsYXNzLm9yZGVyLXVwXT1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uLCAnQVNDJylcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5vcmRlci1kb3duXT1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uLCAnREVTQycpXCJcclxuICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4pXCJcclxuICAgICAgICAgICAgICBbY2xhc3MuY2xpY2thYmxlXT1cImlzU29ydGFibGUoY29sdW1uKVwiXHJcbiAgICAgICAgICAgICAgKGNsaWNrKT1cImNsaWNrT25Db2x1bW4oY29sdW1uKVwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uKVwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbiwgJ0FTQycpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwib3JkZXItYnRuIGJ0biBidG4tbm9uZVwiIChjbGljayk9XCJjbGlja09uQ29sdW1uKGNvbHVtbilcIj48aSBjbGFzcz1cImZhcyBmYS1sb25nLWFycm93LWFsdC11cFwiPjwvaT48L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdERVNDJylcIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJvcmRlci1idG4gYnRuIGJ0bi1ub25lXCIgKGNsaWNrKT1cImNsaWNrT25Db2x1bW4oY29sdW1uKVwiPjxpIGNsYXNzPVwiZmFzIGZhLWxvbmctYXJyb3ctYWx0LXVwXCI+PC9pPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzTGFiZWxWaXNpYmxlSW5UYWJsZShjb2x1bW4pICYmIGhhc0xhYmVsKGNvbHVtbilcIj5cclxuICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChjb2x1bW4pIH19XHJcbiAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgPC90cj5cclxuICAgICAgPC90aGVhZD5cclxuICAgICAgPHRib2R5XHJcbiAgICAgICAgKm5nSWY9XCIhc2hvd0ZldGNoaW5nICYmICFmb3JjZVJlUmVuZGVyXCJcclxuICAgICAgPlxyXG4gICAgICAgIDx0ciBjbGFzcz1cInN0YXJ0LXJvdy1ob29rIGQtbm9uZVwiPjwvdHI+XHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgcm93IG9mIGN1cnJlbnREYXRhXCI+XHJcbiAgICAgICAgPHRyXHJcbiAgICAgICAgICBbY2xhc3Mucm93LXNlbGVjdGVkXT1cImlzQ2hlY2tlZChyb3cpXCJcclxuICAgICAgICAgIGNsYXNzPVwiZGF0YS1yb3dcIlxyXG4gICAgICAgICAgY2RrRHJhZ1xyXG4gICAgICAgICAgW2Nka0RyYWdEYXRhXT1cInJvd1wiXHJcbiAgICAgICAgICBbY2RrRHJhZ0Rpc2FibGVkXT1cIiFhY2NlcHREcmFnXCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICA8dGggc2NvcGU9XCJyb3dcIiAqbmdJZj1cInJvd0V4cGFuc2lvbkVuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICA8ZmEtaWNvbiBjbGFzcz1cImNsaWNrYWJsZVwiIFtpY29uXT1cIidjYXJldC1zcXVhcmUtZG93bidcIiAqbmdJZj1cIiFpc0V4cGFuZGVkKHJvdykgJiYgaXNFeHBhbmRhYmxlKHJvdylcIiAoY2xpY2spPVwiY2xpY2tPblJvd0V4cGFuc2lvbihyb3cpXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICA8ZmEtaWNvbiBjbGFzcz1cImNsaWNrYWJsZVwiIFtpY29uXT1cIidjYXJldC1zcXVhcmUtdXAnXCIgKm5nSWY9XCJpc0V4cGFuZGVkKHJvdylcIiAoY2xpY2spPVwiY2xpY2tPblJvd0V4cGFuc2lvbihyb3cpXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDx0aCBzY29wZT1cInJvd1wiICpuZ0lmPVwiY3VycmVudEVuYWJsZVNlbGVjdGlvblwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVja1wiPlxyXG4gICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJpc0NoZWNrZWQocm93KVwiIChjaGFuZ2UpPVwidG9nZ2xlQ2hlY2tlZChyb3cpXCIgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPHRkICpuZ0lmPVwiY29udGV4dEZpbHRlcmluZ0VuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICA8IS0tIGVtcHR5IGNlbGwgZm9yIGNvbnRleHQgZmlsdGVyaW5nIC0tPlxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiB2aXNpYmxlQ29sdW1uc1wiPlxyXG4gICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiY29sdW1uLmFwcGx5VGVtcGxhdGVcIj5cclxuICAgICAgICAgICAgICA8bmctdGVtcGxhdGVcclxuICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwiY2VsbFRlbXBsYXRlXCJcclxuICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiByb3csIGNvbHVtbjogY29sdW1uLCB2YWx1ZTogZXh0cmFjdFZhbHVlKHJvdywgY29sdW1uKSB9XCI+XHJcbiAgICAgICAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgKm5nSWY9XCIhY29sdW1uLmFwcGx5VGVtcGxhdGVcIj5cclxuICAgICAgICAgICAgICB7eyBleHRyYWN0VmFsdWUocm93LCBjb2x1bW4pIH19XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgPHRkIGNsYXNzPVwiZC1ub25lXCI+XHJcbiAgICAgICAgICAgIDx0ciAqY2RrRHJhZ1BsYWNlaG9sZGVyIGNsYXNzPVwiZHJvcC1jb250YWluZXJcIiBbaGlkZGVuXT1cIiFyZWZlcmVuY2VkVGFibGUuYWNjZXB0RHJvcFwiPlxyXG4gICAgICAgICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgICAgICAgIDx0ZCBbYXR0ci5jb2xzcGFuXT1cImFjdGl2ZUNvbHVtbnNDb3VudCAtIDFcIiBjbGFzcz1cImRyb3AtY2VsbFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3AtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwicmVmZXJlbmNlZFRhYmxlLmRyb3BUZW1wbGF0ZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwieyByb3c6IHJvdyB9XCI+XHJcbiAgICAgICAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICAgIDx0ZCBjbGFzcz1cImQtbm9uZVwiPlxyXG4gICAgICAgICAgICA8ZGl2ICpjZGtEcmFnUHJldmlldyBjbGFzcz1cImRyYWctY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldF09XCJkcmFnVGVtcGxhdGVcIlxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiByb3cgfVwiPlxyXG4gICAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICA8L3RyPlxyXG4gICAgICAgIDx0ciAqbmdJZj1cInJvd0V4cGFuc2lvbkVuYWJsZWRBbmRQb3NzaWJsZSAmJiBpc0V4cGFuZGVkKHJvdylcIiBjbGFzcz1cImRldGFpbC1yb3dcIj5cclxuICAgICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgICAgPHRkIFthdHRyLmNvbHNwYW5dPVwiYWN0aXZlQ29sdW1uc0NvdW50IC0gMVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93LWV4cGFuc2lvbi1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICA8bmctdGVtcGxhdGVcclxuICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwicm93RGV0YWlsVGVtcGxhdGVcIlxyXG4gICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwieyByb3c6IHJvdyB9XCI+XHJcbiAgICAgICAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RkPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgIDwvdGJvZHk+XHJcbiAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCIhc2hvd0ZldGNoaW5nICYmIG5vUmVzdWx0c1wiPlxyXG4gICAgICA8dGJvZHk+XHJcbiAgICAgICAgPHRyIGNsYXNzPVwibWVzc2FnZS1yb3cgbm8tcmVzdWx0cy1yb3dcIj5cclxuICAgICAgICAgIDx0ZCBbYXR0ci5jb2xzcGFuXT1cImFjdGl2ZUNvbHVtbnNDb3VudFwiIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cclxuICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5ub19yZXN1bHRzJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICA8L3RyPlxyXG4gICAgICA8L3Rib2R5PlxyXG4gICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cInNob3dGZXRjaGluZ1wiPlxyXG4gICAgICA8dGJvZHk+XHJcbiAgICAgICAgPHRyIGNsYXNzPVwibWVzc2FnZS1yb3cgZmV0Y2hpbmctcm93XCI+XHJcbiAgICAgICAgICA8dGQgW2F0dHIuY29sc3Bhbl09XCJhY3RpdmVDb2x1bW5zQ291bnRcIiBjbGFzcz1cInRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuZmV0Y2hpbmcnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICA8L3RkPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgIDwvdGJvZHk+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgPC90YWJsZT5cclxuICA8L2Rpdj5cclxuICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgPCEtLSBNQUlOIFRBQkxFIC0gSUUxMSBDT01QQVRJQklMSVRZIE1PREUgLS0+XHJcbiAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImNvbXBhdGliaWxpdHlNb2RlRm9yTWFpblRhYmxlXCI+XHJcbiAgPGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIiAqbmdJZj1cIm1hdFRhYmxlRGF0YU9ic2VydmFibGVcIj5cclxuXHJcbiAgICA8bWF0LXRhYmxlXHJcbiAgICAgICNyZW5kZXJlZE1hdFRhYmxlXHJcbiAgICAgIFtkYXRhU291cmNlXT1cIm1hdFRhYmxlRGF0YU9ic2VydmFibGVcIlxyXG4gICAgICBjbGFzcz1cInRhYmxlIHRhYmxlLWRhcmsgdGFibGUtaG92ZXJcIlxyXG4gICAgICBjZGtEcm9wTGlzdFxyXG4gICAgICBpZD1cInt7Y3VycmVudFRhYmxlSWR9fVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdENvbm5lY3RlZFRvXT1cImRyb3BDb25uZWN0ZWRUb1wiXHJcbiAgICAgIChjZGtEcm9wTGlzdERyb3BwZWQpPVwiaGFuZGxlSXRlbURyb3BwZWQoJGV2ZW50KVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdERhdGFdPVwiY3VycmVudERhdGFcIlxyXG4gICAgICBbY2RrRHJvcExpc3RFbnRlclByZWRpY2F0ZV09XCJhY2NlcHREcm9wUHJlZGljYXRlXCJcclxuICAgICAgW2Nka0Ryb3BMaXN0U29ydGluZ0Rpc2FibGVkXT1cIiFhY2NlcHREcm9wXCJcclxuICAgICAgW2NsYXNzLm5vZHJvcF09XCIhYWNjZXB0RHJvcFwiXHJcbiAgICAgIFtjbGFzcy5hY2NlcHRkcm9wXT1cImFjY2VwdERyb3BcIlxyXG4gICAgICAqbmdJZj1cIiFzaG93RmV0Y2hpbmcgJiYgIWZvcmNlUmVSZW5kZXJcIlxyXG4gICAgPlxyXG4gICAgICA8IS0tIFVzZXIgbmFtZSBEZWZpbml0aW9uIC0tPlxyXG4gICAgICA8bmctY29udGFpbmVyIFttYXRDb2x1bW5EZWZdPVwiJ2ludGVybmFsX19fY29sX3Jvd19leHBhbnNpb24nXCI+XHJcbiAgICAgICAgPG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZiBjbGFzcz1cIm1haXNzaXplLW1pblwiPlxyXG4gICAgICAgICAgPCEtLSBFTVBUWSAtLT5cclxuICAgICAgICA8L21hdC1oZWFkZXItY2VsbD5cclxuICAgICAgICA8bWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCIgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDxmYS1pY29uIGNsYXNzPVwiY2xpY2thYmxlXCIgW2ljb25dPVwiJ2NhcmV0LXNxdWFyZS1kb3duJ1wiICpuZ0lmPVwiIWlzRXhwYW5kZWQocm93KSAmJiBpc0V4cGFuZGFibGUocm93KVwiIChjbGljayk9XCJjbGlja09uUm93RXhwYW5zaW9uKHJvdylcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICA8ZmEtaWNvbiBjbGFzcz1cImNsaWNrYWJsZVwiIFtpY29uXT1cIidjYXJldC1zcXVhcmUtdXAnXCIgKm5nSWY9XCJpc0V4cGFuZGVkKHJvdylcIiAoY2xpY2spPVwiY2xpY2tPblJvd0V4cGFuc2lvbihyb3cpXCI+PC9mYS1pY29uPlxyXG4gICAgICAgIDwvbWF0LWNlbGw+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICA8bmctY29udGFpbmVyIFttYXRDb2x1bW5EZWZdPVwiJ2ludGVybmFsX19fY29sX3Jvd19zZWxlY3Rpb24nXCI+XHJcbiAgICAgICAgPG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZiBjbGFzcz1cIm1haXNzaXplLW1pblwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJjdXJyZW50RW5hYmxlU2VsZWN0QWxsICYmIGN1cnJlbnRFbmFibGVNdWx0aVNlbGVjdCAmJiAhbm9SZXN1bHRzXCI+XHJcbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImFsbENoZWNrZWRcIiAoY2hhbmdlKT1cInRvZ2dsZUFsbENoZWNrZWQoKVwiIC8+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWhlYWRlci1jZWxsPlxyXG4gICAgICAgIDxtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIiBjbGFzcz1cIm1haXNzaXplLW1pblwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImlzQ2hlY2tlZChyb3cpXCIgKGNoYW5nZSk9XCJ0b2dnbGVDaGVja2VkKHJvdylcIiAvPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9tYXQtY2VsbD5cclxuICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgIDxuZy1jb250YWluZXIgW21hdENvbHVtbkRlZl09XCInaW50ZXJuYWxfX19jb2xfY29udGV4dF9maWx0ZXJpbmcnXCI+XHJcbiAgICAgICAgPG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZiBjbGFzcz1cIm1haXNzaXplLW1pblwiPlxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwiYW55Q29udGV4dEZpbHRlckNoZWNrZWRcIlxyXG4gICAgICAgICAgICBuZ2JEcm9wZG93blxyXG4gICAgICAgICAgICBbYXV0b0Nsb3NlXT1cIidvdXRzaWRlJ1wiXHJcbiAgICAgICAgICAgIFtjb250YWluZXJdPVwiJ2JvZHknXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2sgY2xpY2thYmxlXCIgbmdiRHJvcGRvd25Ub2dnbGU+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duIHNob3cgZC1pbmxpbmUtYmxvY2sgZmxvYXQtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIiBuZ2JEcm9wZG93bk1lbnU+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg2IGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCIgbmdiRHJvcGRvd25JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJjb250ZXh0RmlsdGVyaW5nUHJvbXB0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGNvbnRleHRGaWx0ZXJpbmdQcm9tcHQgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiIWNvbnRleHRGaWx0ZXJpbmdQcm9tcHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5jb250ZXh0X2ZpbHRlcl9wcm9tcHQnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9oNj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24taXRlbVwiIG5nYkRyb3Bkb3duSXRlbSAqbmdGb3I9XCJsZXQgZmlsdGVyIG9mIGN1cnJlbnRDb250ZXh0RmlsdGVyc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImlzQ29udGV4dEZpbHRlckNoZWNrZWQoZmlsdGVyKVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwidG9nZ2xlQ29udGV4dEZpbHRlckNoZWNrZWQoZmlsdGVyKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiY2xpY2thYmxlXCIgKGNsaWNrKT1cInRvZ2dsZUNvbnRleHRGaWx0ZXJDaGVja2VkKGZpbHRlcik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGZpbHRlcikgfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21hdC1oZWFkZXItY2VsbD5cclxuICAgICAgICA8bWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCIgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDwhLS0gRU1QVFkgLS0+XHJcbiAgICAgICAgPC9tYXQtY2VsbD5cclxuICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgIDxuZy1jb250YWluZXIgW21hdENvbHVtbkRlZl09XCInaW50ZXJuYWxfX19jb2xfcm93X2RldGFpbCdcIj5cclxuICAgICAgICA8bWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgZGV0YWlsXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93LWV4cGFuc2lvbi1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldF09XCJyb3dEZXRhaWxUZW1wbGF0ZVwiXHJcbiAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwieyByb3c6IGRldGFpbC5fX19wYXJlbnRSb3cgfVwiPlxyXG4gICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9tYXQtY2VsbD5cclxuICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgIDxuZy1jb250YWluZXIgW21hdENvbHVtbkRlZl09XCJjb2x1bW4ubmFtZVwiICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgbWF0VGFibGVWaXNpYmxlQ29sdW1uc1wiPlxyXG4gICAgICAgIDxtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWYgY2xhc3M9XCJtYWlzc2l6ZS17eyBjb2x1bW4uc2l6ZSB8fCAnZGVmYXVsdCcgfX1cIiA+XHJcbiAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgIGNsYXNzPVwie3sgY29sdW1uLmhlYWRlckRpc3BsYXlDbGFzcyB8fCAnJyB9fVwiXHJcbiAgICAgICAgICAgIFtjbGFzcy5vcmRlci11cF09XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbiwgJ0FTQycpXCJcclxuICAgICAgICAgICAgW2NsYXNzLm9yZGVyLWRvd25dPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdERVNDJylcIlxyXG4gICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4pXCJcclxuICAgICAgICAgICAgW2NsYXNzLmNsaWNrYWJsZV09XCJpc1NvcnRhYmxlKGNvbHVtbilcIlxyXG4gICAgICAgICAgICAoY2xpY2spPVwiY2xpY2tPbkNvbHVtbihjb2x1bW4pXCI+XHJcbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4pXCI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbiwgJ0FTQycpXCI+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cIm9yZGVyLWJ0biBidG4gYnRuLW5vbmVcIiAoY2xpY2spPVwiY2xpY2tPbkNvbHVtbihjb2x1bW4pXCI+PGkgY2xhc3M9XCJmYXMgZmEtbG9uZy1hcnJvdy1hbHQtdXBcIj48L2k+PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdERVNDJylcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwib3JkZXItYnRuIGJ0biBidG4tbm9uZVwiIChjbGljayk9XCJjbGlja09uQ29sdW1uKGNvbHVtbilcIj48aSBjbGFzcz1cImZhcyBmYS1sb25nLWFycm93LWFsdC11cFwiPjwvaT48L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0xhYmVsVmlzaWJsZUluVGFibGUoY29sdW1uKSAmJiBoYXNMYWJlbChjb2x1bW4pXCI+XHJcbiAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGNvbHVtbikgfX1cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9tYXQtaGVhZGVyLWNlbGw+XHJcbiAgICAgICAgPG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiIGNsYXNzPVwibWFpc3NpemUte3sgY29sdW1uLnNpemUgfHwgJ2RlZmF1bHQnIH19IHt7IGNvbHVtbi5jZWxsRGlzcGxheUNsYXNzIHx8ICcnIH19XCI+XHJcbiAgICAgICAgICA8ZGl2ICpuZ0lmPVwiY29sdW1uLmFwcGx5VGVtcGxhdGVcIj5cclxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldF09XCJjZWxsVGVtcGxhdGVcIlxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiByb3csIGNvbHVtbjogY29sdW1uLCB2YWx1ZTogZXh0cmFjdFZhbHVlKHJvdywgY29sdW1uKSB9XCI+XHJcbiAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgKm5nSWY9XCIhY29sdW1uLmFwcGx5VGVtcGxhdGVcIj5cclxuICAgICAgICAgICAge3sgZXh0cmFjdFZhbHVlKHJvdywgY29sdW1uKSB9fVxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9tYXQtY2VsbD5cclxuICAgICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgICA8IS0tIEhlYWRlciBhbmQgUm93IERlY2xhcmF0aW9ucyAtLT5cclxuICAgICAgPG1hdC1oZWFkZXItcm93ICptYXRIZWFkZXJSb3dEZWY9XCJtYXRUYWJsZVZpc2libGVDb2x1bW5EZWZzXCI+PC9tYXQtaGVhZGVyLXJvdz5cclxuICAgICAgPG1hdC1yb3cgKm1hdFJvd0RlZj1cImxldCByb3c7IGNvbHVtbnM6IG1hdFRhYmxlVmlzaWJsZUNvbHVtbkRlZnM7XCJcclxuICAgICAgICBjbGFzcz1cImVsZW1lbnQtcm93IGRhdGEtcm93XCJcclxuICAgICAgICBbY2xhc3MuZXhwYW5kZWRdPVwiaXNNYXRUYWJsZUV4cGFuZGVkKHJvdylcIlxyXG4gICAgICAgIFtjbGFzcy5yb3ctc2VsZWN0ZWRdPVwiaXNDaGVja2VkKHJvdylcIlxyXG4gICAgICAgIGNka0RyYWdcclxuICAgICAgICBbY2RrRHJhZ0RhdGFdPVwicm93XCJcclxuICAgICAgICBbY2RrRHJhZ0Rpc2FibGVkXT1cIiFhY2NlcHREcmFnXCJcclxuICAgICAgPjwvbWF0LXJvdz5cclxuICAgICAgPG1hdC1yb3cgKm1hdFJvd0RlZj1cImxldCByb3c7IGNvbHVtbnM6IFsnaW50ZXJuYWxfX19jb2xfcm93X2RldGFpbCddOyB3aGVuOiBpc01hdFRhYmxlRXhwYW5zaW9uRGV0YWlsUm93XCJcclxuICAgICAgICAgIFtAZGV0YWlsRXhwYW5kXT1cImlzTWF0VGFibGVFeHBhbmRlZChyb3cpID8gJ2V4cGFuZGVkJyA6ICdjb2xsYXBzZWQnXCJcclxuICAgICAgICAgIHN0eWxlPVwib3ZlcmZsb3c6IGhpZGRlblwiPlxyXG4gICAgICA8L21hdC1yb3c+XHJcbiAgICA8L21hdC10YWJsZT5cclxuXHJcbiAgICA8bmctY29udGFpbmVyICpuZ0lmPVwiIXNob3dGZXRjaGluZyAmJiBub1Jlc3VsdHNcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJtZXNzYWdlLXJvdyBuby1yZXN1bHRzLXJvd1wiPlxyXG4gICAgICA8cCBjbGFzcz1cImFsZXJ0IGFsZXJ0LXByaW1hcnkgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLm5vX3Jlc3VsdHMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgIDwvcD5cclxuICAgIDwvZGl2PlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcbiAgICA8bmctY29udGFpbmVyICpuZ0lmPVwic2hvd0ZldGNoaW5nXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibWVzc2FnZS1yb3cgZmV0Y2hpbmctcm93XCI+XHJcbiAgICAgIDxwIGNsYXNzPVwiYWxlcnQgYWxlcnQtcHJpbWFyeSB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuZmV0Y2hpbmcnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgIDwvcD5cclxuICAgIDwvZGl2PlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gIDwvZGl2PlxyXG4gIDwvbmctY29udGFpbmVyPlxyXG5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZ1bGwgcHgtNCBweS00IHRhYmxlLWFjdGlvbnMgdGFibGUtYWN0aW9ucy0tZm9vdGVyXCJcclxuICAgICpuZ0lmPVwiY3VycmVudEVuYWJsZVBhZ2luYXRpb24gfHwgaGVhZGVyQWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZSB8fCBjb250ZXh0QWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZVwiXHJcbiAgPlxyXG4gICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtN1wiPlxyXG4gICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImN1cnJlbnRSZXN1bHROdW1iZXIgPiAwICYmIGN1cnJlbnRFbmFibGVQYWdpbmF0aW9uXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5saW5lLWJsb2NrIGlubGluZS1ibG9jay1ib3R0b21cIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhZ2luYXRpb24tY2FwdGlvblwiPlxyXG4gICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ucGFnaW5hdGlvbi50b3RhbF9lbGVtZW50cycgfCB0cmFuc2xhdGU6eyB0b3RhbEVsZW1lbnRzOiBjdXJyZW50UmVzdWx0TnVtYmVyLCB0b3RhbFBhZ2VzOiBjdXJyZW50UGFnZUNvdW50IH0gfX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxuYXYgYXJpYS1sYWJlbD1cInBhZ2luYXRpb25cIiBjbGFzcz1cInBhZ2luYXRpb24tbGlua3NcIj5cclxuICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJwYWdpbmF0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW0gY2xpY2thYmxlXCIgW2NsYXNzLmRpc2FibGVkXT1cIiEoY3VycmVudFBhZ2VJbmRleCA+IDApXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwicGFnZS1saW5rXCIgKGNsaWNrKT1cInN3aXRjaFRvUGFnZSgwKVwiPnt7ICd0YWJsZS5jb21tb24ucGFnaW5hdGlvbi5maXJzdF9wYWdlJyB8IHRyYW5zbGF0ZSB9fTwvYT5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW0gY2xpY2thYmxlXCIgW2NsYXNzLmRpc2FibGVkXT1cIiEoY3VycmVudFBhZ2VJbmRleCA+IDApXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwicGFnZS1saW5rXCIgKGNsaWNrKT1cInN3aXRjaFRvUGFnZShjdXJyZW50UGFnZUluZGV4IC0gMSlcIj57eyAndGFibGUuY29tbW9uLnBhZ2luYXRpb24ucHJldmlvdXNfcGFnZScgfCB0cmFuc2xhdGUgfX08L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgcGFnZSBvZiBlbnVtUGFnZXNcIj5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cInBhZ2UtaXRlbSBjbGlja2FibGVcIiBbY2xhc3MuYWN0aXZlXT1cInBhZ2UgPT09IGN1cnJlbnRQYWdlSW5kZXhcIiAqbmdJZj1cIiFwYWdlLnNraXBcIj5cclxuICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIiAoY2xpY2spPVwic3dpdGNoVG9QYWdlKHBhZ2UpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3sgcGFnZSArIDEgfX1cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInNyLW9ubHlcIiAqbmdJZj1cInBhZ2UgPT09IGN1cnJlbnRQYWdlSW5kZXhcIj4oY3VycmVudCk8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW1cIiAqbmdJZj1cInBhZ2Uuc2tpcFwiPlxyXG4gICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIC4uLlxyXG4gICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW0gY2xpY2thYmxlXCIgW2NsYXNzLmRpc2FibGVkXT1cImN1cnJlbnRQYWdlSW5kZXggPj0gKGN1cnJlbnRQYWdlQ291bnQgLSAxKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiIChjbGljayk9XCJzd2l0Y2hUb1BhZ2UoY3VycmVudFBhZ2VJbmRleCArIDEpXCI+e3sgJ3RhYmxlLmNvbW1vbi5wYWdpbmF0aW9uLm5leHRfcGFnZScgfCB0cmFuc2xhdGUgfX08L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtIGNsaWNrYWJsZVwiIFtjbGFzcy5kaXNhYmxlZF09XCJjdXJyZW50UGFnZUluZGV4ID49IChjdXJyZW50UGFnZUNvdW50IC0gMSlcIj5cclxuICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIiAoY2xpY2spPVwic3dpdGNoVG9QYWdlKGN1cnJlbnRQYWdlQ291bnQgLSAxKVwiPnt7ICd0YWJsZS5jb21tb24ucGFnaW5hdGlvbi5sYXN0X3BhZ2UnIHwgdHJhbnNsYXRlIH19PC9hPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICA8L25hdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZS1ibG9jayBpbmxpbmUtYmxvY2stYm90dG9tIHBsLTJcIj5cclxuICAgICAgICAgICAgPHNwYW4gbmdiRHJvcGRvd24gKm5nSWY9XCJwYWdlU2l6ZVNlbGVjdEVuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAmbmJzcDtcclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1ibGFjayBkcm9wZG93bi10b2dnbGVcIiBuZ2JEcm9wZG93blRvZ2dsZT57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBhZ2Vfc2l6ZV9idXR0b24nIHwgdHJhbnNsYXRlOntjdXJyZW50OiBjdXJyZW50UGFnZVNpemV9IH19PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPHNwYW4gbmdiRHJvcGRvd25NZW51PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gKm5nRm9yPVwibGV0IHBhZ2VTaXplIG9mIGN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplc1wiIChjbGljayk9XCJjbGlja09uUGFnZVNpemUocGFnZVNpemUpXCIgW2Rpc2FibGVkXT1cInBhZ2VTaXplID09PSBjdXJyZW50UGFnZVNpemVcIj5cclxuICAgICAgICAgICAgICAgICAge3sgcGFnZVNpemUgfX1cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTUgdGV4dC1yaWdodFwiPlxyXG4gICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cImFjdGlvbnNDYXB0aW9uVGVtcGxhdGVcIiBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie31cIj4gPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0lmPVwiaGVhZGVyQWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4te3sgYWN0aW9uLmRpc3BsYXlDbGFzcyB8fCAnbGlnaHQnIH19IHt7IGFjdGlvbi5hZGRpdGlvbmFsQ2xhc3NlcyB8fCAnJyB9fSBtci0xXCIgKm5nRm9yPVwibGV0IGFjdGlvbiBvZiBoZWFkZXJBY3Rpb25zXCJcclxuICAgICAgICAgICAgKGNsaWNrKT1cImNsaWNrT25Db250ZXh0QWN0aW9uKGFjdGlvbilcIiBbZGlzYWJsZWRdPVwiIWlzQ29udGV4dEFjdGlvbkFsbG93ZWQoYWN0aW9uKVwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChhY3Rpb24pIH19XHJcbiAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJidG4tZ3JvdXBcIiBuZ2JEcm9wZG93biAqbmdJZj1cImNvbnRleHRBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlXCI+XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWxpZ2h0IGRyb3Bkb3duLXRvZ2dsZVwiIFtkaXNhYmxlZF09XCIhYW55QnV0dG9uQWN0aW9uc0FsbG93ZWRcIiBuZ2JEcm9wZG93blRvZ2dsZT57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmFjdGlvbnNfYnV0dG9uJyB8IHRyYW5zbGF0ZSB9fTwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudT5cclxuICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duSXRlbT5cclxuICAgICAgICAgICAgICAgIDxoNiBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiPnt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMucGlja19hY3Rpb24nIHwgdHJhbnNsYXRlIH19PC9oNj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSAqbmdGb3I9XCJsZXQgYWN0aW9uIG9mIGN1cnJlbnRBY3Rpb25zXCIgKGNsaWNrKT1cImNsaWNrT25Db250ZXh0QWN0aW9uKGFjdGlvbilcIiBbZGlzYWJsZWRdPVwiIWlzQ29udGV4dEFjdGlvbkFsbG93ZWQoYWN0aW9uKVwiPlxyXG4gICAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGFjdGlvbikgfX1cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG5cclxuPC9kaXY+XHJcbiJdfQ==