import { Injectable } from '@angular/core';
import { MaisTableEmbeddedLogger } from '../utils/mais-table-embedded-logger';
import * as i0 from "@angular/core";
export class MaisTableLoggerService {
    constructor() {
    }
    static getConfiguredLogAdapter() {
        return MaisTableLoggerService.logAdapter || MaisTableLoggerService.embeddedLogger;
    }
    withLogAdapter(logAdapter) {
        MaisTableLoggerService.logAdapter = logAdapter;
    }
}
MaisTableLoggerService.embeddedLogger = new MaisTableEmbeddedLogger();
MaisTableLoggerService.logAdapter = null;
/** @nocollapse */ MaisTableLoggerService.ɵfac = function MaisTableLoggerService_Factory(t) { return new (t || MaisTableLoggerService)(); };
/** @nocollapse */ MaisTableLoggerService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableLoggerService, factory: MaisTableLoggerService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableLoggerService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1sb2dnZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYWlzLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL21haXMtdGFibGUtbG9nZ2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQzs7QUFHOUUsTUFBTSxPQUFPLHNCQUFzQjtJQUtqQztJQUNBLENBQUM7SUFFTSxNQUFNLENBQUMsdUJBQXVCO1FBQ25DLE9BQU8sc0JBQXNCLENBQUMsVUFBVSxJQUFJLHNCQUFzQixDQUFDLGNBQWMsQ0FBQztJQUNwRixDQUFDO0lBRU0sY0FBYyxDQUFDLFVBQXVDO1FBQzNELHNCQUFzQixDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDakQsQ0FBQzs7QUFaYyxxQ0FBYyxHQUF5QixJQUFJLHVCQUF1QixFQUFFLENBQUM7QUFDckUsaUNBQVUsR0FBZ0MsSUFBSSxDQUFDOzRGQUhuRCxzQkFBc0I7OERBQXRCLHNCQUFzQixXQUF0QixzQkFBc0IsbUJBRFYsTUFBTTtrREFDbEIsc0JBQXNCO2NBRGxDLFVBQVU7ZUFBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IElNYWlzVGFibGVMb2dBZGFwdGVyIH0gZnJvbSAnLi4vdXRpbHMvbWFpcy10YWJsZS1sb2ctYWRhcHRlcic7XHJcbmltcG9ydCB7IE1haXNUYWJsZUVtYmVkZGVkTG9nZ2VyIH0gZnJvbSAnLi4vdXRpbHMvbWFpcy10YWJsZS1lbWJlZGRlZC1sb2dnZXInO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlIHtcclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgZW1iZWRkZWRMb2dnZXI6IElNYWlzVGFibGVMb2dBZGFwdGVyID0gbmV3IE1haXNUYWJsZUVtYmVkZGVkTG9nZ2VyKCk7XHJcbiAgcHJpdmF0ZSBzdGF0aWMgbG9nQWRhcHRlcjogSU1haXNUYWJsZUxvZ0FkYXB0ZXIgfCBudWxsID0gbnVsbDtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGdldENvbmZpZ3VyZWRMb2dBZGFwdGVyKCk6IElNYWlzVGFibGVMb2dBZGFwdGVyIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlLmxvZ0FkYXB0ZXIgfHwgTWFpc1RhYmxlTG9nZ2VyU2VydmljZS5lbWJlZGRlZExvZ2dlcjtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB3aXRoTG9nQWRhcHRlcihsb2dBZGFwdGVyOiBJTWFpc1RhYmxlTG9nQWRhcHRlciB8IG51bGwpIHtcclxuICAgIE1haXNUYWJsZUxvZ2dlclNlcnZpY2UubG9nQWRhcHRlciA9IGxvZ0FkYXB0ZXI7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=