import { Injectable } from '@angular/core';
import { MaisTableLogger } from '../utils';
import * as i0 from "@angular/core";
export class MaisTableRegistryService {
    constructor() {
        this.registry = {};
        this.logger = new MaisTableLogger('MaisTableRegistryService');
        this.logger.trace('building service');
    }
    register(key, component) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!component) {
            this.logger.warn('Invalid component provided to registry', component);
            throw Error('Invalid component provided to registry');
        }
        if (!this.registry[key]) {
            this.registry[key] = [];
        }
        const uuid = 'MTREG-' + (++MaisTableRegistryService.counter) + '-' + Math.round(Math.random() * 100000);
        const item = {
            key,
            component,
            registrationId: uuid
        };
        this.registry[key].push(item);
        this.logger.debug('registered table', item);
        return uuid;
    }
    unregister(key, uuid) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!uuid) {
            this.logger.warn('Invalid uuid provided to registry', uuid);
            throw Error('Invalid uuid provided to registry');
        }
        this.logger.debug('unregistered table', uuid);
        this.registry[key] = this.registry[key].filter((o) => o.registrationId !== uuid);
    }
    get(key) {
        return this.registry[key];
    }
    getSingle(key) {
        const arr = this.registry[key];
        if (!arr || !arr.length) {
            return null;
        }
        else if (arr.length > 1) {
            this.logger.error('MULTIPLE REFERENCES FOR KEY', key);
            return null;
        }
        else {
            return arr[0].component;
        }
    }
}
MaisTableRegistryService.counter = 0;
/** @nocollapse */ MaisTableRegistryService.ɵfac = function MaisTableRegistryService_Factory(t) { return new (t || MaisTableRegistryService)(); };
/** @nocollapse */ MaisTableRegistryService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableRegistryService, factory: MaisTableRegistryService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableRegistryService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1yZWdpc3RyeS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1haXMtdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpcy10YWJsZS1yZWdpc3RyeS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFVBQVUsQ0FBQzs7QUFJM0MsTUFBTSxPQUFPLHdCQUF3QjtJQVFuQztRQUZRLGFBQVEsR0FBUSxFQUFFLENBQUM7UUFHekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGVBQWUsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVNLFFBQVEsQ0FBQyxHQUFXLEVBQUUsU0FBNkI7UUFDeEQsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNSLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzFELE1BQU0sS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7U0FDakQ7UUFDRCxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsd0NBQXdDLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDdEUsTUFBTSxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztTQUN2RDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3pCO1FBRUQsTUFBTSxJQUFJLEdBQVcsUUFBUSxHQUFHLENBQUMsRUFBRSx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFDaEgsTUFBTSxJQUFJLEdBQUc7WUFDWCxHQUFHO1lBQ0gsU0FBUztZQUNULGNBQWMsRUFBRSxJQUFJO1NBQ3JCLENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU5QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1QyxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxVQUFVLENBQUMsR0FBVyxFQUFFLElBQVk7UUFDekMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNSLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzFELE1BQU0sS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7U0FDakQ7UUFDRCxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUNBQW1DLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDNUQsTUFBTSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQztTQUNsRDtRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUUsQ0FBQyxDQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVNLEdBQUcsQ0FBQyxHQUFXO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sU0FBUyxDQUFDLEdBQVc7UUFDMUIsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUN2QixPQUFPLElBQUksQ0FBQztTQUNiO2FBQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN0RCxPQUFPLElBQUksQ0FBQztTQUNiO2FBQU07WUFDTCxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7U0FDekI7SUFDSCxDQUFDOztBQWxFYyxnQ0FBTyxHQUFHLENBQUMsQ0FBQztnR0FGaEIsd0JBQXdCO2dFQUF4Qix3QkFBd0IsV0FBeEIsd0JBQXdCLG1CQURaLE1BQU07a0RBQ2xCLHdCQUF3QjtjQURwQyxVQUFVO2VBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVMb2dnZXIgfSBmcm9tICcuLi91dGlscyc7XHJcbmltcG9ydCB7IE1haXNUYWJsZUNvbXBvbmVudCB9IGZyb20gJy4uL21haXMtdGFibGUuY29tcG9uZW50JztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlUmVnaXN0cnlTZXJ2aWNlIHtcclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgY291bnRlciA9IDA7XHJcblxyXG4gIHByaXZhdGUgbG9nZ2VyOiBNYWlzVGFibGVMb2dnZXI7XHJcblxyXG4gIHByaXZhdGUgcmVnaXN0cnk6IGFueSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMubG9nZ2VyID0gbmV3IE1haXNUYWJsZUxvZ2dlcignTWFpc1RhYmxlUmVnaXN0cnlTZXJ2aWNlJyk7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnYnVpbGRpbmcgc2VydmljZScpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHJlZ2lzdGVyKGtleTogc3RyaW5nLCBjb21wb25lbnQ6IE1haXNUYWJsZUNvbXBvbmVudCk6IHN0cmluZyB7XHJcbiAgICBpZiAoIWtleSkge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdJbnZhbGlkIGtleSBwcm92aWRlZCB0byByZWdpc3RyeScsIGtleSk7XHJcbiAgICAgIHRocm93IEVycm9yKCdJbnZhbGlkIGtleSBwcm92aWRlZCB0byByZWdpc3RyeScpO1xyXG4gICAgfVxyXG4gICAgaWYgKCFjb21wb25lbnQpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignSW52YWxpZCBjb21wb25lbnQgcHJvdmlkZWQgdG8gcmVnaXN0cnknLCBjb21wb25lbnQpO1xyXG4gICAgICB0aHJvdyBFcnJvcignSW52YWxpZCBjb21wb25lbnQgcHJvdmlkZWQgdG8gcmVnaXN0cnknKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXRoaXMucmVnaXN0cnlba2V5XSkge1xyXG4gICAgICB0aGlzLnJlZ2lzdHJ5W2tleV0gPSBbXTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCB1dWlkOiBzdHJpbmcgPSAnTVRSRUctJyArICgrK01haXNUYWJsZVJlZ2lzdHJ5U2VydmljZS5jb3VudGVyKSArICctJyArIE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDEwMDAwMCk7XHJcbiAgICBjb25zdCBpdGVtID0ge1xyXG4gICAgICBrZXksXHJcbiAgICAgIGNvbXBvbmVudCxcclxuICAgICAgcmVnaXN0cmF0aW9uSWQ6IHV1aWRcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yZWdpc3RyeVtrZXldLnB1c2goaXRlbSk7XHJcblxyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ3JlZ2lzdGVyZWQgdGFibGUnLCBpdGVtKTtcclxuICAgIHJldHVybiB1dWlkO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHVucmVnaXN0ZXIoa2V5OiBzdHJpbmcsIHV1aWQ6IHN0cmluZykge1xyXG4gICAgaWYgKCFrZXkpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignSW52YWxpZCBrZXkgcHJvdmlkZWQgdG8gcmVnaXN0cnknLCBrZXkpO1xyXG4gICAgICB0aHJvdyBFcnJvcignSW52YWxpZCBrZXkgcHJvdmlkZWQgdG8gcmVnaXN0cnknKTtcclxuICAgIH1cclxuICAgIGlmICghdXVpZCkge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdJbnZhbGlkIHV1aWQgcHJvdmlkZWQgdG8gcmVnaXN0cnknLCB1dWlkKTtcclxuICAgICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgdXVpZCBwcm92aWRlZCB0byByZWdpc3RyeScpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCd1bnJlZ2lzdGVyZWQgdGFibGUnLCB1dWlkKTtcclxuICAgIHRoaXMucmVnaXN0cnlba2V5XSA9IHRoaXMucmVnaXN0cnlba2V5XS5maWx0ZXIoIChvOiBhbnkpID0+IG8ucmVnaXN0cmF0aW9uSWQgIT09IHV1aWQpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldChrZXk6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMucmVnaXN0cnlba2V5XTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTaW5nbGUoa2V5OiBzdHJpbmcpOiBNYWlzVGFibGVDb21wb25lbnQgfCBudWxsIHtcclxuICAgIGNvbnN0IGFyciA9IHRoaXMucmVnaXN0cnlba2V5XTtcclxuICAgIGlmICghYXJyIHx8ICFhcnIubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfSBlbHNlIGlmIChhcnIubGVuZ3RoID4gMSkge1xyXG4gICAgICB0aGlzLmxvZ2dlci5lcnJvcignTVVMVElQTEUgUkVGRVJFTkNFUyBGT1IgS0VZJywga2V5KTtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gYXJyWzBdLmNvbXBvbmVudDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19