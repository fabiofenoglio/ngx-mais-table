import { Injectable } from '@angular/core';
import { MaisTableLogger, MaisTableBrowserHelper } from '../utils';
import { MaisTableActionActivationCondition, MaisTablePaginationMethod, MaisTableRefreshStrategy } from '../model';
import { MaisTableLoggerService } from './mais-table-logger.service';
import * as i0 from "@angular/core";
import * as i1 from "./mais-table-logger.service";
export class MaisTableService {
    constructor(maisTableLoggerService) {
        this.maisTableLoggerService = maisTableLoggerService;
        this.logger = new MaisTableLogger('MaisTableService');
        this.logger.trace('building service');
        this.configuration = this.buildDefaultConfiguration();
        this.logger.debug('initializing MAIS table component for browser', MaisTableBrowserHelper.getBrowser());
        this.maisTableLoggerService.withLogAdapter(null);
    }
    configure(config) {
        Object.assign(this.configuration, config);
        return this;
    }
    withLogAdapter(logAdapter) {
        this.maisTableLoggerService.withLogAdapter(logAdapter);
        return this;
    }
    getConfiguration() {
        return this.configuration;
    }
    buildDefaultConfiguration() {
        return {
            currentSchemaVersion: 'DEFAULT',
            refresh: {
                defaultStrategy: MaisTableRefreshStrategy.NONE,
                defaultInterval: 60000,
                defaultOnPushInBackground: true,
                defaultOnTickInBackground: true
            },
            rowSelection: {
                enabledByDefault: false,
                selectAllEnabledByDefault: true,
                multipleSelectionEnabledByDefault: true
            },
            columnToggling: {
                enabledByDefault: true,
                showFixedColumns: true
            },
            filtering: {
                enabledByDefault: true,
                autoReloadOnQueryClear: true,
                autoReloadOnQueryChange: true,
                autoReloadTimeout: 300,
                showUnfilterableColumns: true
            },
            contextFiltering: {
                enabledByDefault: true
            },
            pagination: {
                enabledByDefault: true,
                pageSizeSelectionEnabledByDefault: true,
                defaultPaginationMode: MaisTablePaginationMethod.CLIENT,
                defaultPageSize: 5,
                defaultPossiblePageSizes: [5, 10, 25, 50]
            },
            actions: {
                contextActionsEnabledByDefault: true,
                headerActionsEnabledByDefault: true,
                defaultHeaderActionActivationCondition: MaisTableActionActivationCondition.ALWAYS,
                defaultContextActionActivationCondition: MaisTableActionActivationCondition.MULTIPLE_SELECTION
            },
            columns: {
                defaultShowLabelInTable: true,
                defaultCanSort: false,
                defaultCanHide: true,
                defaultCanFilter: true,
                defaultIsDefaultFilter: true,
                defaultIsDefaulView: true
            },
            rowExpansion: {
                enabledByDefault: false,
                multipleExpansionEnabledByDefault: false
            },
            dragAndDrop: {
                dragEnabledByDefault: false,
                dropEnabledByDefault: false
            },
            itemTracking: {
                enabledByDefault: false
            },
            storePersistence: {
                enabledByDefault: true
            }
        };
    }
}
/** @nocollapse */ MaisTableService.ɵfac = function MaisTableService_Factory(t) { return new (t || MaisTableService)(i0.ɵɵinject(i1.MaisTableLoggerService)); };
/** @nocollapse */ MaisTableService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableService, factory: MaisTableService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.MaisTableLoggerService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1haXMtdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpcy10YWJsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxzQkFBc0IsRUFBd0IsTUFBTSxVQUFVLENBQUM7QUFDekYsT0FBTyxFQUNMLGtDQUFrQyxFQUNsQyx5QkFBeUIsRUFDekIsd0JBQXdCLEVBRXpCLE1BQU0sVUFBVSxDQUFDO0FBQ2xCLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7QUFHckUsTUFBTSxPQUFPLGdCQUFnQjtJQUszQixZQUFvQixzQkFBOEM7UUFBOUMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUNoRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksZUFBZSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1FBRXRELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLCtDQUErQyxFQUFFLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sU0FBUyxDQUFDLE1BQStCO1FBQzlDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMxQyxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxjQUFjLENBQUMsVUFBZ0M7UUFDcEQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN2RCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxnQkFBZ0I7UUFDckIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7SUFFTyx5QkFBeUI7UUFDL0IsT0FBTztZQUNMLG9CQUFvQixFQUFFLFNBQVM7WUFDL0IsT0FBTyxFQUFFO2dCQUNQLGVBQWUsRUFBRSx3QkFBd0IsQ0FBQyxJQUFJO2dCQUM5QyxlQUFlLEVBQUUsS0FBSztnQkFDdEIseUJBQXlCLEVBQUUsSUFBSTtnQkFDL0IseUJBQXlCLEVBQUUsSUFBSTthQUNoQztZQUNELFlBQVksRUFBRTtnQkFDWixnQkFBZ0IsRUFBRSxLQUFLO2dCQUN2Qix5QkFBeUIsRUFBRSxJQUFJO2dCQUMvQixpQ0FBaUMsRUFBRSxJQUFJO2FBQ3hDO1lBQ0QsY0FBYyxFQUFFO2dCQUNkLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGdCQUFnQixFQUFFLElBQUk7YUFDdkI7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsc0JBQXNCLEVBQUUsSUFBSTtnQkFDNUIsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsaUJBQWlCLEVBQUUsR0FBRztnQkFDdEIsdUJBQXVCLEVBQUUsSUFBSTthQUM5QjtZQUNELGdCQUFnQixFQUFFO2dCQUNoQixnQkFBZ0IsRUFBRSxJQUFJO2FBQ3ZCO1lBQ0QsVUFBVSxFQUFFO2dCQUNWLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGlDQUFpQyxFQUFFLElBQUk7Z0JBQ3ZDLHFCQUFxQixFQUFFLHlCQUF5QixDQUFDLE1BQU07Z0JBQ3ZELGVBQWUsRUFBRSxDQUFDO2dCQUNsQix3QkFBd0IsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQzthQUMxQztZQUNELE9BQU8sRUFBRTtnQkFDUCw4QkFBOEIsRUFBRSxJQUFJO2dCQUNwQyw2QkFBNkIsRUFBRSxJQUFJO2dCQUNuQyxzQ0FBc0MsRUFBRSxrQ0FBa0MsQ0FBQyxNQUFNO2dCQUNqRix1Q0FBdUMsRUFBRSxrQ0FBa0MsQ0FBQyxrQkFBa0I7YUFDL0Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsY0FBYyxFQUFFLEtBQUs7Z0JBQ3JCLGNBQWMsRUFBRSxJQUFJO2dCQUNwQixnQkFBZ0IsRUFBRSxJQUFJO2dCQUN0QixzQkFBc0IsRUFBRSxJQUFJO2dCQUM1QixtQkFBbUIsRUFBRSxJQUFJO2FBQzFCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLGdCQUFnQixFQUFFLEtBQUs7Z0JBQ3ZCLGlDQUFpQyxFQUFFLEtBQUs7YUFDekM7WUFDRCxXQUFXLEVBQUU7Z0JBQ1gsb0JBQW9CLEVBQUUsS0FBSztnQkFDM0Isb0JBQW9CLEVBQUUsS0FBSzthQUM1QjtZQUNELFlBQVksRUFBRTtnQkFDWixnQkFBZ0IsRUFBRSxLQUFLO2FBQ3hCO1lBQ0QsZ0JBQWdCLEVBQUU7Z0JBQ2hCLGdCQUFnQixFQUFFLElBQUk7YUFDdkI7U0FDRixDQUFDO0lBQ0osQ0FBQzs7Z0ZBNUZVLGdCQUFnQjt3REFBaEIsZ0JBQWdCLFdBQWhCLGdCQUFnQixtQkFESixNQUFNO2tEQUNsQixnQkFBZ0I7Y0FENUIsVUFBVTtlQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWFpc1RhYmxlTG9nZ2VyLCBNYWlzVGFibGVCcm93c2VySGVscGVyLCBJTWFpc1RhYmxlTG9nQWRhcHRlciB9IGZyb20gJy4uL3V0aWxzJztcclxuaW1wb3J0IHtcclxuICBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLFxyXG4gIE1haXNUYWJsZVBhZ2luYXRpb25NZXRob2QsXHJcbiAgTWFpc1RhYmxlUmVmcmVzaFN0cmF0ZWd5LFxyXG4gIElNYWlzVGFibGVDb25maWd1cmF0aW9uXHJcbn0gZnJvbSAnLi4vbW9kZWwnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlIH0gZnJvbSAnLi9tYWlzLXRhYmxlLWxvZ2dlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlU2VydmljZSB7XHJcblxyXG4gIHByaXZhdGUgbG9nZ2VyOiBNYWlzVGFibGVMb2dnZXI7XHJcbiAgcHJpdmF0ZSBjb25maWd1cmF0aW9uOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtYWlzVGFibGVMb2dnZXJTZXJ2aWNlOiBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLmxvZ2dlciA9IG5ldyBNYWlzVGFibGVMb2dnZXIoJ01haXNUYWJsZVNlcnZpY2UnKTtcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdidWlsZGluZyBzZXJ2aWNlJyk7XHJcbiAgICB0aGlzLmNvbmZpZ3VyYXRpb24gPSB0aGlzLmJ1aWxkRGVmYXVsdENvbmZpZ3VyYXRpb24oKTtcclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnaW5pdGlhbGl6aW5nIE1BSVMgdGFibGUgY29tcG9uZW50IGZvciBicm93c2VyJywgTWFpc1RhYmxlQnJvd3NlckhlbHBlci5nZXRCcm93c2VyKCkpO1xyXG4gICAgdGhpcy5tYWlzVGFibGVMb2dnZXJTZXJ2aWNlLndpdGhMb2dBZGFwdGVyKG51bGwpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNvbmZpZ3VyZShjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKTogTWFpc1RhYmxlU2VydmljZSB7XHJcbiAgICBPYmplY3QuYXNzaWduKHRoaXMuY29uZmlndXJhdGlvbiwgY29uZmlnKTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHdpdGhMb2dBZGFwdGVyKGxvZ0FkYXB0ZXI6IElNYWlzVGFibGVMb2dBZGFwdGVyKTogTWFpc1RhYmxlU2VydmljZSB7XHJcbiAgICB0aGlzLm1haXNUYWJsZUxvZ2dlclNlcnZpY2Uud2l0aExvZ0FkYXB0ZXIobG9nQWRhcHRlcik7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb25maWd1cmF0aW9uKCk6IElNYWlzVGFibGVDb25maWd1cmF0aW9uIHtcclxuICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1aWxkRGVmYXVsdENvbmZpZ3VyYXRpb24oKTogSU1haXNUYWJsZUNvbmZpZ3VyYXRpb24ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgY3VycmVudFNjaGVtYVZlcnNpb246ICdERUZBVUxUJyxcclxuICAgICAgcmVmcmVzaDoge1xyXG4gICAgICAgIGRlZmF1bHRTdHJhdGVneTogTWFpc1RhYmxlUmVmcmVzaFN0cmF0ZWd5Lk5PTkUsXHJcbiAgICAgICAgZGVmYXVsdEludGVydmFsOiA2MDAwMCxcclxuICAgICAgICBkZWZhdWx0T25QdXNoSW5CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRPblRpY2tJbkJhY2tncm91bmQ6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgcm93U2VsZWN0aW9uOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogZmFsc2UsXHJcbiAgICAgICAgc2VsZWN0QWxsRW5hYmxlZEJ5RGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBtdWx0aXBsZVNlbGVjdGlvbkVuYWJsZWRCeURlZmF1bHQ6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgY29sdW1uVG9nZ2xpbmc6IHtcclxuICAgICAgICBlbmFibGVkQnlEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIHNob3dGaXhlZENvbHVtbnM6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgZmlsdGVyaW5nOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBhdXRvUmVsb2FkT25RdWVyeUNsZWFyOiB0cnVlLFxyXG4gICAgICAgIGF1dG9SZWxvYWRPblF1ZXJ5Q2hhbmdlOiB0cnVlLFxyXG4gICAgICAgIGF1dG9SZWxvYWRUaW1lb3V0OiAzMDAsXHJcbiAgICAgICAgc2hvd1VuZmlsdGVyYWJsZUNvbHVtbnM6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgY29udGV4dEZpbHRlcmluZzoge1xyXG4gICAgICAgIGVuYWJsZWRCeURlZmF1bHQ6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICAgIGVuYWJsZWRCeURlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgcGFnZVNpemVTZWxlY3Rpb25FbmFibGVkQnlEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRQYWdpbmF0aW9uTW9kZTogTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZC5DTElFTlQsXHJcbiAgICAgICAgZGVmYXVsdFBhZ2VTaXplOiA1LFxyXG4gICAgICAgIGRlZmF1bHRQb3NzaWJsZVBhZ2VTaXplczogWzUsIDEwLCAyNSwgNTBdXHJcbiAgICAgIH0sXHJcbiAgICAgIGFjdGlvbnM6IHtcclxuICAgICAgICBjb250ZXh0QWN0aW9uc0VuYWJsZWRCeURlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgaGVhZGVyQWN0aW9uc0VuYWJsZWRCeURlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgZGVmYXVsdEhlYWRlckFjdGlvbkFjdGl2YXRpb25Db25kaXRpb246IE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24uQUxXQVlTLFxyXG4gICAgICAgIGRlZmF1bHRDb250ZXh0QWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbjogTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5NVUxUSVBMRV9TRUxFQ1RJT05cclxuICAgICAgfSxcclxuICAgICAgY29sdW1uczoge1xyXG4gICAgICAgIGRlZmF1bHRTaG93TGFiZWxJblRhYmxlOiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRDYW5Tb3J0OiBmYWxzZSxcclxuICAgICAgICBkZWZhdWx0Q2FuSGlkZTogdHJ1ZSxcclxuICAgICAgICBkZWZhdWx0Q2FuRmlsdGVyOiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRJc0RlZmF1bHRGaWx0ZXI6IHRydWUsXHJcbiAgICAgICAgZGVmYXVsdElzRGVmYXVsVmlldzogdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgICByb3dFeHBhbnNpb246IHtcclxuICAgICAgICBlbmFibGVkQnlEZWZhdWx0OiBmYWxzZSxcclxuICAgICAgICBtdWx0aXBsZUV4cGFuc2lvbkVuYWJsZWRCeURlZmF1bHQ6IGZhbHNlXHJcbiAgICAgIH0sXHJcbiAgICAgIGRyYWdBbmREcm9wOiB7XHJcbiAgICAgICAgZHJhZ0VuYWJsZWRCeURlZmF1bHQ6IGZhbHNlLFxyXG4gICAgICAgIGRyb3BFbmFibGVkQnlEZWZhdWx0OiBmYWxzZVxyXG4gICAgICB9LFxyXG4gICAgICBpdGVtVHJhY2tpbmc6IHtcclxuICAgICAgICBlbmFibGVkQnlEZWZhdWx0OiBmYWxzZVxyXG4gICAgICB9LFxyXG4gICAgICBzdG9yZVBlcnNpc3RlbmNlOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogdHJ1ZVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=