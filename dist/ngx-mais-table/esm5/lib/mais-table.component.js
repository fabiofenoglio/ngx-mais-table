import { __assign, __values } from "tslib";
import { Component, Input, Output, EventEmitter, ContentChild, TemplateRef, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { MaisTableSortDirection, MaisTablePaginationMethod, MaisTableActionActivationCondition, MaisTableRefreshStrategy, MaisTableReloadReason } from './model';
import { MaisTableFormatterHelper, MaisTableInMemoryHelper, MaisTableValidatorHelper, MaisTableBrowserHelper, MaisTableLogger } from './utils';
import { MaisTableRegistryService, MaisTableService } from './services';
import { Observable, Subject, throwError, timer } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import $ from 'jquery';
import { MatTable } from '@angular/material/table';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "./services";
import * as i3 from "@angular/common";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "@fortawesome/angular-fontawesome";
import * as i6 from "@angular/cdk/drag-drop";
import * as i7 from "@angular/material/table";
var _c0 = ["cellTemplate"];
var _c1 = ["actionsCaptionTemplate"];
var _c2 = ["rowDetailTemplate"];
var _c3 = ["dragTemplate"];
var _c4 = ["dropTemplate"];
var _c5 = ["#renderedMatTable"];
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    var _r283 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template_input_change_0_listener($event) { i0.ɵɵrestoreView(_r283); var column_r277 = i0.ɵɵnextContext(2).$implicit; var ctx_r281 = i0.ɵɵnextContext(4); ctx_r281.toggleFilteringColumnChecked(column_r277); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r277 = i0.ɵɵnextContext(2).$implicit;
    var ctx_r279 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("checked", ctx_r279.isColumnCheckedForFiltering(column_r277));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 31);
} if (rf & 2) {
    i0.ɵɵproperty("checked", false)("disabled", true);
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r287 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 20);
    i0.ɵɵelementStart(1, "div", 22);
    i0.ɵɵelementStart(2, "div", 23);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template, 1, 1, "input", 29);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template, 1, 2, "input", 30);
    i0.ɵɵelementStart(5, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template_span_click_5_listener($event) { i0.ɵɵrestoreView(_r287); var column_r277 = i0.ɵɵnextContext().$implicit; var ctx_r285 = i0.ɵɵnextContext(4); ctx_r285.toggleFilteringColumnChecked(column_r277); return $event.stopPropagation(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r277 = i0.ɵɵnextContext().$implicit;
    var ctx_r278 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r278.isFilterable(column_r277));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r278.isFilterable(column_r277));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r278.isFilterable(column_r277));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r278.resolveLabel(column_r277), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template, 7, 5, "button", 28);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r277 = ctx.$implicit;
    var ctx_r276 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r276.hasLabel(column_r277) && (ctx_r276.isFilterable(column_r277) || ctx_r276.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    var _r290 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 15);
    i0.ɵɵelementStart(2, "button", 16);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_button_click_2_listener() { i0.ɵɵrestoreView(_r290); var ctx_r289 = i0.ɵɵnextContext(3); return ctx_r289.applySelectedFilter(); });
    i0.ɵɵelement(3, "i", 17);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "button", 18);
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "div", 20);
    i0.ɵɵelementStart(7, "h6", 21);
    i0.ɵɵtext(8);
    i0.ɵɵpipe(9, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "button", 20);
    i0.ɵɵelementStart(11, "div", 22);
    i0.ɵɵelementStart(12, "div", 23);
    i0.ɵɵelementStart(13, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_Template_input_change_13_listener($event) { i0.ɵɵrestoreView(_r290); var ctx_r291 = i0.ɵɵnextContext(3); ctx_r291.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(14, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_span_click_14_listener($event) { i0.ɵɵrestoreView(_r290); var ctx_r292 = i0.ɵɵnextContext(3); ctx_r292.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵtext(15);
    i0.ɵɵpipe(16, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(17, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(18, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r274 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("btn-black", !ctx_r274.searchQueryNeedApply || !ctx_r274.searchQueryActive)("btn-primary", ctx_r274.searchQueryNeedApply && ctx_r274.searchQueryActive)("border-primary", ctx_r274.searchQueryActive && !ctx_r274.searchQueryMalformed)("border-warning", ctx_r274.searchQueryMalformed);
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("border-primary", ctx_r274.searchQueryActive && !ctx_r274.searchQueryMalformed)("border-warning", ctx_r274.searchQueryMalformed)("btn-black", !(ctx_r274.selectedSearchQuery && ctx_r274.noFilteringColumnshecked))("btn-warning", ctx_r274.selectedSearchQuery && ctx_r274.noFilteringColumnshecked);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(9, 21, "table.common.messages.pick_filtering_columns"));
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("checked", ctx_r274.allFilteringColumnsChecked);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(16, 23, "table.common.messages.pick_all_filtering_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r274.columns);
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r303 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r303); var column_r296 = i0.ɵɵnextContext().$implicit; var ctx_r301 = i0.ɵɵnextContext(4); return ctx_r301.toggleFilteringColumnChecked(column_r296); });
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r296 = i0.ɵɵnextContext().$implicit;
    var ctx_r297 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r297.isFilterable(column_r296));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r297.isFilterable(column_r296) && ctx_r297.isColumnCheckedForFiltering(column_r296));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r297.isFilterable(column_r296) && !ctx_r297.isColumnCheckedForFiltering(column_r296));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r297.isFilterable(column_r296));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r297.resolveLabel(column_r296), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template, 5, 6, "button", 35);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r296 = ctx.$implicit;
    var ctx_r295 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r295.hasLabel(column_r296) && (ctx_r295.isFilterable(column_r296) || ctx_r295.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_9_Template(rf, ctx) { if (rf & 1) {
    var _r306 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 15);
    i0.ɵɵelementStart(2, "button", 16);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_2_listener() { i0.ɵɵrestoreView(_r306); var ctx_r305 = i0.ɵɵnextContext(3); return ctx_r305.applySelectedFilter(); });
    i0.ɵɵelement(3, "i", 17);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "button", 18);
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "div", 20);
    i0.ɵɵelementStart(7, "h6", 21);
    i0.ɵɵtext(8);
    i0.ɵɵpipe(9, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_10_listener() { i0.ɵɵrestoreView(_r306); var ctx_r307 = i0.ɵɵnextContext(3); return ctx_r307.toggleAllFilteringColumnsChecked(); });
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(12, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(13);
    i0.ɵɵpipe(14, "translate");
    i0.ɵɵelement(15, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(16, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r275 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("btn-black", !ctx_r275.searchQueryNeedApply || !ctx_r275.searchQueryActive)("btn-primary", ctx_r275.searchQueryNeedApply && ctx_r275.searchQueryActive)("border-primary", ctx_r275.searchQueryActive && !ctx_r275.searchQueryMalformed)("border-warning", ctx_r275.searchQueryMalformed);
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("border-primary", ctx_r275.searchQueryActive && !ctx_r275.searchQueryMalformed)("border-warning", ctx_r275.searchQueryMalformed)("btn-black", !(ctx_r275.selectedSearchQuery && ctx_r275.noFilteringColumnshecked))("btn-warning", ctx_r275.selectedSearchQuery && ctx_r275.noFilteringColumnshecked);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(9, 22, "table.common.messages.pick_filtering_columns"));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r275.allFilteringColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r275.allFilteringColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(14, 24, "table.common.messages.pick_all_filtering_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r275.columns);
} }
function MaisTableComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    var _r309 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "form", 12);
    i0.ɵɵlistener("submit", function MaisTableComponent_div_1_div_2_Template_form_submit_1_listener() { i0.ɵɵrestoreView(_r309); var ctx_r308 = i0.ɵɵnextContext(2); return ctx_r308.applySelectedFilter(); });
    i0.ɵɵelementStart(2, "div");
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 13);
    i0.ɵɵelementStart(6, "input", 14);
    i0.ɵɵlistener("ngModelChange", function MaisTableComponent_div_1_div_2_Template_input_ngModelChange_6_listener($event) { i0.ɵɵrestoreView(_r309); var ctx_r310 = i0.ɵɵnextContext(2); return ctx_r310.selectedSearchQuery = $event; })("focusout", function MaisTableComponent_div_1_div_2_Template_input_focusout_6_listener() { i0.ɵɵrestoreView(_r309); var ctx_r311 = i0.ɵɵnextContext(2); return ctx_r311.searchQueryFocusOut(); });
    i0.ɵɵpipe(7, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_div_1_div_2_ng_container_8_Template, 19, 25, "ng-container", 2);
    i0.ɵɵtemplate(9, MaisTableComponent_div_1_div_2_ng_container_9_Template, 17, 26, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r267 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(4, 9, "table.common.messages.filter_prompt"));
    i0.ɵɵadvance(3);
    i0.ɵɵclassProp("border-primary", ctx_r267.searchQueryActive && !ctx_r267.searchQueryMalformed)("border-warning", ctx_r267.searchQueryMalformed);
    i0.ɵɵpropertyInterpolate("placeholder", i0.ɵɵpipeBind1(7, 11, "table.common.messages.filter_placeholder"));
    i0.ɵɵproperty("ngModel", ctx_r267.selectedSearchQuery);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r267.compatibilityModeForDropDowns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r267.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    var _r321 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template_input_change_0_listener($event) { i0.ɵɵrestoreView(_r321); var column_r315 = i0.ɵɵnextContext(2).$implicit; var ctx_r319 = i0.ɵɵnextContext(4); ctx_r319.toggleVisibleColumnChecked(column_r315); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r315 = i0.ɵɵnextContext(2).$implicit;
    var ctx_r317 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("checked", ctx_r317.isColumnCheckedForVisualization(column_r315));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 31);
} if (rf & 2) {
    i0.ɵɵproperty("checked", true)("disabled", true);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r325 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 20);
    i0.ɵɵelementStart(1, "div", 22);
    i0.ɵɵelementStart(2, "div", 23);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template, 1, 1, "input", 29);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template, 1, 2, "input", 30);
    i0.ɵɵelementStart(5, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template_span_click_5_listener($event) { i0.ɵɵrestoreView(_r325); var column_r315 = i0.ɵɵnextContext().$implicit; var ctx_r323 = i0.ɵɵnextContext(4); ctx_r323.toggleVisibleColumnChecked(column_r315); return $event.stopPropagation(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r315 = i0.ɵɵnextContext().$implicit;
    var ctx_r316 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r316.isHideable(column_r315));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r316.isHideable(column_r315));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r316.isHideable(column_r315));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r316.resolveLabel(column_r315), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template, 7, 5, "button", 28);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r315 = ctx.$implicit;
    var ctx_r314 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r314.hasLabel(column_r315) && (ctx_r314.isHideable(column_r315) || ctx_r314.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    var _r328 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "button", 38);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "h6", 39);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "button", 20);
    i0.ɵɵelementStart(10, "div", 22);
    i0.ɵɵelementStart(11, "div", 23);
    i0.ɵɵelementStart(12, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_Template_input_change_12_listener($event) { i0.ɵɵrestoreView(_r328); var ctx_r327 = i0.ɵɵnextContext(3); ctx_r327.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "span", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_Template_span_click_13_listener($event) { i0.ɵɵrestoreView(_r328); var ctx_r329 = i0.ɵɵnextContext(3); ctx_r329.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    i0.ɵɵtext(14);
    i0.ɵɵpipe(15, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(16, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(17, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r312 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 6, "table.common.messages.add_remove_columns"), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(8, 8, "table.common.messages.select_columns_prompt"), " ");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("checked", ctx_r312.allVisibleColumnsChecked);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(15, 10, "table.common.messages.pick_all_visible_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r312.columns);
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    i0.ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r340 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r340); var column_r333 = i0.ɵɵnextContext().$implicit; var ctx_r338 = i0.ɵɵnextContext(4); return ctx_r338.toggleVisibleColumnChecked(column_r333); });
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r333 = i0.ɵɵnextContext().$implicit;
    var ctx_r334 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("disabled", !ctx_r334.isHideable(column_r333));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r334.isHideable(column_r333) && ctx_r334.isColumnCheckedForVisualization(column_r333));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r334.isHideable(column_r333) && !ctx_r334.isColumnCheckedForVisualization(column_r333));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r334.isHideable(column_r333));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r334.resolveLabel(column_r333), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template, 5, 6, "button", 35);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r333 = ctx.$implicit;
    var ctx_r332 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r332.hasLabel(column_r333) && (ctx_r332.isHideable(column_r333) || ctx_r332.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    var _r343 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "button", 38);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵelementStart(6, "h6", 39);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "button", 32);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_Template_button_click_9_listener() { i0.ɵɵrestoreView(_r343); var ctx_r342 = i0.ɵɵnextContext(3); return ctx_r342.toggleAllVisibleColumnsChecked(); });
    i0.ɵɵtemplate(10, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    i0.ɵɵtext(12);
    i0.ɵɵpipe(13, "translate");
    i0.ɵɵelement(14, "div", 26);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(15, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template, 2, 1, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r313 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 7, "table.common.messages.add_remove_columns"), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(8, 9, "table.common.messages.select_columns_prompt"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r313.allVisibleColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r313.allVisibleColumnsChecked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(13, 11, "table.common.messages.pick_all_visible_columns"), " ");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r313.columns);
} }
function MaisTableComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 36);
    i0.ɵɵelementStart(1, "div");
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_Template, 18, 12, "ng-container", 2);
    i0.ɵɵtemplate(5, MaisTableComponent_div_1_div_3_ng_container_5_Template, 16, 13, "ng-container", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r268 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 3, "table.common.messages.customize_view_prompt"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r268.compatibilityModeForDropDowns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r268.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 36);
} }
function MaisTableComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "div", 11);
} }
function MaisTableComponent_div_1_ng_template_8_Template(rf, ctx) { }
function MaisTableComponent_div_1_ng_container_10_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r347 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 41);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_ng_container_10_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r347); var action_r345 = ctx.$implicit; var ctx_r346 = i0.ɵɵnextContext(3); return ctx_r346.clickOnContextAction(action_r345); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var action_r345 = ctx.$implicit;
    var ctx_r344 = i0.ɵɵnextContext(3);
    i0.ɵɵclassMapInterpolate2("btn btn-", action_r345.displayClass || "light", " ", action_r345.additionalClasses || "", " mr-1");
    i0.ɵɵproperty("disabled", !ctx_r344.isContextActionAllowed(action_r345));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r344.resolveLabel(action_r345), " ");
} }
function MaisTableComponent_div_1_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_1_ng_container_10_button_1_Template, 2, 6, "button", 40);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r272 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r272.headerActions);
} }
function MaisTableComponent_div_1_div_11_button_9_Template(rf, ctx) { if (rf & 1) {
    var _r351 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_1_div_11_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r351); var action_r349 = ctx.$implicit; var ctx_r350 = i0.ɵɵnextContext(3); return ctx_r350.clickOnContextAction(action_r349); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var action_r349 = ctx.$implicit;
    var ctx_r348 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("disabled", !ctx_r348.isContextActionAllowed(action_r349));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r348.resolveLabel(action_r349), " ");
} }
function MaisTableComponent_div_1_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 42);
    i0.ɵɵelementStart(1, "button", 43);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 44);
    i0.ɵɵelementStart(5, "div", 45);
    i0.ɵɵelementStart(6, "h6", 21);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, MaisTableComponent_div_1_div_11_button_9_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r273 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("disabled", !ctx_r273.anyButtonActionsAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r273.currentActions);
} }
var _c6 = function () { return {}; };
function MaisTableComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵelementStart(1, "div", 5);
    i0.ɵɵtemplate(2, MaisTableComponent_div_1_div_2_Template, 10, 13, "div", 6);
    i0.ɵɵtemplate(3, MaisTableComponent_div_1_div_3_Template, 6, 5, "div", 7);
    i0.ɵɵtemplate(4, MaisTableComponent_div_1_div_4_Template, 1, 0, "div", 7);
    i0.ɵɵtemplate(5, MaisTableComponent_div_1_div_5_Template, 1, 0, "div", 6);
    i0.ɵɵelementStart(6, "div", 8);
    i0.ɵɵelementStart(7, "div");
    i0.ɵɵtemplate(8, MaisTableComponent_div_1_ng_template_8_Template, 0, 0, "ng-template", 9);
    i0.ɵɵtext(9, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(10, MaisTableComponent_div_1_ng_container_10_Template, 2, 1, "ng-container", 2);
    i0.ɵɵtemplate(11, MaisTableComponent_div_1_div_11_Template, 10, 8, "div", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r263 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r263.filteringPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r263.columnSelectionPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r263.columnSelectionPossibleAndAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r263.filteringPossibleAndAllowed);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r263.actionsCaptionTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction0(8, _c6));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r263.headerActionsEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r263.contextActionsEnabledAndPossible);
} }
function MaisTableComponent_ng_container_2_th_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 54);
} }
function MaisTableComponent_ng_container_2_th_9_span_2_Template(rf, ctx) { if (rf & 1) {
    var _r361 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_9_span_2_Template_input_change_1_listener() { i0.ɵɵrestoreView(_r361); var ctx_r360 = i0.ɵɵnextContext(3); return ctx_r360.toggleAllChecked(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r359 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r359.allChecked);
} }
function MaisTableComponent_ng_container_2_th_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 54);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_th_9_span_2_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r353 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r353.currentEnableSelectAll && ctx_r353.currentEnableMultiSelect && !ctx_r353.noResults);
} }
function MaisTableComponent_ng_container_2_th_10_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r362 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, ctx_r362.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_2_th_10_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_2_th_10_div_7_Template(rf, ctx) { if (rf & 1) {
    var _r367 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 20);
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_10_div_7_Template_input_change_1_listener($event) { i0.ɵɵrestoreView(_r367); var filter_r365 = ctx.$implicit; var ctx_r366 = i0.ɵɵnextContext(3); ctx_r366.toggleContextFilterChecked(filter_r365); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "span", 59);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_th_10_div_7_Template_span_click_2_listener($event) { i0.ɵɵrestoreView(_r367); var filter_r365 = ctx.$implicit; var ctx_r368 = i0.ɵɵnextContext(3); ctx_r368.toggleContextFilterChecked(filter_r365); return $event.stopPropagation(); });
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var filter_r365 = ctx.$implicit;
    var ctx_r364 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r364.isContextFilterChecked(filter_r365));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r364.resolveLabel(filter_r365), " ");
} }
function MaisTableComponent_ng_container_2_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 55);
    i0.ɵɵelementStart(1, "div", 56);
    i0.ɵɵelementStart(2, "div", 57);
    i0.ɵɵelementStart(3, "div", 19);
    i0.ɵɵelementStart(4, "h6", 39);
    i0.ɵɵtemplate(5, MaisTableComponent_ng_container_2_th_10_span_5_Template, 3, 3, "span", 2);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_2_th_10_span_6_Template, 3, 3, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_2_th_10_div_7_Template, 4, 2, "div", 58);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r354 = i0.ɵɵnextContext(2);
    i0.ɵɵclassProp("border-primary", ctx_r354.anyContextFilterChecked);
    i0.ɵɵproperty("autoClose", "outside");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r354.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r354.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r354.currentContextFilters);
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    var _r376 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r376); var column_r369 = i0.ɵɵnextContext(2).$implicit; var ctx_r374 = i0.ɵɵnextContext(2); return ctx_r374.clickOnColumn(column_r369); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    var _r379 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r379); var column_r369 = i0.ɵɵnextContext(2).$implicit; var ctx_r377 = i0.ɵɵnextContext(2); return ctx_r377.clickOnColumn(column_r369); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template, 3, 0, "span", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template, 3, 0, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r369 = i0.ɵɵnextContext().$implicit;
    var ctx_r370 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r370.isCurrentSortingColumn(column_r369, "ASC"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r370.isCurrentSortingColumn(column_r369, "DESC"));
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r369 = i0.ɵɵnextContext().$implicit;
    var ctx_r371 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r371.resolveLabel(column_r369), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_11_Template(rf, ctx) { if (rf & 1) {
    var _r383 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "th", 60);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_Template_th_click_1_listener() { i0.ɵɵrestoreView(_r383); var column_r369 = ctx.$implicit; var ctx_r382 = i0.ɵɵnextContext(2); return ctx_r382.clickOnColumn(column_r369); });
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_Template, 3, 2, "span", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_ng_container_11_span_3_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r369 = ctx.$implicit;
    var ctx_r355 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("order-up", ctx_r355.isCurrentSortingColumn(column_r369, "ASC"))("order-down", ctx_r355.isCurrentSortingColumn(column_r369, "DESC"))("border-primary", ctx_r355.isCurrentSortingColumn(column_r369))("clickable", ctx_r355.isSortable(column_r369));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r355.isCurrentSortingColumn(column_r369));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r355.isLabelVisibleInTable(column_r369) && ctx_r355.hasLabel(column_r369));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    var _r397 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r397); var row_r385 = i0.ɵɵnextContext(2).$implicit; var ctx_r395 = i0.ɵɵnextContext(3); return ctx_r395.clickOnRowExpansion(row_r385); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    var _r400 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r400); var row_r385 = i0.ɵɵnextContext(2).$implicit; var ctx_r398 = i0.ɵɵnextContext(3); return ctx_r398.clickOnRowExpansion(row_r385); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 69);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r386 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r386.isExpanded(row_r385) && ctx_r386.isExpandable(row_r385));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r386.isExpanded(row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template(rf, ctx) { if (rf & 1) {
    var _r404 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "th", 69);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵelementStart(2, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template_input_change_2_listener() { i0.ɵɵrestoreView(_r404); var row_r385 = i0.ɵɵnextContext().$implicit; var ctx_r402 = i0.ɵɵnextContext(3); return ctx_r402.toggleChecked(row_r385); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r387 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("checked", ctx_r387.isChecked(row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "td");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template(rf, ctx) { }
var _c7 = function (a0, a1, a2) { return { row: a0, column: a1, value: a2 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r406 = i0.ɵɵnextContext().$implicit;
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r407 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r407.cellTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction3(2, _c7, row_r385, column_r406, ctx_r407.extractValue(row_r385, column_r406)));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r406 = i0.ɵɵnextContext().$implicit;
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r408 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r408.extractValue(row_r385, column_r406), " ");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "td");
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template, 2, 6, "div", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template, 2, 1, "div", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r406 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", column_r406.applyTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !column_r406.applyTemplate);
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template(rf, ctx) { }
var _c8 = function (a0) { return { row: a0 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 72);
    i0.ɵɵelement(1, "td");
    i0.ɵɵelementStart(2, "td", 73);
    i0.ɵɵelementStart(3, "div", 74);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r390 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("hidden", !ctx_r390.referencedTable.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵattribute("colspan", ctx_r390.activeColumnsCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r390.referencedTable.dropTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(4, _c8, row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 75);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r391 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r391.dragTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(2, _c8, row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 76);
    i0.ɵɵelement(1, "td");
    i0.ɵɵelementStart(2, "td");
    i0.ɵɵelementStart(3, "div", 77);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r385 = i0.ɵɵnextContext().$implicit;
    var ctx_r392 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(2);
    i0.ɵɵattribute("colspan", ctx_r392.activeColumnsCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r392.rowDetailTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(3, _c8, row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tr", 64);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template, 3, 2, "th", 65);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template, 3, 1, "th", 65);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template, 1, 0, "td", 2);
    i0.ɵɵtemplate(5, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template, 4, 2, "ng-container", 27);
    i0.ɵɵelementStart(6, "td", 50);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template, 5, 6, "tr", 66);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "td", 50);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template, 2, 4, "div", 67);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template, 5, 5, "tr", 68);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var row_r385 = ctx.$implicit;
    var ctx_r384 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("row-selected", ctx_r384.isChecked(row_r385));
    i0.ɵɵproperty("cdkDragData", row_r385)("cdkDragDisabled", !ctx_r384.acceptDrag);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r384.rowExpansionEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r384.currentEnableSelection);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r384.contextFilteringEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r384.visibleColumns);
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r384.rowExpansionEnabledAndPossible && ctx_r384.isExpanded(row_r385));
} }
function MaisTableComponent_ng_container_2_tbody_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tbody");
    i0.ɵɵelement(1, "tr", 63);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template, 11, 9, "ng-container", 27);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r356 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r356.currentData);
} }
function MaisTableComponent_ng_container_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tbody");
    i0.ɵɵelementStart(2, "tr", 78);
    i0.ɵɵelementStart(3, "td", 79);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r357 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵattribute("colspan", ctx_r357.activeColumnsCount);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(5, 2, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tbody");
    i0.ɵɵelementStart(2, "tr", 80);
    i0.ɵɵelementStart(3, "td", 79);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r358 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵattribute("colspan", ctx_r358.activeColumnsCount);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(5, 2, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    var _r421 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 48);
    i0.ɵɵelementStart(2, "table", 49);
    i0.ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_2_Template_table_cdkDropListDropped_2_listener($event) { i0.ɵɵrestoreView(_r421); var ctx_r420 = i0.ɵɵnextContext(); return ctx_r420.handleItemDropped($event); });
    i0.ɵɵelementStart(3, "caption", 50);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "thead", 51);
    i0.ɵɵelementStart(7, "tr");
    i0.ɵɵtemplate(8, MaisTableComponent_ng_container_2_th_8_Template, 1, 0, "th", 52);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_2_th_9_Template, 3, 1, "th", 52);
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_2_th_10_Template, 8, 6, "th", 53);
    i0.ɵɵtemplate(11, MaisTableComponent_ng_container_2_ng_container_11_Template, 4, 10, "ng-container", 27);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(12, MaisTableComponent_ng_container_2_tbody_12_Template, 3, 1, "tbody", 2);
    i0.ɵɵtemplate(13, MaisTableComponent_ng_container_2_ng_container_13_Template, 6, 4, "ng-container", 2);
    i0.ɵɵtemplate(14, MaisTableComponent_ng_container_2_ng_container_14_Template, 6, 4, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r264 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("nodrop", !ctx_r264.acceptDrop)("acceptdrop", ctx_r264.acceptDrop);
    i0.ɵɵpropertyInterpolate("id", ctx_r264.currentTableId);
    i0.ɵɵproperty("cdkDropListConnectedTo", ctx_r264.dropConnectedTo)("cdkDropListData", ctx_r264.currentData)("cdkDropListEnterPredicate", ctx_r264.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r264.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(5, 17, "table.common.accessibility.caption"));
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngIf", ctx_r264.rowExpansionEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r264.currentEnableSelection);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r264.contextFilteringEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r264.visibleColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r264.showFetching && !ctx_r264.forceReRender);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r264.showFetching && ctx_r264.noResults);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r264.showFetching);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-header-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    var _r443 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r443); var row_r438 = i0.ɵɵnextContext().$implicit; var ctx_r441 = i0.ɵɵnextContext(4); return ctx_r441.clickOnRowExpansion(row_r438); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    var _r446 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "fa-icon", 71);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template_fa_icon_click_0_listener() { i0.ɵɵrestoreView(_r446); var row_r438 = i0.ɵɵnextContext().$implicit; var ctx_r444 = i0.ɵɵnextContext(4); return ctx_r444.clickOnRowExpansion(row_r438); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell", 93);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r438 = ctx.$implicit;
    var ctx_r428 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r428.isExpanded(row_r438) && ctx_r428.isExpandable(row_r438));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r428.isExpanded(row_r438));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template(rf, ctx) { if (rf & 1) {
    var _r449 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template_input_change_1_listener() { i0.ɵɵrestoreView(_r449); var ctx_r448 = i0.ɵɵnextContext(5); return ctx_r448.toggleAllChecked(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r447 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r447.allChecked);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-header-cell", 93);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r429 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r429.currentEnableSelectAll && ctx_r429.currentEnableMultiSelect && !ctx_r429.noResults);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template(rf, ctx) { if (rf & 1) {
    var _r452 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-cell", 93);
    i0.ɵɵelementStart(1, "div", 23);
    i0.ɵɵelementStart(2, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template_input_change_2_listener() { i0.ɵɵrestoreView(_r452); var row_r450 = ctx.$implicit; var ctx_r451 = i0.ɵɵnextContext(4); return ctx_r451.toggleChecked(row_r450); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r450 = ctx.$implicit;
    var ctx_r430 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("checked", ctx_r430.isChecked(row_r450));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r453 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, ctx_r453.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "translate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template(rf, ctx) { if (rf & 1) {
    var _r458 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 20);
    i0.ɵɵelementStart(1, "input", 24);
    i0.ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_input_change_1_listener($event) { i0.ɵɵrestoreView(_r458); var filter_r456 = ctx.$implicit; var ctx_r457 = i0.ɵɵnextContext(5); ctx_r457.toggleContextFilterChecked(filter_r456); return $event.stopPropagation(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "span", 59);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_span_click_2_listener($event) { i0.ɵɵrestoreView(_r458); var filter_r456 = ctx.$implicit; var ctx_r459 = i0.ɵɵnextContext(5); ctx_r459.toggleContextFilterChecked(filter_r456); return $event.stopPropagation(); });
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var filter_r456 = ctx.$implicit;
    var ctx_r455 = i0.ɵɵnextContext(5);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", ctx_r455.isContextFilterChecked(filter_r456));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r455.resolveLabel(filter_r456), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-header-cell", 93);
    i0.ɵɵelementStart(1, "div", 94);
    i0.ɵɵelementStart(2, "div", 56);
    i0.ɵɵelementStart(3, "div", 57);
    i0.ɵɵelementStart(4, "div", 19);
    i0.ɵɵelementStart(5, "h6", 39);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template, 3, 3, "span", 2);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template, 3, 3, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template, 4, 2, "div", 58);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r431 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("border-primary", ctx_r431.anyContextFilterChecked);
    i0.ɵɵproperty("autoClose", "outside")("container", "body");
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngIf", ctx_r431.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r431.contextFilteringPrompt);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r431.currentContextFilters);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell");
    i0.ɵɵelementStart(1, "div", 77);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var detail_r461 = ctx.$implicit;
    var ctx_r433 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r433.rowDetailTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction1(2, _c8, detail_r461.___parentRow));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    var _r472 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r472); var column_r463 = i0.ɵɵnextContext(3).$implicit; var ctx_r470 = i0.ɵɵnextContext(4); return ctx_r470.clickOnColumn(column_r463); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    var _r475 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 61);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r475); var column_r463 = i0.ɵɵnextContext(3).$implicit; var ctx_r473 = i0.ɵɵnextContext(4); return ctx_r473.clickOnColumn(column_r463); });
    i0.ɵɵelement(2, "i", 62);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template, 3, 0, "span", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template, 3, 0, "span", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r463 = i0.ɵɵnextContext(2).$implicit;
    var ctx_r466 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r466.isCurrentSortingColumn(column_r463, "ASC"));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r466.isCurrentSortingColumn(column_r463, "DESC"));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r463 = i0.ɵɵnextContext(2).$implicit;
    var ctx_r467 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r467.resolveLabel(column_r463), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template(rf, ctx) { if (rf & 1) {
    var _r480 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-header-cell");
    i0.ɵɵelementStart(1, "div", 25);
    i0.ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template_div_click_1_listener() { i0.ɵɵrestoreView(_r480); var column_r463 = i0.ɵɵnextContext().$implicit; var ctx_r478 = i0.ɵɵnextContext(4); return ctx_r478.clickOnColumn(column_r463); });
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template, 3, 2, "span", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template, 2, 1, "span", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r463 = i0.ɵɵnextContext().$implicit;
    var ctx_r464 = i0.ɵɵnextContext(4);
    i0.ɵɵclassMapInterpolate1("maissize-", column_r463.size || "default", "");
    i0.ɵɵadvance(1);
    i0.ɵɵclassMap(column_r463.headerDisplayClass || "");
    i0.ɵɵclassProp("order-up", ctx_r464.isCurrentSortingColumn(column_r463, "ASC"))("order-down", ctx_r464.isCurrentSortingColumn(column_r463, "DESC"))("border-primary", ctx_r464.isCurrentSortingColumn(column_r463))("clickable", ctx_r464.isSortable(column_r463));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r464.isCurrentSortingColumn(column_r463));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r464.isLabelVisibleInTable(column_r463) && ctx_r464.hasLabel(column_r463));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template, 0, 0, "ng-template", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r482 = i0.ɵɵnextContext().$implicit;
    var column_r463 = i0.ɵɵnextContext().$implicit;
    var ctx_r483 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r483.cellTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction3(2, _c7, row_r482, column_r463, ctx_r483.extractValue(row_r482, column_r463)));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r482 = i0.ɵɵnextContext().$implicit;
    var column_r463 = i0.ɵɵnextContext().$implicit;
    var ctx_r484 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r484.extractValue(row_r482, column_r463), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-cell");
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template, 2, 6, "div", 2);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template, 2, 1, "div", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var column_r463 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵclassMapInterpolate2("maissize-", column_r463.size || "default", " ", column_r463.cellDisplayClass || "", "");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", column_r463.applyTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !column_r463.applyTemplate);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0, 85);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template, 4, 16, "mat-header-cell", 95);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template, 3, 6, "mat-cell", 96);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var column_r463 = ctx.$implicit;
    i0.ɵɵproperty("matColumnDef", column_r463.name);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-header-row");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-row", 97);
} if (rf & 2) {
    var row_r491 = ctx.$implicit;
    var ctx_r436 = i0.ɵɵnextContext(4);
    i0.ɵɵclassProp("expanded", ctx_r436.isMatTableExpanded(row_r491))("row-selected", ctx_r436.isChecked(row_r491));
    i0.ɵɵproperty("cdkDragData", row_r491)("cdkDragDisabled", !ctx_r436.acceptDrag);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-row", 98);
} if (rf & 2) {
    var row_r492 = ctx.$implicit;
    var ctx_r437 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("@detailExpand", ctx_r437.isMatTableExpanded(row_r492) ? "expanded" : "collapsed");
} }
var _c9 = function () { return ["internal___col_row_detail"]; };
function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template(rf, ctx) { if (rf & 1) {
    var _r494 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-table", 83, 84);
    i0.ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template_mat_table_cdkDropListDropped_0_listener($event) { i0.ɵɵrestoreView(_r494); var ctx_r493 = i0.ɵɵnextContext(3); return ctx_r493.handleItemDropped($event); });
    i0.ɵɵelementContainerStart(2, 85);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template, 1, 0, "mat-header-cell", 86);
    i0.ɵɵtemplate(4, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template, 3, 2, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(5, 85);
    i0.ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template, 3, 1, "mat-header-cell", 86);
    i0.ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template, 3, 1, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(8, 85);
    i0.ɵɵtemplate(9, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template, 9, 7, "mat-header-cell", 86);
    i0.ɵɵtemplate(10, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template, 1, 0, "mat-cell", 87);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(11, 85);
    i0.ɵɵtemplate(12, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template, 3, 4, "mat-cell", 88);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵtemplate(13, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template, 3, 1, "ng-container", 89);
    i0.ɵɵtemplate(14, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template, 1, 0, "mat-header-row", 90);
    i0.ɵɵtemplate(15, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template, 1, 6, "mat-row", 91);
    i0.ɵɵtemplate(16, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template, 1, 1, "mat-row", 92);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r423 = i0.ɵɵnextContext(3);
    i0.ɵɵclassProp("nodrop", !ctx_r423.acceptDrop)("acceptdrop", ctx_r423.acceptDrop);
    i0.ɵɵpropertyInterpolate("id", ctx_r423.currentTableId);
    i0.ɵɵproperty("dataSource", ctx_r423.matTableDataObservable)("cdkDropListConnectedTo", ctx_r423.dropConnectedTo)("cdkDropListData", ctx_r423.currentData)("cdkDropListEnterPredicate", ctx_r423.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r423.acceptDrop);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_expansion");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_selection");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_context_filtering");
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("matColumnDef", "internal___col_row_detail");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r423.matTableVisibleColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matHeaderRowDef", ctx_r423.matTableVisibleColumnDefs);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", ctx_r423.matTableVisibleColumnDefs);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", i0.ɵɵpureFunction0(19, _c9))("matRowDefWhen", ctx_r423.isMatTableExpansionDetailRow);
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 78);
    i0.ɵɵelementStart(2, "p", 99);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 1, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 80);
    i0.ɵɵelementStart(2, "p", 99);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind1(4, 1, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 48);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_Template, 17, 20, "mat-table", 82);
    i0.ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_ng_container_2_Template, 5, 3, "ng-container", 2);
    i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_ng_container_3_Template, 5, 3, "ng-container", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r422 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r422.showFetching && !ctx_r422.forceReRender);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r422.showFetching && ctx_r422.noResults);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r422.showFetching);
} }
function MaisTableComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_Template, 4, 3, "div", 81);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r265 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r265.matTableDataObservable);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 113);
    i0.ɵɵtext(1, "(current)");
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template(rf, ctx) { if (rf & 1) {
    var _r507 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 106);
    i0.ɵɵelementStart(1, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template_a_click_1_listener() { i0.ɵɵrestoreView(_r507); var page_r501 = i0.ɵɵnextContext().$implicit; var ctx_r505 = i0.ɵɵnextContext(3); return ctx_r505.switchToPage(page_r501); });
    i0.ɵɵtext(2);
    i0.ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template, 2, 0, "span", 112);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var page_r501 = i0.ɵɵnextContext().$implicit;
    var ctx_r502 = i0.ɵɵnextContext(3);
    i0.ɵɵclassProp("active", page_r501 === ctx_r502.currentPageIndex);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", page_r501 + 1, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", page_r501 === ctx_r502.currentPageIndex);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 114);
    i0.ɵɵelementStart(1, "a", 115);
    i0.ɵɵtext(2, " ... ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template, 4, 4, "li", 110);
    i0.ɵɵtemplate(2, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template, 3, 0, "li", 111);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var page_r501 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !page_r501.skip);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", page_r501.skip);
} }
function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template(rf, ctx) { if (rf & 1) {
    var _r512 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r512); var pageSize_r510 = ctx.$implicit; var ctx_r511 = i0.ɵɵnextContext(4); return ctx_r511.clickOnPageSize(pageSize_r510); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var pageSize_r510 = ctx.$implicit;
    var ctx_r509 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("disabled", pageSize_r510 === ctx_r509.currentPageSize);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", pageSize_r510, " ");
} }
var _c10 = function (a0) { return { current: a0 }; };
function MaisTableComponent_div_4_ng_container_3_span_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 116);
    i0.ɵɵelementStart(1, "div");
    i0.ɵɵtext(2, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "button", 117);
    i0.ɵɵtext(4);
    i0.ɵɵpipe(5, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "span", 44);
    i0.ɵɵtemplate(7, MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r500 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind2(5, 2, "table.common.messages.page_size_button", i0.ɵɵpureFunction1(5, _c10, ctx_r500.currentPageSize)));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", ctx_r500.currentPossiblePageSizes);
} }
var _c11 = function (a0, a1) { return { totalElements: a0, totalPages: a1 }; };
function MaisTableComponent_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    var _r514 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "div", 102);
    i0.ɵɵelementStart(2, "div", 103);
    i0.ɵɵtext(3);
    i0.ɵɵpipe(4, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "nav", 104);
    i0.ɵɵelementStart(6, "ul", 105);
    i0.ɵɵelementStart(7, "li", 106);
    i0.ɵɵelementStart(8, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_8_listener() { i0.ɵɵrestoreView(_r514); var ctx_r513 = i0.ɵɵnextContext(2); return ctx_r513.switchToPage(0); });
    i0.ɵɵtext(9);
    i0.ɵɵpipe(10, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(11, "li", 106);
    i0.ɵɵelementStart(12, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_12_listener() { i0.ɵɵrestoreView(_r514); var ctx_r515 = i0.ɵɵnextContext(2); return ctx_r515.switchToPage(ctx_r515.currentPageIndex - 1); });
    i0.ɵɵtext(13);
    i0.ɵɵpipe(14, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(15, MaisTableComponent_div_4_ng_container_3_ng_container_15_Template, 3, 2, "ng-container", 27);
    i0.ɵɵelementStart(16, "li", 106);
    i0.ɵɵelementStart(17, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_17_listener() { i0.ɵɵrestoreView(_r514); var ctx_r516 = i0.ɵɵnextContext(2); return ctx_r516.switchToPage(ctx_r516.currentPageIndex + 1); });
    i0.ɵɵtext(18);
    i0.ɵɵpipe(19, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(20, "li", 106);
    i0.ɵɵelementStart(21, "a", 107);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_21_listener() { i0.ɵɵrestoreView(_r514); var ctx_r517 = i0.ɵɵnextContext(2); return ctx_r517.switchToPage(ctx_r517.currentPageCount - 1); });
    i0.ɵɵtext(22);
    i0.ɵɵpipe(23, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(24, "div", 108);
    i0.ɵɵtemplate(25, MaisTableComponent_div_4_ng_container_3_span_25_Template, 8, 7, "span", 109);
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r495 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind2(4, 15, "table.common.pagination.total_elements", i0.ɵɵpureFunction2(26, _c11, ctx_r495.currentResultNumber, ctx_r495.currentPageCount)), " ");
    i0.ɵɵadvance(4);
    i0.ɵɵclassProp("disabled", !(ctx_r495.currentPageIndex > 0));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(10, 18, "table.common.pagination.first_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("disabled", !(ctx_r495.currentPageIndex > 0));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(14, 20, "table.common.pagination.previous_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r495.enumPages);
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("disabled", ctx_r495.currentPageIndex >= ctx_r495.currentPageCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(19, 22, "table.common.pagination.next_page"));
    i0.ɵɵadvance(2);
    i0.ɵɵclassProp("disabled", ctx_r495.currentPageIndex >= ctx_r495.currentPageCount - 1);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(23, 24, "table.common.pagination.last_page"));
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r495.pageSizeSelectEnabledAndPossible);
} }
function MaisTableComponent_div_4_ng_template_6_Template(rf, ctx) { }
function MaisTableComponent_div_4_ng_container_8_button_1_Template(rf, ctx) { if (rf & 1) {
    var _r521 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 41);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_8_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r521); var action_r519 = ctx.$implicit; var ctx_r520 = i0.ɵɵnextContext(3); return ctx_r520.clickOnContextAction(action_r519); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var action_r519 = ctx.$implicit;
    var ctx_r518 = i0.ɵɵnextContext(3);
    i0.ɵɵclassMapInterpolate2("btn btn-", action_r519.displayClass || "light", " ", action_r519.additionalClasses || "", " mr-1");
    i0.ɵɵproperty("disabled", !ctx_r518.isContextActionAllowed(action_r519));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r518.resolveLabel(action_r519), " ");
} }
function MaisTableComponent_div_4_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_8_button_1_Template, 2, 6, "button", 40);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var ctx_r497 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r497.headerActions);
} }
function MaisTableComponent_div_4_div_9_button_9_Template(rf, ctx) { if (rf & 1) {
    var _r525 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 47);
    i0.ɵɵlistener("click", function MaisTableComponent_div_4_div_9_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r525); var action_r523 = ctx.$implicit; var ctx_r524 = i0.ɵɵnextContext(3); return ctx_r524.clickOnContextAction(action_r523); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var action_r523 = ctx.$implicit;
    var ctx_r522 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("disabled", !ctx_r522.isContextActionAllowed(action_r523));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r522.resolveLabel(action_r523), " ");
} }
function MaisTableComponent_div_4_div_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 42);
    i0.ɵɵelementStart(1, "button", 43);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 44);
    i0.ɵɵelementStart(5, "div", 45);
    i0.ɵɵelementStart(6, "h6", 21);
    i0.ɵɵtext(7);
    i0.ɵɵpipe(8, "translate");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(9, MaisTableComponent_div_4_div_9_button_9_Template, 2, 2, "button", 46);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r498 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("disabled", !ctx_r498.anyButtonActionsAllowed);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r498.currentActions);
} }
function MaisTableComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 100);
    i0.ɵɵelementStart(1, "div", 5);
    i0.ɵɵelementStart(2, "div", 101);
    i0.ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_Template, 26, 29, "ng-container", 2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 8);
    i0.ɵɵelementStart(5, "div");
    i0.ɵɵtemplate(6, MaisTableComponent_div_4_ng_template_6_Template, 0, 0, "ng-template", 9);
    i0.ɵɵtext(7, " \u00A0 ");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(8, MaisTableComponent_div_4_ng_container_8_Template, 2, 1, "ng-container", 2);
    i0.ɵɵtemplate(9, MaisTableComponent_div_4_div_9_Template, 10, 8, "div", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r266 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r266.currentResultNumber > 0 && ctx_r266.currentEnablePagination);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r266.actionsCaptionTemplate)("ngTemplateOutletContext", i0.ɵɵpureFunction0(5, _c6));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r266.headerActionsEnabledAndPossible);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r266.contextActionsEnabledAndPossible);
} }
var MaisTableComponent = /** @class */ (function () {
    // lifecycle hooks
    function MaisTableComponent(translateService, configurationService, registry, cdr, ngZone) {
        var _this = this;
        this.translateService = translateService;
        this.configurationService = configurationService;
        this.registry = registry;
        this.cdr = cdr;
        this.ngZone = ngZone;
        this.dataProvider = null;
        this.defaultSortingColumn = null;
        this.defaultSortingDirection = null;
        this.defaultPageSize = null;
        this.possiblePageSize = null;
        // output events
        this.pageChange = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.selectionChange = new EventEmitter();
        this.filteringColumnsChange = new EventEmitter();
        this.visibleColumnsChange = new EventEmitter();
        this.contextFiltersChange = new EventEmitter();
        this.statusChange = new EventEmitter();
        this.itemDragged = new EventEmitter();
        this.itemDropped = new EventEmitter();
        this.action = new EventEmitter();
        this.isIE = MaisTableBrowserHelper.isIE();
        this.statusSnapshot = null;
        this.persistableStatusSnapshot = null;
        this.forceReRender = false;
        this.initialized = false;
        this.fetching = false;
        this.showFetching = false;
        this.refreshEmitterSubscription = null;
        this.refreshIntervalSubscription = null;
        // MatTable adapter
        this.matTableDataObservable = null;
        this.expandedElement = null;
        // drag and drop support
        this.acceptDropPredicate = function (item) {
            return _this.acceptDrop;
        };
        this.isMatTableExpansionDetailRow = function (i, row) { return row.hasOwnProperty('___detailRowContent'); };
        this.isMatTableExpanded = function (row, limit) {
            if (limit === void 0) { limit = false; }
            if (!limit && row.___detailRow) {
                if (_this.isMatTableExpanded(row.___detailRow, true)) {
                    return true;
                }
            }
            if (!limit && row.___parentRow) {
                if (_this.isMatTableExpanded(row.___parentRow, true)) {
                    return true;
                }
            }
            if (!_this.rowExpansionEnabledAndPossible) {
                return false;
            }
            if (!_this.isExpandable(row)) {
                return false;
            }
            if (!_this.isExpanded(row)) {
                return false;
            }
            return true;
        };
        this.uuid = 'MTCMP-' + (++MaisTableComponent.counter) + '-' + Math.round(Math.random() * 100000);
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('building component');
        this.matTableDataObservable = new Subject();
    }
    MaisTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('initializing component');
        this.registrationId = this.registry.register(this.currentTableId, this);
        // input validation
        MaisTableValidatorHelper.validateColumnsSpecification(this.columns, this.configurationService.getConfiguration());
        this.clientDataSnapshot = [];
        this.dataSnapshot = [];
        this.fetchedPageCount = 0;
        this.fetchedResultNumber = 0;
        this.selectedPageIndex = 0;
        this.selectedSearchQuery = null;
        this.selectedPageSize = null;
        this.expandedItems = [];
        this.checkedItems = [];
        this.checkedContextFilters = [];
        this.checkedColumnsForFiltering = this.filterableColumns.filter(function (c) { return MaisTableFormatterHelper.isDefaultFilterColumn(c, _this.configurationService.getConfiguration()); });
        this.checkedColumnsForVisualization = this.columns.filter(function (c) { return MaisTableFormatterHelper.isDefaultVisibleColumn(c, _this.configurationService.getConfiguration()); });
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        if (this.dataProvider) {
            // input is a provider function
            if (this.data) {
                throw new Error('MaisTable can\'t be provided both data and dataProvider');
            }
        }
        else if (this.data instanceof Observable || this.data instanceof Subject) {
            // input is observable
            this.data.subscribe(function (dataSnapshot) {
                _this.handleInputDataObservableEmission(dataSnapshot);
            });
        }
        else {
            // input is data array
            this.clientDataSnapshot = this.data;
        }
        // LOAD STATUS from store if possible
        var activationObservable;
        if (this.storePersistenceEnabledAndPossible) {
            activationObservable = this.loadStatusFromStore();
        }
        else {
            activationObservable = new Observable(function (subscriber) {
                subscriber.next(null);
                subscriber.complete();
            });
        }
        activationObservable.subscribe(function (status) {
            if (status) {
                _this.logger.trace('restored status snapshot from storage', status);
                _this.applyStatusSnapshot(status);
            }
            _this.reload({ reason: MaisTableReloadReason.INTERNAL, withStatusSnapshot: status }).subscribe(function (yes) {
                _this.completeInitialization();
            }, function (nope) {
                _this.completeInitialization();
            });
        }, function (failure) {
            _this.logger.error('restoring status from store failed', failure);
            _this.reload({ reason: MaisTableReloadReason.INTERNAL }).subscribe(function (yes) {
                _this.completeInitialization();
            }, function (nope) {
                _this.completeInitialization();
            });
        });
    };
    MaisTableComponent.prototype.ngAfterContentInit = function () {
        this.logger.debug('after content init');
    };
    MaisTableComponent.prototype.handleInputDataObservableEmission = function (dataSnapshot) {
        this.logger.debug('data snapshot emitted from input data');
        this.clientDataSnapshot = dataSnapshot;
        this.reload({ reason: MaisTableReloadReason.EXTERNAL });
    };
    MaisTableComponent.prototype.completeInitialization = function () {
        var _this = this;
        this.initialized = true;
        this.statusChange.pipe(debounceTime(200)).subscribe(function (statusSnapshot) {
            _this.persistStatusToStore().subscribe();
        });
        if (this.refreshEmitter) {
            this.handleRefreshEmitterChange(this.refreshEmitter);
        }
        if (this.refreshInterval) {
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    };
    MaisTableComponent.prototype.ngOnDestroy = function () {
        this.logger.trace('destroying component');
        if (this.registrationId) {
            this.registry.unregister(this.currentTableId, this.registrationId);
        }
    };
    MaisTableComponent.prototype.ngOnChanges = function (changes) {
        if (changes.refreshEmitter) {
            this.handleRefreshEmitterChange(changes.refreshEmitter.currentValue);
        }
        if (changes.refreshInterval) {
            this.handleRefreshIntervalChange(changes.refreshInterval.currentValue);
        }
        if (changes.refreshStrategy && !changes.refreshStrategy.firstChange) {
            this.logger.warn('REFRESH STRATEGY CHANGED WHILE RUNNING. YOU SURE ABOUT THIS?', changes);
            this.handleRefreshEmitterChange(this.refreshEmitter);
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    };
    MaisTableComponent.prototype.handleRefreshEmitterChange = function (newValue) {
        var _this = this;
        if (this.refreshEmitterSubscription) {
            this.refreshEmitterSubscription.unsubscribe();
        }
        if (newValue) {
            this.refreshEmitterSubscription = newValue.subscribe(function (event) {
                _this.logger.trace('received refresh push request', event);
                if (_this.dragInProgress) {
                    _this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (_this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.ON_PUSH) === -1) {
                    _this.logger.warn('refresh from emitter ignored because refresh strategies are ' + _this.currentRefreshStrategies +
                        '. Why are you pushing to this component?');
                }
                else if (!_this.initialized) {
                    _this.logger.warn('refresh from emitter ignored because the component is not fully initialized');
                }
                else if (_this.fetching) {
                    _this.logger.warn('refresh from emitter ignored because the component is fetching already');
                }
                else {
                    _this.logger.debug('launching reload following push request');
                    _this.reload({
                        reason: MaisTableReloadReason.PUSH,
                        pushRequest: event,
                        inBackground: event.inBackground === true || event.inBackground === false ?
                            event.inBackground : _this.currentRefreshOnPushInBackground
                    });
                }
            });
        }
    };
    MaisTableComponent.prototype.handleRefreshIntervalChange = function (newValue) {
        var _this = this;
        if (this.refreshIntervalSubscription) {
            this.refreshIntervalSubscription.unsubscribe();
        }
        if (this.refreshIntervalTimer) {
            this.refreshIntervalTimer = null;
        }
        if (newValue) {
            this.refreshIntervalTimer = timer(newValue, newValue);
            this.refreshIntervalSubscription = this.refreshIntervalTimer.subscribe(function (tick) {
                _this.logger.trace('emitted refresh tick request');
                if (_this.dragInProgress) {
                    _this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (_this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.TIMED) === -1) {
                    _this.logger.warn('refresh from tick ignored because refresh strategies are ' + _this.currentRefreshStrategies +
                        '. Why is this component emitting ticks ?');
                }
                else if (!_this.initialized) {
                    _this.logger.warn('refresh from tick ignored because the component is not fully initialized');
                }
                else if (_this.fetching) {
                    _this.logger.warn('refresh from tick ignored because the component is fetching already');
                }
                else {
                    _this.logger.debug('launching reload following tick');
                    _this.reload({
                        reason: MaisTableReloadReason.INTERVAL,
                        inBackground: _this.currentRefreshIntervalInBackground
                    });
                }
            });
        }
    };
    // public methods
    MaisTableComponent.prototype.refresh = function (background) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        return this.reload({ reason: MaisTableReloadReason.EXTERNAL, inBackground: background });
    };
    MaisTableComponent.prototype.getDataSnapshot = function () {
        return this.dataSnapshot;
    };
    MaisTableComponent.prototype.getStatusSnapshot = function () {
        return this.statusSnapshot;
    };
    MaisTableComponent.prototype.loadStatus = function (status) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        this.logger.trace('loading status snapshot from external caller', status);
        this.applyStatusSnapshot(status);
        this.reload({ reason: MaisTableReloadReason.EXTERNAL, withStatusSnapshot: status });
    };
    MaisTableComponent.prototype.applyStatusSnapshot = function (status) {
        var _this = this;
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (status.orderColumn) {
            this.selectedSortColumn = this.columns.find(function (c) { return _this.isSortable(c) && c.name === status.orderColumn; }) || null;
        }
        if (status.orderColumnDirection) {
            this.selectedSortDirection = (status.orderColumnDirection === MaisTableSortDirection.DESCENDING) ?
                MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
        }
        if (this.currentEnableFiltering && status.query) {
            this.selectedSearchQuery = status.query.trim();
        }
        if (this.currentEnableFiltering && status.queryColumns && status.queryColumns.length) {
            this.checkedColumnsForFiltering = this.filterableColumns.filter(function (c) { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.queryColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableColumnsSelection && status.visibleColumns && status.visibleColumns.length) {
            this.checkedColumnsForVisualization = this.columns.filter(function (c) { var _a, _b; return !_this.isHideable(c) || ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.visibleColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableContextFiltering && status.contextFilters && status.contextFilters.length) {
            this.checkedContextFilters = this.contextFilters.filter(function (f) { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.contextFilters) === null || _b === void 0 ? void 0 : _b.indexOf(f.name)) !== -1; });
        }
        if (this.currentEnablePagination && status.currentPage || status.currentPage === 0) {
            this.selectedPageIndex = status.currentPage;
        }
        if (this.currentEnablePagination && status.pageSize) {
            this.selectedPageSize = this.currentPossiblePageSizes.find(function (s) { return status.pageSize === s; }) || null;
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    };
    MaisTableComponent.prototype.applyStatusSnapshotPostFetch = function (status) {
        var _this = this;
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (this.currentEnableSelection && this.itemTrackingEnabledAndPossible
            && status.checkedItemIdentifiers && status.checkedItemIdentifiers.length) {
            this.checkedItems = this.dataSnapshot.filter(function (data) {
                var _a, _b;
                var id = _this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.checkedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1;
            });
        }
        if (this.currentEnableRowExpansion && this.itemTrackingEnabledAndPossible
            && status.expandedItemIdentifiers && status.expandedItemIdentifiers.length) {
            this.expandedItems = this.dataSnapshot.filter(function (data) {
                var _a, _b;
                var id = _this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.expandedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1 && _this.isExpandable(data);
            });
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    };
    MaisTableComponent.prototype.reload = function (context) {
        var _this = this;
        this.fetching = true;
        this.showFetching = !context.inBackground;
        var obs = new Observable(function (subscriber) {
            try {
                _this.reloadInObservable(subscriber, context);
            }
            catch (e) {
                subscriber.error(e);
                subscriber.complete();
            }
        });
        obs.subscribe(function (success) {
            _this.logger.trace('async reload success');
            _this.fetching = false;
            if (!context.inBackground) {
                _this.showFetching = false;
            }
            _this.handleMatTableDataSnapshotChanged();
        }, function (failure) {
            _this.logger.trace('async reload failed', failure);
            _this.fetching = false;
            if (!context.inBackground) {
                _this.showFetching = false;
            }
            _this.handleMatTableDataSnapshotChanged();
        });
        return obs;
    };
    MaisTableComponent.prototype.reloadInObservable = function (tracker, context) {
        var _this = this;
        var withSnapshot = context.withStatusSnapshot || null;
        var pageRequest = this.buildPageRequest();
        this.logger.debug('reloading table data', pageRequest);
        // clear checked items
        if (!context.inBackground) {
            this.checkedItems = [];
            this.expandedItems = [];
            this.statusChanged();
        }
        this.lastFetchedSearchQuery = pageRequest.query || null;
        if (this.dataProvider) {
            // call data provider
            this.logger.trace('reload has been called, fetching data from provided function');
            this.logger.trace('page request for data fetch is', pageRequest);
            this.dataProvider(pageRequest, context).subscribe(function (response) {
                _this.logger.trace('fetching data completed successfully');
                if (!_this.dragInProgress) {
                    if (_this.paginationMode === MaisTablePaginationMethod.SERVER) {
                        _this.parseResponseWithServerPagination(response);
                    }
                    else {
                        _this.parseResponseWithClientPagination(response.content, pageRequest);
                    }
                    if (withSnapshot) {
                        _this.applyStatusSnapshotPostFetch(withSnapshot);
                    }
                }
                else {
                    _this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.next();
                tracker.complete();
            }, function (failure) {
                _this.logger.error('error fetching data from provider function', failure);
                if (!_this.dragInProgress) {
                    _this.dataSnapshot = [];
                }
                else {
                    _this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.error(failure);
                tracker.complete();
            });
        }
        else {
            // data is not provided on request and is in clientDataSnapshot
            this.logger.trace('reload has been called on locally fetched data');
            if (!this.dragInProgress) {
                this.parseResponseWithClientPagination(this.clientDataSnapshot, pageRequest);
                if (withSnapshot) {
                    this.applyStatusSnapshotPostFetch(withSnapshot);
                }
            }
            else {
                this.logger.warn('data fetch aborted because user is dragging things');
            }
            tracker.next();
            tracker.complete();
        }
    };
    MaisTableComponent.prototype.parseResponseWithServerPagination = function (response) {
        this.dataSnapshot = response.content;
        if (this.currentEnablePagination) {
            if (response.totalPages) {
                this.fetchedPageCount = response.totalPages;
            }
            else {
                throw new Error('data from server did not contain required totalPages field');
            }
            if (response.totalElements) {
                this.fetchedResultNumber = response.totalElements;
            }
            else {
                throw new Error('data from server did not contain required totalElements field');
            }
        }
    };
    MaisTableComponent.prototype.parseResponseWithClientPagination = function (data, request) {
        this.logger.trace('applying in-memory fetching, paginating, ordering and filtering');
        var inMemoryResponse = MaisTableInMemoryHelper.fetchInMemory(data, request, this.currentSortColumn, this.checkedColumnsForFiltering, this.getCurrentLocale());
        this.dataSnapshot = inMemoryResponse.content;
        this.fetchedResultNumber = typeof inMemoryResponse.totalElements === 'undefined' ? null : inMemoryResponse.totalElements;
        this.fetchedPageCount = typeof inMemoryResponse.totalPages === 'undefined' ? null : inMemoryResponse.totalPages;
    };
    MaisTableComponent.prototype.buildPageRequest = function () {
        var e_1, _a;
        var _b, _c;
        var output = {
            page: this.currentEnablePagination ? this.currentPageIndex : null,
            size: this.currentEnablePagination ? this.currentPageSize : null,
            sort: [],
            query: null,
            queryFields: [],
            filters: []
        };
        if (this.currentEnableFiltering && this.searchQueryActive) {
            output.query = this.currentSearchQuery;
            output.queryFields = this.checkedColumnsForFiltering.map(function (column) { return column.serverField || column.field || null; });
        }
        if (this.currentEnableContextFiltering && this.checkedContextFilters.length) {
            try {
                for (var _d = __values(this.checkedContextFilters), _e = _d.next(); !_e.done; _e = _d.next()) {
                    var filter = _e.value;
                    (_b = output.filters) === null || _b === void 0 ? void 0 : _b.push(filter.name || null);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        var sortColumn = this.currentSortColumn;
        var sortDirection = this.currentSortDirection;
        if (sortColumn) {
            (_c = output.sort) === null || _c === void 0 ? void 0 : _c.push({
                property: sortColumn.serverField || sortColumn.field || null,
                direction: sortDirection || MaisTableSortDirection.ASCENDING
            });
        }
        return output;
    };
    MaisTableComponent.prototype.setPage = function (index) {
        this.selectedPageIndex = index;
        this.statusChanged();
        this.emitPageChanged();
    };
    Object.defineProperty(MaisTableComponent.prototype, "currentRefreshOnPushInBackground", {
        get: function () {
            var _a;
            if (this.refreshOnPushInBackground === true || this.refreshOnPushInBackground === false) {
                return this.refreshOnPushInBackground;
            }
            else {
                return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnPushInBackground;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentRefreshIntervalInBackground", {
        get: function () {
            var _a;
            if (this.refreshIntervalInBackground === true || this.refreshIntervalInBackground === false) {
                return this.refreshIntervalInBackground;
            }
            else {
                return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnTickInBackground;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentRefreshStrategies", {
        get: function () {
            var _a;
            if (this.refreshStrategy) {
                if (Array.isArray(this.refreshStrategy) && this.refreshStrategy.length) {
                    return this.refreshStrategy;
                }
                else {
                    return [this.refreshStrategy];
                }
            }
            return [(_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultStrategy];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentRefreshInterval", {
        get: function () {
            var _a;
            return this.refreshInterval || ((_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultInterval);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentTableId", {
        get: function () {
            return this.tableId || this.uuid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableDrag", {
        get: function () {
            var _a;
            if (this.enableDrag === true || this.enableDrag === false) {
                return this.enableDrag;
            }
            else {
                return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dragEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableDrop", {
        get: function () {
            var _a;
            if (this.enableDrop === true || this.enableDrop === false) {
                return this.enableDrop;
            }
            else {
                return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dropEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnablePagination", {
        get: function () {
            var _a;
            if (this.enablePagination === true || this.enablePagination === false) {
                return this.enablePagination;
            }
            else {
                return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableItemTracking", {
        get: function () {
            var _a;
            if (this.enableItemTracking === true || this.enableItemTracking === false) {
                return this.enableItemTracking;
            }
            else {
                return (_a = this.configurationService.getConfiguration().itemTracking) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableStorePersistence", {
        get: function () {
            var _a;
            if (this.enableStorePersistence === true || this.enableStorePersistence === false) {
                return this.enableStorePersistence;
            }
            else {
                return (_a = this.configurationService.getConfiguration().storePersistence) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnablePageSizeSelect", {
        get: function () {
            var _a;
            if (this.enablePageSizeSelect === true || this.enablePageSizeSelect === false) {
                return this.enablePageSizeSelect;
            }
            else {
                return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.pageSizeSelectionEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableMultipleRowExpansion", {
        get: function () {
            var _a;
            if (this.enableMultipleRowExpansion === true || this.enableMultipleRowExpansion === false) {
                return this.enableMultipleRowExpansion;
            }
            else {
                return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.multipleExpansionEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableRowExpansion", {
        get: function () {
            var _a;
            if (this.enableRowExpansion === true || this.enableRowExpansion === false) {
                return this.enableRowExpansion;
            }
            else {
                return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableHeaderActions", {
        get: function () {
            var _a;
            if (this.enableHeaderActions === true || this.enableHeaderActions === false) {
                return this.enableHeaderActions;
            }
            else {
                return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.headerActionsEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableContextActions", {
        get: function () {
            var _a;
            if (this.enableContextActions === true || this.enableContextActions === false) {
                return this.enableContextActions;
            }
            else {
                return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.contextActionsEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableColumnsSelection", {
        get: function () {
            var _a;
            if (this.enableColumnsSelection === true || this.enableColumnsSelection === false) {
                return this.enableColumnsSelection;
            }
            else {
                return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableFiltering", {
        get: function () {
            var _a;
            if (this.enableFiltering === true || this.enableFiltering === false) {
                return this.enableFiltering;
            }
            else {
                return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableMultiSelect", {
        get: function () {
            var _a;
            if (this.enableMultiSelect === true || this.enableMultiSelect === false) {
                return this.enableMultiSelect;
            }
            else {
                return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.multipleSelectionEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableSelectAll", {
        get: function () {
            var _a;
            if (this.enableSelectAll === true || this.enableSelectAll === false) {
                return this.enableSelectAll;
            }
            else {
                return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.selectAllEnabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableContextFiltering", {
        get: function () {
            var _a;
            if (this.enableContextFiltering === true || this.enableContextFiltering === false) {
                return this.enableContextFiltering;
            }
            else {
                return (_a = this.configurationService.getConfiguration().contextFiltering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentEnableSelection", {
        get: function () {
            var _a;
            if (this.enableSelection === true || this.enableSelection === false) {
                return this.enableSelection;
            }
            else {
                return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentPaginationMode", {
        get: function () {
            var _a;
            if (this.paginationMode) {
                return this.paginationMode;
            }
            else {
                return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPaginationMode;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "itemTrackingEnabledAndPossible", {
        get: function () {
            return !!this.itemIdentifier && this.currentEnableItemTracking;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "storePersistenceEnabledAndPossible", {
        get: function () {
            return this.storeAdapter && this.currentEnableStorePersistence;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "rowExpansionEnabledAndPossible", {
        get: function () {
            return this.currentEnableRowExpansion;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "pageSizeSelectEnabledAndPossible", {
        get: function () {
            return this.currentPossiblePageSizes && this.currentPossiblePageSizes.length > 0 && this.currentEnablePageSizeSelect;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "contextFilteringEnabledAndPossible", {
        get: function () {
            return this.contextFilters && this.contextFilters.length > 0 && this.currentEnableContextFiltering;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "contextActionsEnabledAndPossible", {
        get: function () {
            return this.contextActions && this.contextActions.length > 0 && this.currentEnableContextActions;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "headerActionsEnabledAndPossible", {
        get: function () {
            return this.headerActions && this.headerActions.length > 0 && this.currentEnableHeaderActions;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "columnSelectionPossibleAndAllowed", {
        get: function () {
            return this.currentEnableColumnsSelection && this.columns.length > 0 && this.hideableColumns.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "filteringPossibleAndAllowed", {
        get: function () {
            return this.currentEnableFiltering && this.filterableColumns.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentPossiblePageSizes", {
        get: function () {
            var _a;
            if (this.possiblePageSize && this.possiblePageSize.length) {
                return this.possiblePageSize;
            }
            else {
                return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPossiblePageSizes;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentSearchQuery", {
        get: function () {
            if (this.selectedSearchQuery) {
                return this.selectedSearchQuery;
            }
            else {
                return null;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentPageCount", {
        get: function () {
            return this.fetchedPageCount || 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentResultNumber", {
        get: function () {
            return this.fetchedResultNumber || 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentPageSize", {
        get: function () {
            var _a;
            var def = (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPageSize;
            if (this.selectedPageSize) {
                return this.selectedPageSize;
            }
            else if (this.defaultPageSize) {
                return this.defaultPageSize;
            }
            else if (this.currentPossiblePageSizes.length &&
                def &&
                this.currentPossiblePageSizes.indexOf(def) !== -1) {
                return def;
            }
            else if (this.currentPossiblePageSizes.length) {
                return this.currentPossiblePageSizes[0];
            }
            else if (def) {
                return def;
            }
            else {
                return 10;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentPageIndex", {
        get: function () {
            if (this.selectedPageIndex) {
                return this.selectedPageIndex;
            }
            else {
                return 0;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentSortColumn", {
        get: function () {
            var _this = this;
            if (this.selectedSortColumn) {
                // todo log if sorting column disappears
                return this.visibleColumns.find(function (c) { var _a; return c.name === ((_a = _this.selectedSortColumn) === null || _a === void 0 ? void 0 : _a.name); }) || null;
            }
            else if (this.defaultSortingColumn) {
                var foundColumn = this.visibleColumns.find(function (c) { return c.name === _this.defaultSortingColumn; });
                if (foundColumn) {
                    return foundColumn;
                }
                else {
                    // find first sortable column?
                    return this.visibleColumns.find(function (column) {
                        return MaisTableFormatterHelper.isSortable(column, _this.configurationService.getConfiguration());
                    }) || null;
                }
            }
            else {
                // find first sortable column?
                return this.visibleColumns.find(function (column) {
                    return MaisTableFormatterHelper.isSortable(column, _this.configurationService.getConfiguration());
                }) || null;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentSortDirection", {
        get: function () {
            if (this.selectedSortDirection) {
                return this.selectedSortDirection;
            }
            else if (this.defaultSortingDirection) {
                return this.defaultSortingDirection === MaisTableSortDirection.DESCENDING ?
                    MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
            }
            else {
                // return DEFAULT
                return MaisTableSortDirection.ASCENDING;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentData", {
        get: function () {
            return this.dataSnapshot || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "enumPages", {
        get: function () {
            var rangeStart = 1;
            var rangeEnd = 1;
            var rangeSelected = 1;
            var pages = [];
            var pageNum = this.currentPageCount;
            var currentIndex = this.currentPageIndex;
            var isSkipping = false;
            for (var i = 0; i < pageNum; i++) {
                if (Math.abs(i - currentIndex) <= (rangeSelected) || i <= (rangeStart - 1) || i >= (pageNum - rangeEnd)) {
                    isSkipping = false;
                    pages.push(i);
                }
                else {
                    if (!isSkipping) {
                        pages.push({
                            skip: true
                        });
                        isSkipping = true;
                    }
                }
            }
            return pages;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "activeColumnsCount", {
        get: function () {
            var output = this.visibleColumns.length;
            if (this.currentEnableSelection) {
                output++;
            }
            if (this.contextFilteringEnabledAndPossible) {
                output++;
            }
            if (this.rowExpansionEnabledAndPossible) {
                output++;
            }
            return output;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "noResults", {
        get: function () {
            if (this.paginationMode === MaisTablePaginationMethod.CLIENT) {
                return !this.currentData.length;
            }
            else {
                return !this.dataSnapshot.length;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "hideableColumns", {
        get: function () {
            var _this = this;
            return this.columns.filter(function (c) { return MaisTableFormatterHelper.isHideable(c, _this.configurationService.getConfiguration()); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "visibleColumns", {
        get: function () {
            var _this = this;
            return this.columns.filter(function (c) { return _this.checkedColumnsForVisualization.indexOf(c) !== -1; });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentContextFilters", {
        get: function () {
            return this.contextFilters || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentHeaderActions", {
        get: function () {
            return this.headerActions || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "currentActions", {
        get: function () {
            return this.contextActions || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "hasActions", {
        get: function () {
            return this.currentActions.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "searchQueryNeedApply", {
        get: function () {
            return (this.currentSearchQuery || '') !== (this.lastFetchedSearchQuery || '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "searchQueryActive", {
        get: function () {
            if (!this.currentSearchQuery) {
                return false;
            }
            if (!this.checkedColumnsForFiltering.length) {
                return false;
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "searchQueryMalformed", {
        get: function () {
            if (!this.currentSearchQuery) {
                return false;
            }
            if (!this.checkedColumnsForFiltering.length) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "filterableColumns", {
        get: function () {
            var _this = this;
            return this.columns.filter(function (c) { return _this.isFilterable(c); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyButtonActionsAllowed", {
        get: function () {
            var _this = this;
            return !!this.contextActions.find(function (a) { return _this.isContextActionAllowed(a); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyHeaderActionsAllowed", {
        get: function () {
            var _this = this;
            return !!this.headerActions.find(function (a) { return _this.isHeaderActionAllowed(a); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "configColumnVisibilityShowFixedColumns", {
        get: function () {
            var _a;
            return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.showFixedColumns;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "configFilteringShowUnfilterableColumns", {
        get: function () {
            var _a;
            return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.showUnfilterableColumns;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "acceptDrop", {
        get: function () {
            return this.currentEnableDrop;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "acceptDrag", {
        get: function () {
            return this.currentEnableDrag;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "referencedTable", {
        get: function () {
            var v = this.getReferencedTable();
            return v;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.getReferencedTable = function () {
        var e_2, _a, e_3, _b;
        if (this.dropConnectedTo && this.dropConnectedTo.length) {
            var connectedToList = (Array.isArray(this.dropConnectedTo)) ? this.dropConnectedTo : [this.dropConnectedTo];
            var found = [];
            try {
                for (var connectedToList_1 = __values(connectedToList), connectedToList_1_1 = connectedToList_1.next(); !connectedToList_1_1.done; connectedToList_1_1 = connectedToList_1.next()) {
                    var connectedToToken = connectedToList_1_1.value;
                    var registeredList = this.registry.get(connectedToToken);
                    if (registeredList && registeredList.length) {
                        try {
                            for (var registeredList_1 = (e_3 = void 0, __values(registeredList)), registeredList_1_1 = registeredList_1.next(); !registeredList_1_1.done; registeredList_1_1 = registeredList_1.next()) {
                                var regComp = registeredList_1_1.value;
                                found.push(regComp);
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (registeredList_1_1 && !registeredList_1_1.done && (_b = registeredList_1.return)) _b.call(registeredList_1);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (connectedToList_1_1 && !connectedToList_1_1.done && (_a = connectedToList_1.return)) _a.call(connectedToList_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
            if (found.length < 1) {
                return null;
            }
            else if (found.length > 1) {
                this.logger.error('MULTIPLE TABLE REFERENCED STILL ACTIVE', found);
                return null;
            }
            else {
                return found[0].component;
            }
        }
        else {
            return this;
        }
    };
    // user actions
    MaisTableComponent.prototype.handleItemDragged = function (event, from, to) {
        this.logger.debug('item dragged from table ' + this.currentTableId);
        this.itemDragged.emit({
            item: event.item.data,
            event: event,
            fromDataSnapshot: from.getDataSnapshot(),
            toDataSnapshot: to.getDataSnapshot(),
            fromComponent: from,
            toComponent: to
        });
    };
    MaisTableComponent.prototype.handleItemDropped = function (event) {
        var _this = this;
        this.logger.debug('DROP EVENT FROM TABLE ' + this.currentTableId, event);
        var droppedSource = this.registry.getSingle(event.previousContainer.id);
        var droppedTarget = this.registry.getSingle(event.container.id);
        if (!droppedTarget) {
            this.logger.warn('NO DROP TARGET FOUND');
            return;
        }
        if (!droppedTarget.acceptDrop) {
            this.logger.debug('skipping drop on container with acceptDrop = false');
            return;
        }
        if (droppedSource) {
            droppedSource.handleItemDragged(event, droppedSource, droppedTarget);
        }
        this.logger.debug('item dropped on table ' + droppedTarget.currentTableId);
        this.itemDropped.emit({
            item: event.item.data,
            event: event,
            fromDataSnapshot: droppedSource ? droppedSource.getDataSnapshot() : null,
            toDataSnapshot: droppedTarget.getDataSnapshot(),
            fromComponent: droppedSource,
            toComponent: droppedTarget
        });
        // I'm sorry
        this.cdr.detectChanges();
        this.ngZone.run(function () {
            _this.dataSnapshot = _this.dataSnapshot.map(function (o) { return o; });
            _this.cdr.detectChanges();
            setTimeout(function () { return _this.forceReRender = true; });
            setTimeout(function () { return _this.forceReRender = false; });
            _this.handleMatTableDataSnapshotChanged();
        });
    };
    MaisTableComponent.prototype.applySelectedFilter = function () {
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    };
    MaisTableComponent.prototype.clickOnRowExpansion = function (item) {
        if (!this.rowExpansionEnabledAndPossible) {
            return;
        }
        if (!this.isExpandable(item)) {
            return;
        }
        if (this.isExpanded(item)) {
            this.expandedItems.splice(this.expandedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultipleRowExpansion) {
                this.expandedItems = [];
            }
            this.expandedItems.push(item);
        }
        this.handleMatTableDataSnapshotChanged();
        this.statusChanged();
    };
    MaisTableComponent.prototype.clickOnContextAction = function (action) {
        if (!this.currentEnableContextActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isContextActionAllowed(action)) {
            return;
        }
        var dispatchContext = {
            action: action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    };
    MaisTableComponent.prototype.clickOnPageSize = function (pageSize) {
        this.selectedPageSize = pageSize;
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    };
    MaisTableComponent.prototype.clickOnHeaderAction = function (action) {
        if (!this.currentEnableHeaderActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isHeaderActionAllowed(action)) {
            return;
        }
        var dispatchContext = {
            action: action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    };
    MaisTableComponent.prototype.searchQueryFocusOut = function () {
        var _this = this;
        var _a;
        var comp = this;
        setTimeout(function () {
            var _a, _b;
            if ((comp.selectedSearchQuery || '') !== (comp.lastFetchedSearchQuery || '')) {
                if (comp.selectedSearchQuery) {
                    if ((_a = _this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadOnQueryChange) {
                        comp.applySelectedFilter();
                    }
                    else {
                        // comp.selectedSearchQuery = comp.lastFetchedSearchQuery;
                    }
                }
                else {
                    if ((_b = _this.configurationService.getConfiguration().filtering) === null || _b === void 0 ? void 0 : _b.autoReloadOnQueryClear) {
                        comp.applySelectedFilter();
                    }
                }
            }
        }, (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadTimeout);
    };
    MaisTableComponent.prototype.switchToPage = function (pageIndex, context) {
        if (this.currentPageIndex === pageIndex) {
            return;
        }
        if (!context) {
            context = { reason: MaisTableReloadReason.USER };
        }
        this.setPage(pageIndex);
        this.statusChanged();
        this.reload(context);
    };
    MaisTableComponent.prototype.clickOnColumn = function (column) {
        if (!MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) {
            return;
        }
        var sortColumn = this.currentSortColumn;
        var sortDirection = this.currentSortDirection;
        if (sortColumn && sortColumn.name === column.name) {
            this.selectedSortDirection = (sortDirection === MaisTableSortDirection.DESCENDING ?
                MaisTableSortDirection.ASCENDING : MaisTableSortDirection.DESCENDING);
        }
        else {
            this.selectedSortColumn = column;
            this.selectedSortDirection = MaisTableSortDirection.ASCENDING;
        }
        this.emitSortChanged();
        // forza ritorno alla prima pagina
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    };
    // parsing functions
    MaisTableComponent.prototype.getItemIdentifier = function (item) {
        if (this.itemTrackingEnabledAndPossible) {
            if (this.itemIdentifier.extract) {
                return this.itemIdentifier.extract(item);
            }
            else {
                return MaisTableFormatterHelper.getPropertyValue(item, this.itemIdentifier);
            }
        }
        else {
            return null;
        }
    };
    MaisTableComponent.prototype.getCurrentLocale = function () {
        return this.translateService.getDefaultLang();
    };
    MaisTableComponent.prototype.extractValue = function (row, column) {
        return MaisTableFormatterHelper.extractValue(row, column, this.getCurrentLocale());
    };
    MaisTableComponent.prototype.resolveLabel = function (column) {
        return this.resolveLabelOrFixed(column.labelKey, column.label);
    };
    MaisTableComponent.prototype.resolveLabelOrFixed = function (key, fixed) {
        if (key) {
            return this.translateService.instant(key);
        }
        else if (fixed) {
            return fixed;
        }
        else {
            return null;
        }
    };
    MaisTableComponent.prototype.isCurrentSortingColumn = function (column, direction) {
        if (direction === void 0) { direction = null; }
        var sortColumn = this.currentSortColumn;
        if (sortColumn && column && column.name === sortColumn.name) {
            if (!direction) {
                return true;
            }
            return direction === this.currentSortDirection;
        }
        else {
            return false;
        }
    };
    MaisTableComponent.prototype.isDefaultVisibleColumn = function (column) {
        return MaisTableFormatterHelper.isDefaultVisibleColumn(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isDefaultFilterColumn = function (column) {
        return MaisTableFormatterHelper.isDefaultFilterColumn(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isExpandable = function (row) {
        return this.currentEnableRowExpansion &&
            (!this.expandableStatusProvider || this.expandableStatusProvider(row, this.statusSnapshot));
    };
    MaisTableComponent.prototype.isHideable = function (column) {
        return MaisTableFormatterHelper.isHideable(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isSortable = function (column) {
        return MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isFilterable = function (column) {
        return MaisTableFormatterHelper.isFilterable(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.hasLabel = function (column) {
        return MaisTableFormatterHelper.hasLabel(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isLabelVisibleInTable = function (column) {
        return MaisTableFormatterHelper.isLabelVisibleInTable(column, this.configurationService.getConfiguration());
    };
    MaisTableComponent.prototype.isContextActionAllowed = function (action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultContextActionActivationCondition);
    };
    MaisTableComponent.prototype.isHeaderActionAllowed = function (action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultHeaderActionActivationCondition);
    };
    MaisTableComponent.prototype.isActionAllowed = function (action, def) {
        var actCond = action.activationCondition || def;
        if (actCond === MaisTableActionActivationCondition.ALWAYS) {
            return true;
        }
        else if (actCond === MaisTableActionActivationCondition.NEVER) {
            return false;
        }
        else if (actCond === MaisTableActionActivationCondition.DYNAMIC) {
            // delega decisione a provider esterno
            if (!this.actionStatusProvider) {
                this.logger.error('Action with dynamic enabling but no actionStatusProvider provided');
                return false;
            }
            return this.actionStatusProvider(action, this.statusSnapshot || null);
        }
        var numSelected = this.checkedItems.length;
        if (actCond === MaisTableActionActivationCondition.SINGLE_SELECTION) {
            return numSelected === 1;
        }
        else if (actCond === MaisTableActionActivationCondition.MULTIPLE_SELECTION) {
            return numSelected > 0;
        }
        else if (actCond === MaisTableActionActivationCondition.NO_SELECTION) {
            return numSelected < 1;
        }
        else {
            throw new Error('Unknown action activation condition: ' + actCond);
        }
    };
    // gestione delle righe espanse
    MaisTableComponent.prototype.isExpanded = function (item) {
        return this.expandedItems.indexOf(item) !== -1;
    };
    // checkbox select per items
    MaisTableComponent.prototype.isChecked = function (item) {
        return this.checkedItems.indexOf(item) !== -1;
    };
    Object.defineProperty(MaisTableComponent.prototype, "allChecked", {
        get: function () {
            var e_4, _a;
            var dataItems = this.currentData;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_1 = __values(dataItems), dataItems_1_1 = dataItems_1.next(); !dataItems_1_1.done; dataItems_1_1 = dataItems_1.next()) {
                    var item = dataItems_1_1.value;
                    if (this.checkedItems.indexOf(item) === -1) {
                        return false;
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (dataItems_1_1 && !dataItems_1_1.done && (_a = dataItems_1.return)) _a.call(dataItems_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyChecked", {
        get: function () {
            var e_5, _a;
            var dataItems = this.currentData;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_2 = __values(dataItems), dataItems_2_1 = dataItems_2.next(); !dataItems_2_1.done; dataItems_2_1 = dataItems_2.next()) {
                    var item = dataItems_2_1.value;
                    if (this.checkedItems.indexOf(item) !== -1) {
                        return true;
                    }
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (dataItems_2_1 && !dataItems_2_1.done && (_a = dataItems_2.return)) _a.call(dataItems_2);
                }
                finally { if (e_5) throw e_5.error; }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "noneChecked", {
        get: function () {
            return !this.anyChecked;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.toggleChecked = function (item) {
        if (this.isChecked(item)) {
            this.checkedItems.splice(this.checkedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultiSelect) {
                this.checkedItems = [];
            }
            this.checkedItems.push(item);
        }
        this.statusChanged();
        this.emitSelectionChanged();
    };
    MaisTableComponent.prototype.toggleAllChecked = function () {
        var e_6, _a;
        if (!this.currentEnableSelectAll) {
            return;
        }
        if (!this.currentEnableMultiSelect) {
            return;
        }
        if (this.allChecked) {
            this.checkedItems = [];
        }
        else {
            this.checkedItems = [];
            try {
                for (var _b = __values(this.currentData), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var el = _c.value;
                    this.checkedItems.push(el);
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_6) throw e_6.error; }
            }
        }
        this.statusChanged();
        this.emitSelectionChanged();
    };
    // checkbox select per filtering columns
    MaisTableComponent.prototype.isColumnCheckedForFiltering = function (item) {
        return this.checkedColumnsForFiltering.indexOf(item) !== -1;
    };
    Object.defineProperty(MaisTableComponent.prototype, "allFilteringColumnsChecked", {
        get: function () {
            var e_7, _a;
            var dataItems = this.filterableColumns;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_3 = __values(dataItems), dataItems_3_1 = dataItems_3.next(); !dataItems_3_1.done; dataItems_3_1 = dataItems_3.next()) {
                    var item = dataItems_3_1.value;
                    if (this.checkedColumnsForFiltering.indexOf(item) === -1) {
                        return false;
                    }
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (dataItems_3_1 && !dataItems_3_1.done && (_a = dataItems_3.return)) _a.call(dataItems_3);
                }
                finally { if (e_7) throw e_7.error; }
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyFilteringColumnsChecked", {
        get: function () {
            var e_8, _a;
            var dataItems = this.filterableColumns;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_4 = __values(dataItems), dataItems_4_1 = dataItems_4.next(); !dataItems_4_1.done; dataItems_4_1 = dataItems_4.next()) {
                    var item = dataItems_4_1.value;
                    if (this.checkedColumnsForFiltering.indexOf(item) !== -1) {
                        return true;
                    }
                }
            }
            catch (e_8_1) { e_8 = { error: e_8_1 }; }
            finally {
                try {
                    if (dataItems_4_1 && !dataItems_4_1.done && (_a = dataItems_4.return)) _a.call(dataItems_4);
                }
                finally { if (e_8) throw e_8.error; }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "noFilteringColumnshecked", {
        get: function () {
            return !this.anyFilteringColumnsChecked;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.toggleFilteringColumnChecked = function (item) {
        if (!this.currentEnableFiltering) {
            return;
        }
        if (!this.isFilterable(item)) {
            return;
        }
        if (this.isColumnCheckedForFiltering(item)) {
            this.checkedColumnsForFiltering.splice(this.checkedColumnsForFiltering.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForFiltering.push(item);
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    };
    MaisTableComponent.prototype.toggleAllFilteringColumnsChecked = function () {
        var e_9, _a;
        if (!this.currentEnableFiltering) {
            return;
        }
        if (this.allFilteringColumnsChecked) {
            this.checkedColumnsForFiltering = [];
        }
        else {
            this.checkedColumnsForFiltering = [];
            try {
                for (var _b = __values(this.filterableColumns), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var el = _c.value;
                    this.checkedColumnsForFiltering.push(el);
                }
            }
            catch (e_9_1) { e_9 = { error: e_9_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_9) throw e_9.error; }
            }
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    };
    // checkbox select per visible columns
    MaisTableComponent.prototype.isColumnCheckedForVisualization = function (item) {
        return this.checkedColumnsForVisualization.indexOf(item) !== -1;
    };
    Object.defineProperty(MaisTableComponent.prototype, "allVisibleColumnsChecked", {
        get: function () {
            var e_10, _a;
            var dataItems = this.hideableColumns;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_5 = __values(dataItems), dataItems_5_1 = dataItems_5.next(); !dataItems_5_1.done; dataItems_5_1 = dataItems_5.next()) {
                    var item = dataItems_5_1.value;
                    if (this.checkedColumnsForVisualization.indexOf(item) === -1) {
                        return false;
                    }
                }
            }
            catch (e_10_1) { e_10 = { error: e_10_1 }; }
            finally {
                try {
                    if (dataItems_5_1 && !dataItems_5_1.done && (_a = dataItems_5.return)) _a.call(dataItems_5);
                }
                finally { if (e_10) throw e_10.error; }
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyVisibleColumnsChecked", {
        get: function () {
            var e_11, _a;
            var dataItems = this.hideableColumns;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_6 = __values(dataItems), dataItems_6_1 = dataItems_6.next(); !dataItems_6_1.done; dataItems_6_1 = dataItems_6.next()) {
                    var item = dataItems_6_1.value;
                    if (this.checkedColumnsForVisualization.indexOf(item) !== -1) {
                        return true;
                    }
                }
            }
            catch (e_11_1) { e_11 = { error: e_11_1 }; }
            finally {
                try {
                    if (dataItems_6_1 && !dataItems_6_1.done && (_a = dataItems_6.return)) _a.call(dataItems_6);
                }
                finally { if (e_11) throw e_11.error; }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "noVisibleColumnshecked", {
        get: function () {
            return !this.anyVisibleColumnsChecked;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.toggleVisibleColumnChecked = function (item) {
        if (!MaisTableFormatterHelper.isHideable(item, this.configurationService.getConfiguration()) || !this.currentEnableColumnsSelection) {
            return;
        }
        if (this.isColumnCheckedForVisualization(item)) {
            this.checkedColumnsForVisualization.splice(this.checkedColumnsForVisualization.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForVisualization.push(item);
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    };
    MaisTableComponent.prototype.toggleAllVisibleColumnsChecked = function () {
        var e_12, _a;
        var _this = this;
        if (!this.currentEnableColumnsSelection) {
            return;
        }
        if (this.allVisibleColumnsChecked) {
            this.checkedColumnsForVisualization = this.columns.filter(function (c) {
                return !MaisTableFormatterHelper.isHideable(c, _this.configurationService.getConfiguration());
            });
        }
        else {
            this.checkedColumnsForVisualization = [];
            try {
                for (var _b = __values(this.columns), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var el = _c.value;
                    this.checkedColumnsForVisualization.push(el);
                }
            }
            catch (e_12_1) { e_12 = { error: e_12_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_12) throw e_12.error; }
            }
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    };
    // checkbox select per filtri custom
    MaisTableComponent.prototype.isContextFilterChecked = function (item) {
        return this.checkedContextFilters.indexOf(item) !== -1;
    };
    Object.defineProperty(MaisTableComponent.prototype, "allContextFilterChecked", {
        get: function () {
            var e_13, _a;
            var dataItems = this.currentContextFilters;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_7 = __values(dataItems), dataItems_7_1 = dataItems_7.next(); !dataItems_7_1.done; dataItems_7_1 = dataItems_7.next()) {
                    var item = dataItems_7_1.value;
                    if (this.checkedContextFilters.indexOf(item) === -1) {
                        return false;
                    }
                }
            }
            catch (e_13_1) { e_13 = { error: e_13_1 }; }
            finally {
                try {
                    if (dataItems_7_1 && !dataItems_7_1.done && (_a = dataItems_7.return)) _a.call(dataItems_7);
                }
                finally { if (e_13) throw e_13.error; }
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "anyContextFilterChecked", {
        get: function () {
            var e_14, _a;
            var dataItems = this.currentContextFilters;
            if (!dataItems.length) {
                return false;
            }
            try {
                for (var dataItems_8 = __values(dataItems), dataItems_8_1 = dataItems_8.next(); !dataItems_8_1.done; dataItems_8_1 = dataItems_8.next()) {
                    var item = dataItems_8_1.value;
                    if (this.checkedContextFilters.indexOf(item) !== -1) {
                        return true;
                    }
                }
            }
            catch (e_14_1) { e_14 = { error: e_14_1 }; }
            finally {
                try {
                    if (dataItems_8_1 && !dataItems_8_1.done && (_a = dataItems_8.return)) _a.call(dataItems_8);
                }
                finally { if (e_14) throw e_14.error; }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "noContextFiltersChecked", {
        get: function () {
            return !this.anyContextFilterChecked;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.toggleContextFilterChecked = function (item) {
        if (!this.currentEnableContextFiltering) {
            return;
        }
        if (this.isContextFilterChecked(item)) {
            this.checkedContextFilters.splice(this.checkedContextFilters.indexOf(item), 1);
        }
        else {
            this.checkedContextFilters.push(item);
            if (item.group) {
                this.checkedContextFilters = this.checkedContextFilters.filter(function (o) {
                    return (o.name === item.name) || (!o.group) || (o.group !== item.group);
                });
            }
        }
        this.emitContextFiltersSelectionChanged();
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    };
    MaisTableComponent.prototype.buildStatusSnapshot = function () {
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(function (c) { return c.name; }),
            visibleColumns: this.visibleColumns.map(function (c) { return c.name; }),
            contextFilters: this.checkedContextFilters.map(function (c) { return c.name; }),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItems: !this.currentEnableSelection ? [] : this.checkedItems.map(function (v) { return v; }),
            expandedItems: !this.currentEnableRowExpansion ? [] : this.expandedItems.map(function (v) { return v; })
        };
    };
    MaisTableComponent.prototype.buildPersistableStatusSnapshot = function () {
        var _this = this;
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(function (c) { return c.name; }),
            visibleColumns: this.visibleColumns.map(function (c) { return c.name; }),
            contextFilters: this.checkedContextFilters.map(function (c) { return c.name; }),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.checkedItems.map(function (item) { return _this.getItemIdentifier(item); }).filter(function (v) { return !!v; }),
            expandedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.expandedItems.map(function (item) { return _this.getItemIdentifier(item); }).filter(function (v) { return !!v; })
        };
    };
    MaisTableComponent.prototype.statusChanged = function () {
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        this.logger.trace('table status changed', this.persistableStatusSnapshot);
        this.emitStatusChanged();
    };
    MaisTableComponent.prototype.persistStatusToStore = function () {
        var _this = this;
        if (!this.statusSnapshot) {
            return throwError('No status to save');
        }
        return new Observable(function (subscriber) {
            if (_this.storePersistenceEnabledAndPossible) {
                _this.logger.trace('passing status to persistence store');
                if (_this.storeAdapter.save) {
                    _this.storeAdapter.save({
                        status: _this.persistableStatusSnapshot
                    }).subscribe(function (result) {
                        _this.logger.trace('saved status snapshot to persistence store');
                        subscriber.next();
                        subscriber.complete();
                    }, function (failure) {
                        _this.logger.warn('failed to save status snapshot to persistence store', failure);
                        subscriber.error(failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No save function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next();
            subscriber.complete();
        });
    };
    MaisTableComponent.prototype.loadStatusFromStore = function () {
        var _this = this;
        return new Observable(function (subscriber) {
            if (_this.storePersistenceEnabledAndPossible) {
                _this.logger.trace('fetching status from persistence store');
                if (_this.storeAdapter.load) {
                    _this.storeAdapter.load().subscribe(function (result) {
                        _this.logger.trace('fetched status snapshot from persistence store');
                        subscriber.next(result);
                        subscriber.complete();
                    }, function (failure) {
                        subscriber.error(failure);
                        _this.logger.warn('failed to fetch status snapshot from persistence store', failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No load function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next(null);
            subscriber.complete();
        });
    };
    Object.defineProperty(MaisTableComponent.prototype, "dragInProgress", {
        get: function () {
            return ($('.cdk-drag-preview:visible').length +
                $('.cdk-drag-placeholder:visible').length +
                $('.cdk-drop-list-dragging:visible').length +
                $('.cdk-drop-list-receiving:visible').length) > 0;
        },
        enumerable: true,
        configurable: true
    });
    // event emitters
    MaisTableComponent.prototype.emitSelectionChanged = function () {
        this.selectionChange.emit(this.checkedItems);
    };
    MaisTableComponent.prototype.emitSortChanged = function () {
        this.sortChange.emit({
            column: this.currentSortColumn,
            direction: this.currentSortDirection
        });
    };
    MaisTableComponent.prototype.emitPageChanged = function () {
        this.pageChange.emit(this.selectedPageIndex);
    };
    MaisTableComponent.prototype.emitFilteringColumnsSelectionChanged = function () {
        this.filteringColumnsChange.emit(this.checkedColumnsForFiltering);
    };
    MaisTableComponent.prototype.emitVisibleColumnsSelectionChanged = function () {
        this.visibleColumnsChange.emit(this.checkedColumnsForVisualization);
    };
    MaisTableComponent.prototype.emitContextFiltersSelectionChanged = function () {
        this.contextFiltersChange.emit(this.checkedContextFilters);
    };
    MaisTableComponent.prototype.emitStatusChanged = function () {
        if (this.statusSnapshot) {
            this.statusChange.emit(this.statusSnapshot);
        }
    };
    Object.defineProperty(MaisTableComponent.prototype, "matTableData", {
        // MatTable adapters
        get: function () {
            return this.dataSnapshot;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "matTableVisibleColumnDefs", {
        get: function () {
            var o = [];
            if (this.rowExpansionEnabledAndPossible) {
                o.push('internal___col_row_expansion');
            }
            if (this.currentEnableSelection) {
                o.push('internal___col_row_selection');
            }
            if (this.contextFilteringEnabledAndPossible) {
                o.push('internal___col_context_filtering');
            }
            this.visibleColumns.map(function (c) { return c.name; }).forEach(function (c) { return o.push(c); });
            return o;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "matTableVisibleColumns", {
        get: function () {
            return this.visibleColumns;
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.prototype.handleMatTableDataSnapshotChanged = function () {
        var _this = this;
        var rows = [];
        this.dataSnapshot.forEach(function (element) {
            var detailRow = __assign({ ___detailRowContent: true, ___parentRow: element }, element);
            element.___detailRow = detailRow;
            rows.push(element, detailRow);
        });
        // TODO : BUILD STATIC ROW WITH DATA EXTRACTORS!
        this.logger.trace('emitting static rows for mat-table ' + this.currentTableId, rows);
        setTimeout(function () {
            var _a;
            (_a = _this.matTableDataObservable) === null || _a === void 0 ? void 0 : _a.next(rows);
        });
    };
    Object.defineProperty(MaisTableComponent.prototype, "compatibilityModeForMainTable", {
        get: function () {
            if (!this.isIE) {
                return false;
            }
            else if (this.currentEnableDrop) {
                return true;
            }
            else {
                return true;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaisTableComponent.prototype, "compatibilityModeForDropDowns", {
        get: function () {
            return true;
            if (!this.isIE) {
                return false;
            }
            else {
                return true;
            }
        },
        enumerable: true,
        configurable: true
    });
    MaisTableComponent.counter = 0;
    /** @nocollapse */ MaisTableComponent.ɵfac = function MaisTableComponent_Factory(t) { return new (t || MaisTableComponent)(i0.ɵɵdirectiveInject(i1.TranslateService), i0.ɵɵdirectiveInject(i2.MaisTableService), i0.ɵɵdirectiveInject(i2.MaisTableRegistryService), i0.ɵɵdirectiveInject(i0.ChangeDetectorRef), i0.ɵɵdirectiveInject(i0.NgZone)); };
    /** @nocollapse */ MaisTableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MaisTableComponent, selectors: [["mais-table"]], contentQueries: function MaisTableComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuery(dirIndex, _c0, true);
            i0.ɵɵcontentQuery(dirIndex, _c1, true);
            i0.ɵɵcontentQuery(dirIndex, _c2, true);
            i0.ɵɵcontentQuery(dirIndex, _c3, true);
            i0.ɵɵcontentQuery(dirIndex, _c4, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.cellTemplate = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.actionsCaptionTemplate = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.rowDetailTemplate = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.dragTemplate = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.dropTemplate = _t.first);
        } }, viewQuery: function MaisTableComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(_c5, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.table = _t.first);
        } }, inputs: { data: "data", dataProvider: "dataProvider", columns: "columns", defaultSortingColumn: "defaultSortingColumn", defaultSortingDirection: "defaultSortingDirection", defaultPageSize: "defaultPageSize", possiblePageSize: "possiblePageSize", tableId: "tableId", dropConnectedTo: "dropConnectedTo", paginationMode: "paginationMode", enablePagination: "enablePagination", enableSelection: "enableSelection", enableContextFiltering: "enableContextFiltering", enableSelectAll: "enableSelectAll", enableMultiSelect: "enableMultiSelect", enableFiltering: "enableFiltering", enableColumnsSelection: "enableColumnsSelection", enableContextActions: "enableContextActions", enableHeaderActions: "enableHeaderActions", enableRowExpansion: "enableRowExpansion", enablePageSizeSelect: "enablePageSizeSelect", enableMultipleRowExpansion: "enableMultipleRowExpansion", enableItemTracking: "enableItemTracking", enableStorePersistence: "enableStorePersistence", enableDrag: "enableDrag", enableDrop: "enableDrop", contextActions: "contextActions", headerActions: "headerActions", contextFilters: "contextFilters", contextFilteringPrompt: "contextFilteringPrompt", actionStatusProvider: "actionStatusProvider", expandableStatusProvider: "expandableStatusProvider", storeAdapter: "storeAdapter", itemIdentifier: "itemIdentifier", refreshStrategy: "refreshStrategy", refreshInterval: "refreshInterval", refreshEmitter: "refreshEmitter", refreshIntervalInBackground: "refreshIntervalInBackground", refreshOnPushInBackground: "refreshOnPushInBackground" }, outputs: { pageChange: "pageChange", sortChange: "sortChange", selectionChange: "selectionChange", filteringColumnsChange: "filteringColumnsChange", visibleColumnsChange: "visibleColumnsChange", contextFiltersChange: "contextFiltersChange", statusChange: "statusChange", itemDragged: "itemDragged", itemDropped: "itemDropped", action: "action" }, features: [i0.ɵɵNgOnChangesFeature()], decls: 5, vars: 4, consts: [[1, "mais-table"], ["class", "container-full px-4 py-5 table-actions table-actions--header", 4, "ngIf"], [4, "ngIf"], ["class", "container-full px-4 py-4 table-actions table-actions--footer", 4, "ngIf"], [1, "container-full", "px-4", "py-5", "table-actions", "table-actions--header"], [1, "row"], ["class", "col-4", 4, "ngIf"], ["class", "col-3", 4, "ngIf"], [1, "col-5", "text-right"], [3, "ngTemplateOutlet", "ngTemplateOutletContext"], ["class", "btn-group", "ngbDropdown", "", 4, "ngIf"], [1, "col-4"], [3, "submit"], [1, "input-group"], ["type", "text", "name", "selectedSearchQuery", "aria-label", "Search", 1, "form-control", 3, "placeholder", "ngModel", "ngModelChange", "focusout"], ["ngbDropdown", "", 1, "input-group-append", 3, "autoClose"], ["type", "submit", 1, "btn", 3, "click"], [1, "fas", "fa-search"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 1, "dropdown-item"], [1, "dropdown-header"], [1, "form-group", "clickable"], [1, "form-check"], ["type", "checkbox", 3, "checked", "change"], [3, "click"], [1, "dropdown-divider"], [4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", "class", "dropdown-item", 3, "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "change", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled"], ["ngbDropdownItem", "", 3, "click"], [3, "icon", 4, "ngIf"], [3, "icon"], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngIf"], [1, "col-3"], ["ngbDropdown", "", 1, "btn-group", 3, "autoClose"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"], ["ngbDropdownItem", "", 1, "dropdown-header"], ["type", "button", 3, "class", "disabled", "click", 4, "ngFor", "ngForOf"], ["type", "button", 3, "disabled", "click"], ["ngbDropdown", "", 1, "btn-group"], ["ngbDropdownToggle", "", 1, "btn", "btn-light", "dropdown-toggle", 3, "disabled"], ["ngbDropdownMenu", ""], ["ngbDropdownItem", ""], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "table-responsive"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], [1, "d-none"], [1, ""], ["scope", "row", "class", "small-as-possible", 4, "ngIf"], ["scope", "row", "style", "width: 1%;", "ngbDropdown", "", 3, "border-primary", "autoClose", 4, "ngIf"], ["scope", "row", 1, "small-as-possible"], ["scope", "row", "ngbDropdown", "", 2, "width", "1%", 3, "autoClose"], ["ngbDropdownToggle", "", 1, "form-check", "clickable"], [1, "dropdown", "show", "d-inline-block", "float-right"], ["class", "dropdown-item", "ngbDropdownItem", "", 4, "ngFor", "ngForOf"], [1, "clickable", 3, "click"], ["scope", "col", 3, "click"], ["type", "button", 1, "order-btn", "btn", "btn-none", 3, "click"], [1, "fas", "fa-long-arrow-alt-up"], [1, "start-row-hook", "d-none"], ["cdkDrag", "", 1, "data-row", 3, "cdkDragData", "cdkDragDisabled"], ["scope", "row", 4, "ngIf"], ["class", "drop-container", 3, "hidden", 4, "cdkDragPlaceholder"], ["class", "drag-container", 4, "cdkDragPreview"], ["class", "detail-row", 4, "ngIf"], ["scope", "row"], ["class", "clickable", 3, "icon", "click", 4, "ngIf"], [1, "clickable", 3, "icon", "click"], [1, "drop-container", 3, "hidden"], [1, "drop-cell"], [1, "drop-container"], [1, "drag-container"], [1, "detail-row"], [1, "row-expansion-container"], [1, "message-row", "no-results-row"], [1, "text-center"], [1, "message-row", "fetching-row"], ["class", "table-responsive", 4, "ngIf"], ["class", "table table-dark table-hover", "cdkDropList", "", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "nodrop", "acceptdrop", "cdkDropListDropped", 4, "ngIf"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], ["renderedMatTable", ""], [3, "matColumnDef"], ["class", "maissize-min", 4, "matHeaderCellDef"], ["class", "maissize-min", 4, "matCellDef"], [4, "matCellDef"], [3, "matColumnDef", 4, "ngFor", "ngForOf"], [4, "matHeaderRowDef"], ["class", "element-row data-row", "cdkDrag", "", 3, "expanded", "row-selected", "cdkDragData", "cdkDragDisabled", 4, "matRowDef", "matRowDefColumns"], ["style", "overflow: hidden", 4, "matRowDef", "matRowDefColumns", "matRowDefWhen"], [1, "maissize-min"], ["ngbDropdown", "", 3, "autoClose", "container"], [3, "class", 4, "matHeaderCellDef"], [3, "class", 4, "matCellDef"], ["cdkDrag", "", 1, "element-row", "data-row", 3, "cdkDragData", "cdkDragDisabled"], [2, "overflow", "hidden"], [1, "alert", "alert-primary", "text-center"], [1, "container-full", "px-4", "py-4", "table-actions", "table-actions--footer"], [1, "col-7"], [1, "inline-block", "inline-block-bottom"], [1, "pagination-caption"], ["aria-label", "pagination", 1, "pagination-links"], [1, "pagination"], [1, "page-item", "clickable"], [1, "page-link", 3, "click"], [1, "inline-block", "inline-block-bottom", "pl-2"], ["ngbDropdown", "", 4, "ngIf"], ["class", "page-item clickable", 3, "active", 4, "ngIf"], ["class", "page-item", 4, "ngIf"], ["class", "sr-only", 4, "ngIf"], [1, "sr-only"], [1, "page-item"], [1, "page-link"], ["ngbDropdown", ""], ["ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"]], template: function MaisTableComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵtemplate(1, MaisTableComponent_div_1_Template, 12, 9, "div", 1);
            i0.ɵɵtemplate(2, MaisTableComponent_ng_container_2_Template, 15, 19, "ng-container", 2);
            i0.ɵɵtemplate(3, MaisTableComponent_ng_container_3_Template, 2, 1, "ng-container", 2);
            i0.ɵɵtemplate(4, MaisTableComponent_div_4_Template, 10, 6, "div", 3);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.columnSelectionPossibleAndAllowed || ctx.filteringPossibleAndAllowed || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.compatibilityModeForMainTable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.compatibilityModeForMainTable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.currentEnablePagination || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
        } }, directives: [i3.NgIf, i3.NgTemplateOutlet, i4.NgbDropdown, i4.NgbDropdownToggle, i4.NgbDropdownMenu, i4.NgbDropdownItem, i3.NgForOf, i5.FaIconComponent, i6.CdkDropList, i6.CdkDrag, i6.CdkDragPlaceholder, i6.CdkDragPreview, i7.MatTable, i7.MatColumnDef, i7.MatHeaderCellDef, i7.MatCellDef, i7.MatHeaderRowDef, i7.MatRowDef, i7.MatHeaderCell, i7.MatCell, i7.MatHeaderRow, i7.MatRow], pipes: [i1.TranslatePipe], styles: [".message-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding:2em}.clickable[_ngcontent-%COMP%]{cursor:pointer}.inline-block[_ngcontent-%COMP%]{display:inline-block}.inline-block-bottom[_ngcontent-%COMP%]{vertical-align:bottom}.small-as-possible[_ngcontent-%COMP%]{width:1%}.detail-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:2em}.row-selected[_ngcontent-%COMP%]{background-color:#bc001622}.row-selected[_ngcontent-%COMP%]:hover{background-color:#77464755!important}.drag-container[_ngcontent-%COMP%], .drop-container[_ngcontent-%COMP%]{min-height:60px;padding:1em;background:#77464755;border:3px dotted #25161755}.nodrop[_ngcontent-%COMP%]   .drop-cell[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-row[_ngcontent-%COMP%]{display:none!important}.cdk-drop-list-dragging.acceptdrop[_ngcontent-%COMP%]   td[_ngcontent-%COMP%], th[_ngcontent-%COMP%]{border:none}.mat-header-cell[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{padding:0;margin:0}.mat-cell.maissize-min[_ngcontent-%COMP%], .mat-cell.maissize-xxs[_ngcontent-%COMP%], .mat-header-cell.maissize-min[_ngcontent-%COMP%], .mat-header-cell.maissize-xxs[_ngcontent-%COMP%]{flex:0 0 2%;min-width:3em}.mat-cell.maissize-xs[_ngcontent-%COMP%], .mat-header-cell.maissize-xs[_ngcontent-%COMP%]{flex:0 0 3%;min-width:5em}.mat-cell.maissize-s[_ngcontent-%COMP%], .mat-header-cell.maissize-s[_ngcontent-%COMP%]{flex:0 0 3%;min-width:8em}.mat-cell.maissize-m[_ngcontent-%COMP%], .mat-header-cell.maissize-m[_ngcontent-%COMP%]{flex:0 0 3%;min-width:12em}.mat-cell.maissize-l[_ngcontent-%COMP%], .mat-header-cell.maissize-l[_ngcontent-%COMP%]{flex:0 0 3%;min-width:18em}.mat-cell.maissize-xl[_ngcontent-%COMP%], .mat-header-cell.maissize-xl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:25em}.mat-cell.maissize-xxl[_ngcontent-%COMP%], .mat-header-cell.maissize-xxl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:30em}.mat-table[_ngcontent-%COMP%]{border-collapse:collapse}.mat-header-row[_ngcontent-%COMP%], .mat-row[_ngcontent-%COMP%], .mat-table[_ngcontent-%COMP%]{border-color:gray}", "mat-table[_ngcontent-%COMP%]{display:block}mat-header-row[_ngcontent-%COMP%]{min-height:56px}mat-footer-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{min-height:48px}mat-footer-row[_ngcontent-%COMP%], mat-header-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{display:flex;border-width:0 0 1px;border-style:solid;align-items:center;box-sizing:border-box}mat-footer-row[_ngcontent-%COMP%]::after, mat-header-row[_ngcontent-%COMP%]::after, mat-row[_ngcontent-%COMP%]::after{display:inline-block;min-height:inherit;content:\"\"}mat-cell[_ngcontent-%COMP%]:first-of-type, mat-footer-cell[_ngcontent-%COMP%]:first-of-type, mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}mat-cell[_ngcontent-%COMP%]:last-of-type, mat-footer-cell[_ngcontent-%COMP%]:last-of-type, mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}mat-cell[_ngcontent-%COMP%], mat-footer-cell[_ngcontent-%COMP%], mat-header-cell[_ngcontent-%COMP%]{flex:1;display:flex;align-items:center;overflow:hidden;word-wrap:break-word;min-height:inherit}table.mat-table[_ngcontent-%COMP%]{border-spacing:0}tr.mat-header-row[_ngcontent-%COMP%]{height:56px}tr.mat-footer-row[_ngcontent-%COMP%], tr.mat-row[_ngcontent-%COMP%]{height:48px}th.mat-header-cell[_ngcontent-%COMP%]{text-align:left}[dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]{text-align:right}td.mat-cell[_ngcontent-%COMP%], td.mat-footer-cell[_ngcontent-%COMP%], th.mat-header-cell[_ngcontent-%COMP%]{padding:0;border-bottom-width:1px;border-bottom-style:solid}td.mat-cell[_ngcontent-%COMP%]:first-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}td.mat-cell[_ngcontent-%COMP%]:last-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}"], data: { animation: [
                trigger('detailExpand', [
                    state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                    state('expanded', style({ height: '*', visibility: 'visible' })),
                    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                ]),
            ] } });
    return MaisTableComponent;
}());
export { MaisTableComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableComponent, [{
        type: Component,
        args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mais-table',
                templateUrl: './mais-table.component.html',
                styleUrls: ['./mais-table.component.scss', './mais-table-porting.css'],
                animations: [
                    trigger('detailExpand', [
                        state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                        state('expanded', style({ height: '*', visibility: 'visible' })),
                        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                    ]),
                ],
            }]
    }], function () { return [{ type: i1.TranslateService }, { type: i2.MaisTableService }, { type: i2.MaisTableRegistryService }, { type: i0.ChangeDetectorRef }, { type: i0.NgZone }]; }, { data: [{
            type: Input
        }], dataProvider: [{
            type: Input
        }], columns: [{
            type: Input
        }], defaultSortingColumn: [{
            type: Input
        }], defaultSortingDirection: [{
            type: Input
        }], defaultPageSize: [{
            type: Input
        }], possiblePageSize: [{
            type: Input
        }], tableId: [{
            type: Input
        }], dropConnectedTo: [{
            type: Input
        }], paginationMode: [{
            type: Input
        }], enablePagination: [{
            type: Input
        }], enableSelection: [{
            type: Input
        }], enableContextFiltering: [{
            type: Input
        }], enableSelectAll: [{
            type: Input
        }], enableMultiSelect: [{
            type: Input
        }], enableFiltering: [{
            type: Input
        }], enableColumnsSelection: [{
            type: Input
        }], enableContextActions: [{
            type: Input
        }], enableHeaderActions: [{
            type: Input
        }], enableRowExpansion: [{
            type: Input
        }], enablePageSizeSelect: [{
            type: Input
        }], enableMultipleRowExpansion: [{
            type: Input
        }], enableItemTracking: [{
            type: Input
        }], enableStorePersistence: [{
            type: Input
        }], enableDrag: [{
            type: Input
        }], enableDrop: [{
            type: Input
        }], contextActions: [{
            type: Input
        }], headerActions: [{
            type: Input
        }], contextFilters: [{
            type: Input
        }], contextFilteringPrompt: [{
            type: Input
        }], actionStatusProvider: [{
            type: Input
        }], expandableStatusProvider: [{
            type: Input
        }], storeAdapter: [{
            type: Input
        }], itemIdentifier: [{
            type: Input
        }], refreshStrategy: [{
            type: Input
        }], refreshInterval: [{
            type: Input
        }], refreshEmitter: [{
            type: Input
        }], refreshIntervalInBackground: [{
            type: Input
        }], refreshOnPushInBackground: [{
            type: Input
        }], pageChange: [{
            type: Output
        }], sortChange: [{
            type: Output
        }], selectionChange: [{
            type: Output
        }], filteringColumnsChange: [{
            type: Output
        }], visibleColumnsChange: [{
            type: Output
        }], contextFiltersChange: [{
            type: Output
        }], statusChange: [{
            type: Output
        }], itemDragged: [{
            type: Output
        }], itemDropped: [{
            type: Output
        }], action: [{
            type: Output
        }], cellTemplate: [{
            type: ContentChild,
            args: ['cellTemplate']
        }], actionsCaptionTemplate: [{
            type: ContentChild,
            args: ['actionsCaptionTemplate']
        }], rowDetailTemplate: [{
            type: ContentChild,
            args: ['rowDetailTemplate']
        }], dragTemplate: [{
            type: ContentChild,
            args: ['dragTemplate']
        }], dropTemplate: [{
            type: ContentChild,
            args: ['dropTemplate']
        }], table: [{
            type: ViewChild,
            args: ['#renderedMatTable']
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9tYWlzLXRhYmxlLmNvbXBvbmVudC50cyIsImxpYi9tYWlzLXRhYmxlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUdULEtBQUssRUFDTCxNQUFNLEVBQ04sWUFBWSxFQUNaLFlBQVksRUFDWixXQUFXLEVBQ1gsaUJBQWlCLEVBQ2pCLE1BQU0sRUFJTixTQUFTLEVBQ1YsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUNMLHNCQUFzQixFQUl0Qix5QkFBeUIsRUFJekIsa0NBQWtDLEVBT2xDLHdCQUF3QixFQUd4QixxQkFBcUIsRUFFdEIsTUFBTSxTQUFTLENBQUM7QUFDakIsT0FBTyxFQUNMLHdCQUF3QixFQUN4Qix1QkFBdUIsRUFDdkIsd0JBQXdCLEVBQ3hCLHNCQUFzQixFQUN0QixlQUFlLEVBQ2hCLE1BQU0sU0FBUyxDQUFDO0FBQ2pCLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUV4RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBYyxVQUFVLEVBQWdCLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN4RixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRixPQUFPLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDdkIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztJQ003QixpQ0FFQTtJQURFLG9VQUFnRCx3QkFBd0IsSUFBRTtJQUQ1RSxpQkFFQTs7OztJQUZ1QiwyRUFBK0M7OztJQUV0RSw0QkFDQTs7SUFEdUIsK0JBQWlCLGtCQUFBOzs7O0lBTDlDLGtDQUNFO0lBQUEsK0JBQ0U7SUFBQSwrQkFDRTtJQUFBLDRIQUVBO0lBQUEsNEhBQ0E7SUFBQSxnQ0FDRTtJQURLLHdUQUErQyx3QkFBd0IsSUFBRTtJQUM5RSxZQUNGO0lBQUEsaUJBQU87SUFDVCxpQkFBTTtJQUNSLGlCQUFNO0lBQ1IsaUJBQVM7Ozs7SUFYcUMsK0RBQXdDO0lBR1QsZUFBNEI7SUFBNUIseURBQTRCO0lBRTFELGVBQTZCO0lBQTdCLDBEQUE2QjtJQUVwRSxlQUNGO0lBREUsbUVBQ0Y7OztJQVROLDZCQUNBO0lBQUEscUhBQ0U7SUFXRiwwQkFBZTs7OztJQVp3RSxlQUE0RjtJQUE1RixnSkFBNEY7Ozs7SUFsQ3ZMLDZCQUNBO0lBQUEsK0JBQ0U7SUFBQSxrQ0FNRztJQURhLHlOQUErQjtJQUM1Qyx3QkFBNkI7SUFDaEMsaUJBQVM7SUFDVCw2QkFNUztJQUNULCtCQUNFO0lBQUEsK0JBQ0U7SUFBQSw4QkFBNEI7SUFBQSxZQUFnRTs7SUFBQSxpQkFBSztJQUNuRyxpQkFBTTtJQUNOLG1DQUNFO0lBQUEsZ0NBQ0U7SUFBQSxnQ0FDRTtJQUFBLGtDQUVBO0lBREUsNE9BQThDLHdCQUF3QixJQUFFO0lBRDFFLGlCQUVBO0lBQUEsaUNBQ0U7SUFESyx5T0FBNkMsd0JBQXdCLElBQUU7SUFDNUUsYUFDRjs7SUFBQSxpQkFBTztJQUNULGlCQUFNO0lBQ1IsaUJBQU07SUFDTiwyQkFBb0M7SUFDdEMsaUJBQVM7SUFDVCxtSEFDQTtJQWFGLGlCQUFNO0lBQ1IsaUJBQU07SUFDTiwwQkFBZTs7O0lBaEQ2QixlQUF1QjtJQUF2QixxQ0FBdUI7SUFFL0QsZUFBK0Q7SUFBL0QsMEZBQStELDRFQUFBLGdGQUFBLGlEQUFBO0lBUS9ELGVBQW1FO0lBQW5FLDhGQUFtRSxpREFBQSxtRkFBQSxrRkFBQTtJQVFyQyxlQUFnRTtJQUFoRSwyRkFBZ0U7SUFLakUsZUFBc0M7SUFBdEMsNkRBQXNDO0lBRzNELGVBQ0Y7SUFERSwyR0FDRjtJQUtRLGVBQThCO0lBQTlCLDBDQUE4Qjs7O0lBeUMxQyw4QkFBOEU7O0lBQXJFLHFDQUF1Qjs7O0lBQ2hDLDhCQUF5RTs7SUFBaEUsK0JBQWlCOzs7SUFPMUIsOEJBQStHOztJQUF0RyxxQ0FBdUI7OztJQUNoQyw4QkFBMEc7O0lBQWpHLCtCQUFpQjs7O0lBQzFCLDhCQUFtRTs7SUFBMUQsK0JBQWlCOzs7O0lBSjVCLGtDQUVFO0lBRitELHNUQUE4QztJQUU3RyxnSUFBcUc7SUFDckcsZ0lBQWdHO0lBQ2hHLGdJQUF5RDtJQUN6RCxZQUNGO0lBQUEsaUJBQVM7Ozs7SUFOZSwrREFBd0M7SUFFN0IsZUFBbUU7SUFBbkUsOEdBQW1FO0lBQ3pFLGVBQW9FO0lBQXBFLCtHQUFvRTtJQUNwRSxlQUE2QjtJQUE3QiwwREFBNkI7SUFDeEQsZUFDRjtJQURFLG1FQUNGOzs7SUFQQSw2QkFDQTtJQUFBLHFIQUVFO0lBS0YsMEJBQWU7Ozs7SUFOYixlQUE0RjtJQUE1RixnSkFBNEY7Ozs7SUE3QmxHLDZCQUNBO0lBQUEsK0JBQ0U7SUFBQSxrQ0FNRztJQURhLHlOQUErQjtJQUM1Qyx3QkFBNkI7SUFDaEMsaUJBQVM7SUFDVCw2QkFNUztJQUNULCtCQUNFO0lBQUEsK0JBQ0U7SUFBQSw4QkFBNEI7SUFBQSxZQUFnRTs7SUFBQSxpQkFBSztJQUNuRyxpQkFBTTtJQUNOLG1DQUNFO0lBRHNCLHVPQUE0QztJQUNsRSx5R0FBb0U7SUFDcEUseUdBQStEO0lBQy9ELGFBQ0E7O0lBQUEsMkJBQW9DO0lBQ3RDLGlCQUFTO0lBQ1QsbUhBQ0E7SUFRRixpQkFBTTtJQUNSLGlCQUFNO0lBQ04sMEJBQWU7OztJQXJDNkIsZUFBdUI7SUFBdkIscUNBQXVCO0lBRS9ELGVBQStEO0lBQS9ELDBGQUErRCw0RUFBQSxnRkFBQSxpREFBQTtJQVEvRCxlQUFtRTtJQUFuRSw4RkFBbUUsaURBQUEsbUZBQUEsa0ZBQUE7SUFRckMsZUFBZ0U7SUFBaEUsMkZBQWdFO0lBRzNELGVBQWtDO0lBQWxDLDBEQUFrQztJQUN4QyxlQUFtQztJQUFuQywyREFBbUM7SUFDOUQsZUFDQTtJQURBLDJHQUNBO0lBRVksZUFBOEI7SUFBOUIsMENBQThCOzs7O0lBL0Z0RCwrQkFDRTtJQUFBLGdDQUNFO0lBREksME1BQWdDO0lBQ3BDLDJCQUFLO0lBQUEsWUFBdUQ7O0lBQUEsaUJBQU07SUFDbEUsK0JBQ0U7SUFBQSxpQ0FXQTtJQUpFLHNPQUFpQyxrTUFBQTs7SUFQbkMsaUJBV0E7SUFDQSxtR0FDQTtJQW1EQSxtR0FDQTtJQXNDRixpQkFBTTtJQUNSLGlCQUFPO0lBQ1QsaUJBQU07OztJQTNHRyxlQUF1RDtJQUF2RCxpRkFBdUQ7SUFJeEQsZUFBbUU7SUFBbkUsOEZBQW1FLGlEQUFBO0lBR25FLDBHQUEwRTtJQUUxRSxzREFBaUM7SUFLckIsZUFBc0M7SUFBdEMsOERBQXNDO0lBb0R0QyxlQUFxQztJQUFyQyw2REFBcUM7Ozs7SUF1RTNDLGlDQUVBO0lBREUsa1VBQThDLHdCQUF3QixJQUFFO0lBRDFFLGlCQUVBOzs7O0lBRnVCLCtFQUFtRDs7O0lBRTFFLDRCQUNBOztJQUR1Qiw4QkFBZ0Isa0JBQUE7Ozs7SUFMN0Msa0NBQ0U7SUFBQSwrQkFDRTtJQUFBLCtCQUNFO0lBQUEsNEhBRUE7SUFBQSw0SEFDQTtJQUFBLGdDQUNFO0lBREksc1RBQTZDLHdCQUF3QixJQUFFO0lBQzNFLFlBQ0Y7SUFBQSxpQkFBTztJQUNULGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBUzs7OztJQVhxQyw2REFBc0M7SUFHSCxlQUEwQjtJQUExQix1REFBMEI7SUFFN0QsZUFBMkI7SUFBM0Isd0RBQTJCO0lBRWpFLGVBQ0Y7SUFERSxtRUFDRjs7O0lBVE4sNkJBQ0E7SUFBQSxxSEFDRTtJQVdGLDBCQUFlOzs7O0lBWnNFLGVBQTBGO0lBQTFGLDhJQUEwRjs7OztJQXRCckwsNkJBQ0E7SUFBQSwrQkFDSTtJQUFBLGtDQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBUztJQUNULCtCQUNFO0lBQUEsOEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFLO0lBQ0wsa0NBQ0U7SUFBQSxnQ0FDRTtJQUFBLGdDQUNFO0lBQUEsa0NBRUE7SUFERSwwT0FBNEMsd0JBQXdCLElBQUU7SUFEeEUsaUJBRUE7SUFBQSxpQ0FDRTtJQURLLHVPQUEyQyx3QkFBd0IsSUFBRTtJQUMxRSxhQUNGOztJQUFBLGlCQUFPO0lBQ1QsaUJBQU07SUFDUixpQkFBTTtJQUNOLDJCQUFvQztJQUN0QyxpQkFBUztJQUNULG1IQUNBO0lBYUYsaUJBQU07SUFDVixpQkFBTTtJQUNOLDBCQUFlOzs7SUFwQ29CLGVBQXVCO0lBQXZCLHFDQUF1QjtJQUVwRCxlQUNGO0lBREUsaUdBQ0Y7SUFHSSxlQUNGO0lBREUsb0dBQ0Y7SUFJNkIsZUFBb0M7SUFBcEMsMkRBQW9DO0lBR3pELGVBQ0Y7SUFERSx5R0FDRjtJQUtRLGVBQThCO0lBQTlCLDBDQUE4Qjs7O0lBNkJ4Qyw4QkFBNEU7O0lBQW5FLHFDQUF1Qjs7O0lBQ2hDLDhCQUF1RTs7SUFBOUQsK0JBQWlCOzs7SUFPeEIsOEJBQWlIOztJQUF4RyxxQ0FBdUI7OztJQUNoQyw4QkFBNEc7O0lBQW5HLCtCQUFpQjs7O0lBQzFCLDhCQUF1RTs7SUFBOUQscUNBQXVCOzs7O0lBSnBDLGtDQUVJO0lBRHFDLG9UQUE0QztJQUNqRixnSUFBdUc7SUFDdkcsZ0lBQWtHO0lBQ2xHLGdJQUE2RDtJQUM3RCxZQUNKO0lBQUEsaUJBQVM7Ozs7SUFMUCw2REFBc0M7SUFDSCxlQUFxRTtJQUFyRSxnSEFBcUU7SUFDM0UsZUFBc0U7SUFBdEUsaUhBQXNFO0lBQ2hFLGVBQTJCO0lBQTNCLHdEQUEyQjtJQUM1RCxlQUNKO0lBREksbUVBQ0o7OztJQVBBLDZCQUNBO0lBQUEscUhBRUk7SUFLSiwwQkFBZTs7OztJQVBTLGVBQTBGO0lBQTFGLDhJQUEwRjs7OztJQWhCMUgsNkJBQ0U7SUFBQSwrQkFDSTtJQUFBLGtDQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBUztJQUNULCtCQUNFO0lBQUEsOEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFLO0lBQ0wsa0NBQ0U7SUFEc0Isb09BQTBDO0lBQ2hFLHlHQUFrRTtJQUNsRSx5R0FBNkQ7SUFDN0QsYUFDQTs7SUFBQSwyQkFBb0M7SUFDdEMsaUJBQVM7SUFDVCxtSEFDQTtJQVFGLGlCQUFNO0lBQ1YsaUJBQU07SUFDTiwwQkFBZTs7O0lBekJvQixlQUF1QjtJQUF2QixxQ0FBdUI7SUFFcEQsZUFDRjtJQURFLGlHQUNGO0lBR0ksZUFDRjtJQURFLG9HQUNGO0lBRW1DLGVBQWdDO0lBQWhDLHdEQUFnQztJQUN0QyxlQUFpQztJQUFqQyx5REFBaUM7SUFDNUQsZUFDQTtJQURBLHlHQUNBO0lBRVksZUFBOEI7SUFBOUIsMENBQThCOzs7SUEzRHRELCtCQUNFO0lBQUEsMkJBQUs7SUFBQSxZQUErRDs7SUFBQSxpQkFBTTtJQUcxRSxtR0FDQTtJQXVDQSxtR0FDRTtJQTJCSixpQkFBTTs7O0lBdkVDLGVBQStEO0lBQS9ELHlGQUErRDtJQUd0RCxlQUFzQztJQUF0Qyw4REFBc0M7SUF3Q3RDLGVBQXFDO0lBQXJDLDZEQUFxQzs7O0lBNkJyRCwwQkFFTTs7O0lBQ04sMEJBRU07Ozs7O0lBUUosa0NBR0U7SUFGQSwwUUFBc0M7SUFFdEMsWUFDRjtJQUFBLGlCQUFTOzs7O0lBSmEsNkhBQThGO0lBQzNFLHdFQUE0QztJQUVuRixlQUNGO0lBREUsbUVBQ0Y7OztJQUxBLDZCQUNBO0lBQUEsZ0dBR0U7SUFFRiwwQkFBZTs7O0lBTHNHLGVBQW9DO0lBQXBDLGdEQUFvQzs7OztJQWFySixrQ0FDRTtJQUQ0RCxpUUFBc0M7SUFDbEcsWUFDRjtJQUFBLGlCQUFTOzs7O0lBRjRGLHdFQUE0QztJQUMvSSxlQUNGO0lBREUsbUVBQ0Y7OztJQVJKLCtCQUNFO0lBQUEsa0NBQXNHO0lBQUEsWUFBd0Q7O0lBQUEsaUJBQVM7SUFDdkssK0JBQ0U7SUFBQSwrQkFDRTtJQUFBLDhCQUE0QjtJQUFBLFlBQXFEOztJQUFBLGlCQUFLO0lBQ3hGLGlCQUFNO0lBQ04sdUZBQ0U7SUFFSixpQkFBTTtJQUNSLGlCQUFNOzs7SUFUMEMsZUFBcUM7SUFBckMsNERBQXFDO0lBQW1CLGVBQXdEO0lBQXhELGtGQUF3RDtJQUc5SCxlQUFxRDtJQUFyRCwrRUFBcUQ7SUFFM0QsZUFBcUM7SUFBckMsaURBQXFDOzs7O0lBck52RSw4QkFHRTtJQUFBLDhCQUNFO0lBQUEsMkVBQ0U7SUE2R0YseUVBQ0U7SUF3RUYseUVBQ0U7SUFFRix5RUFDRTtJQUVGLDhCQUNFO0lBQUEsMkJBQ0U7SUFBQSx5RkFBeUY7SUFDekYsd0JBQ0Y7SUFBQSxpQkFBTTtJQUVOLDZGQUNBO0lBT0EsNkVBQ0U7SUFVSixpQkFBTTtJQUNSLGlCQUFNO0lBQ1IsaUJBQU07OztJQXhOaUIsZUFBbUM7SUFBbkMsMkRBQW1DO0lBOEduQyxlQUF5QztJQUF6QyxpRUFBeUM7SUF5RXpDLGVBQTBDO0lBQTFDLGtFQUEwQztJQUcxQyxlQUFvQztJQUFwQyw0REFBb0M7SUFLdEMsZUFBMkM7SUFBM0Msa0VBQTJDLHVEQUFBO0lBSTVDLGVBQXVDO0lBQXZDLCtEQUF1QztJQVFsQixlQUF3QztJQUF4QyxnRUFBd0M7OztJQWdDekUseUJBRUs7Ozs7SUFHRCw0QkFDRTtJQUFBLGlDQUNGO0lBRGdELHVOQUE2QjtJQUEzRSxpQkFDRjtJQUFBLGlCQUFPOzs7SUFEa0IsZUFBc0I7SUFBdEIsNkNBQXNCOzs7SUFIbkQsOEJBQ0U7SUFBQSwrQkFDRTtJQUFBLHlGQUNFO0lBRUosaUJBQU07SUFDUixpQkFBSzs7O0lBSkssZUFBd0U7SUFBeEUsa0hBQXdFOzs7SUFXcEUsNEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFPOzs7SUFETCxlQUNGO0lBREUsc0ZBQ0Y7OztJQUNBLDRCQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBTzs7SUFETCxlQUNGO0lBREUsb0dBQ0Y7Ozs7SUFFRiwrQkFDRTtJQUFBLGlDQUVFO0lBREEsaVJBQThDLHdCQUF3QixJQUFFO0lBRDFFLGlCQUVFO0lBQUEsZ0NBQ0E7SUFEd0IsOFFBQTZDLHdCQUF3QixJQUFFO0lBQy9GLFlBQ0E7SUFBQSxpQkFBTztJQUNYLGlCQUFNOzs7O0lBTG1CLGVBQTBDO0lBQTFDLHNFQUEwQztJQUcvRCxlQUNBO0lBREEsbUVBQ0E7OztJQWxCaEIsOEJBRUk7SUFBQSwrQkFDRTtJQUFBLCtCQUNJO0lBQUEsK0JBQ0U7SUFBQSw4QkFDRTtJQUFBLDBGQUNFO0lBRUYsMEZBQ0U7SUFFSixpQkFBSztJQUNMLHlGQUNFO0lBTUosaUJBQU07SUFDVixpQkFBTTtJQUNSLGlCQUFNO0lBQ1YsaUJBQUs7OztJQXRCSCxrRUFBZ0Q7SUFBYSxxQ0FBdUI7SUFLbEUsZUFBOEI7SUFBOUIsc0RBQThCO0lBRzlCLGVBQStCO0lBQS9CLHVEQUErQjtJQUlJLGVBQTRDO0lBQTVDLHdEQUE0Qzs7OztJQW9CN0YsNEJBQ0U7SUFBQSxrQ0FBcUY7SUFBaEMsaVNBQStCO0lBQUMsd0JBQXdDO0lBQUEsaUJBQVM7SUFDeEksaUJBQU87Ozs7SUFDUCw0QkFDRTtJQUFBLGtDQUFxRjtJQUFoQyxpU0FBK0I7SUFBQyx3QkFBd0M7SUFBQSxpQkFBUztJQUN4SSxpQkFBTzs7O0lBTlQsNEJBQ0U7SUFBQSwyR0FDRTtJQUVGLDJHQUNFO0lBRUosaUJBQU87Ozs7SUFOQyxlQUE2QztJQUE3QywwRUFBNkM7SUFHN0MsZUFBOEM7SUFBOUMsMkVBQThDOzs7SUFJdEQsNEJBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQU87Ozs7SUFETCxlQUNGO0lBREUsbUVBQ0Y7Ozs7SUFsQkosNkJBQ0U7SUFBQSw4QkFPRTtJQUZBLCtQQUErQjtJQUUvQixvR0FDRTtJQU9GLG9HQUNFO0lBRUosaUJBQUs7SUFDUCwwQkFBZTs7OztJQWxCWCxlQUF3RDtJQUF4RCwrRUFBd0Qsb0VBQUEsZ0VBQUEsK0NBQUE7SUFNbEQsZUFBc0M7SUFBdEMsbUVBQXNDO0lBUXRDLGVBQXlEO0lBQXpELG9HQUF5RDs7OztJQW9CakUsbUNBQW1KO0lBQTdDLDJTQUFrQztJQUFDLGlCQUFVOztJQUF4SCwwQ0FBNEI7Ozs7SUFDdkQsbUNBQTJIO0lBQTdDLDJTQUFrQztJQUFDLGlCQUFVOztJQUFoRyx3Q0FBMEI7OztJQUZ2RCw4QkFDRTtJQUFBLHdIQUF5STtJQUN6SSx3SEFBaUg7SUFDbkgsaUJBQUs7Ozs7SUFGcUQsZUFBNkM7SUFBN0Msd0ZBQTZDO0lBQy9DLGVBQXVCO0lBQXZCLG9EQUF1Qjs7OztJQUUvRSw4QkFDRTtJQUFBLCtCQUNFO0lBQUEsaUNBQ0Y7SUFEb0QsMFJBQTZCO0lBQS9FLGlCQUNGO0lBQUEsaUJBQU07SUFDUixpQkFBSzs7OztJQUZzQixlQUEwQjtJQUExQixzREFBMEI7OztJQUdyRCxxQkFFSzs7Ozs7SUFHSCwyQkFDRTtJQUFBLCtJQUdBO0lBQ0YsaUJBQU07Ozs7O0lBSEEsZUFBaUM7SUFBakMsd0RBQWlDLDRIQUFBOzs7SUFJdkMsMkJBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQU07Ozs7O0lBREosZUFDRjtJQURFLDZFQUNGOzs7SUFWRiw2QkFDQTtJQUFBLDBCQUNFO0lBQUEseUhBQ0U7SUFLRix5SEFDRTtJQUVKLGlCQUFLO0lBQ0wsMEJBQWU7OztJQVZSLGVBQTRCO0lBQTVCLGdEQUE0QjtJQU01QixlQUE2QjtJQUE3QixpREFBNkI7Ozs7O0lBTWxDLDhCQUNFO0lBQUEscUJBQVM7SUFDVCw4QkFDRTtJQUFBLCtCQUNFO0lBQUEsK0hBR0E7SUFDRixpQkFBTTtJQUNSLGlCQUFLO0lBQ1AsaUJBQUs7Ozs7SUFWMEMsNkRBQXNDO0lBRS9FLGVBQXVDO0lBQXZDLDBEQUF1QztJQUduQyxlQUFpRDtJQUFqRCx3RUFBaUQsaUVBQUE7Ozs7SUFRM0QsK0JBQ0U7SUFBQSxnSUFHQTtJQUNGLGlCQUFNOzs7O0lBSEYsZUFBaUM7SUFBakMsd0RBQWlDLGlFQUFBOzs7O0lBTXpDLDhCQUNFO0lBQUEscUJBQVM7SUFDVCwwQkFDRTtJQUFBLCtCQUNFO0lBQUEsZ0lBR0E7SUFDRixpQkFBTTtJQUNSLGlCQUFLO0lBQ1AsaUJBQUs7Ozs7SUFSQyxlQUF1QztJQUF2QywwREFBdUM7SUFHbkMsZUFBc0M7SUFBdEMsNkRBQXNDLGlFQUFBOzs7SUE1RGhELDZCQUNBO0lBQUEsOEJBT0U7SUFBQSx5R0FDRTtJQUdGLHlHQUNFO0lBSUYsd0dBQ0U7SUFFRiw2SEFDQTtJQVlBLDhCQUNFO0lBQUEseUdBQ0U7SUFVSixpQkFBSztJQUNMLDhCQUNFO0lBQUEsMkdBQ0U7SUFLSixpQkFBSztJQUNQLGlCQUFLO0lBQ0wsMkdBQ0U7SUFVRiwwQkFBZTs7OztJQWhFYixlQUFxQztJQUFyQyw0REFBcUM7SUFHckMsc0NBQW1CLHlDQUFBO0lBR0gsZUFBc0M7SUFBdEMsOERBQXNDO0lBSXRDLGVBQThCO0lBQTlCLHNEQUE4QjtJQUsxQyxlQUEwQztJQUExQyxrRUFBMEM7SUFHaEMsZUFBcUM7SUFBckMsaURBQXFDO0lBbUNqRCxlQUF5RDtJQUF6RCwrRkFBeUQ7OztJQTNEL0QsNkJBR0U7SUFBQSx5QkFBdUM7SUFDdkMsK0dBQ0E7SUFrRUYsaUJBQVE7OztJQW5FUSxlQUErQjtJQUEvQiw4Q0FBK0I7OztJQW9FL0MsNkJBQ0E7SUFBQSw2QkFDRTtJQUFBLDhCQUNFO0lBQUEsOEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFLO0lBQ1AsaUJBQUs7SUFDUCxpQkFBUTtJQUNSLDBCQUFlOzs7SUFMUCxlQUFtQztJQUFuQyxzREFBbUM7SUFDckMsZUFDRjtJQURFLHlGQUNGOzs7SUFJSiw2QkFDQTtJQUFBLDZCQUNFO0lBQUEsOEJBQ0U7SUFBQSw4QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQUs7SUFDUCxpQkFBSztJQUNQLGlCQUFRO0lBQ1IsMEJBQWU7OztJQUxQLGVBQW1DO0lBQW5DLHNEQUFtQztJQUNyQyxlQUNGO0lBREUsdUZBQ0Y7Ozs7SUEvSlIsNkJBQ0E7SUFBQSwrQkFDRTtJQUFBLGlDQVdFO0lBUEEsK09BQWdEO0lBT2hELG1DQUF3QjtJQUFBLFlBQXNEOztJQUFBLGlCQUFVO0lBQ3hGLGlDQUNFO0lBQUEsMEJBQ0U7SUFBQSxpRkFDRTtJQUVGLGlGQUNFO0lBTUYsbUZBRUk7SUFzQkosd0dBQ0U7SUFvQkosaUJBQUs7SUFDUCxpQkFBUTtJQUNSLHdGQUdFO0lBcUVGLHNHQUNBO0lBUUEsc0dBQ0E7SUFRRixpQkFBUTtJQUNWLGlCQUFNO0lBQ04sMEJBQWU7OztJQTNKWCxlQUE0QjtJQUE1Qiw4Q0FBNEIsbUNBQUE7SUFONUIsdURBQXVCO0lBQ3ZCLGlFQUEwQyx5Q0FBQSwyREFBQSxvREFBQTtJQVFsQixlQUFzRDtJQUF0RCxpRkFBc0Q7SUFHMUQsZUFBc0M7SUFBdEMsOERBQXNDO0lBR3RDLGVBQThCO0lBQTlCLHNEQUE4QjtJQU85QixlQUEwQztJQUExQyxrRUFBMEM7SUF3QjVDLGVBQXFDO0lBQXJDLGlEQUFxQztJQXdCckQsZUFBdUM7SUFBdkMsd0VBQXVDO0lBdUUzQixlQUFrQztJQUFsQyxtRUFBa0M7SUFTbEMsZUFBb0I7SUFBcEIsNENBQW9COzs7SUFrQ2hDLHNDQUVrQjs7OztJQUVoQixtQ0FBbUo7SUFBN0MsMFNBQWtDO0lBQUMsaUJBQVU7O0lBQXhILDBDQUE0Qjs7OztJQUN2RCxtQ0FBMkg7SUFBN0MsMFNBQWtDO0lBQUMsaUJBQVU7O0lBQWhHLHdDQUEwQjs7O0lBRnZELG9DQUNFO0lBQUEsd0hBQXlJO0lBQ3pJLHdIQUFpSDtJQUNuSCxpQkFBVzs7OztJQUYrQyxlQUE2QztJQUE3Qyx3RkFBNkM7SUFDL0MsZUFBdUI7SUFBdkIsb0RBQXVCOzs7O0lBTTNFLDRCQUNFO0lBQUEsaUNBQ0Y7SUFEZ0Qsc1BBQTZCO0lBQTNFLGlCQUNGO0lBQUEsaUJBQU87OztJQURrQixlQUFzQjtJQUF0Qiw2Q0FBc0I7OztJQUhuRCwyQ0FDRTtJQUFBLCtCQUNFO0lBQUEsd0hBQ0U7SUFFSixpQkFBTTtJQUNSLGlCQUFrQjs7O0lBSlIsZUFBd0U7SUFBeEUsa0hBQXdFOzs7O0lBS2xGLG9DQUNFO0lBQUEsK0JBQ0U7SUFBQSxpQ0FDRjtJQURvRCwyUUFBNkI7SUFBL0UsaUJBQ0Y7SUFBQSxpQkFBTTtJQUNSLGlCQUFXOzs7O0lBRmdCLGVBQTBCO0lBQTFCLHNEQUEwQjs7O0lBZ0J2Qyw0QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQU87OztJQURMLGVBQ0Y7SUFERSxzRkFDRjs7O0lBQ0EsNEJBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFPOztJQURMLGVBQ0Y7SUFERSxvR0FDRjs7OztJQUVGLCtCQUNFO0lBQUEsaUNBRUU7SUFEQSwrU0FBOEMsd0JBQXdCLElBQUU7SUFEMUUsaUJBRUU7SUFBQSxnQ0FDQTtJQUR3Qiw0U0FBNkMsd0JBQXdCLElBQUU7SUFDL0YsWUFDQTtJQUFBLGlCQUFPO0lBQ1gsaUJBQU07Ozs7SUFMbUIsZUFBMEM7SUFBMUMsc0VBQTBDO0lBRy9ELGVBQ0E7SUFEQSxtRUFDQTs7O0lBdkJoQiwyQ0FDRTtJQUFBLCtCQU1FO0lBQUEsK0JBQ0U7SUFBQSwrQkFDSTtJQUFBLCtCQUNFO0lBQUEsOEJBQ0U7SUFBQSx3SEFDRTtJQUVGLHdIQUNFO0lBRUosaUJBQUs7SUFDTCx1SEFDRTtJQU1KLGlCQUFNO0lBQ1YsaUJBQU07SUFDUixpQkFBTTtJQUNSLGlCQUFNO0lBQ1IsaUJBQWtCOzs7SUEzQmQsZUFBZ0Q7SUFBaEQsa0VBQWdEO0lBRWhELHFDQUF1QixxQkFBQTtJQU9QLGVBQThCO0lBQTlCLHNEQUE4QjtJQUc5QixlQUErQjtJQUEvQix1REFBK0I7SUFJSSxlQUE0QztJQUE1Qyx3REFBNEM7OztJQVluRywrQkFFVzs7OztJQUdYLGdDQUNFO0lBQUEsK0JBQ0U7SUFBQSxnSUFHQTtJQUNGLGlCQUFNO0lBQ1IsaUJBQVc7Ozs7SUFKSCxlQUFzQztJQUF0Qyw2REFBc0MsaUZBQUE7Ozs7SUFnQnhDLDRCQUNFO0lBQUEsa0NBQXFGO0lBQWhDLHFVQUErQjtJQUFDLHdCQUF3QztJQUFBLGlCQUFTO0lBQ3hJLGlCQUFPOzs7O0lBQ1AsNEJBQ0U7SUFBQSxrQ0FBcUY7SUFBaEMscVVBQStCO0lBQUMsd0JBQXdDO0lBQUEsaUJBQVM7SUFDeEksaUJBQU87OztJQU5ULDRCQUNFO0lBQUEsK0lBQ0U7SUFFRiwrSUFDRTtJQUVKLGlCQUFPOzs7O0lBTkMsZUFBNkM7SUFBN0MsMEVBQTZDO0lBRzdDLGVBQThDO0lBQTlDLDJFQUE4Qzs7O0lBSXRELDRCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFPOzs7O0lBREwsZUFDRjtJQURFLG1FQUNGOzs7O0lBbEJKLHVDQUNFO0lBQUEsK0JBT0U7SUFEQSxtVEFBK0I7SUFDL0Isd0lBQ0U7SUFPRix3SUFDRTtJQUVKLGlCQUFNO0lBQ1IsaUJBQWtCOzs7O0lBcEJpQix5RUFBK0M7SUFFOUUsZUFBNkM7SUFBN0MsbURBQTZDO0lBQzdDLCtFQUF3RCxvRUFBQSxnRUFBQSwrQ0FBQTtJQUtsRCxlQUFzQztJQUF0QyxtRUFBc0M7SUFRdEMsZUFBeUQ7SUFBekQsb0dBQXlEOzs7O0lBTWpFLDJCQUNFO0lBQUEscUpBR0E7SUFDRixpQkFBTTs7Ozs7SUFIQSxlQUFpQztJQUFqQyx3REFBaUMsNEhBQUE7OztJQUl2QywyQkFDRTtJQUFBLFlBQ0Y7SUFBQSxpQkFBTTs7Ozs7SUFESixlQUNGO0lBREUsNkVBQ0Y7OztJQVRGLGdDQUNFO0lBQUEsK0hBQ0U7SUFLRiwrSEFDRTtJQUVKLGlCQUFXOzs7SUFWcUIsa0hBQW1GO0lBQzVHLGVBQTRCO0lBQTVCLGdEQUE0QjtJQU01QixlQUE2QjtJQUE3QixpREFBNkI7OztJQTdCdEMsaUNBQ0U7SUFBQSw4SUFDRTtJQW9CRiwrSEFDRTtJQVVKLDBCQUFlOzs7SUFqQ0QsK0NBQTRCOzs7SUFvQzFDLGlDQUE4RTs7O0lBQzlFLDhCQU9XOzs7O0lBTFQsaUVBQTBDLDhDQUFBO0lBRzFDLHNDQUFtQix5Q0FBQTs7O0lBR3JCLDhCQUdVOzs7O0lBRk4sZ0dBQW9FOzs7OztJQWxJMUUseUNBZUU7SUFSQSxzUUFBZ0Q7SUFTaEQsaUNBQ0U7SUFBQSw2SEFDRTtJQUVGLCtHQUNFO0lBR0osMEJBQWU7SUFDZixpQ0FDRTtJQUFBLDZIQUNFO0lBTUYsK0dBQ0U7SUFJSiwwQkFBZTtJQUNmLGlDQUNFO0lBQUEsNkhBQ0U7SUE2QkYsaUhBQ0U7SUFFSiwwQkFBZTtJQUNmLGtDQUNFO0lBQUEsaUhBQ0U7SUFPSiwwQkFBZTtJQUNmLHlIQUNFO0lBbUNGLDZIQUE2RDtJQUM3RCwrR0FPQztJQUNELCtHQUdBO0lBQ0YsaUJBQVk7OztJQTFIViw4Q0FBNEIsbUNBQUE7SUFONUIsdURBQXVCO0lBSHZCLDREQUFxQyxvREFBQSx5Q0FBQSwyREFBQSxvREFBQTtJQWN2QixlQUErQztJQUEvQyw2REFBK0M7SUFTL0MsZUFBK0M7SUFBL0MsNkRBQStDO0lBYy9DLGVBQW1EO0lBQW5ELGlFQUFtRDtJQW1DbkQsZUFBNEM7SUFBNUMsMERBQTRDO0lBVWYsZUFBNkM7SUFBN0MseURBQTZDO0lBb0N4RSxlQUE0QztJQUE1QyxvRUFBNEM7SUFDbkQsZUFBeUQ7SUFBekQscUVBQXlEO0lBUXpELGVBQWdHO0lBQWhHLDhEQUFnRyx3REFBQTs7O0lBTTNHLDZCQUNBO0lBQUEsK0JBQ0U7SUFBQSw2QkFDRTtJQUFBLFlBQ0Y7O0lBQUEsaUJBQUk7SUFDTixpQkFBTTtJQUNOLDBCQUFlOztJQUhYLGVBQ0Y7SUFERSx5RkFDRjs7O0lBR0YsNkJBQ0E7SUFBQSwrQkFDRTtJQUFBLDZCQUNFO0lBQUEsWUFDRjs7SUFBQSxpQkFBSTtJQUNOLGlCQUFNO0lBQ04sMEJBQWU7O0lBSFgsZUFDRjtJQURFLHVGQUNGOzs7SUFwSkosK0JBRUU7SUFBQSx1R0FlRTtJQXdIRiwwR0FDQTtJQU1BLDBHQUNBO0lBT0YsaUJBQU07OztJQXpJRixlQUF1QztJQUF2Qyx3RUFBdUM7SUEwSDNCLGVBQWtDO0lBQWxDLG1FQUFrQztJQU9sQyxlQUFvQjtJQUFwQiw0Q0FBb0I7OztJQWpKcEMsNkJBQ0E7SUFBQSxtRkFFRTtJQXVKRiwwQkFBZTs7O0lBekplLGVBQThCO0lBQTlCLHNEQUE4Qjs7O0lBaUwxQyxpQ0FBd0Q7SUFBQSx5QkFBUztJQUFBLGlCQUFPOzs7O0lBSDVFLCtCQUNFO0lBQUEsOEJBQ0U7SUFEbUIsbVJBQTRCO0lBQy9DLFlBQ0E7SUFBQSxpSEFBd0Q7SUFDMUQsaUJBQUk7SUFDTixpQkFBSzs7OztJQUwyQixpRUFBMEM7SUFFdEUsZUFDQTtJQURBLDhDQUNBO0lBQXNCLGVBQWlDO0lBQWpDLDhEQUFpQzs7O0lBRzNELCtCQUNFO0lBQUEsOEJBQ0U7SUFBQSxxQkFDRjtJQUFBLGlCQUFJO0lBQ04saUJBQUs7OztJQVhMLDZCQUNBO0lBQUEsd0dBQ0U7SUFLRix3R0FDRTtJQUlGLDBCQUFlOzs7SUFYNEQsZUFBa0I7SUFBbEIsc0NBQWtCO0lBTXZFLGVBQWlCO0lBQWpCLHFDQUFpQjs7OztJQXNCdkMsa0NBQ0U7SUFEd0UsZ1JBQW1DO0lBQzNHLFlBQ0Y7SUFBQSxpQkFBUzs7OztJQUZxRyxxRUFBeUM7SUFDckosZUFDRjtJQURFLDhDQUNGOzs7O0lBUkosaUNBQ0U7SUFBQSwyQkFDRTtJQUFBLHdCQUNGO0lBQUEsaUJBQU07SUFDTixtQ0FBZ0U7SUFBQSxZQUFxRjs7SUFBQSxpQkFBUztJQUM5SixnQ0FDRTtJQUFBLHVHQUNFO0lBRUosaUJBQU87SUFDVCxpQkFBTzs7O0lBTjJELGVBQXFGO0lBQXJGLDJJQUFxRjtJQUUzSCxlQUFpRDtJQUFqRCwyREFBaUQ7Ozs7O0lBMUMvRSw2QkFDQTtJQUFBLGdDQUNFO0lBQUEsZ0NBQ0U7SUFBQSxZQUNGOztJQUFBLGlCQUFNO0lBQ04sZ0NBQ0U7SUFBQSwrQkFDRTtJQUFBLCtCQUNFO0lBQUEsOEJBQStDO0lBQTFCLGtNQUFzQixDQUFDLEtBQUU7SUFBQyxZQUFzRDs7SUFBQSxpQkFBSTtJQUMzRyxpQkFBSztJQUNMLGdDQUNFO0lBQUEsK0JBQWtFO0lBQTdDLCtOQUF5QyxDQUFDLEtBQUU7SUFBQyxhQUF5RDs7SUFBQSxpQkFBSTtJQUNqSSxpQkFBSztJQUNMLDZHQUNBO0lBWUEsZ0NBQ0U7SUFBQSwrQkFBa0U7SUFBN0MsK05BQXlDLENBQUMsS0FBRTtJQUFDLGFBQXFEOztJQUFBLGlCQUFJO0lBQzdILGlCQUFLO0lBQ0wsZ0NBQ0U7SUFBQSwrQkFBa0U7SUFBN0MsK05BQXlDLENBQUMsS0FBRTtJQUFDLGFBQXFEOztJQUFBLGlCQUFJO0lBQzdILGlCQUFLO0lBQ1AsaUJBQUs7SUFDUCxpQkFBTTtJQUNSLGlCQUFNO0lBQ04saUNBQ0U7SUFBQSw4RkFDRTtJQVVKLGlCQUFNO0lBQ04sMEJBQWU7OztJQTdDWCxlQUNGO0lBREUsdUxBQ0Y7SUFHb0MsZUFBMEM7SUFBMUMsNERBQTBDO0lBQ3pCLGVBQXNEO0lBQXRELGtGQUFzRDtJQUV2RSxlQUEwQztJQUExQyw0REFBMEM7SUFDTixlQUF5RDtJQUF6RCxxRkFBeUQ7SUFFL0csZUFBOEI7SUFBOUIsNENBQThCO0lBYVosZUFBNkQ7SUFBN0Qsc0ZBQTZEO0lBQ3pCLGVBQXFEO0lBQXJELGlGQUFxRDtJQUV6RixlQUE2RDtJQUE3RCxzRkFBNkQ7SUFDekIsZUFBcUQ7SUFBckQsaUZBQXFEO0lBTTNHLGVBQXdDO0lBQXhDLGdFQUF3Qzs7Ozs7SUFxQjFELGtDQUdBO0lBRkEseVFBQXNDO0lBRXRDLFlBQ0Y7SUFBQSxpQkFBUzs7OztJQUplLDZIQUE4RjtJQUM3RSx3RUFBNEM7SUFFbkYsZUFDRjtJQURFLG1FQUNGOzs7SUFMQSw2QkFDRTtJQUFBLCtGQUdBO0lBRUYsMEJBQWU7OztJQUx3RyxlQUFvQztJQUFwQyxnREFBb0M7Ozs7SUFhdkosa0NBQ0U7SUFENEQsZ1FBQXNDO0lBQ2xHLFlBQ0Y7SUFBQSxpQkFBUzs7OztJQUY0Rix3RUFBNEM7SUFDL0ksZUFDRjtJQURFLG1FQUNGOzs7SUFSSiwrQkFDRTtJQUFBLGtDQUFzRztJQUFBLFlBQXdEOztJQUFBLGlCQUFTO0lBQ3ZLLCtCQUNFO0lBQUEsK0JBQ0U7SUFBQSw4QkFBNEI7SUFBQSxZQUFxRDs7SUFBQSxpQkFBSztJQUN4RixpQkFBTTtJQUNOLHNGQUNFO0lBRUosaUJBQU07SUFDUixpQkFBTTs7O0lBVDBDLGVBQXFDO0lBQXJDLDREQUFxQztJQUFtQixlQUF3RDtJQUF4RCxrRkFBd0Q7SUFHOUgsZUFBcUQ7SUFBckQsK0VBQXFEO0lBRTNELGVBQXFDO0lBQXJDLGlEQUFxQzs7O0lBM0V6RSxnQ0FHRTtJQUFBLDhCQUNJO0lBQUEsZ0NBQ0U7SUFBQSw2RkFDQTtJQWdERixpQkFBTTtJQUNOLDhCQUNFO0lBQUEsMkJBQ0U7SUFBQSx5RkFBeUY7SUFDekYsd0JBQ0Y7SUFBQSxpQkFBTTtJQUVOLDJGQUNFO0lBT0YsMkVBQ0U7SUFVSixpQkFBTTtJQUNWLGlCQUFNO0lBQ1IsaUJBQU07OztJQTdFZ0IsZUFBMEQ7SUFBMUQsMkZBQTBEO0lBb0R6RCxlQUEyQztJQUEzQyxrRUFBMkMsdURBQUE7SUFJNUMsZUFBdUM7SUFBdkMsK0RBQXVDO0lBUWxCLGVBQXdDO0lBQXhDLGdFQUF3Qzs7QURuakJyRjtJQW9JRSxrQkFBa0I7SUFFbEIsNEJBQ1UsZ0JBQWtDLEVBQ2xDLG9CQUFzQyxFQUN0QyxRQUFrQyxFQUNsQyxHQUFzQixFQUN0QixNQUFjO1FBTHhCLGlCQVlDO1FBWFMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQWtCO1FBQ3RDLGFBQVEsR0FBUixRQUFRLENBQTBCO1FBQ2xDLFFBQUcsR0FBSCxHQUFHLENBQW1CO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUF4SGYsaUJBQVksR0FDNkIsSUFBSSxDQUFDO1FBRTlDLHlCQUFvQixHQUFrQixJQUFJLENBQUM7UUFDM0MsNEJBQXVCLEdBQWtCLElBQUksQ0FBQztRQUM5QyxvQkFBZSxHQUFrQixJQUFJLENBQUM7UUFDdEMscUJBQWdCLEdBQW9CLElBQUksQ0FBQztRQWtDbEQsZ0JBQWdCO1FBQ04sZUFBVSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDeEMsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFrQyxDQUFDO1FBQ2hFLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUM1QywyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBc0IsQ0FBQztRQUNoRSx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBc0IsQ0FBQztRQUM5RCx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBNkIsQ0FBQztRQUNyRSxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUE0QixDQUFDO1FBQzVELGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQWdDLENBQUM7UUFDL0QsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBZ0MsQ0FBQztRQUMvRCxXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQXNDLENBQUM7UUFXbEUsU0FBSSxHQUFHLHNCQUFzQixDQUFDLElBQUksRUFBRSxDQUFDO1FBSXJDLG1CQUFjLEdBQW9DLElBQUksQ0FBQztRQUN2RCw4QkFBeUIsR0FBb0MsSUFBSSxDQUFDO1FBRTFFLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ2QsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFpQzVCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFFYiwrQkFBMEIsR0FBd0IsSUFBSSxDQUFDO1FBRXZELGdDQUEyQixHQUF3QixJQUFJLENBQUM7UUFFaEUsbUJBQW1CO1FBQ25CLDJCQUFzQixHQUEwQixJQUFJLENBQUM7UUFDckQsb0JBQWUsR0FBUSxJQUFJLENBQUM7UUE0M0I1Qix3QkFBd0I7UUFFeEIsd0JBQW1CLEdBQUcsVUFBQyxJQUFrQjtZQUN2QyxPQUFPLEtBQUksQ0FBQyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFBO1FBd3lCRCxpQ0FBNEIsR0FBRyxVQUFDLENBQVMsRUFBRSxHQUFRLElBQUssT0FBQSxHQUFHLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLEVBQXpDLENBQXlDLENBQUM7UUFFbEcsdUJBQWtCLEdBQUcsVUFBQyxHQUFRLEVBQUUsS0FBYTtZQUFiLHNCQUFBLEVBQUEsYUFBYTtZQUMzQyxJQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksS0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ25ELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFDRCxJQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksS0FBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ25ELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFDRCxJQUFJLENBQUMsS0FBSSxDQUFDLDhCQUE4QixFQUFFO2dCQUN4QyxPQUFPLEtBQUssQ0FBQzthQUNkO1lBQ0QsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7WUFDRCxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDekIsT0FBTyxLQUFLLENBQUM7YUFDZDtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFBO1FBcHJEQyxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsR0FBRyxDQUFDLEVBQUUsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFlLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFFeEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksT0FBTyxFQUFTLENBQUM7SUFDckQsQ0FBQztJQUVELHFDQUFRLEdBQVI7UUFBQSxpQkF5RUM7UUF4RUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGVBQWUsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUU1QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFeEUsbUJBQW1CO1FBQ25CLHdCQUF3QixDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztRQUVsSCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDaEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUM3RCxVQUFBLENBQUMsSUFBSSxPQUFBLHdCQUF3QixDQUFDLHFCQUFxQixDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxFQUEvRixDQUErRixDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUN2RCxVQUFBLENBQUMsSUFBSSxPQUFBLHdCQUF3QixDQUFDLHNCQUFzQixDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFoRyxDQUFnRyxDQUFDLENBQUM7UUFDekcsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUNqRCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7UUFFdkUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLCtCQUErQjtZQUMvQixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2IsTUFBTSxJQUFJLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO2FBQzVFO1NBRUY7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLFlBQVksVUFBVSxJQUFJLElBQUksQ0FBQyxJQUFJLFlBQVksT0FBTyxFQUFFO1lBQzFFLHNCQUFzQjtZQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFlBQVk7Z0JBQzlCLEtBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2RCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxzQkFBc0I7WUFDdEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDckM7UUFFRCxxQ0FBcUM7UUFDckMsSUFBSSxvQkFBNEUsQ0FBQztRQUNqRixJQUFJLElBQUksQ0FBQyxrQ0FBa0MsRUFBRTtZQUMzQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUNuRDthQUFNO1lBQ0wsb0JBQW9CLEdBQUcsSUFBSSxVQUFVLENBQUMsVUFBQSxVQUFVO2dCQUM5QyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QixVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxVQUFDLE1BQTJDO1lBQ3pFLElBQUksTUFBTSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNuRSxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDbEM7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQzdGLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ2hDLENBQUMsRUFBRSxVQUFBLElBQUk7Z0JBQ0wsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFTCxDQUFDLEVBQUUsVUFBQSxPQUFPO1lBQ1IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsb0NBQW9DLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDakUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQ2pFLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ2hDLENBQUMsRUFBRSxVQUFBLElBQUk7Z0JBQ0wsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwrQ0FBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFTyw4REFBaUMsR0FBekMsVUFBMEMsWUFBbUI7UUFDM0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsWUFBWSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsUUFBUSxFQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRU8sbURBQXNCLEdBQTlCO1FBQUEsaUJBYUM7UUFaQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUV4QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO1lBQ2hFLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzFDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDdEQ7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUMxQyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDcEU7SUFDSCxDQUFDO0lBRUQsd0NBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLGNBQWMsRUFBRTtZQUMxQixJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN0RTtRQUNELElBQUksT0FBTyxDQUFDLGVBQWUsRUFBRTtZQUMzQixJQUFJLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN4RTtRQUNELElBQUksT0FBTyxDQUFDLGVBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFO1lBQ25FLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhEQUE4RCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzFGLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7SUFFTyx1REFBMEIsR0FBbEMsVUFBbUMsUUFBa0Q7UUFBckYsaUJBMkJDO1FBMUJDLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ25DLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMvQztRQUNELElBQUksUUFBUSxFQUFFO1lBQ1osSUFBSSxDQUFDLDBCQUEwQixHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLO2dCQUN4RCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxLQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO2lCQUNwRjtxQkFBTSxJQUFJLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3pGLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhEQUE4RCxHQUFHLEtBQUksQ0FBQyx3QkFBd0I7d0JBQzdHLDBDQUEwQyxDQUFDLENBQUM7aUJBQy9DO3FCQUFNLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFO29CQUM1QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyw2RUFBNkUsQ0FBQyxDQUFDO2lCQUNqRztxQkFBTSxJQUFJLEtBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHdFQUF3RSxDQUFDLENBQUM7aUJBQzVGO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7b0JBQzdELEtBQUksQ0FBQyxNQUFNLENBQUM7d0JBQ1YsTUFBTSxFQUFFLHFCQUFxQixDQUFDLElBQUk7d0JBQ2xDLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixZQUFZLEVBQUUsS0FBSyxDQUFDLFlBQVksS0FBSyxJQUFJLElBQUksS0FBSyxDQUFDLFlBQVksS0FBSyxLQUFLLENBQUMsQ0FBQzs0QkFDekUsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGdDQUFnQztxQkFDN0QsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTyx3REFBMkIsR0FBbkMsVUFBb0MsUUFBZ0I7UUFBcEQsaUJBNkJDO1FBNUJDLElBQUksSUFBSSxDQUFDLDJCQUEyQixFQUFFO1lBQ3BDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNoRDtRQUNELElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzdCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7U0FDbEM7UUFDRCxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQywyQkFBMkIsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDekUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxLQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO2lCQUNwRjtxQkFBTSxJQUFJLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZGLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDJEQUEyRCxHQUFHLEtBQUksQ0FBQyx3QkFBd0I7d0JBQzFHLDBDQUEwQyxDQUFDLENBQUM7aUJBQy9DO3FCQUFNLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFO29CQUM1QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwRUFBMEUsQ0FBQyxDQUFDO2lCQUM5RjtxQkFBTSxJQUFJLEtBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFFQUFxRSxDQUFDLENBQUM7aUJBQ3pGO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7b0JBQ3JELEtBQUksQ0FBQyxNQUFNLENBQUM7d0JBQ1YsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVE7d0JBQ3RDLFlBQVksRUFBRSxLQUFJLENBQUMsa0NBQWtDO3FCQUN0RCxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELGlCQUFpQjtJQUVWLG9DQUFPLEdBQWQsVUFBZSxVQUFvQjtRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFPLFVBQVUsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1NBQzVEO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRU0sNENBQWUsR0FBdEI7UUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQztJQUVNLDhDQUFpQixHQUF4QjtRQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDO0lBRU0sdUNBQVUsR0FBakIsVUFBa0IsTUFBMkM7UUFDM0QsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDckIsT0FBTyxVQUFVLENBQUMsdUNBQXVDLENBQUMsQ0FBQztTQUM1RDtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDhDQUE4QyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7SUFFTyxnREFBbUIsR0FBM0IsVUFBNEIsTUFBMkM7UUFBdkUsaUJBK0NDO1FBOUNBLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDakUsT0FBTztTQUNSO1FBRUQsSUFBSSxNQUFNLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLG9CQUFvQixFQUFFO1lBQy9GLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1FQUFtRTtnQkFDbEYsTUFBTSxDQUFDLGFBQWEsR0FBRyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNqSSxPQUFPO1NBQ1A7UUFFRCxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxXQUFXLEVBQW5ELENBQW1ELENBQUMsSUFBSSxJQUFJLENBQUM7U0FDOUc7UUFFRCxJQUFJLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtZQUMvQixJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEtBQUssc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDakcsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDdkU7UUFFRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO1lBQy9DLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2hEO1FBRUQsSUFBSSxJQUFJLENBQUMsc0JBQXNCLElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRixJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsZ0JBQUksT0FBQSxhQUFBLE1BQU0sMENBQUUsWUFBWSwwQ0FBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTSxDQUFDLENBQUMsQ0FBQSxFQUFBLENBQUMsQ0FBQztTQUNuSDtRQUVELElBQUksSUFBSSxDQUFDLDZCQUE2QixJQUFJLE1BQU0sQ0FBQyxjQUFjLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDaEcsSUFBSSxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxnQkFBSSxPQUFBLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxhQUFBLE1BQU0sMENBQUUsY0FBYywwQ0FBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTSxDQUFDLENBQUMsQ0FBQSxFQUFBLENBQUMsQ0FBQztTQUN0STtRQUVELElBQUksSUFBSSxDQUFDLDZCQUE2QixJQUFJLE1BQU0sQ0FBQyxjQUFjLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDL0YsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxnQkFBSSxPQUFBLGFBQUEsTUFBTSwwQ0FBRSxjQUFjLDBDQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFNLENBQUMsQ0FBQyxDQUFBLEVBQUEsQ0FBQyxDQUFDO1NBQzlHO1FBRUQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtZQUNsRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDbkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxNQUFNLENBQUMsUUFBUSxLQUFLLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxJQUFJLElBQUksQ0FBQztTQUNoRztRQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDakQsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO0lBQ3hFLENBQUM7SUFFTyx5REFBNEIsR0FBcEMsVUFBcUMsTUFBMkM7UUFBaEYsaUJBZ0NDO1FBL0JBLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDakUsT0FBTztTQUNSO1FBRUQsSUFBSSxNQUFNLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLG9CQUFvQixFQUFFO1lBQy9GLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1FQUFtRTtnQkFDbEYsTUFBTSxDQUFDLGFBQWEsR0FBRyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNqSSxPQUFPO1NBQ1A7UUFFRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxJQUFJLENBQUMsOEJBQThCO2VBQ2hFLE1BQU0sQ0FBQyxzQkFBc0IsSUFBSSxNQUFNLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFFO1lBRTVFLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJOztnQkFDL0MsSUFBTSxFQUFFLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsSUFBSSxhQUFBLE1BQU0sMENBQUUsc0JBQXNCLDBDQUFFLE9BQU8sQ0FBQyxFQUFFLE9BQU0sQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxDQUFDLENBQUM7U0FDSDtRQUVELElBQUksSUFBSSxDQUFDLHlCQUF5QixJQUFJLElBQUksQ0FBQyw4QkFBOEI7ZUFDbkUsTUFBTSxDQUFDLHVCQUF1QixJQUFJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUU7WUFFOUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUk7O2dCQUNoRCxJQUFNLEVBQUUsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sRUFBRSxJQUFJLGFBQUEsTUFBTSwwQ0FBRSx1QkFBdUIsMENBQUUsT0FBTyxDQUFDLEVBQUUsT0FBTSxDQUFDLENBQUMsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlGLENBQUMsQ0FBQyxDQUFDO1NBQ0g7UUFFRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2pELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0lBRU8sbUNBQU0sR0FBZCxVQUFnQixPQUFnQztRQUFoRCxpQkE4QkM7UUE3QkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7UUFDMUMsSUFBTSxHQUFHLEdBQXFCLElBQUksVUFBVSxDQUFPLFVBQUEsVUFBVTtZQUMzRCxJQUFJO2dCQUNGLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDOUM7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBQSxPQUFPO1lBQ25CLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDMUMsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2FBQzNCO1lBRUQsS0FBSSxDQUFDLGlDQUFpQyxFQUFFLENBQUM7UUFDM0MsQ0FBQyxFQUFFLFVBQUEsT0FBTztZQUNSLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHFCQUFxQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2xELEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFO2dCQUN6QixLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzthQUMzQjtZQUVELEtBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRU8sK0NBQWtCLEdBQTFCLFVBQTJCLE9BQXlCLEVBQUUsT0FBZ0M7UUFBdEYsaUJBb0VDO1FBbkVDLElBQU0sWUFBWSxHQUErQyxPQUFPLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDO1FBRXBHLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRTVDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHNCQUFzQixFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBRXZELHNCQUFzQjtRQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRTtZQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUV4QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdEI7UUFDRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFFeEQsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLHFCQUFxQjtZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyw4REFBOEQsQ0FBQyxDQUFDO1lBQ2xGLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBRWpFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQWdDO2dCQUNqRixLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUUxRCxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRTtvQkFDeEIsSUFBSSxLQUFJLENBQUMsY0FBYyxLQUFLLHlCQUF5QixDQUFDLE1BQU0sRUFBRTt3QkFDNUQsS0FBSSxDQUFDLGlDQUFpQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUNsRDt5QkFBTTt3QkFDTCxLQUFJLENBQUMsaUNBQWlDLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztxQkFDdkU7b0JBQ0QsSUFBSSxZQUFZLEVBQUU7d0JBQ2hCLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLENBQUMsQ0FBQztxQkFDakQ7aUJBQ0Y7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0RBQW9ELENBQUMsQ0FBQztpQkFDeEU7Z0JBRUQsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNmLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNyQixDQUFDLEVBQUUsVUFBQSxPQUFPO2dCQUNSLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDRDQUE0QyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUV6RSxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRTtvQkFDeEIsS0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7aUJBQ3hCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG9EQUFvRCxDQUFDLENBQUM7aUJBQ3hFO2dCQUVELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsQ0FBQztTQUVKO2FBQU07WUFDTCwrREFBK0Q7WUFDL0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztZQUVwRSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDN0UsSUFBSSxZQUFZLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDakQ7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2FBQ3hFO1lBRUQsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2YsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQztJQUVPLDhEQUFpQyxHQUF6QyxVQUEwQyxRQUFnQztRQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFFckMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxRQUFRLENBQUMsVUFBVSxFQUFFO2dCQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQzthQUM3QztpQkFBTTtnQkFDTCxNQUFNLElBQUksS0FBSyxDQUFDLDREQUE0RCxDQUFDLENBQUM7YUFDL0U7WUFFRCxJQUFJLFFBQVEsQ0FBQyxhQUFhLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO2FBQ25EO2lCQUFNO2dCQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsK0RBQStELENBQUMsQ0FBQzthQUNsRjtTQUNGO0lBQ0gsQ0FBQztJQUVPLDhEQUFpQyxHQUF6QyxVQUEwQyxJQUFXLEVBQUUsT0FBOEI7UUFDbkYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUVBQWlFLENBQUMsQ0FBQztRQUNyRixJQUFNLGdCQUFnQixHQUFHLHVCQUF1QixDQUFDLGFBQWEsQ0FDNUQsSUFBSSxFQUNKLE9BQU8sRUFDUCxJQUFJLENBQUMsaUJBQWlCLEVBQ3RCLElBQUksQ0FBQywwQkFBMEIsRUFDL0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUUsQ0FBQztRQUU1QixJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztRQUM3QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsT0FBTyxnQkFBZ0IsQ0FBQyxhQUFhLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQztRQUN6SCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxnQkFBZ0IsQ0FBQyxVQUFVLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztJQUNsSCxDQUFDO0lBRU8sNkNBQWdCLEdBQXhCOzs7UUFDRSxJQUFNLE1BQU0sR0FBMEI7WUFDcEMsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ2pFLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDaEUsSUFBSSxFQUFFLEVBQUU7WUFDUixLQUFLLEVBQUUsSUFBSTtZQUNYLFdBQVcsRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBRUYsSUFBSSxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3pELE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3ZDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQTFDLENBQTBDLENBQUUsQ0FBQztTQUNqSDtRQUVELElBQUksSUFBSSxDQUFDLDZCQUE2QixJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUU7O2dCQUMzRSxLQUFxQixJQUFBLEtBQUEsU0FBQSxJQUFJLENBQUMscUJBQXFCLENBQUEsZ0JBQUEsNEJBQUU7b0JBQTVDLElBQU0sTUFBTSxXQUFBO29CQUNmLE1BQUEsTUFBTSxDQUFDLE9BQU8sMENBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO2lCQUMzQzs7Ozs7Ozs7O1NBQ0Y7UUFFRCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDMUMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1FBQ2hELElBQUksVUFBVSxFQUFFO1lBQ2QsTUFBQSxNQUFNLENBQUMsSUFBSSwwQ0FBRSxJQUFJLENBQUM7Z0JBQ2hCLFFBQVEsRUFBRSxVQUFVLENBQUMsV0FBVyxJQUFJLFVBQVUsQ0FBQyxLQUFLLElBQUksSUFBSTtnQkFDNUQsU0FBUyxFQUFFLGFBQWEsSUFBSSxzQkFBc0IsQ0FBQyxTQUFTO2FBQzdELEVBQUU7U0FDSjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxvQ0FBTyxHQUFmLFVBQWdCLEtBQWE7UUFDM0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxzQkFBSSxnRUFBZ0M7YUFBcEM7O1lBQ0UsSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyx5QkFBeUIsS0FBSyxLQUFLLEVBQUU7Z0JBQ3ZGLE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDO2FBQ3ZDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHlCQUEwQixDQUFDO2FBQ3pGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrRUFBa0M7YUFBdEM7O1lBQ0UsSUFBSSxJQUFJLENBQUMsMkJBQTJCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQywyQkFBMkIsS0FBSyxLQUFLLEVBQUU7Z0JBQzNGLE9BQU8sSUFBSSxDQUFDLDJCQUEyQixDQUFDO2FBQ3pDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHlCQUEwQixDQUFDO2FBQ3pGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx3REFBd0I7YUFBNUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO29CQUN0RSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7aUJBQzdCO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBMkMsQ0FBQyxDQUFDO2lCQUMzRDthQUNGO1lBQ0QsT0FBTyxDQUFDLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsT0FBTywwQ0FBRSxlQUFnQixDQUFDLENBQUM7UUFDbEYsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzREFBc0I7YUFBMUI7O1lBQ0UsT0FBTyxJQUFJLENBQUMsZUFBZSxLQUFJLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsT0FBTywwQ0FBRSxlQUFnQixDQUFBLENBQUM7UUFDeEcsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4Q0FBYzthQUFsQjtZQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBRUQsc0JBQUksaURBQWlCO2FBQXJCOztZQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7Z0JBQ3pELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUN4QjtpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsV0FBVywwQ0FBRSxvQkFBcUIsQ0FBQzthQUN4RjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksaURBQWlCO2FBQXJCOztZQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7Z0JBQ3pELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUN4QjtpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsV0FBVywwQ0FBRSxvQkFBcUIsQ0FBQzthQUN4RjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksdURBQXVCO2FBQTNCOztZQUNFLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssS0FBSyxFQUFFO2dCQUNyRSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsVUFBVSwwQ0FBRSxnQkFBaUIsQ0FBQzthQUNuRjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUkseURBQXlCO2FBQTdCOztZQUNFLElBQUksSUFBSSxDQUFDLGtCQUFrQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssS0FBSyxFQUFFO2dCQUN6RSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQzthQUNoQztpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsWUFBWSwwQ0FBRSxnQkFBaUIsQ0FBQzthQUNyRjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksNkRBQTZCO2FBQWpDOztZQUNFLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssS0FBSyxFQUFFO2dCQUNqRixPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQzthQUNwQztpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsZ0JBQWdCLDBDQUFFLGdCQUFpQixDQUFDO2FBQ3pGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyREFBMkI7YUFBL0I7O1lBQ0UsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsS0FBSyxLQUFLLEVBQUU7Z0JBQzdFLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDO2FBQ2xDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxVQUFVLDBDQUFFLGlDQUFrQyxDQUFDO2FBQ3BHO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpRUFBaUM7YUFBckM7O1lBQ0UsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQywwQkFBMEIsS0FBSyxLQUFLLEVBQUU7Z0JBQ3pGLE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDO2FBQ3hDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLGlDQUFrQyxDQUFDO2FBQ3RHO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx5REFBeUI7YUFBN0I7O1lBQ0UsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxLQUFLLEVBQUU7Z0JBQ3pFLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO2FBQ2hDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLGdCQUFpQixDQUFDO2FBQ3JGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwREFBMEI7YUFBOUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsS0FBSyxLQUFLLEVBQUU7Z0JBQzNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO2FBQ2pDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLDZCQUE4QixDQUFDO2FBQzdGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyREFBMkI7YUFBL0I7O1lBQ0UsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsS0FBSyxLQUFLLEVBQUU7Z0JBQzdFLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDO2FBQ2xDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLDhCQUErQixDQUFDO2FBQzlGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw2REFBNkI7YUFBakM7O1lBQ0UsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxLQUFLLEVBQUU7Z0JBQ2pGLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO2FBQ3BDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxjQUFjLDBDQUFFLGdCQUFpQixDQUFDO2FBQ3ZGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzREFBc0I7YUFBMUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssRUFBRTtnQkFDbkUsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO2FBQzdCO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLDBDQUFFLGdCQUFpQixDQUFDO2FBQ2xGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx3REFBd0I7YUFBNUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxLQUFLLEVBQUU7Z0JBQ3ZFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO2FBQy9CO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLGlDQUFrQyxDQUFDO2FBQ3RHO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzREFBc0I7YUFBMUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssRUFBRTtnQkFDbkUsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO2FBQzdCO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxZQUFZLDBDQUFFLHlCQUEwQixDQUFDO2FBQzlGO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw2REFBNkI7YUFBakM7O1lBQ0UsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxLQUFLLEVBQUU7Z0JBQ2pGLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO2FBQ3BDO2lCQUFNO2dCQUNMLE9BQU8sTUFBQSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxnQkFBZ0IsMENBQUUsZ0JBQWlCLENBQUM7YUFDekY7UUFDSCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHNEQUFzQjthQUExQjs7WUFDRSxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssS0FBSyxFQUFFO2dCQUNuRSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFlBQVksMENBQUUsZ0JBQWlCLENBQUM7YUFDckY7UUFDSCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFEQUFxQjthQUF6Qjs7WUFDRSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3ZCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQzthQUM1QjtpQkFBTTtnQkFDTCxPQUFPLE1BQUEsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsVUFBVSwwQ0FBRSxxQkFBc0IsQ0FBQzthQUN4RjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksOERBQThCO2FBQWxDO1lBQ0UsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMseUJBQXlCLENBQUM7UUFDakUsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrRUFBa0M7YUFBdEM7WUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLDZCQUE2QixDQUFDO1FBQ2pFLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOERBQThCO2FBQWxDO1lBQ0UsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnRUFBZ0M7YUFBcEM7WUFDRSxPQUFPLElBQUksQ0FBQyx3QkFBd0IsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsMkJBQTJCLENBQUM7UUFDdkgsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrRUFBa0M7YUFBdEM7WUFDRSxPQUFPLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyw2QkFBNkIsQ0FBQztRQUNyRyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdFQUFnQzthQUFwQztZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLDJCQUEyQixDQUFDO1FBQ25HLENBQUM7OztPQUFBO0lBRUQsc0JBQUksK0RBQStCO2FBQW5DO1lBQ0UsT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsMEJBQTBCLENBQUM7UUFDaEcsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpRUFBaUM7YUFBckM7WUFDRSxPQUFPLElBQUksQ0FBQyw2QkFBNkIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQzFHLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMkRBQTJCO2FBQS9CO1lBQ0UsT0FBTyxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDMUUsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx3REFBd0I7YUFBNUI7O1lBQ0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDekQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsT0FBTyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFVBQVUsMENBQUUsd0JBQXlCLENBQUM7YUFDM0Y7UUFDSCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGtEQUFrQjthQUF0QjtZQUNFLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO2dCQUM1QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQzthQUNqQztpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnREFBZ0I7YUFBcEI7WUFDRSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxtREFBbUI7YUFBdkI7WUFDRSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBZTthQUFuQjs7WUFDRSxJQUFNLEdBQUcsR0FBRyxNQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFVBQVUsMENBQUUsZUFBZ0IsQ0FBQztZQUN0RixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDOUI7aUJBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUMvQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7YUFDN0I7aUJBQU0sSUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTTtnQkFDcEMsR0FBRztnQkFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNuRCxPQUFPLEdBQUcsQ0FBQzthQUNkO2lCQUFNLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sRUFBRTtnQkFDL0MsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekM7aUJBQU0sSUFBSSxHQUFHLEVBQUU7Z0JBQ2QsT0FBTyxHQUFHLENBQUM7YUFDWjtpQkFBTTtnQkFDTCxPQUFPLEVBQUUsQ0FBQzthQUNYO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnREFBZ0I7YUFBcEI7WUFDRSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDMUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7YUFDL0I7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLENBQUM7YUFDVjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksaURBQWlCO2FBQXJCO1lBQUEsaUJBa0JDO1lBakJDLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUMzQix3Q0FBd0M7Z0JBQ3hDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLFlBQUksT0FBQSxDQUFDLENBQUMsSUFBSSxZQUFLLEtBQUksQ0FBQyxrQkFBa0IsMENBQUUsSUFBSSxDQUFBLENBQUEsRUFBQSxDQUFDLElBQUksSUFBSSxDQUFDO2FBQ3hGO2lCQUFNLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUNwQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxJQUFJLEtBQUssS0FBSSxDQUFDLG9CQUFvQixFQUFwQyxDQUFvQyxDQUFDLENBQUM7Z0JBQ3hGLElBQUksV0FBVyxFQUFFO29CQUNmLE9BQU8sV0FBVyxDQUFDO2lCQUNwQjtxQkFBTTtvQkFDTCw4QkFBOEI7b0JBQzlCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO3dCQUNwQyxPQUFBLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQXpGLENBQXlGLENBQUMsSUFBSSxJQUFJLENBQUM7aUJBQ3RHO2FBQ0Y7aUJBQU07Z0JBQ0wsOEJBQThCO2dCQUM5QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtvQkFDcEMsT0FBQSx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUF6RixDQUF5RixDQUFDLElBQUksSUFBSSxDQUFDO2FBQ3RHO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxvREFBb0I7YUFBeEI7WUFDRSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtnQkFDOUIsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7YUFDbkM7aUJBQU0sSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7Z0JBQ3ZDLE9BQU8sSUFBSSxDQUFDLHVCQUF1QixLQUFLLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUMzRSxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQzthQUN0RTtpQkFBTTtnQkFDTCxpQkFBaUI7Z0JBQ2pCLE9BQU8sc0JBQXNCLENBQUMsU0FBUyxDQUFDO2FBQ3pDO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyQ0FBVzthQUFmO1lBQ0UsT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsQ0FBQztRQUNqQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHlDQUFTO2FBQWI7WUFDRSxJQUFNLFVBQVUsR0FBRyxDQUFDLENBQUM7WUFDckIsSUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLElBQU0sYUFBYSxHQUFHLENBQUMsQ0FBQztZQUV4QixJQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDakIsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1lBQ3RDLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUMzQyxJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFFdkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sRUFBRSxDQUFDLEVBQUcsRUFBRTtnQkFDakMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLEVBQUU7b0JBQ3ZHLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ25CLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2Y7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDZixLQUFLLENBQUMsSUFBSSxDQUFDOzRCQUNULElBQUksRUFBRSxJQUFJO3lCQUNYLENBQUMsQ0FBQzt3QkFDSCxVQUFVLEdBQUcsSUFBSSxDQUFDO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGtEQUFrQjthQUF0QjtZQUNFLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1lBQ3hDLElBQUksSUFBSSxDQUFDLHNCQUFzQixFQUFFO2dCQUMvQixNQUFNLEVBQUcsQ0FBQzthQUNYO1lBQ0QsSUFBSSxJQUFJLENBQUMsa0NBQWtDLEVBQUU7Z0JBQzNDLE1BQU0sRUFBRyxDQUFDO2FBQ1g7WUFDRCxJQUFJLElBQUksQ0FBQyw4QkFBOEIsRUFBRTtnQkFDdkMsTUFBTSxFQUFHLENBQUM7YUFDWDtZQUNELE9BQU8sTUFBTSxDQUFDO1FBQ2hCLENBQUM7OztPQUFBO0lBRUQsc0JBQUkseUNBQVM7YUFBYjtZQUNFLElBQUksSUFBSSxDQUFDLGNBQWMsS0FBSyx5QkFBeUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzVELE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUNqQztpQkFBTTtnQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7YUFDbEM7UUFDSCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLCtDQUFlO2FBQW5CO1lBQUEsaUJBRUM7WUFEQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsd0JBQXdCLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFwRixDQUFvRixDQUFDLENBQUM7UUFDeEgsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4Q0FBYzthQUFsQjtZQUFBLGlCQUVDO1lBREMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQXJELENBQXFELENBQUMsQ0FBQztRQUN6RixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHFEQUFxQjthQUF6QjtZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsSUFBSSxFQUFFLENBQUM7UUFDbkMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxvREFBb0I7YUFBeEI7WUFDRSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksRUFBRSxDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQWM7YUFBbEI7WUFDRSxPQUFPLElBQUksQ0FBQyxjQUFjLElBQUksRUFBRSxDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMENBQVU7YUFBZDtZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksb0RBQW9CO2FBQXhCO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNqRixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGlEQUFpQjthQUFyQjtZQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQzVCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sRUFBRTtnQkFDM0MsT0FBTyxLQUFLLENBQUM7YUFDZDtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxvREFBb0I7YUFBeEI7WUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUM1QixPQUFPLEtBQUssQ0FBQzthQUNkO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzNDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksaURBQWlCO2FBQXJCO1lBQUEsaUJBRUM7WUFEQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1FBQ3hELENBQUM7OztPQUFBO0lBRUQsc0JBQUksdURBQXVCO2FBQTNCO1lBQUEsaUJBRUM7WUFEQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO1FBQ3pFLENBQUM7OztPQUFBO0lBRUQsc0JBQUksdURBQXVCO2FBQTNCO1lBQUEsaUJBRUM7WUFEQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO1FBQ3ZFLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0VBQXNDO2FBQTFDOztZQUNFLGFBQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsY0FBYywwQ0FBRSxnQkFBZ0IsQ0FBQztRQUN2RixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHNFQUFzQzthQUExQzs7WUFDRSxhQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsMENBQUUsdUJBQXVCLENBQUM7UUFDekYsQ0FBQzs7O09BQUE7SUFRRCxzQkFBSSwwQ0FBVTthQUFkO1lBQ0UsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDaEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwQ0FBVTthQUFkO1lBQ0UsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDaEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBZTthQUFuQjtZQUNFLElBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ3BDLE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQzs7O09BQUE7SUFFRCwrQ0FBa0IsR0FBbEI7O1FBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO1lBQ3ZELElBQU0sZUFBZSxHQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDeEgsSUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDOztnQkFDakIsS0FBK0IsSUFBQSxvQkFBQSxTQUFBLGVBQWUsQ0FBQSxnREFBQSw2RUFBRTtvQkFBM0MsSUFBTSxnQkFBZ0IsNEJBQUE7b0JBQ3pCLElBQU0sY0FBYyxHQUFVLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQ2xFLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7OzRCQUMzQyxLQUFzQixJQUFBLGtDQUFBLFNBQUEsY0FBYyxDQUFBLENBQUEsOENBQUEsMEVBQUU7Z0NBQWpDLElBQU0sT0FBTywyQkFBQTtnQ0FDaEIsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs2QkFDckI7Ozs7Ozs7OztxQkFDRjtpQkFDRjs7Ozs7Ozs7O1lBQ0QsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDcEIsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDbkUsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDM0I7U0FDRjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxlQUFlO0lBRWYsOENBQWlCLEdBQWpCLFVBQWtCLEtBQXlCLEVBQUUsSUFBd0IsRUFBRSxFQUFzQjtRQUMzRixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7WUFDcEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSTtZQUNyQixLQUFLLE9BQUE7WUFDTCxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hDLGNBQWMsRUFBRSxFQUFFLENBQUMsZUFBZSxFQUFFO1lBQ3BDLGFBQWEsRUFBRSxJQUFJO1lBQ25CLFdBQVcsRUFBRSxFQUFFO1NBQ2hCLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBaUIsR0FBakIsVUFBa0IsS0FBeUI7UUFBM0MsaUJBc0NDO1FBckNDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekUsSUFBTSxhQUFhLEdBQThCLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNyRyxJQUFNLGFBQWEsR0FBOEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU3RixJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDekMsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUU7WUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztZQUN4RSxPQUFPO1NBQ1I7UUFFRCxJQUFJLGFBQWEsRUFBRTtZQUNqQixhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQztTQUN0RTtRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHdCQUF3QixHQUFHLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztZQUNwQixJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ3JCLEtBQUssT0FBQTtZQUNMLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ3hFLGNBQWMsRUFBRSxhQUFhLENBQUMsZUFBZSxFQUFFO1lBQy9DLGFBQWEsRUFBRSxhQUFhO1lBQzVCLFdBQVcsRUFBRSxhQUFhO1NBQzNCLENBQUMsQ0FBQztRQUVILFlBQVk7UUFDWixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ2QsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsRUFBRCxDQUFDLENBQUMsQ0FBQztZQUNsRCxLQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3pCLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLEVBQXpCLENBQXlCLENBQUMsQ0FBQztZQUM1QyxVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxFQUExQixDQUEwQixDQUFDLENBQUM7WUFDN0MsS0FBSSxDQUFDLGlDQUFpQyxFQUFFLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0RBQW1CLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxnREFBbUIsR0FBbkIsVUFBb0IsSUFBUztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLDhCQUE4QixFQUFFO1lBQ3hDLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzVCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNoRTthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7YUFDekI7WUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMvQjtRQUVELElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsaURBQW9CLEdBQXBCLFVBQXFCLE1BQXdCO1FBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEVBQUU7WUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUNoRCxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3hDLE9BQU87U0FDUjtRQUVELElBQU0sZUFBZSxHQUF1QztZQUMxRCxNQUFNLFFBQUE7WUFDTixhQUFhLEVBQUUsSUFBSSxDQUFDLFlBQVk7U0FDakMsQ0FBQztRQUVGLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHFCQUFxQixHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsZUFBZSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRTFGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCw0Q0FBZSxHQUFmLFVBQWdCLFFBQWdCO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxnREFBbUIsR0FBbkIsVUFBb0IsTUFBd0I7UUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBQ2hELE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDdkMsT0FBTztTQUNSO1FBRUQsSUFBTSxlQUFlLEdBQXVDO1lBQzFELE1BQU0sUUFBQTtZQUNOLGFBQWEsRUFBRSxJQUFJLENBQUMsWUFBWTtTQUNqQyxDQUFDO1FBRUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxlQUFlLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFFMUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELGdEQUFtQixHQUFuQjtRQUFBLGlCQWlCQzs7UUFoQkMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLFVBQVUsQ0FBQzs7WUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLHNCQUFzQixJQUFJLEVBQUUsQ0FBQyxFQUFFO2dCQUM1RSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDNUIsVUFBSSxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLDBDQUFFLHVCQUF1QixFQUFFO3dCQUNuRixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztxQkFDNUI7eUJBQU07d0JBQ0wsMERBQTBEO3FCQUMzRDtpQkFDRjtxQkFBTTtvQkFDTCxVQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsMENBQUUsc0JBQXNCLEVBQUU7d0JBQ2xGLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO3FCQUM1QjtpQkFDRjthQUNGO1FBQ0gsQ0FBQyxRQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsMENBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQseUNBQVksR0FBWixVQUFhLFNBQWlCLEVBQUUsT0FBaUM7UUFDL0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssU0FBUyxFQUFFO1lBQ3ZDLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLEdBQUcsRUFBQyxNQUFNLEVBQUUscUJBQXFCLENBQUMsSUFBSSxFQUFDLENBQUM7U0FDaEQ7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCwwQ0FBYSxHQUFiLFVBQWMsTUFBd0I7UUFDcEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsRUFBRTtZQUM5RixPQUFPO1NBQ1I7UUFFRCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDMUMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1FBRWhELElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLElBQUksRUFBRTtZQUNqRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxhQUFhLEtBQUssc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2pGLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDekU7YUFBTTtZQUNMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUM7WUFDakMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztTQUMvRDtRQUVELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV2QixrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxvQkFBb0I7SUFFcEIsOENBQWlCLEdBQWpCLFVBQWtCLElBQVM7UUFDekIsSUFBSSxJQUFJLENBQUMsOEJBQThCLEVBQUU7WUFDdkMsSUFBSyxJQUFJLENBQUMsY0FBc0IsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hDLE9BQVEsSUFBSSxDQUFDLGNBQStDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVFO2lCQUFNO2dCQUNMLE9BQU8sd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUF3QixDQUFDLENBQUM7YUFDdkY7U0FDRjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCw2Q0FBZ0IsR0FBaEI7UUFDRSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNoRCxDQUFDO0lBRUQseUNBQVksR0FBWixVQUFhLEdBQVEsRUFBRSxNQUF3QjtRQUM3QyxPQUFPLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVELHlDQUFZLEdBQVosVUFBYSxNQUFxRTtRQUNoRixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsZ0RBQW1CLEdBQW5CLFVBQW9CLEdBQThCLEVBQUUsS0FBaUM7UUFDbkYsSUFBSSxHQUFHLEVBQUU7WUFDUCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDM0M7YUFBTSxJQUFJLEtBQUssRUFBRTtZQUNoQixPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELG1EQUFzQixHQUF0QixVQUF1QixNQUF3QixFQUFFLFNBQWdCO1FBQWhCLDBCQUFBLEVBQUEsZ0JBQWdCO1FBQy9ELElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUMxQyxJQUFJLFVBQVUsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsSUFBSSxFQUFFO1lBQzNELElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2QsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sU0FBUyxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztTQUNoRDthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7SUFFRCxtREFBc0IsR0FBdEIsVUFBdUIsTUFBd0I7UUFDN0MsT0FBTyx3QkFBd0IsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUMvRyxDQUFDO0lBRUQsa0RBQXFCLEdBQXJCLFVBQXNCLE1BQXdCO1FBQzVDLE9BQU8sd0JBQXdCLENBQUMscUJBQXFCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDOUcsQ0FBQztJQUVELHlDQUFZLEdBQVosVUFBYSxHQUFRO1FBQ25CLE9BQU8sSUFBSSxDQUFDLHlCQUF5QjtZQUNuQyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFDaEcsQ0FBQztJQUVELHVDQUFVLEdBQVYsVUFBVyxNQUF3QjtRQUNqQyxPQUFPLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUNuRyxDQUFDO0lBRUQsdUNBQVUsR0FBVixVQUFXLE1BQXdCO1FBQ2pDLE9BQU8sd0JBQXdCLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLENBQUM7SUFFRCx5Q0FBWSxHQUFaLFVBQWEsTUFBd0I7UUFDbkMsT0FBTyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDckcsQ0FBQztJQUVELHFDQUFRLEdBQVIsVUFBUyxNQUF3QjtRQUMvQixPQUFPLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRUQsa0RBQXFCLEdBQXJCLFVBQXNCLE1BQXdCO1FBQzVDLE9BQU8sd0JBQXdCLENBQUMscUJBQXFCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDOUcsQ0FBQztJQUVELG1EQUFzQixHQUF0QixVQUF1QixNQUF3Qjs7UUFDN0MsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sUUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHVDQUF1QyxDQUFDLENBQUM7SUFDckksQ0FBQztJQUVELGtEQUFxQixHQUFyQixVQUFzQixNQUF3Qjs7UUFDNUMsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sUUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxPQUFPLDBDQUFFLHNDQUFzQyxDQUFDLENBQUM7SUFDcEksQ0FBQztJQUVELDRDQUFlLEdBQWYsVUFBZ0IsTUFBd0IsRUFBRSxHQUFtRDtRQUMzRixJQUFNLE9BQU8sR0FBRyxNQUFNLENBQUMsbUJBQW1CLElBQUksR0FBRyxDQUFDO1FBQ2xELElBQUksT0FBTyxLQUFLLGtDQUFrQyxDQUFDLE1BQU0sRUFBRTtZQUN6RCxPQUFPLElBQUksQ0FBQztTQUNiO2FBQU0sSUFBSSxPQUFPLEtBQUssa0NBQWtDLENBQUMsS0FBSyxFQUFFO1lBQy9ELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLE9BQU8sS0FBSyxrQ0FBa0MsQ0FBQyxPQUFPLEVBQUU7WUFDakUsc0NBQXNDO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG1FQUFtRSxDQUFDLENBQUM7Z0JBQ3ZGLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7WUFFRCxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsQ0FBQztTQUN2RTtRQUVELElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO1FBQzdDLElBQUksT0FBTyxLQUFLLGtDQUFrQyxDQUFDLGdCQUFnQixFQUFFO1lBQ25FLE9BQU8sV0FBVyxLQUFLLENBQUMsQ0FBQztTQUMxQjthQUFNLElBQUksT0FBTyxLQUFLLGtDQUFrQyxDQUFDLGtCQUFrQixFQUFFO1lBQzVFLE9BQU8sV0FBVyxHQUFHLENBQUMsQ0FBQztTQUN4QjthQUFNLElBQUksT0FBTyxLQUFLLGtDQUFrQyxDQUFDLFlBQVksRUFBRTtZQUN0RSxPQUFPLFdBQVcsR0FBRyxDQUFDLENBQUM7U0FDeEI7YUFBTTtZQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsdUNBQXVDLEdBQUcsT0FBTyxDQUFDLENBQUM7U0FDcEU7SUFDSCxDQUFDO0lBRUQsK0JBQStCO0lBRS9CLHVDQUFVLEdBQVYsVUFBVyxJQUFTO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDRCQUE0QjtJQUU1QixzQ0FBUyxHQUFULFVBQVUsSUFBUztRQUNqQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxzQkFBSSwwQ0FBVTthQUFkOztZQUNFLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7O2dCQUNELEtBQW1CLElBQUEsY0FBQSxTQUFBLFNBQVMsQ0FBQSxvQ0FBQSwyREFBRTtvQkFBekIsSUFBTSxJQUFJLHNCQUFBO29CQUNiLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQzFDLE9BQU8sS0FBSyxDQUFDO3FCQUNkO2lCQUNGOzs7Ozs7Ozs7WUFDRCxPQUFPLElBQUksQ0FBQztRQUNkLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMENBQVU7YUFBZDs7WUFDRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO2dCQUNyQixPQUFPLEtBQUssQ0FBQzthQUNkOztnQkFDRCxLQUFtQixJQUFBLGNBQUEsU0FBQSxTQUFTLENBQUEsb0NBQUEsMkRBQUU7b0JBQXpCLElBQU0sSUFBSSxzQkFBQTtvQkFDYixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUMxQyxPQUFPLElBQUksQ0FBQztxQkFDYjtpQkFDRjs7Ozs7Ozs7O1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDJDQUFXO2FBQWY7WUFDRSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQUVELDBDQUFhLEdBQWIsVUFBYyxJQUFTO1FBQ3JCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM5RDthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7YUFDeEI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQsNkNBQWdCLEdBQWhCOztRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDaEMsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNsQyxPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7U0FDeEI7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDOztnQkFDdkIsS0FBaUIsSUFBQSxLQUFBLFNBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBOUIsSUFBTSxFQUFFLFdBQUE7b0JBQ1gsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzVCOzs7Ozs7Ozs7U0FDRjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQsd0NBQXdDO0lBRXhDLHdEQUEyQixHQUEzQixVQUE0QixJQUFzQjtRQUNoRCxPQUFPLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHNCQUFJLDBEQUEwQjthQUE5Qjs7WUFDRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7WUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7O2dCQUNELEtBQW1CLElBQUEsY0FBQSxTQUFBLFNBQVMsQ0FBQSxvQ0FBQSwyREFBRTtvQkFBekIsSUFBTSxJQUFJLHNCQUFBO29CQUNiLElBQUksSUFBSSxDQUFDLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDeEQsT0FBTyxLQUFLLENBQUM7cUJBQ2Q7aUJBQ0Y7Ozs7Ozs7OztZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwREFBMEI7YUFBOUI7O1lBQ0UsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1lBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO2dCQUNyQixPQUFPLEtBQUssQ0FBQzthQUNkOztnQkFDRCxLQUFtQixJQUFBLGNBQUEsU0FBQSxTQUFTLENBQUEsb0NBQUEsMkRBQUU7b0JBQXpCLElBQU0sSUFBSSxzQkFBQTtvQkFDYixJQUFJLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3hELE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGOzs7Ozs7Ozs7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksd0RBQXdCO2FBQTVCO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQztRQUMxQyxDQUFDOzs7T0FBQTtJQUVELHlEQUE0QixHQUE1QixVQUE2QixJQUFzQjtRQUNqRCxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQ2hDLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzVCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMxRjthQUFNO1lBQ0wsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM1QztRQUVELElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtFQUFrRSxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLG9DQUFvQyxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVELDZEQUFnQyxHQUFoQzs7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQ2hDLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ25DLElBQUksQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUM7U0FDdEM7YUFBTTtZQUNMLElBQUksQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUM7O2dCQUNyQyxLQUFpQixJQUFBLEtBQUEsU0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUEsZ0JBQUEsNEJBQUU7b0JBQXBDLElBQU0sRUFBRSxXQUFBO29CQUNYLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzFDOzs7Ozs7Ozs7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtFQUFrRSxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLG9DQUFvQyxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVELHNDQUFzQztJQUV0Qyw0REFBK0IsR0FBL0IsVUFBZ0MsSUFBc0I7UUFDcEQsT0FBTyxJQUFJLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxzQkFBSSx3REFBd0I7YUFBNUI7O1lBQ0UsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtnQkFDckIsT0FBTyxLQUFLLENBQUM7YUFDZDs7Z0JBQ0QsS0FBbUIsSUFBQSxjQUFBLFNBQUEsU0FBUyxDQUFBLG9DQUFBLDJEQUFFO29CQUF6QixJQUFNLElBQUksc0JBQUE7b0JBQ2IsSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUM1RCxPQUFPLEtBQUssQ0FBQztxQkFDZDtpQkFDRjs7Ozs7Ozs7O1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHdEQUF3QjthQUE1Qjs7WUFDRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO2dCQUNyQixPQUFPLEtBQUssQ0FBQzthQUNkOztnQkFDRCxLQUFtQixJQUFBLGNBQUEsU0FBQSxTQUFTLENBQUEsb0NBQUEsMkRBQUU7b0JBQXpCLElBQU0sSUFBSSxzQkFBQTtvQkFDYixJQUFJLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQzVELE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGOzs7Ozs7Ozs7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0RBQXNCO2FBQTFCO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztRQUN4QyxDQUFDOzs7T0FBQTtJQUVELHVEQUEwQixHQUExQixVQUEyQixJQUFzQjtRQUMvQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFO1lBQ25JLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzlDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNsRzthQUFNO1lBQ0wsSUFBSSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNoRDtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsa0NBQWtDLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsMkRBQThCLEdBQTlCOztRQUFBLGlCQWlCQztRQWhCQyxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFO1lBQ3ZDLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLElBQUksQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUM7Z0JBQ3pELE9BQUEsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQXJGLENBQXFGLENBQUMsQ0FBQztTQUMxRjthQUFNO1lBQ0wsSUFBSSxDQUFDLDhCQUE4QixHQUFHLEVBQUUsQ0FBQzs7Z0JBQ3pDLEtBQWlCLElBQUEsS0FBQSxTQUFBLElBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUEsNEJBQUU7b0JBQTFCLElBQU0sRUFBRSxXQUFBO29CQUNYLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzlDOzs7Ozs7Ozs7U0FDRjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsa0NBQWtDLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsb0NBQW9DO0lBRXBDLG1EQUFzQixHQUF0QixVQUF1QixJQUE2QjtRQUNsRCxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVELHNCQUFJLHVEQUF1QjthQUEzQjs7WUFDRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUM7WUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7O2dCQUNELEtBQW1CLElBQUEsY0FBQSxTQUFBLFNBQVMsQ0FBQSxvQ0FBQSwyREFBRTtvQkFBekIsSUFBTSxJQUFJLHNCQUFBO29CQUNiLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDbkQsT0FBTyxLQUFLLENBQUM7cUJBQ2Q7aUJBQ0Y7Ozs7Ozs7OztZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx1REFBdUI7YUFBM0I7O1lBQ0UsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDO1lBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO2dCQUNyQixPQUFPLEtBQUssQ0FBQzthQUNkOztnQkFDRCxLQUFtQixJQUFBLGNBQUEsU0FBQSxTQUFTLENBQUEsb0NBQUEsMkRBQUU7b0JBQXpCLElBQU0sSUFBSSxzQkFBQTtvQkFDYixJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ25ELE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGOzs7Ozs7Ozs7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksdURBQXVCO2FBQTNCO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztRQUN2QyxDQUFDOzs7T0FBQTtJQUVELHVEQUEwQixHQUExQixVQUEyQixJQUE2QjtRQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFO1lBQ3ZDLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNoRjthQUFNO1lBQ0wsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDO29CQUM5RCxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxRSxDQUFDLENBQUMsQ0FBQzthQUNKO1NBQ0Y7UUFFRCxJQUFJLENBQUMsa0NBQWtDLEVBQUUsQ0FBQztRQUUxQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDLElBQUksRUFBQyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVPLGdEQUFtQixHQUEzQjtRQUNFLE9BQU87WUFDTCxhQUFhLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFLENBQUMsb0JBQW9CLElBQUksTUFBTTtZQUMxRixXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ3hFLG9CQUFvQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxJQUFJO1lBQ3ZELEtBQUssRUFBRSxJQUFJLENBQUMsa0JBQWtCO1lBQzlCLFlBQVksRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksRUFBTixDQUFNLENBQUM7WUFDOUQsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksRUFBTixDQUFNLENBQUM7WUFDcEQsY0FBYyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFOLENBQU0sQ0FBQztZQUMzRCxXQUFXLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtZQUNsQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDOUIsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxFQUFELENBQUMsQ0FBQztZQUMvRSxhQUFhLEVBQUcsQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEVBQUQsQ0FBQyxDQUFDO1NBQ3RGLENBQUM7SUFDSixDQUFDO0lBRU8sMkRBQThCLEdBQXRDO1FBQUEsaUJBZ0JDO1FBZkMsT0FBTztZQUNMLGFBQWEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsSUFBSSxNQUFNO1lBQzFGLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDeEUsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixJQUFJLElBQUk7WUFDdkQsS0FBSyxFQUFFLElBQUksQ0FBQyxrQkFBa0I7WUFDOUIsWUFBWSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFOLENBQU0sQ0FBQztZQUM5RCxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFOLENBQU0sQ0FBQztZQUNwRCxjQUFjLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQU4sQ0FBTSxDQUFDO1lBQzNELFdBQVcsRUFBRSxJQUFJLENBQUMsZ0JBQWdCO1lBQ2xDLFFBQVEsRUFBRSxJQUFJLENBQUMsZUFBZTtZQUM5QixzQkFBc0IsRUFBRSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2pFLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLENBQUMsRUFBSCxDQUFHLENBQUM7WUFDOUUsdUJBQXVCLEVBQUcsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxDQUFDLEVBQUgsQ0FBRyxDQUFDO1NBQ2hGLENBQUM7SUFDSixDQUFDO0lBRU8sMENBQWEsR0FBckI7UUFDRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2pELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztRQUN2RSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUUxRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU8saURBQW9CLEdBQTVCO1FBQUEsaUJBMkJDO1FBMUJDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3hCLE9BQU8sVUFBVSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDeEM7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFFLFVBQUEsVUFBVTtZQUMvQixJQUFJLEtBQUksQ0FBQyxrQ0FBa0MsRUFBRTtnQkFDM0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQztnQkFDekQsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtvQkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7d0JBQ3JCLE1BQU0sRUFBRSxLQUFJLENBQUMseUJBQXlCO3FCQUN2QyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTt3QkFDakIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQzt3QkFDaEUsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNsQixVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBRSxVQUFBLE9BQU87d0JBQ1IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscURBQXFELEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ2pGLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzFCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO29CQUN0RCxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0Y7WUFDRCxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbEIsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGdEQUFtQixHQUEzQjtRQUFBLGlCQXNCQztRQXJCQyxPQUFPLElBQUksVUFBVSxDQUFFLFVBQUEsVUFBVTtZQUMvQixJQUFJLEtBQUksQ0FBQyxrQ0FBa0MsRUFBRTtnQkFDM0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtvQkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO3dCQUN2QyxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO3dCQUNwRSxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN4QixVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBRSxVQUFBLE9BQU87d0JBQ1IsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsd0RBQXdELEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ3BGLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO29CQUN0RCxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0Y7WUFDRCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQkFBSSw4Q0FBYzthQUFsQjtZQUNFLE9BQU8sQ0FDTCxDQUFDLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxNQUFNO2dCQUNyQyxDQUFDLENBQUMsK0JBQStCLENBQUMsQ0FBQyxNQUFNO2dCQUN6QyxDQUFDLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxNQUFNO2dCQUMzQyxDQUFDLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxNQUFNLENBQzdDLEdBQUcsQ0FBQyxDQUFDO1FBQ1IsQ0FBQzs7O09BQUE7SUFFRCxpQkFBaUI7SUFFVCxpREFBb0IsR0FBNUI7UUFDRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVPLDRDQUFlLEdBQXZCO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDbkIsTUFBTSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7WUFDOUIsU0FBUyxFQUFFLElBQUksQ0FBQyxvQkFBb0I7U0FDckMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLDRDQUFlLEdBQXZCO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVPLGlFQUFvQyxHQUE1QztRQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVPLCtEQUFrQyxHQUExQztRQUNFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVPLCtEQUFrQyxHQUExQztRQUNFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVPLDhDQUFpQixHQUF6QjtRQUNFLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDN0M7SUFDSCxDQUFDO0lBR0Qsc0JBQUksNENBQVk7UUFEaEIsb0JBQW9CO2FBQ3BCO1lBQ0UsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzNCLENBQUM7OztPQUFBO0lBRUQsc0JBQUkseURBQXlCO2FBQTdCO1lBQ0UsSUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2IsSUFBSSxJQUFJLENBQUMsOEJBQThCLEVBQUU7Z0JBQ3ZDLENBQUMsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUN4QztZQUNELElBQUksSUFBSSxDQUFDLHNCQUFzQixFQUFFO2dCQUMvQixDQUFDLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7YUFDeEM7WUFDRCxJQUFJLElBQUksQ0FBQyxrQ0FBa0MsRUFBRTtnQkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO2FBQzVDO1lBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFOLENBQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQVQsQ0FBUyxDQUFDLENBQUM7WUFDN0QsT0FBTyxDQUFDLENBQUM7UUFDWCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHNEQUFzQjthQUExQjtZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUVELDhEQUFpQyxHQUFqQztRQUFBLGlCQWVDO1FBZEMsSUFBTSxJQUFJLEdBQVUsRUFBRSxDQUFDO1FBRXZCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUMvQixJQUFNLFNBQVMsY0FBSyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLE9BQU8sSUFBSyxPQUFPLENBQUUsQ0FBQztZQUNuRixPQUFPLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILGdEQUFnRDtRQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXJGLFVBQVUsQ0FBQzs7WUFDVCxNQUFBLEtBQUksQ0FBQyxzQkFBc0IsMENBQUUsSUFBSSxDQUFDLElBQUksRUFBRTtRQUMxQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUEyQkQsc0JBQUksNkRBQTZCO2FBQWpDO1lBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2QsT0FBTyxLQUFLLENBQUM7YUFDZDtpQkFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDakMsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw2REFBNkI7YUFBakM7WUFDRSxPQUFPLElBQUksQ0FBQztZQUVaLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNkLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUM7OztPQUFBO0lBdDBEYywwQkFBTyxHQUFHLENBQUMsQ0FBQzt3RkFGaEIsa0JBQWtCOzJEQUFsQixrQkFBa0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNwRS9CLDhCQUVFO1lBQUEsb0VBR0U7WUE0TkYsdUZBQ0E7WUF1S0EscUZBQ0E7WUEySkEsb0VBR0U7WUFpRkosaUJBQU07O1lBdG5CRixlQUErSTtZQUEvSSw4S0FBK0k7WUE4Tm5JLGVBQXNDO1lBQXRDLHlEQUFzQztZQXdLdEMsZUFBcUM7WUFBckMsd0RBQXFDO1lBNkpqRCxlQUFzRztZQUF0RyxpSUFBc0c7bzZLRDFlNUY7Z0JBQ1YsT0FBTyxDQUFDLGNBQWMsRUFBRTtvQkFDdEIsS0FBSyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7b0JBQ2xGLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztvQkFDaEUsVUFBVSxDQUFDLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2lCQUN0RixDQUFDO2FBQ0g7NkJBbEVIO0NBODREQyxBQXYxREQsSUF1MURDO1NBMTBEWSxrQkFBa0I7a0RBQWxCLGtCQUFrQjtjQWI5QixTQUFTO2VBQUM7Z0JBQ1QsK0NBQStDO2dCQUMvQyxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsV0FBVyxFQUFFLDZCQUE2QjtnQkFDMUMsU0FBUyxFQUFFLENBQUMsNkJBQTZCLEVBQUUsMEJBQTBCLENBQUM7Z0JBQ3RFLFVBQVUsRUFBRTtvQkFDVixPQUFPLENBQUMsY0FBYyxFQUFFO3dCQUN0QixLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQzt3QkFDbEYsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO3dCQUNoRSxVQUFVLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7cUJBQ3RGLENBQUM7aUJBQ0g7YUFDRjs7a0JBTUUsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBRUwsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBR0wsTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBQ04sTUFBTTs7a0JBR04sWUFBWTttQkFBQyxjQUFjOztrQkFDM0IsWUFBWTttQkFBQyx3QkFBd0I7O2tCQUNyQyxZQUFZO21CQUFDLG1CQUFtQjs7a0JBQ2hDLFlBQVk7bUJBQUMsY0FBYzs7a0JBQzNCLFlBQVk7bUJBQUMsY0FBYzs7a0JBRTNCLFNBQVM7bUJBQUMsbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIE9uRGVzdHJveSxcclxuICBJbnB1dCxcclxuICBPdXRwdXQsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIENvbnRlbnRDaGlsZCxcclxuICBUZW1wbGF0ZVJlZixcclxuICBDaGFuZ2VEZXRlY3RvclJlZixcclxuICBOZ1pvbmUsXHJcbiAgT25DaGFuZ2VzLFxyXG4gIFNpbXBsZUNoYW5nZXMsXHJcbiAgQWZ0ZXJDb250ZW50SW5pdCxcclxuICBWaWV3Q2hpbGRcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLFxyXG4gIElNYWlzVGFibGVQYWdlUmVxdWVzdCxcclxuICBJTWFpc1RhYmxlUGFnZVJlc3BvbnNlLFxyXG4gIElNYWlzVGFibGVDb2x1bW4sXHJcbiAgTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZCxcclxuICBJTWFpc1RhYmxlU29ydGluZ1NwZWNpZmljYXRpb24sXHJcbiAgSU1haXNUYWJsZUFjdGlvbixcclxuICBJTWFpc1RhYmxlQWN0aW9uRGlzcGF0Y2hpbmdDb250ZXh0LFxyXG4gIE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24sXHJcbiAgSU1haXNUYWJsZUNvbnRleHRGaWx0ZXIsXHJcbiAgSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90LFxyXG4gIElNYWlzVGFibGVTdG9yZUFkYXB0ZXIsXHJcbiAgSU1haXNUYWJsZUlkZW50aWZpZXJQcm92aWRlcixcclxuICBJTWFpc1RhYmxlSXRlbURyYWdnZWRDb250ZXh0LFxyXG4gIElNYWlzVGFibGVJdGVtRHJvcHBlZENvbnRleHQsXHJcbiAgTWFpc1RhYmxlUmVmcmVzaFN0cmF0ZWd5LFxyXG4gIElNYWlzVGFibGVQdXNoUmVmcmVzaFJlcXVlc3QsXHJcbiAgSU1haXNUYWJsZVJlbG9hZENvbnRleHQsXHJcbiAgTWFpc1RhYmxlUmVsb2FkUmVhc29uLFxyXG4gIElNYWlzVGFibGVQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90XHJcbn0gZnJvbSAnLi9tb2RlbCc7XHJcbmltcG9ydCB7XHJcbiAgTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLFxyXG4gIE1haXNUYWJsZUluTWVtb3J5SGVscGVyLFxyXG4gIE1haXNUYWJsZVZhbGlkYXRvckhlbHBlcixcclxuICBNYWlzVGFibGVCcm93c2VySGVscGVyLFxyXG4gIE1haXNUYWJsZUxvZ2dlclxyXG59IGZyb20gJy4vdXRpbHMnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVSZWdpc3RyeVNlcnZpY2UsIE1haXNUYWJsZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzJztcclxuXHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIFN1YnNjcmliZXIsIHRocm93RXJyb3IsIFN1YnNjcmlwdGlvbiwgdGltZXIgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wLCBDZGtEcmFnIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBNYXRUYWJsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYmxlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogY29tcG9uZW50LXNlbGVjdG9yXHJcbiAgc2VsZWN0b3I6ICdtYWlzLXRhYmxlJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbWFpcy10YWJsZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbWFpcy10YWJsZS5jb21wb25lbnQuc2NzcycsICcuL21haXMtdGFibGUtcG9ydGluZy5jc3MnXSxcclxuICBhbmltYXRpb25zOiBbXHJcbiAgICB0cmlnZ2VyKCdkZXRhaWxFeHBhbmQnLCBbXHJcbiAgICAgIHN0YXRlKCdjb2xsYXBzZWQnLCBzdHlsZSh7IGhlaWdodDogJzBweCcsIG1pbkhlaWdodDogJzAnLCB2aXNpYmlsaXR5OiAnaGlkZGVuJyB9KSksXHJcbiAgICAgIHN0YXRlKCdleHBhbmRlZCcsIHN0eWxlKHsgaGVpZ2h0OiAnKicsIHZpc2liaWxpdHk6ICd2aXNpYmxlJyB9KSksXHJcbiAgICAgIHRyYW5zaXRpb24oJ2V4cGFuZGVkIDw9PiBjb2xsYXBzZWQnLCBhbmltYXRlKCcyMjVtcyBjdWJpYy1iZXppZXIoMC40LCAwLjAsIDAuMiwgMSknKSksXHJcbiAgICBdKSxcclxuICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcywgQWZ0ZXJDb250ZW50SW5pdCB7XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIGNvdW50ZXIgPSAwO1xyXG5cclxuICAvLyBpbnB1dCBwYXJhbWV0ZXJzXHJcbiAgQElucHV0KCkgZGF0YTogYW55W10gfCBPYnNlcnZhYmxlPGFueT4gfCBTdWJqZWN0PGFueT47XHJcbiAgQElucHV0KCkgZGF0YVByb3ZpZGVyOiAoKGlucHV0OiBJTWFpc1RhYmxlUGFnZVJlcXVlc3QsIGNvbnRleHQ/OiBJTWFpc1RhYmxlUmVsb2FkQ29udGV4dClcclxuICAgID0+IE9ic2VydmFibGU8SU1haXNUYWJsZVBhZ2VSZXNwb25zZT4pIHwgbnVsbCA9IG51bGw7XHJcbiAgQElucHV0KCkgY29sdW1uczogSU1haXNUYWJsZUNvbHVtbltdO1xyXG4gIEBJbnB1dCgpIGRlZmF1bHRTb3J0aW5nQ29sdW1uOiBzdHJpbmcgfCBudWxsID0gbnVsbDtcclxuICBASW5wdXQoKSBkZWZhdWx0U29ydGluZ0RpcmVjdGlvbjogc3RyaW5nIHwgbnVsbCA9IG51bGw7XHJcbiAgQElucHV0KCkgZGVmYXVsdFBhZ2VTaXplOiBudW1iZXIgfCBudWxsID0gbnVsbDtcclxuICBASW5wdXQoKSBwb3NzaWJsZVBhZ2VTaXplOiBudW1iZXJbXSB8IG51bGwgPSBudWxsO1xyXG4gIEBJbnB1dCgpIHRhYmxlSWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBkcm9wQ29ubmVjdGVkVG86IHN0cmluZztcclxuICBASW5wdXQoKSBwYWdpbmF0aW9uTW9kZTogTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZDtcclxuICBASW5wdXQoKSBlbmFibGVQYWdpbmF0aW9uOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVNlbGVjdGlvbjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVDb250ZXh0RmlsdGVyaW5nOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVNlbGVjdEFsbDogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVNdWx0aVNlbGVjdDogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVGaWx0ZXJpbmc6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlQ29sdW1uc1NlbGVjdGlvbjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVDb250ZXh0QWN0aW9uczogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVIZWFkZXJBY3Rpb25zOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVJvd0V4cGFuc2lvbjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVQYWdlU2l6ZVNlbGVjdDogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVNdWx0aXBsZVJvd0V4cGFuc2lvbjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVJdGVtVHJhY2tpbmc6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZW5hYmxlU3RvcmVQZXJzaXN0ZW5jZTogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVEcmFnOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGVuYWJsZURyb3A6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgY29udGV4dEFjdGlvbnM6IElNYWlzVGFibGVBY3Rpb25bXTtcclxuICBASW5wdXQoKSBoZWFkZXJBY3Rpb25zOiBJTWFpc1RhYmxlQWN0aW9uW107XHJcbiAgQElucHV0KCkgY29udGV4dEZpbHRlcnM6IElNYWlzVGFibGVDb250ZXh0RmlsdGVyW107XHJcbiAgQElucHV0KCkgY29udGV4dEZpbHRlcmluZ1Byb21wdDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGFjdGlvblN0YXR1c1Byb3ZpZGVyOiAoYWN0aW9uOiBJTWFpc1RhYmxlQWN0aW9uLCBzdGF0dXM6IElNYWlzVGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGwpID0+IGJvb2xlYW47XHJcbiAgQElucHV0KCkgZXhwYW5kYWJsZVN0YXR1c1Byb3ZpZGVyOiAocm93OiBhbnksIHN0YXR1czogSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbCkgPT4gYm9vbGVhbjtcclxuICBASW5wdXQoKSBzdG9yZUFkYXB0ZXI6IElNYWlzVGFibGVTdG9yZUFkYXB0ZXI7XHJcbiAgQElucHV0KCkgaXRlbUlkZW50aWZpZXI6IHN0cmluZyB8IElNYWlzVGFibGVJZGVudGlmaWVyUHJvdmlkZXI7XHJcbiAgQElucHV0KCkgcmVmcmVzaFN0cmF0ZWd5OiBNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3kgfCBNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3lbXTtcclxuICBASW5wdXQoKSByZWZyZXNoSW50ZXJ2YWw6IG51bWJlcjtcclxuICBASW5wdXQoKSByZWZyZXNoRW1pdHRlcjogT2JzZXJ2YWJsZTxJTWFpc1RhYmxlUHVzaFJlZnJlc2hSZXF1ZXN0PjtcclxuICBASW5wdXQoKSByZWZyZXNoSW50ZXJ2YWxJbkJhY2tncm91bmQ6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgcmVmcmVzaE9uUHVzaEluQmFja2dyb3VuZDogYm9vbGVhbjtcclxuXHJcbiAgLy8gb3V0cHV0IGV2ZW50c1xyXG4gIEBPdXRwdXQoKSBwYWdlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxudW1iZXI+KCk7XHJcbiAgQE91dHB1dCgpIHNvcnRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPElNYWlzVGFibGVTb3J0aW5nU3BlY2lmaWNhdGlvbj4oKTtcclxuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICBAT3V0cHV0KCkgZmlsdGVyaW5nQ29sdW1uc0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8SU1haXNUYWJsZUNvbHVtbltdPigpO1xyXG4gIEBPdXRwdXQoKSB2aXNpYmxlQ29sdW1uc0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8SU1haXNUYWJsZUNvbHVtbltdPigpO1xyXG4gIEBPdXRwdXQoKSBjb250ZXh0RmlsdGVyc0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8SU1haXNUYWJsZUNvbnRleHRGaWx0ZXJbXT4oKTtcclxuICBAT3V0cHV0KCkgc3RhdHVzQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlU3RhdHVzU25hcHNob3Q+KCk7XHJcbiAgQE91dHB1dCgpIGl0ZW1EcmFnZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlSXRlbURyYWdnZWRDb250ZXh0PigpO1xyXG4gIEBPdXRwdXQoKSBpdGVtRHJvcHBlZCA9IG5ldyBFdmVudEVtaXR0ZXI8SU1haXNUYWJsZUl0ZW1Ecm9wcGVkQ29udGV4dD4oKTtcclxuICBAT3V0cHV0KCkgYWN0aW9uID0gbmV3IEV2ZW50RW1pdHRlcjxJTWFpc1RhYmxlQWN0aW9uRGlzcGF0Y2hpbmdDb250ZXh0PigpO1xyXG5cclxuICAvLyBpbnB1dCB0ZW1wbGF0ZVxyXG4gIEBDb250ZW50Q2hpbGQoJ2NlbGxUZW1wbGF0ZScpIGNlbGxUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuICBAQ29udGVudENoaWxkKCdhY3Rpb25zQ2FwdGlvblRlbXBsYXRlJykgYWN0aW9uc0NhcHRpb25UZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuICBAQ29udGVudENoaWxkKCdyb3dEZXRhaWxUZW1wbGF0ZScpIHJvd0RldGFpbFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gIEBDb250ZW50Q2hpbGQoJ2RyYWdUZW1wbGF0ZScpIGRyYWdUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuICBAQ29udGVudENoaWxkKCdkcm9wVGVtcGxhdGUnKSBkcm9wVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XHJcblxyXG4gIEBWaWV3Q2hpbGQoJyNyZW5kZXJlZE1hdFRhYmxlJykgdGFibGU6IE1hdFRhYmxlPGFueT47XHJcblxyXG4gIHByaXZhdGUgaXNJRSA9IE1haXNUYWJsZUJyb3dzZXJIZWxwZXIuaXNJRSgpO1xyXG5cclxuICBwcml2YXRlIGxvZ2dlcjogTWFpc1RhYmxlTG9nZ2VyO1xyXG4gIHByaXZhdGUgcmVnaXN0cmF0aW9uSWQ6IHN0cmluZztcclxuICBwcml2YXRlIHN0YXR1c1NuYXBzaG90OiBJTWFpc1RhYmxlU3RhdHVzU25hcHNob3QgfCBudWxsID0gbnVsbDtcclxuICBwcml2YXRlIHBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3Q6IElNYWlzVGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGwgPSBudWxsO1xyXG4gIHByaXZhdGUgdXVpZDogc3RyaW5nO1xyXG4gIGZvcmNlUmVSZW5kZXIgPSBmYWxzZTtcclxuICBwcml2YXRlIGluaXRpYWxpemVkID0gZmFsc2U7XHJcblxyXG4gIC8vIGxvY2FsIGRhdGFcclxuICBwcml2YXRlIGNsaWVudERhdGFTbmFwc2hvdDogYW55W107XHJcbiAgZGF0YVNuYXBzaG90OiBhbnlbXTtcclxuXHJcbiAgcHJpdmF0ZSBzZWxlY3RlZFBhZ2VJbmRleDogbnVtYmVyO1xyXG4gIHByaXZhdGUgc2VsZWN0ZWRQYWdlU2l6ZTogbnVtYmVyIHwgbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBzZWxlY3RlZFNvcnRDb2x1bW46IElNYWlzVGFibGVDb2x1bW4gfCBudWxsO1xyXG4gIHByaXZhdGUgc2VsZWN0ZWRTb3J0RGlyZWN0aW9uOiBNYWlzVGFibGVTb3J0RGlyZWN0aW9uO1xyXG5cclxuICBzZWxlY3RlZFNlYXJjaFF1ZXJ5OiBzdHJpbmcgfCBudWxsO1xyXG4gIHByaXZhdGUgbGFzdEZldGNoZWRTZWFyY2hRdWVyeTogc3RyaW5nIHwgbnVsbDtcclxuXHJcbiAgLy8gc2VsZWN0ZWQgaXRlbXMgd2l0aCBjaGVja2JveFxyXG4gIHByaXZhdGUgY2hlY2tlZEl0ZW1zOiBhbnlbXTtcclxuXHJcbiAgLy8gc2VsZWN0ZWQgY29sdW1ucyBmb3IgZmlsdGVyaW5nXHJcbiAgcHJpdmF0ZSBjaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZzogSU1haXNUYWJsZUNvbHVtbltdO1xyXG5cclxuICAvLyBzZWxlY3RlZCBjb2x1bW5zIGZvciB2aXN1YWxpemF0aW9uXHJcbiAgcHJpdmF0ZSBjaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb246IElNYWlzVGFibGVDb2x1bW5bXTtcclxuXHJcbiAgLy8gc2VsZWN0ZWQgY3VzdG9tIGZpbHRlcnNcclxuICBwcml2YXRlIGNoZWNrZWRDb250ZXh0RmlsdGVyczogSU1haXNUYWJsZUNvbnRleHRGaWx0ZXJbXTtcclxuXHJcbiAgLy8gZXhwYW5kZWQgcm93c1xyXG4gIHByaXZhdGUgZXhwYW5kZWRJdGVtczogYW55W107XHJcblxyXG4gIC8vIGZvciBzZXJ2ZXIgcGFnaW5hdGlvblxyXG4gIHByaXZhdGUgZmV0Y2hlZFBhZ2VDb3VudDogbnVtYmVyIHwgbnVsbDtcclxuICBwcml2YXRlIGZldGNoZWRSZXN1bHROdW1iZXI6IG51bWJlciB8IG51bGw7XHJcbiAgZmV0Y2hpbmcgPSBmYWxzZTtcclxuICBzaG93RmV0Y2hpbmcgPSBmYWxzZTtcclxuXHJcbiAgcHJpdmF0ZSByZWZyZXNoRW1pdHRlclN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uIHwgbnVsbCA9IG51bGw7XHJcbiAgcHJpdmF0ZSByZWZyZXNoSW50ZXJ2YWxUaW1lcjogT2JzZXJ2YWJsZTxudW1iZXI+IHwgbnVsbDtcclxuICBwcml2YXRlIHJlZnJlc2hJbnRlcnZhbFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uIHwgbnVsbCA9IG51bGw7XHJcblxyXG4gIC8vIE1hdFRhYmxlIGFkYXB0ZXJcclxuICBtYXRUYWJsZURhdGFPYnNlcnZhYmxlOiBTdWJqZWN0PGFueVtdPiB8IG51bGwgPSBudWxsO1xyXG4gIGV4cGFuZGVkRWxlbWVudDogYW55ID0gbnVsbDtcclxuXHJcbiAgLy8gbGlmZWN5Y2xlIGhvb2tzXHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGVTZXJ2aWNlOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjb25maWd1cmF0aW9uU2VydmljZTogTWFpc1RhYmxlU2VydmljZSxcclxuICAgIHByaXZhdGUgcmVnaXN0cnk6IE1haXNUYWJsZVJlZ2lzdHJ5U2VydmljZSxcclxuICAgIHByaXZhdGUgY2RyOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIHByaXZhdGUgbmdab25lOiBOZ1pvbmUgKSB7XHJcblxyXG4gICAgdGhpcy51dWlkID0gJ01UQ01QLScgKyAoKytNYWlzVGFibGVDb21wb25lbnQuY291bnRlcikgKyAnLScgKyBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAxMDAwMDApO1xyXG4gICAgdGhpcy5sb2dnZXIgPSBuZXcgTWFpc1RhYmxlTG9nZ2VyKCdNYWlzVGFibGVDb21wb25lbnRfJyArIHRoaXMuY3VycmVudFRhYmxlSWQpO1xyXG4gICAgdGhpcy5sb2dnZXIudHJhY2UoJ2J1aWxkaW5nIGNvbXBvbmVudCcpO1xyXG5cclxuICAgIHRoaXMubWF0VGFibGVEYXRhT2JzZXJ2YWJsZSA9IG5ldyBTdWJqZWN0PGFueVtdPigpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmxvZ2dlciA9IG5ldyBNYWlzVGFibGVMb2dnZXIoJ01haXNUYWJsZUNvbXBvbmVudF8nICsgdGhpcy5jdXJyZW50VGFibGVJZCk7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnaW5pdGlhbGl6aW5nIGNvbXBvbmVudCcpO1xyXG5cclxuICAgIHRoaXMucmVnaXN0cmF0aW9uSWQgPSB0aGlzLnJlZ2lzdHJ5LnJlZ2lzdGVyKHRoaXMuY3VycmVudFRhYmxlSWQsIHRoaXMpO1xyXG5cclxuICAgIC8vIGlucHV0IHZhbGlkYXRpb25cclxuICAgIE1haXNUYWJsZVZhbGlkYXRvckhlbHBlci52YWxpZGF0ZUNvbHVtbnNTcGVjaWZpY2F0aW9uKHRoaXMuY29sdW1ucywgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG5cclxuICAgIHRoaXMuY2xpZW50RGF0YVNuYXBzaG90ID0gW107XHJcbiAgICB0aGlzLmRhdGFTbmFwc2hvdCA9IFtdO1xyXG4gICAgdGhpcy5mZXRjaGVkUGFnZUNvdW50ID0gMDtcclxuICAgIHRoaXMuZmV0Y2hlZFJlc3VsdE51bWJlciA9IDA7XHJcbiAgICB0aGlzLnNlbGVjdGVkUGFnZUluZGV4ID0gMDtcclxuICAgIHRoaXMuc2VsZWN0ZWRTZWFyY2hRdWVyeSA9IG51bGw7XHJcbiAgICB0aGlzLnNlbGVjdGVkUGFnZVNpemUgPSBudWxsO1xyXG4gICAgdGhpcy5leHBhbmRlZEl0ZW1zID0gW107XHJcbiAgICB0aGlzLmNoZWNrZWRJdGVtcyA9IFtdO1xyXG4gICAgdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMgPSBbXTtcclxuICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcgPSB0aGlzLmZpbHRlcmFibGVDb2x1bW5zLmZpbHRlcihcclxuICAgICAgYyA9PiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNEZWZhdWx0RmlsdGVyQ29sdW1uKGMsIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKSk7XHJcbiAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbiA9IHRoaXMuY29sdW1ucy5maWx0ZXIoXHJcbiAgICAgIGMgPT4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzRGVmYXVsdFZpc2libGVDb2x1bW4oYywgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKTtcclxuICAgIHRoaXMuc3RhdHVzU25hcHNob3QgPSB0aGlzLmJ1aWxkU3RhdHVzU25hcHNob3QoKTtcclxuICAgIHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KCk7XHJcblxyXG4gICAgaWYgKHRoaXMuZGF0YVByb3ZpZGVyKSB7XHJcbiAgICAgIC8vIGlucHV0IGlzIGEgcHJvdmlkZXIgZnVuY3Rpb25cclxuICAgICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignTWFpc1RhYmxlIGNhblxcJ3QgYmUgcHJvdmlkZWQgYm90aCBkYXRhIGFuZCBkYXRhUHJvdmlkZXInKTtcclxuICAgICAgfVxyXG5cclxuICAgIH0gZWxzZSBpZiAodGhpcy5kYXRhIGluc3RhbmNlb2YgT2JzZXJ2YWJsZSB8fCB0aGlzLmRhdGEgaW5zdGFuY2VvZiBTdWJqZWN0KSB7XHJcbiAgICAgIC8vIGlucHV0IGlzIG9ic2VydmFibGVcclxuICAgICAgdGhpcy5kYXRhLnN1YnNjcmliZShkYXRhU25hcHNob3QgPT4ge1xyXG4gICAgICAgIHRoaXMuaGFuZGxlSW5wdXREYXRhT2JzZXJ2YWJsZUVtaXNzaW9uKGRhdGFTbmFwc2hvdCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gaW5wdXQgaXMgZGF0YSBhcnJheVxyXG4gICAgICB0aGlzLmNsaWVudERhdGFTbmFwc2hvdCA9IHRoaXMuZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBMT0FEIFNUQVRVUyBmcm9tIHN0b3JlIGlmIHBvc3NpYmxlXHJcbiAgICBsZXQgYWN0aXZhdGlvbk9ic2VydmFibGU6IE9ic2VydmFibGU8SU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QgfCBudWxsPjtcclxuICAgIGlmICh0aGlzLnN0b3JlUGVyc2lzdGVuY2VFbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgYWN0aXZhdGlvbk9ic2VydmFibGUgPSB0aGlzLmxvYWRTdGF0dXNGcm9tU3RvcmUoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGFjdGl2YXRpb25PYnNlcnZhYmxlID0gbmV3IE9ic2VydmFibGUoc3Vic2NyaWJlciA9PiB7XHJcbiAgICAgICAgc3Vic2NyaWJlci5uZXh0KG51bGwpO1xyXG4gICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZhdGlvbk9ic2VydmFibGUuc3Vic2NyaWJlKChzdGF0dXM6IElNYWlzVGFibGVQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KSA9PiB7XHJcbiAgICAgIGlmIChzdGF0dXMpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgncmVzdG9yZWQgc3RhdHVzIHNuYXBzaG90IGZyb20gc3RvcmFnZScsIHN0YXR1cyk7XHJcbiAgICAgICAgdGhpcy5hcHBseVN0YXR1c1NuYXBzaG90KHN0YXR1cyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5JTlRFUk5BTCwgd2l0aFN0YXR1c1NuYXBzaG90OiBzdGF0dXN9KS5zdWJzY3JpYmUoeWVzID0+IHtcclxuICAgICAgICB0aGlzLmNvbXBsZXRlSW5pdGlhbGl6YXRpb24oKTtcclxuICAgICAgfSwgbm9wZSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb21wbGV0ZUluaXRpYWxpemF0aW9uKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIH0sIGZhaWx1cmUgPT4ge1xyXG4gICAgICB0aGlzLmxvZ2dlci5lcnJvcigncmVzdG9yaW5nIHN0YXR1cyBmcm9tIHN0b3JlIGZhaWxlZCcsIGZhaWx1cmUpO1xyXG4gICAgICB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uSU5URVJOQUx9KS5zdWJzY3JpYmUoeWVzID0+IHtcclxuICAgICAgICB0aGlzLmNvbXBsZXRlSW5pdGlhbGl6YXRpb24oKTtcclxuICAgICAgfSwgbm9wZSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb21wbGV0ZUluaXRpYWxpemF0aW9uKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnYWZ0ZXIgY29udGVudCBpbml0Jyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZUlucHV0RGF0YU9ic2VydmFibGVFbWlzc2lvbihkYXRhU25hcHNob3Q6IGFueVtdKSB7XHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnZGF0YSBzbmFwc2hvdCBlbWl0dGVkIGZyb20gaW5wdXQgZGF0YScpO1xyXG4gICAgdGhpcy5jbGllbnREYXRhU25hcHNob3QgPSBkYXRhU25hcHNob3Q7XHJcbiAgICB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uRVhURVJOQUx9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY29tcGxldGVJbml0aWFsaXphdGlvbigpIHtcclxuICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlLnBpcGUoZGVib3VuY2VUaW1lKDIwMCkpLnN1YnNjcmliZShzdGF0dXNTbmFwc2hvdCA9PiB7XHJcbiAgICAgIHRoaXMucGVyc2lzdFN0YXR1c1RvU3RvcmUoKS5zdWJzY3JpYmUoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIGlmICh0aGlzLnJlZnJlc2hFbWl0dGVyKSB7XHJcbiAgICAgIHRoaXMuaGFuZGxlUmVmcmVzaEVtaXR0ZXJDaGFuZ2UodGhpcy5yZWZyZXNoRW1pdHRlcik7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5yZWZyZXNoSW50ZXJ2YWwpIHtcclxuICAgICAgdGhpcy5oYW5kbGVSZWZyZXNoSW50ZXJ2YWxDaGFuZ2UodGhpcy5yZWZyZXNoSW50ZXJ2YWwpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnZGVzdHJveWluZyBjb21wb25lbnQnKTtcclxuICAgIGlmICh0aGlzLnJlZ2lzdHJhdGlvbklkKSB7XHJcbiAgICAgIHRoaXMucmVnaXN0cnkudW5yZWdpc3Rlcih0aGlzLmN1cnJlbnRUYWJsZUlkLCB0aGlzLnJlZ2lzdHJhdGlvbklkKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmIChjaGFuZ2VzLnJlZnJlc2hFbWl0dGVyKSB7XHJcbiAgICAgIHRoaXMuaGFuZGxlUmVmcmVzaEVtaXR0ZXJDaGFuZ2UoY2hhbmdlcy5yZWZyZXNoRW1pdHRlci5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG4gICAgaWYgKGNoYW5nZXMucmVmcmVzaEludGVydmFsKSB7XHJcbiAgICAgIHRoaXMuaGFuZGxlUmVmcmVzaEludGVydmFsQ2hhbmdlKGNoYW5nZXMucmVmcmVzaEludGVydmFsLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhbmdlcy5yZWZyZXNoU3RyYXRlZ3kgJiYgIWNoYW5nZXMucmVmcmVzaFN0cmF0ZWd5LmZpcnN0Q2hhbmdlKSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ1JFRlJFU0ggU1RSQVRFR1kgQ0hBTkdFRCBXSElMRSBSVU5OSU5HLiBZT1UgU1VSRSBBQk9VVCBUSElTPycsIGNoYW5nZXMpO1xyXG4gICAgICB0aGlzLmhhbmRsZVJlZnJlc2hFbWl0dGVyQ2hhbmdlKHRoaXMucmVmcmVzaEVtaXR0ZXIpO1xyXG4gICAgICB0aGlzLmhhbmRsZVJlZnJlc2hJbnRlcnZhbENoYW5nZSh0aGlzLnJlZnJlc2hJbnRlcnZhbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZVJlZnJlc2hFbWl0dGVyQ2hhbmdlKG5ld1ZhbHVlOiBPYnNlcnZhYmxlPElNYWlzVGFibGVQdXNoUmVmcmVzaFJlcXVlc3Q+KSB7XHJcbiAgICBpZiAodGhpcy5yZWZyZXNoRW1pdHRlclN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnJlZnJlc2hFbWl0dGVyU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgICBpZiAobmV3VmFsdWUpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoRW1pdHRlclN1YnNjcmlwdGlvbiA9IG5ld1ZhbHVlLnN1YnNjcmliZShldmVudCA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3JlY2VpdmVkIHJlZnJlc2ggcHVzaCByZXF1ZXN0JywgZXZlbnQpO1xyXG4gICAgICAgIGlmICh0aGlzLmRyYWdJblByb2dyZXNzKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gZW1pdHRlciBpZ25vcmVkIGJlY2F1c2UgdXNlciBpcyBkcmFnZ2luZyBlbGVtZW50cycpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50UmVmcmVzaFN0cmF0ZWdpZXMuaW5kZXhPZihNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3kuT05fUFVTSCkgPT09IC0xKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gZW1pdHRlciBpZ25vcmVkIGJlY2F1c2UgcmVmcmVzaCBzdHJhdGVnaWVzIGFyZSAnICsgdGhpcy5jdXJyZW50UmVmcmVzaFN0cmF0ZWdpZXMgK1xyXG4gICAgICAgICAgICAnLiBXaHkgYXJlIHlvdSBwdXNoaW5nIHRvIHRoaXMgY29tcG9uZW50PycpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ3JlZnJlc2ggZnJvbSBlbWl0dGVyIGlnbm9yZWQgYmVjYXVzZSB0aGUgY29tcG9uZW50IGlzIG5vdCBmdWxseSBpbml0aWFsaXplZCcpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5mZXRjaGluZykge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybigncmVmcmVzaCBmcm9tIGVtaXR0ZXIgaWdub3JlZCBiZWNhdXNlIHRoZSBjb21wb25lbnQgaXMgZmV0Y2hpbmcgYWxyZWFkeScpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnbGF1bmNoaW5nIHJlbG9hZCBmb2xsb3dpbmcgcHVzaCByZXF1ZXN0Jyk7XHJcbiAgICAgICAgICB0aGlzLnJlbG9hZCh7XHJcbiAgICAgICAgICAgIHJlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLlBVU0gsXHJcbiAgICAgICAgICAgIHB1c2hSZXF1ZXN0OiBldmVudCxcclxuICAgICAgICAgICAgaW5CYWNrZ3JvdW5kOiBldmVudC5pbkJhY2tncm91bmQgPT09IHRydWUgfHwgZXZlbnQuaW5CYWNrZ3JvdW5kID09PSBmYWxzZSA/XHJcbiAgICAgICAgICAgICAgZXZlbnQuaW5CYWNrZ3JvdW5kIDogdGhpcy5jdXJyZW50UmVmcmVzaE9uUHVzaEluQmFja2dyb3VuZFxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlUmVmcmVzaEludGVydmFsQ2hhbmdlKG5ld1ZhbHVlOiBudW1iZXIpIHtcclxuICAgIGlmICh0aGlzLnJlZnJlc2hJbnRlcnZhbFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnJlZnJlc2hJbnRlcnZhbFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucmVmcmVzaEludGVydmFsVGltZXIpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoSW50ZXJ2YWxUaW1lciA9IG51bGw7XHJcbiAgICB9XHJcbiAgICBpZiAobmV3VmFsdWUpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoSW50ZXJ2YWxUaW1lciA9IHRpbWVyKG5ld1ZhbHVlLCBuZXdWYWx1ZSk7XHJcbiAgICAgIHRoaXMucmVmcmVzaEludGVydmFsU3Vic2NyaXB0aW9uID0gdGhpcy5yZWZyZXNoSW50ZXJ2YWxUaW1lci5zdWJzY3JpYmUodGljayA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ2VtaXR0ZWQgcmVmcmVzaCB0aWNrIHJlcXVlc3QnKTtcclxuICAgICAgICBpZiAodGhpcy5kcmFnSW5Qcm9ncmVzcykge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybigncmVmcmVzaCBmcm9tIGVtaXR0ZXIgaWdub3JlZCBiZWNhdXNlIHVzZXIgaXMgZHJhZ2dpbmcgZWxlbWVudHMnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFJlZnJlc2hTdHJhdGVnaWVzLmluZGV4T2YoTWFpc1RhYmxlUmVmcmVzaFN0cmF0ZWd5LlRJTUVEKSA9PT0gLTEpIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ3JlZnJlc2ggZnJvbSB0aWNrIGlnbm9yZWQgYmVjYXVzZSByZWZyZXNoIHN0cmF0ZWdpZXMgYXJlICcgKyB0aGlzLmN1cnJlbnRSZWZyZXNoU3RyYXRlZ2llcyArXHJcbiAgICAgICAgICAgICcuIFdoeSBpcyB0aGlzIGNvbXBvbmVudCBlbWl0dGluZyB0aWNrcyA/Jyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5pbml0aWFsaXplZCkge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybigncmVmcmVzaCBmcm9tIHRpY2sgaWdub3JlZCBiZWNhdXNlIHRoZSBjb21wb25lbnQgaXMgbm90IGZ1bGx5IGluaXRpYWxpemVkJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZldGNoaW5nKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdyZWZyZXNoIGZyb20gdGljayBpZ25vcmVkIGJlY2F1c2UgdGhlIGNvbXBvbmVudCBpcyBmZXRjaGluZyBhbHJlYWR5Jyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdsYXVuY2hpbmcgcmVsb2FkIGZvbGxvd2luZyB0aWNrJyk7XHJcbiAgICAgICAgICB0aGlzLnJlbG9hZCh7XHJcbiAgICAgICAgICAgIHJlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLklOVEVSVkFMLFxyXG4gICAgICAgICAgICBpbkJhY2tncm91bmQ6IHRoaXMuY3VycmVudFJlZnJlc2hJbnRlcnZhbEluQmFja2dyb3VuZFxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIHB1YmxpYyBtZXRob2RzXHJcblxyXG4gIHB1YmxpYyByZWZyZXNoKGJhY2tncm91bmQ/OiBib29sZWFuKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICBpZiAoIXRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgcmV0dXJuIHRocm93RXJyb3IoJ3RhYmxlIGNvbXBvbmVudCBpcyBzdGlsbCBpbml0aWFsaXppbmcnKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uRVhURVJOQUwsIGluQmFja2dyb3VuZDogYmFja2dyb3VuZH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldERhdGFTbmFwc2hvdCgpOiBhbnlbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5kYXRhU25hcHNob3Q7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0U3RhdHVzU25hcHNob3QoKTogSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90IHwgbnVsbCB7XHJcbiAgICByZXR1cm4gdGhpcy5zdGF0dXNTbmFwc2hvdDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBsb2FkU3RhdHVzKHN0YXR1czogSU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QpIHtcclxuICAgIGlmICghdGhpcy5pbml0aWFsaXplZCkge1xyXG4gICAgICByZXR1cm4gdGhyb3dFcnJvcigndGFibGUgY29tcG9uZW50IGlzIHN0aWxsIGluaXRpYWxpemluZycpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5sb2dnZXIudHJhY2UoJ2xvYWRpbmcgc3RhdHVzIHNuYXBzaG90IGZyb20gZXh0ZXJuYWwgY2FsbGVyJywgc3RhdHVzKTtcclxuICAgIHRoaXMuYXBwbHlTdGF0dXNTbmFwc2hvdChzdGF0dXMpO1xyXG4gICAgdGhpcy5yZWxvYWQoe3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLkVYVEVSTkFMLCB3aXRoU3RhdHVzU25hcHNob3Q6IHN0YXR1c30pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhcHBseVN0YXR1c1NuYXBzaG90KHN0YXR1czogSU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QpIHtcclxuICAgaWYgKCFzdGF0dXMgfHwgIXN0YXR1cy5zY2hlbWFWZXJzaW9uKSB7XHJcbiAgICAgdGhpcy5sb2dnZXIud2Fybignbm90IHJlc3RvcmluZyBzdGF0dXMgYmVjYXVzZSBpdCBpcyBtYWxmb3JtZWQnKTtcclxuICAgICByZXR1cm47XHJcbiAgIH1cclxuXHJcbiAgIGlmIChzdGF0dXMuc2NoZW1hVmVyc2lvbiAhPT0gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY3VycmVudFNjaGVtYVZlcnNpb24pIHtcclxuICAgIHRoaXMubG9nZ2VyLndhcm4oJ25vdCByZXN0b3Jpbmcgc3RhdHVzIGJlY2F1c2UgaXQgaXMgb2Jzb2xldGUgKHNuYXBzaG90IGlzIHZlcnNpb24gJyArXHJcbiAgICAgIHN0YXR1cy5zY2hlbWFWZXJzaW9uICsgJyB3aGlsZSBjdXJyZW50IHZlcnNpb24gaXMgJyArIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmN1cnJlbnRTY2hlbWFWZXJzaW9uICsgJyknKTtcclxuICAgIHJldHVybjtcclxuICAgfVxyXG5cclxuICAgaWYgKHN0YXR1cy5vcmRlckNvbHVtbikge1xyXG4gICAgdGhpcy5zZWxlY3RlZFNvcnRDb2x1bW4gPSB0aGlzLmNvbHVtbnMuZmluZChjID0+IHRoaXMuaXNTb3J0YWJsZShjKSAmJiBjLm5hbWUgPT09IHN0YXR1cy5vcmRlckNvbHVtbikgfHwgbnVsbDtcclxuICAgfVxyXG5cclxuICAgaWYgKHN0YXR1cy5vcmRlckNvbHVtbkRpcmVjdGlvbikge1xyXG4gICAgIHRoaXMuc2VsZWN0ZWRTb3J0RGlyZWN0aW9uID0gKHN0YXR1cy5vcmRlckNvbHVtbkRpcmVjdGlvbiA9PT0gTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HKSA/XHJcbiAgICAgIE1haXNUYWJsZVNvcnREaXJlY3Rpb24uREVTQ0VORElORyA6IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlRmlsdGVyaW5nICYmIHN0YXR1cy5xdWVyeSkge1xyXG4gICAgIHRoaXMuc2VsZWN0ZWRTZWFyY2hRdWVyeSA9IHN0YXR1cy5xdWVyeS50cmltKCk7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVGaWx0ZXJpbmcgJiYgc3RhdHVzLnF1ZXJ5Q29sdW1ucyAmJiBzdGF0dXMucXVlcnlDb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZyA9IHRoaXMuZmlsdGVyYWJsZUNvbHVtbnMuZmlsdGVyKGMgPT4gc3RhdHVzPy5xdWVyeUNvbHVtbnM/LmluZGV4T2YoYy5uYW1lKSAhPT0gLTEpO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlQ29sdW1uc1NlbGVjdGlvbiAmJiBzdGF0dXMudmlzaWJsZUNvbHVtbnMgJiYgc3RhdHVzLnZpc2libGVDb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24gPSB0aGlzLmNvbHVtbnMuZmlsdGVyKGMgPT4gIXRoaXMuaXNIaWRlYWJsZShjKSB8fCBzdGF0dXM/LnZpc2libGVDb2x1bW5zPy5pbmRleE9mKGMubmFtZSkgIT09IC0xKTtcclxuICAgfVxyXG5cclxuICAgaWYgKHRoaXMuY3VycmVudEVuYWJsZUNvbnRleHRGaWx0ZXJpbmcgJiYgc3RhdHVzLmNvbnRleHRGaWx0ZXJzICYmIHN0YXR1cy5jb250ZXh0RmlsdGVycy5sZW5ndGgpIHtcclxuICAgICB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycyA9IHRoaXMuY29udGV4dEZpbHRlcnMuZmlsdGVyKGYgPT4gc3RhdHVzPy5jb250ZXh0RmlsdGVycz8uaW5kZXhPZihmLm5hbWUpICE9PSAtMSk7XHJcbiAgIH1cclxuXHJcbiAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVQYWdpbmF0aW9uICYmIHN0YXR1cy5jdXJyZW50UGFnZSB8fCBzdGF0dXMuY3VycmVudFBhZ2UgPT09IDApIHtcclxuICAgICB0aGlzLnNlbGVjdGVkUGFnZUluZGV4ID0gc3RhdHVzLmN1cnJlbnRQYWdlO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlUGFnaW5hdGlvbiAmJiBzdGF0dXMucGFnZVNpemUpIHtcclxuICAgICB0aGlzLnNlbGVjdGVkUGFnZVNpemUgPSB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplcy5maW5kKHMgPT4gc3RhdHVzLnBhZ2VTaXplID09PSBzKSB8fCBudWxsO1xyXG4gICB9XHJcblxyXG4gICB0aGlzLnN0YXR1c1NuYXBzaG90ID0gdGhpcy5idWlsZFN0YXR1c1NuYXBzaG90KCk7XHJcbiAgIHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFwcGx5U3RhdHVzU25hcHNob3RQb3N0RmV0Y2goc3RhdHVzOiBJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCkge1xyXG4gICBpZiAoIXN0YXR1cyB8fCAhc3RhdHVzLnNjaGVtYVZlcnNpb24pIHtcclxuICAgICB0aGlzLmxvZ2dlci53YXJuKCdub3QgcmVzdG9yaW5nIHN0YXR1cyBiZWNhdXNlIGl0IGlzIG1hbGZvcm1lZCcpO1xyXG4gICAgIHJldHVybjtcclxuICAgfVxyXG5cclxuICAgaWYgKHN0YXR1cy5zY2hlbWFWZXJzaW9uICE9PSB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jdXJyZW50U2NoZW1hVmVyc2lvbikge1xyXG4gICAgdGhpcy5sb2dnZXIud2Fybignbm90IHJlc3RvcmluZyBzdGF0dXMgYmVjYXVzZSBpdCBpcyBvYnNvbGV0ZSAoc25hcHNob3QgaXMgdmVyc2lvbiAnICtcclxuICAgICAgc3RhdHVzLnNjaGVtYVZlcnNpb24gKyAnIHdoaWxlIGN1cnJlbnQgdmVyc2lvbiBpcyAnICsgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY3VycmVudFNjaGVtYVZlcnNpb24gKyAnKScpO1xyXG4gICAgcmV0dXJuO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlU2VsZWN0aW9uICYmIHRoaXMuaXRlbVRyYWNraW5nRW5hYmxlZEFuZFBvc3NpYmxlXHJcbiAgICAgICYmIHN0YXR1cy5jaGVja2VkSXRlbUlkZW50aWZpZXJzICYmIHN0YXR1cy5jaGVja2VkSXRlbUlkZW50aWZpZXJzLmxlbmd0aCkge1xyXG5cclxuICAgIHRoaXMuY2hlY2tlZEl0ZW1zID0gdGhpcy5kYXRhU25hcHNob3QuZmlsdGVyKGRhdGEgPT4ge1xyXG4gICAgICBjb25zdCBpZCA9IHRoaXMuZ2V0SXRlbUlkZW50aWZpZXIoZGF0YSk7XHJcbiAgICAgIHJldHVybiBpZCAmJiBzdGF0dXM/LmNoZWNrZWRJdGVtSWRlbnRpZmllcnM/LmluZGV4T2YoaWQpICE9PSAtMTtcclxuICAgIH0pO1xyXG4gICB9XHJcblxyXG4gICBpZiAodGhpcy5jdXJyZW50RW5hYmxlUm93RXhwYW5zaW9uICYmIHRoaXMuaXRlbVRyYWNraW5nRW5hYmxlZEFuZFBvc3NpYmxlXHJcbiAgICAgICYmIHN0YXR1cy5leHBhbmRlZEl0ZW1JZGVudGlmaWVycyAmJiBzdGF0dXMuZXhwYW5kZWRJdGVtSWRlbnRpZmllcnMubGVuZ3RoKSB7XHJcblxyXG4gICAgdGhpcy5leHBhbmRlZEl0ZW1zID0gdGhpcy5kYXRhU25hcHNob3QuZmlsdGVyKGRhdGEgPT4ge1xyXG4gICAgICBjb25zdCBpZCA9IHRoaXMuZ2V0SXRlbUlkZW50aWZpZXIoZGF0YSk7XHJcbiAgICAgIHJldHVybiBpZCAmJiBzdGF0dXM/LmV4cGFuZGVkSXRlbUlkZW50aWZpZXJzPy5pbmRleE9mKGlkKSAhPT0gLTEgJiYgdGhpcy5pc0V4cGFuZGFibGUoZGF0YSk7XHJcbiAgICB9KTtcclxuICAgfVxyXG5cclxuICAgdGhpcy5zdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRTdGF0dXNTbmFwc2hvdCgpO1xyXG4gICB0aGlzLnBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QgPSB0aGlzLmJ1aWxkUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZWxvYWQoIGNvbnRleHQ6IElNYWlzVGFibGVSZWxvYWRDb250ZXh0ICk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgdGhpcy5mZXRjaGluZyA9IHRydWU7XHJcbiAgICB0aGlzLnNob3dGZXRjaGluZyA9ICFjb250ZXh0LmluQmFja2dyb3VuZDtcclxuICAgIGNvbnN0IG9iczogT2JzZXJ2YWJsZTx2b2lkPiA9IG5ldyBPYnNlcnZhYmxlPHZvaWQ+KHN1YnNjcmliZXIgPT4ge1xyXG4gICAgICB0cnkge1xyXG4gICAgICAgIHRoaXMucmVsb2FkSW5PYnNlcnZhYmxlKHN1YnNjcmliZXIsIGNvbnRleHQpO1xyXG4gICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgc3Vic2NyaWJlci5lcnJvcihlKTtcclxuICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIG9icy5zdWJzY3JpYmUoc3VjY2VzcyA9PiB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdhc3luYyByZWxvYWQgc3VjY2VzcycpO1xyXG4gICAgICB0aGlzLmZldGNoaW5nID0gZmFsc2U7XHJcbiAgICAgIGlmICghY29udGV4dC5pbkJhY2tncm91bmQpIHtcclxuICAgICAgICB0aGlzLnNob3dGZXRjaGluZyA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLmhhbmRsZU1hdFRhYmxlRGF0YVNuYXBzaG90Q2hhbmdlZCgpO1xyXG4gICAgfSwgZmFpbHVyZSA9PiB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdhc3luYyByZWxvYWQgZmFpbGVkJywgZmFpbHVyZSk7XHJcbiAgICAgIHRoaXMuZmV0Y2hpbmcgPSBmYWxzZTtcclxuICAgICAgaWYgKCFjb250ZXh0LmluQmFja2dyb3VuZCkge1xyXG4gICAgICAgIHRoaXMuc2hvd0ZldGNoaW5nID0gZmFsc2U7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuaGFuZGxlTWF0VGFibGVEYXRhU25hcHNob3RDaGFuZ2VkKCk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBvYnM7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlbG9hZEluT2JzZXJ2YWJsZSh0cmFja2VyOiBTdWJzY3JpYmVyPHZvaWQ+LCBjb250ZXh0OiBJTWFpc1RhYmxlUmVsb2FkQ29udGV4dCkge1xyXG4gICAgY29uc3Qgd2l0aFNuYXBzaG90OiBJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGwgPSBjb250ZXh0LndpdGhTdGF0dXNTbmFwc2hvdCB8fCBudWxsO1xyXG5cclxuICAgIGNvbnN0IHBhZ2VSZXF1ZXN0ID0gdGhpcy5idWlsZFBhZ2VSZXF1ZXN0KCk7XHJcblxyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ3JlbG9hZGluZyB0YWJsZSBkYXRhJywgcGFnZVJlcXVlc3QpO1xyXG5cclxuICAgIC8vIGNsZWFyIGNoZWNrZWQgaXRlbXNcclxuICAgIGlmICghY29udGV4dC5pbkJhY2tncm91bmQpIHtcclxuICAgICAgdGhpcy5jaGVja2VkSXRlbXMgPSBbXTtcclxuICAgICAgdGhpcy5leHBhbmRlZEl0ZW1zID0gW107XHJcblxyXG4gICAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIH1cclxuICAgIHRoaXMubGFzdEZldGNoZWRTZWFyY2hRdWVyeSA9IHBhZ2VSZXF1ZXN0LnF1ZXJ5IHx8IG51bGw7XHJcblxyXG4gICAgaWYgKHRoaXMuZGF0YVByb3ZpZGVyKSB7XHJcbiAgICAgIC8vIGNhbGwgZGF0YSBwcm92aWRlclxyXG4gICAgICB0aGlzLmxvZ2dlci50cmFjZSgncmVsb2FkIGhhcyBiZWVuIGNhbGxlZCwgZmV0Y2hpbmcgZGF0YSBmcm9tIHByb3ZpZGVkIGZ1bmN0aW9uJyk7XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdwYWdlIHJlcXVlc3QgZm9yIGRhdGEgZmV0Y2ggaXMnLCBwYWdlUmVxdWVzdCk7XHJcblxyXG4gICAgICB0aGlzLmRhdGFQcm92aWRlcihwYWdlUmVxdWVzdCwgY29udGV4dCkuc3Vic2NyaWJlKChyZXNwb25zZTogSU1haXNUYWJsZVBhZ2VSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdmZXRjaGluZyBkYXRhIGNvbXBsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmRyYWdJblByb2dyZXNzKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5wYWdpbmF0aW9uTW9kZSA9PT0gTWFpc1RhYmxlUGFnaW5hdGlvbk1ldGhvZC5TRVJWRVIpIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJzZVJlc3BvbnNlV2l0aFNlcnZlclBhZ2luYXRpb24ocmVzcG9uc2UpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJzZVJlc3BvbnNlV2l0aENsaWVudFBhZ2luYXRpb24ocmVzcG9uc2UuY29udGVudCwgcGFnZVJlcXVlc3QpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHdpdGhTbmFwc2hvdCkge1xyXG4gICAgICAgICAgICB0aGlzLmFwcGx5U3RhdHVzU25hcHNob3RQb3N0RmV0Y2god2l0aFNuYXBzaG90KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5sb2dnZXIud2FybignZGF0YSBmZXRjaCBhYm9ydGVkIGJlY2F1c2UgdXNlciBpcyBkcmFnZ2luZyB0aGluZ3MnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRyYWNrZXIubmV4dCgpO1xyXG4gICAgICAgIHRyYWNrZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZmFpbHVyZSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIuZXJyb3IoJ2Vycm9yIGZldGNoaW5nIGRhdGEgZnJvbSBwcm92aWRlciBmdW5jdGlvbicsIGZhaWx1cmUpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuZHJhZ0luUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgIHRoaXMuZGF0YVNuYXBzaG90ID0gW107XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ2RhdGEgZmV0Y2ggYWJvcnRlZCBiZWNhdXNlIHVzZXIgaXMgZHJhZ2dpbmcgdGhpbmdzJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0cmFja2VyLmVycm9yKGZhaWx1cmUpO1xyXG4gICAgICAgIHRyYWNrZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gZGF0YSBpcyBub3QgcHJvdmlkZWQgb24gcmVxdWVzdCBhbmQgaXMgaW4gY2xpZW50RGF0YVNuYXBzaG90XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdyZWxvYWQgaGFzIGJlZW4gY2FsbGVkIG9uIGxvY2FsbHkgZmV0Y2hlZCBkYXRhJyk7XHJcblxyXG4gICAgICBpZiAoIXRoaXMuZHJhZ0luUHJvZ3Jlc3MpIHtcclxuICAgICAgICB0aGlzLnBhcnNlUmVzcG9uc2VXaXRoQ2xpZW50UGFnaW5hdGlvbih0aGlzLmNsaWVudERhdGFTbmFwc2hvdCwgcGFnZVJlcXVlc3QpO1xyXG4gICAgICAgIGlmICh3aXRoU25hcHNob3QpIHtcclxuICAgICAgICAgIHRoaXMuYXBwbHlTdGF0dXNTbmFwc2hvdFBvc3RGZXRjaCh3aXRoU25hcHNob3QpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdkYXRhIGZldGNoIGFib3J0ZWQgYmVjYXVzZSB1c2VyIGlzIGRyYWdnaW5nIHRoaW5ncycpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0cmFja2VyLm5leHQoKTtcclxuICAgICAgdHJhY2tlci5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwYXJzZVJlc3BvbnNlV2l0aFNlcnZlclBhZ2luYXRpb24ocmVzcG9uc2U6IElNYWlzVGFibGVQYWdlUmVzcG9uc2UpIHtcclxuICAgIHRoaXMuZGF0YVNuYXBzaG90ID0gcmVzcG9uc2UuY29udGVudDtcclxuXHJcbiAgICBpZiAodGhpcy5jdXJyZW50RW5hYmxlUGFnaW5hdGlvbikge1xyXG4gICAgICBpZiAocmVzcG9uc2UudG90YWxQYWdlcykge1xyXG4gICAgICAgIHRoaXMuZmV0Y2hlZFBhZ2VDb3VudCA9IHJlc3BvbnNlLnRvdGFsUGFnZXM7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdkYXRhIGZyb20gc2VydmVyIGRpZCBub3QgY29udGFpbiByZXF1aXJlZCB0b3RhbFBhZ2VzIGZpZWxkJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChyZXNwb25zZS50b3RhbEVsZW1lbnRzKSB7XHJcbiAgICAgICAgdGhpcy5mZXRjaGVkUmVzdWx0TnVtYmVyID0gcmVzcG9uc2UudG90YWxFbGVtZW50cztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ2RhdGEgZnJvbSBzZXJ2ZXIgZGlkIG5vdCBjb250YWluIHJlcXVpcmVkIHRvdGFsRWxlbWVudHMgZmllbGQnKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwYXJzZVJlc3BvbnNlV2l0aENsaWVudFBhZ2luYXRpb24oZGF0YTogYW55W10sIHJlcXVlc3Q6IElNYWlzVGFibGVQYWdlUmVxdWVzdCkge1xyXG4gICAgdGhpcy5sb2dnZXIudHJhY2UoJ2FwcGx5aW5nIGluLW1lbW9yeSBmZXRjaGluZywgcGFnaW5hdGluZywgb3JkZXJpbmcgYW5kIGZpbHRlcmluZycpO1xyXG4gICAgY29uc3QgaW5NZW1vcnlSZXNwb25zZSA9IE1haXNUYWJsZUluTWVtb3J5SGVscGVyLmZldGNoSW5NZW1vcnkoXHJcbiAgICAgIGRhdGEsXHJcbiAgICAgIHJlcXVlc3QsXHJcbiAgICAgIHRoaXMuY3VycmVudFNvcnRDb2x1bW4sXHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcsXHJcbiAgICAgIHRoaXMuZ2V0Q3VycmVudExvY2FsZSgpICk7XHJcblxyXG4gICAgdGhpcy5kYXRhU25hcHNob3QgPSBpbk1lbW9yeVJlc3BvbnNlLmNvbnRlbnQ7XHJcbiAgICB0aGlzLmZldGNoZWRSZXN1bHROdW1iZXIgPSB0eXBlb2YgaW5NZW1vcnlSZXNwb25zZS50b3RhbEVsZW1lbnRzID09PSAndW5kZWZpbmVkJyA/IG51bGwgOiBpbk1lbW9yeVJlc3BvbnNlLnRvdGFsRWxlbWVudHM7XHJcbiAgICB0aGlzLmZldGNoZWRQYWdlQ291bnQgPSB0eXBlb2YgaW5NZW1vcnlSZXNwb25zZS50b3RhbFBhZ2VzID09PSAndW5kZWZpbmVkJyA/IG51bGwgOiBpbk1lbW9yeVJlc3BvbnNlLnRvdGFsUGFnZXM7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1aWxkUGFnZVJlcXVlc3QoKTogSU1haXNUYWJsZVBhZ2VSZXF1ZXN0IHtcclxuICAgIGNvbnN0IG91dHB1dDogSU1haXNUYWJsZVBhZ2VSZXF1ZXN0ID0ge1xyXG4gICAgICBwYWdlOiB0aGlzLmN1cnJlbnRFbmFibGVQYWdpbmF0aW9uID8gdGhpcy5jdXJyZW50UGFnZUluZGV4IDogbnVsbCxcclxuICAgICAgc2l6ZTogdGhpcy5jdXJyZW50RW5hYmxlUGFnaW5hdGlvbiA/IHRoaXMuY3VycmVudFBhZ2VTaXplIDogbnVsbCxcclxuICAgICAgc29ydDogW10sXHJcbiAgICAgIHF1ZXJ5OiBudWxsLFxyXG4gICAgICBxdWVyeUZpZWxkczogW10sXHJcbiAgICAgIGZpbHRlcnM6IFtdXHJcbiAgICB9O1xyXG5cclxuICAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVGaWx0ZXJpbmcgJiYgdGhpcy5zZWFyY2hRdWVyeUFjdGl2ZSkge1xyXG4gICAgICBvdXRwdXQucXVlcnkgPSB0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeTtcclxuICAgICAgb3V0cHV0LnF1ZXJ5RmllbGRzID0gdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5tYXAoY29sdW1uID0+IGNvbHVtbi5zZXJ2ZXJGaWVsZCB8fCBjb2x1bW4uZmllbGQgfHwgbnVsbCApO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmN1cnJlbnRFbmFibGVDb250ZXh0RmlsdGVyaW5nICYmIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzLmxlbmd0aCkge1xyXG4gICAgICBmb3IgKGNvbnN0IGZpbHRlciBvZiB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycykge1xyXG4gICAgICAgIG91dHB1dC5maWx0ZXJzPy5wdXNoKGZpbHRlci5uYW1lIHx8IG51bGwpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3Qgc29ydENvbHVtbiA9IHRoaXMuY3VycmVudFNvcnRDb2x1bW47XHJcbiAgICBjb25zdCBzb3J0RGlyZWN0aW9uID0gdGhpcy5jdXJyZW50U29ydERpcmVjdGlvbjtcclxuICAgIGlmIChzb3J0Q29sdW1uKSB7XHJcbiAgICAgIG91dHB1dC5zb3J0Py5wdXNoKHtcclxuICAgICAgICBwcm9wZXJ0eTogc29ydENvbHVtbi5zZXJ2ZXJGaWVsZCB8fCBzb3J0Q29sdW1uLmZpZWxkIHx8IG51bGwsXHJcbiAgICAgICAgZGlyZWN0aW9uOiBzb3J0RGlyZWN0aW9uIHx8IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBvdXRwdXQ7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldFBhZ2UoaW5kZXg6IG51bWJlcikge1xyXG4gICAgdGhpcy5zZWxlY3RlZFBhZ2VJbmRleCA9IGluZGV4O1xyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLmVtaXRQYWdlQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRSZWZyZXNoT25QdXNoSW5CYWNrZ3JvdW5kKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMucmVmcmVzaE9uUHVzaEluQmFja2dyb3VuZCA9PT0gdHJ1ZSB8fCB0aGlzLnJlZnJlc2hPblB1c2hJbkJhY2tncm91bmQgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnJlZnJlc2hPblB1c2hJbkJhY2tncm91bmQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucmVmcmVzaD8uZGVmYXVsdE9uUHVzaEluQmFja2dyb3VuZCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFJlZnJlc2hJbnRlcnZhbEluQmFja2dyb3VuZCgpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLnJlZnJlc2hJbnRlcnZhbEluQmFja2dyb3VuZCA9PT0gdHJ1ZSB8fCB0aGlzLnJlZnJlc2hJbnRlcnZhbEluQmFja2dyb3VuZCA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMucmVmcmVzaEludGVydmFsSW5CYWNrZ3JvdW5kO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnJlZnJlc2g/LmRlZmF1bHRPblRpY2tJbkJhY2tncm91bmQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRSZWZyZXNoU3RyYXRlZ2llcygpOiBNYWlzVGFibGVSZWZyZXNoU3RyYXRlZ3lbXSB7XHJcbiAgICBpZiAodGhpcy5yZWZyZXNoU3RyYXRlZ3kpIHtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5yZWZyZXNoU3RyYXRlZ3kpICYmIHRoaXMucmVmcmVzaFN0cmF0ZWd5Lmxlbmd0aCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlZnJlc2hTdHJhdGVneTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gW3RoaXMucmVmcmVzaFN0cmF0ZWd5IGFzIE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneV07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBbdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucmVmcmVzaD8uZGVmYXVsdFN0cmF0ZWd5IV07XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFJlZnJlc2hJbnRlcnZhbCgpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMucmVmcmVzaEludGVydmFsIHx8IHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnJlZnJlc2g/LmRlZmF1bHRJbnRlcnZhbCE7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFRhYmxlSWQoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLnRhYmxlSWQgfHwgdGhpcy51dWlkO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVEcmFnKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlRHJhZyA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZURyYWcgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZURyYWc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuZHJhZ0FuZERyb3A/LmRyYWdFbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlRHJvcCgpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZURyb3AgPT09IHRydWUgfHwgdGhpcy5lbmFibGVEcm9wID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVEcm9wO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmRyYWdBbmREcm9wPy5kcm9wRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZVBhZ2luYXRpb24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVQYWdpbmF0aW9uID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlUGFnaW5hdGlvbiA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlUGFnaW5hdGlvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5wYWdpbmF0aW9uPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlSXRlbVRyYWNraW5nKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlSXRlbVRyYWNraW5nID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlSXRlbVRyYWNraW5nID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVJdGVtVHJhY2tpbmc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuaXRlbVRyYWNraW5nPy5lbmFibGVkQnlEZWZhdWx0ITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50RW5hYmxlU3RvcmVQZXJzaXN0ZW5jZSgpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVN0b3JlUGVyc2lzdGVuY2UgPT09IHRydWUgfHwgdGhpcy5lbmFibGVTdG9yZVBlcnNpc3RlbmNlID09PSBmYWxzZSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5lbmFibGVTdG9yZVBlcnNpc3RlbmNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnN0b3JlUGVyc2lzdGVuY2U/LmVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVQYWdlU2l6ZVNlbGVjdCgpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVBhZ2VTaXplU2VsZWN0ID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlUGFnZVNpemVTZWxlY3QgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZVBhZ2VTaXplU2VsZWN0O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnBhZ2luYXRpb24/LnBhZ2VTaXplU2VsZWN0aW9uRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZU11bHRpcGxlUm93RXhwYW5zaW9uKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlTXVsdGlwbGVSb3dFeHBhbnNpb24gPT09IHRydWUgfHwgdGhpcy5lbmFibGVNdWx0aXBsZVJvd0V4cGFuc2lvbiA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlTXVsdGlwbGVSb3dFeHBhbnNpb247XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucm93RXhwYW5zaW9uPy5tdWx0aXBsZUV4cGFuc2lvbkVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVSb3dFeHBhbnNpb24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVSb3dFeHBhbnNpb24gPT09IHRydWUgfHwgdGhpcy5lbmFibGVSb3dFeHBhbnNpb24gPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZVJvd0V4cGFuc2lvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yb3dFeHBhbnNpb24/LmVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVIZWFkZXJBY3Rpb25zKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlSGVhZGVyQWN0aW9ucyA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZUhlYWRlckFjdGlvbnMgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZUhlYWRlckFjdGlvbnM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuYWN0aW9ucz8uaGVhZGVyQWN0aW9uc0VuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVDb250ZXh0QWN0aW9ucygpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZUNvbnRleHRBY3Rpb25zID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlQ29udGV4dEFjdGlvbnMgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZUNvbnRleHRBY3Rpb25zO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmFjdGlvbnM/LmNvbnRleHRBY3Rpb25zRW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZUNvbHVtbnNTZWxlY3Rpb24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVDb2x1bW5zU2VsZWN0aW9uID09PSB0cnVlIHx8IHRoaXMuZW5hYmxlQ29sdW1uc1NlbGVjdGlvbiA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlQ29sdW1uc1NlbGVjdGlvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jb2x1bW5Ub2dnbGluZz8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZUZpbHRlcmluZygpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZUZpbHRlcmluZyA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZUZpbHRlcmluZyA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlRmlsdGVyaW5nO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmZpbHRlcmluZz8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZU11bHRpU2VsZWN0KCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlTXVsdGlTZWxlY3QgPT09IHRydWUgfHwgdGhpcy5lbmFibGVNdWx0aVNlbGVjdCA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlTXVsdGlTZWxlY3Q7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucm93U2VsZWN0aW9uPy5tdWx0aXBsZVNlbGVjdGlvbkVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVTZWxlY3RBbGwoKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5lbmFibGVTZWxlY3RBbGwgPT09IHRydWUgfHwgdGhpcy5lbmFibGVTZWxlY3RBbGwgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZVNlbGVjdEFsbDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5yb3dTZWxlY3Rpb24/LnNlbGVjdEFsbEVuYWJsZWRCeURlZmF1bHQhO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRFbmFibGVDb250ZXh0RmlsdGVyaW5nKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlQ29udGV4dEZpbHRlcmluZyA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZUNvbnRleHRGaWx0ZXJpbmcgPT09IGZhbHNlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVuYWJsZUNvbnRleHRGaWx0ZXJpbmc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY29udGV4dEZpbHRlcmluZz8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEVuYWJsZVNlbGVjdGlvbigpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmVuYWJsZVNlbGVjdGlvbiA9PT0gdHJ1ZSB8fCB0aGlzLmVuYWJsZVNlbGVjdGlvbiA9PT0gZmFsc2UpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlU2VsZWN0aW9uO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnJvd1NlbGVjdGlvbj8uZW5hYmxlZEJ5RGVmYXVsdCE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFBhZ2luYXRpb25Nb2RlKCk6IE1haXNUYWJsZVBhZ2luYXRpb25NZXRob2Qge1xyXG4gICAgaWYgKHRoaXMucGFnaW5hdGlvbk1vZGUpIHtcclxuICAgICAgcmV0dXJuIHRoaXMucGFnaW5hdGlvbk1vZGU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkucGFnaW5hdGlvbj8uZGVmYXVsdFBhZ2luYXRpb25Nb2RlITtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBpdGVtVHJhY2tpbmdFbmFibGVkQW5kUG9zc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISF0aGlzLml0ZW1JZGVudGlmaWVyICYmIHRoaXMuY3VycmVudEVuYWJsZUl0ZW1UcmFja2luZztcclxuICB9XHJcblxyXG4gIGdldCBzdG9yZVBlcnNpc3RlbmNlRW5hYmxlZEFuZFBvc3NpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc3RvcmVBZGFwdGVyICYmIHRoaXMuY3VycmVudEVuYWJsZVN0b3JlUGVyc2lzdGVuY2U7XHJcbiAgfVxyXG5cclxuICBnZXQgcm93RXhwYW5zaW9uRW5hYmxlZEFuZFBvc3NpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudEVuYWJsZVJvd0V4cGFuc2lvbjtcclxuICB9XHJcblxyXG4gIGdldCBwYWdlU2l6ZVNlbGVjdEVuYWJsZWRBbmRQb3NzaWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplcyAmJiB0aGlzLmN1cnJlbnRQb3NzaWJsZVBhZ2VTaXplcy5sZW5ndGggPiAwICYmIHRoaXMuY3VycmVudEVuYWJsZVBhZ2VTaXplU2VsZWN0O1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbnRleHRGaWx0ZXJpbmdFbmFibGVkQW5kUG9zc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jb250ZXh0RmlsdGVycyAmJiB0aGlzLmNvbnRleHRGaWx0ZXJzLmxlbmd0aCA+IDAgJiYgdGhpcy5jdXJyZW50RW5hYmxlQ29udGV4dEZpbHRlcmluZztcclxuICB9XHJcblxyXG4gIGdldCBjb250ZXh0QWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmNvbnRleHRBY3Rpb25zICYmIHRoaXMuY29udGV4dEFjdGlvbnMubGVuZ3RoID4gMCAmJiB0aGlzLmN1cnJlbnRFbmFibGVDb250ZXh0QWN0aW9ucztcclxuICB9XHJcblxyXG4gIGdldCBoZWFkZXJBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaGVhZGVyQWN0aW9ucyAmJiB0aGlzLmhlYWRlckFjdGlvbnMubGVuZ3RoID4gMCAmJiB0aGlzLmN1cnJlbnRFbmFibGVIZWFkZXJBY3Rpb25zO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbHVtblNlbGVjdGlvblBvc3NpYmxlQW5kQWxsb3dlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmN1cnJlbnRFbmFibGVDb2x1bW5zU2VsZWN0aW9uICYmIHRoaXMuY29sdW1ucy5sZW5ndGggPiAwICYmIHRoaXMuaGlkZWFibGVDb2x1bW5zLmxlbmd0aCA+IDA7XHJcbiAgfVxyXG5cclxuICBnZXQgZmlsdGVyaW5nUG9zc2libGVBbmRBbGxvd2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudEVuYWJsZUZpbHRlcmluZyAmJiB0aGlzLmZpbHRlcmFibGVDb2x1bW5zLmxlbmd0aCA+IDA7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFBvc3NpYmxlUGFnZVNpemVzKCk6IG51bWJlcltdIHtcclxuICAgIGlmICh0aGlzLnBvc3NpYmxlUGFnZVNpemUgJiYgdGhpcy5wb3NzaWJsZVBhZ2VTaXplLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5wb3NzaWJsZVBhZ2VTaXplO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnBhZ2luYXRpb24/LmRlZmF1bHRQb3NzaWJsZVBhZ2VTaXplcyE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFNlYXJjaFF1ZXJ5KCk6IHN0cmluZyB8IG51bGwge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTZWFyY2hRdWVyeSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFBhZ2VDb3VudCgpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMuZmV0Y2hlZFBhZ2VDb3VudCB8fCAwO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRSZXN1bHROdW1iZXIoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLmZldGNoZWRSZXN1bHROdW1iZXIgfHwgMDtcclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50UGFnZVNpemUoKTogbnVtYmVyIHtcclxuICAgIGNvbnN0IGRlZiA9IHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLnBhZ2luYXRpb24/LmRlZmF1bHRQYWdlU2l6ZSE7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFBhZ2VTaXplKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkUGFnZVNpemU7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZGVmYXVsdFBhZ2VTaXplKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRQYWdlU2l6ZTtcclxuICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgdGhpcy5jdXJyZW50UG9zc2libGVQYWdlU2l6ZXMubGVuZ3RoICYmXHJcbiAgICAgICAgZGVmICYmXHJcbiAgICAgICAgdGhpcy5jdXJyZW50UG9zc2libGVQYWdlU2l6ZXMuaW5kZXhPZihkZWYpICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiBkZWY7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFBvc3NpYmxlUGFnZVNpemVzLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5jdXJyZW50UG9zc2libGVQYWdlU2l6ZXNbMF07XHJcbiAgICB9IGVsc2UgaWYgKGRlZikge1xyXG4gICAgICByZXR1cm4gZGVmO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIDEwO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnRQYWdlSW5kZXgoKTogbnVtYmVyIHtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkUGFnZUluZGV4KSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkUGFnZUluZGV4O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIDA7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudFNvcnRDb2x1bW4oKTogSU1haXNUYWJsZUNvbHVtbiB8IG51bGwge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTb3J0Q29sdW1uKSB7XHJcbiAgICAgIC8vIHRvZG8gbG9nIGlmIHNvcnRpbmcgY29sdW1uIGRpc2FwcGVhcnNcclxuICAgICAgcmV0dXJuIHRoaXMudmlzaWJsZUNvbHVtbnMuZmluZChjID0+IGMubmFtZSA9PT0gdGhpcy5zZWxlY3RlZFNvcnRDb2x1bW4/Lm5hbWUpIHx8IG51bGw7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZGVmYXVsdFNvcnRpbmdDb2x1bW4pIHtcclxuICAgICAgY29uc3QgZm91bmRDb2x1bW4gPSB0aGlzLnZpc2libGVDb2x1bW5zLmZpbmQoYyA9PiBjLm5hbWUgPT09IHRoaXMuZGVmYXVsdFNvcnRpbmdDb2x1bW4pO1xyXG4gICAgICBpZiAoZm91bmRDb2x1bW4pIHtcclxuICAgICAgICByZXR1cm4gZm91bmRDb2x1bW47XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gZmluZCBmaXJzdCBzb3J0YWJsZSBjb2x1bW4/XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmlzaWJsZUNvbHVtbnMuZmluZChjb2x1bW4gPT5cclxuICAgICAgICAgIE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc1NvcnRhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKSB8fCBudWxsO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBmaW5kIGZpcnN0IHNvcnRhYmxlIGNvbHVtbj9cclxuICAgICAgcmV0dXJuIHRoaXMudmlzaWJsZUNvbHVtbnMuZmluZChjb2x1bW4gPT5cclxuICAgICAgICBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNTb3J0YWJsZShjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKSkgfHwgbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBjdXJyZW50U29ydERpcmVjdGlvbigpOiBNYWlzVGFibGVTb3J0RGlyZWN0aW9uIHtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkU29ydERpcmVjdGlvbikge1xyXG4gICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZFNvcnREaXJlY3Rpb247XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZGVmYXVsdFNvcnRpbmdEaXJlY3Rpb24pIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdFNvcnRpbmdEaXJlY3Rpb24gPT09IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uREVTQ0VORElORyA/XHJcbiAgICAgIE1haXNUYWJsZVNvcnREaXJlY3Rpb24uREVTQ0VORElORyA6IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gcmV0dXJuIERFRkFVTFRcclxuICAgICAgcmV0dXJuIE1haXNUYWJsZVNvcnREaXJlY3Rpb24uQVNDRU5ESU5HO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGN1cnJlbnREYXRhKCk6IGFueVtdIHtcclxuICAgIHJldHVybiB0aGlzLmRhdGFTbmFwc2hvdCB8fCBbXTtcclxuICB9XHJcblxyXG4gIGdldCBlbnVtUGFnZXMoKTogKG51bWJlciB8IHsgc2tpcDogYm9vbGVhbjsgfSlbXSB7XHJcbiAgICBjb25zdCByYW5nZVN0YXJ0ID0gMTtcclxuICAgIGNvbnN0IHJhbmdlRW5kID0gMTtcclxuICAgIGNvbnN0IHJhbmdlU2VsZWN0ZWQgPSAxO1xyXG5cclxuICAgIGNvbnN0IHBhZ2VzID0gW107XHJcbiAgICBjb25zdCBwYWdlTnVtID0gdGhpcy5jdXJyZW50UGFnZUNvdW50O1xyXG4gICAgY29uc3QgY3VycmVudEluZGV4ID0gdGhpcy5jdXJyZW50UGFnZUluZGV4O1xyXG4gICAgbGV0IGlzU2tpcHBpbmcgPSBmYWxzZTtcclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhZ2VOdW07IGkgKyspIHtcclxuICAgICAgaWYgKE1hdGguYWJzKGkgLSBjdXJyZW50SW5kZXgpIDw9IChyYW5nZVNlbGVjdGVkKSB8fCBpIDw9IChyYW5nZVN0YXJ0IC0gMSkgfHwgaSA+PSAocGFnZU51bSAtIHJhbmdlRW5kKSkge1xyXG4gICAgICAgIGlzU2tpcHBpbmcgPSBmYWxzZTtcclxuICAgICAgICBwYWdlcy5wdXNoKGkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmICghaXNTa2lwcGluZykge1xyXG4gICAgICAgICAgcGFnZXMucHVzaCh7XHJcbiAgICAgICAgICAgIHNraXA6IHRydWVcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaXNTa2lwcGluZyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcGFnZXM7XHJcbiAgfVxyXG5cclxuICBnZXQgYWN0aXZlQ29sdW1uc0NvdW50KCk6IG51bWJlciB7XHJcbiAgICBsZXQgb3V0cHV0ID0gdGhpcy52aXNpYmxlQ29sdW1ucy5sZW5ndGg7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50RW5hYmxlU2VsZWN0aW9uKSB7XHJcbiAgICAgIG91dHB1dCArKztcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmNvbnRleHRGaWx0ZXJpbmdFbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgb3V0cHV0ICsrO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucm93RXhwYW5zaW9uRW5hYmxlZEFuZFBvc3NpYmxlKSB7XHJcbiAgICAgIG91dHB1dCArKztcclxuICAgIH1cclxuICAgIHJldHVybiBvdXRwdXQ7XHJcbiAgfVxyXG5cclxuICBnZXQgbm9SZXN1bHRzKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMucGFnaW5hdGlvbk1vZGUgPT09IE1haXNUYWJsZVBhZ2luYXRpb25NZXRob2QuQ0xJRU5UKSB7XHJcbiAgICAgIHJldHVybiAhdGhpcy5jdXJyZW50RGF0YS5sZW5ndGg7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gIXRoaXMuZGF0YVNuYXBzaG90Lmxlbmd0aDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldCBoaWRlYWJsZUNvbHVtbnMoKTogSU1haXNUYWJsZUNvbHVtbltdIHtcclxuICAgIHJldHVybiB0aGlzLmNvbHVtbnMuZmlsdGVyKGMgPT4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzSGlkZWFibGUoYywgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKTtcclxuICB9XHJcblxyXG4gIGdldCB2aXNpYmxlQ29sdW1ucygpOiBJTWFpc1RhYmxlQ29sdW1uW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sdW1ucy5maWx0ZXIoYyA9PiB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5pbmRleE9mKGMpICE9PSAtMSk7XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudENvbnRleHRGaWx0ZXJzKCk6IElNYWlzVGFibGVDb250ZXh0RmlsdGVyW10ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dEZpbHRlcnMgfHwgW107XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEhlYWRlckFjdGlvbnMoKTogSU1haXNUYWJsZUFjdGlvbltdIHtcclxuICAgIHJldHVybiB0aGlzLmhlYWRlckFjdGlvbnMgfHwgW107XHJcbiAgfVxyXG5cclxuICBnZXQgY3VycmVudEFjdGlvbnMoKTogSU1haXNUYWJsZUFjdGlvbltdIHtcclxuICAgIHJldHVybiB0aGlzLmNvbnRleHRBY3Rpb25zIHx8IFtdO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGhhc0FjdGlvbnMoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50QWN0aW9ucy5sZW5ndGggPiAwO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHNlYXJjaFF1ZXJ5TmVlZEFwcGx5KCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICh0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeSB8fCAnJykgIT09ICh0aGlzLmxhc3RGZXRjaGVkU2VhcmNoUXVlcnkgfHwgJycpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHNlYXJjaFF1ZXJ5QWN0aXZlKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IHNlYXJjaFF1ZXJ5TWFsZm9ybWVkKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGZpbHRlcmFibGVDb2x1bW5zKCk6IElNYWlzVGFibGVDb2x1bW5bXSB7XHJcbiAgICByZXR1cm4gdGhpcy5jb2x1bW5zLmZpbHRlcihjID0+IHRoaXMuaXNGaWx0ZXJhYmxlKGMpKTtcclxuICB9XHJcblxyXG4gIGdldCBhbnlCdXR0b25BY3Rpb25zQWxsb3dlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhIXRoaXMuY29udGV4dEFjdGlvbnMuZmluZChhID0+IHRoaXMuaXNDb250ZXh0QWN0aW9uQWxsb3dlZChhKSk7XHJcbiAgfVxyXG5cclxuICBnZXQgYW55SGVhZGVyQWN0aW9uc0FsbG93ZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISF0aGlzLmhlYWRlckFjdGlvbnMuZmluZChhID0+IHRoaXMuaXNIZWFkZXJBY3Rpb25BbGxvd2VkKGEpKTtcclxuICB9XHJcblxyXG4gIGdldCBjb25maWdDb2x1bW5WaXNpYmlsaXR5U2hvd0ZpeGVkQ29sdW1ucygpIHtcclxuICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5jb2x1bW5Ub2dnbGluZz8uc2hvd0ZpeGVkQ29sdW1ucztcclxuICB9XHJcblxyXG4gIGdldCBjb25maWdGaWx0ZXJpbmdTaG93VW5maWx0ZXJhYmxlQ29sdW1ucygpIHtcclxuICAgIHJldHVybiB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5maWx0ZXJpbmc/LnNob3dVbmZpbHRlcmFibGVDb2x1bW5zO1xyXG4gIH1cclxuXHJcbiAgLy8gZHJhZyBhbmQgZHJvcCBzdXBwb3J0XHJcblxyXG4gIGFjY2VwdERyb3BQcmVkaWNhdGUgPSAoaXRlbTogQ2RrRHJhZzxhbnk+KSA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5hY2NlcHREcm9wO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFjY2VwdERyb3AoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RW5hYmxlRHJvcDtcclxuICB9XHJcblxyXG4gIGdldCBhY2NlcHREcmFnKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudEVuYWJsZURyYWc7XHJcbiAgfVxyXG5cclxuICBnZXQgcmVmZXJlbmNlZFRhYmxlKCk6IE1haXNUYWJsZUNvbXBvbmVudCB8IG51bGwge1xyXG4gICAgY29uc3QgdiA9IHRoaXMuZ2V0UmVmZXJlbmNlZFRhYmxlKCk7XHJcbiAgICByZXR1cm4gdjtcclxuICB9XHJcblxyXG4gIGdldFJlZmVyZW5jZWRUYWJsZSgpOiBNYWlzVGFibGVDb21wb25lbnQgfCBudWxsIHtcclxuICAgIGlmICh0aGlzLmRyb3BDb25uZWN0ZWRUbyAmJiB0aGlzLmRyb3BDb25uZWN0ZWRUby5sZW5ndGgpIHtcclxuICAgICAgY29uc3QgY29ubmVjdGVkVG9MaXN0OiBzdHJpbmdbXSA9IChBcnJheS5pc0FycmF5KHRoaXMuZHJvcENvbm5lY3RlZFRvKSkgPyB0aGlzLmRyb3BDb25uZWN0ZWRUbyA6IFt0aGlzLmRyb3BDb25uZWN0ZWRUb107XHJcbiAgICAgIGNvbnN0IGZvdW5kID0gW107XHJcbiAgICAgIGZvciAoY29uc3QgY29ubmVjdGVkVG9Ub2tlbiBvZiBjb25uZWN0ZWRUb0xpc3QpIHtcclxuICAgICAgICBjb25zdCByZWdpc3RlcmVkTGlzdDogYW55W10gPSB0aGlzLnJlZ2lzdHJ5LmdldChjb25uZWN0ZWRUb1Rva2VuKTtcclxuICAgICAgICBpZiAocmVnaXN0ZXJlZExpc3QgJiYgcmVnaXN0ZXJlZExpc3QubGVuZ3RoKSB7XHJcbiAgICAgICAgICBmb3IgKGNvbnN0IHJlZ0NvbXAgb2YgcmVnaXN0ZXJlZExpc3QpIHtcclxuICAgICAgICAgICAgZm91bmQucHVzaChyZWdDb21wKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGZvdW5kLmxlbmd0aCA8IDEpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgfSBlbHNlIGlmIChmb3VuZC5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIuZXJyb3IoJ01VTFRJUExFIFRBQkxFIFJFRkVSRU5DRUQgU1RJTEwgQUNUSVZFJywgZm91bmQpO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBmb3VuZFswXS5jb21wb25lbnQ7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gdXNlciBhY3Rpb25zXHJcblxyXG4gIGhhbmRsZUl0ZW1EcmFnZ2VkKGV2ZW50OiBDZGtEcmFnRHJvcDxhbnlbXT4sIGZyb206IE1haXNUYWJsZUNvbXBvbmVudCwgdG86IE1haXNUYWJsZUNvbXBvbmVudCkge1xyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ2l0ZW0gZHJhZ2dlZCBmcm9tIHRhYmxlICcgKyB0aGlzLmN1cnJlbnRUYWJsZUlkKTtcclxuICAgIHRoaXMuaXRlbURyYWdnZWQuZW1pdCh7XHJcbiAgICAgIGl0ZW06IGV2ZW50Lml0ZW0uZGF0YSxcclxuICAgICAgZXZlbnQsXHJcbiAgICAgIGZyb21EYXRhU25hcHNob3Q6IGZyb20uZ2V0RGF0YVNuYXBzaG90KCksXHJcbiAgICAgIHRvRGF0YVNuYXBzaG90OiB0by5nZXREYXRhU25hcHNob3QoKSxcclxuICAgICAgZnJvbUNvbXBvbmVudDogZnJvbSxcclxuICAgICAgdG9Db21wb25lbnQ6IHRvXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGhhbmRsZUl0ZW1Ecm9wcGVkKGV2ZW50OiBDZGtEcmFnRHJvcDxhbnlbXT4pIHtcclxuICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdEUk9QIEVWRU5UIEZST00gVEFCTEUgJyArIHRoaXMuY3VycmVudFRhYmxlSWQsIGV2ZW50KTtcclxuICAgIGNvbnN0IGRyb3BwZWRTb3VyY2U6IE1haXNUYWJsZUNvbXBvbmVudCB8IG51bGwgPSB0aGlzLnJlZ2lzdHJ5LmdldFNpbmdsZShldmVudC5wcmV2aW91c0NvbnRhaW5lci5pZCk7XHJcbiAgICBjb25zdCBkcm9wcGVkVGFyZ2V0OiBNYWlzVGFibGVDb21wb25lbnQgfCBudWxsID0gdGhpcy5yZWdpc3RyeS5nZXRTaW5nbGUoZXZlbnQuY29udGFpbmVyLmlkKTtcclxuXHJcbiAgICBpZiAoIWRyb3BwZWRUYXJnZXQpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignTk8gRFJPUCBUQVJHRVQgRk9VTkQnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghZHJvcHBlZFRhcmdldC5hY2NlcHREcm9wKSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLmRlYnVnKCdza2lwcGluZyBkcm9wIG9uIGNvbnRhaW5lciB3aXRoIGFjY2VwdERyb3AgPSBmYWxzZScpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGRyb3BwZWRTb3VyY2UpIHtcclxuICAgICAgZHJvcHBlZFNvdXJjZS5oYW5kbGVJdGVtRHJhZ2dlZChldmVudCwgZHJvcHBlZFNvdXJjZSwgZHJvcHBlZFRhcmdldCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ2l0ZW0gZHJvcHBlZCBvbiB0YWJsZSAnICsgZHJvcHBlZFRhcmdldC5jdXJyZW50VGFibGVJZCk7XHJcbiAgICB0aGlzLml0ZW1Ecm9wcGVkLmVtaXQoe1xyXG4gICAgICBpdGVtOiBldmVudC5pdGVtLmRhdGEsXHJcbiAgICAgIGV2ZW50LFxyXG4gICAgICBmcm9tRGF0YVNuYXBzaG90OiBkcm9wcGVkU291cmNlID8gZHJvcHBlZFNvdXJjZS5nZXREYXRhU25hcHNob3QoKSA6IG51bGwsXHJcbiAgICAgIHRvRGF0YVNuYXBzaG90OiBkcm9wcGVkVGFyZ2V0LmdldERhdGFTbmFwc2hvdCgpLFxyXG4gICAgICBmcm9tQ29tcG9uZW50OiBkcm9wcGVkU291cmNlLFxyXG4gICAgICB0b0NvbXBvbmVudDogZHJvcHBlZFRhcmdldFxyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gSSdtIHNvcnJ5XHJcbiAgICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xyXG4gICAgICB0aGlzLmRhdGFTbmFwc2hvdCA9IHRoaXMuZGF0YVNuYXBzaG90Lm1hcChvID0+IG8pO1xyXG4gICAgICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5mb3JjZVJlUmVuZGVyID0gdHJ1ZSk7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5mb3JjZVJlUmVuZGVyID0gZmFsc2UpO1xyXG4gICAgICB0aGlzLmhhbmRsZU1hdFRhYmxlRGF0YVNuYXBzaG90Q2hhbmdlZCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhcHBseVNlbGVjdGVkRmlsdGVyKCkge1xyXG4gICAgdGhpcy5zZXRQYWdlKDApO1xyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLnJlbG9hZCh7cmVhc29uOiBNYWlzVGFibGVSZWxvYWRSZWFzb24uVVNFUn0pO1xyXG4gIH1cclxuXHJcbiAgY2xpY2tPblJvd0V4cGFuc2lvbihpdGVtOiBhbnkpIHtcclxuICAgIGlmICghdGhpcy5yb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLmlzRXhwYW5kYWJsZShpdGVtKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuaXNFeHBhbmRlZChpdGVtKSkge1xyXG4gICAgICB0aGlzLmV4cGFuZGVkSXRlbXMuc3BsaWNlKHRoaXMuZXhwYW5kZWRJdGVtcy5pbmRleE9mKGl0ZW0pLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlTXVsdGlwbGVSb3dFeHBhbnNpb24pIHtcclxuICAgICAgICB0aGlzLmV4cGFuZGVkSXRlbXMgPSBbXTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmV4cGFuZGVkSXRlbXMucHVzaChpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmhhbmRsZU1hdFRhYmxlRGF0YVNuYXBzaG90Q2hhbmdlZCgpO1xyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICBjbGlja09uQ29udGV4dEFjdGlvbihhY3Rpb246IElNYWlzVGFibGVBY3Rpb24pIHtcclxuICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlQ29udGV4dEFjdGlvbnMpIHtcclxuICAgICAgdGhpcy5sb2dnZXIud2FybignYnV0dG9uIGFjdGlvbnMgYXJlIGRpc2FibGVkJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5pc0NvbnRleHRBY3Rpb25BbGxvd2VkKGFjdGlvbikpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGRpc3BhdGNoQ29udGV4dDogSU1haXNUYWJsZUFjdGlvbkRpc3BhdGNoaW5nQ29udGV4dCA9IHtcclxuICAgICAgYWN0aW9uLFxyXG4gICAgICBzZWxlY3RlZEl0ZW1zOiB0aGlzLmNoZWNrZWRJdGVtc1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnZGlzcGF0Y2hpbmcgYWN0aW9uICcgKyBhY3Rpb24ubmFtZSArICcgd2l0aCBwYXlsb2FkJywgZGlzcGF0Y2hDb250ZXh0KTtcclxuXHJcbiAgICB0aGlzLmFjdGlvbi5lbWl0KGRpc3BhdGNoQ29udGV4dCk7XHJcbiAgfVxyXG5cclxuICBjbGlja09uUGFnZVNpemUocGFnZVNpemU6IG51bWJlcikge1xyXG4gICAgdGhpcy5zZWxlY3RlZFBhZ2VTaXplID0gcGFnZVNpemU7XHJcbiAgICB0aGlzLnNldFBhZ2UoMCk7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5VU0VSfSk7XHJcbiAgfVxyXG5cclxuICBjbGlja09uSGVhZGVyQWN0aW9uKGFjdGlvbjogSU1haXNUYWJsZUFjdGlvbikge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVIZWFkZXJBY3Rpb25zKSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ2J1dHRvbiBhY3Rpb25zIGFyZSBkaXNhYmxlZCcpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuaXNIZWFkZXJBY3Rpb25BbGxvd2VkKGFjdGlvbikpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGRpc3BhdGNoQ29udGV4dDogSU1haXNUYWJsZUFjdGlvbkRpc3BhdGNoaW5nQ29udGV4dCA9IHtcclxuICAgICAgYWN0aW9uLFxyXG4gICAgICBzZWxlY3RlZEl0ZW1zOiB0aGlzLmNoZWNrZWRJdGVtc1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygnZGlzcGF0Y2hpbmcgYWN0aW9uICcgKyBhY3Rpb24ubmFtZSArICcgd2l0aCBwYXlsb2FkJywgZGlzcGF0Y2hDb250ZXh0KTtcclxuXHJcbiAgICB0aGlzLmFjdGlvbi5lbWl0KGRpc3BhdGNoQ29udGV4dCk7XHJcbiAgfVxyXG5cclxuICBzZWFyY2hRdWVyeUZvY3VzT3V0KCkge1xyXG4gICAgY29uc3QgY29tcCA9IHRoaXM7XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgaWYgKChjb21wLnNlbGVjdGVkU2VhcmNoUXVlcnkgfHwgJycpICE9PSAoY29tcC5sYXN0RmV0Y2hlZFNlYXJjaFF1ZXJ5IHx8ICcnKSkge1xyXG4gICAgICAgIGlmIChjb21wLnNlbGVjdGVkU2VhcmNoUXVlcnkpIHtcclxuICAgICAgICAgIGlmICh0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5maWx0ZXJpbmc/LmF1dG9SZWxvYWRPblF1ZXJ5Q2hhbmdlKSB7XHJcbiAgICAgICAgICAgIGNvbXAuYXBwbHlTZWxlY3RlZEZpbHRlcigpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gY29tcC5zZWxlY3RlZFNlYXJjaFF1ZXJ5ID0gY29tcC5sYXN0RmV0Y2hlZFNlYXJjaFF1ZXJ5O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuZmlsdGVyaW5nPy5hdXRvUmVsb2FkT25RdWVyeUNsZWFyKSB7XHJcbiAgICAgICAgICAgIGNvbXAuYXBwbHlTZWxlY3RlZEZpbHRlcigpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuZmlsdGVyaW5nPy5hdXRvUmVsb2FkVGltZW91dCk7XHJcbiAgfVxyXG5cclxuICBzd2l0Y2hUb1BhZ2UocGFnZUluZGV4OiBudW1iZXIsIGNvbnRleHQ/OiBJTWFpc1RhYmxlUmVsb2FkQ29udGV4dCkge1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFBhZ2VJbmRleCA9PT0gcGFnZUluZGV4KSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICghY29udGV4dCkge1xyXG4gICAgICBjb250ZXh0ID0ge3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLlVTRVJ9O1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc2V0UGFnZShwYWdlSW5kZXgpO1xyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLnJlbG9hZChjb250ZXh0KTtcclxuICB9XHJcblxyXG4gIGNsaWNrT25Db2x1bW4oY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICBpZiAoIU1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc1NvcnRhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzb3J0Q29sdW1uID0gdGhpcy5jdXJyZW50U29ydENvbHVtbjtcclxuICAgIGNvbnN0IHNvcnREaXJlY3Rpb24gPSB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uO1xyXG5cclxuICAgIGlmIChzb3J0Q29sdW1uICYmIHNvcnRDb2x1bW4ubmFtZSA9PT0gY29sdW1uLm5hbWUpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFNvcnREaXJlY3Rpb24gPSAoc29ydERpcmVjdGlvbiA9PT0gTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HID9cclxuICAgICAgICBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLkFTQ0VORElORyA6IE1haXNUYWJsZVNvcnREaXJlY3Rpb24uREVTQ0VORElORyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU29ydENvbHVtbiA9IGNvbHVtbjtcclxuICAgICAgdGhpcy5zZWxlY3RlZFNvcnREaXJlY3Rpb24gPSBNYWlzVGFibGVTb3J0RGlyZWN0aW9uLkFTQ0VORElORztcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmVtaXRTb3J0Q2hhbmdlZCgpO1xyXG5cclxuICAgIC8vIGZvcnphIHJpdG9ybm8gYWxsYSBwcmltYSBwYWdpbmFcclxuICAgIHRoaXMuc2V0UGFnZSgwKTtcclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5yZWxvYWQoe3JlYXNvbjogTWFpc1RhYmxlUmVsb2FkUmVhc29uLlVTRVJ9KTtcclxuICB9XHJcblxyXG4gIC8vIHBhcnNpbmcgZnVuY3Rpb25zXHJcblxyXG4gIGdldEl0ZW1JZGVudGlmaWVyKGl0ZW06IGFueSkge1xyXG4gICAgaWYgKHRoaXMuaXRlbVRyYWNraW5nRW5hYmxlZEFuZFBvc3NpYmxlKSB7XHJcbiAgICAgIGlmICgodGhpcy5pdGVtSWRlbnRpZmllciBhcyBhbnkpLmV4dHJhY3QpIHtcclxuICAgICAgICByZXR1cm4gKHRoaXMuaXRlbUlkZW50aWZpZXIgYXMgSU1haXNUYWJsZUlkZW50aWZpZXJQcm92aWRlcikuZXh0cmFjdChpdGVtKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmdldFByb3BlcnR5VmFsdWUoaXRlbSwgdGhpcy5pdGVtSWRlbnRpZmllciBhcyBzdHJpbmcpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldEN1cnJlbnRMb2NhbGUoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UuZ2V0RGVmYXVsdExhbmcoKTtcclxuICB9XHJcblxyXG4gIGV4dHJhY3RWYWx1ZShyb3c6IGFueSwgY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmV4dHJhY3RWYWx1ZShyb3csIGNvbHVtbiwgdGhpcy5nZXRDdXJyZW50TG9jYWxlKCkpO1xyXG4gIH1cclxuXHJcbiAgcmVzb2x2ZUxhYmVsKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbiB8IElNYWlzVGFibGVBY3Rpb24gfCBJTWFpc1RhYmxlQ29udGV4dEZpbHRlcikge1xyXG4gICAgcmV0dXJuIHRoaXMucmVzb2x2ZUxhYmVsT3JGaXhlZChjb2x1bW4ubGFiZWxLZXksIGNvbHVtbi5sYWJlbCk7XHJcbiAgfVxyXG5cclxuICByZXNvbHZlTGFiZWxPckZpeGVkKGtleTogc3RyaW5nIHwgbnVsbCB8IHVuZGVmaW5lZCwgZml4ZWQ6ICBzdHJpbmcgfCBudWxsIHwgdW5kZWZpbmVkKSB7XHJcbiAgICBpZiAoa2V5KSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UuaW5zdGFudChrZXkpO1xyXG4gICAgfSBlbHNlIGlmIChmaXhlZCkge1xyXG4gICAgICByZXR1cm4gZml4ZWQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uLCBkaXJlY3Rpb24gPSBudWxsKSB7XHJcbiAgICBjb25zdCBzb3J0Q29sdW1uID0gdGhpcy5jdXJyZW50U29ydENvbHVtbjtcclxuICAgIGlmIChzb3J0Q29sdW1uICYmIGNvbHVtbiAmJiBjb2x1bW4ubmFtZSA9PT0gc29ydENvbHVtbi5uYW1lKSB7XHJcbiAgICAgIGlmICghZGlyZWN0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGRpcmVjdGlvbiA9PT0gdGhpcy5jdXJyZW50U29ydERpcmVjdGlvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzRGVmYXVsdFZpc2libGVDb2x1bW4oY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzRGVmYXVsdFZpc2libGVDb2x1bW4oY29sdW1uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSk7XHJcbiAgfVxyXG5cclxuICBpc0RlZmF1bHRGaWx0ZXJDb2x1bW4oY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzRGVmYXVsdEZpbHRlckNvbHVtbihjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGlzRXhwYW5kYWJsZShyb3c6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY3VycmVudEVuYWJsZVJvd0V4cGFuc2lvbiAmJlxyXG4gICAgICAoIXRoaXMuZXhwYW5kYWJsZVN0YXR1c1Byb3ZpZGVyIHx8IHRoaXMuZXhwYW5kYWJsZVN0YXR1c1Byb3ZpZGVyKHJvdywgdGhpcy5zdGF0dXNTbmFwc2hvdCkpO1xyXG4gIH1cclxuXHJcbiAgaXNIaWRlYWJsZShjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGlzU29ydGFibGUoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzU29ydGFibGUoY29sdW1uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSk7XHJcbiAgfVxyXG5cclxuICBpc0ZpbHRlcmFibGUoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmlzRmlsdGVyYWJsZShjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGhhc0xhYmVsKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbikge1xyXG4gICAgcmV0dXJuIE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5oYXNMYWJlbChjb2x1bW4sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKTtcclxuICB9XHJcblxyXG4gIGlzTGFiZWxWaXNpYmxlSW5UYWJsZShjb2x1bW46IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNMYWJlbFZpc2libGVJblRhYmxlKGNvbHVtbiwgdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkpO1xyXG4gIH1cclxuXHJcbiAgaXNDb250ZXh0QWN0aW9uQWxsb3dlZChhY3Rpb246IElNYWlzVGFibGVBY3Rpb24pOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcclxuICAgIHJldHVybiB0aGlzLmlzQWN0aW9uQWxsb3dlZChhY3Rpb24sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmFjdGlvbnM/LmRlZmF1bHRDb250ZXh0QWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbik7XHJcbiAgfVxyXG5cclxuICBpc0hlYWRlckFjdGlvbkFsbG93ZWQoYWN0aW9uOiBJTWFpc1RhYmxlQWN0aW9uKTogYm9vbGVhbiB8IHVuZGVmaW5lZCB7XHJcbiAgICByZXR1cm4gdGhpcy5pc0FjdGlvbkFsbG93ZWQoYWN0aW9uLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKS5hY3Rpb25zPy5kZWZhdWx0SGVhZGVyQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbik7XHJcbiAgfVxyXG5cclxuICBpc0FjdGlvbkFsbG93ZWQoYWN0aW9uOiBJTWFpc1RhYmxlQWN0aW9uLCBkZWY6IE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24gfCB1bmRlZmluZWQpIHtcclxuICAgIGNvbnN0IGFjdENvbmQgPSBhY3Rpb24uYWN0aXZhdGlvbkNvbmRpdGlvbiB8fCBkZWY7XHJcbiAgICBpZiAoYWN0Q29uZCA9PT0gTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5BTFdBWVMpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKGFjdENvbmQgPT09IE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24uTkVWRVIpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSBlbHNlIGlmIChhY3RDb25kID09PSBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLkRZTkFNSUMpIHtcclxuICAgICAgLy8gZGVsZWdhIGRlY2lzaW9uZSBhIHByb3ZpZGVyIGVzdGVybm9cclxuICAgICAgaWYgKCF0aGlzLmFjdGlvblN0YXR1c1Byb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIuZXJyb3IoJ0FjdGlvbiB3aXRoIGR5bmFtaWMgZW5hYmxpbmcgYnV0IG5vIGFjdGlvblN0YXR1c1Byb3ZpZGVyIHByb3ZpZGVkJyk7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5hY3Rpb25TdGF0dXNQcm92aWRlcihhY3Rpb24sIHRoaXMuc3RhdHVzU25hcHNob3QgfHwgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgbnVtU2VsZWN0ZWQgPSB0aGlzLmNoZWNrZWRJdGVtcy5sZW5ndGg7XHJcbiAgICBpZiAoYWN0Q29uZCA9PT0gTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5TSU5HTEVfU0VMRUNUSU9OKSB7XHJcbiAgICAgIHJldHVybiBudW1TZWxlY3RlZCA9PT0gMTtcclxuICAgIH0gZWxzZSBpZiAoYWN0Q29uZCA9PT0gTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5NVUxUSVBMRV9TRUxFQ1RJT04pIHtcclxuICAgICAgcmV0dXJuIG51bVNlbGVjdGVkID4gMDtcclxuICAgIH0gZWxzZSBpZiAoYWN0Q29uZCA9PT0gTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbi5OT19TRUxFQ1RJT04pIHtcclxuICAgICAgcmV0dXJuIG51bVNlbGVjdGVkIDwgMTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcignVW5rbm93biBhY3Rpb24gYWN0aXZhdGlvbiBjb25kaXRpb246ICcgKyBhY3RDb25kKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIGdlc3Rpb25lIGRlbGxlIHJpZ2hlIGVzcGFuc2VcclxuXHJcbiAgaXNFeHBhbmRlZChpdGVtOiBhbnkpIHtcclxuICAgIHJldHVybiB0aGlzLmV4cGFuZGVkSXRlbXMuaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbiAgfVxyXG5cclxuICAvLyBjaGVja2JveCBzZWxlY3QgcGVyIGl0ZW1zXHJcblxyXG4gIGlzQ2hlY2tlZChpdGVtOiBhbnkpIHtcclxuICAgIHJldHVybiB0aGlzLmNoZWNrZWRJdGVtcy5pbmRleE9mKGl0ZW0pICE9PSAtMTtcclxuICB9XHJcblxyXG4gIGdldCBhbGxDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5jdXJyZW50RGF0YTtcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRJdGVtcy5pbmRleE9mKGl0ZW0pID09PSAtMSkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXQgYW55Q2hlY2tlZCgpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IGRhdGFJdGVtcyA9IHRoaXMuY3VycmVudERhdGE7XHJcbiAgICBpZiAoIWRhdGFJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGFJdGVtcykge1xyXG4gICAgICBpZiAodGhpcy5jaGVja2VkSXRlbXMuaW5kZXhPZihpdGVtKSAhPT0gLTEpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG5vbmVDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICF0aGlzLmFueUNoZWNrZWQ7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVDaGVja2VkKGl0ZW06IGFueSkge1xyXG4gICAgaWYgKHRoaXMuaXNDaGVja2VkKGl0ZW0pKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZEl0ZW1zLnNwbGljZSh0aGlzLmNoZWNrZWRJdGVtcy5pbmRleE9mKGl0ZW0pLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlTXVsdGlTZWxlY3QpIHtcclxuICAgICAgICB0aGlzLmNoZWNrZWRJdGVtcyA9IFtdO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuY2hlY2tlZEl0ZW1zLnB1c2goaXRlbSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGF0dXNDaGFuZ2VkKCk7XHJcbiAgICB0aGlzLmVtaXRTZWxlY3Rpb25DaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVBbGxDaGVja2VkKCkge1xyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVTZWxlY3RBbGwpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLmN1cnJlbnRFbmFibGVNdWx0aVNlbGVjdCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuYWxsQ2hlY2tlZCkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRJdGVtcyA9IFtdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGVja2VkSXRlbXMgPSBbXTtcclxuICAgICAgZm9yIChjb25zdCBlbCBvZiB0aGlzLmN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5jaGVja2VkSXRlbXMucHVzaChlbCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMuZW1pdFNlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIC8vIGNoZWNrYm94IHNlbGVjdCBwZXIgZmlsdGVyaW5nIGNvbHVtbnNcclxuXHJcbiAgaXNDb2x1bW5DaGVja2VkRm9yRmlsdGVyaW5nKGl0ZW06IElNYWlzVGFibGVDb2x1bW4pIHtcclxuICAgIHJldHVybiB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLmluZGV4T2YoaXRlbSkgIT09IC0xO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5maWx0ZXJhYmxlQ29sdW1ucztcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLmluZGV4T2YoaXRlbSkgPT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldCBhbnlGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZCgpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IGRhdGFJdGVtcyA9IHRoaXMuZmlsdGVyYWJsZUNvbHVtbnM7XHJcbiAgICBpZiAoIWRhdGFJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGFJdGVtcykge1xyXG4gICAgICBpZiAodGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5pbmRleE9mKGl0ZW0pICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBnZXQgbm9GaWx0ZXJpbmdDb2x1bW5zaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICF0aGlzLmFueUZpbHRlcmluZ0NvbHVtbnNDaGVja2VkO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlRmlsdGVyaW5nQ29sdW1uQ2hlY2tlZChpdGVtOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudEVuYWJsZUZpbHRlcmluZykge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0aGlzLmlzRmlsdGVyYWJsZShpdGVtKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuaXNDb2x1bW5DaGVja2VkRm9yRmlsdGVyaW5nKGl0ZW0pKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcuc3BsaWNlKHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcuaW5kZXhPZihpdGVtKSwgMSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLnB1c2goaXRlbSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTZWFyY2hRdWVyeSkge1xyXG4gICAgICB0aGlzLmxvZ2dlci50cmFjZSgnc2VhcmNoIHF1ZXJ5IGNvbHVtbnMgY2hhbmdlZCB3aXRoIGFjdGl2ZSBzZWFyY2ggcXVlcnksIHJlbG9hZGluZycpO1xyXG4gICAgICB0aGlzLmFwcGx5U2VsZWN0ZWRGaWx0ZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMuZW1pdEZpbHRlcmluZ0NvbHVtbnNTZWxlY3Rpb25DaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVBbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZCgpIHtcclxuICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlRmlsdGVyaW5nKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5hbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZCkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nID0gW107XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nID0gW107XHJcbiAgICAgIGZvciAoY29uc3QgZWwgb2YgdGhpcy5maWx0ZXJhYmxlQ29sdW1ucykge1xyXG4gICAgICAgIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcucHVzaChlbCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFNlYXJjaFF1ZXJ5KSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdzZWFyY2ggcXVlcnkgY29sdW1ucyBjaGFuZ2VkIHdpdGggYWN0aXZlIHNlYXJjaCBxdWVyeSwgcmVsb2FkaW5nJyk7XHJcbiAgICAgIHRoaXMuYXBwbHlTZWxlY3RlZEZpbHRlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5lbWl0RmlsdGVyaW5nQ29sdW1uc1NlbGVjdGlvbkNoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIC8vIGNoZWNrYm94IHNlbGVjdCBwZXIgdmlzaWJsZSBjb2x1bW5zXHJcblxyXG4gIGlzQ29sdW1uQ2hlY2tlZEZvclZpc3VhbGl6YXRpb24oaXRlbTogSU1haXNUYWJsZUNvbHVtbikge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLmluZGV4T2YoaXRlbSkgIT09IC0xO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCgpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IGRhdGFJdGVtcyA9IHRoaXMuaGlkZWFibGVDb2x1bW5zO1xyXG4gICAgaWYgKCFkYXRhSXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBkYXRhSXRlbXMpIHtcclxuICAgICAgaWYgKHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uLmluZGV4T2YoaXRlbSkgPT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldCBhbnlWaXNpYmxlQ29sdW1uc0NoZWNrZWQoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBkYXRhSXRlbXMgPSB0aGlzLmhpZGVhYmxlQ29sdW1ucztcclxuICAgIGlmICghZGF0YUl0ZW1zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YUl0ZW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5pbmRleE9mKGl0ZW0pICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBnZXQgbm9WaXNpYmxlQ29sdW1uc2hlY2tlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhdGhpcy5hbnlWaXNpYmxlQ29sdW1uc0NoZWNrZWQ7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVWaXNpYmxlQ29sdW1uQ2hlY2tlZChpdGVtOiBJTWFpc1RhYmxlQ29sdW1uKSB7XHJcbiAgICBpZiAoIU1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5pc0hpZGVhYmxlKGl0ZW0sIHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpKSB8fCAhdGhpcy5jdXJyZW50RW5hYmxlQ29sdW1uc1NlbGVjdGlvbikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuaXNDb2x1bW5DaGVja2VkRm9yVmlzdWFsaXphdGlvbihpdGVtKSkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5zcGxpY2UodGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24uaW5kZXhPZihpdGVtKSwgMSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5wdXNoKGl0ZW0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5lbWl0VmlzaWJsZUNvbHVtbnNTZWxlY3Rpb25DaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVBbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWQoKSB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudEVuYWJsZUNvbHVtbnNTZWxlY3Rpb24pIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZCkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbiA9IHRoaXMuY29sdW1ucy5maWx0ZXIoYyA9PlxyXG4gICAgICAgICFNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjLCB0aGlzLmNvbmZpZ3VyYXRpb25TZXJ2aWNlLmdldENvbmZpZ3VyYXRpb24oKSkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGVja2VkQ29sdW1uc0ZvclZpc3VhbGl6YXRpb24gPSBbXTtcclxuICAgICAgZm9yIChjb25zdCBlbCBvZiB0aGlzLmNvbHVtbnMpIHtcclxuICAgICAgICB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yVmlzdWFsaXphdGlvbi5wdXNoKGVsKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhdHVzQ2hhbmdlZCgpO1xyXG4gICAgdGhpcy5lbWl0VmlzaWJsZUNvbHVtbnNTZWxlY3Rpb25DaGFuZ2VkKCk7XHJcbiAgfVxyXG5cclxuICAvLyBjaGVja2JveCBzZWxlY3QgcGVyIGZpbHRyaSBjdXN0b21cclxuXHJcbiAgaXNDb250ZXh0RmlsdGVyQ2hlY2tlZChpdGVtOiBJTWFpc1RhYmxlQ29udGV4dEZpbHRlcikge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzLmluZGV4T2YoaXRlbSkgIT09IC0xO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFsbENvbnRleHRGaWx0ZXJDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5jdXJyZW50Q29udGV4dEZpbHRlcnM7XHJcbiAgICBpZiAoIWRhdGFJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGFJdGVtcykge1xyXG4gICAgICBpZiAodGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMuaW5kZXhPZihpdGVtKSA9PT0gLTEpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGFueUNvbnRleHRGaWx0ZXJDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZGF0YUl0ZW1zID0gdGhpcy5jdXJyZW50Q29udGV4dEZpbHRlcnM7XHJcbiAgICBpZiAoIWRhdGFJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGFJdGVtcykge1xyXG4gICAgICBpZiAodGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMuaW5kZXhPZihpdGVtKSAhPT0gLTEpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG5vQ29udGV4dEZpbHRlcnNDaGVja2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICF0aGlzLmFueUNvbnRleHRGaWx0ZXJDaGVja2VkO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlQ29udGV4dEZpbHRlckNoZWNrZWQoaXRlbTogSU1haXNUYWJsZUNvbnRleHRGaWx0ZXIpIHtcclxuICAgIGlmICghdGhpcy5jdXJyZW50RW5hYmxlQ29udGV4dEZpbHRlcmluZykge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuaXNDb250ZXh0RmlsdGVyQ2hlY2tlZChpdGVtKSkge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5zcGxpY2UodGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMuaW5kZXhPZihpdGVtKSwgMSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5wdXNoKGl0ZW0pO1xyXG4gICAgICBpZiAoaXRlbS5ncm91cCkge1xyXG4gICAgICAgIHRoaXMuY2hlY2tlZENvbnRleHRGaWx0ZXJzID0gdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMuZmlsdGVyKG8gPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIChvLm5hbWUgPT09IGl0ZW0ubmFtZSkgfHwgKCFvLmdyb3VwKSB8fCAoby5ncm91cCAhPT0gaXRlbS5ncm91cCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmVtaXRDb250ZXh0RmlsdGVyc1NlbGVjdGlvbkNoYW5nZWQoKTtcclxuXHJcbiAgICB0aGlzLnNldFBhZ2UoMCk7XHJcbiAgICB0aGlzLnN0YXR1c0NoYW5nZWQoKTtcclxuICAgIHRoaXMucmVsb2FkKHtyZWFzb246IE1haXNUYWJsZVJlbG9hZFJlYXNvbi5VU0VSfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1aWxkU3RhdHVzU25hcHNob3QoKTogSU1haXNUYWJsZVN0YXR1c1NuYXBzaG90IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHNjaGVtYVZlcnNpb246IHRoaXMuY29uZmlndXJhdGlvblNlcnZpY2UuZ2V0Q29uZmlndXJhdGlvbigpLmN1cnJlbnRTY2hlbWFWZXJzaW9uIHx8ICdOT05FJyxcclxuICAgICAgb3JkZXJDb2x1bW46IHRoaXMuY3VycmVudFNvcnRDb2x1bW4gPyB0aGlzLmN1cnJlbnRTb3J0Q29sdW1uLm5hbWUgOiBudWxsLFxyXG4gICAgICBvcmRlckNvbHVtbkRpcmVjdGlvbjogdGhpcy5jdXJyZW50U29ydERpcmVjdGlvbiB8fCBudWxsLFxyXG4gICAgICBxdWVyeTogdGhpcy5jdXJyZW50U2VhcmNoUXVlcnksXHJcbiAgICAgIHF1ZXJ5Q29sdW1uczogdGhpcy5jaGVja2VkQ29sdW1uc0ZvckZpbHRlcmluZy5tYXAoYyA9PiBjLm5hbWUpLFxyXG4gICAgICB2aXNpYmxlQ29sdW1uczogdGhpcy52aXNpYmxlQ29sdW1ucy5tYXAoYyA9PiBjLm5hbWUpLFxyXG4gICAgICBjb250ZXh0RmlsdGVyczogdGhpcy5jaGVja2VkQ29udGV4dEZpbHRlcnMubWFwKGMgPT4gYy5uYW1lKSxcclxuICAgICAgY3VycmVudFBhZ2U6IHRoaXMuY3VycmVudFBhZ2VJbmRleCxcclxuICAgICAgcGFnZVNpemU6IHRoaXMuY3VycmVudFBhZ2VTaXplLFxyXG4gICAgICBjaGVja2VkSXRlbXM6ICF0aGlzLmN1cnJlbnRFbmFibGVTZWxlY3Rpb24gPyBbXSA6IHRoaXMuY2hlY2tlZEl0ZW1zLm1hcCh2ID0+IHYpLFxyXG4gICAgICBleHBhbmRlZEl0ZW1zOiAgIXRoaXMuY3VycmVudEVuYWJsZVJvd0V4cGFuc2lvbiA/IFtdIDogdGhpcy5leHBhbmRlZEl0ZW1zLm1hcCh2ID0+IHYpXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBidWlsZFBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QoKTogSU1haXNUYWJsZVBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3Qge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgc2NoZW1hVmVyc2lvbjogdGhpcy5jb25maWd1cmF0aW9uU2VydmljZS5nZXRDb25maWd1cmF0aW9uKCkuY3VycmVudFNjaGVtYVZlcnNpb24gfHwgJ05PTkUnLFxyXG4gICAgICBvcmRlckNvbHVtbjogdGhpcy5jdXJyZW50U29ydENvbHVtbiA/IHRoaXMuY3VycmVudFNvcnRDb2x1bW4ubmFtZSA6IG51bGwsXHJcbiAgICAgIG9yZGVyQ29sdW1uRGlyZWN0aW9uOiB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uIHx8IG51bGwsXHJcbiAgICAgIHF1ZXJ5OiB0aGlzLmN1cnJlbnRTZWFyY2hRdWVyeSxcclxuICAgICAgcXVlcnlDb2x1bW5zOiB0aGlzLmNoZWNrZWRDb2x1bW5zRm9yRmlsdGVyaW5nLm1hcChjID0+IGMubmFtZSksXHJcbiAgICAgIHZpc2libGVDb2x1bW5zOiB0aGlzLnZpc2libGVDb2x1bW5zLm1hcChjID0+IGMubmFtZSksXHJcbiAgICAgIGNvbnRleHRGaWx0ZXJzOiB0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycy5tYXAoYyA9PiBjLm5hbWUpLFxyXG4gICAgICBjdXJyZW50UGFnZTogdGhpcy5jdXJyZW50UGFnZUluZGV4LFxyXG4gICAgICBwYWdlU2l6ZTogdGhpcy5jdXJyZW50UGFnZVNpemUsXHJcbiAgICAgIGNoZWNrZWRJdGVtSWRlbnRpZmllcnM6ICF0aGlzLml0ZW1UcmFja2luZ0VuYWJsZWRBbmRQb3NzaWJsZSA/IFtdIDpcclxuICAgICAgICB0aGlzLmNoZWNrZWRJdGVtcy5tYXAoaXRlbSA9PiB0aGlzLmdldEl0ZW1JZGVudGlmaWVyKGl0ZW0pKS5maWx0ZXIodiA9PiAhIXYpLFxyXG4gICAgICBleHBhbmRlZEl0ZW1JZGVudGlmaWVyczogICF0aGlzLml0ZW1UcmFja2luZ0VuYWJsZWRBbmRQb3NzaWJsZSA/IFtdIDpcclxuICAgICAgICB0aGlzLmV4cGFuZGVkSXRlbXMubWFwKGl0ZW0gPT4gdGhpcy5nZXRJdGVtSWRlbnRpZmllcihpdGVtKSkuZmlsdGVyKHYgPT4gISF2KVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdHVzQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMuc3RhdHVzU25hcHNob3QgPSB0aGlzLmJ1aWxkU3RhdHVzU25hcHNob3QoKTtcclxuICAgIHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCA9IHRoaXMuYnVpbGRQZXJzaXN0YWJsZVN0YXR1c1NuYXBzaG90KCk7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgndGFibGUgc3RhdHVzIGNoYW5nZWQnLCB0aGlzLnBlcnNpc3RhYmxlU3RhdHVzU25hcHNob3QpO1xyXG5cclxuICAgIHRoaXMuZW1pdFN0YXR1c0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcGVyc2lzdFN0YXR1c1RvU3RvcmUoKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICBpZiAoIXRoaXMuc3RhdHVzU25hcHNob3QpIHtcclxuICAgICAgcmV0dXJuIHRocm93RXJyb3IoJ05vIHN0YXR1cyB0byBzYXZlJyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoIHN1YnNjcmliZXIgPT4ge1xyXG4gICAgICBpZiAodGhpcy5zdG9yZVBlcnNpc3RlbmNlRW5hYmxlZEFuZFBvc3NpYmxlKSB7XHJcbiAgICAgICAgdGhpcy5sb2dnZXIudHJhY2UoJ3Bhc3Npbmcgc3RhdHVzIHRvIHBlcnNpc3RlbmNlIHN0b3JlJyk7XHJcbiAgICAgICAgaWYgKHRoaXMuc3RvcmVBZGFwdGVyLnNhdmUpIHtcclxuICAgICAgICAgIHRoaXMuc3RvcmVBZGFwdGVyLnNhdmUoe1xyXG4gICAgICAgICAgICBzdGF0dXM6IHRoaXMucGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdFxyXG4gICAgICAgICAgfSkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdzYXZlZCBzdGF0dXMgc25hcHNob3QgdG8gcGVyc2lzdGVuY2Ugc3RvcmUnKTtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5uZXh0KCk7XHJcbiAgICAgICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0sIGZhaWx1cmUgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdmYWlsZWQgdG8gc2F2ZSBzdGF0dXMgc25hcHNob3QgdG8gcGVyc2lzdGVuY2Ugc3RvcmUnLCBmYWlsdXJlKTtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5lcnJvcihmYWlsdXJlKTtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHN1YnNjcmliZXIuZXJyb3IoJ05vIHNhdmUgZnVuY3Rpb24gaW4gc3RvcmUgYWRhcHRlcicpO1xyXG4gICAgICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBzdWJzY3JpYmVyLm5leHQoKTtcclxuICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGxvYWRTdGF0dXNGcm9tU3RvcmUoKTogT2JzZXJ2YWJsZTxJTWFpc1RhYmxlUGVyc2lzdGFibGVTdGF0dXNTbmFwc2hvdCB8IG51bGw+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSggc3Vic2NyaWJlciA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnN0b3JlUGVyc2lzdGVuY2VFbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgnZmV0Y2hpbmcgc3RhdHVzIGZyb20gcGVyc2lzdGVuY2Ugc3RvcmUnKTtcclxuICAgICAgICBpZiAodGhpcy5zdG9yZUFkYXB0ZXIubG9hZCkge1xyXG4gICAgICAgICAgdGhpcy5zdG9yZUFkYXB0ZXIubG9hZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvZ2dlci50cmFjZSgnZmV0Y2hlZCBzdGF0dXMgc25hcHNob3QgZnJvbSBwZXJzaXN0ZW5jZSBzdG9yZScpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLm5leHQocmVzdWx0KTtcclxuICAgICAgICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZmFpbHVyZSA9PiB7XHJcbiAgICAgICAgICAgIHN1YnNjcmliZXIuZXJyb3IoZmFpbHVyZSk7XHJcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ2ZhaWxlZCB0byBmZXRjaCBzdGF0dXMgc25hcHNob3QgZnJvbSBwZXJzaXN0ZW5jZSBzdG9yZScsIGZhaWx1cmUpO1xyXG4gICAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgc3Vic2NyaWJlci5lcnJvcignTm8gbG9hZCBmdW5jdGlvbiBpbiBzdG9yZSBhZGFwdGVyJyk7XHJcbiAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHN1YnNjcmliZXIubmV4dChudWxsKTtcclxuICAgICAgc3Vic2NyaWJlci5jb21wbGV0ZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXQgZHJhZ0luUHJvZ3Jlc3MoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAkKCcuY2RrLWRyYWctcHJldmlldzp2aXNpYmxlJykubGVuZ3RoICtcclxuICAgICAgJCgnLmNkay1kcmFnLXBsYWNlaG9sZGVyOnZpc2libGUnKS5sZW5ndGggK1xyXG4gICAgICAkKCcuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZzp2aXNpYmxlJykubGVuZ3RoICtcclxuICAgICAgJCgnLmNkay1kcm9wLWxpc3QtcmVjZWl2aW5nOnZpc2libGUnKS5sZW5ndGhcclxuICAgICkgPiAwO1xyXG4gIH1cclxuXHJcbiAgLy8gZXZlbnQgZW1pdHRlcnNcclxuXHJcbiAgcHJpdmF0ZSBlbWl0U2VsZWN0aW9uQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlLmVtaXQodGhpcy5jaGVja2VkSXRlbXMpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbWl0U29ydENoYW5nZWQoKSB7XHJcbiAgICB0aGlzLnNvcnRDaGFuZ2UuZW1pdCh7XHJcbiAgICAgIGNvbHVtbjogdGhpcy5jdXJyZW50U29ydENvbHVtbixcclxuICAgICAgZGlyZWN0aW9uOiB0aGlzLmN1cnJlbnRTb3J0RGlyZWN0aW9uXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW1pdFBhZ2VDaGFuZ2VkKCkge1xyXG4gICAgdGhpcy5wYWdlQ2hhbmdlLmVtaXQodGhpcy5zZWxlY3RlZFBhZ2VJbmRleCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGVtaXRGaWx0ZXJpbmdDb2x1bW5zU2VsZWN0aW9uQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMuZmlsdGVyaW5nQ29sdW1uc0NoYW5nZS5lbWl0KHRoaXMuY2hlY2tlZENvbHVtbnNGb3JGaWx0ZXJpbmcpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbWl0VmlzaWJsZUNvbHVtbnNTZWxlY3Rpb25DaGFuZ2VkKCkge1xyXG4gICAgdGhpcy52aXNpYmxlQ29sdW1uc0NoYW5nZS5lbWl0KHRoaXMuY2hlY2tlZENvbHVtbnNGb3JWaXN1YWxpemF0aW9uKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW1pdENvbnRleHRGaWx0ZXJzU2VsZWN0aW9uQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMuY29udGV4dEZpbHRlcnNDaGFuZ2UuZW1pdCh0aGlzLmNoZWNrZWRDb250ZXh0RmlsdGVycyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGVtaXRTdGF0dXNDaGFuZ2VkKCkge1xyXG4gICAgaWYgKHRoaXMuc3RhdHVzU25hcHNob3QpIHtcclxuICAgICAgdGhpcy5zdGF0dXNDaGFuZ2UuZW1pdCh0aGlzLnN0YXR1c1NuYXBzaG90KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIE1hdFRhYmxlIGFkYXB0ZXJzXHJcbiAgZ2V0IG1hdFRhYmxlRGF0YSgpOiBhbnlbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5kYXRhU25hcHNob3Q7XHJcbiAgfVxyXG5cclxuICBnZXQgbWF0VGFibGVWaXNpYmxlQ29sdW1uRGVmcygpOiBzdHJpbmdbXSB7XHJcbiAgICBjb25zdCBvID0gW107XHJcbiAgICBpZiAodGhpcy5yb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGUpIHtcclxuICAgICAgby5wdXNoKCdpbnRlcm5hbF9fX2NvbF9yb3dfZXhwYW5zaW9uJyk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jdXJyZW50RW5hYmxlU2VsZWN0aW9uKSB7XHJcbiAgICAgIG8ucHVzaCgnaW50ZXJuYWxfX19jb2xfcm93X3NlbGVjdGlvbicpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuY29udGV4dEZpbHRlcmluZ0VuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICBvLnB1c2goJ2ludGVybmFsX19fY29sX2NvbnRleHRfZmlsdGVyaW5nJyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnZpc2libGVDb2x1bW5zLm1hcChjID0+IGMubmFtZSkuZm9yRWFjaChjID0+IG8ucHVzaChjKSk7XHJcbiAgICByZXR1cm4gbztcclxuICB9XHJcblxyXG4gIGdldCBtYXRUYWJsZVZpc2libGVDb2x1bW5zKCk6IElNYWlzVGFibGVDb2x1bW5bXSB7XHJcbiAgICByZXR1cm4gdGhpcy52aXNpYmxlQ29sdW1ucztcclxuICB9XHJcblxyXG4gIGhhbmRsZU1hdFRhYmxlRGF0YVNuYXBzaG90Q2hhbmdlZCgpIHtcclxuICAgIGNvbnN0IHJvd3M6IGFueVtdID0gW107XHJcblxyXG4gICAgdGhpcy5kYXRhU25hcHNob3QuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgY29uc3QgZGV0YWlsUm93ID0geyBfX19kZXRhaWxSb3dDb250ZW50OiB0cnVlLCBfX19wYXJlbnRSb3c6IGVsZW1lbnQsIC4uLmVsZW1lbnQgfTtcclxuICAgICAgZWxlbWVudC5fX19kZXRhaWxSb3cgPSBkZXRhaWxSb3c7XHJcbiAgICAgIHJvd3MucHVzaChlbGVtZW50LCBkZXRhaWxSb3cpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gVE9ETyA6IEJVSUxEIFNUQVRJQyBST1cgV0lUSCBEQVRBIEVYVFJBQ1RPUlMhXHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnZW1pdHRpbmcgc3RhdGljIHJvd3MgZm9yIG1hdC10YWJsZSAnICsgdGhpcy5jdXJyZW50VGFibGVJZCwgcm93cyk7XHJcblxyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHRoaXMubWF0VGFibGVEYXRhT2JzZXJ2YWJsZT8ubmV4dChyb3dzKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaXNNYXRUYWJsZUV4cGFuc2lvbkRldGFpbFJvdyA9IChpOiBudW1iZXIsIHJvdzogYW55KSA9PiByb3cuaGFzT3duUHJvcGVydHkoJ19fX2RldGFpbFJvd0NvbnRlbnQnKTtcclxuXHJcbiAgaXNNYXRUYWJsZUV4cGFuZGVkID0gKHJvdzogYW55LCBsaW1pdCA9IGZhbHNlKSA9PiB7XHJcbiAgICBpZiAoIWxpbWl0ICYmIHJvdy5fX19kZXRhaWxSb3cpIHtcclxuICAgICAgaWYgKHRoaXMuaXNNYXRUYWJsZUV4cGFuZGVkKHJvdy5fX19kZXRhaWxSb3csIHRydWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmICghbGltaXQgJiYgcm93Ll9fX3BhcmVudFJvdykge1xyXG4gICAgICBpZiAodGhpcy5pc01hdFRhYmxlRXhwYW5kZWQocm93Ll9fX3BhcmVudFJvdywgdHJ1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLnJvd0V4cGFuc2lvbkVuYWJsZWRBbmRQb3NzaWJsZSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuaXNFeHBhbmRhYmxlKHJvdykpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLmlzRXhwYW5kZWQocm93KSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldCBjb21wYXRpYmlsaXR5TW9kZUZvck1haW5UYWJsZSgpOiBib29sZWFuIHtcclxuICAgIGlmICghdGhpcy5pc0lFKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50RW5hYmxlRHJvcCkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbXBhdGliaWxpdHlNb2RlRm9yRHJvcERvd25zKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRydWU7XHJcblxyXG4gICAgaWYgKCF0aGlzLmlzSUUpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iLCI8ZGl2IGNsYXNzPVwibWFpcy10YWJsZVwiPlxyXG5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZ1bGwgcHgtNCBweS01IHRhYmxlLWFjdGlvbnMgdGFibGUtYWN0aW9ucy0taGVhZGVyXCJcclxuICAgICpuZ0lmPVwiY29sdW1uU2VsZWN0aW9uUG9zc2libGVBbmRBbGxvd2VkIHx8IGZpbHRlcmluZ1Bvc3NpYmxlQW5kQWxsb3dlZCB8fCBoZWFkZXJBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlIHx8IGNvbnRleHRBY3Rpb25zRW5hYmxlZEFuZFBvc3NpYmxlXCJcclxuICA+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtNFwiICpuZ0lmPVwiZmlsdGVyaW5nUG9zc2libGVBbmRBbGxvd2VkXCI+XHJcbiAgICAgICAgPGZvcm0gKHN1Ym1pdCk9XCJhcHBseVNlbGVjdGVkRmlsdGVyKClcIj5cclxuICAgICAgICAgIDxkaXY+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5maWx0ZXJfcHJvbXB0JyB8IHRyYW5zbGF0ZSB9fTwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItcHJpbWFyeV09XCJzZWFyY2hRdWVyeUFjdGl2ZSAmJiAhc2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItd2FybmluZ109XCJzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgbmFtZT1cInNlbGVjdGVkU2VhcmNoUXVlcnlcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwie3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5maWx0ZXJfcGxhY2Vob2xkZXInIHwgdHJhbnNsYXRlIH19XCJcclxuICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiU2VhcmNoXCJcclxuICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cInNlbGVjdGVkU2VhcmNoUXVlcnlcIlxyXG4gICAgICAgICAgICAgIChmb2N1c291dCk9XCJzZWFyY2hRdWVyeUZvY3VzT3V0KClcIlxyXG4gICAgICAgICAgICA+XHJcblxyXG4gICAgICAgICAgICA8IS0tIERST1BET1dOIElOIFdFQktJVCBNT0RFIC0tPlxyXG4gICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0lmPVwiIWNvbXBhdGliaWxpdHlNb2RlRm9yRHJvcERvd25zXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cC1hcHBlbmRcIiBuZ2JEcm9wZG93biBbYXV0b0Nsb3NlXT1cIidvdXRzaWRlJ1wiPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG5cIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi1ibGFja109XCIhc2VhcmNoUXVlcnlOZWVkQXBwbHkgfHwgIXNlYXJjaFF1ZXJ5QWN0aXZlXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4tcHJpbWFyeV09XCJzZWFyY2hRdWVyeU5lZWRBcHBseSAmJiBzZWFyY2hRdWVyeUFjdGl2ZVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwic2VhcmNoUXVlcnlBY3RpdmUgJiYgIXNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItd2FybmluZ109XCJzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCIgKGNsaWNrKT1cImFwcGx5U2VsZWN0ZWRGaWx0ZXIoKVwiXHJcbiAgICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJmYXMgZmEtc2VhcmNoXCI+PC9pPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWJsYWNrXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItcHJpbWFyeV09XCJzZWFyY2hRdWVyeUFjdGl2ZSAmJiAhc2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci13YXJuaW5nXT1cInNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4tYmxhY2tdPVwiIShzZWxlY3RlZFNlYXJjaFF1ZXJ5ICYmIG5vRmlsdGVyaW5nQ29sdW1uc2hlY2tlZClcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJ0bi13YXJuaW5nXT1cInNlbGVjdGVkU2VhcmNoUXVlcnkgJiYgbm9GaWx0ZXJpbmdDb2x1bW5zaGVja2VkXCJcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIiBuZ2JEcm9wZG93blRvZ2dsZT5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duTWVudSBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfZmlsdGVyaW5nX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19PC9oNj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gY2xhc3M9XCJkcm9wZG93bi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNsaWNrYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiYWxsRmlsdGVyaW5nQ29sdW1uc0NoZWNrZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZUFsbEZpbHRlcmluZ0NvbHVtbnNDaGVja2VkKCk7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4gIChjbGljayk9XCJ0b2dnbGVBbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZCgpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMucGlja19hbGxfZmlsdGVyaW5nX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGl2aWRlclwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgY29sdW1uc1wiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gY2xhc3M9XCJkcm9wZG93bi1pdGVtXCIgW2NsYXNzLmRpc2FibGVkXT1cIiFpc0ZpbHRlcmFibGUoY29sdW1uKVwiICpuZ0lmPVwiaGFzTGFiZWwoY29sdW1uKSAmJiAoaXNGaWx0ZXJhYmxlKGNvbHVtbikgfHwgY29uZmlnRmlsdGVyaW5nU2hvd1VuZmlsdGVyYWJsZUNvbHVtbnMpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNsaWNrYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiaXNDb2x1bW5DaGVja2VkRm9yRmlsdGVyaW5nKGNvbHVtbilcIiAqbmdJZj1cImlzRmlsdGVyYWJsZShjb2x1bW4pXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGNoYW5nZSk9XCJ0b2dnbGVGaWx0ZXJpbmdDb2x1bW5DaGVja2VkKGNvbHVtbik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImZhbHNlXCIgKm5nSWY9XCIhaXNGaWx0ZXJhYmxlKGNvbHVtbilcIiBbZGlzYWJsZWRdPVwidHJ1ZVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAgKGNsaWNrKT1cInRvZ2dsZUZpbHRlcmluZ0NvbHVtbkNoZWNrZWQoY29sdW1uKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoY29sdW1uKSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gRFJPUERPV04gSU4gSUUxMSBDT01QQVRJQklMSVRZIE1PREUgLS0+XHJcbiAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCJjb21wYXRpYmlsaXR5TW9kZUZvckRyb3BEb3duc1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAtYXBwZW5kXCIgbmdiRHJvcGRvd24gW2F1dG9DbG9zZV09XCInb3V0c2lkZSdcIj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4tYmxhY2tdPVwiIXNlYXJjaFF1ZXJ5TmVlZEFwcGx5IHx8ICFzZWFyY2hRdWVyeUFjdGl2ZVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYnRuLXByaW1hcnldPVwic2VhcmNoUXVlcnlOZWVkQXBwbHkgJiYgc2VhcmNoUXVlcnlBY3RpdmVcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cInNlYXJjaFF1ZXJ5QWN0aXZlICYmICFzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXdhcm5pbmddPVwic2VhcmNoUXVlcnlNYWxmb3JtZWRcIlxyXG4gICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiIChjbGljayk9XCJhcHBseVNlbGVjdGVkRmlsdGVyKClcIlxyXG4gICAgICAgICAgICAgICAgPjxpIGNsYXNzPVwiZmFzIGZhLXNlYXJjaFwiPjwvaT5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1ibGFja1wiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYm9yZGVyLXByaW1hcnldPVwic2VhcmNoUXVlcnlBY3RpdmUgJiYgIXNlYXJjaFF1ZXJ5TWFsZm9ybWVkXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItd2FybmluZ109XCJzZWFyY2hRdWVyeU1hbGZvcm1lZFwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYnRuLWJsYWNrXT1cIiEoc2VsZWN0ZWRTZWFyY2hRdWVyeSAmJiBub0ZpbHRlcmluZ0NvbHVtbnNoZWNrZWQpXCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5idG4td2FybmluZ109XCJzZWxlY3RlZFNlYXJjaFF1ZXJ5ICYmIG5vRmlsdGVyaW5nQ29sdW1uc2hlY2tlZFwiXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCIgbmdiRHJvcGRvd25Ub2dnbGU+XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bk1lbnUgY2xhc3M9XCJkcm9wZG93bi1tZW51XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duSXRlbSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGg2IGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCI+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2ZpbHRlcmluZ19jb2x1bW5zJyB8IHRyYW5zbGF0ZSB9fTwvaDY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtIChjbGljayk9XCJ0b2dnbGVBbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZCgpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidjaGVjay1zcXVhcmUnXCIgKm5nSWY9XCJhbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZFwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ3NxdWFyZSdcIiAqbmdJZj1cIiFhbGxGaWx0ZXJpbmdDb2x1bW5zQ2hlY2tlZFwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2FsbF9maWx0ZXJpbmdfY29sdW1ucycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWRpdmlkZXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgY29sdW1uIG9mIGNvbHVtbnNcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtIFtjbGFzcy5kaXNhYmxlZF09XCIhaXNGaWx0ZXJhYmxlKGNvbHVtbilcIiAoY2xpY2spPVwidG9nZ2xlRmlsdGVyaW5nQ29sdW1uQ2hlY2tlZChjb2x1bW4pXCJcclxuICAgICAgICAgICAgICAgICAgKm5nSWY9XCJoYXNMYWJlbChjb2x1bW4pICYmIChpc0ZpbHRlcmFibGUoY29sdW1uKSB8fCBjb25maWdGaWx0ZXJpbmdTaG93VW5maWx0ZXJhYmxlQ29sdW1ucylcIj5cclxuICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ2NoZWNrLXNxdWFyZSdcIiAqbmdJZj1cImlzRmlsdGVyYWJsZShjb2x1bW4pICYmIGlzQ29sdW1uQ2hlY2tlZEZvckZpbHRlcmluZyhjb2x1bW4pXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInc3F1YXJlJ1wiICpuZ0lmPVwiaXNGaWx0ZXJhYmxlKGNvbHVtbikgJiYgIWlzQ29sdW1uQ2hlY2tlZEZvckZpbHRlcmluZyhjb2x1bW4pXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInc3F1YXJlJ1wiICpuZ0lmPVwiIWlzRmlsdGVyYWJsZShjb2x1bW4pXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoY29sdW1uKSB9fVxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbC0zXCIgKm5nSWY9XCJjb2x1bW5TZWxlY3Rpb25Qb3NzaWJsZUFuZEFsbG93ZWRcIj5cclxuICAgICAgICA8ZGl2Pnt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuY3VzdG9taXplX3ZpZXdfcHJvbXB0JyB8IHRyYW5zbGF0ZSB9fTwvZGl2PlxyXG5cclxuICAgICAgICA8IS0tIERST1BET1dOIElOIFdFQktJVCBNT0RFIC0tPlxyXG4gICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCIhY29tcGF0aWJpbGl0eU1vZGVGb3JEcm9wRG93bnNcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwXCIgbmdiRHJvcGRvd24gW2F1dG9DbG9zZV09XCInb3V0c2lkZSdcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWJsYWNrIGRyb3Bkb3duLXRvZ2dsZVwiIG5nYkRyb3Bkb3duVG9nZ2xlPlxyXG4gICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuYWRkX3JlbW92ZV9jb2x1bW5zJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bk1lbnUgY2xhc3M9XCJkcm9wZG93bi1tZW51XCI+XHJcbiAgICAgICAgICAgICAgPGg2IG5nYkRyb3Bkb3duSXRlbSBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5zZWxlY3RfY29sdW1uc19wcm9tcHQnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgPC9oNj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNsaWNrYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVja1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJhbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgKGNoYW5nZSk9XCJ0b2dnbGVBbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWQoKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gIChjbGljayk9XCJ0b2dnbGVBbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWQoKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2FsbF92aXNpYmxlX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWRpdmlkZXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgY29sdW1uc1wiPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taXRlbVwiIFtjbGFzcy5kaXNhYmxlZF09XCIhaXNIaWRlYWJsZShjb2x1bW4pXCIgKm5nSWY9XCJoYXNMYWJlbChjb2x1bW4pICYmIChpc0hpZGVhYmxlKGNvbHVtbikgfHwgY29uZmlnQ29sdW1uVmlzaWJpbGl0eVNob3dGaXhlZENvbHVtbnMpXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBjbGlja2FibGVcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiaXNDb2x1bW5DaGVja2VkRm9yVmlzdWFsaXphdGlvbihjb2x1bW4pXCIgKm5nSWY9XCJpc0hpZGVhYmxlKGNvbHVtbilcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgKGNoYW5nZSk9XCJ0b2dnbGVWaXNpYmxlQ29sdW1uQ2hlY2tlZChjb2x1bW4pOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwidHJ1ZVwiICpuZ0lmPVwiIWlzSGlkZWFibGUoY29sdW1uKVwiIFtkaXNhYmxlZF09XCJ0cnVlXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiAoY2xpY2spPVwidG9nZ2xlVmlzaWJsZUNvbHVtbkNoZWNrZWQoY29sdW1uKTsgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGNvbHVtbikgfX1cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgICA8IS0tIERST1BET1dOIElOIElFMTEgQ09NUEFUSUJJTElUWSBNT0RFIC0tPlxyXG4gICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCJjb21wYXRpYmlsaXR5TW9kZUZvckRyb3BEb3duc1wiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImJ0bi1ncm91cFwiIG5nYkRyb3Bkb3duIFthdXRvQ2xvc2VdPVwiJ291dHNpZGUnXCI+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWJsYWNrIGRyb3Bkb3duLXRvZ2dsZVwiIG5nYkRyb3Bkb3duVG9nZ2xlPlxyXG4gICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5hZGRfcmVtb3ZlX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bk1lbnUgY2xhc3M9XCJkcm9wZG93bi1tZW51XCI+XHJcbiAgICAgICAgICAgICAgICA8aDYgbmdiRHJvcGRvd25JdGVtIGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuc2VsZWN0X2NvbHVtbnNfcHJvbXB0JyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgICAgICAgPC9oNj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtIChjbGljayk9XCJ0b2dnbGVBbGxWaXNpYmxlQ29sdW1uc0NoZWNrZWQoKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInY2hlY2stc3F1YXJlJ1wiICpuZ0lmPVwiYWxsVmlzaWJsZUNvbHVtbnNDaGVja2VkXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICA8ZmEtaWNvbiBbaWNvbl09XCInc3F1YXJlJ1wiICpuZ0lmPVwiIWFsbFZpc2libGVDb2x1bW5zQ2hlY2tlZFwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgICAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5waWNrX2FsbF92aXNpYmxlX2NvbHVtbnMnIHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1kaXZpZGVyXCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiBjb2x1bW5zXCI+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSAqbmdJZj1cImhhc0xhYmVsKGNvbHVtbikgJiYgKGlzSGlkZWFibGUoY29sdW1uKSB8fCBjb25maWdDb2x1bW5WaXNpYmlsaXR5U2hvd0ZpeGVkQ29sdW1ucylcIlxyXG4gICAgICAgICAgICAgICAgICBbY2xhc3MuZGlzYWJsZWRdPVwiIWlzSGlkZWFibGUoY29sdW1uKVwiIChjbGljayk9XCJ0b2dnbGVWaXNpYmxlQ29sdW1uQ2hlY2tlZChjb2x1bW4pXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ2NoZWNrLXNxdWFyZSdcIiAqbmdJZj1cImlzSGlkZWFibGUoY29sdW1uKSAmJiBpc0NvbHVtbkNoZWNrZWRGb3JWaXN1YWxpemF0aW9uKGNvbHVtbilcIj48L2ZhLWljb24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGZhLWljb24gW2ljb25dPVwiJ3NxdWFyZSdcIiAqbmdJZj1cImlzSGlkZWFibGUoY29sdW1uKSAmJiAhaXNDb2x1bW5DaGVja2VkRm9yVmlzdWFsaXphdGlvbihjb2x1bW4pXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxmYS1pY29uIFtpY29uXT1cIidjaGVjay1zcXVhcmUnXCIgKm5nSWY9XCIhaXNIaWRlYWJsZShjb2x1bW4pXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChjb2x1bW4pIH19XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLTNcIiAqbmdJZj1cIiFjb2x1bW5TZWxlY3Rpb25Qb3NzaWJsZUFuZEFsbG93ZWRcIj5cclxuICAgICAgICA8IS0tIEZJTExFUiAtLT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtNFwiICpuZ0lmPVwiIWZpbHRlcmluZ1Bvc3NpYmxlQW5kQWxsb3dlZFwiPlxyXG4gICAgICAgIDwhLS0gRklMTEVSIC0tPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbC01IHRleHQtcmlnaHRcIj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cImFjdGlvbnNDYXB0aW9uVGVtcGxhdGVcIiBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie31cIj4gPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICZuYnNwO1xyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICA8bmctY29udGFpbmVyICpuZ0lmPVwiaGVhZGVyQWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi17eyBhY3Rpb24uZGlzcGxheUNsYXNzIHx8ICdsaWdodCcgfX0ge3sgYWN0aW9uLmFkZGl0aW9uYWxDbGFzc2VzIHx8ICcnIH19IG1yLTFcIiAqbmdGb3I9XCJsZXQgYWN0aW9uIG9mIGhlYWRlckFjdGlvbnNcIlxyXG4gICAgICAgICAgKGNsaWNrKT1cImNsaWNrT25Db250ZXh0QWN0aW9uKGFjdGlvbilcIiBbZGlzYWJsZWRdPVwiIWlzQ29udGV4dEFjdGlvbkFsbG93ZWQoYWN0aW9uKVwiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGFjdGlvbikgfX1cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImJ0bi1ncm91cFwiIG5nYkRyb3Bkb3duICpuZ0lmPVwiY29udGV4dEFjdGlvbnNFbmFibGVkQW5kUG9zc2libGVcIj5cclxuICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWxpZ2h0IGRyb3Bkb3duLXRvZ2dsZVwiIFtkaXNhYmxlZF09XCIhYW55QnV0dG9uQWN0aW9uc0FsbG93ZWRcIiBuZ2JEcm9wZG93blRvZ2dsZT57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmFjdGlvbnNfYnV0dG9uJyB8IHRyYW5zbGF0ZSB9fTwvYnV0dG9uPlxyXG4gICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bk1lbnU+XHJcbiAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25JdGVtPlxyXG4gICAgICAgICAgICAgIDxoNiBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiPnt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMucGlja19hY3Rpb24nIHwgdHJhbnNsYXRlIH19PC9oNj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtICpuZ0Zvcj1cImxldCBhY3Rpb24gb2YgY3VycmVudEFjdGlvbnNcIiAoY2xpY2spPVwiY2xpY2tPbkNvbnRleHRBY3Rpb24oYWN0aW9uKVwiIFtkaXNhYmxlZF09XCIhaXNDb250ZXh0QWN0aW9uQWxsb3dlZChhY3Rpb24pXCI+XHJcbiAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGFjdGlvbikgfX1cclxuICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuXHJcbiAgPCEtLSBNQUlOIFRBQkxFIC0gV0VCS0lUIE1PREUgLS0+XHJcbiAgPG5nLWNvbnRhaW5lciAqbmdJZj1cIiFjb21wYXRpYmlsaXR5TW9kZUZvck1haW5UYWJsZVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJ0YWJsZS1yZXNwb25zaXZlXCI+XHJcbiAgICA8dGFibGUgY2xhc3M9XCJ0YWJsZSB0YWJsZS1kYXJrIHRhYmxlLWhvdmVyXCJcclxuICAgICAgY2RrRHJvcExpc3RcclxuICAgICAgaWQ9XCJ7e2N1cnJlbnRUYWJsZUlkfX1cIlxyXG4gICAgICBbY2RrRHJvcExpc3RDb25uZWN0ZWRUb109XCJkcm9wQ29ubmVjdGVkVG9cIlxyXG4gICAgICAoY2RrRHJvcExpc3REcm9wcGVkKT1cImhhbmRsZUl0ZW1Ecm9wcGVkKCRldmVudClcIlxyXG4gICAgICBbY2RrRHJvcExpc3REYXRhXT1cImN1cnJlbnREYXRhXCJcclxuICAgICAgW2Nka0Ryb3BMaXN0RW50ZXJQcmVkaWNhdGVdPVwiYWNjZXB0RHJvcFByZWRpY2F0ZVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdFNvcnRpbmdEaXNhYmxlZF09XCIhYWNjZXB0RHJvcFwiXHJcbiAgICAgIFtjbGFzcy5ub2Ryb3BdPVwiIWFjY2VwdERyb3BcIlxyXG4gICAgICBbY2xhc3MuYWNjZXB0ZHJvcF09XCJhY2NlcHREcm9wXCJcclxuICAgID5cclxuICAgICAgPGNhcHRpb24gY2xhc3M9XCJkLW5vbmVcIj57eyAndGFibGUuY29tbW9uLmFjY2Vzc2liaWxpdHkuY2FwdGlvbicgfCB0cmFuc2xhdGUgfX08L2NhcHRpb24+XHJcbiAgICAgIDx0aGVhZCBjbGFzcz1cIlwiPlxyXG4gICAgICAgIDx0cj5cclxuICAgICAgICAgIDx0aCBzY29wZT1cInJvd1wiICpuZ0lmPVwicm93RXhwYW5zaW9uRW5hYmxlZEFuZFBvc3NpYmxlXCIgY2xhc3M9XCJzbWFsbC1hcy1wb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICA8IS0tIGVtcHR5IGhlYWRlciBmb3Igcm93IGV4cGFuc2lvbiAtLT5cclxuICAgICAgICAgIDwvdGg+XHJcbiAgICAgICAgICA8dGggc2NvcGU9XCJyb3dcIiAqbmdJZj1cImN1cnJlbnRFbmFibGVTZWxlY3Rpb25cIiBjbGFzcz1cInNtYWxsLWFzLXBvc3NpYmxlXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJjdXJyZW50RW5hYmxlU2VsZWN0QWxsICYmIGN1cnJlbnRFbmFibGVNdWx0aVNlbGVjdCAmJiAhbm9SZXN1bHRzXCI+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiYWxsQ2hlY2tlZFwiIChjaGFuZ2UpPVwidG9nZ2xlQWxsQ2hlY2tlZCgpXCIgLz5cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDx0aCBzY29wZT1cInJvd1wiICpuZ0lmPVwiY29udGV4dEZpbHRlcmluZ0VuYWJsZWRBbmRQb3NzaWJsZVwiIHN0eWxlPVwid2lkdGg6IDElO1wiXHJcbiAgICAgICAgICAgIFtjbGFzcy5ib3JkZXItcHJpbWFyeV09XCJhbnlDb250ZXh0RmlsdGVyQ2hlY2tlZFwiIG5nYkRyb3Bkb3duIFthdXRvQ2xvc2VdPVwiJ291dHNpZGUnXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2sgY2xpY2thYmxlXCIgbmdiRHJvcGRvd25Ub2dnbGU+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24gc2hvdyBkLWlubGluZS1ibG9jayBmbG9hdC1yaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1tZW51XCIgbmdiRHJvcGRvd25NZW51PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg2IGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCIgbmdiRHJvcGRvd25JdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImNvbnRleHRGaWx0ZXJpbmdQcm9tcHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB7eyBjb250ZXh0RmlsdGVyaW5nUHJvbXB0IHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCIhY29udGV4dEZpbHRlcmluZ1Byb21wdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuY29udGV4dF9maWx0ZXJfcHJvbXB0JyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2g2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBuZ2JEcm9wZG93bkl0ZW0gKm5nRm9yPVwibGV0IGZpbHRlciBvZiBjdXJyZW50Q29udGV4dEZpbHRlcnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIFtjaGVja2VkXT1cImlzQ29udGV4dEZpbHRlckNoZWNrZWQoZmlsdGVyKVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKGNoYW5nZSk9XCJ0b2dnbGVDb250ZXh0RmlsdGVyQ2hlY2tlZChmaWx0ZXIpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImNsaWNrYWJsZVwiIChjbGljayk9XCJ0b2dnbGVDb250ZXh0RmlsdGVyQ2hlY2tlZChmaWx0ZXIpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAge3sgcmVzb2x2ZUxhYmVsKGZpbHRlcikgfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiB2aXNpYmxlQ29sdW1uc1wiPlxyXG4gICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5vcmRlci11cF09XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbiwgJ0FTQycpXCJcclxuICAgICAgICAgICAgICBbY2xhc3Mub3JkZXItZG93bl09XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbiwgJ0RFU0MnKVwiXHJcbiAgICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uKVwiXHJcbiAgICAgICAgICAgICAgW2NsYXNzLmNsaWNrYWJsZV09XCJpc1NvcnRhYmxlKGNvbHVtbilcIlxyXG4gICAgICAgICAgICAgIChjbGljayk9XCJjbGlja09uQ29sdW1uKGNvbHVtbilcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0N1cnJlbnRTb3J0aW5nQ29sdW1uKGNvbHVtbilcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdBU0MnKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cIm9yZGVyLWJ0biBidG4gYnRuLW5vbmVcIiAoY2xpY2spPVwiY2xpY2tPbkNvbHVtbihjb2x1bW4pXCI+PGkgY2xhc3M9XCJmYXMgZmEtbG9uZy1hcnJvdy1hbHQtdXBcIj48L2k+PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uLCAnREVTQycpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwib3JkZXItYnRuIGJ0biBidG4tbm9uZVwiIChjbGljayk9XCJjbGlja09uQ29sdW1uKGNvbHVtbilcIj48aSBjbGFzcz1cImZhcyBmYS1sb25nLWFycm93LWFsdC11cFwiPjwvaT48L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0xhYmVsVmlzaWJsZUluVGFibGUoY29sdW1uKSAmJiBoYXNMYWJlbChjb2x1bW4pXCI+XHJcbiAgICAgICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoY29sdW1uKSB9fVxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgIDwvdGhlYWQ+XHJcbiAgICAgIDx0Ym9keVxyXG4gICAgICAgICpuZ0lmPVwiIXNob3dGZXRjaGluZyAmJiAhZm9yY2VSZVJlbmRlclwiXHJcbiAgICAgID5cclxuICAgICAgICA8dHIgY2xhc3M9XCJzdGFydC1yb3ctaG9vayBkLW5vbmVcIj48L3RyPlxyXG4gICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IHJvdyBvZiBjdXJyZW50RGF0YVwiPlxyXG4gICAgICAgIDx0clxyXG4gICAgICAgICAgW2NsYXNzLnJvdy1zZWxlY3RlZF09XCJpc0NoZWNrZWQocm93KVwiXHJcbiAgICAgICAgICBjbGFzcz1cImRhdGEtcm93XCJcclxuICAgICAgICAgIGNka0RyYWdcclxuICAgICAgICAgIFtjZGtEcmFnRGF0YV09XCJyb3dcIlxyXG4gICAgICAgICAgW2Nka0RyYWdEaXNhYmxlZF09XCIhYWNjZXB0RHJhZ1wiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPHRoIHNjb3BlPVwicm93XCIgKm5nSWY9XCJyb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGVcIj5cclxuICAgICAgICAgICAgPGZhLWljb24gY2xhc3M9XCJjbGlja2FibGVcIiBbaWNvbl09XCInY2FyZXQtc3F1YXJlLWRvd24nXCIgKm5nSWY9XCIhaXNFeHBhbmRlZChyb3cpICYmIGlzRXhwYW5kYWJsZShyb3cpXCIgKGNsaWNrKT1cImNsaWNrT25Sb3dFeHBhbnNpb24ocm93KVwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgICAgPGZhLWljb24gY2xhc3M9XCJjbGlja2FibGVcIiBbaWNvbl09XCInY2FyZXQtc3F1YXJlLXVwJ1wiICpuZ0lmPVwiaXNFeHBhbmRlZChyb3cpXCIgKGNsaWNrKT1cImNsaWNrT25Sb3dFeHBhbnNpb24ocm93KVwiPjwvZmEtaWNvbj5cclxuICAgICAgICAgIDwvdGg+XHJcbiAgICAgICAgICA8dGggc2NvcGU9XCJyb3dcIiAqbmdJZj1cImN1cnJlbnRFbmFibGVTZWxlY3Rpb25cIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgW2NoZWNrZWRdPVwiaXNDaGVja2VkKHJvdylcIiAoY2hhbmdlKT1cInRvZ2dsZUNoZWNrZWQocm93KVwiIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90aD5cclxuICAgICAgICAgIDx0ZCAqbmdJZj1cImNvbnRleHRGaWx0ZXJpbmdFbmFibGVkQW5kUG9zc2libGVcIj5cclxuICAgICAgICAgICAgPCEtLSBlbXB0eSBjZWxsIGZvciBjb250ZXh0IGZpbHRlcmluZyAtLT5cclxuICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgdmlzaWJsZUNvbHVtbnNcIj5cclxuICAgICAgICAgIDx0ZD5cclxuICAgICAgICAgICAgPGRpdiAqbmdJZj1cImNvbHVtbi5hcHBseVRlbXBsYXRlXCI+XHJcbiAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0XT1cImNlbGxUZW1wbGF0ZVwiXHJcbiAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7IHJvdzogcm93LCBjb2x1bW46IGNvbHVtbiwgdmFsdWU6IGV4dHJhY3RWYWx1ZShyb3csIGNvbHVtbikgfVwiPlxyXG4gICAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiIWNvbHVtbi5hcHBseVRlbXBsYXRlXCI+XHJcbiAgICAgICAgICAgICAge3sgZXh0cmFjdFZhbHVlKHJvdywgY29sdW1uKSB9fVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgIDx0ZCBjbGFzcz1cImQtbm9uZVwiPlxyXG4gICAgICAgICAgICA8dHIgKmNka0RyYWdQbGFjZWhvbGRlciBjbGFzcz1cImRyb3AtY29udGFpbmVyXCIgW2hpZGRlbl09XCIhcmVmZXJlbmNlZFRhYmxlLmFjY2VwdERyb3BcIj5cclxuICAgICAgICAgICAgICA8dGQ+PC90ZD5cclxuICAgICAgICAgICAgICA8dGQgW2F0dHIuY29sc3Bhbl09XCJhY3RpdmVDb2x1bW5zQ291bnQgLSAxXCIgY2xhc3M9XCJkcm9wLWNlbGxcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICA8bmctdGVtcGxhdGVcclxuICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInJlZmVyZW5jZWRUYWJsZS5kcm9wVGVtcGxhdGVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiByb3cgfVwiPlxyXG4gICAgICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICA8dGQgY2xhc3M9XCJkLW5vbmVcIj5cclxuICAgICAgICAgICAgPGRpdiAqY2RrRHJhZ1ByZXZpZXcgY2xhc3M9XCJkcmFnLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZVxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwiZHJhZ1RlbXBsYXRlXCJcclxuICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7IHJvdzogcm93IH1cIj5cclxuICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgPC90cj5cclxuICAgICAgICA8dHIgKm5nSWY9XCJyb3dFeHBhbnNpb25FbmFibGVkQW5kUG9zc2libGUgJiYgaXNFeHBhbmRlZChyb3cpXCIgY2xhc3M9XCJkZXRhaWwtcm93XCI+XHJcbiAgICAgICAgICA8dGQ+PC90ZD5cclxuICAgICAgICAgIDx0ZCBbYXR0ci5jb2xzcGFuXT1cImFjdGl2ZUNvbHVtbnNDb3VudCAtIDFcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdy1leHBhbnNpb24tY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInJvd0RldGFpbFRlbXBsYXRlXCJcclxuICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiByb3cgfVwiPlxyXG4gICAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICA8L3RyPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICA8L3Rib2R5PlxyXG4gICAgICA8bmctY29udGFpbmVyICpuZ0lmPVwiIXNob3dGZXRjaGluZyAmJiBub1Jlc3VsdHNcIj5cclxuICAgICAgPHRib2R5PlxyXG4gICAgICAgIDx0ciBjbGFzcz1cIm1lc3NhZ2Utcm93IG5vLXJlc3VsdHMtcm93XCI+XHJcbiAgICAgICAgICA8dGQgW2F0dHIuY29sc3Bhbl09XCJhY3RpdmVDb2x1bW5zQ291bnRcIiBjbGFzcz1cInRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMubm9fcmVzdWx0cycgfCB0cmFuc2xhdGUgfX1cclxuICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgPC90cj5cclxuICAgICAgPC90Ym9keT5cclxuICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCJzaG93RmV0Y2hpbmdcIj5cclxuICAgICAgPHRib2R5PlxyXG4gICAgICAgIDx0ciBjbGFzcz1cIm1lc3NhZ2Utcm93IGZldGNoaW5nLXJvd1wiPlxyXG4gICAgICAgICAgPHRkIFthdHRyLmNvbHNwYW5dPVwiYWN0aXZlQ29sdW1uc0NvdW50XCIgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmZldGNoaW5nJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgPC90ZD5cclxuICAgICAgICA8L3RyPlxyXG4gICAgICA8L3Rib2R5PlxyXG4gICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgIDwvdGFibGU+XHJcbiAgPC9kaXY+XHJcbiAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gIDwhLS0gTUFJTiBUQUJMRSAtIElFMTEgQ09NUEFUSUJJTElUWSBNT0RFIC0tPlxyXG4gIDxuZy1jb250YWluZXIgKm5nSWY9XCJjb21wYXRpYmlsaXR5TW9kZUZvck1haW5UYWJsZVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJ0YWJsZS1yZXNwb25zaXZlXCIgKm5nSWY9XCJtYXRUYWJsZURhdGFPYnNlcnZhYmxlXCI+XHJcblxyXG4gICAgPG1hdC10YWJsZVxyXG4gICAgICAjcmVuZGVyZWRNYXRUYWJsZVxyXG4gICAgICBbZGF0YVNvdXJjZV09XCJtYXRUYWJsZURhdGFPYnNlcnZhYmxlXCJcclxuICAgICAgY2xhc3M9XCJ0YWJsZSB0YWJsZS1kYXJrIHRhYmxlLWhvdmVyXCJcclxuICAgICAgY2RrRHJvcExpc3RcclxuICAgICAgaWQ9XCJ7e2N1cnJlbnRUYWJsZUlkfX1cIlxyXG4gICAgICBbY2RrRHJvcExpc3RDb25uZWN0ZWRUb109XCJkcm9wQ29ubmVjdGVkVG9cIlxyXG4gICAgICAoY2RrRHJvcExpc3REcm9wcGVkKT1cImhhbmRsZUl0ZW1Ecm9wcGVkKCRldmVudClcIlxyXG4gICAgICBbY2RrRHJvcExpc3REYXRhXT1cImN1cnJlbnREYXRhXCJcclxuICAgICAgW2Nka0Ryb3BMaXN0RW50ZXJQcmVkaWNhdGVdPVwiYWNjZXB0RHJvcFByZWRpY2F0ZVwiXHJcbiAgICAgIFtjZGtEcm9wTGlzdFNvcnRpbmdEaXNhYmxlZF09XCIhYWNjZXB0RHJvcFwiXHJcbiAgICAgIFtjbGFzcy5ub2Ryb3BdPVwiIWFjY2VwdERyb3BcIlxyXG4gICAgICBbY2xhc3MuYWNjZXB0ZHJvcF09XCJhY2NlcHREcm9wXCJcclxuICAgICAgKm5nSWY9XCIhc2hvd0ZldGNoaW5nICYmICFmb3JjZVJlUmVuZGVyXCJcclxuICAgID5cclxuICAgICAgPCEtLSBVc2VyIG5hbWUgRGVmaW5pdGlvbiAtLT5cclxuICAgICAgPG5nLWNvbnRhaW5lciBbbWF0Q29sdW1uRGVmXT1cIidpbnRlcm5hbF9fX2NvbF9yb3dfZXhwYW5zaW9uJ1wiPlxyXG4gICAgICAgIDxtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWYgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDwhLS0gRU1QVFkgLS0+XHJcbiAgICAgICAgPC9tYXQtaGVhZGVyLWNlbGw+XHJcbiAgICAgICAgPG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiIGNsYXNzPVwibWFpc3NpemUtbWluXCI+XHJcbiAgICAgICAgICA8ZmEtaWNvbiBjbGFzcz1cImNsaWNrYWJsZVwiIFtpY29uXT1cIidjYXJldC1zcXVhcmUtZG93bidcIiAqbmdJZj1cIiFpc0V4cGFuZGVkKHJvdykgJiYgaXNFeHBhbmRhYmxlKHJvdylcIiAoY2xpY2spPVwiY2xpY2tPblJvd0V4cGFuc2lvbihyb3cpXCI+PC9mYS1pY29uPlxyXG4gICAgICAgICAgPGZhLWljb24gY2xhc3M9XCJjbGlja2FibGVcIiBbaWNvbl09XCInY2FyZXQtc3F1YXJlLXVwJ1wiICpuZ0lmPVwiaXNFeHBhbmRlZChyb3cpXCIgKGNsaWNrKT1cImNsaWNrT25Sb3dFeHBhbnNpb24ocm93KVwiPjwvZmEtaWNvbj5cclxuICAgICAgICA8L21hdC1jZWxsPlxyXG4gICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgPG5nLWNvbnRhaW5lciBbbWF0Q29sdW1uRGVmXT1cIidpbnRlcm5hbF9fX2NvbF9yb3dfc2VsZWN0aW9uJ1wiPlxyXG4gICAgICAgIDxtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWYgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiY3VycmVudEVuYWJsZVNlbGVjdEFsbCAmJiBjdXJyZW50RW5hYmxlTXVsdGlTZWxlY3QgJiYgIW5vUmVzdWx0c1wiPlxyXG4gICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJhbGxDaGVja2VkXCIgKGNoYW5nZSk9XCJ0b2dnbGVBbGxDaGVja2VkKClcIiAvPlxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21hdC1oZWFkZXItY2VsbD5cclxuICAgICAgICA8bWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCIgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrXCI+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJpc0NoZWNrZWQocm93KVwiIChjaGFuZ2UpPVwidG9nZ2xlQ2hlY2tlZChyb3cpXCIgLz5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWNlbGw+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICA8bmctY29udGFpbmVyIFttYXRDb2x1bW5EZWZdPVwiJ2ludGVybmFsX19fY29sX2NvbnRleHRfZmlsdGVyaW5nJ1wiPlxyXG4gICAgICAgIDxtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWYgY2xhc3M9XCJtYWlzc2l6ZS1taW5cIj5cclxuICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cImFueUNvbnRleHRGaWx0ZXJDaGVja2VkXCJcclxuICAgICAgICAgICAgbmdiRHJvcGRvd25cclxuICAgICAgICAgICAgW2F1dG9DbG9zZV09XCInb3V0c2lkZSdcIlxyXG4gICAgICAgICAgICBbY29udGFpbmVyXT1cIidib2R5J1wiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrIGNsaWNrYWJsZVwiIG5nYkRyb3Bkb3duVG9nZ2xlPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93biBzaG93IGQtaW5saW5lLWJsb2NrIGZsb2F0LXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1tZW51XCIgbmdiRHJvcGRvd25NZW51PlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNiBjbGFzcz1cImRyb3Bkb3duLWhlYWRlclwiIG5nYkRyb3Bkb3duSXRlbT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiY29udGV4dEZpbHRlcmluZ1Byb21wdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7eyBjb250ZXh0RmlsdGVyaW5nUHJvbXB0IHwgdHJhbnNsYXRlIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cIiFjb250ZXh0RmlsdGVyaW5nUHJvbXB0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7ICd0YWJsZS5jb21tb24ubWVzc2FnZXMuY29udGV4dF9maWx0ZXJfcHJvbXB0JyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvaDY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBuZ2JEcm9wZG93bkl0ZW0gKm5nRm9yPVwibGV0IGZpbHRlciBvZiBjdXJyZW50Q29udGV4dEZpbHRlcnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJpc0NvbnRleHRGaWx0ZXJDaGVja2VkKGZpbHRlcilcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRvZ2dsZUNvbnRleHRGaWx0ZXJDaGVja2VkKGZpbHRlcik7ICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImNsaWNrYWJsZVwiIChjbGljayk9XCJ0b2dnbGVDb250ZXh0RmlsdGVyQ2hlY2tlZChmaWx0ZXIpOyAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChmaWx0ZXIpIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9tYXQtaGVhZGVyLWNlbGw+XHJcbiAgICAgICAgPG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiIGNsYXNzPVwibWFpc3NpemUtbWluXCI+XHJcbiAgICAgICAgICA8IS0tIEVNUFRZIC0tPlxyXG4gICAgICAgIDwvbWF0LWNlbGw+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICA8bmctY29udGFpbmVyIFttYXRDb2x1bW5EZWZdPVwiJ2ludGVybmFsX19fY29sX3Jvd19kZXRhaWwnXCI+XHJcbiAgICAgICAgPG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IGRldGFpbFwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cInJvdy1leHBhbnNpb24tY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgIDxuZy10ZW1wbGF0ZVxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwicm93RGV0YWlsVGVtcGxhdGVcIlxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInsgcm93OiBkZXRhaWwuX19fcGFyZW50Um93IH1cIj5cclxuICAgICAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWNlbGw+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICA8bmctY29udGFpbmVyIFttYXRDb2x1bW5EZWZdPVwiY29sdW1uLm5hbWVcIiAqbmdGb3I9XCJsZXQgY29sdW1uIG9mIG1hdFRhYmxlVmlzaWJsZUNvbHVtbnNcIj5cclxuICAgICAgICA8bWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmIGNsYXNzPVwibWFpc3NpemUte3sgY29sdW1uLnNpemUgfHwgJ2RlZmF1bHQnIH19XCIgPlxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzcz1cInt7IGNvbHVtbi5oZWFkZXJEaXNwbGF5Q2xhc3MgfHwgJycgfX1cIlxyXG4gICAgICAgICAgICBbY2xhc3Mub3JkZXItdXBdPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdBU0MnKVwiXHJcbiAgICAgICAgICAgIFtjbGFzcy5vcmRlci1kb3duXT1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uLCAnREVTQycpXCJcclxuICAgICAgICAgICAgW2NsYXNzLmJvcmRlci1wcmltYXJ5XT1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uKVwiXHJcbiAgICAgICAgICAgIFtjbGFzcy5jbGlja2FibGVdPVwiaXNTb3J0YWJsZShjb2x1bW4pXCJcclxuICAgICAgICAgICAgKGNsaWNrKT1cImNsaWNrT25Db2x1bW4oY29sdW1uKVwiPlxyXG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uKVwiPlxyXG4gICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNDdXJyZW50U29ydGluZ0NvbHVtbihjb2x1bW4sICdBU0MnKVwiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJvcmRlci1idG4gYnRuIGJ0bi1ub25lXCIgKGNsaWNrKT1cImNsaWNrT25Db2x1bW4oY29sdW1uKVwiPjxpIGNsYXNzPVwiZmFzIGZhLWxvbmctYXJyb3ctYWx0LXVwXCI+PC9pPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzQ3VycmVudFNvcnRpbmdDb2x1bW4oY29sdW1uLCAnREVTQycpXCI+XHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cIm9yZGVyLWJ0biBidG4gYnRuLW5vbmVcIiAoY2xpY2spPVwiY2xpY2tPbkNvbHVtbihjb2x1bW4pXCI+PGkgY2xhc3M9XCJmYXMgZmEtbG9uZy1hcnJvdy1hbHQtdXBcIj48L2k+PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNMYWJlbFZpc2libGVJblRhYmxlKGNvbHVtbikgJiYgaGFzTGFiZWwoY29sdW1uKVwiPlxyXG4gICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChjb2x1bW4pIH19XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWhlYWRlci1jZWxsPlxyXG4gICAgICAgIDxtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIiBjbGFzcz1cIm1haXNzaXplLXt7IGNvbHVtbi5zaXplIHx8ICdkZWZhdWx0JyB9fSB7eyBjb2x1bW4uY2VsbERpc3BsYXlDbGFzcyB8fCAnJyB9fVwiPlxyXG4gICAgICAgICAgPGRpdiAqbmdJZj1cImNvbHVtbi5hcHBseVRlbXBsYXRlXCI+XHJcbiAgICAgICAgICAgIDxuZy10ZW1wbGF0ZVxyXG4gICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRdPVwiY2VsbFRlbXBsYXRlXCJcclxuICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7IHJvdzogcm93LCBjb2x1bW46IGNvbHVtbiwgdmFsdWU6IGV4dHJhY3RWYWx1ZShyb3csIGNvbHVtbikgfVwiPlxyXG4gICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2ICpuZ0lmPVwiIWNvbHVtbi5hcHBseVRlbXBsYXRlXCI+XHJcbiAgICAgICAgICAgIHt7IGV4dHJhY3RWYWx1ZShyb3csIGNvbHVtbikgfX1cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWNlbGw+XHJcbiAgICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgICAgPCEtLSBIZWFkZXIgYW5kIFJvdyBEZWNsYXJhdGlvbnMgLS0+XHJcbiAgICAgIDxtYXQtaGVhZGVyLXJvdyAqbWF0SGVhZGVyUm93RGVmPVwibWF0VGFibGVWaXNpYmxlQ29sdW1uRGVmc1wiPjwvbWF0LWhlYWRlci1yb3c+XHJcbiAgICAgIDxtYXQtcm93ICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiBtYXRUYWJsZVZpc2libGVDb2x1bW5EZWZzO1wiXHJcbiAgICAgICAgY2xhc3M9XCJlbGVtZW50LXJvdyBkYXRhLXJvd1wiXHJcbiAgICAgICAgW2NsYXNzLmV4cGFuZGVkXT1cImlzTWF0VGFibGVFeHBhbmRlZChyb3cpXCJcclxuICAgICAgICBbY2xhc3Mucm93LXNlbGVjdGVkXT1cImlzQ2hlY2tlZChyb3cpXCJcclxuICAgICAgICBjZGtEcmFnXHJcbiAgICAgICAgW2Nka0RyYWdEYXRhXT1cInJvd1wiXHJcbiAgICAgICAgW2Nka0RyYWdEaXNhYmxlZF09XCIhYWNjZXB0RHJhZ1wiXHJcbiAgICAgID48L21hdC1yb3c+XHJcbiAgICAgIDxtYXQtcm93ICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiBbJ2ludGVybmFsX19fY29sX3Jvd19kZXRhaWwnXTsgd2hlbjogaXNNYXRUYWJsZUV4cGFuc2lvbkRldGFpbFJvd1wiXHJcbiAgICAgICAgICBbQGRldGFpbEV4cGFuZF09XCJpc01hdFRhYmxlRXhwYW5kZWQocm93KSA/ICdleHBhbmRlZCcgOiAnY29sbGFwc2VkJ1wiXHJcbiAgICAgICAgICBzdHlsZT1cIm92ZXJmbG93OiBoaWRkZW5cIj5cclxuICAgICAgPC9tYXQtcm93PlxyXG4gICAgPC9tYXQtdGFibGU+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cIiFzaG93RmV0Y2hpbmcgJiYgbm9SZXN1bHRzXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibWVzc2FnZS1yb3cgbm8tcmVzdWx0cy1yb3dcIj5cclxuICAgICAgPHAgY2xhc3M9XCJhbGVydCBhbGVydC1wcmltYXJ5IHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAge3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5ub19yZXN1bHRzJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICA8L3A+XHJcbiAgICA8L2Rpdj5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cInNob3dGZXRjaGluZ1wiPlxyXG4gICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2Utcm93IGZldGNoaW5nLXJvd1wiPlxyXG4gICAgICA8cCBjbGFzcz1cImFsZXJ0IGFsZXJ0LXByaW1hcnkgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICB7eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLmZldGNoaW5nJyB8IHRyYW5zbGF0ZSB9fVxyXG4gICAgICA8L3A+XHJcbiAgICA8L2Rpdj5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICA8L2Rpdj5cclxuICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgPGRpdiBjbGFzcz1cImNvbnRhaW5lci1mdWxsIHB4LTQgcHktNCB0YWJsZS1hY3Rpb25zIHRhYmxlLWFjdGlvbnMtLWZvb3RlclwiXHJcbiAgICAqbmdJZj1cImN1cnJlbnRFbmFibGVQYWdpbmF0aW9uIHx8IGhlYWRlckFjdGlvbnNFbmFibGVkQW5kUG9zc2libGUgfHwgY29udGV4dEFjdGlvbnNFbmFibGVkQW5kUG9zc2libGVcIlxyXG4gID5cclxuICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTdcIj5cclxuICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nSWY9XCJjdXJyZW50UmVzdWx0TnVtYmVyID4gMCAmJiBjdXJyZW50RW5hYmxlUGFnaW5hdGlvblwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZS1ibG9jayBpbmxpbmUtYmxvY2stYm90dG9tXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwYWdpbmF0aW9uLWNhcHRpb25cIj5cclxuICAgICAgICAgICAgICB7eyAndGFibGUuY29tbW9uLnBhZ2luYXRpb24udG90YWxfZWxlbWVudHMnIHwgdHJhbnNsYXRlOnsgdG90YWxFbGVtZW50czogY3VycmVudFJlc3VsdE51bWJlciwgdG90YWxQYWdlczogY3VycmVudFBhZ2VDb3VudCB9IH19XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8bmF2IGFyaWEtbGFiZWw9XCJwYWdpbmF0aW9uXCIgY2xhc3M9XCJwYWdpbmF0aW9uLWxpbmtzXCI+XHJcbiAgICAgICAgICAgICAgPHVsIGNsYXNzPVwicGFnaW5hdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtIGNsaWNrYWJsZVwiIFtjbGFzcy5kaXNhYmxlZF09XCIhKGN1cnJlbnRQYWdlSW5kZXggPiAwKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiIChjbGljayk9XCJzd2l0Y2hUb1BhZ2UoMClcIj57eyAndGFibGUuY29tbW9uLnBhZ2luYXRpb24uZmlyc3RfcGFnZScgfCB0cmFuc2xhdGUgfX08L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtIGNsaWNrYWJsZVwiIFtjbGFzcy5kaXNhYmxlZF09XCIhKGN1cnJlbnRQYWdlSW5kZXggPiAwKVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInBhZ2UtbGlua1wiIChjbGljayk9XCJzd2l0Y2hUb1BhZ2UoY3VycmVudFBhZ2VJbmRleCAtIDEpXCI+e3sgJ3RhYmxlLmNvbW1vbi5wYWdpbmF0aW9uLnByZXZpb3VzX3BhZ2UnIHwgdHJhbnNsYXRlIH19PC9hPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IHBhZ2Ugb2YgZW51bVBhZ2VzXCI+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJwYWdlLWl0ZW0gY2xpY2thYmxlXCIgW2NsYXNzLmFjdGl2ZV09XCJwYWdlID09PSBjdXJyZW50UGFnZUluZGV4XCIgKm5nSWY9XCIhcGFnZS5za2lwXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwicGFnZS1saW5rXCIgKGNsaWNrKT1cInN3aXRjaFRvUGFnZShwYWdlKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt7IHBhZ2UgKyAxIH19XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJzci1vbmx5XCIgKm5nSWY9XCJwYWdlID09PSBjdXJyZW50UGFnZUluZGV4XCI+KGN1cnJlbnQpPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtXCIgKm5nSWY9XCJwYWdlLnNraXBcIj5cclxuICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIj5cclxuICAgICAgICAgICAgICAgICAgICAuLi5cclxuICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwicGFnZS1pdGVtIGNsaWNrYWJsZVwiIFtjbGFzcy5kaXNhYmxlZF09XCJjdXJyZW50UGFnZUluZGV4ID49IChjdXJyZW50UGFnZUNvdW50IC0gMSlcIj5cclxuICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJwYWdlLWxpbmtcIiAoY2xpY2spPVwic3dpdGNoVG9QYWdlKGN1cnJlbnRQYWdlSW5kZXggKyAxKVwiPnt7ICd0YWJsZS5jb21tb24ucGFnaW5hdGlvbi5uZXh0X3BhZ2UnIHwgdHJhbnNsYXRlIH19PC9hPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cInBhZ2UtaXRlbSBjbGlja2FibGVcIiBbY2xhc3MuZGlzYWJsZWRdPVwiY3VycmVudFBhZ2VJbmRleCA+PSAoY3VycmVudFBhZ2VDb3VudCAtIDEpXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwicGFnZS1saW5rXCIgKGNsaWNrKT1cInN3aXRjaFRvUGFnZShjdXJyZW50UGFnZUNvdW50IC0gMSlcIj57eyAndGFibGUuY29tbW9uLnBhZ2luYXRpb24ubGFzdF9wYWdlJyB8IHRyYW5zbGF0ZSB9fTwvYT5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9uYXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbmxpbmUtYmxvY2sgaW5saW5lLWJsb2NrLWJvdHRvbSBwbC0yXCI+XHJcbiAgICAgICAgICAgIDxzcGFuIG5nYkRyb3Bkb3duICpuZ0lmPVwicGFnZVNpemVTZWxlY3RFbmFibGVkQW5kUG9zc2libGVcIj5cclxuICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tYmxhY2sgZHJvcGRvd24tdG9nZ2xlXCIgbmdiRHJvcGRvd25Ub2dnbGU+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5wYWdlX3NpemVfYnV0dG9uJyB8IHRyYW5zbGF0ZTp7Y3VycmVudDogY3VycmVudFBhZ2VTaXplfSB9fTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxzcGFuIG5nYkRyb3Bkb3duTWVudT5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gbmdiRHJvcGRvd25JdGVtICpuZ0Zvcj1cImxldCBwYWdlU2l6ZSBvZiBjdXJyZW50UG9zc2libGVQYWdlU2l6ZXNcIiAoY2xpY2spPVwiY2xpY2tPblBhZ2VTaXplKHBhZ2VTaXplKVwiIFtkaXNhYmxlZF09XCJwYWdlU2l6ZSA9PT0gY3VycmVudFBhZ2VTaXplXCI+XHJcbiAgICAgICAgICAgICAgICAgIHt7IHBhZ2VTaXplIH19XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC01IHRleHQtcmlnaHRcIj5cclxuICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJhY3Rpb25zQ2FwdGlvblRlbXBsYXRlXCIgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInt9XCI+IDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICZuYnNwO1xyXG4gICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cImhlYWRlckFjdGlvbnNFbmFibGVkQW5kUG9zc2libGVcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXt7IGFjdGlvbi5kaXNwbGF5Q2xhc3MgfHwgJ2xpZ2h0JyB9fSB7eyBhY3Rpb24uYWRkaXRpb25hbENsYXNzZXMgfHwgJycgfX0gbXItMVwiICpuZ0Zvcj1cImxldCBhY3Rpb24gb2YgaGVhZGVyQWN0aW9uc1wiXHJcbiAgICAgICAgICAgIChjbGljayk9XCJjbGlja09uQ29udGV4dEFjdGlvbihhY3Rpb24pXCIgW2Rpc2FibGVkXT1cIiFpc0NvbnRleHRBY3Rpb25BbGxvd2VkKGFjdGlvbilcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICB7eyByZXNvbHZlTGFiZWwoYWN0aW9uKSB9fVxyXG4gICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwXCIgbmdiRHJvcGRvd24gKm5nSWY9XCJjb250ZXh0QWN0aW9uc0VuYWJsZWRBbmRQb3NzaWJsZVwiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1saWdodCBkcm9wZG93bi10b2dnbGVcIiBbZGlzYWJsZWRdPVwiIWFueUJ1dHRvbkFjdGlvbnNBbGxvd2VkXCIgbmdiRHJvcGRvd25Ub2dnbGU+e3sgJ3RhYmxlLmNvbW1vbi5tZXNzYWdlcy5hY3Rpb25zX2J1dHRvbicgfCB0cmFuc2xhdGUgfX08L2J1dHRvbj5cclxuICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bk1lbnU+XHJcbiAgICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93bkl0ZW0+XHJcbiAgICAgICAgICAgICAgICA8aDYgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj57eyAndGFibGUuY29tbW9uLm1lc3NhZ2VzLnBpY2tfYWN0aW9uJyB8IHRyYW5zbGF0ZSB9fTwvaDY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBuZ2JEcm9wZG93bkl0ZW0gKm5nRm9yPVwibGV0IGFjdGlvbiBvZiBjdXJyZW50QWN0aW9uc1wiIChjbGljayk9XCJjbGlja09uQ29udGV4dEFjdGlvbihhY3Rpb24pXCIgW2Rpc2FibGVkXT1cIiFpc0NvbnRleHRBY3Rpb25BbGxvd2VkKGFjdGlvbilcIj5cclxuICAgICAgICAgICAgICAgIHt7IHJlc29sdmVMYWJlbChhY3Rpb24pIH19XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuXHJcbjwvZGl2PlxyXG4iXX0=