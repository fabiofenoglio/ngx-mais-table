import { Injectable } from '@angular/core';
import { MaisTableLogger } from '../utils';
import * as i0 from "@angular/core";
var MaisTableRegistryService = /** @class */ (function () {
    function MaisTableRegistryService() {
        this.registry = {};
        this.logger = new MaisTableLogger('MaisTableRegistryService');
        this.logger.trace('building service');
    }
    MaisTableRegistryService.prototype.register = function (key, component) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!component) {
            this.logger.warn('Invalid component provided to registry', component);
            throw Error('Invalid component provided to registry');
        }
        if (!this.registry[key]) {
            this.registry[key] = [];
        }
        var uuid = 'MTREG-' + (++MaisTableRegistryService.counter) + '-' + Math.round(Math.random() * 100000);
        var item = {
            key: key,
            component: component,
            registrationId: uuid
        };
        this.registry[key].push(item);
        this.logger.debug('registered table', item);
        return uuid;
    };
    MaisTableRegistryService.prototype.unregister = function (key, uuid) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!uuid) {
            this.logger.warn('Invalid uuid provided to registry', uuid);
            throw Error('Invalid uuid provided to registry');
        }
        this.logger.debug('unregistered table', uuid);
        this.registry[key] = this.registry[key].filter(function (o) { return o.registrationId !== uuid; });
    };
    MaisTableRegistryService.prototype.get = function (key) {
        return this.registry[key];
    };
    MaisTableRegistryService.prototype.getSingle = function (key) {
        var arr = this.registry[key];
        if (!arr || !arr.length) {
            return null;
        }
        else if (arr.length > 1) {
            this.logger.error('MULTIPLE REFERENCES FOR KEY', key);
            return null;
        }
        else {
            return arr[0].component;
        }
    };
    MaisTableRegistryService.counter = 0;
    /** @nocollapse */ MaisTableRegistryService.ɵfac = function MaisTableRegistryService_Factory(t) { return new (t || MaisTableRegistryService)(); };
    /** @nocollapse */ MaisTableRegistryService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableRegistryService, factory: MaisTableRegistryService.ɵfac, providedIn: 'root' });
    return MaisTableRegistryService;
}());
export { MaisTableRegistryService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableRegistryService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1yZWdpc3RyeS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1haXMtdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpcy10YWJsZS1yZWdpc3RyeS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFVBQVUsQ0FBQzs7QUFHM0M7SUFTRTtRQUZRLGFBQVEsR0FBUSxFQUFFLENBQUM7UUFHekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGVBQWUsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVNLDJDQUFRLEdBQWYsVUFBZ0IsR0FBVyxFQUFFLFNBQTZCO1FBQ3hELElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUMxRCxNQUFNLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1NBQ2pEO1FBQ0QsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHdDQUF3QyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ3RFLE1BQU0sS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7U0FDdkQ7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN6QjtRQUVELElBQU0sSUFBSSxHQUFXLFFBQVEsR0FBRyxDQUFDLEVBQUUsd0JBQXdCLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQ2hILElBQU0sSUFBSSxHQUFHO1lBQ1gsR0FBRyxLQUFBO1lBQ0gsU0FBUyxXQUFBO1lBQ1QsY0FBYyxFQUFFLElBQUk7U0FDckIsQ0FBQztRQUVGLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTlCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVNLDZDQUFVLEdBQWpCLFVBQWtCLEdBQVcsRUFBRSxJQUFZO1FBQ3pDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUMxRCxNQUFNLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1NBQ2pEO1FBQ0QsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1DQUFtQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzVELE1BQU0sS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7U0FDbEQ7UUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsQ0FBQyxDQUFDLGNBQWMsS0FBSyxJQUFJLEVBQXpCLENBQXlCLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRU0sc0NBQUcsR0FBVixVQUFXLEdBQVc7UUFDcEIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFTSw0Q0FBUyxHQUFoQixVQUFpQixHQUFXO1FBQzFCLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdEQsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNO1lBQ0wsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQWxFYyxnQ0FBTyxHQUFHLENBQUMsQ0FBQztvR0FGaEIsd0JBQXdCO29FQUF4Qix3QkFBd0IsV0FBeEIsd0JBQXdCLG1CQURaLE1BQU07bUNBSi9CO0NBMEVDLEFBdEVELElBc0VDO1NBckVZLHdCQUF3QjtrREFBeEIsd0JBQXdCO2NBRHBDLFVBQVU7ZUFBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1haXNUYWJsZUxvZ2dlciB9IGZyb20gJy4uL3V0aWxzJztcclxuaW1wb3J0IHsgTWFpc1RhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi4vbWFpcy10YWJsZS5jb21wb25lbnQnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBNYWlzVGFibGVSZWdpc3RyeVNlcnZpY2Uge1xyXG5cclxuICBwcml2YXRlIHN0YXRpYyBjb3VudGVyID0gMDtcclxuXHJcbiAgcHJpdmF0ZSBsb2dnZXI6IE1haXNUYWJsZUxvZ2dlcjtcclxuXHJcbiAgcHJpdmF0ZSByZWdpc3RyeTogYW55ID0ge307XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5sb2dnZXIgPSBuZXcgTWFpc1RhYmxlTG9nZ2VyKCdNYWlzVGFibGVSZWdpc3RyeVNlcnZpY2UnKTtcclxuICAgIHRoaXMubG9nZ2VyLnRyYWNlKCdidWlsZGluZyBzZXJ2aWNlJyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcmVnaXN0ZXIoa2V5OiBzdHJpbmcsIGNvbXBvbmVudDogTWFpc1RhYmxlQ29tcG9uZW50KTogc3RyaW5nIHtcclxuICAgIGlmICgha2V5KSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ0ludmFsaWQga2V5IHByb3ZpZGVkIHRvIHJlZ2lzdHJ5Jywga2V5KTtcclxuICAgICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQga2V5IHByb3ZpZGVkIHRvIHJlZ2lzdHJ5Jyk7XHJcbiAgICB9XHJcbiAgICBpZiAoIWNvbXBvbmVudCkge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdJbnZhbGlkIGNvbXBvbmVudCBwcm92aWRlZCB0byByZWdpc3RyeScsIGNvbXBvbmVudCk7XHJcbiAgICAgIHRocm93IEVycm9yKCdJbnZhbGlkIGNvbXBvbmVudCBwcm92aWRlZCB0byByZWdpc3RyeScpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghdGhpcy5yZWdpc3RyeVtrZXldKSB7XHJcbiAgICAgIHRoaXMucmVnaXN0cnlba2V5XSA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHV1aWQ6IHN0cmluZyA9ICdNVFJFRy0nICsgKCsrTWFpc1RhYmxlUmVnaXN0cnlTZXJ2aWNlLmNvdW50ZXIpICsgJy0nICsgTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMTAwMDAwKTtcclxuICAgIGNvbnN0IGl0ZW0gPSB7XHJcbiAgICAgIGtleSxcclxuICAgICAgY29tcG9uZW50LFxyXG4gICAgICByZWdpc3RyYXRpb25JZDogdXVpZFxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJlZ2lzdHJ5W2tleV0ucHVzaChpdGVtKTtcclxuXHJcbiAgICB0aGlzLmxvZ2dlci5kZWJ1ZygncmVnaXN0ZXJlZCB0YWJsZScsIGl0ZW0pO1xyXG4gICAgcmV0dXJuIHV1aWQ7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdW5yZWdpc3RlcihrZXk6IHN0cmluZywgdXVpZDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIWtleSkge1xyXG4gICAgICB0aGlzLmxvZ2dlci53YXJuKCdJbnZhbGlkIGtleSBwcm92aWRlZCB0byByZWdpc3RyeScsIGtleSk7XHJcbiAgICAgIHRocm93IEVycm9yKCdJbnZhbGlkIGtleSBwcm92aWRlZCB0byByZWdpc3RyeScpO1xyXG4gICAgfVxyXG4gICAgaWYgKCF1dWlkKSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ0ludmFsaWQgdXVpZCBwcm92aWRlZCB0byByZWdpc3RyeScsIHV1aWQpO1xyXG4gICAgICB0aHJvdyBFcnJvcignSW52YWxpZCB1dWlkIHByb3ZpZGVkIHRvIHJlZ2lzdHJ5Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ3VucmVnaXN0ZXJlZCB0YWJsZScsIHV1aWQpO1xyXG4gICAgdGhpcy5yZWdpc3RyeVtrZXldID0gdGhpcy5yZWdpc3RyeVtrZXldLmZpbHRlciggKG86IGFueSkgPT4gby5yZWdpc3RyYXRpb25JZCAhPT0gdXVpZCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0KGtleTogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy5yZWdpc3RyeVtrZXldO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFNpbmdsZShrZXk6IHN0cmluZyk6IE1haXNUYWJsZUNvbXBvbmVudCB8IG51bGwge1xyXG4gICAgY29uc3QgYXJyID0gdGhpcy5yZWdpc3RyeVtrZXldO1xyXG4gICAgaWYgKCFhcnIgfHwgIWFyci5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9IGVsc2UgaWYgKGFyci5sZW5ndGggPiAxKSB7XHJcbiAgICAgIHRoaXMubG9nZ2VyLmVycm9yKCdNVUxUSVBMRSBSRUZFUkVOQ0VTIEZPUiBLRVknLCBrZXkpO1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBhcnJbMF0uY29tcG9uZW50O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=