import { Injectable } from '@angular/core';
import { MaisTableEmbeddedLogger } from '../utils/mais-table-embedded-logger';
import * as i0 from "@angular/core";
var MaisTableLoggerService = /** @class */ (function () {
    function MaisTableLoggerService() {
    }
    MaisTableLoggerService.getConfiguredLogAdapter = function () {
        return MaisTableLoggerService.logAdapter || MaisTableLoggerService.embeddedLogger;
    };
    MaisTableLoggerService.prototype.withLogAdapter = function (logAdapter) {
        MaisTableLoggerService.logAdapter = logAdapter;
    };
    MaisTableLoggerService.embeddedLogger = new MaisTableEmbeddedLogger();
    MaisTableLoggerService.logAdapter = null;
    /** @nocollapse */ MaisTableLoggerService.ɵfac = function MaisTableLoggerService_Factory(t) { return new (t || MaisTableLoggerService)(); };
    /** @nocollapse */ MaisTableLoggerService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableLoggerService, factory: MaisTableLoggerService.ɵfac, providedIn: 'root' });
    return MaisTableLoggerService;
}());
export { MaisTableLoggerService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableLoggerService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1sb2dnZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYWlzLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL21haXMtdGFibGUtbG9nZ2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQzs7QUFFOUU7SUFNRTtJQUNBLENBQUM7SUFFYSw4Q0FBdUIsR0FBckM7UUFDRSxPQUFPLHNCQUFzQixDQUFDLFVBQVUsSUFBSSxzQkFBc0IsQ0FBQyxjQUFjLENBQUM7SUFDcEYsQ0FBQztJQUVNLCtDQUFjLEdBQXJCLFVBQXNCLFVBQXVDO1FBQzNELHNCQUFzQixDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDakQsQ0FBQztJQVpjLHFDQUFjLEdBQXlCLElBQUksdUJBQXVCLEVBQUUsQ0FBQztJQUNyRSxpQ0FBVSxHQUFnQyxJQUFJLENBQUM7Z0dBSG5ELHNCQUFzQjtrRUFBdEIsc0JBQXNCLFdBQXRCLHNCQUFzQixtQkFEVixNQUFNO2lDQUovQjtDQXFCQyxBQWpCRCxJQWlCQztTQWhCWSxzQkFBc0I7a0RBQXRCLHNCQUFzQjtjQURsQyxVQUFVO2VBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJTWFpc1RhYmxlTG9nQWRhcHRlciB9IGZyb20gJy4uL3V0aWxzL21haXMtdGFibGUtbG9nLWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVFbWJlZGRlZExvZ2dlciB9IGZyb20gJy4uL3V0aWxzL21haXMtdGFibGUtZW1iZWRkZWQtbG9nZ2VyJztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlTG9nZ2VyU2VydmljZSB7XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIGVtYmVkZGVkTG9nZ2VyOiBJTWFpc1RhYmxlTG9nQWRhcHRlciA9IG5ldyBNYWlzVGFibGVFbWJlZGRlZExvZ2dlcigpO1xyXG4gIHByaXZhdGUgc3RhdGljIGxvZ0FkYXB0ZXI6IElNYWlzVGFibGVMb2dBZGFwdGVyIHwgbnVsbCA9IG51bGw7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBnZXRDb25maWd1cmVkTG9nQWRhcHRlcigpOiBJTWFpc1RhYmxlTG9nQWRhcHRlciB7XHJcbiAgICByZXR1cm4gTWFpc1RhYmxlTG9nZ2VyU2VydmljZS5sb2dBZGFwdGVyIHx8IE1haXNUYWJsZUxvZ2dlclNlcnZpY2UuZW1iZWRkZWRMb2dnZXI7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgd2l0aExvZ0FkYXB0ZXIobG9nQWRhcHRlcjogSU1haXNUYWJsZUxvZ0FkYXB0ZXIgfCBudWxsKSB7XHJcbiAgICBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlLmxvZ0FkYXB0ZXIgPSBsb2dBZGFwdGVyO1xyXG4gIH1cclxuXHJcbn1cclxuIl19