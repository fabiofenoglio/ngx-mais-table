import { Injectable } from '@angular/core';
import { MaisTableLogger, MaisTableBrowserHelper } from '../utils';
import { MaisTableActionActivationCondition, MaisTablePaginationMethod, MaisTableRefreshStrategy } from '../model';
import { MaisTableLoggerService } from './mais-table-logger.service';
import * as i0 from "@angular/core";
import * as i1 from "./mais-table-logger.service";
var MaisTableService = /** @class */ (function () {
    function MaisTableService(maisTableLoggerService) {
        this.maisTableLoggerService = maisTableLoggerService;
        this.logger = new MaisTableLogger('MaisTableService');
        this.logger.trace('building service');
        this.configuration = this.buildDefaultConfiguration();
        this.logger.debug('initializing MAIS table component for browser', MaisTableBrowserHelper.getBrowser());
        this.maisTableLoggerService.withLogAdapter(null);
    }
    MaisTableService.prototype.configure = function (config) {
        Object.assign(this.configuration, config);
        return this;
    };
    MaisTableService.prototype.withLogAdapter = function (logAdapter) {
        this.maisTableLoggerService.withLogAdapter(logAdapter);
        return this;
    };
    MaisTableService.prototype.getConfiguration = function () {
        return this.configuration;
    };
    MaisTableService.prototype.buildDefaultConfiguration = function () {
        return {
            currentSchemaVersion: 'DEFAULT',
            refresh: {
                defaultStrategy: MaisTableRefreshStrategy.NONE,
                defaultInterval: 60000,
                defaultOnPushInBackground: true,
                defaultOnTickInBackground: true
            },
            rowSelection: {
                enabledByDefault: false,
                selectAllEnabledByDefault: true,
                multipleSelectionEnabledByDefault: true
            },
            columnToggling: {
                enabledByDefault: true,
                showFixedColumns: true
            },
            filtering: {
                enabledByDefault: true,
                autoReloadOnQueryClear: true,
                autoReloadOnQueryChange: true,
                autoReloadTimeout: 300,
                showUnfilterableColumns: true
            },
            contextFiltering: {
                enabledByDefault: true
            },
            pagination: {
                enabledByDefault: true,
                pageSizeSelectionEnabledByDefault: true,
                defaultPaginationMode: MaisTablePaginationMethod.CLIENT,
                defaultPageSize: 5,
                defaultPossiblePageSizes: [5, 10, 25, 50]
            },
            actions: {
                contextActionsEnabledByDefault: true,
                headerActionsEnabledByDefault: true,
                defaultHeaderActionActivationCondition: MaisTableActionActivationCondition.ALWAYS,
                defaultContextActionActivationCondition: MaisTableActionActivationCondition.MULTIPLE_SELECTION
            },
            columns: {
                defaultShowLabelInTable: true,
                defaultCanSort: false,
                defaultCanHide: true,
                defaultCanFilter: true,
                defaultIsDefaultFilter: true,
                defaultIsDefaulView: true
            },
            rowExpansion: {
                enabledByDefault: false,
                multipleExpansionEnabledByDefault: false
            },
            dragAndDrop: {
                dragEnabledByDefault: false,
                dropEnabledByDefault: false
            },
            itemTracking: {
                enabledByDefault: false
            },
            storePersistence: {
                enabledByDefault: true
            }
        };
    };
    /** @nocollapse */ MaisTableService.ɵfac = function MaisTableService_Factory(t) { return new (t || MaisTableService)(i0.ɵɵinject(i1.MaisTableLoggerService)); };
    /** @nocollapse */ MaisTableService.ɵprov = i0.ɵɵdefineInjectable({ token: MaisTableService, factory: MaisTableService.ɵfac, providedIn: 'root' });
    return MaisTableService;
}());
export { MaisTableService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaisTableService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.MaisTableLoggerService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1haXMtdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpcy10YWJsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxzQkFBc0IsRUFBd0IsTUFBTSxVQUFVLENBQUM7QUFDekYsT0FBTyxFQUNMLGtDQUFrQyxFQUNsQyx5QkFBeUIsRUFDekIsd0JBQXdCLEVBRXpCLE1BQU0sVUFBVSxDQUFDO0FBQ2xCLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7QUFFckU7SUFNRSwwQkFBb0Isc0JBQThDO1FBQTlDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDaEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUV0RCxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQywrQ0FBK0MsRUFBRSxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ3hHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVNLG9DQUFTLEdBQWhCLFVBQWlCLE1BQStCO1FBQzlDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMxQyxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSx5Q0FBYyxHQUFyQixVQUFzQixVQUFnQztRQUNwRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3ZELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVNLDJDQUFnQixHQUF2QjtRQUNFLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0lBRU8sb0RBQXlCLEdBQWpDO1FBQ0UsT0FBTztZQUNMLG9CQUFvQixFQUFFLFNBQVM7WUFDL0IsT0FBTyxFQUFFO2dCQUNQLGVBQWUsRUFBRSx3QkFBd0IsQ0FBQyxJQUFJO2dCQUM5QyxlQUFlLEVBQUUsS0FBSztnQkFDdEIseUJBQXlCLEVBQUUsSUFBSTtnQkFDL0IseUJBQXlCLEVBQUUsSUFBSTthQUNoQztZQUNELFlBQVksRUFBRTtnQkFDWixnQkFBZ0IsRUFBRSxLQUFLO2dCQUN2Qix5QkFBeUIsRUFBRSxJQUFJO2dCQUMvQixpQ0FBaUMsRUFBRSxJQUFJO2FBQ3hDO1lBQ0QsY0FBYyxFQUFFO2dCQUNkLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGdCQUFnQixFQUFFLElBQUk7YUFDdkI7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsc0JBQXNCLEVBQUUsSUFBSTtnQkFDNUIsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsaUJBQWlCLEVBQUUsR0FBRztnQkFDdEIsdUJBQXVCLEVBQUUsSUFBSTthQUM5QjtZQUNELGdCQUFnQixFQUFFO2dCQUNoQixnQkFBZ0IsRUFBRSxJQUFJO2FBQ3ZCO1lBQ0QsVUFBVSxFQUFFO2dCQUNWLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGlDQUFpQyxFQUFFLElBQUk7Z0JBQ3ZDLHFCQUFxQixFQUFFLHlCQUF5QixDQUFDLE1BQU07Z0JBQ3ZELGVBQWUsRUFBRSxDQUFDO2dCQUNsQix3QkFBd0IsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQzthQUMxQztZQUNELE9BQU8sRUFBRTtnQkFDUCw4QkFBOEIsRUFBRSxJQUFJO2dCQUNwQyw2QkFBNkIsRUFBRSxJQUFJO2dCQUNuQyxzQ0FBc0MsRUFBRSxrQ0FBa0MsQ0FBQyxNQUFNO2dCQUNqRix1Q0FBdUMsRUFBRSxrQ0FBa0MsQ0FBQyxrQkFBa0I7YUFDL0Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsY0FBYyxFQUFFLEtBQUs7Z0JBQ3JCLGNBQWMsRUFBRSxJQUFJO2dCQUNwQixnQkFBZ0IsRUFBRSxJQUFJO2dCQUN0QixzQkFBc0IsRUFBRSxJQUFJO2dCQUM1QixtQkFBbUIsRUFBRSxJQUFJO2FBQzFCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLGdCQUFnQixFQUFFLEtBQUs7Z0JBQ3ZCLGlDQUFpQyxFQUFFLEtBQUs7YUFDekM7WUFDRCxXQUFXLEVBQUU7Z0JBQ1gsb0JBQW9CLEVBQUUsS0FBSztnQkFDM0Isb0JBQW9CLEVBQUUsS0FBSzthQUM1QjtZQUNELFlBQVksRUFBRTtnQkFDWixnQkFBZ0IsRUFBRSxLQUFLO2FBQ3hCO1lBQ0QsZ0JBQWdCLEVBQUU7Z0JBQ2hCLGdCQUFnQixFQUFFLElBQUk7YUFDdkI7U0FDRixDQUFDO0lBQ0osQ0FBQztvRkE1RlUsZ0JBQWdCOzREQUFoQixnQkFBZ0IsV0FBaEIsZ0JBQWdCLG1CQURKLE1BQU07MkJBVi9CO0NBd0dDLEFBOUZELElBOEZDO1NBN0ZZLGdCQUFnQjtrREFBaEIsZ0JBQWdCO2NBRDVCLFVBQVU7ZUFBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1haXNUYWJsZUxvZ2dlciwgTWFpc1RhYmxlQnJvd3NlckhlbHBlciwgSU1haXNUYWJsZUxvZ0FkYXB0ZXIgfSBmcm9tICcuLi91dGlscyc7XHJcbmltcG9ydCB7XHJcbiAgTWFpc1RhYmxlQWN0aW9uQWN0aXZhdGlvbkNvbmRpdGlvbixcclxuICBNYWlzVGFibGVQYWdpbmF0aW9uTWV0aG9kLFxyXG4gIE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneSxcclxuICBJTWFpc1RhYmxlQ29uZmlndXJhdGlvblxyXG59IGZyb20gJy4uL21vZGVsJztcclxuaW1wb3J0IHsgTWFpc1RhYmxlTG9nZ2VyU2VydmljZSB9IGZyb20gJy4vbWFpcy10YWJsZS1sb2dnZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE1haXNUYWJsZVNlcnZpY2Uge1xyXG5cclxuICBwcml2YXRlIGxvZ2dlcjogTWFpc1RhYmxlTG9nZ2VyO1xyXG4gIHByaXZhdGUgY29uZmlndXJhdGlvbjogSU1haXNUYWJsZUNvbmZpZ3VyYXRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbWFpc1RhYmxlTG9nZ2VyU2VydmljZTogTWFpc1RhYmxlTG9nZ2VyU2VydmljZSkge1xyXG4gICAgdGhpcy5sb2dnZXIgPSBuZXcgTWFpc1RhYmxlTG9nZ2VyKCdNYWlzVGFibGVTZXJ2aWNlJyk7XHJcbiAgICB0aGlzLmxvZ2dlci50cmFjZSgnYnVpbGRpbmcgc2VydmljZScpO1xyXG4gICAgdGhpcy5jb25maWd1cmF0aW9uID0gdGhpcy5idWlsZERlZmF1bHRDb25maWd1cmF0aW9uKCk7XHJcblxyXG4gICAgdGhpcy5sb2dnZXIuZGVidWcoJ2luaXRpYWxpemluZyBNQUlTIHRhYmxlIGNvbXBvbmVudCBmb3IgYnJvd3NlcicsIE1haXNUYWJsZUJyb3dzZXJIZWxwZXIuZ2V0QnJvd3NlcigpKTtcclxuICAgIHRoaXMubWFpc1RhYmxlTG9nZ2VyU2VydmljZS53aXRoTG9nQWRhcHRlcihudWxsKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjb25maWd1cmUoY29uZmlnOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbik6IE1haXNUYWJsZVNlcnZpY2Uge1xyXG4gICAgT2JqZWN0LmFzc2lnbih0aGlzLmNvbmZpZ3VyYXRpb24sIGNvbmZpZyk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIHB1YmxpYyB3aXRoTG9nQWRhcHRlcihsb2dBZGFwdGVyOiBJTWFpc1RhYmxlTG9nQWRhcHRlcik6IE1haXNUYWJsZVNlcnZpY2Uge1xyXG4gICAgdGhpcy5tYWlzVGFibGVMb2dnZXJTZXJ2aWNlLndpdGhMb2dBZGFwdGVyKGxvZ0FkYXB0ZXIpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0Q29uZmlndXJhdGlvbigpOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBidWlsZERlZmF1bHRDb25maWd1cmF0aW9uKCk6IElNYWlzVGFibGVDb25maWd1cmF0aW9uIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGN1cnJlbnRTY2hlbWFWZXJzaW9uOiAnREVGQVVMVCcsXHJcbiAgICAgIHJlZnJlc2g6IHtcclxuICAgICAgICBkZWZhdWx0U3RyYXRlZ3k6IE1haXNUYWJsZVJlZnJlc2hTdHJhdGVneS5OT05FLFxyXG4gICAgICAgIGRlZmF1bHRJbnRlcnZhbDogNjAwMDAsXHJcbiAgICAgICAgZGVmYXVsdE9uUHVzaEluQmFja2dyb3VuZDogdHJ1ZSxcclxuICAgICAgICBkZWZhdWx0T25UaWNrSW5CYWNrZ3JvdW5kOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIHJvd1NlbGVjdGlvbjoge1xyXG4gICAgICAgIGVuYWJsZWRCeURlZmF1bHQ6IGZhbHNlLFxyXG4gICAgICAgIHNlbGVjdEFsbEVuYWJsZWRCeURlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgbXVsdGlwbGVTZWxlY3Rpb25FbmFibGVkQnlEZWZhdWx0OiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIGNvbHVtblRvZ2dsaW5nOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBzaG93Rml4ZWRDb2x1bW5zOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIGZpbHRlcmluZzoge1xyXG4gICAgICAgIGVuYWJsZWRCeURlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgYXV0b1JlbG9hZE9uUXVlcnlDbGVhcjogdHJ1ZSxcclxuICAgICAgICBhdXRvUmVsb2FkT25RdWVyeUNoYW5nZTogdHJ1ZSxcclxuICAgICAgICBhdXRvUmVsb2FkVGltZW91dDogMzAwLFxyXG4gICAgICAgIHNob3dVbmZpbHRlcmFibGVDb2x1bW5zOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIGNvbnRleHRGaWx0ZXJpbmc6IHtcclxuICAgICAgICBlbmFibGVkQnlEZWZhdWx0OiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgICBlbmFibGVkQnlEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIHBhZ2VTaXplU2VsZWN0aW9uRW5hYmxlZEJ5RGVmYXVsdDogdHJ1ZSxcclxuICAgICAgICBkZWZhdWx0UGFnaW5hdGlvbk1vZGU6IE1haXNUYWJsZVBhZ2luYXRpb25NZXRob2QuQ0xJRU5ULFxyXG4gICAgICAgIGRlZmF1bHRQYWdlU2l6ZTogNSxcclxuICAgICAgICBkZWZhdWx0UG9zc2libGVQYWdlU2l6ZXM6IFs1LCAxMCwgMjUsIDUwXVxyXG4gICAgICB9LFxyXG4gICAgICBhY3Rpb25zOiB7XHJcbiAgICAgICAgY29udGV4dEFjdGlvbnNFbmFibGVkQnlEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGhlYWRlckFjdGlvbnNFbmFibGVkQnlEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRIZWFkZXJBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uOiBNYWlzVGFibGVBY3Rpb25BY3RpdmF0aW9uQ29uZGl0aW9uLkFMV0FZUyxcclxuICAgICAgICBkZWZhdWx0Q29udGV4dEFjdGlvbkFjdGl2YXRpb25Db25kaXRpb246IE1haXNUYWJsZUFjdGlvbkFjdGl2YXRpb25Db25kaXRpb24uTVVMVElQTEVfU0VMRUNUSU9OXHJcbiAgICAgIH0sXHJcbiAgICAgIGNvbHVtbnM6IHtcclxuICAgICAgICBkZWZhdWx0U2hvd0xhYmVsSW5UYWJsZTogdHJ1ZSxcclxuICAgICAgICBkZWZhdWx0Q2FuU29ydDogZmFsc2UsXHJcbiAgICAgICAgZGVmYXVsdENhbkhpZGU6IHRydWUsXHJcbiAgICAgICAgZGVmYXVsdENhbkZpbHRlcjogdHJ1ZSxcclxuICAgICAgICBkZWZhdWx0SXNEZWZhdWx0RmlsdGVyOiB0cnVlLFxyXG4gICAgICAgIGRlZmF1bHRJc0RlZmF1bFZpZXc6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgcm93RXhwYW5zaW9uOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogZmFsc2UsXHJcbiAgICAgICAgbXVsdGlwbGVFeHBhbnNpb25FbmFibGVkQnlEZWZhdWx0OiBmYWxzZVxyXG4gICAgICB9LFxyXG4gICAgICBkcmFnQW5kRHJvcDoge1xyXG4gICAgICAgIGRyYWdFbmFibGVkQnlEZWZhdWx0OiBmYWxzZSxcclxuICAgICAgICBkcm9wRW5hYmxlZEJ5RGVmYXVsdDogZmFsc2VcclxuICAgICAgfSxcclxuICAgICAgaXRlbVRyYWNraW5nOiB7XHJcbiAgICAgICAgZW5hYmxlZEJ5RGVmYXVsdDogZmFsc2VcclxuICAgICAgfSxcclxuICAgICAgc3RvcmVQZXJzaXN0ZW5jZToge1xyXG4gICAgICAgIGVuYWJsZWRCeURlZmF1bHQ6IHRydWVcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIl19