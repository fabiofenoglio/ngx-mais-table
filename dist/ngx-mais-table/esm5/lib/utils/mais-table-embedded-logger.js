import { __read, __spread } from "tslib";
var MaisTableEmbeddedLogger = /** @class */ (function () {
    function MaisTableEmbeddedLogger() {
    }
    MaisTableEmbeddedLogger.prototype.trace = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.log.apply(console, __spread(['[trace] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.debug = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.log.apply(console, __spread(['[trace] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.info = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.log.apply(console, __spread(['[INFO] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.log = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.log.apply(console, __spread(['[INFO] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.warn = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.warn.apply(console, __spread(['[WARN] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.error = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.error.apply(console, __spread(['[ERROR] ' + message], additional));
    };
    MaisTableEmbeddedLogger.prototype.fatal = function (message) {
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        console.error.apply(console, __spread(['[FATAL] ' + message], additional));
    };
    return MaisTableEmbeddedLogger;
}());
export { MaisTableEmbeddedLogger };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1lbWJlZGRlZC1sb2dnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLWVtYmVkZGVkLWxvZ2dlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBRUE7SUFBQTtJQTZCQSxDQUFDO0lBM0JDLHVDQUFLLEdBQUwsVUFBTSxPQUFZO1FBQUUsb0JBQW9CO2FBQXBCLFVBQW9CLEVBQXBCLHFCQUFvQixFQUFwQixJQUFvQjtZQUFwQixtQ0FBb0I7O1FBQ3RDLE9BQU8sQ0FBQyxHQUFHLE9BQVgsT0FBTyxZQUFLLFVBQVUsR0FBRyxPQUFPLEdBQUssVUFBVSxHQUFFO0lBQ25ELENBQUM7SUFFRCx1Q0FBSyxHQUFMLFVBQU0sT0FBWTtRQUFFLG9CQUFvQjthQUFwQixVQUFvQixFQUFwQixxQkFBb0IsRUFBcEIsSUFBb0I7WUFBcEIsbUNBQW9COztRQUN0QyxPQUFPLENBQUMsR0FBRyxPQUFYLE9BQU8sWUFBSyxVQUFVLEdBQUcsT0FBTyxHQUFLLFVBQVUsR0FBRTtJQUNuRCxDQUFDO0lBRUQsc0NBQUksR0FBSixVQUFLLE9BQVk7UUFBRSxvQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLG1DQUFvQjs7UUFDckMsT0FBTyxDQUFDLEdBQUcsT0FBWCxPQUFPLFlBQUssU0FBUyxHQUFHLE9BQU8sR0FBSyxVQUFVLEdBQUU7SUFDbEQsQ0FBQztJQUVELHFDQUFHLEdBQUgsVUFBSSxPQUFZO1FBQUUsb0JBQW9CO2FBQXBCLFVBQW9CLEVBQXBCLHFCQUFvQixFQUFwQixJQUFvQjtZQUFwQixtQ0FBb0I7O1FBQ3BDLE9BQU8sQ0FBQyxHQUFHLE9BQVgsT0FBTyxZQUFLLFNBQVMsR0FBRyxPQUFPLEdBQUssVUFBVSxHQUFFO0lBQ2xELENBQUM7SUFFRCxzQ0FBSSxHQUFKLFVBQUssT0FBWTtRQUFFLG9CQUFvQjthQUFwQixVQUFvQixFQUFwQixxQkFBb0IsRUFBcEIsSUFBb0I7WUFBcEIsbUNBQW9COztRQUNyQyxPQUFPLENBQUMsSUFBSSxPQUFaLE9BQU8sWUFBTSxTQUFTLEdBQUcsT0FBTyxHQUFLLFVBQVUsR0FBRTtJQUNuRCxDQUFDO0lBRUQsdUNBQUssR0FBTCxVQUFNLE9BQVk7UUFBRSxvQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLG1DQUFvQjs7UUFDdEMsT0FBTyxDQUFDLEtBQUssT0FBYixPQUFPLFlBQU8sVUFBVSxHQUFHLE9BQU8sR0FBSyxVQUFVLEdBQUU7SUFDckQsQ0FBQztJQUVELHVDQUFLLEdBQUwsVUFBTSxPQUFZO1FBQUUsb0JBQW9CO2FBQXBCLFVBQW9CLEVBQXBCLHFCQUFvQixFQUFwQixJQUFvQjtZQUFwQixtQ0FBb0I7O1FBQ3RDLE9BQU8sQ0FBQyxLQUFLLE9BQWIsT0FBTyxZQUFPLFVBQVUsR0FBRyxPQUFPLEdBQUssVUFBVSxHQUFFO0lBQ3JELENBQUM7SUFDSCw4QkFBQztBQUFELENBQUMsQUE3QkQsSUE2QkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTWFpc1RhYmxlTG9nQWRhcHRlciB9IGZyb20gJy4vbWFpcy10YWJsZS1sb2ctYWRhcHRlcic7XHJcblxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlRW1iZWRkZWRMb2dnZXIgaW1wbGVtZW50cyBJTWFpc1RhYmxlTG9nQWRhcHRlciB7XHJcblxyXG4gIHRyYWNlKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdbdHJhY2VdICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGRlYnVnKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdbdHJhY2VdICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGluZm8obWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ1tJTkZPXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG5cclxuICBsb2cobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5sb2coJ1tJTkZPXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG5cclxuICB3YXJuKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUud2FybignW1dBUk5dICcgKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGVycm9yKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoJ1tFUlJPUl0gJyArIG1lc3NhZ2UsIC4uLmFkZGl0aW9uYWwpO1xyXG4gIH1cclxuXHJcbiAgZmF0YWwobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgY29uc29sZS5lcnJvcignW0ZBVEFMXSAnICsgbWVzc2FnZSwgLi4uYWRkaXRpb25hbCk7XHJcbiAgfVxyXG59XHJcblxyXG4iXX0=