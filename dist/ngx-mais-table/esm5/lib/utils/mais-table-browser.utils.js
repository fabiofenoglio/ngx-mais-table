import { detect } from 'detect-browser';
var MaisTableBrowserHelper = /** @class */ (function () {
    function MaisTableBrowserHelper() {
    }
    MaisTableBrowserHelper.getBrowser = function () {
        var _a;
        var name = (_a = this.browser) === null || _a === void 0 ? void 0 : _a.name;
        if (name === Browser.CHROME ||
            name === Browser.FIREFOX ||
            name === Browser.EDGE ||
            name === Browser.IE ||
            name === Browser.OPERA ||
            name === Browser.SAFARI) {
            return name;
        }
        else {
            return Browser.OTHER;
        }
    };
    MaisTableBrowserHelper.isIE = function () {
        return MaisTableBrowserHelper.getBrowser() === Browser.IE;
    };
    MaisTableBrowserHelper.browser = detect();
    return MaisTableBrowserHelper;
}());
export { MaisTableBrowserHelper };
export var Browser;
(function (Browser) {
    Browser["EDGE"] = "edge";
    Browser["OPERA"] = "opera";
    Browser["CHROME"] = "chrome";
    Browser["IE"] = "ie";
    Browser["FIREFOX"] = "firefox";
    Browser["SAFARI"] = "safari";
    Browser["OTHER"] = "other";
})(Browser || (Browser = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1icm93c2VyLnV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1haXMtdGFibGUvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvbWFpcy10YWJsZS1icm93c2VyLnV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQWtDLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEU7SUFBQTtJQXNCQSxDQUFDO0lBbEJlLGlDQUFVLEdBQXhCOztRQUNFLElBQU0sSUFBSSxTQUFHLElBQUksQ0FBQyxPQUFPLDBDQUFFLElBQUksQ0FBQztRQUNoQyxJQUNFLElBQUksS0FBSyxPQUFPLENBQUMsTUFBTTtZQUN2QixJQUFJLEtBQUssT0FBTyxDQUFDLE9BQU87WUFDeEIsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJO1lBQ3JCLElBQUksS0FBSyxPQUFPLENBQUMsRUFBRTtZQUNuQixJQUFJLEtBQUssT0FBTyxDQUFDLEtBQUs7WUFDdEIsSUFBSSxLQUFLLE9BQU8sQ0FBQyxNQUFNLEVBQUc7WUFDeEIsT0FBTyxJQUFlLENBQUM7U0FDeEI7YUFBTTtZQUNMLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFFYSwyQkFBSSxHQUFsQjtRQUNFLE9BQU8sc0JBQXNCLENBQUMsVUFBVSxFQUFFLEtBQUssT0FBTyxDQUFDLEVBQUUsQ0FBQztJQUM1RCxDQUFDO0lBbkJjLDhCQUFPLEdBQTRDLE1BQU0sRUFBRSxDQUFDO0lBb0I3RSw2QkFBQztDQUFBLEFBdEJELElBc0JDO1NBdEJxQixzQkFBc0I7QUF3QjVDLE1BQU0sQ0FBTixJQUFZLE9BUVg7QUFSRCxXQUFZLE9BQU87SUFDakIsd0JBQWEsQ0FBQTtJQUNiLDBCQUFlLENBQUE7SUFDZiw0QkFBaUIsQ0FBQTtJQUNqQixvQkFBUyxDQUFBO0lBQ1QsOEJBQW1CLENBQUE7SUFDbkIsNEJBQWlCLENBQUE7SUFDakIsMEJBQWUsQ0FBQTtBQUNqQixDQUFDLEVBUlcsT0FBTyxLQUFQLE9BQU8sUUFRbEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBkZXRlY3QsIEJyb3dzZXJJbmZvLCBCb3RJbmZvLCBOb2RlSW5mbyB9IGZyb20gJ2RldGVjdC1icm93c2VyJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBNYWlzVGFibGVCcm93c2VySGVscGVyIHtcclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgYnJvd3NlcjogQnJvd3NlckluZm8gfCBCb3RJbmZvIHwgTm9kZUluZm8gfCBudWxsID0gZGV0ZWN0KCk7XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgZ2V0QnJvd3NlcigpOiBCcm93c2VyIHtcclxuICAgIGNvbnN0IG5hbWUgPSB0aGlzLmJyb3dzZXI/Lm5hbWU7XHJcbiAgICBpZiAoXHJcbiAgICAgIG5hbWUgPT09IEJyb3dzZXIuQ0hST01FIHx8XHJcbiAgICAgIG5hbWUgPT09IEJyb3dzZXIuRklSRUZPWCB8fFxyXG4gICAgICBuYW1lID09PSBCcm93c2VyLkVER0UgfHxcclxuICAgICAgbmFtZSA9PT0gQnJvd3Nlci5JRSB8fFxyXG4gICAgICBuYW1lID09PSBCcm93c2VyLk9QRVJBIHx8XHJcbiAgICAgIG5hbWUgPT09IEJyb3dzZXIuU0FGQVJJICkge1xyXG4gICAgICAgIHJldHVybiBuYW1lIGFzIEJyb3dzZXI7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIEJyb3dzZXIuT1RIRVI7XHJcbiAgICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgaXNJRSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVCcm93c2VySGVscGVyLmdldEJyb3dzZXIoKSA9PT0gQnJvd3Nlci5JRTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIEJyb3dzZXIge1xyXG4gIEVER0UgPSAnZWRnZScsXHJcbiAgT1BFUkEgPSAnb3BlcmEnLFxyXG4gIENIUk9NRSA9ICdjaHJvbWUnLFxyXG4gIElFID0gJ2llJyxcclxuICBGSVJFRk9YID0gJ2ZpcmVmb3gnLFxyXG4gIFNBRkFSSSA9ICdzYWZhcmknLFxyXG4gIE9USEVSID0gJ290aGVyJ1xyXG59XHJcbiJdfQ==