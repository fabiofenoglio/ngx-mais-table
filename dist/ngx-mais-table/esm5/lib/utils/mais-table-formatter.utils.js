import { __values } from "tslib";
import { LowerCasePipe, UpperCasePipe, DatePipe, CurrencyPipe } from '@angular/common';
// @dynamic
var MaisTableFormatterHelper = /** @class */ (function () {
    function MaisTableFormatterHelper() {
    }
    MaisTableFormatterHelper.getPropertyValue = function (object, field) {
        if (object === null || object === undefined) {
            return null;
        }
        if (field.indexOf('.') === -1) {
            return object[field];
        }
        var splitted = field.split('.');
        var subObject = object[splitted[0]];
        return this.getPropertyValue(subObject, splitted.slice(1).join('.'));
    };
    MaisTableFormatterHelper.format = function (raw, formatterSpec, locale) {
        if (!raw) {
            return raw;
        }
        var transformFn = MaisTableFormatterHelper.transformMap[formatterSpec.formatter];
        if (!transformFn) {
            throw new Error('Unknown formatter: ' + formatterSpec.formatter);
        }
        return transformFn(raw, locale || 'en', formatterSpec.arguments);
    };
    MaisTableFormatterHelper.extractValue = function (row, column, locale) {
        var e_1, _a;
        if (!row) {
            return null;
        }
        var val;
        if (column.valueExtractor) {
            val = column.valueExtractor(row);
        }
        else if (column.field) {
            val = MaisTableFormatterHelper.getPropertyValue(row, column.field);
        }
        else {
            val = undefined;
        }
        if (column.formatters) {
            try {
                for (var _b = __values((Array.isArray(column.formatters) ? column.formatters : [column.formatters])), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var formatter = _c.value;
                    if (formatter.formatter) {
                        val = MaisTableFormatterHelper.format(val, formatter, locale);
                    }
                    else if (formatter.format) {
                        val = formatter.format(val);
                    }
                    else {
                        val = MaisTableFormatterHelper.format(val, { formatter: formatter, arguments: null }, locale);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return val;
    };
    MaisTableFormatterHelper.isDefaultVisibleColumn = function (column, config) {
        var _a, _b;
        return (column.defaultVisible === true || column.defaultVisible === false ?
            column.defaultVisible : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaulView) ||
            !MaisTableFormatterHelper.isHideable(column, config);
    };
    MaisTableFormatterHelper.isDefaultFilterColumn = function (column, config) {
        var _a, _b;
        return column.defaultFilter === true || column.defaultFilter === false ? column.defaultFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaultFilter;
    };
    MaisTableFormatterHelper.isHideable = function (column, config) {
        var _a, _b;
        return column.canHide === true || column.canHide === false ? column.canHide : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanHide;
    };
    MaisTableFormatterHelper.isSortable = function (column, config) {
        var _a, _b;
        return column.canSort === true || column.canSort === false ? column.canSort : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanSort;
    };
    MaisTableFormatterHelper.isFilterable = function (column, config) {
        var _a, _b;
        return (column.canFilter === true || column.canFilter === false ? column.canFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanFilter) &&
            (column.serverField || column.field);
    };
    MaisTableFormatterHelper.hasLabel = function (column, config) {
        return (column.labelKey || column.label);
    };
    MaisTableFormatterHelper.isLabelVisibleInTable = function (column, config) {
        var _a, _b;
        return column.showLabelInTable === true || column.showLabelInTable === false ?
            column.showLabelInTable : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultShowLabelInTable;
    };
    MaisTableFormatterHelper.lowercasePipe = new LowerCasePipe();
    MaisTableFormatterHelper.uppercasePipe = new UpperCasePipe();
    MaisTableFormatterHelper.transformMap = {
        UPPERCASE: function (raw, locale, args) { return MaisTableFormatterHelper.uppercasePipe.transform(raw); },
        LOWERCASE: function (raw, locale, args) { return MaisTableFormatterHelper.lowercasePipe.transform(raw); },
        DATE: function (raw, locale, args) {
            return new DatePipe(locale).transform(raw, args);
        },
        CURRENCY: function (raw, locale, args) {
            // signature: currencyCode?: string, display?: string | boolean, digitsInfo?: string, locale?: string
            if (!args) {
                args = {};
            }
            return new CurrencyPipe(locale).transform(raw, args.currencyCode, args.display, args.digitsInfo, locale);
        }
    };
    return MaisTableFormatterHelper;
}());
export { MaisTableFormatterHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1mb3JtYXR0ZXIudXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLWZvcm1hdHRlci51dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBT0EsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXZGLFdBQVc7QUFDWDtJQUFBO0lBd0dBLENBQUM7SUFyRmUseUNBQWdCLEdBQTlCLFVBQStCLE1BQVcsRUFBRSxLQUFhO1FBQ3ZELElBQUksTUFBTSxLQUFLLElBQUksSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO1lBQzNDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDN0IsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEI7UUFDRCxJQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QyxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRWEsK0JBQU0sR0FBcEIsVUFBcUIsR0FBUSxFQUFFLGFBQTRDLEVBQUUsTUFBZTtRQUMxRixJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ1IsT0FBTyxHQUFHLENBQUM7U0FDWjtRQUVELElBQU0sV0FBVyxHQUFHLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLHFCQUFxQixHQUFHLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNsRTtRQUVELE9BQU8sV0FBVyxDQUFDLEdBQUcsRUFBRSxNQUFNLElBQUksSUFBSSxFQUFFLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRWEscUNBQVksR0FBMUIsVUFBMkIsR0FBUSxFQUFFLE1BQXdCLEVBQUUsTUFBZTs7UUFDNUUsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNSLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFDRCxJQUFJLEdBQVEsQ0FBQztRQUNiLElBQUksTUFBTSxDQUFDLGNBQWMsRUFBRTtZQUN6QixHQUFHLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtZQUN2QixHQUFHLEdBQUcsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwRTthQUFNO1lBQ0wsR0FBRyxHQUFHLFNBQVMsQ0FBQztTQUNqQjtRQUVELElBQUksTUFBTSxDQUFDLFVBQVUsRUFBRTs7Z0JBQ3JCLEtBQXdCLElBQUEsS0FBQSxTQUFBLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7b0JBQWpHLElBQU0sU0FBUyxXQUFBO29CQUNsQixJQUFNLFNBQWlCLENBQUMsU0FBUyxFQUFHO3dCQUNsQyxHQUFHLEdBQUcsd0JBQXdCLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRyxTQUEyQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO3FCQUNsRzt5QkFBTSxJQUFNLFNBQWlCLENBQUMsTUFBTSxFQUFHO3dCQUN0QyxHQUFHLEdBQUksU0FBc0MsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQzNEO3lCQUFNO3dCQUNMLEdBQUcsR0FBRyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEVBQUUsU0FBUyxFQUFHLFNBQWdDLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3FCQUN2SDtpQkFDRjs7Ozs7Ozs7O1NBQ0Y7UUFFRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFYSwrQ0FBc0IsR0FBcEMsVUFBcUMsTUFBd0IsRUFBRSxNQUErQjs7UUFDNUYsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEtBQUssSUFBSSxJQUFJLE1BQU0sQ0FBQyxjQUFjLEtBQUssS0FBSyxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLGFBQUMsTUFBTSwwQ0FBRSxPQUFPLDBDQUFFLG1CQUFtQixDQUFDO1lBQzNELENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRWEsOENBQXFCLEdBQW5DLFVBQW9DLE1BQXdCLEVBQUUsTUFBK0I7O1FBQzNGLE9BQU8sTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLGFBQWEsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxhQUFDLE1BQU0sMENBQUUsT0FBTywwQ0FBRSxzQkFBc0IsQ0FBQztJQUMxSSxDQUFDO0lBRWEsbUNBQVUsR0FBeEIsVUFBeUIsTUFBd0IsRUFBRSxNQUErQjs7UUFDaEYsT0FBTyxNQUFNLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxNQUFNLENBQUMsT0FBTyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGFBQUMsTUFBTSwwQ0FBRSxPQUFPLDBDQUFFLGNBQWMsQ0FBQztJQUNoSCxDQUFDO0lBRWEsbUNBQVUsR0FBeEIsVUFBeUIsTUFBd0IsRUFBRSxNQUErQjs7UUFDaEYsT0FBTyxNQUFNLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxNQUFNLENBQUMsT0FBTyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGFBQUMsTUFBTSwwQ0FBRSxPQUFPLDBDQUFFLGNBQWMsQ0FBQztJQUNoSCxDQUFDO0lBRWEscUNBQVksR0FBMUIsVUFBMkIsTUFBd0IsRUFBRSxNQUErQjs7UUFDbEYsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLE1BQU0sQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsYUFBQyxNQUFNLDBDQUFFLE9BQU8sMENBQUUsZ0JBQWdCLENBQUM7WUFDckgsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRWEsaUNBQVEsR0FBdEIsVUFBdUIsTUFBd0IsRUFBRSxNQUErQjtRQUM5RSxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVhLDhDQUFxQixHQUFuQyxVQUFvQyxNQUF3QixFQUFFLE1BQStCOztRQUMzRixPQUFPLE1BQU0sQ0FBQyxnQkFBZ0IsS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLGdCQUFnQixLQUFLLEtBQUssQ0FBQyxDQUFDO1lBQzVFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLGFBQUMsTUFBTSwwQ0FBRSxPQUFPLDBDQUFFLHVCQUF1QixDQUFDO0lBQ3ZFLENBQUM7SUFyR2Msc0NBQWEsR0FBRyxJQUFJLGFBQWEsRUFBRSxDQUFDO0lBQ3BDLHNDQUFhLEdBQUcsSUFBSSxhQUFhLEVBQUUsQ0FBQztJQUVwQyxxQ0FBWSxHQUFHO1FBQzVCLFNBQVMsRUFBRSxVQUFDLEdBQVEsRUFBRSxNQUFjLEVBQUUsSUFBUyxJQUFLLE9BQUEsd0JBQXdCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBckQsQ0FBcUQ7UUFDekcsU0FBUyxFQUFFLFVBQUMsR0FBUSxFQUFFLE1BQWMsRUFBRSxJQUFTLElBQUssT0FBQSx3QkFBd0IsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFyRCxDQUFxRDtRQUN6RyxJQUFJLEVBQUUsVUFBQyxHQUFRLEVBQUUsTUFBYyxFQUFFLElBQVM7WUFDeEMsT0FBTyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ25ELENBQUM7UUFDRCxRQUFRLEVBQUUsVUFBQyxHQUFRLEVBQUUsTUFBYyxFQUFFLElBQVM7WUFDNUMscUdBQXFHO1lBQ3JHLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1QsSUFBSSxHQUFHLEVBQUUsQ0FBQzthQUNYO1lBQ0QsT0FBTyxJQUFJLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzNHLENBQUM7S0FDRixDQUFDO0lBdUZKLCtCQUFDO0NBQUEsQUF4R0QsSUF3R0M7U0F4R3FCLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgTWFpc1RhYmxlRm9ybWF0dGVyLFxyXG4gIElNYWlzVGFibGVDb2x1bW4sXHJcbiAgSU1haXNUYWJsZUZvcm1hdFNwZWNpZmljYXRpb24sXHJcbiAgSU1haXNUYWJsZUZvcm1hdFByb3ZpZGVyLFxyXG4gIElNYWlzVGFibGVDb25maWd1cmF0aW9uXHJcbn0gZnJvbSAnLi4vbW9kZWwnO1xyXG5pbXBvcnQgeyBMb3dlckNhc2VQaXBlLCBVcHBlckNhc2VQaXBlLCBEYXRlUGlwZSwgQ3VycmVuY3lQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbi8vIEBkeW5hbWljXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIge1xyXG4gIHByaXZhdGUgc3RhdGljIGxvd2VyY2FzZVBpcGUgPSBuZXcgTG93ZXJDYXNlUGlwZSgpO1xyXG4gIHByaXZhdGUgc3RhdGljIHVwcGVyY2FzZVBpcGUgPSBuZXcgVXBwZXJDYXNlUGlwZSgpO1xyXG5cclxuICBwcml2YXRlIHN0YXRpYyB0cmFuc2Zvcm1NYXAgPSB7XHJcbiAgICBVUFBFUkNBU0U6IChyYXc6IGFueSwgbG9jYWxlOiBzdHJpbmcsIGFyZ3M6IGFueSkgPT4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLnVwcGVyY2FzZVBpcGUudHJhbnNmb3JtKHJhdyksXHJcbiAgICBMT1dFUkNBU0U6IChyYXc6IGFueSwgbG9jYWxlOiBzdHJpbmcsIGFyZ3M6IGFueSkgPT4gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmxvd2VyY2FzZVBpcGUudHJhbnNmb3JtKHJhdyksXHJcbiAgICBEQVRFOiAocmF3OiBhbnksIGxvY2FsZTogc3RyaW5nLCBhcmdzOiBhbnkpID0+IHtcclxuICAgICAgcmV0dXJuIG5ldyBEYXRlUGlwZShsb2NhbGUpLnRyYW5zZm9ybShyYXcsIGFyZ3MpO1xyXG4gICAgfSxcclxuICAgIENVUlJFTkNZOiAocmF3OiBhbnksIGxvY2FsZTogc3RyaW5nLCBhcmdzOiBhbnkpID0+IHtcclxuICAgICAgLy8gc2lnbmF0dXJlOiBjdXJyZW5jeUNvZGU/OiBzdHJpbmcsIGRpc3BsYXk/OiBzdHJpbmcgfCBib29sZWFuLCBkaWdpdHNJbmZvPzogc3RyaW5nLCBsb2NhbGU/OiBzdHJpbmdcclxuICAgICAgaWYgKCFhcmdzKSB7XHJcbiAgICAgICAgYXJncyA9IHt9O1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBuZXcgQ3VycmVuY3lQaXBlKGxvY2FsZSkudHJhbnNmb3JtKHJhdywgYXJncy5jdXJyZW5jeUNvZGUsIGFyZ3MuZGlzcGxheSwgYXJncy5kaWdpdHNJbmZvLCBsb2NhbGUpO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgZ2V0UHJvcGVydHlWYWx1ZShvYmplY3Q6IGFueSwgZmllbGQ6IHN0cmluZyk6IHN0cmluZyB8IG51bGwge1xyXG4gICAgaWYgKG9iamVjdCA9PT0gbnVsbCB8fCBvYmplY3QgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIGlmIChmaWVsZC5pbmRleE9mKCcuJykgPT09IC0xKSB7XHJcbiAgICAgIHJldHVybiBvYmplY3RbZmllbGRdO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc3BsaXR0ZWQgPSBmaWVsZC5zcGxpdCgnLicpO1xyXG4gICAgY29uc3Qgc3ViT2JqZWN0ID0gb2JqZWN0W3NwbGl0dGVkWzBdXTtcclxuICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5VmFsdWUoc3ViT2JqZWN0LCBzcGxpdHRlZC5zbGljZSgxKS5qb2luKCcuJykpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBmb3JtYXQocmF3OiBhbnksIGZvcm1hdHRlclNwZWM6IElNYWlzVGFibGVGb3JtYXRTcGVjaWZpY2F0aW9uLCBsb2NhbGU/OiBzdHJpbmcpIHtcclxuICAgIGlmICghcmF3KSB7XHJcbiAgICAgIHJldHVybiByYXc7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgdHJhbnNmb3JtRm4gPSBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIudHJhbnNmb3JtTWFwW2Zvcm1hdHRlclNwZWMuZm9ybWF0dGVyXTtcclxuICAgIGlmICghdHJhbnNmb3JtRm4pIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbmtub3duIGZvcm1hdHRlcjogJyArIGZvcm1hdHRlclNwZWMuZm9ybWF0dGVyKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHJhbnNmb3JtRm4ocmF3LCBsb2NhbGUgfHwgJ2VuJywgZm9ybWF0dGVyU3BlYy5hcmd1bWVudHMpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBleHRyYWN0VmFsdWUocm93OiBhbnksIGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbiwgbG9jYWxlPzogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXJvdykge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIGxldCB2YWw6IGFueTtcclxuICAgIGlmIChjb2x1bW4udmFsdWVFeHRyYWN0b3IpIHtcclxuICAgICAgdmFsID0gY29sdW1uLnZhbHVlRXh0cmFjdG9yKHJvdyk7XHJcbiAgICB9IGVsc2UgaWYgKGNvbHVtbi5maWVsZCkge1xyXG4gICAgICB2YWwgPSBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuZ2V0UHJvcGVydHlWYWx1ZShyb3csIGNvbHVtbi5maWVsZCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2YWwgPSB1bmRlZmluZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNvbHVtbi5mb3JtYXR0ZXJzKSB7XHJcbiAgICAgIGZvciAoY29uc3QgZm9ybWF0dGVyIG9mIChBcnJheS5pc0FycmF5KGNvbHVtbi5mb3JtYXR0ZXJzKSA/IGNvbHVtbi5mb3JtYXR0ZXJzIDogW2NvbHVtbi5mb3JtYXR0ZXJzXSkpIHtcclxuICAgICAgICBpZiAoIChmb3JtYXR0ZXIgYXMgYW55KS5mb3JtYXR0ZXIgKSB7XHJcbiAgICAgICAgICB2YWwgPSBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuZm9ybWF0KHZhbCwgKGZvcm1hdHRlciBhcyBJTWFpc1RhYmxlRm9ybWF0U3BlY2lmaWNhdGlvbiksIGxvY2FsZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICggKGZvcm1hdHRlciBhcyBhbnkpLmZvcm1hdCApIHtcclxuICAgICAgICAgIHZhbCA9IChmb3JtYXR0ZXIgYXMgSU1haXNUYWJsZUZvcm1hdFByb3ZpZGVyKS5mb3JtYXQodmFsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdmFsID0gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmZvcm1hdCh2YWwsIHsgZm9ybWF0dGVyOiAoZm9ybWF0dGVyIGFzIE1haXNUYWJsZUZvcm1hdHRlciksIGFyZ3VtZW50czogbnVsbCB9LCBsb2NhbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB2YWw7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGlzRGVmYXVsdFZpc2libGVDb2x1bW4oY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uLCBjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKSB7XHJcbiAgICByZXR1cm4gKGNvbHVtbi5kZWZhdWx0VmlzaWJsZSA9PT0gdHJ1ZSB8fCBjb2x1bW4uZGVmYXVsdFZpc2libGUgPT09IGZhbHNlID9cclxuICAgICAgY29sdW1uLmRlZmF1bHRWaXNpYmxlIDogY29uZmlnPy5jb2x1bW5zPy5kZWZhdWx0SXNEZWZhdWxWaWV3KSB8fFxyXG4gICAgICAgICFNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjb2x1bW4sIGNvbmZpZyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGlzRGVmYXVsdEZpbHRlckNvbHVtbihjb2x1bW46IElNYWlzVGFibGVDb2x1bW4sIGNvbmZpZzogSU1haXNUYWJsZUNvbmZpZ3VyYXRpb24pIHtcclxuICAgIHJldHVybiBjb2x1bW4uZGVmYXVsdEZpbHRlciA9PT0gdHJ1ZSB8fCBjb2x1bW4uZGVmYXVsdEZpbHRlciA9PT0gZmFsc2UgPyBjb2x1bW4uZGVmYXVsdEZpbHRlciA6IGNvbmZpZz8uY29sdW1ucz8uZGVmYXVsdElzRGVmYXVsdEZpbHRlcjtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgaXNIaWRlYWJsZShjb2x1bW46IElNYWlzVGFibGVDb2x1bW4sIGNvbmZpZzogSU1haXNUYWJsZUNvbmZpZ3VyYXRpb24pOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcclxuICAgIHJldHVybiBjb2x1bW4uY2FuSGlkZSA9PT0gdHJ1ZSB8fCBjb2x1bW4uY2FuSGlkZSA9PT0gZmFsc2UgPyBjb2x1bW4uY2FuSGlkZSA6IGNvbmZpZz8uY29sdW1ucz8uZGVmYXVsdENhbkhpZGU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGlzU29ydGFibGUoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uLCBjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKSB7XHJcbiAgICByZXR1cm4gY29sdW1uLmNhblNvcnQgPT09IHRydWUgfHwgY29sdW1uLmNhblNvcnQgPT09IGZhbHNlID8gY29sdW1uLmNhblNvcnQgOiBjb25maWc/LmNvbHVtbnM/LmRlZmF1bHRDYW5Tb3J0O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBpc0ZpbHRlcmFibGUoY29sdW1uOiBJTWFpc1RhYmxlQ29sdW1uLCBjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKSB7XHJcbiAgICByZXR1cm4gKGNvbHVtbi5jYW5GaWx0ZXIgPT09IHRydWUgfHwgY29sdW1uLmNhbkZpbHRlciA9PT0gZmFsc2UgPyBjb2x1bW4uY2FuRmlsdGVyIDogY29uZmlnPy5jb2x1bW5zPy5kZWZhdWx0Q2FuRmlsdGVyKSAmJlxyXG4gICAgICAoY29sdW1uLnNlcnZlckZpZWxkIHx8IGNvbHVtbi5maWVsZCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGhhc0xhYmVsKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbiwgY29uZmlnOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbikge1xyXG4gICAgcmV0dXJuIChjb2x1bW4ubGFiZWxLZXkgfHwgY29sdW1uLmxhYmVsKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgaXNMYWJlbFZpc2libGVJblRhYmxlKGNvbHVtbjogSU1haXNUYWJsZUNvbHVtbiwgY29uZmlnOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbikge1xyXG4gICAgcmV0dXJuIGNvbHVtbi5zaG93TGFiZWxJblRhYmxlID09PSB0cnVlIHx8IGNvbHVtbi5zaG93TGFiZWxJblRhYmxlID09PSBmYWxzZSA/XHJcbiAgICAgIGNvbHVtbi5zaG93TGFiZWxJblRhYmxlIDogY29uZmlnPy5jb2x1bW5zPy5kZWZhdWx0U2hvd0xhYmVsSW5UYWJsZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==