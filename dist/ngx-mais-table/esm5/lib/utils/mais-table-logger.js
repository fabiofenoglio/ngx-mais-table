import { __read, __spread } from "tslib";
import { MaisTableLoggerService } from '../services/mais-table-logger.service';
var MaisTableLogger = /** @class */ (function () {
    function MaisTableLogger(componentName) {
        this.componentName = componentName;
        // NOP
        this.prefix = '[' + this.componentName + '] ';
    }
    MaisTableLogger.prototype.trace = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).trace.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.debug = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).debug.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.info = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).info.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.log = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).log.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.warn = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).warn.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.error = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).error.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.fatal = function (message) {
        var _a;
        var additional = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additional[_i - 1] = arguments[_i];
        }
        (_a = this.getEffectiveLogger()).fatal.apply(_a, __spread([this.prefix + message], additional));
    };
    MaisTableLogger.prototype.getEffectiveLogger = function () {
        return MaisTableLoggerService.getConfiguredLogAdapter();
    };
    return MaisTableLogger;
}());
export { MaisTableLogger };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1sb2dnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLWxvZ2dlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFFL0U7SUFJRSx5QkFBb0IsYUFBcUI7UUFBckIsa0JBQWEsR0FBYixhQUFhLENBQVE7UUFDdkMsTUFBTTtRQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQ2hELENBQUM7SUFFRCwrQkFBSyxHQUFMLFVBQU0sT0FBWTs7UUFBRSxvQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLG1DQUFvQjs7UUFDdEMsQ0FBQSxLQUFBLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBLENBQUMsS0FBSyxxQkFBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sR0FBSyxVQUFVLEdBQUU7SUFDeEUsQ0FBQztJQUVELCtCQUFLLEdBQUwsVUFBTSxPQUFZOztRQUFFLG9CQUFvQjthQUFwQixVQUFvQixFQUFwQixxQkFBb0IsRUFBcEIsSUFBb0I7WUFBcEIsbUNBQW9COztRQUN0QyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUEsQ0FBQyxLQUFLLHFCQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxHQUFLLFVBQVUsR0FBRTtJQUN4RSxDQUFDO0lBRUQsOEJBQUksR0FBSixVQUFLLE9BQVk7O1FBQUUsb0JBQW9CO2FBQXBCLFVBQW9CLEVBQXBCLHFCQUFvQixFQUFwQixJQUFvQjtZQUFwQixtQ0FBb0I7O1FBQ3JDLENBQUEsS0FBQSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQSxDQUFDLElBQUkscUJBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUssVUFBVSxHQUFFO0lBQ3ZFLENBQUM7SUFFRCw2QkFBRyxHQUFILFVBQUksT0FBWTs7UUFBRSxvQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLG1DQUFvQjs7UUFDcEMsQ0FBQSxLQUFBLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBLENBQUMsR0FBRyxxQkFBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sR0FBSyxVQUFVLEdBQUU7SUFDdEUsQ0FBQztJQUVELDhCQUFJLEdBQUosVUFBSyxPQUFZOztRQUFFLG9CQUFvQjthQUFwQixVQUFvQixFQUFwQixxQkFBb0IsRUFBcEIsSUFBb0I7WUFBcEIsbUNBQW9COztRQUNyQyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUEsQ0FBQyxJQUFJLHFCQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxHQUFLLFVBQVUsR0FBRTtJQUN2RSxDQUFDO0lBRUQsK0JBQUssR0FBTCxVQUFNLE9BQVk7O1FBQUUsb0JBQW9CO2FBQXBCLFVBQW9CLEVBQXBCLHFCQUFvQixFQUFwQixJQUFvQjtZQUFwQixtQ0FBb0I7O1FBQ3RDLENBQUEsS0FBQSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQSxDQUFDLEtBQUsscUJBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUssVUFBVSxHQUFFO0lBQ3hFLENBQUM7SUFFRCwrQkFBSyxHQUFMLFVBQU0sT0FBWTs7UUFBRSxvQkFBb0I7YUFBcEIsVUFBb0IsRUFBcEIscUJBQW9CLEVBQXBCLElBQW9CO1lBQXBCLG1DQUFvQjs7UUFDdEMsQ0FBQSxLQUFBLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBLENBQUMsS0FBSyxxQkFBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sR0FBSyxVQUFVLEdBQUU7SUFDeEUsQ0FBQztJQUVPLDRDQUFrQixHQUExQjtRQUNFLE9BQU8sc0JBQXNCLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztJQUMxRCxDQUFDO0lBQ0gsc0JBQUM7QUFBRCxDQUFDLEFBeENELElBd0NDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU1haXNUYWJsZUxvZ0FkYXB0ZXIgfSBmcm9tICcuL21haXMtdGFibGUtbG9nLWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbWFpcy10YWJsZS1sb2dnZXIuc2VydmljZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgTWFpc1RhYmxlTG9nZ2VyIHtcclxuXHJcbiAgcHJpdmF0ZSBwcmVmaXg6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb21wb25lbnROYW1lOiBzdHJpbmcpIHtcclxuICAgIC8vIE5PUFxyXG4gICAgdGhpcy5wcmVmaXggPSAnWycgKyB0aGlzLmNvbXBvbmVudE5hbWUgKyAnXSAnO1xyXG4gIH1cclxuXHJcbiAgdHJhY2UobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRFZmZlY3RpdmVMb2dnZXIoKS50cmFjZSh0aGlzLnByZWZpeCArIG1lc3NhZ2UsIC4uLmFkZGl0aW9uYWwpO1xyXG4gIH1cclxuXHJcbiAgZGVidWcobWVzc2FnZTogYW55LCAuLi5hZGRpdGlvbmFsOiBhbnlbXSk6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRFZmZlY3RpdmVMb2dnZXIoKS5kZWJ1Zyh0aGlzLnByZWZpeCArIG1lc3NhZ2UsIC4uLmFkZGl0aW9uYWwpO1xyXG4gIH1cclxuXHJcbiAgaW5mbyhtZXNzYWdlOiBhbnksIC4uLmFkZGl0aW9uYWw6IGFueVtdKTogdm9pZCB7XHJcbiAgICB0aGlzLmdldEVmZmVjdGl2ZUxvZ2dlcigpLmluZm8odGhpcy5wcmVmaXggKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGxvZyhtZXNzYWdlOiBhbnksIC4uLmFkZGl0aW9uYWw6IGFueVtdKTogdm9pZCB7XHJcbiAgICB0aGlzLmdldEVmZmVjdGl2ZUxvZ2dlcigpLmxvZyh0aGlzLnByZWZpeCArIG1lc3NhZ2UsIC4uLmFkZGl0aW9uYWwpO1xyXG4gIH1cclxuXHJcbiAgd2FybihtZXNzYWdlOiBhbnksIC4uLmFkZGl0aW9uYWw6IGFueVtdKTogdm9pZCB7XHJcbiAgICB0aGlzLmdldEVmZmVjdGl2ZUxvZ2dlcigpLndhcm4odGhpcy5wcmVmaXggKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGVycm9yKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0RWZmZWN0aXZlTG9nZ2VyKCkuZXJyb3IodGhpcy5wcmVmaXggKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIGZhdGFsKG1lc3NhZ2U6IGFueSwgLi4uYWRkaXRpb25hbDogYW55W10pOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0RWZmZWN0aXZlTG9nZ2VyKCkuZmF0YWwodGhpcy5wcmVmaXggKyBtZXNzYWdlLCAuLi5hZGRpdGlvbmFsKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0RWZmZWN0aXZlTG9nZ2VyKCk6IElNYWlzVGFibGVMb2dBZGFwdGVyIHtcclxuICAgIHJldHVybiBNYWlzVGFibGVMb2dnZXJTZXJ2aWNlLmdldENvbmZpZ3VyZWRMb2dBZGFwdGVyKCk7XHJcbiAgfVxyXG59XHJcblxyXG4iXX0=