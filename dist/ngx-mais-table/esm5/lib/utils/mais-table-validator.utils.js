import { __values } from "tslib";
import { MaisTableFormatterHelper } from './mais-table-formatter.utils';
var MaisTableValidatorHelper = /** @class */ (function () {
    function MaisTableValidatorHelper() {
    }
    MaisTableValidatorHelper.validateColumnsSpecification = function (columns, config) {
        var err = MaisTableValidatorHelper._validateColumnsSpecification(columns, config);
        if (err) {
            throw new Error(err);
        }
        return null;
    };
    MaisTableValidatorHelper._validateColumnsSpecification = function (columns, config) {
        var e_1, _a, e_2, _b;
        if (!columns || !columns.length) {
            return 'A non-empty list of columns must be provided';
        }
        try {
            for (var columns_1 = __values(columns), columns_1_1 = columns_1.next(); !columns_1_1.done; columns_1_1 = columns_1.next()) {
                var col = columns_1_1.value;
                if (!col.name) {
                    return 'Columns need to have a non-empty name.';
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (columns_1_1 && !columns_1_1.done && (_a = columns_1.return)) _a.call(columns_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        try {
            for (var columns_2 = __values(columns), columns_2_1 = columns_2.next(); !columns_2_1.done; columns_2_1 = columns_2.next()) {
                var col = columns_2_1.value;
                if ((MaisTableFormatterHelper.isFilterable(col, config) || MaisTableFormatterHelper.isHideable(col, config))
                    && !MaisTableFormatterHelper.hasLabel(col, config)) {
                    return 'Columns to be filtered or toggled must have either a labelKey or a label. ' +
                        'To hide a column label use the showLabelInTable property. Offending column is [' + col.name + ']';
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (columns_2_1 && !columns_2_1.done && (_b = columns_2.return)) _b.call(columns_2);
            }
            finally { if (e_2) throw e_2.error; }
        }
        if (columns.filter(function (c) { return !MaisTableFormatterHelper.isHideable(c, config); }).length < 1) {
            return 'At least one of the columns must be not hideable: set canHide=false on primary or relevant column.';
        }
        return null;
    };
    return MaisTableValidatorHelper;
}());
export { MaisTableValidatorHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS12YWxpZGF0b3IudXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtbWFpcy10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYWlzLXRhYmxlLXZhbGlkYXRvci51dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFeEU7SUFBQTtJQW1DQSxDQUFDO0lBakNlLHFEQUE0QixHQUExQyxVQUEyQyxPQUEyQixFQUFFLE1BQStCO1FBQ3JHLElBQU0sR0FBRyxHQUFHLHdCQUF3QixDQUFDLDZCQUE2QixDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNwRixJQUFJLEdBQUcsRUFBRTtZQUNQLE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDdEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFYyxzREFBNkIsR0FBNUMsVUFBNkMsT0FBMkIsRUFBRSxNQUErQjs7UUFDdkcsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsT0FBTyw4Q0FBOEMsQ0FBQztTQUN2RDs7WUFFRCxLQUFrQixJQUFBLFlBQUEsU0FBQSxPQUFPLENBQUEsZ0NBQUEscURBQUU7Z0JBQXRCLElBQU0sR0FBRyxvQkFBQTtnQkFDWixJQUFLLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRztvQkFDZixPQUFPLHdDQUF3QyxDQUFDO2lCQUNqRDthQUNGOzs7Ozs7Ozs7O1lBRUQsS0FBa0IsSUFBQSxZQUFBLFNBQUEsT0FBTyxDQUFBLGdDQUFBLHFEQUFFO2dCQUF0QixJQUFNLEdBQUcsb0JBQUE7Z0JBQ1osSUFBSyxDQUFDLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLElBQUksd0JBQXdCLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBRTt1QkFDM0csQ0FBQyx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUFHO29CQUNuRCxPQUFPLDRFQUE0RTt3QkFDakYsaUZBQWlGLEdBQUcsR0FBRyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7aUJBQ3RHO2FBQ0Y7Ozs7Ozs7OztRQUVELElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkYsT0FBTyxvR0FBb0csQ0FBQztTQUM3RztRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNILCtCQUFDO0FBQUQsQ0FBQyxBQW5DRCxJQW1DQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNYWlzVGFibGVDb2x1bW4sIElNYWlzVGFibGVDb25maWd1cmF0aW9uIH0gZnJvbSAnLi4vbW9kZWwnO1xyXG5pbXBvcnQgeyBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIgfSBmcm9tICcuL21haXMtdGFibGUtZm9ybWF0dGVyLnV0aWxzJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBNYWlzVGFibGVWYWxpZGF0b3JIZWxwZXIge1xyXG5cclxuICBwdWJsaWMgc3RhdGljIHZhbGlkYXRlQ29sdW1uc1NwZWNpZmljYXRpb24oY29sdW1uczogSU1haXNUYWJsZUNvbHVtbltdLCBjb25maWc6IElNYWlzVGFibGVDb25maWd1cmF0aW9uKSB7XHJcbiAgICBjb25zdCBlcnIgPSBNYWlzVGFibGVWYWxpZGF0b3JIZWxwZXIuX3ZhbGlkYXRlQ29sdW1uc1NwZWNpZmljYXRpb24oY29sdW1ucywgY29uZmlnKTtcclxuICAgIGlmIChlcnIpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbnVsbDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIF92YWxpZGF0ZUNvbHVtbnNTcGVjaWZpY2F0aW9uKGNvbHVtbnM6IElNYWlzVGFibGVDb2x1bW5bXSwgY29uZmlnOiBJTWFpc1RhYmxlQ29uZmlndXJhdGlvbikge1xyXG4gICAgaWYgKCFjb2x1bW5zIHx8ICFjb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgICByZXR1cm4gJ0Egbm9uLWVtcHR5IGxpc3Qgb2YgY29sdW1ucyBtdXN0IGJlIHByb3ZpZGVkJztcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGNvbnN0IGNvbCBvZiBjb2x1bW5zKSB7XHJcbiAgICAgIGlmICggIWNvbC5uYW1lICkge1xyXG4gICAgICAgIHJldHVybiAnQ29sdW1ucyBuZWVkIHRvIGhhdmUgYSBub24tZW1wdHkgbmFtZS4nO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChjb25zdCBjb2wgb2YgY29sdW1ucykge1xyXG4gICAgICBpZiAoIChNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNGaWx0ZXJhYmxlKGNvbCwgY29uZmlnKSB8fCBNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjb2wsIGNvbmZpZykgKVxyXG4gICAgICAmJiAhTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmhhc0xhYmVsKGNvbCwgY29uZmlnKSApIHtcclxuICAgICAgICByZXR1cm4gJ0NvbHVtbnMgdG8gYmUgZmlsdGVyZWQgb3IgdG9nZ2xlZCBtdXN0IGhhdmUgZWl0aGVyIGEgbGFiZWxLZXkgb3IgYSBsYWJlbC4gJyArXHJcbiAgICAgICAgICAnVG8gaGlkZSBhIGNvbHVtbiBsYWJlbCB1c2UgdGhlIHNob3dMYWJlbEluVGFibGUgcHJvcGVydHkuIE9mZmVuZGluZyBjb2x1bW4gaXMgWycgKyBjb2wubmFtZSArICddJztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjb2x1bW5zLmZpbHRlcihjID0+ICFNYWlzVGFibGVGb3JtYXR0ZXJIZWxwZXIuaXNIaWRlYWJsZShjLCBjb25maWcpKS5sZW5ndGggPCAxKSB7XHJcbiAgICAgIHJldHVybiAnQXQgbGVhc3Qgb25lIG9mIHRoZSBjb2x1bW5zIG11c3QgYmUgbm90IGhpZGVhYmxlOiBzZXQgY2FuSGlkZT1mYWxzZSBvbiBwcmltYXJ5IG9yIHJlbGV2YW50IGNvbHVtbi4nO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxufVxyXG4iXX0=