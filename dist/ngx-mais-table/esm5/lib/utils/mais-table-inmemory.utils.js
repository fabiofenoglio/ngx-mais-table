import { __values } from "tslib";
import { MaisTableSortDirection } from '../model';
import { MaisTableFormatterHelper } from './mais-table-formatter.utils';
var MaisTableInMemoryHelper = /** @class */ (function () {
    function MaisTableInMemoryHelper() {
    }
    MaisTableInMemoryHelper.fetchInMemory = function (data, request, sortColumn, filteringColumns, locale) {
        var output = {
            content: [],
            size: 0,
            totalElements: 0,
            totalPages: 0
        };
        if (!data || !data.length) {
            return output;
        }
        // apply filtering
        var filtered = data;
        if (request.query && request.query.length && request.query.trim().length
            && request.queryFields && request.queryFields.length) {
            var cleanedSearchQuery_1 = request.query.trim().toLowerCase();
            filtered = filtered.filter(function (candidateRow) {
                var e_1, _a;
                var allowed = false;
                if (filteringColumns) {
                    try {
                        for (var filteringColumns_1 = __values(filteringColumns), filteringColumns_1_1 = filteringColumns_1.next(); !filteringColumns_1_1.done; filteringColumns_1_1 = filteringColumns_1.next()) {
                            var filteringColumn = filteringColumns_1_1.value;
                            var extractedValue = MaisTableFormatterHelper.extractValue(candidateRow, filteringColumn, locale);
                            if (extractedValue) {
                                var extractedValueStr = extractedValue + '';
                                if (extractedValueStr.toLowerCase().indexOf(cleanedSearchQuery_1) !== -1) {
                                    allowed = true;
                                    break;
                                }
                            }
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (filteringColumns_1_1 && !filteringColumns_1_1.done && (_a = filteringColumns_1.return)) _a.call(filteringColumns_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
                return allowed;
            });
        }
        // apply pagination
        var startingIndex;
        var endingIndex;
        if (request && request.page != null && request.size != null &&
            (typeof request.page !== 'undefined') && (typeof request.size !== 'undefined') && (request.page === 0 || request.page > 0)) {
            startingIndex = request.page * request.size;
            endingIndex = (request.page + 1) * request.size;
            if (startingIndex >= filtered.length) {
                return output;
            }
            if (endingIndex > filtered.length) {
                endingIndex = filtered.length;
            }
        }
        else {
            startingIndex = 0;
            endingIndex = filtered.length;
        }
        if (request.sort && request.sort.length && request.sort[0]) {
            var sortDirection_1 = request.sort[0].direction === MaisTableSortDirection.DESCENDING ? -1 : +1;
            if (sortColumn) {
                filtered.sort(function (c1, c2) {
                    var v1 = MaisTableFormatterHelper.extractValue(c1, sortColumn, locale) || '';
                    var v2 = MaisTableFormatterHelper.extractValue(c2, sortColumn, locale) || '';
                    var v1n = (v1 === null || v1 === undefined || Number.isNaN(v1));
                    var v2n = (v2 === null || v2 === undefined || Number.isNaN(v2));
                    if (v1n && v2n) {
                        return 0;
                    }
                    else if (v1n && !v2n) {
                        return -1 * sortDirection_1;
                    }
                    else if (!v1n && v2n) {
                        return 1 * sortDirection_1;
                    }
                    else {
                        return (v1 > v2 ? 1 : v1 < v2 ? -1 : 0) * sortDirection_1;
                    }
                });
            }
        }
        var fetchedPage = filtered.slice(startingIndex, endingIndex);
        output.content = fetchedPage;
        output.size = fetchedPage.length;
        output.totalElements = filtered.length;
        if (request.size) {
            output.totalPages = Math.ceil(filtered.length);
        }
        else {
            output.totalPages = 1;
        }
        return output;
    };
    return MaisTableInMemoryHelper;
}());
export { MaisTableInMemoryHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpcy10YWJsZS1pbm1lbW9yeS51dGlscy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYWlzLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL21haXMtdGFibGUtaW5tZW1vcnkudXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBbUUsc0JBQXNCLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDbkgsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFeEU7SUFBQTtJQWdHQSxDQUFDO0lBOUZlLHFDQUFhLEdBQTNCLFVBQ0UsSUFBVyxFQUNYLE9BQThCLEVBQzlCLFVBQW9DLEVBQ3BDLGdCQUFxQyxFQUNyQyxNQUFlO1FBQ2YsSUFBTSxNQUFNLEdBQTJCO1lBQ3JDLE9BQU8sRUFBRSxFQUFFO1lBQ1gsSUFBSSxFQUFFLENBQUM7WUFDUCxhQUFhLEVBQUUsQ0FBQztZQUNoQixVQUFVLEVBQUUsQ0FBQztTQUNkLENBQUM7UUFFRixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN6QixPQUFPLE1BQU0sQ0FBQztTQUNmO1FBRUQsa0JBQWtCO1FBQ2xCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNO2VBQ25FLE9BQU8sQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7WUFFdEQsSUFBTSxvQkFBa0IsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzlELFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsWUFBWTs7Z0JBQ3JDLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDcEIsSUFBSSxnQkFBZ0IsRUFBRTs7d0JBQ3BCLEtBQThCLElBQUEscUJBQUEsU0FBQSxnQkFBZ0IsQ0FBQSxrREFBQSxnRkFBRTs0QkFBM0MsSUFBTSxlQUFlLDZCQUFBOzRCQUN4QixJQUFNLGNBQWMsR0FBRyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLENBQUMsQ0FBQzs0QkFDcEcsSUFBSSxjQUFjLEVBQUU7Z0NBQ2xCLElBQU0saUJBQWlCLEdBQUcsY0FBYyxHQUFHLEVBQUUsQ0FBQztnQ0FDOUMsSUFBSSxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsb0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtvQ0FDdEUsT0FBTyxHQUFHLElBQUksQ0FBQztvQ0FDZixNQUFNO2lDQUNQOzZCQUNGO3lCQUNGOzs7Ozs7Ozs7aUJBQ0Y7Z0JBQ0QsT0FBTyxPQUFPLENBQUM7WUFDakIsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELG1CQUFtQjtRQUNuQixJQUFJLGFBQWEsQ0FBQztRQUNsQixJQUFJLFdBQVcsQ0FBQztRQUVoQixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUk7WUFDekQsQ0FBQyxPQUFPLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQzVILGFBQWEsR0FBRyxPQUFPLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDNUMsV0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2hELElBQUksYUFBYSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUc7Z0JBQ3JDLE9BQU8sTUFBTSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLFdBQVcsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFHO2dCQUNsQyxXQUFXLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQzthQUMvQjtTQUNGO2FBQU07WUFDTCxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLFdBQVcsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO1NBQy9CO1FBRUQsSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDMUQsSUFBTSxlQUFhLEdBQVcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEcsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFDLEVBQUUsRUFBRSxFQUFFO29CQUNuQixJQUFNLEVBQUUsR0FBRyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQy9FLElBQU0sRUFBRSxHQUFHLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDL0UsSUFBTSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEtBQUssSUFBSSxJQUFJLEVBQUUsS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNsRSxJQUFNLEdBQUcsR0FBRyxDQUFDLEVBQUUsS0FBSyxJQUFJLElBQUksRUFBRSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2xFLElBQUksR0FBRyxJQUFJLEdBQUcsRUFBRTt3QkFDZCxPQUFPLENBQUMsQ0FBQztxQkFDVjt5QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDdEIsT0FBTyxDQUFDLENBQUMsR0FBRyxlQUFhLENBQUM7cUJBQzNCO3lCQUFNLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxFQUFFO3dCQUN0QixPQUFPLENBQUMsR0FBRyxlQUFhLENBQUM7cUJBQzFCO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBSSxlQUFhLENBQUM7cUJBQzFEO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjtRQUVELElBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9ELE1BQU0sQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztRQUNqQyxNQUFNLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDdkMsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDaEQ7YUFBTTtZQUNMLE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUNILDhCQUFDO0FBQUQsQ0FBQyxBQWhHRCxJQWdHQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNYWlzVGFibGVDb2x1bW4sIElNYWlzVGFibGVQYWdlUmVzcG9uc2UsIElNYWlzVGFibGVQYWdlUmVxdWVzdCwgTWFpc1RhYmxlU29ydERpcmVjdGlvbiB9IGZyb20gJy4uL21vZGVsJztcclxuaW1wb3J0IHsgTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyIH0gZnJvbSAnLi9tYWlzLXRhYmxlLWZvcm1hdHRlci51dGlscyc7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgTWFpc1RhYmxlSW5NZW1vcnlIZWxwZXIge1xyXG5cclxuICBwdWJsaWMgc3RhdGljIGZldGNoSW5NZW1vcnkoXHJcbiAgICBkYXRhOiBhbnlbXSxcclxuICAgIHJlcXVlc3Q6IElNYWlzVGFibGVQYWdlUmVxdWVzdCxcclxuICAgIHNvcnRDb2x1bW4/OiBJTWFpc1RhYmxlQ29sdW1uIHwgbnVsbCxcclxuICAgIGZpbHRlcmluZ0NvbHVtbnM/OiBJTWFpc1RhYmxlQ29sdW1uW10sXHJcbiAgICBsb2NhbGU/OiBzdHJpbmcgKTogSU1haXNUYWJsZVBhZ2VSZXNwb25zZSB7XHJcbiAgICBjb25zdCBvdXRwdXQ6IElNYWlzVGFibGVQYWdlUmVzcG9uc2UgPSB7XHJcbiAgICAgIGNvbnRlbnQ6IFtdLFxyXG4gICAgICBzaXplOiAwLFxyXG4gICAgICB0b3RhbEVsZW1lbnRzOiAwLFxyXG4gICAgICB0b3RhbFBhZ2VzOiAwXHJcbiAgICB9O1xyXG5cclxuICAgIGlmICghZGF0YSB8fCAhZGF0YS5sZW5ndGgpIHtcclxuICAgICAgcmV0dXJuIG91dHB1dDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBhcHBseSBmaWx0ZXJpbmdcclxuICAgIGxldCBmaWx0ZXJlZCA9IGRhdGE7XHJcbiAgICBpZiAocmVxdWVzdC5xdWVyeSAmJiByZXF1ZXN0LnF1ZXJ5Lmxlbmd0aCAmJiByZXF1ZXN0LnF1ZXJ5LnRyaW0oKS5sZW5ndGhcclxuICAgICAgJiYgcmVxdWVzdC5xdWVyeUZpZWxkcyAmJiByZXF1ZXN0LnF1ZXJ5RmllbGRzLmxlbmd0aCkge1xyXG5cclxuICAgICAgY29uc3QgY2xlYW5lZFNlYXJjaFF1ZXJ5ID0gcmVxdWVzdC5xdWVyeS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgZmlsdGVyZWQgPSBmaWx0ZXJlZC5maWx0ZXIoY2FuZGlkYXRlUm93ID0+IHtcclxuICAgICAgICBsZXQgYWxsb3dlZCA9IGZhbHNlO1xyXG4gICAgICAgIGlmIChmaWx0ZXJpbmdDb2x1bW5zKSB7XHJcbiAgICAgICAgICBmb3IgKGNvbnN0IGZpbHRlcmluZ0NvbHVtbiBvZiBmaWx0ZXJpbmdDb2x1bW5zKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV4dHJhY3RlZFZhbHVlID0gTWFpc1RhYmxlRm9ybWF0dGVySGVscGVyLmV4dHJhY3RWYWx1ZShjYW5kaWRhdGVSb3csIGZpbHRlcmluZ0NvbHVtbiwgbG9jYWxlKTtcclxuICAgICAgICAgICAgaWYgKGV4dHJhY3RlZFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgZXh0cmFjdGVkVmFsdWVTdHIgPSBleHRyYWN0ZWRWYWx1ZSArICcnO1xyXG4gICAgICAgICAgICAgIGlmIChleHRyYWN0ZWRWYWx1ZVN0ci50b0xvd2VyQ2FzZSgpLmluZGV4T2YoY2xlYW5lZFNlYXJjaFF1ZXJ5KSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgIGFsbG93ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhbGxvd2VkO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBhcHBseSBwYWdpbmF0aW9uXHJcbiAgICBsZXQgc3RhcnRpbmdJbmRleDtcclxuICAgIGxldCBlbmRpbmdJbmRleDtcclxuXHJcbiAgICBpZiAocmVxdWVzdCAmJiByZXF1ZXN0LnBhZ2UgIT0gbnVsbCAmJiByZXF1ZXN0LnNpemUgIT0gbnVsbCAmJlxyXG4gICAgICAodHlwZW9mIHJlcXVlc3QucGFnZSAhPT0gJ3VuZGVmaW5lZCcpICYmICh0eXBlb2YgcmVxdWVzdC5zaXplICE9PSAndW5kZWZpbmVkJykgJiYgKHJlcXVlc3QucGFnZSA9PT0gMCB8fCByZXF1ZXN0LnBhZ2UgPiAwKSkge1xyXG4gICAgICBzdGFydGluZ0luZGV4ID0gcmVxdWVzdC5wYWdlICogcmVxdWVzdC5zaXplO1xyXG4gICAgICBlbmRpbmdJbmRleCA9IChyZXF1ZXN0LnBhZ2UgKyAxKSAqIHJlcXVlc3Quc2l6ZTtcclxuICAgICAgaWYgKHN0YXJ0aW5nSW5kZXggPj0gZmlsdGVyZWQubGVuZ3RoICkge1xyXG4gICAgICAgIHJldHVybiBvdXRwdXQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChlbmRpbmdJbmRleCA+IGZpbHRlcmVkLmxlbmd0aCApIHtcclxuICAgICAgICBlbmRpbmdJbmRleCA9IGZpbHRlcmVkLmxlbmd0aDtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc3RhcnRpbmdJbmRleCA9IDA7XHJcbiAgICAgIGVuZGluZ0luZGV4ID0gZmlsdGVyZWQubGVuZ3RoO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChyZXF1ZXN0LnNvcnQgJiYgcmVxdWVzdC5zb3J0Lmxlbmd0aCAmJiByZXF1ZXN0LnNvcnRbMF0pIHtcclxuICAgICAgY29uc3Qgc29ydERpcmVjdGlvbjogbnVtYmVyID0gcmVxdWVzdC5zb3J0WzBdLmRpcmVjdGlvbiA9PT0gTWFpc1RhYmxlU29ydERpcmVjdGlvbi5ERVNDRU5ESU5HID8gLTEgOiArMTtcclxuXHJcbiAgICAgIGlmIChzb3J0Q29sdW1uKSB7XHJcbiAgICAgICAgZmlsdGVyZWQuc29ydCgoYzEsIGMyKSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB2MSA9IE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5leHRyYWN0VmFsdWUoYzEsIHNvcnRDb2x1bW4sIGxvY2FsZSkgfHwgJyc7XHJcbiAgICAgICAgICBjb25zdCB2MiA9IE1haXNUYWJsZUZvcm1hdHRlckhlbHBlci5leHRyYWN0VmFsdWUoYzIsIHNvcnRDb2x1bW4sIGxvY2FsZSkgfHwgJyc7XHJcbiAgICAgICAgICBjb25zdCB2MW4gPSAodjEgPT09IG51bGwgfHwgdjEgPT09IHVuZGVmaW5lZCB8fCBOdW1iZXIuaXNOYU4odjEpKTtcclxuICAgICAgICAgIGNvbnN0IHYybiA9ICh2MiA9PT0gbnVsbCB8fCB2MiA9PT0gdW5kZWZpbmVkIHx8IE51bWJlci5pc05hTih2MikpO1xyXG4gICAgICAgICAgaWYgKHYxbiAmJiB2Mm4pIHtcclxuICAgICAgICAgICAgcmV0dXJuIDA7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHYxbiAmJiAhdjJuKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAtMSAqIHNvcnREaXJlY3Rpb247XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKCF2MW4gJiYgdjJuKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAxICogc29ydERpcmVjdGlvbjtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAodjEgPiB2MiA/IDEgOiB2MSA8IHYyID8gLTEgOiAwKSAgKiBzb3J0RGlyZWN0aW9uO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgZmV0Y2hlZFBhZ2UgPSBmaWx0ZXJlZC5zbGljZShzdGFydGluZ0luZGV4LCBlbmRpbmdJbmRleCk7XHJcbiAgICBvdXRwdXQuY29udGVudCA9IGZldGNoZWRQYWdlO1xyXG4gICAgb3V0cHV0LnNpemUgPSBmZXRjaGVkUGFnZS5sZW5ndGg7XHJcbiAgICBvdXRwdXQudG90YWxFbGVtZW50cyA9IGZpbHRlcmVkLmxlbmd0aDtcclxuICAgIGlmIChyZXF1ZXN0LnNpemUpIHtcclxuICAgICAgb3V0cHV0LnRvdGFsUGFnZXMgPSBNYXRoLmNlaWwoZmlsdGVyZWQubGVuZ3RoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG91dHB1dC50b3RhbFBhZ2VzID0gMTtcclxuICAgIH1cclxuICAgIHJldHVybiBvdXRwdXQ7XHJcbiAgfVxyXG59XHJcbiJdfQ==