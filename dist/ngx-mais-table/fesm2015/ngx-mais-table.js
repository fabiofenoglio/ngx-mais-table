import { ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵinject, ɵɵgetCurrentView, ɵɵelementStart, ɵɵlistener, ɵɵrestoreView, ɵɵnextContext, ɵɵelementEnd, ɵɵproperty, ɵɵelement, ɵɵtemplate, ɵɵtext, ɵɵclassProp, ɵɵadvance, ɵɵtextInterpolate1, ɵɵelementContainerStart, ɵɵelementContainerEnd, ɵɵpipe, ɵɵtextInterpolate, ɵɵpipeBind1, ɵɵpropertyInterpolate, ɵɵclassMapInterpolate2, ɵɵpureFunction0, ɵɵpureFunction3, ɵɵattribute, ɵɵpureFunction1, ɵɵclassMapInterpolate1, ɵɵclassMap, ɵɵpipeBind2, ɵɵpureFunction2, EventEmitter, ɵɵdirectiveInject, ChangeDetectorRef, NgZone, ɵɵdefineComponent, ɵɵcontentQuery, ɵɵqueryRefresh, ɵɵloadQuery, ɵɵviewQuery, ɵɵNgOnChangesFeature, Component, Input, Output, ContentChild, ViewChild, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { detect } from 'detect-browser';
import { LowerCasePipe, UpperCasePipe, DatePipe, CurrencyPipe, NgIf, NgTemplateOutlet, NgForOf, CommonModule } from '@angular/common';
import { Subject, Observable, timer, throwError } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { TranslateService, TranslatePipe, TranslateModule } from '@ngx-translate/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import $ from 'jquery';
import { MatTable, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatHeaderCell, MatCell, MatHeaderRow, MatRow, MatTableModule } from '@angular/material/table';
import { NgbDropdown, NgbDropdownToggle, NgbDropdownMenu, NgbDropdownItem, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CdkDropList, CdkDrag, CdkDragPlaceholder, CdkDragPreview, DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTableModule } from '@angular/cdk/table';

class MaisTableBrowserHelper {
    static getBrowser() {
        var _a;
        const name = (_a = this.browser) === null || _a === void 0 ? void 0 : _a.name;
        if (name === Browser.CHROME ||
            name === Browser.FIREFOX ||
            name === Browser.EDGE ||
            name === Browser.IE ||
            name === Browser.OPERA ||
            name === Browser.SAFARI) {
            return name;
        }
        else {
            return Browser.OTHER;
        }
    }
    static isIE() {
        return MaisTableBrowserHelper.getBrowser() === Browser.IE;
    }
}
MaisTableBrowserHelper.browser = detect();
var Browser;
(function (Browser) {
    Browser["EDGE"] = "edge";
    Browser["OPERA"] = "opera";
    Browser["CHROME"] = "chrome";
    Browser["IE"] = "ie";
    Browser["FIREFOX"] = "firefox";
    Browser["SAFARI"] = "safari";
    Browser["OTHER"] = "other";
})(Browser || (Browser = {}));

// @dynamic
class MaisTableFormatterHelper {
    static getPropertyValue(object, field) {
        if (object === null || object === undefined) {
            return null;
        }
        if (field.indexOf('.') === -1) {
            return object[field];
        }
        const splitted = field.split('.');
        const subObject = object[splitted[0]];
        return this.getPropertyValue(subObject, splitted.slice(1).join('.'));
    }
    static format(raw, formatterSpec, locale) {
        if (!raw) {
            return raw;
        }
        const transformFn = MaisTableFormatterHelper.transformMap[formatterSpec.formatter];
        if (!transformFn) {
            throw new Error('Unknown formatter: ' + formatterSpec.formatter);
        }
        return transformFn(raw, locale || 'en', formatterSpec.arguments);
    }
    static extractValue(row, column, locale) {
        if (!row) {
            return null;
        }
        let val;
        if (column.valueExtractor) {
            val = column.valueExtractor(row);
        }
        else if (column.field) {
            val = MaisTableFormatterHelper.getPropertyValue(row, column.field);
        }
        else {
            val = undefined;
        }
        if (column.formatters) {
            for (const formatter of (Array.isArray(column.formatters) ? column.formatters : [column.formatters])) {
                if (formatter.formatter) {
                    val = MaisTableFormatterHelper.format(val, formatter, locale);
                }
                else if (formatter.format) {
                    val = formatter.format(val);
                }
                else {
                    val = MaisTableFormatterHelper.format(val, { formatter: formatter, arguments: null }, locale);
                }
            }
        }
        return val;
    }
    static isDefaultVisibleColumn(column, config) {
        var _a, _b;
        return (column.defaultVisible === true || column.defaultVisible === false ?
            column.defaultVisible : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaulView) ||
            !MaisTableFormatterHelper.isHideable(column, config);
    }
    static isDefaultFilterColumn(column, config) {
        var _a, _b;
        return column.defaultFilter === true || column.defaultFilter === false ? column.defaultFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaultFilter;
    }
    static isHideable(column, config) {
        var _a, _b;
        return column.canHide === true || column.canHide === false ? column.canHide : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanHide;
    }
    static isSortable(column, config) {
        var _a, _b;
        return column.canSort === true || column.canSort === false ? column.canSort : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanSort;
    }
    static isFilterable(column, config) {
        var _a, _b;
        return (column.canFilter === true || column.canFilter === false ? column.canFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanFilter) &&
            (column.serverField || column.field);
    }
    static hasLabel(column, config) {
        return (column.labelKey || column.label);
    }
    static isLabelVisibleInTable(column, config) {
        var _a, _b;
        return column.showLabelInTable === true || column.showLabelInTable === false ?
            column.showLabelInTable : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultShowLabelInTable;
    }
}
MaisTableFormatterHelper.lowercasePipe = new LowerCasePipe();
MaisTableFormatterHelper.uppercasePipe = new UpperCasePipe();
MaisTableFormatterHelper.transformMap = {
    UPPERCASE: (raw, locale, args) => MaisTableFormatterHelper.uppercasePipe.transform(raw),
    LOWERCASE: (raw, locale, args) => MaisTableFormatterHelper.lowercasePipe.transform(raw),
    DATE: (raw, locale, args) => {
        return new DatePipe(locale).transform(raw, args);
    },
    CURRENCY: (raw, locale, args) => {
        // signature: currencyCode?: string, display?: string | boolean, digitsInfo?: string, locale?: string
        if (!args) {
            args = {};
        }
        return new CurrencyPipe(locale).transform(raw, args.currencyCode, args.display, args.digitsInfo, locale);
    }
};

var MaisTableSortDirection;
(function (MaisTableSortDirection) {
    MaisTableSortDirection["ASCENDING"] = "ASC";
    MaisTableSortDirection["DESCENDING"] = "DESC";
})(MaisTableSortDirection || (MaisTableSortDirection = {}));
var MaisTablePaginationMethod;
(function (MaisTablePaginationMethod) {
    MaisTablePaginationMethod["CLIENT"] = "CLIENT";
    MaisTablePaginationMethod["SERVER"] = "SERVER";
})(MaisTablePaginationMethod || (MaisTablePaginationMethod = {}));
var MaisTableFormatter;
(function (MaisTableFormatter) {
    MaisTableFormatter["UPPERCASE"] = "UPPERCASE";
    MaisTableFormatter["LOWERCASE"] = "LOWERCASE";
    MaisTableFormatter["DATE"] = "DATE";
    MaisTableFormatter["CURRENCY"] = "CURRENCY";
})(MaisTableFormatter || (MaisTableFormatter = {}));
var MaisTableRefreshStrategy;
(function (MaisTableRefreshStrategy) {
    MaisTableRefreshStrategy["ON_PUSH"] = "ON_PUSH";
    MaisTableRefreshStrategy["TIMED"] = "TIMED";
    MaisTableRefreshStrategy["NONE"] = "NONE";
})(MaisTableRefreshStrategy || (MaisTableRefreshStrategy = {}));
var MaisTableReloadReason;
(function (MaisTableReloadReason) {
    MaisTableReloadReason["INTERNAL"] = "INTERNAL";
    MaisTableReloadReason["INTERVAL"] = "INTERVAL";
    MaisTableReloadReason["USER"] = "USER";
    MaisTableReloadReason["PUSH"] = "PUSH";
    MaisTableReloadReason["EXTERNAL"] = "EXTERNAL";
})(MaisTableReloadReason || (MaisTableReloadReason = {}));
var MaisTableColumnSize;
(function (MaisTableColumnSize) {
    MaisTableColumnSize["XXS"] = "xxs";
    MaisTableColumnSize["XS"] = "xs";
    MaisTableColumnSize["SMALL"] = "s";
    MaisTableColumnSize["MEDIUM"] = "m";
    MaisTableColumnSize["LARGE"] = "l";
    MaisTableColumnSize["XL"] = "xl";
    MaisTableColumnSize["XXL"] = "xxl";
    MaisTableColumnSize["DEFAULT"] = "default";
})(MaisTableColumnSize || (MaisTableColumnSize = {}));

var MaisTableActionActivationCondition;
(function (MaisTableActionActivationCondition) {
    MaisTableActionActivationCondition["NEVER"] = "NEVER";
    MaisTableActionActivationCondition["ALWAYS"] = "ALWAYS";
    MaisTableActionActivationCondition["SINGLE_SELECTION"] = "SINGLE_SELECTION";
    MaisTableActionActivationCondition["MULTIPLE_SELECTION"] = "MULTIPLE_SELECTION";
    MaisTableActionActivationCondition["NO_SELECTION"] = "NO_SELECTION";
    MaisTableActionActivationCondition["DYNAMIC"] = "DYNAMIC";
})(MaisTableActionActivationCondition || (MaisTableActionActivationCondition = {}));

const COLUMN_DEFAULTS = {
    SHOW_LABEL_IN_TABLE: true,
    CAN_SORT: false,
    CAN_HIDE: true,
    CAN_FILTER: true,
    DEFAULT_FILTER: true,
    DEFAULT_VIEW: true
};

class MaisTableInMemoryHelper {
    static fetchInMemory(data, request, sortColumn, filteringColumns, locale) {
        const output = {
            content: [],
            size: 0,
            totalElements: 0,
            totalPages: 0
        };
        if (!data || !data.length) {
            return output;
        }
        // apply filtering
        let filtered = data;
        if (request.query && request.query.length && request.query.trim().length
            && request.queryFields && request.queryFields.length) {
            const cleanedSearchQuery = request.query.trim().toLowerCase();
            filtered = filtered.filter(candidateRow => {
                let allowed = false;
                if (filteringColumns) {
                    for (const filteringColumn of filteringColumns) {
                        const extractedValue = MaisTableFormatterHelper.extractValue(candidateRow, filteringColumn, locale);
                        if (extractedValue) {
                            const extractedValueStr = extractedValue + '';
                            if (extractedValueStr.toLowerCase().indexOf(cleanedSearchQuery) !== -1) {
                                allowed = true;
                                break;
                            }
                        }
                    }
                }
                return allowed;
            });
        }
        // apply pagination
        let startingIndex;
        let endingIndex;
        if (request && request.page != null && request.size != null &&
            (typeof request.page !== 'undefined') && (typeof request.size !== 'undefined') && (request.page === 0 || request.page > 0)) {
            startingIndex = request.page * request.size;
            endingIndex = (request.page + 1) * request.size;
            if (startingIndex >= filtered.length) {
                return output;
            }
            if (endingIndex > filtered.length) {
                endingIndex = filtered.length;
            }
        }
        else {
            startingIndex = 0;
            endingIndex = filtered.length;
        }
        if (request.sort && request.sort.length && request.sort[0]) {
            const sortDirection = request.sort[0].direction === MaisTableSortDirection.DESCENDING ? -1 : +1;
            if (sortColumn) {
                filtered.sort((c1, c2) => {
                    const v1 = MaisTableFormatterHelper.extractValue(c1, sortColumn, locale) || '';
                    const v2 = MaisTableFormatterHelper.extractValue(c2, sortColumn, locale) || '';
                    const v1n = (v1 === null || v1 === undefined || Number.isNaN(v1));
                    const v2n = (v2 === null || v2 === undefined || Number.isNaN(v2));
                    if (v1n && v2n) {
                        return 0;
                    }
                    else if (v1n && !v2n) {
                        return -1 * sortDirection;
                    }
                    else if (!v1n && v2n) {
                        return 1 * sortDirection;
                    }
                    else {
                        return (v1 > v2 ? 1 : v1 < v2 ? -1 : 0) * sortDirection;
                    }
                });
            }
        }
        const fetchedPage = filtered.slice(startingIndex, endingIndex);
        output.content = fetchedPage;
        output.size = fetchedPage.length;
        output.totalElements = filtered.length;
        if (request.size) {
            output.totalPages = Math.ceil(filtered.length);
        }
        else {
            output.totalPages = 1;
        }
        return output;
    }
}

class MaisTableValidatorHelper {
    static validateColumnsSpecification(columns, config) {
        const err = MaisTableValidatorHelper._validateColumnsSpecification(columns, config);
        if (err) {
            throw new Error(err);
        }
        return null;
    }
    static _validateColumnsSpecification(columns, config) {
        if (!columns || !columns.length) {
            return 'A non-empty list of columns must be provided';
        }
        for (const col of columns) {
            if (!col.name) {
                return 'Columns need to have a non-empty name.';
            }
        }
        for (const col of columns) {
            if ((MaisTableFormatterHelper.isFilterable(col, config) || MaisTableFormatterHelper.isHideable(col, config))
                && !MaisTableFormatterHelper.hasLabel(col, config)) {
                return 'Columns to be filtered or toggled must have either a labelKey or a label. ' +
                    'To hide a column label use the showLabelInTable property. Offending column is [' + col.name + ']';
            }
        }
        if (columns.filter(c => !MaisTableFormatterHelper.isHideable(c, config)).length < 1) {
            return 'At least one of the columns must be not hideable: set canHide=false on primary or relevant column.';
        }
        return null;
    }
}

class MaisTableEmbeddedLogger {
    trace(message, ...additional) {
        console.log('[trace] ' + message, ...additional);
    }
    debug(message, ...additional) {
        console.log('[trace] ' + message, ...additional);
    }
    info(message, ...additional) {
        console.log('[INFO] ' + message, ...additional);
    }
    log(message, ...additional) {
        console.log('[INFO] ' + message, ...additional);
    }
    warn(message, ...additional) {
        console.warn('[WARN] ' + message, ...additional);
    }
    error(message, ...additional) {
        console.error('[ERROR] ' + message, ...additional);
    }
    fatal(message, ...additional) {
        console.error('[FATAL] ' + message, ...additional);
    }
}

class MaisTableLoggerService {
    constructor() {
    }
    static getConfiguredLogAdapter() {
        return MaisTableLoggerService.logAdapter || MaisTableLoggerService.embeddedLogger;
    }
    withLogAdapter(logAdapter) {
        MaisTableLoggerService.logAdapter = logAdapter;
    }
}
MaisTableLoggerService.embeddedLogger = new MaisTableEmbeddedLogger();
MaisTableLoggerService.logAdapter = null;
/** @nocollapse */ MaisTableLoggerService.ɵfac = function MaisTableLoggerService_Factory(t) { return new (t || MaisTableLoggerService)(); };
/** @nocollapse */ MaisTableLoggerService.ɵprov = ɵɵdefineInjectable({ token: MaisTableLoggerService, factory: MaisTableLoggerService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaisTableLoggerService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();

class MaisTableLogger {
    constructor(componentName) {
        this.componentName = componentName;
        // NOP
        this.prefix = '[' + this.componentName + '] ';
    }
    trace(message, ...additional) {
        this.getEffectiveLogger().trace(this.prefix + message, ...additional);
    }
    debug(message, ...additional) {
        this.getEffectiveLogger().debug(this.prefix + message, ...additional);
    }
    info(message, ...additional) {
        this.getEffectiveLogger().info(this.prefix + message, ...additional);
    }
    log(message, ...additional) {
        this.getEffectiveLogger().log(this.prefix + message, ...additional);
    }
    warn(message, ...additional) {
        this.getEffectiveLogger().warn(this.prefix + message, ...additional);
    }
    error(message, ...additional) {
        this.getEffectiveLogger().error(this.prefix + message, ...additional);
    }
    fatal(message, ...additional) {
        this.getEffectiveLogger().fatal(this.prefix + message, ...additional);
    }
    getEffectiveLogger() {
        return MaisTableLoggerService.getConfiguredLogAdapter();
    }
}

class MaisTableRegistryService {
    constructor() {
        this.registry = {};
        this.logger = new MaisTableLogger('MaisTableRegistryService');
        this.logger.trace('building service');
    }
    register(key, component) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!component) {
            this.logger.warn('Invalid component provided to registry', component);
            throw Error('Invalid component provided to registry');
        }
        if (!this.registry[key]) {
            this.registry[key] = [];
        }
        const uuid = 'MTREG-' + (++MaisTableRegistryService.counter) + '-' + Math.round(Math.random() * 100000);
        const item = {
            key,
            component,
            registrationId: uuid
        };
        this.registry[key].push(item);
        this.logger.debug('registered table', item);
        return uuid;
    }
    unregister(key, uuid) {
        if (!key) {
            this.logger.warn('Invalid key provided to registry', key);
            throw Error('Invalid key provided to registry');
        }
        if (!uuid) {
            this.logger.warn('Invalid uuid provided to registry', uuid);
            throw Error('Invalid uuid provided to registry');
        }
        this.logger.debug('unregistered table', uuid);
        this.registry[key] = this.registry[key].filter((o) => o.registrationId !== uuid);
    }
    get(key) {
        return this.registry[key];
    }
    getSingle(key) {
        const arr = this.registry[key];
        if (!arr || !arr.length) {
            return null;
        }
        else if (arr.length > 1) {
            this.logger.error('MULTIPLE REFERENCES FOR KEY', key);
            return null;
        }
        else {
            return arr[0].component;
        }
    }
}
MaisTableRegistryService.counter = 0;
/** @nocollapse */ MaisTableRegistryService.ɵfac = function MaisTableRegistryService_Factory(t) { return new (t || MaisTableRegistryService)(); };
/** @nocollapse */ MaisTableRegistryService.ɵprov = ɵɵdefineInjectable({ token: MaisTableRegistryService, factory: MaisTableRegistryService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaisTableRegistryService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return []; }, null); })();

class MaisTableService {
    constructor(maisTableLoggerService) {
        this.maisTableLoggerService = maisTableLoggerService;
        this.logger = new MaisTableLogger('MaisTableService');
        this.logger.trace('building service');
        this.configuration = this.buildDefaultConfiguration();
        this.logger.debug('initializing MAIS table component for browser', MaisTableBrowserHelper.getBrowser());
        this.maisTableLoggerService.withLogAdapter(null);
    }
    configure(config) {
        Object.assign(this.configuration, config);
        return this;
    }
    withLogAdapter(logAdapter) {
        this.maisTableLoggerService.withLogAdapter(logAdapter);
        return this;
    }
    getConfiguration() {
        return this.configuration;
    }
    buildDefaultConfiguration() {
        return {
            currentSchemaVersion: 'DEFAULT',
            refresh: {
                defaultStrategy: MaisTableRefreshStrategy.NONE,
                defaultInterval: 60000,
                defaultOnPushInBackground: true,
                defaultOnTickInBackground: true
            },
            rowSelection: {
                enabledByDefault: false,
                selectAllEnabledByDefault: true,
                multipleSelectionEnabledByDefault: true
            },
            columnToggling: {
                enabledByDefault: true,
                showFixedColumns: true
            },
            filtering: {
                enabledByDefault: true,
                autoReloadOnQueryClear: true,
                autoReloadOnQueryChange: true,
                autoReloadTimeout: 300,
                showUnfilterableColumns: true
            },
            contextFiltering: {
                enabledByDefault: true
            },
            pagination: {
                enabledByDefault: true,
                pageSizeSelectionEnabledByDefault: true,
                defaultPaginationMode: MaisTablePaginationMethod.CLIENT,
                defaultPageSize: 5,
                defaultPossiblePageSizes: [5, 10, 25, 50]
            },
            actions: {
                contextActionsEnabledByDefault: true,
                headerActionsEnabledByDefault: true,
                defaultHeaderActionActivationCondition: MaisTableActionActivationCondition.ALWAYS,
                defaultContextActionActivationCondition: MaisTableActionActivationCondition.MULTIPLE_SELECTION
            },
            columns: {
                defaultShowLabelInTable: true,
                defaultCanSort: false,
                defaultCanHide: true,
                defaultCanFilter: true,
                defaultIsDefaultFilter: true,
                defaultIsDefaulView: true
            },
            rowExpansion: {
                enabledByDefault: false,
                multipleExpansionEnabledByDefault: false
            },
            dragAndDrop: {
                dragEnabledByDefault: false,
                dropEnabledByDefault: false
            },
            itemTracking: {
                enabledByDefault: false
            },
            storePersistence: {
                enabledByDefault: true
            }
        };
    }
}
/** @nocollapse */ MaisTableService.ɵfac = function MaisTableService_Factory(t) { return new (t || MaisTableService)(ɵɵinject(MaisTableLoggerService)); };
/** @nocollapse */ MaisTableService.ɵprov = ɵɵdefineInjectable({ token: MaisTableService, factory: MaisTableService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaisTableService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: MaisTableLoggerService }]; }, null); })();

const _c0 = ["cellTemplate"];
const _c1 = ["actionsCaptionTemplate"];
const _c2 = ["rowDetailTemplate"];
const _c3 = ["dragTemplate"];
const _c4 = ["dropTemplate"];
const _c5 = ["#renderedMatTable"];
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r20 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template_input_change_0_listener($event) { ɵɵrestoreView(_r20); const column_r14 = ɵɵnextContext(2).$implicit; const ctx_r18 = ɵɵnextContext(4); ctx_r18.toggleFilteringColumnChecked(column_r14); return $event.stopPropagation(); });
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r14 = ɵɵnextContext(2).$implicit;
    const ctx_r16 = ɵɵnextContext(4);
    ɵɵproperty("checked", ctx_r16.isColumnCheckedForFiltering(column_r14));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "input", 31);
} if (rf & 2) {
    ɵɵproperty("checked", false)("disabled", true);
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r24 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 20);
    ɵɵelementStart(1, "div", 22);
    ɵɵelementStart(2, "div", 23);
    ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template, 1, 1, "input", 29);
    ɵɵtemplate(4, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template, 1, 2, "input", 30);
    ɵɵelementStart(5, "span", 25);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template_span_click_5_listener($event) { ɵɵrestoreView(_r24); const column_r14 = ɵɵnextContext().$implicit; const ctx_r22 = ɵɵnextContext(4); ctx_r22.toggleFilteringColumnChecked(column_r14); return $event.stopPropagation(); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r14 = ɵɵnextContext().$implicit;
    const ctx_r15 = ɵɵnextContext(4);
    ɵɵclassProp("disabled", !ctx_r15.isFilterable(column_r14));
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r15.isFilterable(column_r14));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r15.isFilterable(column_r14));
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ctx_r15.resolveLabel(column_r14), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template, 7, 5, "button", 28);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r14 = ctx.$implicit;
    const ctx_r13 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r13.hasLabel(column_r14) && (ctx_r13.isFilterable(column_r14) || ctx_r13.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    const _r27 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 15);
    ɵɵelementStart(2, "button", 16);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_button_click_2_listener() { ɵɵrestoreView(_r27); const ctx_r26 = ɵɵnextContext(3); return ctx_r26.applySelectedFilter(); });
    ɵɵelement(3, "i", 17);
    ɵɵelementEnd();
    ɵɵelement(4, "button", 18);
    ɵɵelementStart(5, "div", 19);
    ɵɵelementStart(6, "div", 20);
    ɵɵelementStart(7, "h6", 21);
    ɵɵtext(8);
    ɵɵpipe(9, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(10, "button", 20);
    ɵɵelementStart(11, "div", 22);
    ɵɵelementStart(12, "div", 23);
    ɵɵelementStart(13, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_div_1_div_2_ng_container_8_Template_input_change_13_listener($event) { ɵɵrestoreView(_r27); const ctx_r28 = ɵɵnextContext(3); ctx_r28.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    ɵɵelementEnd();
    ɵɵelementStart(14, "span", 25);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_span_click_14_listener($event) { ɵɵrestoreView(_r27); const ctx_r29 = ɵɵnextContext(3); ctx_r29.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
    ɵɵtext(15);
    ɵɵpipe(16, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelement(17, "div", 26);
    ɵɵelementEnd();
    ɵɵtemplate(18, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template, 2, 1, "ng-container", 27);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r11 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("autoClose", "outside");
    ɵɵadvance(1);
    ɵɵclassProp("btn-black", !ctx_r11.searchQueryNeedApply || !ctx_r11.searchQueryActive)("btn-primary", ctx_r11.searchQueryNeedApply && ctx_r11.searchQueryActive)("border-primary", ctx_r11.searchQueryActive && !ctx_r11.searchQueryMalformed)("border-warning", ctx_r11.searchQueryMalformed);
    ɵɵadvance(2);
    ɵɵclassProp("border-primary", ctx_r11.searchQueryActive && !ctx_r11.searchQueryMalformed)("border-warning", ctx_r11.searchQueryMalformed)("btn-black", !(ctx_r11.selectedSearchQuery && ctx_r11.noFilteringColumnshecked))("btn-warning", ctx_r11.selectedSearchQuery && ctx_r11.noFilteringColumnshecked);
    ɵɵadvance(4);
    ɵɵtextInterpolate(ɵɵpipeBind1(9, 21, "table.common.messages.pick_filtering_columns"));
    ɵɵadvance(5);
    ɵɵproperty("checked", ctx_r11.allFilteringColumnsChecked);
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(16, 23, "table.common.messages.pick_all_filtering_columns"), " ");
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", ctx_r11.columns);
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r40 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 32);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r40); const column_r33 = ɵɵnextContext().$implicit; const ctx_r38 = ɵɵnextContext(4); return ctx_r38.toggleFilteringColumnChecked(column_r33); });
    ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(2, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(3, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    ɵɵtext(4);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r33 = ɵɵnextContext().$implicit;
    const ctx_r34 = ɵɵnextContext(4);
    ɵɵclassProp("disabled", !ctx_r34.isFilterable(column_r33));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r34.isFilterable(column_r33) && ctx_r34.isColumnCheckedForFiltering(column_r33));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r34.isFilterable(column_r33) && !ctx_r34.isColumnCheckedForFiltering(column_r33));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r34.isFilterable(column_r33));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r34.resolveLabel(column_r33), " ");
} }
function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template, 5, 6, "button", 35);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r33 = ctx.$implicit;
    const ctx_r32 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r32.hasLabel(column_r33) && (ctx_r32.isFilterable(column_r33) || ctx_r32.configFilteringShowUnfilterableColumns));
} }
function MaisTableComponent_div_1_div_2_ng_container_9_Template(rf, ctx) { if (rf & 1) {
    const _r43 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 15);
    ɵɵelementStart(2, "button", 16);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_2_listener() { ɵɵrestoreView(_r43); const ctx_r42 = ɵɵnextContext(3); return ctx_r42.applySelectedFilter(); });
    ɵɵelement(3, "i", 17);
    ɵɵelementEnd();
    ɵɵelement(4, "button", 18);
    ɵɵelementStart(5, "div", 19);
    ɵɵelementStart(6, "div", 20);
    ɵɵelementStart(7, "h6", 21);
    ɵɵtext(8);
    ɵɵpipe(9, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(10, "button", 32);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_10_listener() { ɵɵrestoreView(_r43); const ctx_r44 = ɵɵnextContext(3); return ctx_r44.toggleAllFilteringColumnsChecked(); });
    ɵɵtemplate(11, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(12, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template, 1, 1, "fa-icon", 33);
    ɵɵtext(13);
    ɵɵpipe(14, "translate");
    ɵɵelement(15, "div", 26);
    ɵɵelementEnd();
    ɵɵtemplate(16, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template, 2, 1, "ng-container", 27);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r12 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("autoClose", "outside");
    ɵɵadvance(1);
    ɵɵclassProp("btn-black", !ctx_r12.searchQueryNeedApply || !ctx_r12.searchQueryActive)("btn-primary", ctx_r12.searchQueryNeedApply && ctx_r12.searchQueryActive)("border-primary", ctx_r12.searchQueryActive && !ctx_r12.searchQueryMalformed)("border-warning", ctx_r12.searchQueryMalformed);
    ɵɵadvance(2);
    ɵɵclassProp("border-primary", ctx_r12.searchQueryActive && !ctx_r12.searchQueryMalformed)("border-warning", ctx_r12.searchQueryMalformed)("btn-black", !(ctx_r12.selectedSearchQuery && ctx_r12.noFilteringColumnshecked))("btn-warning", ctx_r12.selectedSearchQuery && ctx_r12.noFilteringColumnshecked);
    ɵɵadvance(4);
    ɵɵtextInterpolate(ɵɵpipeBind1(9, 22, "table.common.messages.pick_filtering_columns"));
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r12.allFilteringColumnsChecked);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r12.allFilteringColumnsChecked);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(14, 24, "table.common.messages.pick_all_filtering_columns"), " ");
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", ctx_r12.columns);
} }
function MaisTableComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r46 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 11);
    ɵɵelementStart(1, "form", 12);
    ɵɵlistener("submit", function MaisTableComponent_div_1_div_2_Template_form_submit_1_listener() { ɵɵrestoreView(_r46); const ctx_r45 = ɵɵnextContext(2); return ctx_r45.applySelectedFilter(); });
    ɵɵelementStart(2, "div");
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(5, "div", 13);
    ɵɵelementStart(6, "input", 14);
    ɵɵlistener("ngModelChange", function MaisTableComponent_div_1_div_2_Template_input_ngModelChange_6_listener($event) { ɵɵrestoreView(_r46); const ctx_r47 = ɵɵnextContext(2); return ctx_r47.selectedSearchQuery = $event; })("focusout", function MaisTableComponent_div_1_div_2_Template_input_focusout_6_listener() { ɵɵrestoreView(_r46); const ctx_r48 = ɵɵnextContext(2); return ctx_r48.searchQueryFocusOut(); });
    ɵɵpipe(7, "translate");
    ɵɵelementEnd();
    ɵɵtemplate(8, MaisTableComponent_div_1_div_2_ng_container_8_Template, 19, 25, "ng-container", 2);
    ɵɵtemplate(9, MaisTableComponent_div_1_div_2_ng_container_9_Template, 17, 26, "ng-container", 2);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = ɵɵnextContext(2);
    ɵɵadvance(3);
    ɵɵtextInterpolate(ɵɵpipeBind1(4, 9, "table.common.messages.filter_prompt"));
    ɵɵadvance(3);
    ɵɵclassProp("border-primary", ctx_r4.searchQueryActive && !ctx_r4.searchQueryMalformed)("border-warning", ctx_r4.searchQueryMalformed);
    ɵɵpropertyInterpolate("placeholder", ɵɵpipeBind1(7, 11, "table.common.messages.filter_placeholder"));
    ɵɵproperty("ngModel", ctx_r4.selectedSearchQuery);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r4.compatibilityModeForDropDowns);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r4.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r58 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template_input_change_0_listener($event) { ɵɵrestoreView(_r58); const column_r52 = ɵɵnextContext(2).$implicit; const ctx_r56 = ɵɵnextContext(4); ctx_r56.toggleVisibleColumnChecked(column_r52); return $event.stopPropagation(); });
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r52 = ɵɵnextContext(2).$implicit;
    const ctx_r54 = ɵɵnextContext(4);
    ɵɵproperty("checked", ctx_r54.isColumnCheckedForVisualization(column_r52));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "input", 31);
} if (rf & 2) {
    ɵɵproperty("checked", true)("disabled", true);
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r62 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 20);
    ɵɵelementStart(1, "div", 22);
    ɵɵelementStart(2, "div", 23);
    ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template, 1, 1, "input", 29);
    ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template, 1, 2, "input", 30);
    ɵɵelementStart(5, "span", 25);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template_span_click_5_listener($event) { ɵɵrestoreView(_r62); const column_r52 = ɵɵnextContext().$implicit; const ctx_r60 = ɵɵnextContext(4); ctx_r60.toggleVisibleColumnChecked(column_r52); return $event.stopPropagation(); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r52 = ɵɵnextContext().$implicit;
    const ctx_r53 = ɵɵnextContext(4);
    ɵɵclassProp("disabled", !ctx_r53.isHideable(column_r52));
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r53.isHideable(column_r52));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r53.isHideable(column_r52));
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ctx_r53.resolveLabel(column_r52), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template, 7, 5, "button", 28);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r52 = ctx.$implicit;
    const ctx_r51 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r51.hasLabel(column_r52) && (ctx_r51.isHideable(column_r52) || ctx_r51.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    const _r65 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 37);
    ɵɵelementStart(2, "button", 38);
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(5, "div", 19);
    ɵɵelementStart(6, "h6", 39);
    ɵɵtext(7);
    ɵɵpipe(8, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(9, "button", 20);
    ɵɵelementStart(10, "div", 22);
    ɵɵelementStart(11, "div", 23);
    ɵɵelementStart(12, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_div_1_div_3_ng_container_4_Template_input_change_12_listener($event) { ɵɵrestoreView(_r65); const ctx_r64 = ɵɵnextContext(3); ctx_r64.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    ɵɵelementEnd();
    ɵɵelementStart(13, "span", 25);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_4_Template_span_click_13_listener($event) { ɵɵrestoreView(_r65); const ctx_r66 = ɵɵnextContext(3); ctx_r66.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
    ɵɵtext(14);
    ɵɵpipe(15, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelement(16, "div", 26);
    ɵɵelementEnd();
    ɵɵtemplate(17, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template, 2, 1, "ng-container", 27);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r49 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("autoClose", "outside");
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(4, 6, "table.common.messages.add_remove_columns"), " ");
    ɵɵadvance(4);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(8, 8, "table.common.messages.select_columns_prompt"), " ");
    ɵɵadvance(5);
    ɵɵproperty("checked", ctx_r49.allVisibleColumnsChecked);
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(15, 10, "table.common.messages.pick_all_visible_columns"), " ");
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", ctx_r49.columns);
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "fa-icon", 34);
} if (rf & 2) {
    ɵɵproperty("icon", "check-square");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r77 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 32);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r77); const column_r70 = ɵɵnextContext().$implicit; const ctx_r75 = ɵɵnextContext(4); return ctx_r75.toggleVisibleColumnChecked(column_r70); });
    ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(2, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(3, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
    ɵɵtext(4);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r70 = ɵɵnextContext().$implicit;
    const ctx_r71 = ɵɵnextContext(4);
    ɵɵclassProp("disabled", !ctx_r71.isHideable(column_r70));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r71.isHideable(column_r70) && ctx_r71.isColumnCheckedForVisualization(column_r70));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r71.isHideable(column_r70) && !ctx_r71.isColumnCheckedForVisualization(column_r70));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r71.isHideable(column_r70));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r71.resolveLabel(column_r70), " ");
} }
function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template, 5, 6, "button", 35);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r70 = ctx.$implicit;
    const ctx_r69 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r69.hasLabel(column_r70) && (ctx_r69.isHideable(column_r70) || ctx_r69.configColumnVisibilityShowFixedColumns));
} }
function MaisTableComponent_div_1_div_3_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    const _r80 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 37);
    ɵɵelementStart(2, "button", 38);
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(5, "div", 19);
    ɵɵelementStart(6, "h6", 39);
    ɵɵtext(7);
    ɵɵpipe(8, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(9, "button", 32);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_3_ng_container_5_Template_button_click_9_listener() { ɵɵrestoreView(_r80); const ctx_r79 = ɵɵnextContext(3); return ctx_r79.toggleAllVisibleColumnsChecked(); });
    ɵɵtemplate(10, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template, 1, 1, "fa-icon", 33);
    ɵɵtemplate(11, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template, 1, 1, "fa-icon", 33);
    ɵɵtext(12);
    ɵɵpipe(13, "translate");
    ɵɵelement(14, "div", 26);
    ɵɵelementEnd();
    ɵɵtemplate(15, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template, 2, 1, "ng-container", 27);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r50 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("autoClose", "outside");
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(4, 7, "table.common.messages.add_remove_columns"), " ");
    ɵɵadvance(4);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(8, 9, "table.common.messages.select_columns_prompt"), " ");
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r50.allVisibleColumnsChecked);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r50.allVisibleColumnsChecked);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(13, 11, "table.common.messages.pick_all_visible_columns"), " ");
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", ctx_r50.columns);
} }
function MaisTableComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 36);
    ɵɵelementStart(1, "div");
    ɵɵtext(2);
    ɵɵpipe(3, "translate");
    ɵɵelementEnd();
    ɵɵtemplate(4, MaisTableComponent_div_1_div_3_ng_container_4_Template, 18, 12, "ng-container", 2);
    ɵɵtemplate(5, MaisTableComponent_div_1_div_3_ng_container_5_Template, 16, 13, "ng-container", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(3, 3, "table.common.messages.customize_view_prompt"));
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r5.compatibilityModeForDropDowns);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r5.compatibilityModeForDropDowns);
} }
function MaisTableComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "div", 36);
} }
function MaisTableComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "div", 11);
} }
function MaisTableComponent_div_1_ng_template_8_Template(rf, ctx) { }
function MaisTableComponent_div_1_ng_container_10_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r84 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 41);
    ɵɵlistener("click", function MaisTableComponent_div_1_ng_container_10_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r84); const action_r82 = ctx.$implicit; const ctx_r83 = ɵɵnextContext(3); return ctx_r83.clickOnContextAction(action_r82); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const action_r82 = ctx.$implicit;
    const ctx_r81 = ɵɵnextContext(3);
    ɵɵclassMapInterpolate2("btn btn-", action_r82.displayClass || "light", " ", action_r82.additionalClasses || "", " mr-1");
    ɵɵproperty("disabled", !ctx_r81.isContextActionAllowed(action_r82));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r81.resolveLabel(action_r82), " ");
} }
function MaisTableComponent_div_1_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_1_ng_container_10_button_1_Template, 2, 6, "button", 40);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r9 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r9.headerActions);
} }
function MaisTableComponent_div_1_div_11_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r88 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 47);
    ɵɵlistener("click", function MaisTableComponent_div_1_div_11_button_9_Template_button_click_0_listener() { ɵɵrestoreView(_r88); const action_r86 = ctx.$implicit; const ctx_r87 = ɵɵnextContext(3); return ctx_r87.clickOnContextAction(action_r86); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const action_r86 = ctx.$implicit;
    const ctx_r85 = ɵɵnextContext(3);
    ɵɵproperty("disabled", !ctx_r85.isContextActionAllowed(action_r86));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r85.resolveLabel(action_r86), " ");
} }
function MaisTableComponent_div_1_div_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 42);
    ɵɵelementStart(1, "button", 43);
    ɵɵtext(2);
    ɵɵpipe(3, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 44);
    ɵɵelementStart(5, "div", 45);
    ɵɵelementStart(6, "h6", 21);
    ɵɵtext(7);
    ɵɵpipe(8, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(9, MaisTableComponent_div_1_div_11_button_9_Template, 2, 2, "button", 46);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("disabled", !ctx_r10.anyButtonActionsAllowed);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    ɵɵadvance(5);
    ɵɵtextInterpolate(ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r10.currentActions);
} }
const _c6 = function () { return {}; };
function MaisTableComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 4);
    ɵɵelementStart(1, "div", 5);
    ɵɵtemplate(2, MaisTableComponent_div_1_div_2_Template, 10, 13, "div", 6);
    ɵɵtemplate(3, MaisTableComponent_div_1_div_3_Template, 6, 5, "div", 7);
    ɵɵtemplate(4, MaisTableComponent_div_1_div_4_Template, 1, 0, "div", 7);
    ɵɵtemplate(5, MaisTableComponent_div_1_div_5_Template, 1, 0, "div", 6);
    ɵɵelementStart(6, "div", 8);
    ɵɵelementStart(7, "div");
    ɵɵtemplate(8, MaisTableComponent_div_1_ng_template_8_Template, 0, 0, "ng-template", 9);
    ɵɵtext(9, " \u00A0 ");
    ɵɵelementEnd();
    ɵɵtemplate(10, MaisTableComponent_div_1_ng_container_10_Template, 2, 1, "ng-container", 2);
    ɵɵtemplate(11, MaisTableComponent_div_1_div_11_Template, 10, 8, "div", 10);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r0.filteringPossibleAndAllowed);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r0.columnSelectionPossibleAndAllowed);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r0.columnSelectionPossibleAndAllowed);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r0.filteringPossibleAndAllowed);
    ɵɵadvance(3);
    ɵɵproperty("ngTemplateOutlet", ctx_r0.actionsCaptionTemplate)("ngTemplateOutletContext", ɵɵpureFunction0(8, _c6));
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r0.headerActionsEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r0.contextActionsEnabledAndPossible);
} }
function MaisTableComponent_ng_container_2_th_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "th", 54);
} }
function MaisTableComponent_ng_container_2_th_9_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r98 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_9_span_2_Template_input_change_1_listener() { ɵɵrestoreView(_r98); const ctx_r97 = ɵɵnextContext(3); return ctx_r97.toggleAllChecked(); });
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r96 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("checked", ctx_r96.allChecked);
} }
function MaisTableComponent_ng_container_2_th_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 54);
    ɵɵelementStart(1, "div", 23);
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_th_9_span_2_Template, 2, 1, "span", 2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r90 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r90.currentEnableSelectAll && ctx_r90.currentEnableMultiSelect && !ctx_r90.noResults);
} }
function MaisTableComponent_ng_container_2_th_10_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵpipe(2, "translate");
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r99 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(2, 1, ctx_r99.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_2_th_10_span_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵpipe(2, "translate");
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_2_th_10_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r104 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 20);
    ɵɵelementStart(1, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_2_th_10_div_7_Template_input_change_1_listener($event) { ɵɵrestoreView(_r104); const filter_r102 = ctx.$implicit; const ctx_r103 = ɵɵnextContext(3); ctx_r103.toggleContextFilterChecked(filter_r102); return $event.stopPropagation(); });
    ɵɵelementEnd();
    ɵɵelementStart(2, "span", 59);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_th_10_div_7_Template_span_click_2_listener($event) { ɵɵrestoreView(_r104); const filter_r102 = ctx.$implicit; const ctx_r105 = ɵɵnextContext(3); ctx_r105.toggleContextFilterChecked(filter_r102); return $event.stopPropagation(); });
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const filter_r102 = ctx.$implicit;
    const ctx_r101 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("checked", ctx_r101.isContextFilterChecked(filter_r102));
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ctx_r101.resolveLabel(filter_r102), " ");
} }
function MaisTableComponent_ng_container_2_th_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 55);
    ɵɵelementStart(1, "div", 56);
    ɵɵelementStart(2, "div", 57);
    ɵɵelementStart(3, "div", 19);
    ɵɵelementStart(4, "h6", 39);
    ɵɵtemplate(5, MaisTableComponent_ng_container_2_th_10_span_5_Template, 3, 3, "span", 2);
    ɵɵtemplate(6, MaisTableComponent_ng_container_2_th_10_span_6_Template, 3, 3, "span", 2);
    ɵɵelementEnd();
    ɵɵtemplate(7, MaisTableComponent_ng_container_2_th_10_div_7_Template, 4, 2, "div", 58);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r91 = ɵɵnextContext(2);
    ɵɵclassProp("border-primary", ctx_r91.anyContextFilterChecked);
    ɵɵproperty("autoClose", "outside");
    ɵɵadvance(5);
    ɵɵproperty("ngIf", ctx_r91.contextFilteringPrompt);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r91.contextFilteringPrompt);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r91.currentContextFilters);
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r113 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "button", 61);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template_button_click_1_listener() { ɵɵrestoreView(_r113); const column_r106 = ɵɵnextContext(2).$implicit; const ctx_r111 = ɵɵnextContext(2); return ctx_r111.clickOnColumn(column_r106); });
    ɵɵelement(2, "i", 62);
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r116 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "button", 61);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template_button_click_1_listener() { ɵɵrestoreView(_r116); const column_r106 = ɵɵnextContext(2).$implicit; const ctx_r114 = ɵɵnextContext(2); return ctx_r114.clickOnColumn(column_r106); });
    ɵɵelement(2, "i", 62);
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtemplate(1, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template, 3, 0, "span", 2);
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template, 3, 0, "span", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r106 = ɵɵnextContext().$implicit;
    const ctx_r107 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r107.isCurrentSortingColumn(column_r106, "ASC"));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r107.isCurrentSortingColumn(column_r106, "DESC"));
} }
function MaisTableComponent_ng_container_2_ng_container_11_span_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r106 = ɵɵnextContext().$implicit;
    const ctx_r108 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r108.resolveLabel(column_r106), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_11_Template(rf, ctx) { if (rf & 1) {
    const _r120 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "th", 60);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_ng_container_11_Template_th_click_1_listener() { ɵɵrestoreView(_r120); const column_r106 = ctx.$implicit; const ctx_r119 = ɵɵnextContext(2); return ctx_r119.clickOnColumn(column_r106); });
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_ng_container_11_span_2_Template, 3, 2, "span", 2);
    ɵɵtemplate(3, MaisTableComponent_ng_container_2_ng_container_11_span_3_Template, 2, 1, "span", 2);
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r106 = ctx.$implicit;
    const ctx_r92 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵclassProp("order-up", ctx_r92.isCurrentSortingColumn(column_r106, "ASC"))("order-down", ctx_r92.isCurrentSortingColumn(column_r106, "DESC"))("border-primary", ctx_r92.isCurrentSortingColumn(column_r106))("clickable", ctx_r92.isSortable(column_r106));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r92.isCurrentSortingColumn(column_r106));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r92.isLabelVisibleInTable(column_r106) && ctx_r92.hasLabel(column_r106));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    const _r134 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "fa-icon", 71);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template_fa_icon_click_0_listener() { ɵɵrestoreView(_r134); const row_r122 = ɵɵnextContext(2).$implicit; const ctx_r132 = ɵɵnextContext(3); return ctx_r132.clickOnRowExpansion(row_r122); });
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    const _r137 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "fa-icon", 71);
    ɵɵlistener("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template_fa_icon_click_0_listener() { ɵɵrestoreView(_r137); const row_r122 = ɵɵnextContext(2).$implicit; const ctx_r135 = ɵɵnextContext(3); return ctx_r135.clickOnRowExpansion(row_r122); });
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 69);
    ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r123 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r123.isExpanded(row_r122) && ctx_r123.isExpandable(row_r122));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r123.isExpanded(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template(rf, ctx) { if (rf & 1) {
    const _r141 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "th", 69);
    ɵɵelementStart(1, "div", 23);
    ɵɵelementStart(2, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template_input_change_2_listener() { ɵɵrestoreView(_r141); const row_r122 = ɵɵnextContext().$implicit; const ctx_r139 = ɵɵnextContext(3); return ctx_r139.toggleChecked(row_r122); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r124 = ɵɵnextContext(3);
    ɵɵadvance(2);
    ɵɵproperty("checked", ctx_r124.isChecked(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "td");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template(rf, ctx) { }
const _c7 = function (a0, a1, a2) { return { row: a0, column: a1, value: a2 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r143 = ɵɵnextContext().$implicit;
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r144 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("ngTemplateOutlet", ctx_r144.cellTemplate)("ngTemplateOutletContext", ɵɵpureFunction3(2, _c7, row_r122, column_r143, ctx_r144.extractValue(row_r122, column_r143)));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r143 = ɵɵnextContext().$implicit;
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r145 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r145.extractValue(row_r122, column_r143), " ");
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "td");
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template, 2, 6, "div", 2);
    ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template, 2, 1, "div", 2);
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r143 = ctx.$implicit;
    ɵɵadvance(2);
    ɵɵproperty("ngIf", column_r143.applyTemplate);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !column_r143.applyTemplate);
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template(rf, ctx) { }
const _c8 = function (a0) { return { row: a0 }; };
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tr", 72);
    ɵɵelement(1, "td");
    ɵɵelementStart(2, "td", 73);
    ɵɵelementStart(3, "div", 74);
    ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r127 = ɵɵnextContext(3);
    ɵɵproperty("hidden", !ctx_r127.referencedTable.acceptDrop);
    ɵɵadvance(2);
    ɵɵattribute("colspan", ctx_r127.activeColumnsCount - 1);
    ɵɵadvance(2);
    ɵɵproperty("ngTemplateOutlet", ctx_r127.referencedTable.dropTemplate)("ngTemplateOutletContext", ɵɵpureFunction1(4, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 75);
    ɵɵtemplate(1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r128 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("ngTemplateOutlet", ctx_r128.dragTemplate)("ngTemplateOutletContext", ɵɵpureFunction1(2, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template(rf, ctx) { }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tr", 76);
    ɵɵelement(1, "td");
    ɵɵelementStart(2, "td");
    ɵɵelementStart(3, "div", 77);
    ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r122 = ɵɵnextContext().$implicit;
    const ctx_r129 = ɵɵnextContext(3);
    ɵɵadvance(2);
    ɵɵattribute("colspan", ctx_r129.activeColumnsCount - 1);
    ɵɵadvance(2);
    ɵɵproperty("ngTemplateOutlet", ctx_r129.rowDetailTemplate)("ngTemplateOutletContext", ɵɵpureFunction1(3, _c8, row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "tr", 64);
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template, 3, 2, "th", 65);
    ɵɵtemplate(3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template, 3, 1, "th", 65);
    ɵɵtemplate(4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template, 1, 0, "td", 2);
    ɵɵtemplate(5, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template, 4, 2, "ng-container", 27);
    ɵɵelementStart(6, "td", 50);
    ɵɵtemplate(7, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template, 5, 6, "tr", 66);
    ɵɵelementEnd();
    ɵɵelementStart(8, "td", 50);
    ɵɵtemplate(9, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template, 2, 4, "div", 67);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(10, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template, 5, 5, "tr", 68);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const row_r122 = ctx.$implicit;
    const ctx_r121 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵclassProp("row-selected", ctx_r121.isChecked(row_r122));
    ɵɵproperty("cdkDragData", row_r122)("cdkDragDisabled", !ctx_r121.acceptDrag);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r121.rowExpansionEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r121.currentEnableSelection);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r121.contextFilteringEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r121.visibleColumns);
    ɵɵadvance(5);
    ɵɵproperty("ngIf", ctx_r121.rowExpansionEnabledAndPossible && ctx_r121.isExpanded(row_r122));
} }
function MaisTableComponent_ng_container_2_tbody_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tbody");
    ɵɵelement(1, "tr", 63);
    ɵɵtemplate(2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template, 11, 9, "ng-container", 27);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r93 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r93.currentData);
} }
function MaisTableComponent_ng_container_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "tbody");
    ɵɵelementStart(2, "tr", 78);
    ɵɵelementStart(3, "td", 79);
    ɵɵtext(4);
    ɵɵpipe(5, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r94 = ɵɵnextContext(2);
    ɵɵadvance(3);
    ɵɵattribute("colspan", ctx_r94.activeColumnsCount);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(5, 2, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_2_ng_container_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "tbody");
    ɵɵelementStart(2, "tr", 80);
    ɵɵelementStart(3, "td", 79);
    ɵɵtext(4);
    ɵɵpipe(5, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r95 = ɵɵnextContext(2);
    ɵɵadvance(3);
    ɵɵattribute("colspan", ctx_r95.activeColumnsCount);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(5, 2, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    const _r158 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 48);
    ɵɵelementStart(2, "table", 49);
    ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_2_Template_table_cdkDropListDropped_2_listener($event) { ɵɵrestoreView(_r158); const ctx_r157 = ɵɵnextContext(); return ctx_r157.handleItemDropped($event); });
    ɵɵelementStart(3, "caption", 50);
    ɵɵtext(4);
    ɵɵpipe(5, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(6, "thead", 51);
    ɵɵelementStart(7, "tr");
    ɵɵtemplate(8, MaisTableComponent_ng_container_2_th_8_Template, 1, 0, "th", 52);
    ɵɵtemplate(9, MaisTableComponent_ng_container_2_th_9_Template, 3, 1, "th", 52);
    ɵɵtemplate(10, MaisTableComponent_ng_container_2_th_10_Template, 8, 6, "th", 53);
    ɵɵtemplate(11, MaisTableComponent_ng_container_2_ng_container_11_Template, 4, 10, "ng-container", 27);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(12, MaisTableComponent_ng_container_2_tbody_12_Template, 3, 1, "tbody", 2);
    ɵɵtemplate(13, MaisTableComponent_ng_container_2_ng_container_13_Template, 6, 4, "ng-container", 2);
    ɵɵtemplate(14, MaisTableComponent_ng_container_2_ng_container_14_Template, 6, 4, "ng-container", 2);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵclassProp("nodrop", !ctx_r1.acceptDrop)("acceptdrop", ctx_r1.acceptDrop);
    ɵɵpropertyInterpolate("id", ctx_r1.currentTableId);
    ɵɵproperty("cdkDropListConnectedTo", ctx_r1.dropConnectedTo)("cdkDropListData", ctx_r1.currentData)("cdkDropListEnterPredicate", ctx_r1.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r1.acceptDrop);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(5, 17, "table.common.accessibility.caption"));
    ɵɵadvance(4);
    ɵɵproperty("ngIf", ctx_r1.rowExpansionEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.currentEnableSelection);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.contextFilteringEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r1.visibleColumns);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r1.showFetching && !ctx_r1.forceReRender);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r1.showFetching && ctx_r1.noResults);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.showFetching);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-header-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
    const _r180 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "fa-icon", 71);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template_fa_icon_click_0_listener() { ɵɵrestoreView(_r180); const row_r175 = ɵɵnextContext().$implicit; const ctx_r178 = ɵɵnextContext(4); return ctx_r178.clickOnRowExpansion(row_r175); });
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("icon", "caret-square-down");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
    const _r183 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "fa-icon", 71);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template_fa_icon_click_0_listener() { ɵɵrestoreView(_r183); const row_r175 = ɵɵnextContext().$implicit; const ctx_r181 = ɵɵnextContext(4); return ctx_r181.clickOnRowExpansion(row_r175); });
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("icon", "caret-square-up");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-cell", 93);
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template, 1, 1, "fa-icon", 70);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template, 1, 1, "fa-icon", 70);
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r175 = ctx.$implicit;
    const ctx_r165 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r165.isExpanded(row_r175) && ctx_r165.isExpandable(row_r175));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r165.isExpanded(row_r175));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r186 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template_input_change_1_listener() { ɵɵrestoreView(_r186); const ctx_r185 = ɵɵnextContext(5); return ctx_r185.toggleAllChecked(); });
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r184 = ɵɵnextContext(5);
    ɵɵadvance(1);
    ɵɵproperty("checked", ctx_r184.allChecked);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-header-cell", 93);
    ɵɵelementStart(1, "div", 23);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template, 2, 1, "span", 2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r166 = ɵɵnextContext(4);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r166.currentEnableSelectAll && ctx_r166.currentEnableMultiSelect && !ctx_r166.noResults);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template(rf, ctx) { if (rf & 1) {
    const _r189 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-cell", 93);
    ɵɵelementStart(1, "div", 23);
    ɵɵelementStart(2, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template_input_change_2_listener() { ɵɵrestoreView(_r189); const row_r187 = ctx.$implicit; const ctx_r188 = ɵɵnextContext(4); return ctx_r188.toggleChecked(row_r187); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r187 = ctx.$implicit;
    const ctx_r167 = ɵɵnextContext(4);
    ɵɵadvance(2);
    ɵɵproperty("checked", ctx_r167.isChecked(row_r187));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵpipe(2, "translate");
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r190 = ɵɵnextContext(5);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(2, 1, ctx_r190.contextFilteringPrompt), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵpipe(2, "translate");
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(2, 1, "table.common.messages.context_filter_prompt"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r195 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 20);
    ɵɵelementStart(1, "input", 24);
    ɵɵlistener("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_input_change_1_listener($event) { ɵɵrestoreView(_r195); const filter_r193 = ctx.$implicit; const ctx_r194 = ɵɵnextContext(5); ctx_r194.toggleContextFilterChecked(filter_r193); return $event.stopPropagation(); });
    ɵɵelementEnd();
    ɵɵelementStart(2, "span", 59);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_span_click_2_listener($event) { ɵɵrestoreView(_r195); const filter_r193 = ctx.$implicit; const ctx_r196 = ɵɵnextContext(5); ctx_r196.toggleContextFilterChecked(filter_r193); return $event.stopPropagation(); });
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const filter_r193 = ctx.$implicit;
    const ctx_r192 = ɵɵnextContext(5);
    ɵɵadvance(1);
    ɵɵproperty("checked", ctx_r192.isContextFilterChecked(filter_r193));
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ctx_r192.resolveLabel(filter_r193), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-header-cell", 93);
    ɵɵelementStart(1, "div", 94);
    ɵɵelementStart(2, "div", 56);
    ɵɵelementStart(3, "div", 57);
    ɵɵelementStart(4, "div", 19);
    ɵɵelementStart(5, "h6", 39);
    ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template, 3, 3, "span", 2);
    ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template, 3, 3, "span", 2);
    ɵɵelementEnd();
    ɵɵtemplate(8, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template, 4, 2, "div", 58);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r168 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵclassProp("border-primary", ctx_r168.anyContextFilterChecked);
    ɵɵproperty("autoClose", "outside")("container", "body");
    ɵɵadvance(5);
    ɵɵproperty("ngIf", ctx_r168.contextFilteringPrompt);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r168.contextFilteringPrompt);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r168.currentContextFilters);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-cell", 93);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-cell");
    ɵɵelementStart(1, "div", 77);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const detail_r198 = ctx.$implicit;
    const ctx_r170 = ɵɵnextContext(4);
    ɵɵadvance(2);
    ɵɵproperty("ngTemplateOutlet", ctx_r170.rowDetailTemplate)("ngTemplateOutletContext", ɵɵpureFunction1(2, _c8, detail_r198.___parentRow));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r209 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "button", 61);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template_button_click_1_listener() { ɵɵrestoreView(_r209); const column_r200 = ɵɵnextContext(3).$implicit; const ctx_r207 = ɵɵnextContext(4); return ctx_r207.clickOnColumn(column_r200); });
    ɵɵelement(2, "i", 62);
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r212 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "button", 61);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template_button_click_1_listener() { ɵɵrestoreView(_r212); const column_r200 = ɵɵnextContext(3).$implicit; const ctx_r210 = ɵɵnextContext(4); return ctx_r210.clickOnColumn(column_r200); });
    ɵɵelement(2, "i", 62);
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template, 3, 0, "span", 2);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template, 3, 0, "span", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = ɵɵnextContext(2).$implicit;
    const ctx_r203 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r203.isCurrentSortingColumn(column_r200, "ASC"));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r203.isCurrentSortingColumn(column_r200, "DESC"));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = ɵɵnextContext(2).$implicit;
    const ctx_r204 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r204.resolveLabel(column_r200), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template(rf, ctx) { if (rf & 1) {
    const _r217 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-header-cell");
    ɵɵelementStart(1, "div", 25);
    ɵɵlistener("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template_div_click_1_listener() { ɵɵrestoreView(_r217); const column_r200 = ɵɵnextContext().$implicit; const ctx_r215 = ɵɵnextContext(4); return ctx_r215.clickOnColumn(column_r200); });
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template, 3, 2, "span", 2);
    ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template, 2, 1, "span", 2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = ɵɵnextContext().$implicit;
    const ctx_r201 = ɵɵnextContext(4);
    ɵɵclassMapInterpolate1("maissize-", column_r200.size || "default", "");
    ɵɵadvance(1);
    ɵɵclassMap(column_r200.headerDisplayClass || "");
    ɵɵclassProp("order-up", ctx_r201.isCurrentSortingColumn(column_r200, "ASC"))("order-down", ctx_r201.isCurrentSortingColumn(column_r200, "DESC"))("border-primary", ctx_r201.isCurrentSortingColumn(column_r200))("clickable", ctx_r201.isSortable(column_r200));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r201.isCurrentSortingColumn(column_r200));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r201.isLabelVisibleInTable(column_r200) && ctx_r201.hasLabel(column_r200));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template(rf, ctx) { }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template, 0, 0, "ng-template", 9);
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r219 = ɵɵnextContext().$implicit;
    const column_r200 = ɵɵnextContext().$implicit;
    const ctx_r220 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵproperty("ngTemplateOutlet", ctx_r220.cellTemplate)("ngTemplateOutletContext", ɵɵpureFunction3(2, _c7, row_r219, column_r200, ctx_r220.extractValue(row_r219, column_r200)));
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const row_r219 = ɵɵnextContext().$implicit;
    const column_r200 = ɵɵnextContext().$implicit;
    const ctx_r221 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r221.extractValue(row_r219, column_r200), " ");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-cell");
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template, 2, 6, "div", 2);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template, 2, 1, "div", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r200 = ɵɵnextContext().$implicit;
    ɵɵclassMapInterpolate2("maissize-", column_r200.size || "default", " ", column_r200.cellDisplayClass || "", "");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", column_r200.applyTemplate);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !column_r200.applyTemplate);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0, 85);
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template, 4, 16, "mat-header-cell", 95);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template, 3, 6, "mat-cell", 96);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const column_r200 = ctx.$implicit;
    ɵɵproperty("matColumnDef", column_r200.name);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-header-row");
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-row", 97);
} if (rf & 2) {
    const row_r228 = ctx.$implicit;
    const ctx_r173 = ɵɵnextContext(4);
    ɵɵclassProp("expanded", ctx_r173.isMatTableExpanded(row_r228))("row-selected", ctx_r173.isChecked(row_r228));
    ɵɵproperty("cdkDragData", row_r228)("cdkDragDisabled", !ctx_r173.acceptDrag);
} }
function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-row", 98);
} if (rf & 2) {
    const row_r229 = ctx.$implicit;
    const ctx_r174 = ɵɵnextContext(4);
    ɵɵproperty("@detailExpand", ctx_r174.isMatTableExpanded(row_r229) ? "expanded" : "collapsed");
} }
const _c9 = function () { return ["internal___col_row_detail"]; };
function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template(rf, ctx) { if (rf & 1) {
    const _r231 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-table", 83, 84);
    ɵɵlistener("cdkDropListDropped", function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template_mat_table_cdkDropListDropped_0_listener($event) { ɵɵrestoreView(_r231); const ctx_r230 = ɵɵnextContext(3); return ctx_r230.handleItemDropped($event); });
    ɵɵelementContainerStart(2, 85);
    ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template, 1, 0, "mat-header-cell", 86);
    ɵɵtemplate(4, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template, 3, 2, "mat-cell", 87);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(5, 85);
    ɵɵtemplate(6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template, 3, 1, "mat-header-cell", 86);
    ɵɵtemplate(7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template, 3, 1, "mat-cell", 87);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(8, 85);
    ɵɵtemplate(9, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template, 9, 7, "mat-header-cell", 86);
    ɵɵtemplate(10, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template, 1, 0, "mat-cell", 87);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(11, 85);
    ɵɵtemplate(12, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template, 3, 4, "mat-cell", 88);
    ɵɵelementContainerEnd();
    ɵɵtemplate(13, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template, 3, 1, "ng-container", 89);
    ɵɵtemplate(14, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template, 1, 0, "mat-header-row", 90);
    ɵɵtemplate(15, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template, 1, 6, "mat-row", 91);
    ɵɵtemplate(16, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template, 1, 1, "mat-row", 92);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r160 = ɵɵnextContext(3);
    ɵɵclassProp("nodrop", !ctx_r160.acceptDrop)("acceptdrop", ctx_r160.acceptDrop);
    ɵɵpropertyInterpolate("id", ctx_r160.currentTableId);
    ɵɵproperty("dataSource", ctx_r160.matTableDataObservable)("cdkDropListConnectedTo", ctx_r160.dropConnectedTo)("cdkDropListData", ctx_r160.currentData)("cdkDropListEnterPredicate", ctx_r160.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r160.acceptDrop);
    ɵɵadvance(2);
    ɵɵproperty("matColumnDef", "internal___col_row_expansion");
    ɵɵadvance(3);
    ɵɵproperty("matColumnDef", "internal___col_row_selection");
    ɵɵadvance(3);
    ɵɵproperty("matColumnDef", "internal___col_context_filtering");
    ɵɵadvance(3);
    ɵɵproperty("matColumnDef", "internal___col_row_detail");
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r160.matTableVisibleColumns);
    ɵɵadvance(1);
    ɵɵproperty("matHeaderRowDef", ctx_r160.matTableVisibleColumnDefs);
    ɵɵadvance(1);
    ɵɵproperty("matRowDefColumns", ctx_r160.matTableVisibleColumnDefs);
    ɵɵadvance(1);
    ɵɵproperty("matRowDefColumns", ɵɵpureFunction0(19, _c9))("matRowDefWhen", ctx_r160.isMatTableExpansionDetailRow);
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 78);
    ɵɵelementStart(2, "p", 99);
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(4, 1, "table.common.messages.no_results"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 80);
    ɵɵelementStart(2, "p", 99);
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind1(4, 1, "table.common.messages.fetching"), " ");
} }
function MaisTableComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 48);
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_mat_table_1_Template, 17, 20, "mat-table", 82);
    ɵɵtemplate(2, MaisTableComponent_ng_container_3_div_1_ng_container_2_Template, 5, 3, "ng-container", 2);
    ɵɵtemplate(3, MaisTableComponent_ng_container_3_div_1_ng_container_3_Template, 5, 3, "ng-container", 2);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r159 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r159.showFetching && !ctx_r159.forceReRender);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r159.showFetching && ctx_r159.noResults);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r159.showFetching);
} }
function MaisTableComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_ng_container_3_div_1_Template, 4, 3, "div", 81);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r2.matTableDataObservable);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 113);
    ɵɵtext(1, "(current)");
    ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template(rf, ctx) { if (rf & 1) {
    const _r244 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "li", 106);
    ɵɵelementStart(1, "a", 107);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template_a_click_1_listener() { ɵɵrestoreView(_r244); const page_r238 = ɵɵnextContext().$implicit; const ctx_r242 = ɵɵnextContext(3); return ctx_r242.switchToPage(page_r238); });
    ɵɵtext(2);
    ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template, 2, 0, "span", 112);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const page_r238 = ɵɵnextContext().$implicit;
    const ctx_r239 = ɵɵnextContext(3);
    ɵɵclassProp("active", page_r238 === ctx_r239.currentPageIndex);
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", page_r238 + 1, " ");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", page_r238 === ctx_r239.currentPageIndex);
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "li", 114);
    ɵɵelementStart(1, "a", 115);
    ɵɵtext(2, " ... ");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function MaisTableComponent_div_4_ng_container_3_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template, 4, 4, "li", 110);
    ɵɵtemplate(2, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template, 3, 0, "li", 111);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const page_r238 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !page_r238.skip);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", page_r238.skip);
} }
function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r249 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 47);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template_button_click_0_listener() { ɵɵrestoreView(_r249); const pageSize_r247 = ctx.$implicit; const ctx_r248 = ɵɵnextContext(4); return ctx_r248.clickOnPageSize(pageSize_r247); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const pageSize_r247 = ctx.$implicit;
    const ctx_r246 = ɵɵnextContext(4);
    ɵɵproperty("disabled", pageSize_r247 === ctx_r246.currentPageSize);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", pageSize_r247, " ");
} }
const _c10 = function (a0) { return { current: a0 }; };
function MaisTableComponent_div_4_ng_container_3_span_25_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 116);
    ɵɵelementStart(1, "div");
    ɵɵtext(2, " \u00A0 ");
    ɵɵelementEnd();
    ɵɵelementStart(3, "button", 117);
    ɵɵtext(4);
    ɵɵpipe(5, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(6, "span", 44);
    ɵɵtemplate(7, MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template, 2, 2, "button", 46);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r237 = ɵɵnextContext(3);
    ɵɵadvance(4);
    ɵɵtextInterpolate(ɵɵpipeBind2(5, 2, "table.common.messages.page_size_button", ɵɵpureFunction1(5, _c10, ctx_r237.currentPageSize)));
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", ctx_r237.currentPossiblePageSizes);
} }
const _c11 = function (a0, a1) { return { totalElements: a0, totalPages: a1 }; };
function MaisTableComponent_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    const _r251 = ɵɵgetCurrentView();
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "div", 102);
    ɵɵelementStart(2, "div", 103);
    ɵɵtext(3);
    ɵɵpipe(4, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(5, "nav", 104);
    ɵɵelementStart(6, "ul", 105);
    ɵɵelementStart(7, "li", 106);
    ɵɵelementStart(8, "a", 107);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_8_listener() { ɵɵrestoreView(_r251); const ctx_r250 = ɵɵnextContext(2); return ctx_r250.switchToPage(0); });
    ɵɵtext(9);
    ɵɵpipe(10, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(11, "li", 106);
    ɵɵelementStart(12, "a", 107);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_12_listener() { ɵɵrestoreView(_r251); const ctx_r252 = ɵɵnextContext(2); return ctx_r252.switchToPage(ctx_r252.currentPageIndex - 1); });
    ɵɵtext(13);
    ɵɵpipe(14, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(15, MaisTableComponent_div_4_ng_container_3_ng_container_15_Template, 3, 2, "ng-container", 27);
    ɵɵelementStart(16, "li", 106);
    ɵɵelementStart(17, "a", 107);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_17_listener() { ɵɵrestoreView(_r251); const ctx_r253 = ɵɵnextContext(2); return ctx_r253.switchToPage(ctx_r253.currentPageIndex + 1); });
    ɵɵtext(18);
    ɵɵpipe(19, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(20, "li", 106);
    ɵɵelementStart(21, "a", 107);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_21_listener() { ɵɵrestoreView(_r251); const ctx_r254 = ɵɵnextContext(2); return ctx_r254.switchToPage(ctx_r254.currentPageCount - 1); });
    ɵɵtext(22);
    ɵɵpipe(23, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(24, "div", 108);
    ɵɵtemplate(25, MaisTableComponent_div_4_ng_container_3_span_25_Template, 8, 7, "span", 109);
    ɵɵelementEnd();
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r232 = ɵɵnextContext(2);
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ɵɵpipeBind2(4, 15, "table.common.pagination.total_elements", ɵɵpureFunction2(26, _c11, ctx_r232.currentResultNumber, ctx_r232.currentPageCount)), " ");
    ɵɵadvance(4);
    ɵɵclassProp("disabled", !(ctx_r232.currentPageIndex > 0));
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(10, 18, "table.common.pagination.first_page"));
    ɵɵadvance(2);
    ɵɵclassProp("disabled", !(ctx_r232.currentPageIndex > 0));
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(14, 20, "table.common.pagination.previous_page"));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r232.enumPages);
    ɵɵadvance(1);
    ɵɵclassProp("disabled", ctx_r232.currentPageIndex >= ctx_r232.currentPageCount - 1);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(19, 22, "table.common.pagination.next_page"));
    ɵɵadvance(2);
    ɵɵclassProp("disabled", ctx_r232.currentPageIndex >= ctx_r232.currentPageCount - 1);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind1(23, 24, "table.common.pagination.last_page"));
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r232.pageSizeSelectEnabledAndPossible);
} }
function MaisTableComponent_div_4_ng_template_6_Template(rf, ctx) { }
function MaisTableComponent_div_4_ng_container_8_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r258 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 41);
    ɵɵlistener("click", function MaisTableComponent_div_4_ng_container_8_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r258); const action_r256 = ctx.$implicit; const ctx_r257 = ɵɵnextContext(3); return ctx_r257.clickOnContextAction(action_r256); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const action_r256 = ctx.$implicit;
    const ctx_r255 = ɵɵnextContext(3);
    ɵɵclassMapInterpolate2("btn btn-", action_r256.displayClass || "light", " ", action_r256.additionalClasses || "", " mr-1");
    ɵɵproperty("disabled", !ctx_r255.isContextActionAllowed(action_r256));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r255.resolveLabel(action_r256), " ");
} }
function MaisTableComponent_div_4_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, MaisTableComponent_div_4_ng_container_8_button_1_Template, 2, 6, "button", 40);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r234 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r234.headerActions);
} }
function MaisTableComponent_div_4_div_9_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r262 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 47);
    ɵɵlistener("click", function MaisTableComponent_div_4_div_9_button_9_Template_button_click_0_listener() { ɵɵrestoreView(_r262); const action_r260 = ctx.$implicit; const ctx_r261 = ɵɵnextContext(3); return ctx_r261.clickOnContextAction(action_r260); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const action_r260 = ctx.$implicit;
    const ctx_r259 = ɵɵnextContext(3);
    ɵɵproperty("disabled", !ctx_r259.isContextActionAllowed(action_r260));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r259.resolveLabel(action_r260), " ");
} }
function MaisTableComponent_div_4_div_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 42);
    ɵɵelementStart(1, "button", 43);
    ɵɵtext(2);
    ɵɵpipe(3, "translate");
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 44);
    ɵɵelementStart(5, "div", 45);
    ɵɵelementStart(6, "h6", 21);
    ɵɵtext(7);
    ɵɵpipe(8, "translate");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(9, MaisTableComponent_div_4_div_9_button_9_Template, 2, 2, "button", 46);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r235 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("disabled", !ctx_r235.anyButtonActionsAllowed);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ɵɵpipeBind1(3, 4, "table.common.messages.actions_button"));
    ɵɵadvance(5);
    ɵɵtextInterpolate(ɵɵpipeBind1(8, 6, "table.common.messages.pick_action"));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r235.currentActions);
} }
function MaisTableComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 100);
    ɵɵelementStart(1, "div", 5);
    ɵɵelementStart(2, "div", 101);
    ɵɵtemplate(3, MaisTableComponent_div_4_ng_container_3_Template, 26, 29, "ng-container", 2);
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 8);
    ɵɵelementStart(5, "div");
    ɵɵtemplate(6, MaisTableComponent_div_4_ng_template_6_Template, 0, 0, "ng-template", 9);
    ɵɵtext(7, " \u00A0 ");
    ɵɵelementEnd();
    ɵɵtemplate(8, MaisTableComponent_div_4_ng_container_8_Template, 2, 1, "ng-container", 2);
    ɵɵtemplate(9, MaisTableComponent_div_4_div_9_Template, 10, 8, "div", 10);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = ɵɵnextContext();
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r3.currentResultNumber > 0 && ctx_r3.currentEnablePagination);
    ɵɵadvance(3);
    ɵɵproperty("ngTemplateOutlet", ctx_r3.actionsCaptionTemplate)("ngTemplateOutletContext", ɵɵpureFunction0(5, _c6));
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r3.headerActionsEnabledAndPossible);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r3.contextActionsEnabledAndPossible);
} }
class MaisTableComponent {
    // lifecycle hooks
    constructor(translateService, configurationService, registry, cdr, ngZone) {
        this.translateService = translateService;
        this.configurationService = configurationService;
        this.registry = registry;
        this.cdr = cdr;
        this.ngZone = ngZone;
        this.dataProvider = null;
        this.defaultSortingColumn = null;
        this.defaultSortingDirection = null;
        this.defaultPageSize = null;
        this.possiblePageSize = null;
        // output events
        this.pageChange = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.selectionChange = new EventEmitter();
        this.filteringColumnsChange = new EventEmitter();
        this.visibleColumnsChange = new EventEmitter();
        this.contextFiltersChange = new EventEmitter();
        this.statusChange = new EventEmitter();
        this.itemDragged = new EventEmitter();
        this.itemDropped = new EventEmitter();
        this.action = new EventEmitter();
        this.isIE = MaisTableBrowserHelper.isIE();
        this.statusSnapshot = null;
        this.persistableStatusSnapshot = null;
        this.forceReRender = false;
        this.initialized = false;
        this.fetching = false;
        this.showFetching = false;
        this.refreshEmitterSubscription = null;
        this.refreshIntervalSubscription = null;
        // MatTable adapter
        this.matTableDataObservable = null;
        this.expandedElement = null;
        // drag and drop support
        this.acceptDropPredicate = (item) => {
            return this.acceptDrop;
        };
        this.isMatTableExpansionDetailRow = (i, row) => row.hasOwnProperty('___detailRowContent');
        this.isMatTableExpanded = (row, limit = false) => {
            if (!limit && row.___detailRow) {
                if (this.isMatTableExpanded(row.___detailRow, true)) {
                    return true;
                }
            }
            if (!limit && row.___parentRow) {
                if (this.isMatTableExpanded(row.___parentRow, true)) {
                    return true;
                }
            }
            if (!this.rowExpansionEnabledAndPossible) {
                return false;
            }
            if (!this.isExpandable(row)) {
                return false;
            }
            if (!this.isExpanded(row)) {
                return false;
            }
            return true;
        };
        this.uuid = 'MTCMP-' + (++MaisTableComponent.counter) + '-' + Math.round(Math.random() * 100000);
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('building component');
        this.matTableDataObservable = new Subject();
    }
    ngOnInit() {
        this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
        this.logger.trace('initializing component');
        this.registrationId = this.registry.register(this.currentTableId, this);
        // input validation
        MaisTableValidatorHelper.validateColumnsSpecification(this.columns, this.configurationService.getConfiguration());
        this.clientDataSnapshot = [];
        this.dataSnapshot = [];
        this.fetchedPageCount = 0;
        this.fetchedResultNumber = 0;
        this.selectedPageIndex = 0;
        this.selectedSearchQuery = null;
        this.selectedPageSize = null;
        this.expandedItems = [];
        this.checkedItems = [];
        this.checkedContextFilters = [];
        this.checkedColumnsForFiltering = this.filterableColumns.filter(c => MaisTableFormatterHelper.isDefaultFilterColumn(c, this.configurationService.getConfiguration()));
        this.checkedColumnsForVisualization = this.columns.filter(c => MaisTableFormatterHelper.isDefaultVisibleColumn(c, this.configurationService.getConfiguration()));
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        if (this.dataProvider) {
            // input is a provider function
            if (this.data) {
                throw new Error('MaisTable can\'t be provided both data and dataProvider');
            }
        }
        else if (this.data instanceof Observable || this.data instanceof Subject) {
            // input is observable
            this.data.subscribe(dataSnapshot => {
                this.handleInputDataObservableEmission(dataSnapshot);
            });
        }
        else {
            // input is data array
            this.clientDataSnapshot = this.data;
        }
        // LOAD STATUS from store if possible
        let activationObservable;
        if (this.storePersistenceEnabledAndPossible) {
            activationObservable = this.loadStatusFromStore();
        }
        else {
            activationObservable = new Observable(subscriber => {
                subscriber.next(null);
                subscriber.complete();
            });
        }
        activationObservable.subscribe((status) => {
            if (status) {
                this.logger.trace('restored status snapshot from storage', status);
                this.applyStatusSnapshot(status);
            }
            this.reload({ reason: MaisTableReloadReason.INTERNAL, withStatusSnapshot: status }).subscribe(yes => {
                this.completeInitialization();
            }, nope => {
                this.completeInitialization();
            });
        }, failure => {
            this.logger.error('restoring status from store failed', failure);
            this.reload({ reason: MaisTableReloadReason.INTERNAL }).subscribe(yes => {
                this.completeInitialization();
            }, nope => {
                this.completeInitialization();
            });
        });
    }
    ngAfterContentInit() {
        this.logger.debug('after content init');
    }
    handleInputDataObservableEmission(dataSnapshot) {
        this.logger.debug('data snapshot emitted from input data');
        this.clientDataSnapshot = dataSnapshot;
        this.reload({ reason: MaisTableReloadReason.EXTERNAL });
    }
    completeInitialization() {
        this.initialized = true;
        this.statusChange.pipe(debounceTime(200)).subscribe(statusSnapshot => {
            this.persistStatusToStore().subscribe();
        });
        if (this.refreshEmitter) {
            this.handleRefreshEmitterChange(this.refreshEmitter);
        }
        if (this.refreshInterval) {
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    }
    ngOnDestroy() {
        this.logger.trace('destroying component');
        if (this.registrationId) {
            this.registry.unregister(this.currentTableId, this.registrationId);
        }
    }
    ngOnChanges(changes) {
        if (changes.refreshEmitter) {
            this.handleRefreshEmitterChange(changes.refreshEmitter.currentValue);
        }
        if (changes.refreshInterval) {
            this.handleRefreshIntervalChange(changes.refreshInterval.currentValue);
        }
        if (changes.refreshStrategy && !changes.refreshStrategy.firstChange) {
            this.logger.warn('REFRESH STRATEGY CHANGED WHILE RUNNING. YOU SURE ABOUT THIS?', changes);
            this.handleRefreshEmitterChange(this.refreshEmitter);
            this.handleRefreshIntervalChange(this.refreshInterval);
        }
    }
    handleRefreshEmitterChange(newValue) {
        if (this.refreshEmitterSubscription) {
            this.refreshEmitterSubscription.unsubscribe();
        }
        if (newValue) {
            this.refreshEmitterSubscription = newValue.subscribe(event => {
                this.logger.trace('received refresh push request', event);
                if (this.dragInProgress) {
                    this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.ON_PUSH) === -1) {
                    this.logger.warn('refresh from emitter ignored because refresh strategies are ' + this.currentRefreshStrategies +
                        '. Why are you pushing to this component?');
                }
                else if (!this.initialized) {
                    this.logger.warn('refresh from emitter ignored because the component is not fully initialized');
                }
                else if (this.fetching) {
                    this.logger.warn('refresh from emitter ignored because the component is fetching already');
                }
                else {
                    this.logger.debug('launching reload following push request');
                    this.reload({
                        reason: MaisTableReloadReason.PUSH,
                        pushRequest: event,
                        inBackground: event.inBackground === true || event.inBackground === false ?
                            event.inBackground : this.currentRefreshOnPushInBackground
                    });
                }
            });
        }
    }
    handleRefreshIntervalChange(newValue) {
        if (this.refreshIntervalSubscription) {
            this.refreshIntervalSubscription.unsubscribe();
        }
        if (this.refreshIntervalTimer) {
            this.refreshIntervalTimer = null;
        }
        if (newValue) {
            this.refreshIntervalTimer = timer(newValue, newValue);
            this.refreshIntervalSubscription = this.refreshIntervalTimer.subscribe(tick => {
                this.logger.trace('emitted refresh tick request');
                if (this.dragInProgress) {
                    this.logger.warn('refresh from emitter ignored because user is dragging elements');
                }
                else if (this.currentRefreshStrategies.indexOf(MaisTableRefreshStrategy.TIMED) === -1) {
                    this.logger.warn('refresh from tick ignored because refresh strategies are ' + this.currentRefreshStrategies +
                        '. Why is this component emitting ticks ?');
                }
                else if (!this.initialized) {
                    this.logger.warn('refresh from tick ignored because the component is not fully initialized');
                }
                else if (this.fetching) {
                    this.logger.warn('refresh from tick ignored because the component is fetching already');
                }
                else {
                    this.logger.debug('launching reload following tick');
                    this.reload({
                        reason: MaisTableReloadReason.INTERVAL,
                        inBackground: this.currentRefreshIntervalInBackground
                    });
                }
            });
        }
    }
    // public methods
    refresh(background) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        return this.reload({ reason: MaisTableReloadReason.EXTERNAL, inBackground: background });
    }
    getDataSnapshot() {
        return this.dataSnapshot;
    }
    getStatusSnapshot() {
        return this.statusSnapshot;
    }
    loadStatus(status) {
        if (!this.initialized) {
            return throwError('table component is still initializing');
        }
        this.logger.trace('loading status snapshot from external caller', status);
        this.applyStatusSnapshot(status);
        this.reload({ reason: MaisTableReloadReason.EXTERNAL, withStatusSnapshot: status });
    }
    applyStatusSnapshot(status) {
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (status.orderColumn) {
            this.selectedSortColumn = this.columns.find(c => this.isSortable(c) && c.name === status.orderColumn) || null;
        }
        if (status.orderColumnDirection) {
            this.selectedSortDirection = (status.orderColumnDirection === MaisTableSortDirection.DESCENDING) ?
                MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
        }
        if (this.currentEnableFiltering && status.query) {
            this.selectedSearchQuery = status.query.trim();
        }
        if (this.currentEnableFiltering && status.queryColumns && status.queryColumns.length) {
            this.checkedColumnsForFiltering = this.filterableColumns.filter(c => { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.queryColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableColumnsSelection && status.visibleColumns && status.visibleColumns.length) {
            this.checkedColumnsForVisualization = this.columns.filter(c => { var _a, _b; return !this.isHideable(c) || ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.visibleColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
        }
        if (this.currentEnableContextFiltering && status.contextFilters && status.contextFilters.length) {
            this.checkedContextFilters = this.contextFilters.filter(f => { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.contextFilters) === null || _b === void 0 ? void 0 : _b.indexOf(f.name)) !== -1; });
        }
        if (this.currentEnablePagination && status.currentPage || status.currentPage === 0) {
            this.selectedPageIndex = status.currentPage;
        }
        if (this.currentEnablePagination && status.pageSize) {
            this.selectedPageSize = this.currentPossiblePageSizes.find(s => status.pageSize === s) || null;
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    }
    applyStatusSnapshotPostFetch(status) {
        if (!status || !status.schemaVersion) {
            this.logger.warn('not restoring status because it is malformed');
            return;
        }
        if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
            this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
            return;
        }
        if (this.currentEnableSelection && this.itemTrackingEnabledAndPossible
            && status.checkedItemIdentifiers && status.checkedItemIdentifiers.length) {
            this.checkedItems = this.dataSnapshot.filter(data => {
                var _a, _b;
                const id = this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.checkedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1;
            });
        }
        if (this.currentEnableRowExpansion && this.itemTrackingEnabledAndPossible
            && status.expandedItemIdentifiers && status.expandedItemIdentifiers.length) {
            this.expandedItems = this.dataSnapshot.filter(data => {
                var _a, _b;
                const id = this.getItemIdentifier(data);
                return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.expandedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1 && this.isExpandable(data);
            });
        }
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
    }
    reload(context) {
        this.fetching = true;
        this.showFetching = !context.inBackground;
        const obs = new Observable(subscriber => {
            try {
                this.reloadInObservable(subscriber, context);
            }
            catch (e) {
                subscriber.error(e);
                subscriber.complete();
            }
        });
        obs.subscribe(success => {
            this.logger.trace('async reload success');
            this.fetching = false;
            if (!context.inBackground) {
                this.showFetching = false;
            }
            this.handleMatTableDataSnapshotChanged();
        }, failure => {
            this.logger.trace('async reload failed', failure);
            this.fetching = false;
            if (!context.inBackground) {
                this.showFetching = false;
            }
            this.handleMatTableDataSnapshotChanged();
        });
        return obs;
    }
    reloadInObservable(tracker, context) {
        const withSnapshot = context.withStatusSnapshot || null;
        const pageRequest = this.buildPageRequest();
        this.logger.debug('reloading table data', pageRequest);
        // clear checked items
        if (!context.inBackground) {
            this.checkedItems = [];
            this.expandedItems = [];
            this.statusChanged();
        }
        this.lastFetchedSearchQuery = pageRequest.query || null;
        if (this.dataProvider) {
            // call data provider
            this.logger.trace('reload has been called, fetching data from provided function');
            this.logger.trace('page request for data fetch is', pageRequest);
            this.dataProvider(pageRequest, context).subscribe((response) => {
                this.logger.trace('fetching data completed successfully');
                if (!this.dragInProgress) {
                    if (this.paginationMode === MaisTablePaginationMethod.SERVER) {
                        this.parseResponseWithServerPagination(response);
                    }
                    else {
                        this.parseResponseWithClientPagination(response.content, pageRequest);
                    }
                    if (withSnapshot) {
                        this.applyStatusSnapshotPostFetch(withSnapshot);
                    }
                }
                else {
                    this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.next();
                tracker.complete();
            }, failure => {
                this.logger.error('error fetching data from provider function', failure);
                if (!this.dragInProgress) {
                    this.dataSnapshot = [];
                }
                else {
                    this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.error(failure);
                tracker.complete();
            });
        }
        else {
            // data is not provided on request and is in clientDataSnapshot
            this.logger.trace('reload has been called on locally fetched data');
            if (!this.dragInProgress) {
                this.parseResponseWithClientPagination(this.clientDataSnapshot, pageRequest);
                if (withSnapshot) {
                    this.applyStatusSnapshotPostFetch(withSnapshot);
                }
            }
            else {
                this.logger.warn('data fetch aborted because user is dragging things');
            }
            tracker.next();
            tracker.complete();
        }
    }
    parseResponseWithServerPagination(response) {
        this.dataSnapshot = response.content;
        if (this.currentEnablePagination) {
            if (response.totalPages) {
                this.fetchedPageCount = response.totalPages;
            }
            else {
                throw new Error('data from server did not contain required totalPages field');
            }
            if (response.totalElements) {
                this.fetchedResultNumber = response.totalElements;
            }
            else {
                throw new Error('data from server did not contain required totalElements field');
            }
        }
    }
    parseResponseWithClientPagination(data, request) {
        this.logger.trace('applying in-memory fetching, paginating, ordering and filtering');
        const inMemoryResponse = MaisTableInMemoryHelper.fetchInMemory(data, request, this.currentSortColumn, this.checkedColumnsForFiltering, this.getCurrentLocale());
        this.dataSnapshot = inMemoryResponse.content;
        this.fetchedResultNumber = typeof inMemoryResponse.totalElements === 'undefined' ? null : inMemoryResponse.totalElements;
        this.fetchedPageCount = typeof inMemoryResponse.totalPages === 'undefined' ? null : inMemoryResponse.totalPages;
    }
    buildPageRequest() {
        var _a, _b;
        const output = {
            page: this.currentEnablePagination ? this.currentPageIndex : null,
            size: this.currentEnablePagination ? this.currentPageSize : null,
            sort: [],
            query: null,
            queryFields: [],
            filters: []
        };
        if (this.currentEnableFiltering && this.searchQueryActive) {
            output.query = this.currentSearchQuery;
            output.queryFields = this.checkedColumnsForFiltering.map(column => column.serverField || column.field || null);
        }
        if (this.currentEnableContextFiltering && this.checkedContextFilters.length) {
            for (const filter of this.checkedContextFilters) {
                (_a = output.filters) === null || _a === void 0 ? void 0 : _a.push(filter.name || null);
            }
        }
        const sortColumn = this.currentSortColumn;
        const sortDirection = this.currentSortDirection;
        if (sortColumn) {
            (_b = output.sort) === null || _b === void 0 ? void 0 : _b.push({
                property: sortColumn.serverField || sortColumn.field || null,
                direction: sortDirection || MaisTableSortDirection.ASCENDING
            });
        }
        return output;
    }
    setPage(index) {
        this.selectedPageIndex = index;
        this.statusChanged();
        this.emitPageChanged();
    }
    get currentRefreshOnPushInBackground() {
        var _a;
        if (this.refreshOnPushInBackground === true || this.refreshOnPushInBackground === false) {
            return this.refreshOnPushInBackground;
        }
        else {
            return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnPushInBackground;
        }
    }
    get currentRefreshIntervalInBackground() {
        var _a;
        if (this.refreshIntervalInBackground === true || this.refreshIntervalInBackground === false) {
            return this.refreshIntervalInBackground;
        }
        else {
            return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnTickInBackground;
        }
    }
    get currentRefreshStrategies() {
        var _a;
        if (this.refreshStrategy) {
            if (Array.isArray(this.refreshStrategy) && this.refreshStrategy.length) {
                return this.refreshStrategy;
            }
            else {
                return [this.refreshStrategy];
            }
        }
        return [(_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultStrategy];
    }
    get currentRefreshInterval() {
        var _a;
        return this.refreshInterval || ((_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultInterval);
    }
    get currentTableId() {
        return this.tableId || this.uuid;
    }
    get currentEnableDrag() {
        var _a;
        if (this.enableDrag === true || this.enableDrag === false) {
            return this.enableDrag;
        }
        else {
            return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dragEnabledByDefault;
        }
    }
    get currentEnableDrop() {
        var _a;
        if (this.enableDrop === true || this.enableDrop === false) {
            return this.enableDrop;
        }
        else {
            return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dropEnabledByDefault;
        }
    }
    get currentEnablePagination() {
        var _a;
        if (this.enablePagination === true || this.enablePagination === false) {
            return this.enablePagination;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableItemTracking() {
        var _a;
        if (this.enableItemTracking === true || this.enableItemTracking === false) {
            return this.enableItemTracking;
        }
        else {
            return (_a = this.configurationService.getConfiguration().itemTracking) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableStorePersistence() {
        var _a;
        if (this.enableStorePersistence === true || this.enableStorePersistence === false) {
            return this.enableStorePersistence;
        }
        else {
            return (_a = this.configurationService.getConfiguration().storePersistence) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnablePageSizeSelect() {
        var _a;
        if (this.enablePageSizeSelect === true || this.enablePageSizeSelect === false) {
            return this.enablePageSizeSelect;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.pageSizeSelectionEnabledByDefault;
        }
    }
    get currentEnableMultipleRowExpansion() {
        var _a;
        if (this.enableMultipleRowExpansion === true || this.enableMultipleRowExpansion === false) {
            return this.enableMultipleRowExpansion;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.multipleExpansionEnabledByDefault;
        }
    }
    get currentEnableRowExpansion() {
        var _a;
        if (this.enableRowExpansion === true || this.enableRowExpansion === false) {
            return this.enableRowExpansion;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableHeaderActions() {
        var _a;
        if (this.enableHeaderActions === true || this.enableHeaderActions === false) {
            return this.enableHeaderActions;
        }
        else {
            return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.headerActionsEnabledByDefault;
        }
    }
    get currentEnableContextActions() {
        var _a;
        if (this.enableContextActions === true || this.enableContextActions === false) {
            return this.enableContextActions;
        }
        else {
            return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.contextActionsEnabledByDefault;
        }
    }
    get currentEnableColumnsSelection() {
        var _a;
        if (this.enableColumnsSelection === true || this.enableColumnsSelection === false) {
            return this.enableColumnsSelection;
        }
        else {
            return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableFiltering() {
        var _a;
        if (this.enableFiltering === true || this.enableFiltering === false) {
            return this.enableFiltering;
        }
        else {
            return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableMultiSelect() {
        var _a;
        if (this.enableMultiSelect === true || this.enableMultiSelect === false) {
            return this.enableMultiSelect;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.multipleSelectionEnabledByDefault;
        }
    }
    get currentEnableSelectAll() {
        var _a;
        if (this.enableSelectAll === true || this.enableSelectAll === false) {
            return this.enableSelectAll;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.selectAllEnabledByDefault;
        }
    }
    get currentEnableContextFiltering() {
        var _a;
        if (this.enableContextFiltering === true || this.enableContextFiltering === false) {
            return this.enableContextFiltering;
        }
        else {
            return (_a = this.configurationService.getConfiguration().contextFiltering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentEnableSelection() {
        var _a;
        if (this.enableSelection === true || this.enableSelection === false) {
            return this.enableSelection;
        }
        else {
            return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
        }
    }
    get currentPaginationMode() {
        var _a;
        if (this.paginationMode) {
            return this.paginationMode;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPaginationMode;
        }
    }
    get itemTrackingEnabledAndPossible() {
        return !!this.itemIdentifier && this.currentEnableItemTracking;
    }
    get storePersistenceEnabledAndPossible() {
        return this.storeAdapter && this.currentEnableStorePersistence;
    }
    get rowExpansionEnabledAndPossible() {
        return this.currentEnableRowExpansion;
    }
    get pageSizeSelectEnabledAndPossible() {
        return this.currentPossiblePageSizes && this.currentPossiblePageSizes.length > 0 && this.currentEnablePageSizeSelect;
    }
    get contextFilteringEnabledAndPossible() {
        return this.contextFilters && this.contextFilters.length > 0 && this.currentEnableContextFiltering;
    }
    get contextActionsEnabledAndPossible() {
        return this.contextActions && this.contextActions.length > 0 && this.currentEnableContextActions;
    }
    get headerActionsEnabledAndPossible() {
        return this.headerActions && this.headerActions.length > 0 && this.currentEnableHeaderActions;
    }
    get columnSelectionPossibleAndAllowed() {
        return this.currentEnableColumnsSelection && this.columns.length > 0 && this.hideableColumns.length > 0;
    }
    get filteringPossibleAndAllowed() {
        return this.currentEnableFiltering && this.filterableColumns.length > 0;
    }
    get currentPossiblePageSizes() {
        var _a;
        if (this.possiblePageSize && this.possiblePageSize.length) {
            return this.possiblePageSize;
        }
        else {
            return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPossiblePageSizes;
        }
    }
    get currentSearchQuery() {
        if (this.selectedSearchQuery) {
            return this.selectedSearchQuery;
        }
        else {
            return null;
        }
    }
    get currentPageCount() {
        return this.fetchedPageCount || 0;
    }
    get currentResultNumber() {
        return this.fetchedResultNumber || 0;
    }
    get currentPageSize() {
        var _a;
        const def = (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPageSize;
        if (this.selectedPageSize) {
            return this.selectedPageSize;
        }
        else if (this.defaultPageSize) {
            return this.defaultPageSize;
        }
        else if (this.currentPossiblePageSizes.length &&
            def &&
            this.currentPossiblePageSizes.indexOf(def) !== -1) {
            return def;
        }
        else if (this.currentPossiblePageSizes.length) {
            return this.currentPossiblePageSizes[0];
        }
        else if (def) {
            return def;
        }
        else {
            return 10;
        }
    }
    get currentPageIndex() {
        if (this.selectedPageIndex) {
            return this.selectedPageIndex;
        }
        else {
            return 0;
        }
    }
    get currentSortColumn() {
        if (this.selectedSortColumn) {
            // todo log if sorting column disappears
            return this.visibleColumns.find(c => { var _a; return c.name === ((_a = this.selectedSortColumn) === null || _a === void 0 ? void 0 : _a.name); }) || null;
        }
        else if (this.defaultSortingColumn) {
            const foundColumn = this.visibleColumns.find(c => c.name === this.defaultSortingColumn);
            if (foundColumn) {
                return foundColumn;
            }
            else {
                // find first sortable column?
                return this.visibleColumns.find(column => MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
            }
        }
        else {
            // find first sortable column?
            return this.visibleColumns.find(column => MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) || null;
        }
    }
    get currentSortDirection() {
        if (this.selectedSortDirection) {
            return this.selectedSortDirection;
        }
        else if (this.defaultSortingDirection) {
            return this.defaultSortingDirection === MaisTableSortDirection.DESCENDING ?
                MaisTableSortDirection.DESCENDING : MaisTableSortDirection.ASCENDING;
        }
        else {
            // return DEFAULT
            return MaisTableSortDirection.ASCENDING;
        }
    }
    get currentData() {
        return this.dataSnapshot || [];
    }
    get enumPages() {
        const rangeStart = 1;
        const rangeEnd = 1;
        const rangeSelected = 1;
        const pages = [];
        const pageNum = this.currentPageCount;
        const currentIndex = this.currentPageIndex;
        let isSkipping = false;
        for (let i = 0; i < pageNum; i++) {
            if (Math.abs(i - currentIndex) <= (rangeSelected) || i <= (rangeStart - 1) || i >= (pageNum - rangeEnd)) {
                isSkipping = false;
                pages.push(i);
            }
            else {
                if (!isSkipping) {
                    pages.push({
                        skip: true
                    });
                    isSkipping = true;
                }
            }
        }
        return pages;
    }
    get activeColumnsCount() {
        let output = this.visibleColumns.length;
        if (this.currentEnableSelection) {
            output++;
        }
        if (this.contextFilteringEnabledAndPossible) {
            output++;
        }
        if (this.rowExpansionEnabledAndPossible) {
            output++;
        }
        return output;
    }
    get noResults() {
        if (this.paginationMode === MaisTablePaginationMethod.CLIENT) {
            return !this.currentData.length;
        }
        else {
            return !this.dataSnapshot.length;
        }
    }
    get hideableColumns() {
        return this.columns.filter(c => MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
    }
    get visibleColumns() {
        return this.columns.filter(c => this.checkedColumnsForVisualization.indexOf(c) !== -1);
    }
    get currentContextFilters() {
        return this.contextFilters || [];
    }
    get currentHeaderActions() {
        return this.headerActions || [];
    }
    get currentActions() {
        return this.contextActions || [];
    }
    get hasActions() {
        return this.currentActions.length > 0;
    }
    get searchQueryNeedApply() {
        return (this.currentSearchQuery || '') !== (this.lastFetchedSearchQuery || '');
    }
    get searchQueryActive() {
        if (!this.currentSearchQuery) {
            return false;
        }
        if (!this.checkedColumnsForFiltering.length) {
            return false;
        }
        return true;
    }
    get searchQueryMalformed() {
        if (!this.currentSearchQuery) {
            return false;
        }
        if (!this.checkedColumnsForFiltering.length) {
            return true;
        }
        return false;
    }
    get filterableColumns() {
        return this.columns.filter(c => this.isFilterable(c));
    }
    get anyButtonActionsAllowed() {
        return !!this.contextActions.find(a => this.isContextActionAllowed(a));
    }
    get anyHeaderActionsAllowed() {
        return !!this.headerActions.find(a => this.isHeaderActionAllowed(a));
    }
    get configColumnVisibilityShowFixedColumns() {
        var _a;
        return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.showFixedColumns;
    }
    get configFilteringShowUnfilterableColumns() {
        var _a;
        return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.showUnfilterableColumns;
    }
    get acceptDrop() {
        return this.currentEnableDrop;
    }
    get acceptDrag() {
        return this.currentEnableDrag;
    }
    get referencedTable() {
        const v = this.getReferencedTable();
        return v;
    }
    getReferencedTable() {
        if (this.dropConnectedTo && this.dropConnectedTo.length) {
            const connectedToList = (Array.isArray(this.dropConnectedTo)) ? this.dropConnectedTo : [this.dropConnectedTo];
            const found = [];
            for (const connectedToToken of connectedToList) {
                const registeredList = this.registry.get(connectedToToken);
                if (registeredList && registeredList.length) {
                    for (const regComp of registeredList) {
                        found.push(regComp);
                    }
                }
            }
            if (found.length < 1) {
                return null;
            }
            else if (found.length > 1) {
                this.logger.error('MULTIPLE TABLE REFERENCED STILL ACTIVE', found);
                return null;
            }
            else {
                return found[0].component;
            }
        }
        else {
            return this;
        }
    }
    // user actions
    handleItemDragged(event, from, to) {
        this.logger.debug('item dragged from table ' + this.currentTableId);
        this.itemDragged.emit({
            item: event.item.data,
            event,
            fromDataSnapshot: from.getDataSnapshot(),
            toDataSnapshot: to.getDataSnapshot(),
            fromComponent: from,
            toComponent: to
        });
    }
    handleItemDropped(event) {
        this.logger.debug('DROP EVENT FROM TABLE ' + this.currentTableId, event);
        const droppedSource = this.registry.getSingle(event.previousContainer.id);
        const droppedTarget = this.registry.getSingle(event.container.id);
        if (!droppedTarget) {
            this.logger.warn('NO DROP TARGET FOUND');
            return;
        }
        if (!droppedTarget.acceptDrop) {
            this.logger.debug('skipping drop on container with acceptDrop = false');
            return;
        }
        if (droppedSource) {
            droppedSource.handleItemDragged(event, droppedSource, droppedTarget);
        }
        this.logger.debug('item dropped on table ' + droppedTarget.currentTableId);
        this.itemDropped.emit({
            item: event.item.data,
            event,
            fromDataSnapshot: droppedSource ? droppedSource.getDataSnapshot() : null,
            toDataSnapshot: droppedTarget.getDataSnapshot(),
            fromComponent: droppedSource,
            toComponent: droppedTarget
        });
        // I'm sorry
        this.cdr.detectChanges();
        this.ngZone.run(() => {
            this.dataSnapshot = this.dataSnapshot.map(o => o);
            this.cdr.detectChanges();
            setTimeout(() => this.forceReRender = true);
            setTimeout(() => this.forceReRender = false);
            this.handleMatTableDataSnapshotChanged();
        });
    }
    applySelectedFilter() {
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    clickOnRowExpansion(item) {
        if (!this.rowExpansionEnabledAndPossible) {
            return;
        }
        if (!this.isExpandable(item)) {
            return;
        }
        if (this.isExpanded(item)) {
            this.expandedItems.splice(this.expandedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultipleRowExpansion) {
                this.expandedItems = [];
            }
            this.expandedItems.push(item);
        }
        this.handleMatTableDataSnapshotChanged();
        this.statusChanged();
    }
    clickOnContextAction(action) {
        if (!this.currentEnableContextActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isContextActionAllowed(action)) {
            return;
        }
        const dispatchContext = {
            action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    }
    clickOnPageSize(pageSize) {
        this.selectedPageSize = pageSize;
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    clickOnHeaderAction(action) {
        if (!this.currentEnableHeaderActions) {
            this.logger.warn('button actions are disabled');
            return;
        }
        if (!this.isHeaderActionAllowed(action)) {
            return;
        }
        const dispatchContext = {
            action,
            selectedItems: this.checkedItems
        };
        this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
        this.action.emit(dispatchContext);
    }
    searchQueryFocusOut() {
        var _a;
        const comp = this;
        setTimeout(() => {
            var _a, _b;
            if ((comp.selectedSearchQuery || '') !== (comp.lastFetchedSearchQuery || '')) {
                if (comp.selectedSearchQuery) {
                    if ((_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadOnQueryChange) {
                        comp.applySelectedFilter();
                    }
                    else {
                        // comp.selectedSearchQuery = comp.lastFetchedSearchQuery;
                    }
                }
                else {
                    if ((_b = this.configurationService.getConfiguration().filtering) === null || _b === void 0 ? void 0 : _b.autoReloadOnQueryClear) {
                        comp.applySelectedFilter();
                    }
                }
            }
        }, (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadTimeout);
    }
    switchToPage(pageIndex, context) {
        if (this.currentPageIndex === pageIndex) {
            return;
        }
        if (!context) {
            context = { reason: MaisTableReloadReason.USER };
        }
        this.setPage(pageIndex);
        this.statusChanged();
        this.reload(context);
    }
    clickOnColumn(column) {
        if (!MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) {
            return;
        }
        const sortColumn = this.currentSortColumn;
        const sortDirection = this.currentSortDirection;
        if (sortColumn && sortColumn.name === column.name) {
            this.selectedSortDirection = (sortDirection === MaisTableSortDirection.DESCENDING ?
                MaisTableSortDirection.ASCENDING : MaisTableSortDirection.DESCENDING);
        }
        else {
            this.selectedSortColumn = column;
            this.selectedSortDirection = MaisTableSortDirection.ASCENDING;
        }
        this.emitSortChanged();
        // forza ritorno alla prima pagina
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    // parsing functions
    getItemIdentifier(item) {
        if (this.itemTrackingEnabledAndPossible) {
            if (this.itemIdentifier.extract) {
                return this.itemIdentifier.extract(item);
            }
            else {
                return MaisTableFormatterHelper.getPropertyValue(item, this.itemIdentifier);
            }
        }
        else {
            return null;
        }
    }
    getCurrentLocale() {
        return this.translateService.getDefaultLang();
    }
    extractValue(row, column) {
        return MaisTableFormatterHelper.extractValue(row, column, this.getCurrentLocale());
    }
    resolveLabel(column) {
        return this.resolveLabelOrFixed(column.labelKey, column.label);
    }
    resolveLabelOrFixed(key, fixed) {
        if (key) {
            return this.translateService.instant(key);
        }
        else if (fixed) {
            return fixed;
        }
        else {
            return null;
        }
    }
    isCurrentSortingColumn(column, direction = null) {
        const sortColumn = this.currentSortColumn;
        if (sortColumn && column && column.name === sortColumn.name) {
            if (!direction) {
                return true;
            }
            return direction === this.currentSortDirection;
        }
        else {
            return false;
        }
    }
    isDefaultVisibleColumn(column) {
        return MaisTableFormatterHelper.isDefaultVisibleColumn(column, this.configurationService.getConfiguration());
    }
    isDefaultFilterColumn(column) {
        return MaisTableFormatterHelper.isDefaultFilterColumn(column, this.configurationService.getConfiguration());
    }
    isExpandable(row) {
        return this.currentEnableRowExpansion &&
            (!this.expandableStatusProvider || this.expandableStatusProvider(row, this.statusSnapshot));
    }
    isHideable(column) {
        return MaisTableFormatterHelper.isHideable(column, this.configurationService.getConfiguration());
    }
    isSortable(column) {
        return MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration());
    }
    isFilterable(column) {
        return MaisTableFormatterHelper.isFilterable(column, this.configurationService.getConfiguration());
    }
    hasLabel(column) {
        return MaisTableFormatterHelper.hasLabel(column, this.configurationService.getConfiguration());
    }
    isLabelVisibleInTable(column) {
        return MaisTableFormatterHelper.isLabelVisibleInTable(column, this.configurationService.getConfiguration());
    }
    isContextActionAllowed(action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultContextActionActivationCondition);
    }
    isHeaderActionAllowed(action) {
        var _a;
        return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultHeaderActionActivationCondition);
    }
    isActionAllowed(action, def) {
        const actCond = action.activationCondition || def;
        if (actCond === MaisTableActionActivationCondition.ALWAYS) {
            return true;
        }
        else if (actCond === MaisTableActionActivationCondition.NEVER) {
            return false;
        }
        else if (actCond === MaisTableActionActivationCondition.DYNAMIC) {
            // delega decisione a provider esterno
            if (!this.actionStatusProvider) {
                this.logger.error('Action with dynamic enabling but no actionStatusProvider provided');
                return false;
            }
            return this.actionStatusProvider(action, this.statusSnapshot || null);
        }
        const numSelected = this.checkedItems.length;
        if (actCond === MaisTableActionActivationCondition.SINGLE_SELECTION) {
            return numSelected === 1;
        }
        else if (actCond === MaisTableActionActivationCondition.MULTIPLE_SELECTION) {
            return numSelected > 0;
        }
        else if (actCond === MaisTableActionActivationCondition.NO_SELECTION) {
            return numSelected < 1;
        }
        else {
            throw new Error('Unknown action activation condition: ' + actCond);
        }
    }
    // gestione delle righe espanse
    isExpanded(item) {
        return this.expandedItems.indexOf(item) !== -1;
    }
    // checkbox select per items
    isChecked(item) {
        return this.checkedItems.indexOf(item) !== -1;
    }
    get allChecked() {
        const dataItems = this.currentData;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedItems.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyChecked() {
        const dataItems = this.currentData;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedItems.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noneChecked() {
        return !this.anyChecked;
    }
    toggleChecked(item) {
        if (this.isChecked(item)) {
            this.checkedItems.splice(this.checkedItems.indexOf(item), 1);
        }
        else {
            if (!this.currentEnableMultiSelect) {
                this.checkedItems = [];
            }
            this.checkedItems.push(item);
        }
        this.statusChanged();
        this.emitSelectionChanged();
    }
    toggleAllChecked() {
        if (!this.currentEnableSelectAll) {
            return;
        }
        if (!this.currentEnableMultiSelect) {
            return;
        }
        if (this.allChecked) {
            this.checkedItems = [];
        }
        else {
            this.checkedItems = [];
            for (const el of this.currentData) {
                this.checkedItems.push(el);
            }
        }
        this.statusChanged();
        this.emitSelectionChanged();
    }
    // checkbox select per filtering columns
    isColumnCheckedForFiltering(item) {
        return this.checkedColumnsForFiltering.indexOf(item) !== -1;
    }
    get allFilteringColumnsChecked() {
        const dataItems = this.filterableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForFiltering.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyFilteringColumnsChecked() {
        const dataItems = this.filterableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForFiltering.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noFilteringColumnshecked() {
        return !this.anyFilteringColumnsChecked;
    }
    toggleFilteringColumnChecked(item) {
        if (!this.currentEnableFiltering) {
            return;
        }
        if (!this.isFilterable(item)) {
            return;
        }
        if (this.isColumnCheckedForFiltering(item)) {
            this.checkedColumnsForFiltering.splice(this.checkedColumnsForFiltering.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForFiltering.push(item);
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    }
    toggleAllFilteringColumnsChecked() {
        if (!this.currentEnableFiltering) {
            return;
        }
        if (this.allFilteringColumnsChecked) {
            this.checkedColumnsForFiltering = [];
        }
        else {
            this.checkedColumnsForFiltering = [];
            for (const el of this.filterableColumns) {
                this.checkedColumnsForFiltering.push(el);
            }
        }
        if (this.selectedSearchQuery) {
            this.logger.trace('search query columns changed with active search query, reloading');
            this.applySelectedFilter();
        }
        this.statusChanged();
        this.emitFilteringColumnsSelectionChanged();
    }
    // checkbox select per visible columns
    isColumnCheckedForVisualization(item) {
        return this.checkedColumnsForVisualization.indexOf(item) !== -1;
    }
    get allVisibleColumnsChecked() {
        const dataItems = this.hideableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForVisualization.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyVisibleColumnsChecked() {
        const dataItems = this.hideableColumns;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedColumnsForVisualization.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noVisibleColumnshecked() {
        return !this.anyVisibleColumnsChecked;
    }
    toggleVisibleColumnChecked(item) {
        if (!MaisTableFormatterHelper.isHideable(item, this.configurationService.getConfiguration()) || !this.currentEnableColumnsSelection) {
            return;
        }
        if (this.isColumnCheckedForVisualization(item)) {
            this.checkedColumnsForVisualization.splice(this.checkedColumnsForVisualization.indexOf(item), 1);
        }
        else {
            this.checkedColumnsForVisualization.push(item);
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    }
    toggleAllVisibleColumnsChecked() {
        if (!this.currentEnableColumnsSelection) {
            return;
        }
        if (this.allVisibleColumnsChecked) {
            this.checkedColumnsForVisualization = this.columns.filter(c => !MaisTableFormatterHelper.isHideable(c, this.configurationService.getConfiguration()));
        }
        else {
            this.checkedColumnsForVisualization = [];
            for (const el of this.columns) {
                this.checkedColumnsForVisualization.push(el);
            }
        }
        this.statusChanged();
        this.emitVisibleColumnsSelectionChanged();
    }
    // checkbox select per filtri custom
    isContextFilterChecked(item) {
        return this.checkedContextFilters.indexOf(item) !== -1;
    }
    get allContextFilterChecked() {
        const dataItems = this.currentContextFilters;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedContextFilters.indexOf(item) === -1) {
                return false;
            }
        }
        return true;
    }
    get anyContextFilterChecked() {
        const dataItems = this.currentContextFilters;
        if (!dataItems.length) {
            return false;
        }
        for (const item of dataItems) {
            if (this.checkedContextFilters.indexOf(item) !== -1) {
                return true;
            }
        }
        return false;
    }
    get noContextFiltersChecked() {
        return !this.anyContextFilterChecked;
    }
    toggleContextFilterChecked(item) {
        if (!this.currentEnableContextFiltering) {
            return;
        }
        if (this.isContextFilterChecked(item)) {
            this.checkedContextFilters.splice(this.checkedContextFilters.indexOf(item), 1);
        }
        else {
            this.checkedContextFilters.push(item);
            if (item.group) {
                this.checkedContextFilters = this.checkedContextFilters.filter(o => {
                    return (o.name === item.name) || (!o.group) || (o.group !== item.group);
                });
            }
        }
        this.emitContextFiltersSelectionChanged();
        this.setPage(0);
        this.statusChanged();
        this.reload({ reason: MaisTableReloadReason.USER });
    }
    buildStatusSnapshot() {
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
            visibleColumns: this.visibleColumns.map(c => c.name),
            contextFilters: this.checkedContextFilters.map(c => c.name),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItems: !this.currentEnableSelection ? [] : this.checkedItems.map(v => v),
            expandedItems: !this.currentEnableRowExpansion ? [] : this.expandedItems.map(v => v)
        };
    }
    buildPersistableStatusSnapshot() {
        return {
            schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
            orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
            orderColumnDirection: this.currentSortDirection || null,
            query: this.currentSearchQuery,
            queryColumns: this.checkedColumnsForFiltering.map(c => c.name),
            visibleColumns: this.visibleColumns.map(c => c.name),
            contextFilters: this.checkedContextFilters.map(c => c.name),
            currentPage: this.currentPageIndex,
            pageSize: this.currentPageSize,
            checkedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.checkedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v),
            expandedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                this.expandedItems.map(item => this.getItemIdentifier(item)).filter(v => !!v)
        };
    }
    statusChanged() {
        this.statusSnapshot = this.buildStatusSnapshot();
        this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        this.logger.trace('table status changed', this.persistableStatusSnapshot);
        this.emitStatusChanged();
    }
    persistStatusToStore() {
        if (!this.statusSnapshot) {
            return throwError('No status to save');
        }
        return new Observable(subscriber => {
            if (this.storePersistenceEnabledAndPossible) {
                this.logger.trace('passing status to persistence store');
                if (this.storeAdapter.save) {
                    this.storeAdapter.save({
                        status: this.persistableStatusSnapshot
                    }).subscribe(result => {
                        this.logger.trace('saved status snapshot to persistence store');
                        subscriber.next();
                        subscriber.complete();
                    }, failure => {
                        this.logger.warn('failed to save status snapshot to persistence store', failure);
                        subscriber.error(failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No save function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next();
            subscriber.complete();
        });
    }
    loadStatusFromStore() {
        return new Observable(subscriber => {
            if (this.storePersistenceEnabledAndPossible) {
                this.logger.trace('fetching status from persistence store');
                if (this.storeAdapter.load) {
                    this.storeAdapter.load().subscribe(result => {
                        this.logger.trace('fetched status snapshot from persistence store');
                        subscriber.next(result);
                        subscriber.complete();
                    }, failure => {
                        subscriber.error(failure);
                        this.logger.warn('failed to fetch status snapshot from persistence store', failure);
                        subscriber.complete();
                    });
                }
                else {
                    subscriber.error('No load function in store adapter');
                    subscriber.complete();
                }
            }
            subscriber.next(null);
            subscriber.complete();
        });
    }
    get dragInProgress() {
        return ($('.cdk-drag-preview:visible').length +
            $('.cdk-drag-placeholder:visible').length +
            $('.cdk-drop-list-dragging:visible').length +
            $('.cdk-drop-list-receiving:visible').length) > 0;
    }
    // event emitters
    emitSelectionChanged() {
        this.selectionChange.emit(this.checkedItems);
    }
    emitSortChanged() {
        this.sortChange.emit({
            column: this.currentSortColumn,
            direction: this.currentSortDirection
        });
    }
    emitPageChanged() {
        this.pageChange.emit(this.selectedPageIndex);
    }
    emitFilteringColumnsSelectionChanged() {
        this.filteringColumnsChange.emit(this.checkedColumnsForFiltering);
    }
    emitVisibleColumnsSelectionChanged() {
        this.visibleColumnsChange.emit(this.checkedColumnsForVisualization);
    }
    emitContextFiltersSelectionChanged() {
        this.contextFiltersChange.emit(this.checkedContextFilters);
    }
    emitStatusChanged() {
        if (this.statusSnapshot) {
            this.statusChange.emit(this.statusSnapshot);
        }
    }
    // MatTable adapters
    get matTableData() {
        return this.dataSnapshot;
    }
    get matTableVisibleColumnDefs() {
        const o = [];
        if (this.rowExpansionEnabledAndPossible) {
            o.push('internal___col_row_expansion');
        }
        if (this.currentEnableSelection) {
            o.push('internal___col_row_selection');
        }
        if (this.contextFilteringEnabledAndPossible) {
            o.push('internal___col_context_filtering');
        }
        this.visibleColumns.map(c => c.name).forEach(c => o.push(c));
        return o;
    }
    get matTableVisibleColumns() {
        return this.visibleColumns;
    }
    handleMatTableDataSnapshotChanged() {
        const rows = [];
        this.dataSnapshot.forEach(element => {
            const detailRow = Object.assign({ ___detailRowContent: true, ___parentRow: element }, element);
            element.___detailRow = detailRow;
            rows.push(element, detailRow);
        });
        // TODO : BUILD STATIC ROW WITH DATA EXTRACTORS!
        this.logger.trace('emitting static rows for mat-table ' + this.currentTableId, rows);
        setTimeout(() => {
            var _a;
            (_a = this.matTableDataObservable) === null || _a === void 0 ? void 0 : _a.next(rows);
        });
    }
    get compatibilityModeForMainTable() {
        if (!this.isIE) {
            return false;
        }
        else if (this.currentEnableDrop) {
            return true;
        }
        else {
            return true;
        }
    }
    get compatibilityModeForDropDowns() {
        return true;
        if (!this.isIE) {
            return false;
        }
        else {
            return true;
        }
    }
}
MaisTableComponent.counter = 0;
/** @nocollapse */ MaisTableComponent.ɵfac = function MaisTableComponent_Factory(t) { return new (t || MaisTableComponent)(ɵɵdirectiveInject(TranslateService), ɵɵdirectiveInject(MaisTableService), ɵɵdirectiveInject(MaisTableRegistryService), ɵɵdirectiveInject(ChangeDetectorRef), ɵɵdirectiveInject(NgZone)); };
/** @nocollapse */ MaisTableComponent.ɵcmp = ɵɵdefineComponent({ type: MaisTableComponent, selectors: [["mais-table"]], contentQueries: function MaisTableComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
        ɵɵcontentQuery(dirIndex, _c0, true);
        ɵɵcontentQuery(dirIndex, _c1, true);
        ɵɵcontentQuery(dirIndex, _c2, true);
        ɵɵcontentQuery(dirIndex, _c3, true);
        ɵɵcontentQuery(dirIndex, _c4, true);
    } if (rf & 2) {
        var _t;
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.cellTemplate = _t.first);
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.actionsCaptionTemplate = _t.first);
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.rowDetailTemplate = _t.first);
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.dragTemplate = _t.first);
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.dropTemplate = _t.first);
    } }, viewQuery: function MaisTableComponent_Query(rf, ctx) { if (rf & 1) {
        ɵɵviewQuery(_c5, true);
    } if (rf & 2) {
        var _t;
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.table = _t.first);
    } }, inputs: { data: "data", dataProvider: "dataProvider", columns: "columns", defaultSortingColumn: "defaultSortingColumn", defaultSortingDirection: "defaultSortingDirection", defaultPageSize: "defaultPageSize", possiblePageSize: "possiblePageSize", tableId: "tableId", dropConnectedTo: "dropConnectedTo", paginationMode: "paginationMode", enablePagination: "enablePagination", enableSelection: "enableSelection", enableContextFiltering: "enableContextFiltering", enableSelectAll: "enableSelectAll", enableMultiSelect: "enableMultiSelect", enableFiltering: "enableFiltering", enableColumnsSelection: "enableColumnsSelection", enableContextActions: "enableContextActions", enableHeaderActions: "enableHeaderActions", enableRowExpansion: "enableRowExpansion", enablePageSizeSelect: "enablePageSizeSelect", enableMultipleRowExpansion: "enableMultipleRowExpansion", enableItemTracking: "enableItemTracking", enableStorePersistence: "enableStorePersistence", enableDrag: "enableDrag", enableDrop: "enableDrop", contextActions: "contextActions", headerActions: "headerActions", contextFilters: "contextFilters", contextFilteringPrompt: "contextFilteringPrompt", actionStatusProvider: "actionStatusProvider", expandableStatusProvider: "expandableStatusProvider", storeAdapter: "storeAdapter", itemIdentifier: "itemIdentifier", refreshStrategy: "refreshStrategy", refreshInterval: "refreshInterval", refreshEmitter: "refreshEmitter", refreshIntervalInBackground: "refreshIntervalInBackground", refreshOnPushInBackground: "refreshOnPushInBackground" }, outputs: { pageChange: "pageChange", sortChange: "sortChange", selectionChange: "selectionChange", filteringColumnsChange: "filteringColumnsChange", visibleColumnsChange: "visibleColumnsChange", contextFiltersChange: "contextFiltersChange", statusChange: "statusChange", itemDragged: "itemDragged", itemDropped: "itemDropped", action: "action" }, features: [ɵɵNgOnChangesFeature()], decls: 5, vars: 4, consts: [[1, "mais-table"], ["class", "container-full px-4 py-5 table-actions table-actions--header", 4, "ngIf"], [4, "ngIf"], ["class", "container-full px-4 py-4 table-actions table-actions--footer", 4, "ngIf"], [1, "container-full", "px-4", "py-5", "table-actions", "table-actions--header"], [1, "row"], ["class", "col-4", 4, "ngIf"], ["class", "col-3", 4, "ngIf"], [1, "col-5", "text-right"], [3, "ngTemplateOutlet", "ngTemplateOutletContext"], ["class", "btn-group", "ngbDropdown", "", 4, "ngIf"], [1, "col-4"], [3, "submit"], [1, "input-group"], ["type", "text", "name", "selectedSearchQuery", "aria-label", "Search", 1, "form-control", 3, "placeholder", "ngModel", "ngModelChange", "focusout"], ["ngbDropdown", "", 1, "input-group-append", 3, "autoClose"], ["type", "submit", 1, "btn", 3, "click"], [1, "fas", "fa-search"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 1, "dropdown-item"], [1, "dropdown-header"], [1, "form-group", "clickable"], [1, "form-check"], ["type", "checkbox", 3, "checked", "change"], [3, "click"], [1, "dropdown-divider"], [4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", "class", "dropdown-item", 3, "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "change", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled"], ["ngbDropdownItem", "", 3, "click"], [3, "icon", 4, "ngIf"], [3, "icon"], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngIf"], [1, "col-3"], ["ngbDropdown", "", 1, "btn-group", 3, "autoClose"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"], ["ngbDropdownItem", "", 1, "dropdown-header"], ["type", "button", 3, "class", "disabled", "click", 4, "ngFor", "ngForOf"], ["type", "button", 3, "disabled", "click"], ["ngbDropdown", "", 1, "btn-group"], ["ngbDropdownToggle", "", 1, "btn", "btn-light", "dropdown-toggle", 3, "disabled"], ["ngbDropdownMenu", ""], ["ngbDropdownItem", ""], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "table-responsive"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], [1, "d-none"], [1, ""], ["scope", "row", "class", "small-as-possible", 4, "ngIf"], ["scope", "row", "style", "width: 1%;", "ngbDropdown", "", 3, "border-primary", "autoClose", 4, "ngIf"], ["scope", "row", 1, "small-as-possible"], ["scope", "row", "ngbDropdown", "", 2, "width", "1%", 3, "autoClose"], ["ngbDropdownToggle", "", 1, "form-check", "clickable"], [1, "dropdown", "show", "d-inline-block", "float-right"], ["class", "dropdown-item", "ngbDropdownItem", "", 4, "ngFor", "ngForOf"], [1, "clickable", 3, "click"], ["scope", "col", 3, "click"], ["type", "button", 1, "order-btn", "btn", "btn-none", 3, "click"], [1, "fas", "fa-long-arrow-alt-up"], [1, "start-row-hook", "d-none"], ["cdkDrag", "", 1, "data-row", 3, "cdkDragData", "cdkDragDisabled"], ["scope", "row", 4, "ngIf"], ["class", "drop-container", 3, "hidden", 4, "cdkDragPlaceholder"], ["class", "drag-container", 4, "cdkDragPreview"], ["class", "detail-row", 4, "ngIf"], ["scope", "row"], ["class", "clickable", 3, "icon", "click", 4, "ngIf"], [1, "clickable", 3, "icon", "click"], [1, "drop-container", 3, "hidden"], [1, "drop-cell"], [1, "drop-container"], [1, "drag-container"], [1, "detail-row"], [1, "row-expansion-container"], [1, "message-row", "no-results-row"], [1, "text-center"], [1, "message-row", "fetching-row"], ["class", "table-responsive", 4, "ngIf"], ["class", "table table-dark table-hover", "cdkDropList", "", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "nodrop", "acceptdrop", "cdkDropListDropped", 4, "ngIf"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], ["renderedMatTable", ""], [3, "matColumnDef"], ["class", "maissize-min", 4, "matHeaderCellDef"], ["class", "maissize-min", 4, "matCellDef"], [4, "matCellDef"], [3, "matColumnDef", 4, "ngFor", "ngForOf"], [4, "matHeaderRowDef"], ["class", "element-row data-row", "cdkDrag", "", 3, "expanded", "row-selected", "cdkDragData", "cdkDragDisabled", 4, "matRowDef", "matRowDefColumns"], ["style", "overflow: hidden", 4, "matRowDef", "matRowDefColumns", "matRowDefWhen"], [1, "maissize-min"], ["ngbDropdown", "", 3, "autoClose", "container"], [3, "class", 4, "matHeaderCellDef"], [3, "class", 4, "matCellDef"], ["cdkDrag", "", 1, "element-row", "data-row", 3, "cdkDragData", "cdkDragDisabled"], [2, "overflow", "hidden"], [1, "alert", "alert-primary", "text-center"], [1, "container-full", "px-4", "py-4", "table-actions", "table-actions--footer"], [1, "col-7"], [1, "inline-block", "inline-block-bottom"], [1, "pagination-caption"], ["aria-label", "pagination", 1, "pagination-links"], [1, "pagination"], [1, "page-item", "clickable"], [1, "page-link", 3, "click"], [1, "inline-block", "inline-block-bottom", "pl-2"], ["ngbDropdown", "", 4, "ngIf"], ["class", "page-item clickable", 3, "active", 4, "ngIf"], ["class", "page-item", 4, "ngIf"], ["class", "sr-only", 4, "ngIf"], [1, "sr-only"], [1, "page-item"], [1, "page-link"], ["ngbDropdown", ""], ["ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"]], template: function MaisTableComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵtemplate(1, MaisTableComponent_div_1_Template, 12, 9, "div", 1);
        ɵɵtemplate(2, MaisTableComponent_ng_container_2_Template, 15, 19, "ng-container", 2);
        ɵɵtemplate(3, MaisTableComponent_ng_container_3_Template, 2, 1, "ng-container", 2);
        ɵɵtemplate(4, MaisTableComponent_div_4_Template, 10, 6, "div", 3);
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.columnSelectionPossibleAndAllowed || ctx.filteringPossibleAndAllowed || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.compatibilityModeForMainTable);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.compatibilityModeForMainTable);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.currentEnablePagination || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
    } }, directives: [NgIf, NgTemplateOutlet, NgbDropdown, NgbDropdownToggle, NgbDropdownMenu, NgbDropdownItem, NgForOf, FaIconComponent, CdkDropList, CdkDrag, CdkDragPlaceholder, CdkDragPreview, MatTable, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatHeaderCell, MatCell, MatHeaderRow, MatRow], pipes: [TranslatePipe], styles: [".message-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding:2em}.clickable[_ngcontent-%COMP%]{cursor:pointer}.inline-block[_ngcontent-%COMP%]{display:inline-block}.inline-block-bottom[_ngcontent-%COMP%]{vertical-align:bottom}.small-as-possible[_ngcontent-%COMP%]{width:1%}.detail-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:2em}.row-selected[_ngcontent-%COMP%]{background-color:#bc001622}.row-selected[_ngcontent-%COMP%]:hover{background-color:#77464755!important}.drag-container[_ngcontent-%COMP%], .drop-container[_ngcontent-%COMP%]{min-height:60px;padding:1em;background:#77464755;border:3px dotted #25161755}.nodrop[_ngcontent-%COMP%]   .drop-cell[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-row[_ngcontent-%COMP%]{display:none!important}.cdk-drop-list-dragging.acceptdrop[_ngcontent-%COMP%]   td[_ngcontent-%COMP%], th[_ngcontent-%COMP%]{border:none}.mat-header-cell[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{padding:0;margin:0}.mat-cell.maissize-min[_ngcontent-%COMP%], .mat-cell.maissize-xxs[_ngcontent-%COMP%], .mat-header-cell.maissize-min[_ngcontent-%COMP%], .mat-header-cell.maissize-xxs[_ngcontent-%COMP%]{flex:0 0 2%;min-width:3em}.mat-cell.maissize-xs[_ngcontent-%COMP%], .mat-header-cell.maissize-xs[_ngcontent-%COMP%]{flex:0 0 3%;min-width:5em}.mat-cell.maissize-s[_ngcontent-%COMP%], .mat-header-cell.maissize-s[_ngcontent-%COMP%]{flex:0 0 3%;min-width:8em}.mat-cell.maissize-m[_ngcontent-%COMP%], .mat-header-cell.maissize-m[_ngcontent-%COMP%]{flex:0 0 3%;min-width:12em}.mat-cell.maissize-l[_ngcontent-%COMP%], .mat-header-cell.maissize-l[_ngcontent-%COMP%]{flex:0 0 3%;min-width:18em}.mat-cell.maissize-xl[_ngcontent-%COMP%], .mat-header-cell.maissize-xl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:25em}.mat-cell.maissize-xxl[_ngcontent-%COMP%], .mat-header-cell.maissize-xxl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:30em}.mat-table[_ngcontent-%COMP%]{border-collapse:collapse}.mat-header-row[_ngcontent-%COMP%], .mat-row[_ngcontent-%COMP%], .mat-table[_ngcontent-%COMP%]{border-color:gray}", "mat-table[_ngcontent-%COMP%]{display:block}mat-header-row[_ngcontent-%COMP%]{min-height:56px}mat-footer-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{min-height:48px}mat-footer-row[_ngcontent-%COMP%], mat-header-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{display:flex;border-width:0 0 1px;border-style:solid;align-items:center;box-sizing:border-box}mat-footer-row[_ngcontent-%COMP%]::after, mat-header-row[_ngcontent-%COMP%]::after, mat-row[_ngcontent-%COMP%]::after{display:inline-block;min-height:inherit;content:\"\"}mat-cell[_ngcontent-%COMP%]:first-of-type, mat-footer-cell[_ngcontent-%COMP%]:first-of-type, mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}mat-cell[_ngcontent-%COMP%]:last-of-type, mat-footer-cell[_ngcontent-%COMP%]:last-of-type, mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}mat-cell[_ngcontent-%COMP%], mat-footer-cell[_ngcontent-%COMP%], mat-header-cell[_ngcontent-%COMP%]{flex:1;display:flex;align-items:center;overflow:hidden;word-wrap:break-word;min-height:inherit}table.mat-table[_ngcontent-%COMP%]{border-spacing:0}tr.mat-header-row[_ngcontent-%COMP%]{height:56px}tr.mat-footer-row[_ngcontent-%COMP%], tr.mat-row[_ngcontent-%COMP%]{height:48px}th.mat-header-cell[_ngcontent-%COMP%]{text-align:left}[dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]{text-align:right}td.mat-cell[_ngcontent-%COMP%], td.mat-footer-cell[_ngcontent-%COMP%], th.mat-header-cell[_ngcontent-%COMP%]{padding:0;border-bottom-width:1px;border-bottom-style:solid}td.mat-cell[_ngcontent-%COMP%]:first-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}td.mat-cell[_ngcontent-%COMP%]:last-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}"], data: { animation: [
            trigger('detailExpand', [
                state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                state('expanded', style({ height: '*', visibility: 'visible' })),
                transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
            ]),
        ] } });
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaisTableComponent, [{
        type: Component,
        args: [{
                // tslint:disable-next-line: component-selector
                selector: 'mais-table',
                templateUrl: './mais-table.component.html',
                styleUrls: ['./mais-table.component.scss', './mais-table-porting.css'],
                animations: [
                    trigger('detailExpand', [
                        state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                        state('expanded', style({ height: '*', visibility: 'visible' })),
                        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                    ]),
                ],
            }]
    }], function () { return [{ type: TranslateService }, { type: MaisTableService }, { type: MaisTableRegistryService }, { type: ChangeDetectorRef }, { type: NgZone }]; }, { data: [{
            type: Input
        }], dataProvider: [{
            type: Input
        }], columns: [{
            type: Input
        }], defaultSortingColumn: [{
            type: Input
        }], defaultSortingDirection: [{
            type: Input
        }], defaultPageSize: [{
            type: Input
        }], possiblePageSize: [{
            type: Input
        }], tableId: [{
            type: Input
        }], dropConnectedTo: [{
            type: Input
        }], paginationMode: [{
            type: Input
        }], enablePagination: [{
            type: Input
        }], enableSelection: [{
            type: Input
        }], enableContextFiltering: [{
            type: Input
        }], enableSelectAll: [{
            type: Input
        }], enableMultiSelect: [{
            type: Input
        }], enableFiltering: [{
            type: Input
        }], enableColumnsSelection: [{
            type: Input
        }], enableContextActions: [{
            type: Input
        }], enableHeaderActions: [{
            type: Input
        }], enableRowExpansion: [{
            type: Input
        }], enablePageSizeSelect: [{
            type: Input
        }], enableMultipleRowExpansion: [{
            type: Input
        }], enableItemTracking: [{
            type: Input
        }], enableStorePersistence: [{
            type: Input
        }], enableDrag: [{
            type: Input
        }], enableDrop: [{
            type: Input
        }], contextActions: [{
            type: Input
        }], headerActions: [{
            type: Input
        }], contextFilters: [{
            type: Input
        }], contextFilteringPrompt: [{
            type: Input
        }], actionStatusProvider: [{
            type: Input
        }], expandableStatusProvider: [{
            type: Input
        }], storeAdapter: [{
            type: Input
        }], itemIdentifier: [{
            type: Input
        }], refreshStrategy: [{
            type: Input
        }], refreshInterval: [{
            type: Input
        }], refreshEmitter: [{
            type: Input
        }], refreshIntervalInBackground: [{
            type: Input
        }], refreshOnPushInBackground: [{
            type: Input
        }], pageChange: [{
            type: Output
        }], sortChange: [{
            type: Output
        }], selectionChange: [{
            type: Output
        }], filteringColumnsChange: [{
            type: Output
        }], visibleColumnsChange: [{
            type: Output
        }], contextFiltersChange: [{
            type: Output
        }], statusChange: [{
            type: Output
        }], itemDragged: [{
            type: Output
        }], itemDropped: [{
            type: Output
        }], action: [{
            type: Output
        }], cellTemplate: [{
            type: ContentChild,
            args: ['cellTemplate']
        }], actionsCaptionTemplate: [{
            type: ContentChild,
            args: ['actionsCaptionTemplate']
        }], rowDetailTemplate: [{
            type: ContentChild,
            args: ['rowDetailTemplate']
        }], dragTemplate: [{
            type: ContentChild,
            args: ['dragTemplate']
        }], dropTemplate: [{
            type: ContentChild,
            args: ['dropTemplate']
        }], table: [{
            type: ViewChild,
            args: ['#renderedMatTable']
        }] }); })();

class MaisTableModule {
}
/** @nocollapse */ MaisTableModule.ɵmod = ɵɵdefineNgModule({ type: MaisTableModule });
/** @nocollapse */ MaisTableModule.ɵinj = ɵɵdefineInjector({ factory: function MaisTableModule_Factory(t) { return new (t || MaisTableModule)(); }, providers: [], imports: [[
            CommonModule,
            FontAwesomeModule,
            NgbModule,
            TranslateModule,
            CdkTableModule,
            MatTableModule,
            DragDropModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MaisTableModule, { declarations: [MaisTableComponent], imports: [CommonModule,
        FontAwesomeModule,
        NgbModule,
        TranslateModule,
        CdkTableModule,
        MatTableModule,
        DragDropModule], exports: [MaisTableComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaisTableModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    MaisTableComponent,
                ],
                imports: [
                    CommonModule,
                    FontAwesomeModule,
                    NgbModule,
                    TranslateModule,
                    CdkTableModule,
                    MatTableModule,
                    DragDropModule,
                ],
                exports: [
                    MaisTableComponent
                ],
                providers: []
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-mais-table
 */

/**
 * Generated bundle index. Do not edit.
 */

export { COLUMN_DEFAULTS, MaisTableActionActivationCondition, MaisTableColumnSize, MaisTableComponent, MaisTableFormatter, MaisTableModule, MaisTablePaginationMethod, MaisTableRefreshStrategy, MaisTableRegistryService, MaisTableReloadReason, MaisTableService, MaisTableSortDirection };
//# sourceMappingURL=ngx-mais-table.js.map
