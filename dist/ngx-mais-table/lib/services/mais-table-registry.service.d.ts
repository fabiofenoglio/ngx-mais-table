import { MaisTableComponent } from '../mais-table.component';
import * as i0 from "@angular/core";
export declare class MaisTableRegistryService {
    private static counter;
    private logger;
    private registry;
    constructor();
    register(key: string, component: MaisTableComponent): string;
    unregister(key: string, uuid: string): void;
    get(key: string): any;
    getSingle(key: string): MaisTableComponent | null;
    static ɵfac: i0.ɵɵFactoryDef<MaisTableRegistryService>;
    static ɵprov: i0.ɵɵInjectableDef<MaisTableRegistryService>;
}
