import { IMaisTableLogAdapter } from '../utils/mais-table-log-adapter';
import * as i0 from "@angular/core";
export declare class MaisTableLoggerService {
    private static embeddedLogger;
    private static logAdapter;
    constructor();
    static getConfiguredLogAdapter(): IMaisTableLogAdapter;
    withLogAdapter(logAdapter: IMaisTableLogAdapter | null): void;
    static ɵfac: i0.ɵɵFactoryDef<MaisTableLoggerService>;
    static ɵprov: i0.ɵɵInjectableDef<MaisTableLoggerService>;
}
