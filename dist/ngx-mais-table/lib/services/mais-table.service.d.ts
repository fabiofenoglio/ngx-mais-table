import { IMaisTableLogAdapter } from '../utils';
import { IMaisTableConfiguration } from '../model';
import { MaisTableLoggerService } from './mais-table-logger.service';
import * as i0 from "@angular/core";
export declare class MaisTableService {
    private maisTableLoggerService;
    private logger;
    private configuration;
    constructor(maisTableLoggerService: MaisTableLoggerService);
    configure(config: IMaisTableConfiguration): MaisTableService;
    withLogAdapter(logAdapter: IMaisTableLogAdapter): MaisTableService;
    getConfiguration(): IMaisTableConfiguration;
    private buildDefaultConfiguration;
    static ɵfac: i0.ɵɵFactoryDef<MaisTableService>;
    static ɵprov: i0.ɵɵInjectableDef<MaisTableService>;
}
