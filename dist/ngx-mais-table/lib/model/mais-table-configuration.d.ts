import { MaisTableActionActivationCondition } from './mais-table-action.model';
import { MaisTablePaginationMethod, MaisTableRefreshStrategy } from './mais-table-entities.model';
export interface IMaisTableConfiguration {
    rowSelection?: IMaisTableRowSelectionConfiguration;
    columnToggling?: IMaisTableColumnTogglingConfiguration;
    filtering?: IMaisTableFilteringConfiguration;
    contextFiltering?: IMaisTableContextFilteringConfiguration;
    pagination?: IMaisTablePaginationConfiguration;
    actions?: IMaisTableActionsConfiguration;
    columns?: IMaisTableColumnsConfiguration;
    rowExpansion?: IMaisTableRowExpansionConfiguration;
    itemTracking?: IMaisTableItemTrackingConfiguration;
    storePersistence?: IMaisTableStorePersistenceConfiguration;
    dragAndDrop?: IMaisTableDnDConfiguration;
    refresh?: IMaisTableRefreshConfiguration;
    currentSchemaVersion?: string;
}
export interface IMaisTableRefreshConfiguration {
    defaultStrategy?: MaisTableRefreshStrategy;
    defaultInterval?: number;
    defaultOnPushInBackground?: boolean;
    defaultOnTickInBackground?: boolean;
}
export interface IMaisTableRowSelectionConfiguration {
    enabledByDefault?: boolean;
    selectAllEnabledByDefault?: boolean;
    multipleSelectionEnabledByDefault?: boolean;
}
export interface IMaisTablePaginationConfiguration {
    enabledByDefault?: boolean;
    pageSizeSelectionEnabledByDefault?: boolean;
    defaultPaginationMode?: MaisTablePaginationMethod;
    defaultPageSize?: number;
    defaultPossiblePageSizes?: number[];
}
export interface IMaisTableFilteringConfiguration {
    enabledByDefault?: boolean;
    autoReloadOnQueryClear?: boolean;
    autoReloadOnQueryChange?: boolean;
    autoReloadTimeout?: number;
    showUnfilterableColumns?: boolean;
}
export interface IMaisTableDnDConfiguration {
    dragEnabledByDefault?: boolean;
    dropEnabledByDefault?: boolean;
}
export interface IMaisTableContextFilteringConfiguration {
    enabledByDefault?: boolean;
}
export interface IMaisTableItemTrackingConfiguration {
    enabledByDefault?: boolean;
}
export interface IMaisTableStorePersistenceConfiguration {
    enabledByDefault?: boolean;
}
export interface IMaisTableRowExpansionConfiguration {
    enabledByDefault?: boolean;
    multipleExpansionEnabledByDefault?: boolean;
}
export interface IMaisTableColumnTogglingConfiguration {
    enabledByDefault?: boolean;
    showFixedColumns?: boolean;
}
export interface IMaisTableActionsConfiguration {
    contextActionsEnabledByDefault?: boolean;
    headerActionsEnabledByDefault?: boolean;
    defaultHeaderActionActivationCondition?: MaisTableActionActivationCondition;
    defaultContextActionActivationCondition?: MaisTableActionActivationCondition;
}
export interface IMaisTableColumnsConfiguration {
    defaultShowLabelInTable?: boolean;
    defaultCanSort?: boolean;
    defaultCanHide?: boolean;
    defaultCanFilter?: boolean;
    defaultIsDefaultFilter?: boolean;
    defaultIsDefaulView?: boolean;
}
export declare const COLUMN_DEFAULTS: {
    SHOW_LABEL_IN_TABLE: boolean;
    CAN_SORT: boolean;
    CAN_HIDE: boolean;
    CAN_FILTER: boolean;
    DEFAULT_FILTER: boolean;
    DEFAULT_VIEW: boolean;
};
