import { MaisTableFormatter, IMaisTableFormatSpecification, IMaisTableFormatProvider } from './mais-table-entities.model';
export interface IMaisTableColumn {
    name: string;
    label?: string;
    labelKey?: string;
    showLabelInTable?: boolean;
    field?: string;
    serverField?: string;
    valueExtractor?: (input: any) => any;
    applyTemplate?: boolean;
    formatters?: IMaisTableFormatSpecification | MaisTableFormatter | IMaisTableFormatProvider | (IMaisTableFormatSpecification | MaisTableFormatter | IMaisTableFormatProvider)[];
    canSort?: boolean;
    canHide?: boolean;
    defaultVisible?: boolean;
    canFilter?: boolean;
    defaultFilter?: boolean;
    size?: string;
    headerDisplayClass?: string;
    cellDisplayClass?: string;
}
