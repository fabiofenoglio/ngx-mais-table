export interface IMaisTableAction {
    name: string;
    label?: string;
    labelKey?: string;
    displayClass?: string;
    additionalClasses?: string;
    activationCondition?: MaisTableActionActivationCondition;
}
export declare enum MaisTableActionActivationCondition {
    NEVER = "NEVER",
    ALWAYS = "ALWAYS",
    SINGLE_SELECTION = "SINGLE_SELECTION",
    MULTIPLE_SELECTION = "MULTIPLE_SELECTION",
    NO_SELECTION = "NO_SELECTION",
    DYNAMIC = "DYNAMIC"
}
