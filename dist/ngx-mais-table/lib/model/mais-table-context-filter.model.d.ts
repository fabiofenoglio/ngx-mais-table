export interface IMaisTableContextFilter {
    name: string;
    label?: string;
    labelKey?: string;
    group?: string;
}
