import * as i0 from "@angular/core";
import * as i1 from "./mais-table.component";
import * as i2 from "@angular/common";
import * as i3 from "@fortawesome/angular-fontawesome";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "@ngx-translate/core";
import * as i6 from "@angular/cdk/table";
import * as i7 from "@angular/material/table";
import * as i8 from "@angular/cdk/drag-drop";
export declare class MaisTableModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MaisTableModule, [typeof i1.MaisTableComponent], [typeof i2.CommonModule, typeof i3.FontAwesomeModule, typeof i4.NgbModule, typeof i5.TranslateModule, typeof i6.CdkTableModule, typeof i7.MatTableModule, typeof i8.DragDropModule], [typeof i1.MaisTableComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<MaisTableModule>;
}
