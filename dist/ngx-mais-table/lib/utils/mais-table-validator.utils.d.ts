import { IMaisTableColumn, IMaisTableConfiguration } from '../model';
export declare abstract class MaisTableValidatorHelper {
    static validateColumnsSpecification(columns: IMaisTableColumn[], config: IMaisTableConfiguration): any;
    private static _validateColumnsSpecification;
}
