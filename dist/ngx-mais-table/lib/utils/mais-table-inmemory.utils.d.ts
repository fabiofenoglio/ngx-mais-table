import { IMaisTableColumn, IMaisTablePageResponse, IMaisTablePageRequest } from '../model';
export declare abstract class MaisTableInMemoryHelper {
    static fetchInMemory(data: any[], request: IMaisTablePageRequest, sortColumn?: IMaisTableColumn | null, filteringColumns?: IMaisTableColumn[], locale?: string): IMaisTablePageResponse;
}
