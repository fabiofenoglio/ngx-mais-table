import { IMaisTableLogAdapter } from './mais-table-log-adapter';
export declare class MaisTableEmbeddedLogger implements IMaisTableLogAdapter {
    trace(message: any, ...additional: any[]): void;
    debug(message: any, ...additional: any[]): void;
    info(message: any, ...additional: any[]): void;
    log(message: any, ...additional: any[]): void;
    warn(message: any, ...additional: any[]): void;
    error(message: any, ...additional: any[]): void;
    fatal(message: any, ...additional: any[]): void;
}
