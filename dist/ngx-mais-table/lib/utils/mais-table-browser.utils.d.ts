export declare abstract class MaisTableBrowserHelper {
    private static browser;
    static getBrowser(): Browser;
    static isIE(): boolean;
}
export declare enum Browser {
    EDGE = "edge",
    OPERA = "opera",
    CHROME = "chrome",
    IE = "ie",
    FIREFOX = "firefox",
    SAFARI = "safari",
    OTHER = "other"
}
