import { IMaisTableColumn, IMaisTableFormatSpecification, IMaisTableConfiguration } from '../model';
export declare abstract class MaisTableFormatterHelper {
    private static lowercasePipe;
    private static uppercasePipe;
    private static transformMap;
    static getPropertyValue(object: any, field: string): string | null;
    static format(raw: any, formatterSpec: IMaisTableFormatSpecification, locale?: string): any;
    static extractValue(row: any, column: IMaisTableColumn, locale?: string): any;
    static isDefaultVisibleColumn(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean;
    static isDefaultFilterColumn(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean;
    static isHideable(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean | undefined;
    static isSortable(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean;
    static isFilterable(column: IMaisTableColumn, config: IMaisTableConfiguration): string;
    static hasLabel(column: IMaisTableColumn, config: IMaisTableConfiguration): string;
    static isLabelVisibleInTable(column: IMaisTableColumn, config: IMaisTableConfiguration): boolean;
}
