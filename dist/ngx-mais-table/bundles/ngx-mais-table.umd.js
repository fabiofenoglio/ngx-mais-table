(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('detect-browser'), require('@angular/common'), require('rxjs'), require('rxjs/operators'), require('@ngx-translate/core'), require('@angular/animations'), require('jquery'), require('@angular/material/table'), require('@ng-bootstrap/ng-bootstrap'), require('@fortawesome/angular-fontawesome'), require('@angular/cdk/drag-drop'), require('@angular/cdk/table')) :
    typeof define === 'function' && define.amd ? define('ngx-mais-table', ['exports', '@angular/core', 'detect-browser', '@angular/common', 'rxjs', 'rxjs/operators', '@ngx-translate/core', '@angular/animations', 'jquery', '@angular/material/table', '@ng-bootstrap/ng-bootstrap', '@fortawesome/angular-fontawesome', '@angular/cdk/drag-drop', '@angular/cdk/table'], factory) :
    (global = global || self, factory(global['ngx-mais-table'] = {}, global.ng.core, global.detectBrowser, global.ng.common, global.rxjs, global.rxjs.operators, global.core$1, global.ng.animations, global.$, global.ng.material.table, global.ngBootstrap, global.angularFontawesome, global.ng.cdk['drag-drop'], global.ng.cdk.table));
}(this, (function (exports, core, detectBrowser, common, rxjs, operators, core$1, animations, $, table, ngBootstrap, angularFontawesome, dragDrop, table$1) { 'use strict';

    $ = $ && $.hasOwnProperty('default') ? $['default'] : $;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var MaisTableBrowserHelper = /** @class */ (function () {
        function MaisTableBrowserHelper() {
        }
        MaisTableBrowserHelper.getBrowser = function () {
            var _a;
            var name = (_a = this.browser) === null || _a === void 0 ? void 0 : _a.name;
            if (name === Browser.CHROME ||
                name === Browser.FIREFOX ||
                name === Browser.EDGE ||
                name === Browser.IE ||
                name === Browser.OPERA ||
                name === Browser.SAFARI) {
                return name;
            }
            else {
                return Browser.OTHER;
            }
        };
        MaisTableBrowserHelper.isIE = function () {
            return MaisTableBrowserHelper.getBrowser() === Browser.IE;
        };
        MaisTableBrowserHelper.browser = detectBrowser.detect();
        return MaisTableBrowserHelper;
    }());
    var Browser;
    (function (Browser) {
        Browser["EDGE"] = "edge";
        Browser["OPERA"] = "opera";
        Browser["CHROME"] = "chrome";
        Browser["IE"] = "ie";
        Browser["FIREFOX"] = "firefox";
        Browser["SAFARI"] = "safari";
        Browser["OTHER"] = "other";
    })(Browser || (Browser = {}));

    // @dynamic
    var MaisTableFormatterHelper = /** @class */ (function () {
        function MaisTableFormatterHelper() {
        }
        MaisTableFormatterHelper.getPropertyValue = function (object, field) {
            if (object === null || object === undefined) {
                return null;
            }
            if (field.indexOf('.') === -1) {
                return object[field];
            }
            var splitted = field.split('.');
            var subObject = object[splitted[0]];
            return this.getPropertyValue(subObject, splitted.slice(1).join('.'));
        };
        MaisTableFormatterHelper.format = function (raw, formatterSpec, locale) {
            if (!raw) {
                return raw;
            }
            var transformFn = MaisTableFormatterHelper.transformMap[formatterSpec.formatter];
            if (!transformFn) {
                throw new Error('Unknown formatter: ' + formatterSpec.formatter);
            }
            return transformFn(raw, locale || 'en', formatterSpec.arguments);
        };
        MaisTableFormatterHelper.extractValue = function (row, column, locale) {
            var e_1, _a;
            if (!row) {
                return null;
            }
            var val;
            if (column.valueExtractor) {
                val = column.valueExtractor(row);
            }
            else if (column.field) {
                val = MaisTableFormatterHelper.getPropertyValue(row, column.field);
            }
            else {
                val = undefined;
            }
            if (column.formatters) {
                try {
                    for (var _b = __values((Array.isArray(column.formatters) ? column.formatters : [column.formatters])), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var formatter = _c.value;
                        if (formatter.formatter) {
                            val = MaisTableFormatterHelper.format(val, formatter, locale);
                        }
                        else if (formatter.format) {
                            val = formatter.format(val);
                        }
                        else {
                            val = MaisTableFormatterHelper.format(val, { formatter: formatter, arguments: null }, locale);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            return val;
        };
        MaisTableFormatterHelper.isDefaultVisibleColumn = function (column, config) {
            var _a, _b;
            return (column.defaultVisible === true || column.defaultVisible === false ?
                column.defaultVisible : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaulView) ||
                !MaisTableFormatterHelper.isHideable(column, config);
        };
        MaisTableFormatterHelper.isDefaultFilterColumn = function (column, config) {
            var _a, _b;
            return column.defaultFilter === true || column.defaultFilter === false ? column.defaultFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultIsDefaultFilter;
        };
        MaisTableFormatterHelper.isHideable = function (column, config) {
            var _a, _b;
            return column.canHide === true || column.canHide === false ? column.canHide : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanHide;
        };
        MaisTableFormatterHelper.isSortable = function (column, config) {
            var _a, _b;
            return column.canSort === true || column.canSort === false ? column.canSort : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanSort;
        };
        MaisTableFormatterHelper.isFilterable = function (column, config) {
            var _a, _b;
            return (column.canFilter === true || column.canFilter === false ? column.canFilter : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultCanFilter) &&
                (column.serverField || column.field);
        };
        MaisTableFormatterHelper.hasLabel = function (column, config) {
            return (column.labelKey || column.label);
        };
        MaisTableFormatterHelper.isLabelVisibleInTable = function (column, config) {
            var _a, _b;
            return column.showLabelInTable === true || column.showLabelInTable === false ?
                column.showLabelInTable : (_b = (_a = config) === null || _a === void 0 ? void 0 : _a.columns) === null || _b === void 0 ? void 0 : _b.defaultShowLabelInTable;
        };
        MaisTableFormatterHelper.lowercasePipe = new common.LowerCasePipe();
        MaisTableFormatterHelper.uppercasePipe = new common.UpperCasePipe();
        MaisTableFormatterHelper.transformMap = {
            UPPERCASE: function (raw, locale, args) { return MaisTableFormatterHelper.uppercasePipe.transform(raw); },
            LOWERCASE: function (raw, locale, args) { return MaisTableFormatterHelper.lowercasePipe.transform(raw); },
            DATE: function (raw, locale, args) {
                return new common.DatePipe(locale).transform(raw, args);
            },
            CURRENCY: function (raw, locale, args) {
                // signature: currencyCode?: string, display?: string | boolean, digitsInfo?: string, locale?: string
                if (!args) {
                    args = {};
                }
                return new common.CurrencyPipe(locale).transform(raw, args.currencyCode, args.display, args.digitsInfo, locale);
            }
        };
        return MaisTableFormatterHelper;
    }());


    (function (MaisTableSortDirection) {
        MaisTableSortDirection["ASCENDING"] = "ASC";
        MaisTableSortDirection["DESCENDING"] = "DESC";
    })(exports.MaisTableSortDirection || (exports.MaisTableSortDirection = {}));

    (function (MaisTablePaginationMethod) {
        MaisTablePaginationMethod["CLIENT"] = "CLIENT";
        MaisTablePaginationMethod["SERVER"] = "SERVER";
    })(exports.MaisTablePaginationMethod || (exports.MaisTablePaginationMethod = {}));

    (function (MaisTableFormatter) {
        MaisTableFormatter["UPPERCASE"] = "UPPERCASE";
        MaisTableFormatter["LOWERCASE"] = "LOWERCASE";
        MaisTableFormatter["DATE"] = "DATE";
        MaisTableFormatter["CURRENCY"] = "CURRENCY";
    })(exports.MaisTableFormatter || (exports.MaisTableFormatter = {}));

    (function (MaisTableRefreshStrategy) {
        MaisTableRefreshStrategy["ON_PUSH"] = "ON_PUSH";
        MaisTableRefreshStrategy["TIMED"] = "TIMED";
        MaisTableRefreshStrategy["NONE"] = "NONE";
    })(exports.MaisTableRefreshStrategy || (exports.MaisTableRefreshStrategy = {}));

    (function (MaisTableReloadReason) {
        MaisTableReloadReason["INTERNAL"] = "INTERNAL";
        MaisTableReloadReason["INTERVAL"] = "INTERVAL";
        MaisTableReloadReason["USER"] = "USER";
        MaisTableReloadReason["PUSH"] = "PUSH";
        MaisTableReloadReason["EXTERNAL"] = "EXTERNAL";
    })(exports.MaisTableReloadReason || (exports.MaisTableReloadReason = {}));

    (function (MaisTableColumnSize) {
        MaisTableColumnSize["XXS"] = "xxs";
        MaisTableColumnSize["XS"] = "xs";
        MaisTableColumnSize["SMALL"] = "s";
        MaisTableColumnSize["MEDIUM"] = "m";
        MaisTableColumnSize["LARGE"] = "l";
        MaisTableColumnSize["XL"] = "xl";
        MaisTableColumnSize["XXL"] = "xxl";
        MaisTableColumnSize["DEFAULT"] = "default";
    })(exports.MaisTableColumnSize || (exports.MaisTableColumnSize = {}));


    (function (MaisTableActionActivationCondition) {
        MaisTableActionActivationCondition["NEVER"] = "NEVER";
        MaisTableActionActivationCondition["ALWAYS"] = "ALWAYS";
        MaisTableActionActivationCondition["SINGLE_SELECTION"] = "SINGLE_SELECTION";
        MaisTableActionActivationCondition["MULTIPLE_SELECTION"] = "MULTIPLE_SELECTION";
        MaisTableActionActivationCondition["NO_SELECTION"] = "NO_SELECTION";
        MaisTableActionActivationCondition["DYNAMIC"] = "DYNAMIC";
    })(exports.MaisTableActionActivationCondition || (exports.MaisTableActionActivationCondition = {}));

    var COLUMN_DEFAULTS = {
        SHOW_LABEL_IN_TABLE: true,
        CAN_SORT: false,
        CAN_HIDE: true,
        CAN_FILTER: true,
        DEFAULT_FILTER: true,
        DEFAULT_VIEW: true
    };

    var MaisTableInMemoryHelper = /** @class */ (function () {
        function MaisTableInMemoryHelper() {
        }
        MaisTableInMemoryHelper.fetchInMemory = function (data, request, sortColumn, filteringColumns, locale) {
            var output = {
                content: [],
                size: 0,
                totalElements: 0,
                totalPages: 0
            };
            if (!data || !data.length) {
                return output;
            }
            // apply filtering
            var filtered = data;
            if (request.query && request.query.length && request.query.trim().length
                && request.queryFields && request.queryFields.length) {
                var cleanedSearchQuery_1 = request.query.trim().toLowerCase();
                filtered = filtered.filter(function (candidateRow) {
                    var e_1, _a;
                    var allowed = false;
                    if (filteringColumns) {
                        try {
                            for (var filteringColumns_1 = __values(filteringColumns), filteringColumns_1_1 = filteringColumns_1.next(); !filteringColumns_1_1.done; filteringColumns_1_1 = filteringColumns_1.next()) {
                                var filteringColumn = filteringColumns_1_1.value;
                                var extractedValue = MaisTableFormatterHelper.extractValue(candidateRow, filteringColumn, locale);
                                if (extractedValue) {
                                    var extractedValueStr = extractedValue + '';
                                    if (extractedValueStr.toLowerCase().indexOf(cleanedSearchQuery_1) !== -1) {
                                        allowed = true;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (filteringColumns_1_1 && !filteringColumns_1_1.done && (_a = filteringColumns_1.return)) _a.call(filteringColumns_1);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                    }
                    return allowed;
                });
            }
            // apply pagination
            var startingIndex;
            var endingIndex;
            if (request && request.page != null && request.size != null &&
                (typeof request.page !== 'undefined') && (typeof request.size !== 'undefined') && (request.page === 0 || request.page > 0)) {
                startingIndex = request.page * request.size;
                endingIndex = (request.page + 1) * request.size;
                if (startingIndex >= filtered.length) {
                    return output;
                }
                if (endingIndex > filtered.length) {
                    endingIndex = filtered.length;
                }
            }
            else {
                startingIndex = 0;
                endingIndex = filtered.length;
            }
            if (request.sort && request.sort.length && request.sort[0]) {
                var sortDirection_1 = request.sort[0].direction === exports.MaisTableSortDirection.DESCENDING ? -1 : +1;
                if (sortColumn) {
                    filtered.sort(function (c1, c2) {
                        var v1 = MaisTableFormatterHelper.extractValue(c1, sortColumn, locale) || '';
                        var v2 = MaisTableFormatterHelper.extractValue(c2, sortColumn, locale) || '';
                        var v1n = (v1 === null || v1 === undefined || Number.isNaN(v1));
                        var v2n = (v2 === null || v2 === undefined || Number.isNaN(v2));
                        if (v1n && v2n) {
                            return 0;
                        }
                        else if (v1n && !v2n) {
                            return -1 * sortDirection_1;
                        }
                        else if (!v1n && v2n) {
                            return 1 * sortDirection_1;
                        }
                        else {
                            return (v1 > v2 ? 1 : v1 < v2 ? -1 : 0) * sortDirection_1;
                        }
                    });
                }
            }
            var fetchedPage = filtered.slice(startingIndex, endingIndex);
            output.content = fetchedPage;
            output.size = fetchedPage.length;
            output.totalElements = filtered.length;
            if (request.size) {
                output.totalPages = Math.ceil(filtered.length);
            }
            else {
                output.totalPages = 1;
            }
            return output;
        };
        return MaisTableInMemoryHelper;
    }());

    var MaisTableValidatorHelper = /** @class */ (function () {
        function MaisTableValidatorHelper() {
        }
        MaisTableValidatorHelper.validateColumnsSpecification = function (columns, config) {
            var err = MaisTableValidatorHelper._validateColumnsSpecification(columns, config);
            if (err) {
                throw new Error(err);
            }
            return null;
        };
        MaisTableValidatorHelper._validateColumnsSpecification = function (columns, config) {
            var e_1, _a, e_2, _b;
            if (!columns || !columns.length) {
                return 'A non-empty list of columns must be provided';
            }
            try {
                for (var columns_1 = __values(columns), columns_1_1 = columns_1.next(); !columns_1_1.done; columns_1_1 = columns_1.next()) {
                    var col = columns_1_1.value;
                    if (!col.name) {
                        return 'Columns need to have a non-empty name.';
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (columns_1_1 && !columns_1_1.done && (_a = columns_1.return)) _a.call(columns_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            try {
                for (var columns_2 = __values(columns), columns_2_1 = columns_2.next(); !columns_2_1.done; columns_2_1 = columns_2.next()) {
                    var col = columns_2_1.value;
                    if ((MaisTableFormatterHelper.isFilterable(col, config) || MaisTableFormatterHelper.isHideable(col, config))
                        && !MaisTableFormatterHelper.hasLabel(col, config)) {
                        return 'Columns to be filtered or toggled must have either a labelKey or a label. ' +
                            'To hide a column label use the showLabelInTable property. Offending column is [' + col.name + ']';
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (columns_2_1 && !columns_2_1.done && (_b = columns_2.return)) _b.call(columns_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
            if (columns.filter(function (c) { return !MaisTableFormatterHelper.isHideable(c, config); }).length < 1) {
                return 'At least one of the columns must be not hideable: set canHide=false on primary or relevant column.';
            }
            return null;
        };
        return MaisTableValidatorHelper;
    }());

    var MaisTableEmbeddedLogger = /** @class */ (function () {
        function MaisTableEmbeddedLogger() {
        }
        MaisTableEmbeddedLogger.prototype.trace = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.log.apply(console, __spread(['[trace] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.debug = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.log.apply(console, __spread(['[trace] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.info = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.log.apply(console, __spread(['[INFO] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.log = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.log.apply(console, __spread(['[INFO] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.warn = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.warn.apply(console, __spread(['[WARN] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.error = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.error.apply(console, __spread(['[ERROR] ' + message], additional));
        };
        MaisTableEmbeddedLogger.prototype.fatal = function (message) {
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            console.error.apply(console, __spread(['[FATAL] ' + message], additional));
        };
        return MaisTableEmbeddedLogger;
    }());

    var MaisTableLoggerService = /** @class */ (function () {
        function MaisTableLoggerService() {
        }
        MaisTableLoggerService.getConfiguredLogAdapter = function () {
            return MaisTableLoggerService.logAdapter || MaisTableLoggerService.embeddedLogger;
        };
        MaisTableLoggerService.prototype.withLogAdapter = function (logAdapter) {
            MaisTableLoggerService.logAdapter = logAdapter;
        };
        MaisTableLoggerService.embeddedLogger = new MaisTableEmbeddedLogger();
        MaisTableLoggerService.logAdapter = null;
        /** @nocollapse */ MaisTableLoggerService.ɵfac = function MaisTableLoggerService_Factory(t) { return new (t || MaisTableLoggerService)(); };
        /** @nocollapse */ MaisTableLoggerService.ɵprov = core["ɵɵdefineInjectable"]({ token: MaisTableLoggerService, factory: MaisTableLoggerService.ɵfac, providedIn: 'root' });
        return MaisTableLoggerService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaisTableLoggerService, [{
            type: core.Injectable,
            args: [{ providedIn: 'root' }]
        }], function () { return []; }, null); })();

    var MaisTableLogger = /** @class */ (function () {
        function MaisTableLogger(componentName) {
            this.componentName = componentName;
            // NOP
            this.prefix = '[' + this.componentName + '] ';
        }
        MaisTableLogger.prototype.trace = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).trace.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.debug = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).debug.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.info = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).info.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.log = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).log.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.warn = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).warn.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.error = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).error.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.fatal = function (message) {
            var _a;
            var additional = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                additional[_i - 1] = arguments[_i];
            }
            (_a = this.getEffectiveLogger()).fatal.apply(_a, __spread([this.prefix + message], additional));
        };
        MaisTableLogger.prototype.getEffectiveLogger = function () {
            return MaisTableLoggerService.getConfiguredLogAdapter();
        };
        return MaisTableLogger;
    }());

    var MaisTableRegistryService = /** @class */ (function () {
        function MaisTableRegistryService() {
            this.registry = {};
            this.logger = new MaisTableLogger('MaisTableRegistryService');
            this.logger.trace('building service');
        }
        MaisTableRegistryService.prototype.register = function (key, component) {
            if (!key) {
                this.logger.warn('Invalid key provided to registry', key);
                throw Error('Invalid key provided to registry');
            }
            if (!component) {
                this.logger.warn('Invalid component provided to registry', component);
                throw Error('Invalid component provided to registry');
            }
            if (!this.registry[key]) {
                this.registry[key] = [];
            }
            var uuid = 'MTREG-' + (++MaisTableRegistryService.counter) + '-' + Math.round(Math.random() * 100000);
            var item = {
                key: key,
                component: component,
                registrationId: uuid
            };
            this.registry[key].push(item);
            this.logger.debug('registered table', item);
            return uuid;
        };
        MaisTableRegistryService.prototype.unregister = function (key, uuid) {
            if (!key) {
                this.logger.warn('Invalid key provided to registry', key);
                throw Error('Invalid key provided to registry');
            }
            if (!uuid) {
                this.logger.warn('Invalid uuid provided to registry', uuid);
                throw Error('Invalid uuid provided to registry');
            }
            this.logger.debug('unregistered table', uuid);
            this.registry[key] = this.registry[key].filter(function (o) { return o.registrationId !== uuid; });
        };
        MaisTableRegistryService.prototype.get = function (key) {
            return this.registry[key];
        };
        MaisTableRegistryService.prototype.getSingle = function (key) {
            var arr = this.registry[key];
            if (!arr || !arr.length) {
                return null;
            }
            else if (arr.length > 1) {
                this.logger.error('MULTIPLE REFERENCES FOR KEY', key);
                return null;
            }
            else {
                return arr[0].component;
            }
        };
        MaisTableRegistryService.counter = 0;
        /** @nocollapse */ MaisTableRegistryService.ɵfac = function MaisTableRegistryService_Factory(t) { return new (t || MaisTableRegistryService)(); };
        /** @nocollapse */ MaisTableRegistryService.ɵprov = core["ɵɵdefineInjectable"]({ token: MaisTableRegistryService, factory: MaisTableRegistryService.ɵfac, providedIn: 'root' });
        return MaisTableRegistryService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaisTableRegistryService, [{
            type: core.Injectable,
            args: [{ providedIn: 'root' }]
        }], function () { return []; }, null); })();

    var MaisTableService = /** @class */ (function () {
        function MaisTableService(maisTableLoggerService) {
            this.maisTableLoggerService = maisTableLoggerService;
            this.logger = new MaisTableLogger('MaisTableService');
            this.logger.trace('building service');
            this.configuration = this.buildDefaultConfiguration();
            this.logger.debug('initializing MAIS table component for browser', MaisTableBrowserHelper.getBrowser());
            this.maisTableLoggerService.withLogAdapter(null);
        }
        MaisTableService.prototype.configure = function (config) {
            Object.assign(this.configuration, config);
            return this;
        };
        MaisTableService.prototype.withLogAdapter = function (logAdapter) {
            this.maisTableLoggerService.withLogAdapter(logAdapter);
            return this;
        };
        MaisTableService.prototype.getConfiguration = function () {
            return this.configuration;
        };
        MaisTableService.prototype.buildDefaultConfiguration = function () {
            return {
                currentSchemaVersion: 'DEFAULT',
                refresh: {
                    defaultStrategy: exports.MaisTableRefreshStrategy.NONE,
                    defaultInterval: 60000,
                    defaultOnPushInBackground: true,
                    defaultOnTickInBackground: true
                },
                rowSelection: {
                    enabledByDefault: false,
                    selectAllEnabledByDefault: true,
                    multipleSelectionEnabledByDefault: true
                },
                columnToggling: {
                    enabledByDefault: true,
                    showFixedColumns: true
                },
                filtering: {
                    enabledByDefault: true,
                    autoReloadOnQueryClear: true,
                    autoReloadOnQueryChange: true,
                    autoReloadTimeout: 300,
                    showUnfilterableColumns: true
                },
                contextFiltering: {
                    enabledByDefault: true
                },
                pagination: {
                    enabledByDefault: true,
                    pageSizeSelectionEnabledByDefault: true,
                    defaultPaginationMode: exports.MaisTablePaginationMethod.CLIENT,
                    defaultPageSize: 5,
                    defaultPossiblePageSizes: [5, 10, 25, 50]
                },
                actions: {
                    contextActionsEnabledByDefault: true,
                    headerActionsEnabledByDefault: true,
                    defaultHeaderActionActivationCondition: exports.MaisTableActionActivationCondition.ALWAYS,
                    defaultContextActionActivationCondition: exports.MaisTableActionActivationCondition.MULTIPLE_SELECTION
                },
                columns: {
                    defaultShowLabelInTable: true,
                    defaultCanSort: false,
                    defaultCanHide: true,
                    defaultCanFilter: true,
                    defaultIsDefaultFilter: true,
                    defaultIsDefaulView: true
                },
                rowExpansion: {
                    enabledByDefault: false,
                    multipleExpansionEnabledByDefault: false
                },
                dragAndDrop: {
                    dragEnabledByDefault: false,
                    dropEnabledByDefault: false
                },
                itemTracking: {
                    enabledByDefault: false
                },
                storePersistence: {
                    enabledByDefault: true
                }
            };
        };
        /** @nocollapse */ MaisTableService.ɵfac = function MaisTableService_Factory(t) { return new (t || MaisTableService)(core["ɵɵinject"](MaisTableLoggerService)); };
        /** @nocollapse */ MaisTableService.ɵprov = core["ɵɵdefineInjectable"]({ token: MaisTableService, factory: MaisTableService.ɵfac, providedIn: 'root' });
        return MaisTableService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaisTableService, [{
            type: core.Injectable,
            args: [{ providedIn: 'root' }]
        }], function () { return [{ type: MaisTableLoggerService }]; }, null); })();

    var _c0 = ["cellTemplate"];
    var _c1 = ["actionsCaptionTemplate"];
    var _c2 = ["rowDetailTemplate"];
    var _c3 = ["dragTemplate"];
    var _c4 = ["dropTemplate"];
    var _c5 = ["#renderedMatTable"];
    function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
        var _r283 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template_input_change_0_listener($event) { core["ɵɵrestoreView"](_r283); var column_r277 = core["ɵɵnextContext"](2).$implicit; var ctx_r281 = core["ɵɵnextContext"](4); ctx_r281.toggleFilteringColumnChecked(column_r277); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r277 = core["ɵɵnextContext"](2).$implicit;
        var ctx_r279 = core["ɵɵnextContext"](4);
        core["ɵɵproperty"]("checked", ctx_r279.isColumnCheckedForFiltering(column_r277));
    } }
    function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "input", 31);
    } if (rf & 2) {
        core["ɵɵproperty"]("checked", false)("disabled", true);
    } }
    function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r287 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 20);
        core["ɵɵelementStart"](1, "div", 22);
        core["ɵɵelementStart"](2, "div", 23);
        core["ɵɵtemplate"](3, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_3_Template, 1, 1, "input", 29);
        core["ɵɵtemplate"](4, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_input_4_Template, 1, 2, "input", 30);
        core["ɵɵelementStart"](5, "span", 25);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template_span_click_5_listener($event) { core["ɵɵrestoreView"](_r287); var column_r277 = core["ɵɵnextContext"]().$implicit; var ctx_r285 = core["ɵɵnextContext"](4); ctx_r285.toggleFilteringColumnChecked(column_r277); return $event.stopPropagation(); });
        core["ɵɵtext"](6);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r277 = core["ɵɵnextContext"]().$implicit;
        var ctx_r278 = core["ɵɵnextContext"](4);
        core["ɵɵclassProp"]("disabled", !ctx_r278.isFilterable(column_r277));
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r278.isFilterable(column_r277));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r278.isFilterable(column_r277));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", ctx_r278.resolveLabel(column_r277), " ");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_button_1_Template, 7, 5, "button", 28);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r277 = ctx.$implicit;
        var ctx_r276 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r276.hasLabel(column_r277) && (ctx_r276.isFilterable(column_r277) || ctx_r276.configFilteringShowUnfilterableColumns));
    } }
    function MaisTableComponent_div_1_div_2_ng_container_8_Template(rf, ctx) { if (rf & 1) {
        var _r290 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 15);
        core["ɵɵelementStart"](2, "button", 16);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_button_click_2_listener() { core["ɵɵrestoreView"](_r290); var ctx_r289 = core["ɵɵnextContext"](3); return ctx_r289.applySelectedFilter(); });
        core["ɵɵelement"](3, "i", 17);
        core["ɵɵelementEnd"]();
        core["ɵɵelement"](4, "button", 18);
        core["ɵɵelementStart"](5, "div", 19);
        core["ɵɵelementStart"](6, "div", 20);
        core["ɵɵelementStart"](7, "h6", 21);
        core["ɵɵtext"](8);
        core["ɵɵpipe"](9, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](10, "button", 20);
        core["ɵɵelementStart"](11, "div", 22);
        core["ɵɵelementStart"](12, "div", 23);
        core["ɵɵelementStart"](13, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_div_1_div_2_ng_container_8_Template_input_change_13_listener($event) { core["ɵɵrestoreView"](_r290); var ctx_r291 = core["ɵɵnextContext"](3); ctx_r291.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](14, "span", 25);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_8_Template_span_click_14_listener($event) { core["ɵɵrestoreView"](_r290); var ctx_r292 = core["ɵɵnextContext"](3); ctx_r292.toggleAllFilteringColumnsChecked(); return $event.stopPropagation(); });
        core["ɵɵtext"](15);
        core["ɵɵpipe"](16, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelement"](17, "div", 26);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](18, MaisTableComponent_div_1_div_2_ng_container_8_ng_container_18_Template, 2, 1, "ng-container", 27);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r274 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("autoClose", "outside");
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("btn-black", !ctx_r274.searchQueryNeedApply || !ctx_r274.searchQueryActive)("btn-primary", ctx_r274.searchQueryNeedApply && ctx_r274.searchQueryActive)("border-primary", ctx_r274.searchQueryActive && !ctx_r274.searchQueryMalformed)("border-warning", ctx_r274.searchQueryMalformed);
        core["ɵɵadvance"](2);
        core["ɵɵclassProp"]("border-primary", ctx_r274.searchQueryActive && !ctx_r274.searchQueryMalformed)("border-warning", ctx_r274.searchQueryMalformed)("btn-black", !(ctx_r274.selectedSearchQuery && ctx_r274.noFilteringColumnshecked))("btn-warning", ctx_r274.selectedSearchQuery && ctx_r274.noFilteringColumnshecked);
        core["ɵɵadvance"](4);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](9, 21, "table.common.messages.pick_filtering_columns"));
        core["ɵɵadvance"](5);
        core["ɵɵproperty"]("checked", ctx_r274.allFilteringColumnsChecked);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](16, 23, "table.common.messages.pick_all_filtering_columns"), " ");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngForOf", ctx_r274.columns);
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "check-square");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "square");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "check-square");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "square");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "square");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r303 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 32);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r303); var column_r296 = core["ɵɵnextContext"]().$implicit; var ctx_r301 = core["ɵɵnextContext"](4); return ctx_r301.toggleFilteringColumnChecked(column_r296); });
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](2, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](3, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtext"](4);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r296 = core["ɵɵnextContext"]().$implicit;
        var ctx_r297 = core["ɵɵnextContext"](4);
        core["ɵɵclassProp"]("disabled", !ctx_r297.isFilterable(column_r296));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r297.isFilterable(column_r296) && ctx_r297.isColumnCheckedForFiltering(column_r296));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r297.isFilterable(column_r296) && !ctx_r297.isColumnCheckedForFiltering(column_r296));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r297.isFilterable(column_r296));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r297.resolveLabel(column_r296), " ");
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_button_1_Template, 5, 6, "button", 35);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r296 = ctx.$implicit;
        var ctx_r295 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r295.hasLabel(column_r296) && (ctx_r295.isFilterable(column_r296) || ctx_r295.configFilteringShowUnfilterableColumns));
    } }
    function MaisTableComponent_div_1_div_2_ng_container_9_Template(rf, ctx) { if (rf & 1) {
        var _r306 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 15);
        core["ɵɵelementStart"](2, "button", 16);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_2_listener() { core["ɵɵrestoreView"](_r306); var ctx_r305 = core["ɵɵnextContext"](3); return ctx_r305.applySelectedFilter(); });
        core["ɵɵelement"](3, "i", 17);
        core["ɵɵelementEnd"]();
        core["ɵɵelement"](4, "button", 18);
        core["ɵɵelementStart"](5, "div", 19);
        core["ɵɵelementStart"](6, "div", 20);
        core["ɵɵelementStart"](7, "h6", 21);
        core["ɵɵtext"](8);
        core["ɵɵpipe"](9, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](10, "button", 32);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_2_ng_container_9_Template_button_click_10_listener() { core["ɵɵrestoreView"](_r306); var ctx_r307 = core["ɵɵnextContext"](3); return ctx_r307.toggleAllFilteringColumnsChecked(); });
        core["ɵɵtemplate"](11, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_11_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](12, MaisTableComponent_div_1_div_2_ng_container_9_fa_icon_12_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtext"](13);
        core["ɵɵpipe"](14, "translate");
        core["ɵɵelement"](15, "div", 26);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](16, MaisTableComponent_div_1_div_2_ng_container_9_ng_container_16_Template, 2, 1, "ng-container", 27);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r275 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("autoClose", "outside");
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("btn-black", !ctx_r275.searchQueryNeedApply || !ctx_r275.searchQueryActive)("btn-primary", ctx_r275.searchQueryNeedApply && ctx_r275.searchQueryActive)("border-primary", ctx_r275.searchQueryActive && !ctx_r275.searchQueryMalformed)("border-warning", ctx_r275.searchQueryMalformed);
        core["ɵɵadvance"](2);
        core["ɵɵclassProp"]("border-primary", ctx_r275.searchQueryActive && !ctx_r275.searchQueryMalformed)("border-warning", ctx_r275.searchQueryMalformed)("btn-black", !(ctx_r275.selectedSearchQuery && ctx_r275.noFilteringColumnshecked))("btn-warning", ctx_r275.selectedSearchQuery && ctx_r275.noFilteringColumnshecked);
        core["ɵɵadvance"](4);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](9, 22, "table.common.messages.pick_filtering_columns"));
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r275.allFilteringColumnsChecked);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r275.allFilteringColumnsChecked);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](14, 24, "table.common.messages.pick_all_filtering_columns"), " ");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngForOf", ctx_r275.columns);
    } }
    function MaisTableComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
        var _r309 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 11);
        core["ɵɵelementStart"](1, "form", 12);
        core["ɵɵlistener"]("submit", function MaisTableComponent_div_1_div_2_Template_form_submit_1_listener() { core["ɵɵrestoreView"](_r309); var ctx_r308 = core["ɵɵnextContext"](2); return ctx_r308.applySelectedFilter(); });
        core["ɵɵelementStart"](2, "div");
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "div", 13);
        core["ɵɵelementStart"](6, "input", 14);
        core["ɵɵlistener"]("ngModelChange", function MaisTableComponent_div_1_div_2_Template_input_ngModelChange_6_listener($event) { core["ɵɵrestoreView"](_r309); var ctx_r310 = core["ɵɵnextContext"](2); return ctx_r310.selectedSearchQuery = $event; })("focusout", function MaisTableComponent_div_1_div_2_Template_input_focusout_6_listener() { core["ɵɵrestoreView"](_r309); var ctx_r311 = core["ɵɵnextContext"](2); return ctx_r311.searchQueryFocusOut(); });
        core["ɵɵpipe"](7, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](8, MaisTableComponent_div_1_div_2_ng_container_8_Template, 19, 25, "ng-container", 2);
        core["ɵɵtemplate"](9, MaisTableComponent_div_1_div_2_ng_container_9_Template, 17, 26, "ng-container", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r267 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](4, 9, "table.common.messages.filter_prompt"));
        core["ɵɵadvance"](3);
        core["ɵɵclassProp"]("border-primary", ctx_r267.searchQueryActive && !ctx_r267.searchQueryMalformed)("border-warning", ctx_r267.searchQueryMalformed);
        core["ɵɵpropertyInterpolate"]("placeholder", core["ɵɵpipeBind1"](7, 11, "table.common.messages.filter_placeholder"));
        core["ɵɵproperty"]("ngModel", ctx_r267.selectedSearchQuery);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", !ctx_r267.compatibilityModeForDropDowns);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r267.compatibilityModeForDropDowns);
    } }
    function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template(rf, ctx) { if (rf & 1) {
        var _r321 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template_input_change_0_listener($event) { core["ɵɵrestoreView"](_r321); var column_r315 = core["ɵɵnextContext"](2).$implicit; var ctx_r319 = core["ɵɵnextContext"](4); ctx_r319.toggleVisibleColumnChecked(column_r315); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r315 = core["ɵɵnextContext"](2).$implicit;
        var ctx_r317 = core["ɵɵnextContext"](4);
        core["ɵɵproperty"]("checked", ctx_r317.isColumnCheckedForVisualization(column_r315));
    } }
    function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "input", 31);
    } if (rf & 2) {
        core["ɵɵproperty"]("checked", true)("disabled", true);
    } }
    function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r325 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 20);
        core["ɵɵelementStart"](1, "div", 22);
        core["ɵɵelementStart"](2, "div", 23);
        core["ɵɵtemplate"](3, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_3_Template, 1, 1, "input", 29);
        core["ɵɵtemplate"](4, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_input_4_Template, 1, 2, "input", 30);
        core["ɵɵelementStart"](5, "span", 25);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template_span_click_5_listener($event) { core["ɵɵrestoreView"](_r325); var column_r315 = core["ɵɵnextContext"]().$implicit; var ctx_r323 = core["ɵɵnextContext"](4); ctx_r323.toggleVisibleColumnChecked(column_r315); return $event.stopPropagation(); });
        core["ɵɵtext"](6);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r315 = core["ɵɵnextContext"]().$implicit;
        var ctx_r316 = core["ɵɵnextContext"](4);
        core["ɵɵclassProp"]("disabled", !ctx_r316.isHideable(column_r315));
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r316.isHideable(column_r315));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r316.isHideable(column_r315));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", ctx_r316.resolveLabel(column_r315), " ");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_button_1_Template, 7, 5, "button", 28);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r315 = ctx.$implicit;
        var ctx_r314 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r314.hasLabel(column_r315) && (ctx_r314.isHideable(column_r315) || ctx_r314.configColumnVisibilityShowFixedColumns));
    } }
    function MaisTableComponent_div_1_div_3_ng_container_4_Template(rf, ctx) { if (rf & 1) {
        var _r328 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 37);
        core["ɵɵelementStart"](2, "button", 38);
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "div", 19);
        core["ɵɵelementStart"](6, "h6", 39);
        core["ɵɵtext"](7);
        core["ɵɵpipe"](8, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](9, "button", 20);
        core["ɵɵelementStart"](10, "div", 22);
        core["ɵɵelementStart"](11, "div", 23);
        core["ɵɵelementStart"](12, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_div_1_div_3_ng_container_4_Template_input_change_12_listener($event) { core["ɵɵrestoreView"](_r328); var ctx_r327 = core["ɵɵnextContext"](3); ctx_r327.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](13, "span", 25);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_3_ng_container_4_Template_span_click_13_listener($event) { core["ɵɵrestoreView"](_r328); var ctx_r329 = core["ɵɵnextContext"](3); ctx_r329.toggleAllVisibleColumnsChecked(); return $event.stopPropagation(); });
        core["ɵɵtext"](14);
        core["ɵɵpipe"](15, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelement"](16, "div", 26);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](17, MaisTableComponent_div_1_div_3_ng_container_4_ng_container_17_Template, 2, 1, "ng-container", 27);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r312 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("autoClose", "outside");
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](4, 6, "table.common.messages.add_remove_columns"), " ");
        core["ɵɵadvance"](4);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](8, 8, "table.common.messages.select_columns_prompt"), " ");
        core["ɵɵadvance"](5);
        core["ɵɵproperty"]("checked", ctx_r312.allVisibleColumnsChecked);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](15, 10, "table.common.messages.pick_all_visible_columns"), " ");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngForOf", ctx_r312.columns);
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "check-square");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "square");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "check-square");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "square");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "fa-icon", 34);
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "check-square");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r340 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 32);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r340); var column_r333 = core["ɵɵnextContext"]().$implicit; var ctx_r338 = core["ɵɵnextContext"](4); return ctx_r338.toggleVisibleColumnChecked(column_r333); });
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_1_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](2, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_2_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](3, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_fa_icon_3_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtext"](4);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r333 = core["ɵɵnextContext"]().$implicit;
        var ctx_r334 = core["ɵɵnextContext"](4);
        core["ɵɵclassProp"]("disabled", !ctx_r334.isHideable(column_r333));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r334.isHideable(column_r333) && ctx_r334.isColumnCheckedForVisualization(column_r333));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r334.isHideable(column_r333) && !ctx_r334.isColumnCheckedForVisualization(column_r333));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r334.isHideable(column_r333));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r334.resolveLabel(column_r333), " ");
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_button_1_Template, 5, 6, "button", 35);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r333 = ctx.$implicit;
        var ctx_r332 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r332.hasLabel(column_r333) && (ctx_r332.isHideable(column_r333) || ctx_r332.configColumnVisibilityShowFixedColumns));
    } }
    function MaisTableComponent_div_1_div_3_ng_container_5_Template(rf, ctx) { if (rf & 1) {
        var _r343 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 37);
        core["ɵɵelementStart"](2, "button", 38);
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "div", 19);
        core["ɵɵelementStart"](6, "h6", 39);
        core["ɵɵtext"](7);
        core["ɵɵpipe"](8, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](9, "button", 32);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_3_ng_container_5_Template_button_click_9_listener() { core["ɵɵrestoreView"](_r343); var ctx_r342 = core["ɵɵnextContext"](3); return ctx_r342.toggleAllVisibleColumnsChecked(); });
        core["ɵɵtemplate"](10, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_10_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtemplate"](11, MaisTableComponent_div_1_div_3_ng_container_5_fa_icon_11_Template, 1, 1, "fa-icon", 33);
        core["ɵɵtext"](12);
        core["ɵɵpipe"](13, "translate");
        core["ɵɵelement"](14, "div", 26);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](15, MaisTableComponent_div_1_div_3_ng_container_5_ng_container_15_Template, 2, 1, "ng-container", 27);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r313 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("autoClose", "outside");
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](4, 7, "table.common.messages.add_remove_columns"), " ");
        core["ɵɵadvance"](4);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](8, 9, "table.common.messages.select_columns_prompt"), " ");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r313.allVisibleColumnsChecked);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r313.allVisibleColumnsChecked);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](13, 11, "table.common.messages.pick_all_visible_columns"), " ");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngForOf", ctx_r313.columns);
    } }
    function MaisTableComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 36);
        core["ɵɵelementStart"](1, "div");
        core["ɵɵtext"](2);
        core["ɵɵpipe"](3, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](4, MaisTableComponent_div_1_div_3_ng_container_4_Template, 18, 12, "ng-container", 2);
        core["ɵɵtemplate"](5, MaisTableComponent_div_1_div_3_ng_container_5_Template, 16, 13, "ng-container", 2);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r268 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](3, 3, "table.common.messages.customize_view_prompt"));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", !ctx_r268.compatibilityModeForDropDowns);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r268.compatibilityModeForDropDowns);
    } }
    function MaisTableComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "div", 36);
    } }
    function MaisTableComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "div", 11);
    } }
    function MaisTableComponent_div_1_ng_template_8_Template(rf, ctx) { }
    function MaisTableComponent_div_1_ng_container_10_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r347 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 41);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_ng_container_10_button_1_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r347); var action_r345 = ctx.$implicit; var ctx_r346 = core["ɵɵnextContext"](3); return ctx_r346.clickOnContextAction(action_r345); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var action_r345 = ctx.$implicit;
        var ctx_r344 = core["ɵɵnextContext"](3);
        core["ɵɵclassMapInterpolate2"]("btn btn-", action_r345.displayClass || "light", " ", action_r345.additionalClasses || "", " mr-1");
        core["ɵɵproperty"]("disabled", !ctx_r344.isContextActionAllowed(action_r345));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r344.resolveLabel(action_r345), " ");
    } }
    function MaisTableComponent_div_1_ng_container_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_1_ng_container_10_button_1_Template, 2, 6, "button", 40);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r272 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r272.headerActions);
    } }
    function MaisTableComponent_div_1_div_11_button_9_Template(rf, ctx) { if (rf & 1) {
        var _r351 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 47);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_1_div_11_button_9_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r351); var action_r349 = ctx.$implicit; var ctx_r350 = core["ɵɵnextContext"](3); return ctx_r350.clickOnContextAction(action_r349); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var action_r349 = ctx.$implicit;
        var ctx_r348 = core["ɵɵnextContext"](3);
        core["ɵɵproperty"]("disabled", !ctx_r348.isContextActionAllowed(action_r349));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r348.resolveLabel(action_r349), " ");
    } }
    function MaisTableComponent_div_1_div_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 42);
        core["ɵɵelementStart"](1, "button", 43);
        core["ɵɵtext"](2);
        core["ɵɵpipe"](3, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](4, "div", 44);
        core["ɵɵelementStart"](5, "div", 45);
        core["ɵɵelementStart"](6, "h6", 21);
        core["ɵɵtext"](7);
        core["ɵɵpipe"](8, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](9, MaisTableComponent_div_1_div_11_button_9_Template, 2, 2, "button", 46);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r273 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("disabled", !ctx_r273.anyButtonActionsAllowed);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](3, 4, "table.common.messages.actions_button"));
        core["ɵɵadvance"](5);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](8, 6, "table.common.messages.pick_action"));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngForOf", ctx_r273.currentActions);
    } }
    var _c6 = function () { return {}; };
    function MaisTableComponent_div_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 4);
        core["ɵɵelementStart"](1, "div", 5);
        core["ɵɵtemplate"](2, MaisTableComponent_div_1_div_2_Template, 10, 13, "div", 6);
        core["ɵɵtemplate"](3, MaisTableComponent_div_1_div_3_Template, 6, 5, "div", 7);
        core["ɵɵtemplate"](4, MaisTableComponent_div_1_div_4_Template, 1, 0, "div", 7);
        core["ɵɵtemplate"](5, MaisTableComponent_div_1_div_5_Template, 1, 0, "div", 6);
        core["ɵɵelementStart"](6, "div", 8);
        core["ɵɵelementStart"](7, "div");
        core["ɵɵtemplate"](8, MaisTableComponent_div_1_ng_template_8_Template, 0, 0, "ng-template", 9);
        core["ɵɵtext"](9, " \u00A0 ");
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](10, MaisTableComponent_div_1_ng_container_10_Template, 2, 1, "ng-container", 2);
        core["ɵɵtemplate"](11, MaisTableComponent_div_1_div_11_Template, 10, 8, "div", 10);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r263 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r263.filteringPossibleAndAllowed);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r263.columnSelectionPossibleAndAllowed);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r263.columnSelectionPossibleAndAllowed);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r263.filteringPossibleAndAllowed);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r263.actionsCaptionTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction0"](8, _c6));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r263.headerActionsEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r263.contextActionsEnabledAndPossible);
    } }
    function MaisTableComponent_ng_container_2_th_8_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "th", 54);
    } }
    function MaisTableComponent_ng_container_2_th_9_span_2_Template(rf, ctx) { if (rf & 1) {
        var _r361 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_2_th_9_span_2_Template_input_change_1_listener() { core["ɵɵrestoreView"](_r361); var ctx_r360 = core["ɵɵnextContext"](3); return ctx_r360.toggleAllChecked(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r359 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("checked", ctx_r359.allChecked);
    } }
    function MaisTableComponent_ng_container_2_th_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 54);
        core["ɵɵelementStart"](1, "div", 23);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_th_9_span_2_Template, 2, 1, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r353 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r353.currentEnableSelectAll && ctx_r353.currentEnableMultiSelect && !ctx_r353.noResults);
    } }
    function MaisTableComponent_ng_container_2_th_10_span_5_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵpipe"](2, "translate");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r362 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](2, 1, ctx_r362.contextFilteringPrompt), " ");
    } }
    function MaisTableComponent_ng_container_2_th_10_span_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵpipe"](2, "translate");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](2, 1, "table.common.messages.context_filter_prompt"), " ");
    } }
    function MaisTableComponent_ng_container_2_th_10_div_7_Template(rf, ctx) { if (rf & 1) {
        var _r367 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 20);
        core["ɵɵelementStart"](1, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_2_th_10_div_7_Template_input_change_1_listener($event) { core["ɵɵrestoreView"](_r367); var filter_r365 = ctx.$implicit; var ctx_r366 = core["ɵɵnextContext"](3); ctx_r366.toggleContextFilterChecked(filter_r365); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](2, "span", 59);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_th_10_div_7_Template_span_click_2_listener($event) { core["ɵɵrestoreView"](_r367); var filter_r365 = ctx.$implicit; var ctx_r368 = core["ɵɵnextContext"](3); ctx_r368.toggleContextFilterChecked(filter_r365); return $event.stopPropagation(); });
        core["ɵɵtext"](3);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var filter_r365 = ctx.$implicit;
        var ctx_r364 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("checked", ctx_r364.isContextFilterChecked(filter_r365));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", ctx_r364.resolveLabel(filter_r365), " ");
    } }
    function MaisTableComponent_ng_container_2_th_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 55);
        core["ɵɵelementStart"](1, "div", 56);
        core["ɵɵelementStart"](2, "div", 57);
        core["ɵɵelementStart"](3, "div", 19);
        core["ɵɵelementStart"](4, "h6", 39);
        core["ɵɵtemplate"](5, MaisTableComponent_ng_container_2_th_10_span_5_Template, 3, 3, "span", 2);
        core["ɵɵtemplate"](6, MaisTableComponent_ng_container_2_th_10_span_6_Template, 3, 3, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](7, MaisTableComponent_ng_container_2_th_10_div_7_Template, 4, 2, "div", 58);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r354 = core["ɵɵnextContext"](2);
        core["ɵɵclassProp"]("border-primary", ctx_r354.anyContextFilterChecked);
        core["ɵɵproperty"]("autoClose", "outside");
        core["ɵɵadvance"](5);
        core["ɵɵproperty"]("ngIf", ctx_r354.contextFilteringPrompt);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r354.contextFilteringPrompt);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r354.currentContextFilters);
    } }
    function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
        var _r376 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "button", 61);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template_button_click_1_listener() { core["ɵɵrestoreView"](_r376); var column_r369 = core["ɵɵnextContext"](2).$implicit; var ctx_r374 = core["ɵɵnextContext"](2); return ctx_r374.clickOnColumn(column_r369); });
        core["ɵɵelement"](2, "i", 62);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
        var _r379 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "button", 61);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template_button_click_1_listener() { core["ɵɵrestoreView"](_r379); var column_r369 = core["ɵɵnextContext"](2).$implicit; var ctx_r377 = core["ɵɵnextContext"](2); return ctx_r377.clickOnColumn(column_r369); });
        core["ɵɵelement"](2, "i", 62);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_ng_container_2_ng_container_11_span_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_1_Template, 3, 0, "span", 2);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_ng_container_11_span_2_span_2_Template, 3, 0, "span", 2);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r369 = core["ɵɵnextContext"]().$implicit;
        var ctx_r370 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r370.isCurrentSortingColumn(column_r369, "ASC"));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r370.isCurrentSortingColumn(column_r369, "DESC"));
    } }
    function MaisTableComponent_ng_container_2_ng_container_11_span_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r369 = core["ɵɵnextContext"]().$implicit;
        var ctx_r371 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r371.resolveLabel(column_r369), " ");
    } }
    function MaisTableComponent_ng_container_2_ng_container_11_Template(rf, ctx) { if (rf & 1) {
        var _r383 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "th", 60);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_ng_container_11_Template_th_click_1_listener() { core["ɵɵrestoreView"](_r383); var column_r369 = ctx.$implicit; var ctx_r382 = core["ɵɵnextContext"](2); return ctx_r382.clickOnColumn(column_r369); });
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_ng_container_11_span_2_Template, 3, 2, "span", 2);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_2_ng_container_11_span_3_Template, 2, 1, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r369 = ctx.$implicit;
        var ctx_r355 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("order-up", ctx_r355.isCurrentSortingColumn(column_r369, "ASC"))("order-down", ctx_r355.isCurrentSortingColumn(column_r369, "DESC"))("border-primary", ctx_r355.isCurrentSortingColumn(column_r369))("clickable", ctx_r355.isSortable(column_r369));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r355.isCurrentSortingColumn(column_r369));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r355.isLabelVisibleInTable(column_r369) && ctx_r355.hasLabel(column_r369));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
        var _r397 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "fa-icon", 71);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template_fa_icon_click_0_listener() { core["ɵɵrestoreView"](_r397); var row_r385 = core["ɵɵnextContext"](2).$implicit; var ctx_r395 = core["ɵɵnextContext"](3); return ctx_r395.clickOnRowExpansion(row_r385); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "caret-square-down");
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
        var _r400 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "fa-icon", 71);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template_fa_icon_click_0_listener() { core["ɵɵrestoreView"](_r400); var row_r385 = core["ɵɵnextContext"](2).$implicit; var ctx_r398 = core["ɵɵnextContext"](3); return ctx_r398.clickOnRowExpansion(row_r385); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "caret-square-up");
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 69);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_1_Template, 1, 1, "fa-icon", 70);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_fa_icon_2_Template, 1, 1, "fa-icon", 70);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r386 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r386.isExpanded(row_r385) && ctx_r386.isExpandable(row_r385));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r386.isExpanded(row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template(rf, ctx) { if (rf & 1) {
        var _r404 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "th", 69);
        core["ɵɵelementStart"](1, "div", 23);
        core["ɵɵelementStart"](2, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template_input_change_2_listener() { core["ɵɵrestoreView"](_r404); var row_r385 = core["ɵɵnextContext"]().$implicit; var ctx_r402 = core["ɵɵnextContext"](3); return ctx_r402.toggleChecked(row_r385); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r387 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("checked", ctx_r387.isChecked(row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "td");
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template(rf, ctx) { }
    var _c7 = function (a0, a1, a2) { return { row: a0, column: a1, value: a2 }; };
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div");
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_ng_template_1_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r406 = core["ɵɵnextContext"]().$implicit;
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r407 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r407.cellTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction3"](2, _c7, row_r385, column_r406, ctx_r407.extractValue(row_r385, column_r406)));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r406 = core["ɵɵnextContext"]().$implicit;
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r408 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r408.extractValue(row_r385, column_r406), " ");
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "td");
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_2_Template, 2, 6, "div", 2);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_div_3_Template, 2, 1, "div", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r406 = ctx.$implicit;
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", column_r406.applyTemplate);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !column_r406.applyTemplate);
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template(rf, ctx) { }
    var _c8 = function (a0) { return { row: a0 }; };
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "tr", 72);
        core["ɵɵelement"](1, "td");
        core["ɵɵelementStart"](2, "td", 73);
        core["ɵɵelementStart"](3, "div", 74);
        core["ɵɵtemplate"](4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_ng_template_4_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r390 = core["ɵɵnextContext"](3);
        core["ɵɵproperty"]("hidden", !ctx_r390.referencedTable.acceptDrop);
        core["ɵɵadvance"](2);
        core["ɵɵattribute"]("colspan", ctx_r390.activeColumnsCount - 1);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r390.referencedTable.dropTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction1"](4, _c8, row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template(rf, ctx) { }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 75);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_ng_template_1_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r391 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r391.dragTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction1"](2, _c8, row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template(rf, ctx) { }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "tr", 76);
        core["ɵɵelement"](1, "td");
        core["ɵɵelementStart"](2, "td");
        core["ɵɵelementStart"](3, "div", 77);
        core["ɵɵtemplate"](4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_ng_template_4_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r385 = core["ɵɵnextContext"]().$implicit;
        var ctx_r392 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](2);
        core["ɵɵattribute"]("colspan", ctx_r392.activeColumnsCount - 1);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r392.rowDetailTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction1"](3, _c8, row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "tr", 64);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_2_Template, 3, 2, "th", 65);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_th_3_Template, 3, 1, "th", 65);
        core["ɵɵtemplate"](4, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_td_4_Template, 1, 0, "td", 2);
        core["ɵɵtemplate"](5, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_ng_container_5_Template, 4, 2, "ng-container", 27);
        core["ɵɵelementStart"](6, "td", 50);
        core["ɵɵtemplate"](7, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_7_Template, 5, 6, "tr", 66);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](8, "td", 50);
        core["ɵɵtemplate"](9, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_div_9_Template, 2, 4, "div", 67);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](10, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_tr_10_Template, 5, 5, "tr", 68);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var row_r385 = ctx.$implicit;
        var ctx_r384 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("row-selected", ctx_r384.isChecked(row_r385));
        core["ɵɵproperty"]("cdkDragData", row_r385)("cdkDragDisabled", !ctx_r384.acceptDrag);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r384.rowExpansionEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r384.currentEnableSelection);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r384.contextFilteringEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r384.visibleColumns);
        core["ɵɵadvance"](5);
        core["ɵɵproperty"]("ngIf", ctx_r384.rowExpansionEnabledAndPossible && ctx_r384.isExpanded(row_r385));
    } }
    function MaisTableComponent_ng_container_2_tbody_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "tbody");
        core["ɵɵelement"](1, "tr", 63);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_tbody_12_ng_container_2_Template, 11, 9, "ng-container", 27);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r356 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngForOf", ctx_r356.currentData);
    } }
    function MaisTableComponent_ng_container_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "tbody");
        core["ɵɵelementStart"](2, "tr", 78);
        core["ɵɵelementStart"](3, "td", 79);
        core["ɵɵtext"](4);
        core["ɵɵpipe"](5, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r357 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](3);
        core["ɵɵattribute"]("colspan", ctx_r357.activeColumnsCount);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](5, 2, "table.common.messages.no_results"), " ");
    } }
    function MaisTableComponent_ng_container_2_ng_container_14_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "tbody");
        core["ɵɵelementStart"](2, "tr", 80);
        core["ɵɵelementStart"](3, "td", 79);
        core["ɵɵtext"](4);
        core["ɵɵpipe"](5, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r358 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](3);
        core["ɵɵattribute"]("colspan", ctx_r358.activeColumnsCount);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](5, 2, "table.common.messages.fetching"), " ");
    } }
    function MaisTableComponent_ng_container_2_Template(rf, ctx) { if (rf & 1) {
        var _r421 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 48);
        core["ɵɵelementStart"](2, "table", 49);
        core["ɵɵlistener"]("cdkDropListDropped", function MaisTableComponent_ng_container_2_Template_table_cdkDropListDropped_2_listener($event) { core["ɵɵrestoreView"](_r421); var ctx_r420 = core["ɵɵnextContext"](); return ctx_r420.handleItemDropped($event); });
        core["ɵɵelementStart"](3, "caption", 50);
        core["ɵɵtext"](4);
        core["ɵɵpipe"](5, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](6, "thead", 51);
        core["ɵɵelementStart"](7, "tr");
        core["ɵɵtemplate"](8, MaisTableComponent_ng_container_2_th_8_Template, 1, 0, "th", 52);
        core["ɵɵtemplate"](9, MaisTableComponent_ng_container_2_th_9_Template, 3, 1, "th", 52);
        core["ɵɵtemplate"](10, MaisTableComponent_ng_container_2_th_10_Template, 8, 6, "th", 53);
        core["ɵɵtemplate"](11, MaisTableComponent_ng_container_2_ng_container_11_Template, 4, 10, "ng-container", 27);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](12, MaisTableComponent_ng_container_2_tbody_12_Template, 3, 1, "tbody", 2);
        core["ɵɵtemplate"](13, MaisTableComponent_ng_container_2_ng_container_13_Template, 6, 4, "ng-container", 2);
        core["ɵɵtemplate"](14, MaisTableComponent_ng_container_2_ng_container_14_Template, 6, 4, "ng-container", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r264 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](2);
        core["ɵɵclassProp"]("nodrop", !ctx_r264.acceptDrop)("acceptdrop", ctx_r264.acceptDrop);
        core["ɵɵpropertyInterpolate"]("id", ctx_r264.currentTableId);
        core["ɵɵproperty"]("cdkDropListConnectedTo", ctx_r264.dropConnectedTo)("cdkDropListData", ctx_r264.currentData)("cdkDropListEnterPredicate", ctx_r264.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r264.acceptDrop);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](5, 17, "table.common.accessibility.caption"));
        core["ɵɵadvance"](4);
        core["ɵɵproperty"]("ngIf", ctx_r264.rowExpansionEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r264.currentEnableSelection);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r264.contextFilteringEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r264.visibleColumns);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r264.showFetching && !ctx_r264.forceReRender);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r264.showFetching && ctx_r264.noResults);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r264.showFetching);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-header-cell", 93);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template(rf, ctx) { if (rf & 1) {
        var _r443 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "fa-icon", 71);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template_fa_icon_click_0_listener() { core["ɵɵrestoreView"](_r443); var row_r438 = core["ɵɵnextContext"]().$implicit; var ctx_r441 = core["ɵɵnextContext"](4); return ctx_r441.clickOnRowExpansion(row_r438); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "caret-square-down");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template(rf, ctx) { if (rf & 1) {
        var _r446 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "fa-icon", 71);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template_fa_icon_click_0_listener() { core["ɵɵrestoreView"](_r446); var row_r438 = core["ɵɵnextContext"]().$implicit; var ctx_r444 = core["ɵɵnextContext"](4); return ctx_r444.clickOnRowExpansion(row_r438); });
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵproperty"]("icon", "caret-square-up");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-cell", 93);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_1_Template, 1, 1, "fa-icon", 70);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_fa_icon_2_Template, 1, 1, "fa-icon", 70);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r438 = ctx.$implicit;
        var ctx_r428 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r428.isExpanded(row_r438) && ctx_r428.isExpandable(row_r438));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r428.isExpanded(row_r438));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template(rf, ctx) { if (rf & 1) {
        var _r449 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template_input_change_1_listener() { core["ɵɵrestoreView"](_r449); var ctx_r448 = core["ɵɵnextContext"](5); return ctx_r448.toggleAllChecked(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r447 = core["ɵɵnextContext"](5);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("checked", ctx_r447.allChecked);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-header-cell", 93);
        core["ɵɵelementStart"](1, "div", 23);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_span_2_Template, 2, 1, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r429 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r429.currentEnableSelectAll && ctx_r429.currentEnableMultiSelect && !ctx_r429.noResults);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template(rf, ctx) { if (rf & 1) {
        var _r452 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-cell", 93);
        core["ɵɵelementStart"](1, "div", 23);
        core["ɵɵelementStart"](2, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template_input_change_2_listener() { core["ɵɵrestoreView"](_r452); var row_r450 = ctx.$implicit; var ctx_r451 = core["ɵɵnextContext"](4); return ctx_r451.toggleChecked(row_r450); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r450 = ctx.$implicit;
        var ctx_r430 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("checked", ctx_r430.isChecked(row_r450));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵpipe"](2, "translate");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r453 = core["ɵɵnextContext"](5);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](2, 1, ctx_r453.contextFilteringPrompt), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵpipe"](2, "translate");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](2, 1, "table.common.messages.context_filter_prompt"), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template(rf, ctx) { if (rf & 1) {
        var _r458 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 20);
        core["ɵɵelementStart"](1, "input", 24);
        core["ɵɵlistener"]("change", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_input_change_1_listener($event) { core["ɵɵrestoreView"](_r458); var filter_r456 = ctx.$implicit; var ctx_r457 = core["ɵɵnextContext"](5); ctx_r457.toggleContextFilterChecked(filter_r456); return $event.stopPropagation(); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](2, "span", 59);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template_span_click_2_listener($event) { core["ɵɵrestoreView"](_r458); var filter_r456 = ctx.$implicit; var ctx_r459 = core["ɵɵnextContext"](5); ctx_r459.toggleContextFilterChecked(filter_r456); return $event.stopPropagation(); });
        core["ɵɵtext"](3);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var filter_r456 = ctx.$implicit;
        var ctx_r455 = core["ɵɵnextContext"](5);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("checked", ctx_r455.isContextFilterChecked(filter_r456));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", ctx_r455.resolveLabel(filter_r456), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-header-cell", 93);
        core["ɵɵelementStart"](1, "div", 94);
        core["ɵɵelementStart"](2, "div", 56);
        core["ɵɵelementStart"](3, "div", 57);
        core["ɵɵelementStart"](4, "div", 19);
        core["ɵɵelementStart"](5, "h6", 39);
        core["ɵɵtemplate"](6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_6_Template, 3, 3, "span", 2);
        core["ɵɵtemplate"](7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_span_7_Template, 3, 3, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](8, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_div_8_Template, 4, 2, "div", 58);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r431 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("border-primary", ctx_r431.anyContextFilterChecked);
        core["ɵɵproperty"]("autoClose", "outside")("container", "body");
        core["ɵɵadvance"](5);
        core["ɵɵproperty"]("ngIf", ctx_r431.contextFilteringPrompt);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r431.contextFilteringPrompt);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r431.currentContextFilters);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-cell", 93);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template(rf, ctx) { }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-cell");
        core["ɵɵelementStart"](1, "div", 77);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_ng_template_2_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var detail_r461 = ctx.$implicit;
        var ctx_r433 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r433.rowDetailTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction1"](2, _c8, detail_r461.___parentRow));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template(rf, ctx) { if (rf & 1) {
        var _r472 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "button", 61);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template_button_click_1_listener() { core["ɵɵrestoreView"](_r472); var column_r463 = core["ɵɵnextContext"](3).$implicit; var ctx_r470 = core["ɵɵnextContext"](4); return ctx_r470.clickOnColumn(column_r463); });
        core["ɵɵelement"](2, "i", 62);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template(rf, ctx) { if (rf & 1) {
        var _r475 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "span");
        core["ɵɵelementStart"](1, "button", 61);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template_button_click_1_listener() { core["ɵɵrestoreView"](_r475); var column_r463 = core["ɵɵnextContext"](3).$implicit; var ctx_r473 = core["ɵɵnextContext"](4); return ctx_r473.clickOnColumn(column_r463); });
        core["ɵɵelement"](2, "i", 62);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_1_Template, 3, 0, "span", 2);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_span_2_Template, 3, 0, "span", 2);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r463 = core["ɵɵnextContext"](2).$implicit;
        var ctx_r466 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r466.isCurrentSortingColumn(column_r463, "ASC"));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r466.isCurrentSortingColumn(column_r463, "DESC"));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r463 = core["ɵɵnextContext"](2).$implicit;
        var ctx_r467 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r467.resolveLabel(column_r463), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template(rf, ctx) { if (rf & 1) {
        var _r480 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-header-cell");
        core["ɵɵelementStart"](1, "div", 25);
        core["ɵɵlistener"]("click", function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template_div_click_1_listener() { core["ɵɵrestoreView"](_r480); var column_r463 = core["ɵɵnextContext"]().$implicit; var ctx_r478 = core["ɵɵnextContext"](4); return ctx_r478.clickOnColumn(column_r463); });
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_2_Template, 3, 2, "span", 2);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_span_3_Template, 2, 1, "span", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r463 = core["ɵɵnextContext"]().$implicit;
        var ctx_r464 = core["ɵɵnextContext"](4);
        core["ɵɵclassMapInterpolate1"]("maissize-", column_r463.size || "default", "");
        core["ɵɵadvance"](1);
        core["ɵɵclassMap"](column_r463.headerDisplayClass || "");
        core["ɵɵclassProp"]("order-up", ctx_r464.isCurrentSortingColumn(column_r463, "ASC"))("order-down", ctx_r464.isCurrentSortingColumn(column_r463, "DESC"))("border-primary", ctx_r464.isCurrentSortingColumn(column_r463))("clickable", ctx_r464.isSortable(column_r463));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r464.isCurrentSortingColumn(column_r463));
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r464.isLabelVisibleInTable(column_r463) && ctx_r464.hasLabel(column_r463));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template(rf, ctx) { }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div");
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_ng_template_1_Template, 0, 0, "ng-template", 9);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r482 = core["ɵɵnextContext"]().$implicit;
        var column_r463 = core["ɵɵnextContext"]().$implicit;
        var ctx_r483 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r483.cellTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction3"](2, _c7, row_r482, column_r463, ctx_r483.extractValue(row_r482, column_r463)));
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r482 = core["ɵɵnextContext"]().$implicit;
        var column_r463 = core["ɵɵnextContext"]().$implicit;
        var ctx_r484 = core["ɵɵnextContext"](4);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r484.extractValue(row_r482, column_r463), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-cell");
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_1_Template, 2, 6, "div", 2);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_div_2_Template, 2, 1, "div", 2);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var column_r463 = core["ɵɵnextContext"]().$implicit;
        core["ɵɵclassMapInterpolate2"]("maissize-", column_r463.size || "default", " ", column_r463.cellDisplayClass || "", "");
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", column_r463.applyTemplate);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !column_r463.applyTemplate);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0, 85);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_header_cell_1_Template, 4, 16, "mat-header-cell", 95);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_mat_cell_2_Template, 3, 6, "mat-cell", 96);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var column_r463 = ctx.$implicit;
        core["ɵɵproperty"]("matColumnDef", column_r463.name);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-header-row");
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-row", 97);
    } if (rf & 2) {
        var row_r491 = ctx.$implicit;
        var ctx_r436 = core["ɵɵnextContext"](4);
        core["ɵɵclassProp"]("expanded", ctx_r436.isMatTableExpanded(row_r491))("row-selected", ctx_r436.isChecked(row_r491));
        core["ɵɵproperty"]("cdkDragData", row_r491)("cdkDragDisabled", !ctx_r436.acceptDrag);
    } }
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-row", 98);
    } if (rf & 2) {
        var row_r492 = ctx.$implicit;
        var ctx_r437 = core["ɵɵnextContext"](4);
        core["ɵɵproperty"]("@detailExpand", ctx_r437.isMatTableExpanded(row_r492) ? "expanded" : "collapsed");
    } }
    var _c9 = function () { return ["internal___col_row_detail"]; };
    function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template(rf, ctx) { if (rf & 1) {
        var _r494 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-table", 83, 84);
        core["ɵɵlistener"]("cdkDropListDropped", function MaisTableComponent_ng_container_3_div_1_mat_table_1_Template_mat_table_cdkDropListDropped_0_listener($event) { core["ɵɵrestoreView"](_r494); var ctx_r493 = core["ɵɵnextContext"](3); return ctx_r493.handleItemDropped($event); });
        core["ɵɵelementContainerStart"](2, 85);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_3_Template, 1, 0, "mat-header-cell", 86);
        core["ɵɵtemplate"](4, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_4_Template, 3, 2, "mat-cell", 87);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](5, 85);
        core["ɵɵtemplate"](6, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_6_Template, 3, 1, "mat-header-cell", 86);
        core["ɵɵtemplate"](7, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_7_Template, 3, 1, "mat-cell", 87);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](8, 85);
        core["ɵɵtemplate"](9, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_cell_9_Template, 9, 7, "mat-header-cell", 86);
        core["ɵɵtemplate"](10, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_10_Template, 1, 0, "mat-cell", 87);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](11, 85);
        core["ɵɵtemplate"](12, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_cell_12_Template, 3, 4, "mat-cell", 88);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵtemplate"](13, MaisTableComponent_ng_container_3_div_1_mat_table_1_ng_container_13_Template, 3, 1, "ng-container", 89);
        core["ɵɵtemplate"](14, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_header_row_14_Template, 1, 0, "mat-header-row", 90);
        core["ɵɵtemplate"](15, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_15_Template, 1, 6, "mat-row", 91);
        core["ɵɵtemplate"](16, MaisTableComponent_ng_container_3_div_1_mat_table_1_mat_row_16_Template, 1, 1, "mat-row", 92);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r423 = core["ɵɵnextContext"](3);
        core["ɵɵclassProp"]("nodrop", !ctx_r423.acceptDrop)("acceptdrop", ctx_r423.acceptDrop);
        core["ɵɵpropertyInterpolate"]("id", ctx_r423.currentTableId);
        core["ɵɵproperty"]("dataSource", ctx_r423.matTableDataObservable)("cdkDropListConnectedTo", ctx_r423.dropConnectedTo)("cdkDropListData", ctx_r423.currentData)("cdkDropListEnterPredicate", ctx_r423.acceptDropPredicate)("cdkDropListSortingDisabled", !ctx_r423.acceptDrop);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("matColumnDef", "internal___col_row_expansion");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("matColumnDef", "internal___col_row_selection");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("matColumnDef", "internal___col_context_filtering");
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("matColumnDef", "internal___col_row_detail");
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngForOf", ctx_r423.matTableVisibleColumns);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("matHeaderRowDef", ctx_r423.matTableVisibleColumnDefs);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("matRowDefColumns", ctx_r423.matTableVisibleColumnDefs);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("matRowDefColumns", core["ɵɵpureFunction0"](19, _c9))("matRowDefWhen", ctx_r423.isMatTableExpansionDetailRow);
    } }
    function MaisTableComponent_ng_container_3_div_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 78);
        core["ɵɵelementStart"](2, "p", 99);
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](4, 1, "table.common.messages.no_results"), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 80);
        core["ɵɵelementStart"](2, "p", 99);
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind1"](4, 1, "table.common.messages.fetching"), " ");
    } }
    function MaisTableComponent_ng_container_3_div_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 48);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_mat_table_1_Template, 17, 20, "mat-table", 82);
        core["ɵɵtemplate"](2, MaisTableComponent_ng_container_3_div_1_ng_container_2_Template, 5, 3, "ng-container", 2);
        core["ɵɵtemplate"](3, MaisTableComponent_ng_container_3_div_1_ng_container_3_Template, 5, 3, "ng-container", 2);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r422 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r422.showFetching && !ctx_r422.forceReRender);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !ctx_r422.showFetching && ctx_r422.noResults);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r422.showFetching);
    } }
    function MaisTableComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_ng_container_3_div_1_Template, 4, 3, "div", 81);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r265 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r265.matTableDataObservable);
    } }
    function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span", 113);
        core["ɵɵtext"](1, "(current)");
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template(rf, ctx) { if (rf & 1) {
        var _r507 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "li", 106);
        core["ɵɵelementStart"](1, "a", 107);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template_a_click_1_listener() { core["ɵɵrestoreView"](_r507); var page_r501 = core["ɵɵnextContext"]().$implicit; var ctx_r505 = core["ɵɵnextContext"](3); return ctx_r505.switchToPage(page_r501); });
        core["ɵɵtext"](2);
        core["ɵɵtemplate"](3, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_span_3_Template, 2, 0, "span", 112);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var page_r501 = core["ɵɵnextContext"]().$implicit;
        var ctx_r502 = core["ɵɵnextContext"](3);
        core["ɵɵclassProp"]("active", page_r501 === ctx_r502.currentPageIndex);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate1"](" ", page_r501 + 1, " ");
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", page_r501 === ctx_r502.currentPageIndex);
    } }
    function MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "li", 114);
        core["ɵɵelementStart"](1, "a", 115);
        core["ɵɵtext"](2, " ... ");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function MaisTableComponent_div_4_ng_container_3_ng_container_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_1_Template, 4, 4, "li", 110);
        core["ɵɵtemplate"](2, MaisTableComponent_div_4_ng_container_3_ng_container_15_li_2_Template, 3, 0, "li", 111);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var page_r501 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", !page_r501.skip);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", page_r501.skip);
    } }
    function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template(rf, ctx) { if (rf & 1) {
        var _r512 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 47);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r512); var pageSize_r510 = ctx.$implicit; var ctx_r511 = core["ɵɵnextContext"](4); return ctx_r511.clickOnPageSize(pageSize_r510); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var pageSize_r510 = ctx.$implicit;
        var ctx_r509 = core["ɵɵnextContext"](4);
        core["ɵɵproperty"]("disabled", pageSize_r510 === ctx_r509.currentPageSize);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", pageSize_r510, " ");
    } }
    var _c10 = function (a0) { return { current: a0 }; };
    function MaisTableComponent_div_4_ng_container_3_span_25_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "span", 116);
        core["ɵɵelementStart"](1, "div");
        core["ɵɵtext"](2, " \u00A0 ");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](3, "button", 117);
        core["ɵɵtext"](4);
        core["ɵɵpipe"](5, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](6, "span", 44);
        core["ɵɵtemplate"](7, MaisTableComponent_div_4_ng_container_3_span_25_button_7_Template, 2, 2, "button", 46);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r500 = core["ɵɵnextContext"](3);
        core["ɵɵadvance"](4);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind2"](5, 2, "table.common.messages.page_size_button", core["ɵɵpureFunction1"](5, _c10, ctx_r500.currentPageSize)));
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngForOf", ctx_r500.currentPossiblePageSizes);
    } }
    var _c11 = function (a0, a1) { return { totalElements: a0, totalPages: a1 }; };
    function MaisTableComponent_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
        var _r514 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementContainerStart"](0);
        core["ɵɵelementStart"](1, "div", 102);
        core["ɵɵelementStart"](2, "div", 103);
        core["ɵɵtext"](3);
        core["ɵɵpipe"](4, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "nav", 104);
        core["ɵɵelementStart"](6, "ul", 105);
        core["ɵɵelementStart"](7, "li", 106);
        core["ɵɵelementStart"](8, "a", 107);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_8_listener() { core["ɵɵrestoreView"](_r514); var ctx_r513 = core["ɵɵnextContext"](2); return ctx_r513.switchToPage(0); });
        core["ɵɵtext"](9);
        core["ɵɵpipe"](10, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](11, "li", 106);
        core["ɵɵelementStart"](12, "a", 107);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_12_listener() { core["ɵɵrestoreView"](_r514); var ctx_r515 = core["ɵɵnextContext"](2); return ctx_r515.switchToPage(ctx_r515.currentPageIndex - 1); });
        core["ɵɵtext"](13);
        core["ɵɵpipe"](14, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](15, MaisTableComponent_div_4_ng_container_3_ng_container_15_Template, 3, 2, "ng-container", 27);
        core["ɵɵelementStart"](16, "li", 106);
        core["ɵɵelementStart"](17, "a", 107);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_17_listener() { core["ɵɵrestoreView"](_r514); var ctx_r516 = core["ɵɵnextContext"](2); return ctx_r516.switchToPage(ctx_r516.currentPageIndex + 1); });
        core["ɵɵtext"](18);
        core["ɵɵpipe"](19, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](20, "li", 106);
        core["ɵɵelementStart"](21, "a", 107);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_3_Template_a_click_21_listener() { core["ɵɵrestoreView"](_r514); var ctx_r517 = core["ɵɵnextContext"](2); return ctx_r517.switchToPage(ctx_r517.currentPageCount - 1); });
        core["ɵɵtext"](22);
        core["ɵɵpipe"](23, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](24, "div", 108);
        core["ɵɵtemplate"](25, MaisTableComponent_div_4_ng_container_3_span_25_Template, 8, 7, "span", 109);
        core["ɵɵelementEnd"]();
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r495 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate1"](" ", core["ɵɵpipeBind2"](4, 15, "table.common.pagination.total_elements", core["ɵɵpureFunction2"](26, _c11, ctx_r495.currentResultNumber, ctx_r495.currentPageCount)), " ");
        core["ɵɵadvance"](4);
        core["ɵɵclassProp"]("disabled", !(ctx_r495.currentPageIndex > 0));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](10, 18, "table.common.pagination.first_page"));
        core["ɵɵadvance"](2);
        core["ɵɵclassProp"]("disabled", !(ctx_r495.currentPageIndex > 0));
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](14, 20, "table.common.pagination.previous_page"));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngForOf", ctx_r495.enumPages);
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("disabled", ctx_r495.currentPageIndex >= ctx_r495.currentPageCount - 1);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](19, 22, "table.common.pagination.next_page"));
        core["ɵɵadvance"](2);
        core["ɵɵclassProp"]("disabled", ctx_r495.currentPageIndex >= ctx_r495.currentPageCount - 1);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](23, 24, "table.common.pagination.last_page"));
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r495.pageSizeSelectEnabledAndPossible);
    } }
    function MaisTableComponent_div_4_ng_template_6_Template(rf, ctx) { }
    function MaisTableComponent_div_4_ng_container_8_button_1_Template(rf, ctx) { if (rf & 1) {
        var _r521 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 41);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_ng_container_8_button_1_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r521); var action_r519 = ctx.$implicit; var ctx_r520 = core["ɵɵnextContext"](3); return ctx_r520.clickOnContextAction(action_r519); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var action_r519 = ctx.$implicit;
        var ctx_r518 = core["ɵɵnextContext"](3);
        core["ɵɵclassMapInterpolate2"]("btn btn-", action_r519.displayClass || "light", " ", action_r519.additionalClasses || "", " mr-1");
        core["ɵɵproperty"]("disabled", !ctx_r518.isContextActionAllowed(action_r519));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r518.resolveLabel(action_r519), " ");
    } }
    function MaisTableComponent_div_4_ng_container_8_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, MaisTableComponent_div_4_ng_container_8_button_1_Template, 2, 6, "button", 40);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var ctx_r497 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", ctx_r497.headerActions);
    } }
    function MaisTableComponent_div_4_div_9_button_9_Template(rf, ctx) { if (rf & 1) {
        var _r525 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 47);
        core["ɵɵlistener"]("click", function MaisTableComponent_div_4_div_9_button_9_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r525); var action_r523 = ctx.$implicit; var ctx_r524 = core["ɵɵnextContext"](3); return ctx_r524.clickOnContextAction(action_r523); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var action_r523 = ctx.$implicit;
        var ctx_r522 = core["ɵɵnextContext"](3);
        core["ɵɵproperty"]("disabled", !ctx_r522.isContextActionAllowed(action_r523));
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", ctx_r522.resolveLabel(action_r523), " ");
    } }
    function MaisTableComponent_div_4_div_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 42);
        core["ɵɵelementStart"](1, "button", 43);
        core["ɵɵtext"](2);
        core["ɵɵpipe"](3, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](4, "div", 44);
        core["ɵɵelementStart"](5, "div", 45);
        core["ɵɵelementStart"](6, "h6", 21);
        core["ɵɵtext"](7);
        core["ɵɵpipe"](8, "translate");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](9, MaisTableComponent_div_4_div_9_button_9_Template, 2, 2, "button", 46);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r498 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("disabled", !ctx_r498.anyButtonActionsAllowed);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](3, 4, "table.common.messages.actions_button"));
        core["ɵɵadvance"](5);
        core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](8, 6, "table.common.messages.pick_action"));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngForOf", ctx_r498.currentActions);
    } }
    function MaisTableComponent_div_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 100);
        core["ɵɵelementStart"](1, "div", 5);
        core["ɵɵelementStart"](2, "div", 101);
        core["ɵɵtemplate"](3, MaisTableComponent_div_4_ng_container_3_Template, 26, 29, "ng-container", 2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](4, "div", 8);
        core["ɵɵelementStart"](5, "div");
        core["ɵɵtemplate"](6, MaisTableComponent_div_4_ng_template_6_Template, 0, 0, "ng-template", 9);
        core["ɵɵtext"](7, " \u00A0 ");
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](8, MaisTableComponent_div_4_ng_container_8_Template, 2, 1, "ng-container", 2);
        core["ɵɵtemplate"](9, MaisTableComponent_div_4_div_9_Template, 10, 8, "div", 10);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r266 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r266.currentResultNumber > 0 && ctx_r266.currentEnablePagination);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngTemplateOutlet", ctx_r266.actionsCaptionTemplate)("ngTemplateOutletContext", core["ɵɵpureFunction0"](5, _c6));
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r266.headerActionsEnabledAndPossible);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r266.contextActionsEnabledAndPossible);
    } }
    var MaisTableComponent = /** @class */ (function () {
        // lifecycle hooks
        function MaisTableComponent(translateService, configurationService, registry, cdr, ngZone) {
            var _this = this;
            this.translateService = translateService;
            this.configurationService = configurationService;
            this.registry = registry;
            this.cdr = cdr;
            this.ngZone = ngZone;
            this.dataProvider = null;
            this.defaultSortingColumn = null;
            this.defaultSortingDirection = null;
            this.defaultPageSize = null;
            this.possiblePageSize = null;
            // output events
            this.pageChange = new core.EventEmitter();
            this.sortChange = new core.EventEmitter();
            this.selectionChange = new core.EventEmitter();
            this.filteringColumnsChange = new core.EventEmitter();
            this.visibleColumnsChange = new core.EventEmitter();
            this.contextFiltersChange = new core.EventEmitter();
            this.statusChange = new core.EventEmitter();
            this.itemDragged = new core.EventEmitter();
            this.itemDropped = new core.EventEmitter();
            this.action = new core.EventEmitter();
            this.isIE = MaisTableBrowserHelper.isIE();
            this.statusSnapshot = null;
            this.persistableStatusSnapshot = null;
            this.forceReRender = false;
            this.initialized = false;
            this.fetching = false;
            this.showFetching = false;
            this.refreshEmitterSubscription = null;
            this.refreshIntervalSubscription = null;
            // MatTable adapter
            this.matTableDataObservable = null;
            this.expandedElement = null;
            // drag and drop support
            this.acceptDropPredicate = function (item) {
                return _this.acceptDrop;
            };
            this.isMatTableExpansionDetailRow = function (i, row) { return row.hasOwnProperty('___detailRowContent'); };
            this.isMatTableExpanded = function (row, limit) {
                if (limit === void 0) { limit = false; }
                if (!limit && row.___detailRow) {
                    if (_this.isMatTableExpanded(row.___detailRow, true)) {
                        return true;
                    }
                }
                if (!limit && row.___parentRow) {
                    if (_this.isMatTableExpanded(row.___parentRow, true)) {
                        return true;
                    }
                }
                if (!_this.rowExpansionEnabledAndPossible) {
                    return false;
                }
                if (!_this.isExpandable(row)) {
                    return false;
                }
                if (!_this.isExpanded(row)) {
                    return false;
                }
                return true;
            };
            this.uuid = 'MTCMP-' + (++MaisTableComponent.counter) + '-' + Math.round(Math.random() * 100000);
            this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
            this.logger.trace('building component');
            this.matTableDataObservable = new rxjs.Subject();
        }
        MaisTableComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.logger = new MaisTableLogger('MaisTableComponent_' + this.currentTableId);
            this.logger.trace('initializing component');
            this.registrationId = this.registry.register(this.currentTableId, this);
            // input validation
            MaisTableValidatorHelper.validateColumnsSpecification(this.columns, this.configurationService.getConfiguration());
            this.clientDataSnapshot = [];
            this.dataSnapshot = [];
            this.fetchedPageCount = 0;
            this.fetchedResultNumber = 0;
            this.selectedPageIndex = 0;
            this.selectedSearchQuery = null;
            this.selectedPageSize = null;
            this.expandedItems = [];
            this.checkedItems = [];
            this.checkedContextFilters = [];
            this.checkedColumnsForFiltering = this.filterableColumns.filter(function (c) { return MaisTableFormatterHelper.isDefaultFilterColumn(c, _this.configurationService.getConfiguration()); });
            this.checkedColumnsForVisualization = this.columns.filter(function (c) { return MaisTableFormatterHelper.isDefaultVisibleColumn(c, _this.configurationService.getConfiguration()); });
            this.statusSnapshot = this.buildStatusSnapshot();
            this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
            if (this.dataProvider) {
                // input is a provider function
                if (this.data) {
                    throw new Error('MaisTable can\'t be provided both data and dataProvider');
                }
            }
            else if (this.data instanceof rxjs.Observable || this.data instanceof rxjs.Subject) {
                // input is observable
                this.data.subscribe(function (dataSnapshot) {
                    _this.handleInputDataObservableEmission(dataSnapshot);
                });
            }
            else {
                // input is data array
                this.clientDataSnapshot = this.data;
            }
            // LOAD STATUS from store if possible
            var activationObservable;
            if (this.storePersistenceEnabledAndPossible) {
                activationObservable = this.loadStatusFromStore();
            }
            else {
                activationObservable = new rxjs.Observable(function (subscriber) {
                    subscriber.next(null);
                    subscriber.complete();
                });
            }
            activationObservable.subscribe(function (status) {
                if (status) {
                    _this.logger.trace('restored status snapshot from storage', status);
                    _this.applyStatusSnapshot(status);
                }
                _this.reload({ reason: exports.MaisTableReloadReason.INTERNAL, withStatusSnapshot: status }).subscribe(function (yes) {
                    _this.completeInitialization();
                }, function (nope) {
                    _this.completeInitialization();
                });
            }, function (failure) {
                _this.logger.error('restoring status from store failed', failure);
                _this.reload({ reason: exports.MaisTableReloadReason.INTERNAL }).subscribe(function (yes) {
                    _this.completeInitialization();
                }, function (nope) {
                    _this.completeInitialization();
                });
            });
        };
        MaisTableComponent.prototype.ngAfterContentInit = function () {
            this.logger.debug('after content init');
        };
        MaisTableComponent.prototype.handleInputDataObservableEmission = function (dataSnapshot) {
            this.logger.debug('data snapshot emitted from input data');
            this.clientDataSnapshot = dataSnapshot;
            this.reload({ reason: exports.MaisTableReloadReason.EXTERNAL });
        };
        MaisTableComponent.prototype.completeInitialization = function () {
            var _this = this;
            this.initialized = true;
            this.statusChange.pipe(operators.debounceTime(200)).subscribe(function (statusSnapshot) {
                _this.persistStatusToStore().subscribe();
            });
            if (this.refreshEmitter) {
                this.handleRefreshEmitterChange(this.refreshEmitter);
            }
            if (this.refreshInterval) {
                this.handleRefreshIntervalChange(this.refreshInterval);
            }
        };
        MaisTableComponent.prototype.ngOnDestroy = function () {
            this.logger.trace('destroying component');
            if (this.registrationId) {
                this.registry.unregister(this.currentTableId, this.registrationId);
            }
        };
        MaisTableComponent.prototype.ngOnChanges = function (changes) {
            if (changes.refreshEmitter) {
                this.handleRefreshEmitterChange(changes.refreshEmitter.currentValue);
            }
            if (changes.refreshInterval) {
                this.handleRefreshIntervalChange(changes.refreshInterval.currentValue);
            }
            if (changes.refreshStrategy && !changes.refreshStrategy.firstChange) {
                this.logger.warn('REFRESH STRATEGY CHANGED WHILE RUNNING. YOU SURE ABOUT THIS?', changes);
                this.handleRefreshEmitterChange(this.refreshEmitter);
                this.handleRefreshIntervalChange(this.refreshInterval);
            }
        };
        MaisTableComponent.prototype.handleRefreshEmitterChange = function (newValue) {
            var _this = this;
            if (this.refreshEmitterSubscription) {
                this.refreshEmitterSubscription.unsubscribe();
            }
            if (newValue) {
                this.refreshEmitterSubscription = newValue.subscribe(function (event) {
                    _this.logger.trace('received refresh push request', event);
                    if (_this.dragInProgress) {
                        _this.logger.warn('refresh from emitter ignored because user is dragging elements');
                    }
                    else if (_this.currentRefreshStrategies.indexOf(exports.MaisTableRefreshStrategy.ON_PUSH) === -1) {
                        _this.logger.warn('refresh from emitter ignored because refresh strategies are ' + _this.currentRefreshStrategies +
                            '. Why are you pushing to this component?');
                    }
                    else if (!_this.initialized) {
                        _this.logger.warn('refresh from emitter ignored because the component is not fully initialized');
                    }
                    else if (_this.fetching) {
                        _this.logger.warn('refresh from emitter ignored because the component is fetching already');
                    }
                    else {
                        _this.logger.debug('launching reload following push request');
                        _this.reload({
                            reason: exports.MaisTableReloadReason.PUSH,
                            pushRequest: event,
                            inBackground: event.inBackground === true || event.inBackground === false ?
                                event.inBackground : _this.currentRefreshOnPushInBackground
                        });
                    }
                });
            }
        };
        MaisTableComponent.prototype.handleRefreshIntervalChange = function (newValue) {
            var _this = this;
            if (this.refreshIntervalSubscription) {
                this.refreshIntervalSubscription.unsubscribe();
            }
            if (this.refreshIntervalTimer) {
                this.refreshIntervalTimer = null;
            }
            if (newValue) {
                this.refreshIntervalTimer = rxjs.timer(newValue, newValue);
                this.refreshIntervalSubscription = this.refreshIntervalTimer.subscribe(function (tick) {
                    _this.logger.trace('emitted refresh tick request');
                    if (_this.dragInProgress) {
                        _this.logger.warn('refresh from emitter ignored because user is dragging elements');
                    }
                    else if (_this.currentRefreshStrategies.indexOf(exports.MaisTableRefreshStrategy.TIMED) === -1) {
                        _this.logger.warn('refresh from tick ignored because refresh strategies are ' + _this.currentRefreshStrategies +
                            '. Why is this component emitting ticks ?');
                    }
                    else if (!_this.initialized) {
                        _this.logger.warn('refresh from tick ignored because the component is not fully initialized');
                    }
                    else if (_this.fetching) {
                        _this.logger.warn('refresh from tick ignored because the component is fetching already');
                    }
                    else {
                        _this.logger.debug('launching reload following tick');
                        _this.reload({
                            reason: exports.MaisTableReloadReason.INTERVAL,
                            inBackground: _this.currentRefreshIntervalInBackground
                        });
                    }
                });
            }
        };
        // public methods
        MaisTableComponent.prototype.refresh = function (background) {
            if (!this.initialized) {
                return rxjs.throwError('table component is still initializing');
            }
            return this.reload({ reason: exports.MaisTableReloadReason.EXTERNAL, inBackground: background });
        };
        MaisTableComponent.prototype.getDataSnapshot = function () {
            return this.dataSnapshot;
        };
        MaisTableComponent.prototype.getStatusSnapshot = function () {
            return this.statusSnapshot;
        };
        MaisTableComponent.prototype.loadStatus = function (status) {
            if (!this.initialized) {
                return rxjs.throwError('table component is still initializing');
            }
            this.logger.trace('loading status snapshot from external caller', status);
            this.applyStatusSnapshot(status);
            this.reload({ reason: exports.MaisTableReloadReason.EXTERNAL, withStatusSnapshot: status });
        };
        MaisTableComponent.prototype.applyStatusSnapshot = function (status) {
            var _this = this;
            if (!status || !status.schemaVersion) {
                this.logger.warn('not restoring status because it is malformed');
                return;
            }
            if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
                this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                    status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
                return;
            }
            if (status.orderColumn) {
                this.selectedSortColumn = this.columns.find(function (c) { return _this.isSortable(c) && c.name === status.orderColumn; }) || null;
            }
            if (status.orderColumnDirection) {
                this.selectedSortDirection = (status.orderColumnDirection === exports.MaisTableSortDirection.DESCENDING) ?
                    exports.MaisTableSortDirection.DESCENDING : exports.MaisTableSortDirection.ASCENDING;
            }
            if (this.currentEnableFiltering && status.query) {
                this.selectedSearchQuery = status.query.trim();
            }
            if (this.currentEnableFiltering && status.queryColumns && status.queryColumns.length) {
                this.checkedColumnsForFiltering = this.filterableColumns.filter(function (c) { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.queryColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
            }
            if (this.currentEnableColumnsSelection && status.visibleColumns && status.visibleColumns.length) {
                this.checkedColumnsForVisualization = this.columns.filter(function (c) { var _a, _b; return !_this.isHideable(c) || ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.visibleColumns) === null || _b === void 0 ? void 0 : _b.indexOf(c.name)) !== -1; });
            }
            if (this.currentEnableContextFiltering && status.contextFilters && status.contextFilters.length) {
                this.checkedContextFilters = this.contextFilters.filter(function (f) { var _a, _b; return ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.contextFilters) === null || _b === void 0 ? void 0 : _b.indexOf(f.name)) !== -1; });
            }
            if (this.currentEnablePagination && status.currentPage || status.currentPage === 0) {
                this.selectedPageIndex = status.currentPage;
            }
            if (this.currentEnablePagination && status.pageSize) {
                this.selectedPageSize = this.currentPossiblePageSizes.find(function (s) { return status.pageSize === s; }) || null;
            }
            this.statusSnapshot = this.buildStatusSnapshot();
            this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        };
        MaisTableComponent.prototype.applyStatusSnapshotPostFetch = function (status) {
            var _this = this;
            if (!status || !status.schemaVersion) {
                this.logger.warn('not restoring status because it is malformed');
                return;
            }
            if (status.schemaVersion !== this.configurationService.getConfiguration().currentSchemaVersion) {
                this.logger.warn('not restoring status because it is obsolete (snapshot is version ' +
                    status.schemaVersion + ' while current version is ' + this.configurationService.getConfiguration().currentSchemaVersion + ')');
                return;
            }
            if (this.currentEnableSelection && this.itemTrackingEnabledAndPossible
                && status.checkedItemIdentifiers && status.checkedItemIdentifiers.length) {
                this.checkedItems = this.dataSnapshot.filter(function (data) {
                    var _a, _b;
                    var id = _this.getItemIdentifier(data);
                    return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.checkedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1;
                });
            }
            if (this.currentEnableRowExpansion && this.itemTrackingEnabledAndPossible
                && status.expandedItemIdentifiers && status.expandedItemIdentifiers.length) {
                this.expandedItems = this.dataSnapshot.filter(function (data) {
                    var _a, _b;
                    var id = _this.getItemIdentifier(data);
                    return id && ((_b = (_a = status) === null || _a === void 0 ? void 0 : _a.expandedItemIdentifiers) === null || _b === void 0 ? void 0 : _b.indexOf(id)) !== -1 && _this.isExpandable(data);
                });
            }
            this.statusSnapshot = this.buildStatusSnapshot();
            this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
        };
        MaisTableComponent.prototype.reload = function (context) {
            var _this = this;
            this.fetching = true;
            this.showFetching = !context.inBackground;
            var obs = new rxjs.Observable(function (subscriber) {
                try {
                    _this.reloadInObservable(subscriber, context);
                }
                catch (e) {
                    subscriber.error(e);
                    subscriber.complete();
                }
            });
            obs.subscribe(function (success) {
                _this.logger.trace('async reload success');
                _this.fetching = false;
                if (!context.inBackground) {
                    _this.showFetching = false;
                }
                _this.handleMatTableDataSnapshotChanged();
            }, function (failure) {
                _this.logger.trace('async reload failed', failure);
                _this.fetching = false;
                if (!context.inBackground) {
                    _this.showFetching = false;
                }
                _this.handleMatTableDataSnapshotChanged();
            });
            return obs;
        };
        MaisTableComponent.prototype.reloadInObservable = function (tracker, context) {
            var _this = this;
            var withSnapshot = context.withStatusSnapshot || null;
            var pageRequest = this.buildPageRequest();
            this.logger.debug('reloading table data', pageRequest);
            // clear checked items
            if (!context.inBackground) {
                this.checkedItems = [];
                this.expandedItems = [];
                this.statusChanged();
            }
            this.lastFetchedSearchQuery = pageRequest.query || null;
            if (this.dataProvider) {
                // call data provider
                this.logger.trace('reload has been called, fetching data from provided function');
                this.logger.trace('page request for data fetch is', pageRequest);
                this.dataProvider(pageRequest, context).subscribe(function (response) {
                    _this.logger.trace('fetching data completed successfully');
                    if (!_this.dragInProgress) {
                        if (_this.paginationMode === exports.MaisTablePaginationMethod.SERVER) {
                            _this.parseResponseWithServerPagination(response);
                        }
                        else {
                            _this.parseResponseWithClientPagination(response.content, pageRequest);
                        }
                        if (withSnapshot) {
                            _this.applyStatusSnapshotPostFetch(withSnapshot);
                        }
                    }
                    else {
                        _this.logger.warn('data fetch aborted because user is dragging things');
                    }
                    tracker.next();
                    tracker.complete();
                }, function (failure) {
                    _this.logger.error('error fetching data from provider function', failure);
                    if (!_this.dragInProgress) {
                        _this.dataSnapshot = [];
                    }
                    else {
                        _this.logger.warn('data fetch aborted because user is dragging things');
                    }
                    tracker.error(failure);
                    tracker.complete();
                });
            }
            else {
                // data is not provided on request and is in clientDataSnapshot
                this.logger.trace('reload has been called on locally fetched data');
                if (!this.dragInProgress) {
                    this.parseResponseWithClientPagination(this.clientDataSnapshot, pageRequest);
                    if (withSnapshot) {
                        this.applyStatusSnapshotPostFetch(withSnapshot);
                    }
                }
                else {
                    this.logger.warn('data fetch aborted because user is dragging things');
                }
                tracker.next();
                tracker.complete();
            }
        };
        MaisTableComponent.prototype.parseResponseWithServerPagination = function (response) {
            this.dataSnapshot = response.content;
            if (this.currentEnablePagination) {
                if (response.totalPages) {
                    this.fetchedPageCount = response.totalPages;
                }
                else {
                    throw new Error('data from server did not contain required totalPages field');
                }
                if (response.totalElements) {
                    this.fetchedResultNumber = response.totalElements;
                }
                else {
                    throw new Error('data from server did not contain required totalElements field');
                }
            }
        };
        MaisTableComponent.prototype.parseResponseWithClientPagination = function (data, request) {
            this.logger.trace('applying in-memory fetching, paginating, ordering and filtering');
            var inMemoryResponse = MaisTableInMemoryHelper.fetchInMemory(data, request, this.currentSortColumn, this.checkedColumnsForFiltering, this.getCurrentLocale());
            this.dataSnapshot = inMemoryResponse.content;
            this.fetchedResultNumber = typeof inMemoryResponse.totalElements === 'undefined' ? null : inMemoryResponse.totalElements;
            this.fetchedPageCount = typeof inMemoryResponse.totalPages === 'undefined' ? null : inMemoryResponse.totalPages;
        };
        MaisTableComponent.prototype.buildPageRequest = function () {
            var e_1, _a;
            var _b, _c;
            var output = {
                page: this.currentEnablePagination ? this.currentPageIndex : null,
                size: this.currentEnablePagination ? this.currentPageSize : null,
                sort: [],
                query: null,
                queryFields: [],
                filters: []
            };
            if (this.currentEnableFiltering && this.searchQueryActive) {
                output.query = this.currentSearchQuery;
                output.queryFields = this.checkedColumnsForFiltering.map(function (column) { return column.serverField || column.field || null; });
            }
            if (this.currentEnableContextFiltering && this.checkedContextFilters.length) {
                try {
                    for (var _d = __values(this.checkedContextFilters), _e = _d.next(); !_e.done; _e = _d.next()) {
                        var filter = _e.value;
                        (_b = output.filters) === null || _b === void 0 ? void 0 : _b.push(filter.name || null);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            var sortColumn = this.currentSortColumn;
            var sortDirection = this.currentSortDirection;
            if (sortColumn) {
                (_c = output.sort) === null || _c === void 0 ? void 0 : _c.push({
                    property: sortColumn.serverField || sortColumn.field || null,
                    direction: sortDirection || exports.MaisTableSortDirection.ASCENDING
                });
            }
            return output;
        };
        MaisTableComponent.prototype.setPage = function (index) {
            this.selectedPageIndex = index;
            this.statusChanged();
            this.emitPageChanged();
        };
        Object.defineProperty(MaisTableComponent.prototype, "currentRefreshOnPushInBackground", {
            get: function () {
                var _a;
                if (this.refreshOnPushInBackground === true || this.refreshOnPushInBackground === false) {
                    return this.refreshOnPushInBackground;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnPushInBackground;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentRefreshIntervalInBackground", {
            get: function () {
                var _a;
                if (this.refreshIntervalInBackground === true || this.refreshIntervalInBackground === false) {
                    return this.refreshIntervalInBackground;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultOnTickInBackground;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentRefreshStrategies", {
            get: function () {
                var _a;
                if (this.refreshStrategy) {
                    if (Array.isArray(this.refreshStrategy) && this.refreshStrategy.length) {
                        return this.refreshStrategy;
                    }
                    else {
                        return [this.refreshStrategy];
                    }
                }
                return [(_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultStrategy];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentRefreshInterval", {
            get: function () {
                var _a;
                return this.refreshInterval || ((_a = this.configurationService.getConfiguration().refresh) === null || _a === void 0 ? void 0 : _a.defaultInterval);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentTableId", {
            get: function () {
                return this.tableId || this.uuid;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableDrag", {
            get: function () {
                var _a;
                if (this.enableDrag === true || this.enableDrag === false) {
                    return this.enableDrag;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dragEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableDrop", {
            get: function () {
                var _a;
                if (this.enableDrop === true || this.enableDrop === false) {
                    return this.enableDrop;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().dragAndDrop) === null || _a === void 0 ? void 0 : _a.dropEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnablePagination", {
            get: function () {
                var _a;
                if (this.enablePagination === true || this.enablePagination === false) {
                    return this.enablePagination;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableItemTracking", {
            get: function () {
                var _a;
                if (this.enableItemTracking === true || this.enableItemTracking === false) {
                    return this.enableItemTracking;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().itemTracking) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableStorePersistence", {
            get: function () {
                var _a;
                if (this.enableStorePersistence === true || this.enableStorePersistence === false) {
                    return this.enableStorePersistence;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().storePersistence) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnablePageSizeSelect", {
            get: function () {
                var _a;
                if (this.enablePageSizeSelect === true || this.enablePageSizeSelect === false) {
                    return this.enablePageSizeSelect;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.pageSizeSelectionEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableMultipleRowExpansion", {
            get: function () {
                var _a;
                if (this.enableMultipleRowExpansion === true || this.enableMultipleRowExpansion === false) {
                    return this.enableMultipleRowExpansion;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.multipleExpansionEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableRowExpansion", {
            get: function () {
                var _a;
                if (this.enableRowExpansion === true || this.enableRowExpansion === false) {
                    return this.enableRowExpansion;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().rowExpansion) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableHeaderActions", {
            get: function () {
                var _a;
                if (this.enableHeaderActions === true || this.enableHeaderActions === false) {
                    return this.enableHeaderActions;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.headerActionsEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableContextActions", {
            get: function () {
                var _a;
                if (this.enableContextActions === true || this.enableContextActions === false) {
                    return this.enableContextActions;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.contextActionsEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableColumnsSelection", {
            get: function () {
                var _a;
                if (this.enableColumnsSelection === true || this.enableColumnsSelection === false) {
                    return this.enableColumnsSelection;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableFiltering", {
            get: function () {
                var _a;
                if (this.enableFiltering === true || this.enableFiltering === false) {
                    return this.enableFiltering;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableMultiSelect", {
            get: function () {
                var _a;
                if (this.enableMultiSelect === true || this.enableMultiSelect === false) {
                    return this.enableMultiSelect;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.multipleSelectionEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableSelectAll", {
            get: function () {
                var _a;
                if (this.enableSelectAll === true || this.enableSelectAll === false) {
                    return this.enableSelectAll;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.selectAllEnabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableContextFiltering", {
            get: function () {
                var _a;
                if (this.enableContextFiltering === true || this.enableContextFiltering === false) {
                    return this.enableContextFiltering;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().contextFiltering) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentEnableSelection", {
            get: function () {
                var _a;
                if (this.enableSelection === true || this.enableSelection === false) {
                    return this.enableSelection;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().rowSelection) === null || _a === void 0 ? void 0 : _a.enabledByDefault;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentPaginationMode", {
            get: function () {
                var _a;
                if (this.paginationMode) {
                    return this.paginationMode;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPaginationMode;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "itemTrackingEnabledAndPossible", {
            get: function () {
                return !!this.itemIdentifier && this.currentEnableItemTracking;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "storePersistenceEnabledAndPossible", {
            get: function () {
                return this.storeAdapter && this.currentEnableStorePersistence;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "rowExpansionEnabledAndPossible", {
            get: function () {
                return this.currentEnableRowExpansion;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "pageSizeSelectEnabledAndPossible", {
            get: function () {
                return this.currentPossiblePageSizes && this.currentPossiblePageSizes.length > 0 && this.currentEnablePageSizeSelect;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "contextFilteringEnabledAndPossible", {
            get: function () {
                return this.contextFilters && this.contextFilters.length > 0 && this.currentEnableContextFiltering;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "contextActionsEnabledAndPossible", {
            get: function () {
                return this.contextActions && this.contextActions.length > 0 && this.currentEnableContextActions;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "headerActionsEnabledAndPossible", {
            get: function () {
                return this.headerActions && this.headerActions.length > 0 && this.currentEnableHeaderActions;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "columnSelectionPossibleAndAllowed", {
            get: function () {
                return this.currentEnableColumnsSelection && this.columns.length > 0 && this.hideableColumns.length > 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "filteringPossibleAndAllowed", {
            get: function () {
                return this.currentEnableFiltering && this.filterableColumns.length > 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentPossiblePageSizes", {
            get: function () {
                var _a;
                if (this.possiblePageSize && this.possiblePageSize.length) {
                    return this.possiblePageSize;
                }
                else {
                    return (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPossiblePageSizes;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentSearchQuery", {
            get: function () {
                if (this.selectedSearchQuery) {
                    return this.selectedSearchQuery;
                }
                else {
                    return null;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentPageCount", {
            get: function () {
                return this.fetchedPageCount || 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentResultNumber", {
            get: function () {
                return this.fetchedResultNumber || 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentPageSize", {
            get: function () {
                var _a;
                var def = (_a = this.configurationService.getConfiguration().pagination) === null || _a === void 0 ? void 0 : _a.defaultPageSize;
                if (this.selectedPageSize) {
                    return this.selectedPageSize;
                }
                else if (this.defaultPageSize) {
                    return this.defaultPageSize;
                }
                else if (this.currentPossiblePageSizes.length &&
                    def &&
                    this.currentPossiblePageSizes.indexOf(def) !== -1) {
                    return def;
                }
                else if (this.currentPossiblePageSizes.length) {
                    return this.currentPossiblePageSizes[0];
                }
                else if (def) {
                    return def;
                }
                else {
                    return 10;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentPageIndex", {
            get: function () {
                if (this.selectedPageIndex) {
                    return this.selectedPageIndex;
                }
                else {
                    return 0;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentSortColumn", {
            get: function () {
                var _this = this;
                if (this.selectedSortColumn) {
                    // todo log if sorting column disappears
                    return this.visibleColumns.find(function (c) { var _a; return c.name === ((_a = _this.selectedSortColumn) === null || _a === void 0 ? void 0 : _a.name); }) || null;
                }
                else if (this.defaultSortingColumn) {
                    var foundColumn = this.visibleColumns.find(function (c) { return c.name === _this.defaultSortingColumn; });
                    if (foundColumn) {
                        return foundColumn;
                    }
                    else {
                        // find first sortable column?
                        return this.visibleColumns.find(function (column) {
                            return MaisTableFormatterHelper.isSortable(column, _this.configurationService.getConfiguration());
                        }) || null;
                    }
                }
                else {
                    // find first sortable column?
                    return this.visibleColumns.find(function (column) {
                        return MaisTableFormatterHelper.isSortable(column, _this.configurationService.getConfiguration());
                    }) || null;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentSortDirection", {
            get: function () {
                if (this.selectedSortDirection) {
                    return this.selectedSortDirection;
                }
                else if (this.defaultSortingDirection) {
                    return this.defaultSortingDirection === exports.MaisTableSortDirection.DESCENDING ?
                        exports.MaisTableSortDirection.DESCENDING : exports.MaisTableSortDirection.ASCENDING;
                }
                else {
                    // return DEFAULT
                    return exports.MaisTableSortDirection.ASCENDING;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentData", {
            get: function () {
                return this.dataSnapshot || [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "enumPages", {
            get: function () {
                var rangeStart = 1;
                var rangeEnd = 1;
                var rangeSelected = 1;
                var pages = [];
                var pageNum = this.currentPageCount;
                var currentIndex = this.currentPageIndex;
                var isSkipping = false;
                for (var i = 0; i < pageNum; i++) {
                    if (Math.abs(i - currentIndex) <= (rangeSelected) || i <= (rangeStart - 1) || i >= (pageNum - rangeEnd)) {
                        isSkipping = false;
                        pages.push(i);
                    }
                    else {
                        if (!isSkipping) {
                            pages.push({
                                skip: true
                            });
                            isSkipping = true;
                        }
                    }
                }
                return pages;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "activeColumnsCount", {
            get: function () {
                var output = this.visibleColumns.length;
                if (this.currentEnableSelection) {
                    output++;
                }
                if (this.contextFilteringEnabledAndPossible) {
                    output++;
                }
                if (this.rowExpansionEnabledAndPossible) {
                    output++;
                }
                return output;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "noResults", {
            get: function () {
                if (this.paginationMode === exports.MaisTablePaginationMethod.CLIENT) {
                    return !this.currentData.length;
                }
                else {
                    return !this.dataSnapshot.length;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "hideableColumns", {
            get: function () {
                var _this = this;
                return this.columns.filter(function (c) { return MaisTableFormatterHelper.isHideable(c, _this.configurationService.getConfiguration()); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "visibleColumns", {
            get: function () {
                var _this = this;
                return this.columns.filter(function (c) { return _this.checkedColumnsForVisualization.indexOf(c) !== -1; });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentContextFilters", {
            get: function () {
                return this.contextFilters || [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentHeaderActions", {
            get: function () {
                return this.headerActions || [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "currentActions", {
            get: function () {
                return this.contextActions || [];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "hasActions", {
            get: function () {
                return this.currentActions.length > 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "searchQueryNeedApply", {
            get: function () {
                return (this.currentSearchQuery || '') !== (this.lastFetchedSearchQuery || '');
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "searchQueryActive", {
            get: function () {
                if (!this.currentSearchQuery) {
                    return false;
                }
                if (!this.checkedColumnsForFiltering.length) {
                    return false;
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "searchQueryMalformed", {
            get: function () {
                if (!this.currentSearchQuery) {
                    return false;
                }
                if (!this.checkedColumnsForFiltering.length) {
                    return true;
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "filterableColumns", {
            get: function () {
                var _this = this;
                return this.columns.filter(function (c) { return _this.isFilterable(c); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyButtonActionsAllowed", {
            get: function () {
                var _this = this;
                return !!this.contextActions.find(function (a) { return _this.isContextActionAllowed(a); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyHeaderActionsAllowed", {
            get: function () {
                var _this = this;
                return !!this.headerActions.find(function (a) { return _this.isHeaderActionAllowed(a); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "configColumnVisibilityShowFixedColumns", {
            get: function () {
                var _a;
                return (_a = this.configurationService.getConfiguration().columnToggling) === null || _a === void 0 ? void 0 : _a.showFixedColumns;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "configFilteringShowUnfilterableColumns", {
            get: function () {
                var _a;
                return (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.showUnfilterableColumns;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "acceptDrop", {
            get: function () {
                return this.currentEnableDrop;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "acceptDrag", {
            get: function () {
                return this.currentEnableDrag;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "referencedTable", {
            get: function () {
                var v = this.getReferencedTable();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.getReferencedTable = function () {
            var e_2, _a, e_3, _b;
            if (this.dropConnectedTo && this.dropConnectedTo.length) {
                var connectedToList = (Array.isArray(this.dropConnectedTo)) ? this.dropConnectedTo : [this.dropConnectedTo];
                var found = [];
                try {
                    for (var connectedToList_1 = __values(connectedToList), connectedToList_1_1 = connectedToList_1.next(); !connectedToList_1_1.done; connectedToList_1_1 = connectedToList_1.next()) {
                        var connectedToToken = connectedToList_1_1.value;
                        var registeredList = this.registry.get(connectedToToken);
                        if (registeredList && registeredList.length) {
                            try {
                                for (var registeredList_1 = (e_3 = void 0, __values(registeredList)), registeredList_1_1 = registeredList_1.next(); !registeredList_1_1.done; registeredList_1_1 = registeredList_1.next()) {
                                    var regComp = registeredList_1_1.value;
                                    found.push(regComp);
                                }
                            }
                            catch (e_3_1) { e_3 = { error: e_3_1 }; }
                            finally {
                                try {
                                    if (registeredList_1_1 && !registeredList_1_1.done && (_b = registeredList_1.return)) _b.call(registeredList_1);
                                }
                                finally { if (e_3) throw e_3.error; }
                            }
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (connectedToList_1_1 && !connectedToList_1_1.done && (_a = connectedToList_1.return)) _a.call(connectedToList_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
                if (found.length < 1) {
                    return null;
                }
                else if (found.length > 1) {
                    this.logger.error('MULTIPLE TABLE REFERENCED STILL ACTIVE', found);
                    return null;
                }
                else {
                    return found[0].component;
                }
            }
            else {
                return this;
            }
        };
        // user actions
        MaisTableComponent.prototype.handleItemDragged = function (event, from, to) {
            this.logger.debug('item dragged from table ' + this.currentTableId);
            this.itemDragged.emit({
                item: event.item.data,
                event: event,
                fromDataSnapshot: from.getDataSnapshot(),
                toDataSnapshot: to.getDataSnapshot(),
                fromComponent: from,
                toComponent: to
            });
        };
        MaisTableComponent.prototype.handleItemDropped = function (event) {
            var _this = this;
            this.logger.debug('DROP EVENT FROM TABLE ' + this.currentTableId, event);
            var droppedSource = this.registry.getSingle(event.previousContainer.id);
            var droppedTarget = this.registry.getSingle(event.container.id);
            if (!droppedTarget) {
                this.logger.warn('NO DROP TARGET FOUND');
                return;
            }
            if (!droppedTarget.acceptDrop) {
                this.logger.debug('skipping drop on container with acceptDrop = false');
                return;
            }
            if (droppedSource) {
                droppedSource.handleItemDragged(event, droppedSource, droppedTarget);
            }
            this.logger.debug('item dropped on table ' + droppedTarget.currentTableId);
            this.itemDropped.emit({
                item: event.item.data,
                event: event,
                fromDataSnapshot: droppedSource ? droppedSource.getDataSnapshot() : null,
                toDataSnapshot: droppedTarget.getDataSnapshot(),
                fromComponent: droppedSource,
                toComponent: droppedTarget
            });
            // I'm sorry
            this.cdr.detectChanges();
            this.ngZone.run(function () {
                _this.dataSnapshot = _this.dataSnapshot.map(function (o) { return o; });
                _this.cdr.detectChanges();
                setTimeout(function () { return _this.forceReRender = true; });
                setTimeout(function () { return _this.forceReRender = false; });
                _this.handleMatTableDataSnapshotChanged();
            });
        };
        MaisTableComponent.prototype.applySelectedFilter = function () {
            this.setPage(0);
            this.statusChanged();
            this.reload({ reason: exports.MaisTableReloadReason.USER });
        };
        MaisTableComponent.prototype.clickOnRowExpansion = function (item) {
            if (!this.rowExpansionEnabledAndPossible) {
                return;
            }
            if (!this.isExpandable(item)) {
                return;
            }
            if (this.isExpanded(item)) {
                this.expandedItems.splice(this.expandedItems.indexOf(item), 1);
            }
            else {
                if (!this.currentEnableMultipleRowExpansion) {
                    this.expandedItems = [];
                }
                this.expandedItems.push(item);
            }
            this.handleMatTableDataSnapshotChanged();
            this.statusChanged();
        };
        MaisTableComponent.prototype.clickOnContextAction = function (action) {
            if (!this.currentEnableContextActions) {
                this.logger.warn('button actions are disabled');
                return;
            }
            if (!this.isContextActionAllowed(action)) {
                return;
            }
            var dispatchContext = {
                action: action,
                selectedItems: this.checkedItems
            };
            this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
            this.action.emit(dispatchContext);
        };
        MaisTableComponent.prototype.clickOnPageSize = function (pageSize) {
            this.selectedPageSize = pageSize;
            this.setPage(0);
            this.statusChanged();
            this.reload({ reason: exports.MaisTableReloadReason.USER });
        };
        MaisTableComponent.prototype.clickOnHeaderAction = function (action) {
            if (!this.currentEnableHeaderActions) {
                this.logger.warn('button actions are disabled');
                return;
            }
            if (!this.isHeaderActionAllowed(action)) {
                return;
            }
            var dispatchContext = {
                action: action,
                selectedItems: this.checkedItems
            };
            this.logger.debug('dispatching action ' + action.name + ' with payload', dispatchContext);
            this.action.emit(dispatchContext);
        };
        MaisTableComponent.prototype.searchQueryFocusOut = function () {
            var _this = this;
            var _a;
            var comp = this;
            setTimeout(function () {
                var _a, _b;
                if ((comp.selectedSearchQuery || '') !== (comp.lastFetchedSearchQuery || '')) {
                    if (comp.selectedSearchQuery) {
                        if ((_a = _this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadOnQueryChange) {
                            comp.applySelectedFilter();
                        }
                        else {
                            // comp.selectedSearchQuery = comp.lastFetchedSearchQuery;
                        }
                    }
                    else {
                        if ((_b = _this.configurationService.getConfiguration().filtering) === null || _b === void 0 ? void 0 : _b.autoReloadOnQueryClear) {
                            comp.applySelectedFilter();
                        }
                    }
                }
            }, (_a = this.configurationService.getConfiguration().filtering) === null || _a === void 0 ? void 0 : _a.autoReloadTimeout);
        };
        MaisTableComponent.prototype.switchToPage = function (pageIndex, context) {
            if (this.currentPageIndex === pageIndex) {
                return;
            }
            if (!context) {
                context = { reason: exports.MaisTableReloadReason.USER };
            }
            this.setPage(pageIndex);
            this.statusChanged();
            this.reload(context);
        };
        MaisTableComponent.prototype.clickOnColumn = function (column) {
            if (!MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration())) {
                return;
            }
            var sortColumn = this.currentSortColumn;
            var sortDirection = this.currentSortDirection;
            if (sortColumn && sortColumn.name === column.name) {
                this.selectedSortDirection = (sortDirection === exports.MaisTableSortDirection.DESCENDING ?
                    exports.MaisTableSortDirection.ASCENDING : exports.MaisTableSortDirection.DESCENDING);
            }
            else {
                this.selectedSortColumn = column;
                this.selectedSortDirection = exports.MaisTableSortDirection.ASCENDING;
            }
            this.emitSortChanged();
            // forza ritorno alla prima pagina
            this.setPage(0);
            this.statusChanged();
            this.reload({ reason: exports.MaisTableReloadReason.USER });
        };
        // parsing functions
        MaisTableComponent.prototype.getItemIdentifier = function (item) {
            if (this.itemTrackingEnabledAndPossible) {
                if (this.itemIdentifier.extract) {
                    return this.itemIdentifier.extract(item);
                }
                else {
                    return MaisTableFormatterHelper.getPropertyValue(item, this.itemIdentifier);
                }
            }
            else {
                return null;
            }
        };
        MaisTableComponent.prototype.getCurrentLocale = function () {
            return this.translateService.getDefaultLang();
        };
        MaisTableComponent.prototype.extractValue = function (row, column) {
            return MaisTableFormatterHelper.extractValue(row, column, this.getCurrentLocale());
        };
        MaisTableComponent.prototype.resolveLabel = function (column) {
            return this.resolveLabelOrFixed(column.labelKey, column.label);
        };
        MaisTableComponent.prototype.resolveLabelOrFixed = function (key, fixed) {
            if (key) {
                return this.translateService.instant(key);
            }
            else if (fixed) {
                return fixed;
            }
            else {
                return null;
            }
        };
        MaisTableComponent.prototype.isCurrentSortingColumn = function (column, direction) {
            if (direction === void 0) { direction = null; }
            var sortColumn = this.currentSortColumn;
            if (sortColumn && column && column.name === sortColumn.name) {
                if (!direction) {
                    return true;
                }
                return direction === this.currentSortDirection;
            }
            else {
                return false;
            }
        };
        MaisTableComponent.prototype.isDefaultVisibleColumn = function (column) {
            return MaisTableFormatterHelper.isDefaultVisibleColumn(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isDefaultFilterColumn = function (column) {
            return MaisTableFormatterHelper.isDefaultFilterColumn(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isExpandable = function (row) {
            return this.currentEnableRowExpansion &&
                (!this.expandableStatusProvider || this.expandableStatusProvider(row, this.statusSnapshot));
        };
        MaisTableComponent.prototype.isHideable = function (column) {
            return MaisTableFormatterHelper.isHideable(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isSortable = function (column) {
            return MaisTableFormatterHelper.isSortable(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isFilterable = function (column) {
            return MaisTableFormatterHelper.isFilterable(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.hasLabel = function (column) {
            return MaisTableFormatterHelper.hasLabel(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isLabelVisibleInTable = function (column) {
            return MaisTableFormatterHelper.isLabelVisibleInTable(column, this.configurationService.getConfiguration());
        };
        MaisTableComponent.prototype.isContextActionAllowed = function (action) {
            var _a;
            return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultContextActionActivationCondition);
        };
        MaisTableComponent.prototype.isHeaderActionAllowed = function (action) {
            var _a;
            return this.isActionAllowed(action, (_a = this.configurationService.getConfiguration().actions) === null || _a === void 0 ? void 0 : _a.defaultHeaderActionActivationCondition);
        };
        MaisTableComponent.prototype.isActionAllowed = function (action, def) {
            var actCond = action.activationCondition || def;
            if (actCond === exports.MaisTableActionActivationCondition.ALWAYS) {
                return true;
            }
            else if (actCond === exports.MaisTableActionActivationCondition.NEVER) {
                return false;
            }
            else if (actCond === exports.MaisTableActionActivationCondition.DYNAMIC) {
                // delega decisione a provider esterno
                if (!this.actionStatusProvider) {
                    this.logger.error('Action with dynamic enabling but no actionStatusProvider provided');
                    return false;
                }
                return this.actionStatusProvider(action, this.statusSnapshot || null);
            }
            var numSelected = this.checkedItems.length;
            if (actCond === exports.MaisTableActionActivationCondition.SINGLE_SELECTION) {
                return numSelected === 1;
            }
            else if (actCond === exports.MaisTableActionActivationCondition.MULTIPLE_SELECTION) {
                return numSelected > 0;
            }
            else if (actCond === exports.MaisTableActionActivationCondition.NO_SELECTION) {
                return numSelected < 1;
            }
            else {
                throw new Error('Unknown action activation condition: ' + actCond);
            }
        };
        // gestione delle righe espanse
        MaisTableComponent.prototype.isExpanded = function (item) {
            return this.expandedItems.indexOf(item) !== -1;
        };
        // checkbox select per items
        MaisTableComponent.prototype.isChecked = function (item) {
            return this.checkedItems.indexOf(item) !== -1;
        };
        Object.defineProperty(MaisTableComponent.prototype, "allChecked", {
            get: function () {
                var e_4, _a;
                var dataItems = this.currentData;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_1 = __values(dataItems), dataItems_1_1 = dataItems_1.next(); !dataItems_1_1.done; dataItems_1_1 = dataItems_1.next()) {
                        var item = dataItems_1_1.value;
                        if (this.checkedItems.indexOf(item) === -1) {
                            return false;
                        }
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (dataItems_1_1 && !dataItems_1_1.done && (_a = dataItems_1.return)) _a.call(dataItems_1);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyChecked", {
            get: function () {
                var e_5, _a;
                var dataItems = this.currentData;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_2 = __values(dataItems), dataItems_2_1 = dataItems_2.next(); !dataItems_2_1.done; dataItems_2_1 = dataItems_2.next()) {
                        var item = dataItems_2_1.value;
                        if (this.checkedItems.indexOf(item) !== -1) {
                            return true;
                        }
                    }
                }
                catch (e_5_1) { e_5 = { error: e_5_1 }; }
                finally {
                    try {
                        if (dataItems_2_1 && !dataItems_2_1.done && (_a = dataItems_2.return)) _a.call(dataItems_2);
                    }
                    finally { if (e_5) throw e_5.error; }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "noneChecked", {
            get: function () {
                return !this.anyChecked;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.toggleChecked = function (item) {
            if (this.isChecked(item)) {
                this.checkedItems.splice(this.checkedItems.indexOf(item), 1);
            }
            else {
                if (!this.currentEnableMultiSelect) {
                    this.checkedItems = [];
                }
                this.checkedItems.push(item);
            }
            this.statusChanged();
            this.emitSelectionChanged();
        };
        MaisTableComponent.prototype.toggleAllChecked = function () {
            var e_6, _a;
            if (!this.currentEnableSelectAll) {
                return;
            }
            if (!this.currentEnableMultiSelect) {
                return;
            }
            if (this.allChecked) {
                this.checkedItems = [];
            }
            else {
                this.checkedItems = [];
                try {
                    for (var _b = __values(this.currentData), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var el = _c.value;
                        this.checkedItems.push(el);
                    }
                }
                catch (e_6_1) { e_6 = { error: e_6_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_6) throw e_6.error; }
                }
            }
            this.statusChanged();
            this.emitSelectionChanged();
        };
        // checkbox select per filtering columns
        MaisTableComponent.prototype.isColumnCheckedForFiltering = function (item) {
            return this.checkedColumnsForFiltering.indexOf(item) !== -1;
        };
        Object.defineProperty(MaisTableComponent.prototype, "allFilteringColumnsChecked", {
            get: function () {
                var e_7, _a;
                var dataItems = this.filterableColumns;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_3 = __values(dataItems), dataItems_3_1 = dataItems_3.next(); !dataItems_3_1.done; dataItems_3_1 = dataItems_3.next()) {
                        var item = dataItems_3_1.value;
                        if (this.checkedColumnsForFiltering.indexOf(item) === -1) {
                            return false;
                        }
                    }
                }
                catch (e_7_1) { e_7 = { error: e_7_1 }; }
                finally {
                    try {
                        if (dataItems_3_1 && !dataItems_3_1.done && (_a = dataItems_3.return)) _a.call(dataItems_3);
                    }
                    finally { if (e_7) throw e_7.error; }
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyFilteringColumnsChecked", {
            get: function () {
                var e_8, _a;
                var dataItems = this.filterableColumns;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_4 = __values(dataItems), dataItems_4_1 = dataItems_4.next(); !dataItems_4_1.done; dataItems_4_1 = dataItems_4.next()) {
                        var item = dataItems_4_1.value;
                        if (this.checkedColumnsForFiltering.indexOf(item) !== -1) {
                            return true;
                        }
                    }
                }
                catch (e_8_1) { e_8 = { error: e_8_1 }; }
                finally {
                    try {
                        if (dataItems_4_1 && !dataItems_4_1.done && (_a = dataItems_4.return)) _a.call(dataItems_4);
                    }
                    finally { if (e_8) throw e_8.error; }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "noFilteringColumnshecked", {
            get: function () {
                return !this.anyFilteringColumnsChecked;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.toggleFilteringColumnChecked = function (item) {
            if (!this.currentEnableFiltering) {
                return;
            }
            if (!this.isFilterable(item)) {
                return;
            }
            if (this.isColumnCheckedForFiltering(item)) {
                this.checkedColumnsForFiltering.splice(this.checkedColumnsForFiltering.indexOf(item), 1);
            }
            else {
                this.checkedColumnsForFiltering.push(item);
            }
            if (this.selectedSearchQuery) {
                this.logger.trace('search query columns changed with active search query, reloading');
                this.applySelectedFilter();
            }
            this.statusChanged();
            this.emitFilteringColumnsSelectionChanged();
        };
        MaisTableComponent.prototype.toggleAllFilteringColumnsChecked = function () {
            var e_9, _a;
            if (!this.currentEnableFiltering) {
                return;
            }
            if (this.allFilteringColumnsChecked) {
                this.checkedColumnsForFiltering = [];
            }
            else {
                this.checkedColumnsForFiltering = [];
                try {
                    for (var _b = __values(this.filterableColumns), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var el = _c.value;
                        this.checkedColumnsForFiltering.push(el);
                    }
                }
                catch (e_9_1) { e_9 = { error: e_9_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_9) throw e_9.error; }
                }
            }
            if (this.selectedSearchQuery) {
                this.logger.trace('search query columns changed with active search query, reloading');
                this.applySelectedFilter();
            }
            this.statusChanged();
            this.emitFilteringColumnsSelectionChanged();
        };
        // checkbox select per visible columns
        MaisTableComponent.prototype.isColumnCheckedForVisualization = function (item) {
            return this.checkedColumnsForVisualization.indexOf(item) !== -1;
        };
        Object.defineProperty(MaisTableComponent.prototype, "allVisibleColumnsChecked", {
            get: function () {
                var e_10, _a;
                var dataItems = this.hideableColumns;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_5 = __values(dataItems), dataItems_5_1 = dataItems_5.next(); !dataItems_5_1.done; dataItems_5_1 = dataItems_5.next()) {
                        var item = dataItems_5_1.value;
                        if (this.checkedColumnsForVisualization.indexOf(item) === -1) {
                            return false;
                        }
                    }
                }
                catch (e_10_1) { e_10 = { error: e_10_1 }; }
                finally {
                    try {
                        if (dataItems_5_1 && !dataItems_5_1.done && (_a = dataItems_5.return)) _a.call(dataItems_5);
                    }
                    finally { if (e_10) throw e_10.error; }
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyVisibleColumnsChecked", {
            get: function () {
                var e_11, _a;
                var dataItems = this.hideableColumns;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_6 = __values(dataItems), dataItems_6_1 = dataItems_6.next(); !dataItems_6_1.done; dataItems_6_1 = dataItems_6.next()) {
                        var item = dataItems_6_1.value;
                        if (this.checkedColumnsForVisualization.indexOf(item) !== -1) {
                            return true;
                        }
                    }
                }
                catch (e_11_1) { e_11 = { error: e_11_1 }; }
                finally {
                    try {
                        if (dataItems_6_1 && !dataItems_6_1.done && (_a = dataItems_6.return)) _a.call(dataItems_6);
                    }
                    finally { if (e_11) throw e_11.error; }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "noVisibleColumnshecked", {
            get: function () {
                return !this.anyVisibleColumnsChecked;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.toggleVisibleColumnChecked = function (item) {
            if (!MaisTableFormatterHelper.isHideable(item, this.configurationService.getConfiguration()) || !this.currentEnableColumnsSelection) {
                return;
            }
            if (this.isColumnCheckedForVisualization(item)) {
                this.checkedColumnsForVisualization.splice(this.checkedColumnsForVisualization.indexOf(item), 1);
            }
            else {
                this.checkedColumnsForVisualization.push(item);
            }
            this.statusChanged();
            this.emitVisibleColumnsSelectionChanged();
        };
        MaisTableComponent.prototype.toggleAllVisibleColumnsChecked = function () {
            var e_12, _a;
            var _this = this;
            if (!this.currentEnableColumnsSelection) {
                return;
            }
            if (this.allVisibleColumnsChecked) {
                this.checkedColumnsForVisualization = this.columns.filter(function (c) {
                    return !MaisTableFormatterHelper.isHideable(c, _this.configurationService.getConfiguration());
                });
            }
            else {
                this.checkedColumnsForVisualization = [];
                try {
                    for (var _b = __values(this.columns), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var el = _c.value;
                        this.checkedColumnsForVisualization.push(el);
                    }
                }
                catch (e_12_1) { e_12 = { error: e_12_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_12) throw e_12.error; }
                }
            }
            this.statusChanged();
            this.emitVisibleColumnsSelectionChanged();
        };
        // checkbox select per filtri custom
        MaisTableComponent.prototype.isContextFilterChecked = function (item) {
            return this.checkedContextFilters.indexOf(item) !== -1;
        };
        Object.defineProperty(MaisTableComponent.prototype, "allContextFilterChecked", {
            get: function () {
                var e_13, _a;
                var dataItems = this.currentContextFilters;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_7 = __values(dataItems), dataItems_7_1 = dataItems_7.next(); !dataItems_7_1.done; dataItems_7_1 = dataItems_7.next()) {
                        var item = dataItems_7_1.value;
                        if (this.checkedContextFilters.indexOf(item) === -1) {
                            return false;
                        }
                    }
                }
                catch (e_13_1) { e_13 = { error: e_13_1 }; }
                finally {
                    try {
                        if (dataItems_7_1 && !dataItems_7_1.done && (_a = dataItems_7.return)) _a.call(dataItems_7);
                    }
                    finally { if (e_13) throw e_13.error; }
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "anyContextFilterChecked", {
            get: function () {
                var e_14, _a;
                var dataItems = this.currentContextFilters;
                if (!dataItems.length) {
                    return false;
                }
                try {
                    for (var dataItems_8 = __values(dataItems), dataItems_8_1 = dataItems_8.next(); !dataItems_8_1.done; dataItems_8_1 = dataItems_8.next()) {
                        var item = dataItems_8_1.value;
                        if (this.checkedContextFilters.indexOf(item) !== -1) {
                            return true;
                        }
                    }
                }
                catch (e_14_1) { e_14 = { error: e_14_1 }; }
                finally {
                    try {
                        if (dataItems_8_1 && !dataItems_8_1.done && (_a = dataItems_8.return)) _a.call(dataItems_8);
                    }
                    finally { if (e_14) throw e_14.error; }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "noContextFiltersChecked", {
            get: function () {
                return !this.anyContextFilterChecked;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.toggleContextFilterChecked = function (item) {
            if (!this.currentEnableContextFiltering) {
                return;
            }
            if (this.isContextFilterChecked(item)) {
                this.checkedContextFilters.splice(this.checkedContextFilters.indexOf(item), 1);
            }
            else {
                this.checkedContextFilters.push(item);
                if (item.group) {
                    this.checkedContextFilters = this.checkedContextFilters.filter(function (o) {
                        return (o.name === item.name) || (!o.group) || (o.group !== item.group);
                    });
                }
            }
            this.emitContextFiltersSelectionChanged();
            this.setPage(0);
            this.statusChanged();
            this.reload({ reason: exports.MaisTableReloadReason.USER });
        };
        MaisTableComponent.prototype.buildStatusSnapshot = function () {
            return {
                schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
                orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
                orderColumnDirection: this.currentSortDirection || null,
                query: this.currentSearchQuery,
                queryColumns: this.checkedColumnsForFiltering.map(function (c) { return c.name; }),
                visibleColumns: this.visibleColumns.map(function (c) { return c.name; }),
                contextFilters: this.checkedContextFilters.map(function (c) { return c.name; }),
                currentPage: this.currentPageIndex,
                pageSize: this.currentPageSize,
                checkedItems: !this.currentEnableSelection ? [] : this.checkedItems.map(function (v) { return v; }),
                expandedItems: !this.currentEnableRowExpansion ? [] : this.expandedItems.map(function (v) { return v; })
            };
        };
        MaisTableComponent.prototype.buildPersistableStatusSnapshot = function () {
            var _this = this;
            return {
                schemaVersion: this.configurationService.getConfiguration().currentSchemaVersion || 'NONE',
                orderColumn: this.currentSortColumn ? this.currentSortColumn.name : null,
                orderColumnDirection: this.currentSortDirection || null,
                query: this.currentSearchQuery,
                queryColumns: this.checkedColumnsForFiltering.map(function (c) { return c.name; }),
                visibleColumns: this.visibleColumns.map(function (c) { return c.name; }),
                contextFilters: this.checkedContextFilters.map(function (c) { return c.name; }),
                currentPage: this.currentPageIndex,
                pageSize: this.currentPageSize,
                checkedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                    this.checkedItems.map(function (item) { return _this.getItemIdentifier(item); }).filter(function (v) { return !!v; }),
                expandedItemIdentifiers: !this.itemTrackingEnabledAndPossible ? [] :
                    this.expandedItems.map(function (item) { return _this.getItemIdentifier(item); }).filter(function (v) { return !!v; })
            };
        };
        MaisTableComponent.prototype.statusChanged = function () {
            this.statusSnapshot = this.buildStatusSnapshot();
            this.persistableStatusSnapshot = this.buildPersistableStatusSnapshot();
            this.logger.trace('table status changed', this.persistableStatusSnapshot);
            this.emitStatusChanged();
        };
        MaisTableComponent.prototype.persistStatusToStore = function () {
            var _this = this;
            if (!this.statusSnapshot) {
                return rxjs.throwError('No status to save');
            }
            return new rxjs.Observable(function (subscriber) {
                if (_this.storePersistenceEnabledAndPossible) {
                    _this.logger.trace('passing status to persistence store');
                    if (_this.storeAdapter.save) {
                        _this.storeAdapter.save({
                            status: _this.persistableStatusSnapshot
                        }).subscribe(function (result) {
                            _this.logger.trace('saved status snapshot to persistence store');
                            subscriber.next();
                            subscriber.complete();
                        }, function (failure) {
                            _this.logger.warn('failed to save status snapshot to persistence store', failure);
                            subscriber.error(failure);
                            subscriber.complete();
                        });
                    }
                    else {
                        subscriber.error('No save function in store adapter');
                        subscriber.complete();
                    }
                }
                subscriber.next();
                subscriber.complete();
            });
        };
        MaisTableComponent.prototype.loadStatusFromStore = function () {
            var _this = this;
            return new rxjs.Observable(function (subscriber) {
                if (_this.storePersistenceEnabledAndPossible) {
                    _this.logger.trace('fetching status from persistence store');
                    if (_this.storeAdapter.load) {
                        _this.storeAdapter.load().subscribe(function (result) {
                            _this.logger.trace('fetched status snapshot from persistence store');
                            subscriber.next(result);
                            subscriber.complete();
                        }, function (failure) {
                            subscriber.error(failure);
                            _this.logger.warn('failed to fetch status snapshot from persistence store', failure);
                            subscriber.complete();
                        });
                    }
                    else {
                        subscriber.error('No load function in store adapter');
                        subscriber.complete();
                    }
                }
                subscriber.next(null);
                subscriber.complete();
            });
        };
        Object.defineProperty(MaisTableComponent.prototype, "dragInProgress", {
            get: function () {
                return ($('.cdk-drag-preview:visible').length +
                    $('.cdk-drag-placeholder:visible').length +
                    $('.cdk-drop-list-dragging:visible').length +
                    $('.cdk-drop-list-receiving:visible').length) > 0;
            },
            enumerable: true,
            configurable: true
        });
        // event emitters
        MaisTableComponent.prototype.emitSelectionChanged = function () {
            this.selectionChange.emit(this.checkedItems);
        };
        MaisTableComponent.prototype.emitSortChanged = function () {
            this.sortChange.emit({
                column: this.currentSortColumn,
                direction: this.currentSortDirection
            });
        };
        MaisTableComponent.prototype.emitPageChanged = function () {
            this.pageChange.emit(this.selectedPageIndex);
        };
        MaisTableComponent.prototype.emitFilteringColumnsSelectionChanged = function () {
            this.filteringColumnsChange.emit(this.checkedColumnsForFiltering);
        };
        MaisTableComponent.prototype.emitVisibleColumnsSelectionChanged = function () {
            this.visibleColumnsChange.emit(this.checkedColumnsForVisualization);
        };
        MaisTableComponent.prototype.emitContextFiltersSelectionChanged = function () {
            this.contextFiltersChange.emit(this.checkedContextFilters);
        };
        MaisTableComponent.prototype.emitStatusChanged = function () {
            if (this.statusSnapshot) {
                this.statusChange.emit(this.statusSnapshot);
            }
        };
        Object.defineProperty(MaisTableComponent.prototype, "matTableData", {
            // MatTable adapters
            get: function () {
                return this.dataSnapshot;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "matTableVisibleColumnDefs", {
            get: function () {
                var o = [];
                if (this.rowExpansionEnabledAndPossible) {
                    o.push('internal___col_row_expansion');
                }
                if (this.currentEnableSelection) {
                    o.push('internal___col_row_selection');
                }
                if (this.contextFilteringEnabledAndPossible) {
                    o.push('internal___col_context_filtering');
                }
                this.visibleColumns.map(function (c) { return c.name; }).forEach(function (c) { return o.push(c); });
                return o;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "matTableVisibleColumns", {
            get: function () {
                return this.visibleColumns;
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.prototype.handleMatTableDataSnapshotChanged = function () {
            var _this = this;
            var rows = [];
            this.dataSnapshot.forEach(function (element) {
                var detailRow = __assign({ ___detailRowContent: true, ___parentRow: element }, element);
                element.___detailRow = detailRow;
                rows.push(element, detailRow);
            });
            // TODO : BUILD STATIC ROW WITH DATA EXTRACTORS!
            this.logger.trace('emitting static rows for mat-table ' + this.currentTableId, rows);
            setTimeout(function () {
                var _a;
                (_a = _this.matTableDataObservable) === null || _a === void 0 ? void 0 : _a.next(rows);
            });
        };
        Object.defineProperty(MaisTableComponent.prototype, "compatibilityModeForMainTable", {
            get: function () {
                if (!this.isIE) {
                    return false;
                }
                else if (this.currentEnableDrop) {
                    return true;
                }
                else {
                    return true;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaisTableComponent.prototype, "compatibilityModeForDropDowns", {
            get: function () {
                return true;
                if (!this.isIE) {
                    return false;
                }
                else {
                    return true;
                }
            },
            enumerable: true,
            configurable: true
        });
        MaisTableComponent.counter = 0;
        /** @nocollapse */ MaisTableComponent.ɵfac = function MaisTableComponent_Factory(t) { return new (t || MaisTableComponent)(core["ɵɵdirectiveInject"](core$1.TranslateService), core["ɵɵdirectiveInject"](MaisTableService), core["ɵɵdirectiveInject"](MaisTableRegistryService), core["ɵɵdirectiveInject"](core.ChangeDetectorRef), core["ɵɵdirectiveInject"](core.NgZone)); };
        /** @nocollapse */ MaisTableComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: MaisTableComponent, selectors: [["mais-table"]], contentQueries: function MaisTableComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
                core["ɵɵcontentQuery"](dirIndex, _c0, true);
                core["ɵɵcontentQuery"](dirIndex, _c1, true);
                core["ɵɵcontentQuery"](dirIndex, _c2, true);
                core["ɵɵcontentQuery"](dirIndex, _c3, true);
                core["ɵɵcontentQuery"](dirIndex, _c4, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.cellTemplate = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.actionsCaptionTemplate = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.rowDetailTemplate = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.dragTemplate = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.dropTemplate = _t.first);
            } }, viewQuery: function MaisTableComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](_c5, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.table = _t.first);
            } }, inputs: { data: "data", dataProvider: "dataProvider", columns: "columns", defaultSortingColumn: "defaultSortingColumn", defaultSortingDirection: "defaultSortingDirection", defaultPageSize: "defaultPageSize", possiblePageSize: "possiblePageSize", tableId: "tableId", dropConnectedTo: "dropConnectedTo", paginationMode: "paginationMode", enablePagination: "enablePagination", enableSelection: "enableSelection", enableContextFiltering: "enableContextFiltering", enableSelectAll: "enableSelectAll", enableMultiSelect: "enableMultiSelect", enableFiltering: "enableFiltering", enableColumnsSelection: "enableColumnsSelection", enableContextActions: "enableContextActions", enableHeaderActions: "enableHeaderActions", enableRowExpansion: "enableRowExpansion", enablePageSizeSelect: "enablePageSizeSelect", enableMultipleRowExpansion: "enableMultipleRowExpansion", enableItemTracking: "enableItemTracking", enableStorePersistence: "enableStorePersistence", enableDrag: "enableDrag", enableDrop: "enableDrop", contextActions: "contextActions", headerActions: "headerActions", contextFilters: "contextFilters", contextFilteringPrompt: "contextFilteringPrompt", actionStatusProvider: "actionStatusProvider", expandableStatusProvider: "expandableStatusProvider", storeAdapter: "storeAdapter", itemIdentifier: "itemIdentifier", refreshStrategy: "refreshStrategy", refreshInterval: "refreshInterval", refreshEmitter: "refreshEmitter", refreshIntervalInBackground: "refreshIntervalInBackground", refreshOnPushInBackground: "refreshOnPushInBackground" }, outputs: { pageChange: "pageChange", sortChange: "sortChange", selectionChange: "selectionChange", filteringColumnsChange: "filteringColumnsChange", visibleColumnsChange: "visibleColumnsChange", contextFiltersChange: "contextFiltersChange", statusChange: "statusChange", itemDragged: "itemDragged", itemDropped: "itemDropped", action: "action" }, features: [core["ɵɵNgOnChangesFeature"]()], decls: 5, vars: 4, consts: [[1, "mais-table"], ["class", "container-full px-4 py-5 table-actions table-actions--header", 4, "ngIf"], [4, "ngIf"], ["class", "container-full px-4 py-4 table-actions table-actions--footer", 4, "ngIf"], [1, "container-full", "px-4", "py-5", "table-actions", "table-actions--header"], [1, "row"], ["class", "col-4", 4, "ngIf"], ["class", "col-3", 4, "ngIf"], [1, "col-5", "text-right"], [3, "ngTemplateOutlet", "ngTemplateOutletContext"], ["class", "btn-group", "ngbDropdown", "", 4, "ngIf"], [1, "col-4"], [3, "submit"], [1, "input-group"], ["type", "text", "name", "selectedSearchQuery", "aria-label", "Search", 1, "form-control", 3, "placeholder", "ngModel", "ngModelChange", "focusout"], ["ngbDropdown", "", 1, "input-group-append", 3, "autoClose"], ["type", "submit", 1, "btn", 3, "click"], [1, "fas", "fa-search"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 1, "dropdown-item"], [1, "dropdown-header"], [1, "form-group", "clickable"], [1, "form-check"], ["type", "checkbox", 3, "checked", "change"], [3, "click"], [1, "dropdown-divider"], [4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", "class", "dropdown-item", 3, "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "change", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled", 4, "ngIf"], ["type", "checkbox", 3, "checked", "disabled"], ["ngbDropdownItem", "", 3, "click"], [3, "icon", 4, "ngIf"], [3, "icon"], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngIf"], [1, "col-3"], ["ngbDropdown", "", 1, "btn-group", 3, "autoClose"], ["type", "button", "ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"], ["ngbDropdownItem", "", 1, "dropdown-header"], ["type", "button", 3, "class", "disabled", "click", 4, "ngFor", "ngForOf"], ["type", "button", 3, "disabled", "click"], ["ngbDropdown", "", 1, "btn-group"], ["ngbDropdownToggle", "", 1, "btn", "btn-light", "dropdown-toggle", 3, "disabled"], ["ngbDropdownMenu", ""], ["ngbDropdownItem", ""], ["ngbDropdownItem", "", 3, "disabled", "click", 4, "ngFor", "ngForOf"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "table-responsive"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], [1, "d-none"], [1, ""], ["scope", "row", "class", "small-as-possible", 4, "ngIf"], ["scope", "row", "style", "width: 1%;", "ngbDropdown", "", 3, "border-primary", "autoClose", 4, "ngIf"], ["scope", "row", 1, "small-as-possible"], ["scope", "row", "ngbDropdown", "", 2, "width", "1%", 3, "autoClose"], ["ngbDropdownToggle", "", 1, "form-check", "clickable"], [1, "dropdown", "show", "d-inline-block", "float-right"], ["class", "dropdown-item", "ngbDropdownItem", "", 4, "ngFor", "ngForOf"], [1, "clickable", 3, "click"], ["scope", "col", 3, "click"], ["type", "button", 1, "order-btn", "btn", "btn-none", 3, "click"], [1, "fas", "fa-long-arrow-alt-up"], [1, "start-row-hook", "d-none"], ["cdkDrag", "", 1, "data-row", 3, "cdkDragData", "cdkDragDisabled"], ["scope", "row", 4, "ngIf"], ["class", "drop-container", 3, "hidden", 4, "cdkDragPlaceholder"], ["class", "drag-container", 4, "cdkDragPreview"], ["class", "detail-row", 4, "ngIf"], ["scope", "row"], ["class", "clickable", 3, "icon", "click", 4, "ngIf"], [1, "clickable", 3, "icon", "click"], [1, "drop-container", 3, "hidden"], [1, "drop-cell"], [1, "drop-container"], [1, "drag-container"], [1, "detail-row"], [1, "row-expansion-container"], [1, "message-row", "no-results-row"], [1, "text-center"], [1, "message-row", "fetching-row"], ["class", "table-responsive", 4, "ngIf"], ["class", "table table-dark table-hover", "cdkDropList", "", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "nodrop", "acceptdrop", "cdkDropListDropped", 4, "ngIf"], ["cdkDropList", "", 1, "table", "table-dark", "table-hover", 3, "dataSource", "id", "cdkDropListConnectedTo", "cdkDropListData", "cdkDropListEnterPredicate", "cdkDropListSortingDisabled", "cdkDropListDropped"], ["renderedMatTable", ""], [3, "matColumnDef"], ["class", "maissize-min", 4, "matHeaderCellDef"], ["class", "maissize-min", 4, "matCellDef"], [4, "matCellDef"], [3, "matColumnDef", 4, "ngFor", "ngForOf"], [4, "matHeaderRowDef"], ["class", "element-row data-row", "cdkDrag", "", 3, "expanded", "row-selected", "cdkDragData", "cdkDragDisabled", 4, "matRowDef", "matRowDefColumns"], ["style", "overflow: hidden", 4, "matRowDef", "matRowDefColumns", "matRowDefWhen"], [1, "maissize-min"], ["ngbDropdown", "", 3, "autoClose", "container"], [3, "class", 4, "matHeaderCellDef"], [3, "class", 4, "matCellDef"], ["cdkDrag", "", 1, "element-row", "data-row", 3, "cdkDragData", "cdkDragDisabled"], [2, "overflow", "hidden"], [1, "alert", "alert-primary", "text-center"], [1, "container-full", "px-4", "py-4", "table-actions", "table-actions--footer"], [1, "col-7"], [1, "inline-block", "inline-block-bottom"], [1, "pagination-caption"], ["aria-label", "pagination", 1, "pagination-links"], [1, "pagination"], [1, "page-item", "clickable"], [1, "page-link", 3, "click"], [1, "inline-block", "inline-block-bottom", "pl-2"], ["ngbDropdown", "", 4, "ngIf"], ["class", "page-item clickable", 3, "active", 4, "ngIf"], ["class", "page-item", 4, "ngIf"], ["class", "sr-only", 4, "ngIf"], [1, "sr-only"], [1, "page-item"], [1, "page-link"], ["ngbDropdown", ""], ["ngbDropdownToggle", "", 1, "btn", "btn-black", "dropdown-toggle"]], template: function MaisTableComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵtemplate"](1, MaisTableComponent_div_1_Template, 12, 9, "div", 1);
                core["ɵɵtemplate"](2, MaisTableComponent_ng_container_2_Template, 15, 19, "ng-container", 2);
                core["ɵɵtemplate"](3, MaisTableComponent_ng_container_3_Template, 2, 1, "ng-container", 2);
                core["ɵɵtemplate"](4, MaisTableComponent_div_4_Template, 10, 6, "div", 3);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.columnSelectionPossibleAndAllowed || ctx.filteringPossibleAndAllowed || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", !ctx.compatibilityModeForMainTable);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.compatibilityModeForMainTable);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.currentEnablePagination || ctx.headerActionsEnabledAndPossible || ctx.contextActionsEnabledAndPossible);
            } }, directives: [common.NgIf, common.NgTemplateOutlet, ngBootstrap.NgbDropdown, ngBootstrap.NgbDropdownToggle, ngBootstrap.NgbDropdownMenu, ngBootstrap.NgbDropdownItem, common.NgForOf, angularFontawesome.FaIconComponent, dragDrop.CdkDropList, dragDrop.CdkDrag, dragDrop.CdkDragPlaceholder, dragDrop.CdkDragPreview, table.MatTable, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, table.MatHeaderCell, table.MatCell, table.MatHeaderRow, table.MatRow], pipes: [core$1.TranslatePipe], styles: [".message-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding:2em}.clickable[_ngcontent-%COMP%]{cursor:pointer}.inline-block[_ngcontent-%COMP%]{display:inline-block}.inline-block-bottom[_ngcontent-%COMP%]{vertical-align:bottom}.small-as-possible[_ngcontent-%COMP%]{width:1%}.detail-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:2em}.row-selected[_ngcontent-%COMP%]{background-color:#bc001622}.row-selected[_ngcontent-%COMP%]:hover{background-color:#77464755!important}.drag-container[_ngcontent-%COMP%], .drop-container[_ngcontent-%COMP%]{min-height:60px;padding:1em;background:#77464755;border:3px dotted #25161755}.nodrop[_ngcontent-%COMP%]   .drop-cell[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%], .nodrop[_ngcontent-%COMP%]   .drop-row[_ngcontent-%COMP%]{display:none!important}.cdk-drop-list-dragging.acceptdrop[_ngcontent-%COMP%]   td[_ngcontent-%COMP%], th[_ngcontent-%COMP%]{border:none}.mat-header-cell[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{padding:0;margin:0}.mat-cell.maissize-min[_ngcontent-%COMP%], .mat-cell.maissize-xxs[_ngcontent-%COMP%], .mat-header-cell.maissize-min[_ngcontent-%COMP%], .mat-header-cell.maissize-xxs[_ngcontent-%COMP%]{flex:0 0 2%;min-width:3em}.mat-cell.maissize-xs[_ngcontent-%COMP%], .mat-header-cell.maissize-xs[_ngcontent-%COMP%]{flex:0 0 3%;min-width:5em}.mat-cell.maissize-s[_ngcontent-%COMP%], .mat-header-cell.maissize-s[_ngcontent-%COMP%]{flex:0 0 3%;min-width:8em}.mat-cell.maissize-m[_ngcontent-%COMP%], .mat-header-cell.maissize-m[_ngcontent-%COMP%]{flex:0 0 3%;min-width:12em}.mat-cell.maissize-l[_ngcontent-%COMP%], .mat-header-cell.maissize-l[_ngcontent-%COMP%]{flex:0 0 3%;min-width:18em}.mat-cell.maissize-xl[_ngcontent-%COMP%], .mat-header-cell.maissize-xl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:25em}.mat-cell.maissize-xxl[_ngcontent-%COMP%], .mat-header-cell.maissize-xxl[_ngcontent-%COMP%]{flex:0 0 3%;min-width:30em}.mat-table[_ngcontent-%COMP%]{border-collapse:collapse}.mat-header-row[_ngcontent-%COMP%], .mat-row[_ngcontent-%COMP%], .mat-table[_ngcontent-%COMP%]{border-color:gray}", "mat-table[_ngcontent-%COMP%]{display:block}mat-header-row[_ngcontent-%COMP%]{min-height:56px}mat-footer-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{min-height:48px}mat-footer-row[_ngcontent-%COMP%], mat-header-row[_ngcontent-%COMP%], mat-row[_ngcontent-%COMP%]{display:flex;border-width:0 0 1px;border-style:solid;align-items:center;box-sizing:border-box}mat-footer-row[_ngcontent-%COMP%]::after, mat-header-row[_ngcontent-%COMP%]::after, mat-row[_ngcontent-%COMP%]::after{display:inline-block;min-height:inherit;content:\"\"}mat-cell[_ngcontent-%COMP%]:first-of-type, mat-footer-cell[_ngcontent-%COMP%]:first-of-type, mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}mat-cell[_ngcontent-%COMP%]:last-of-type, mat-footer-cell[_ngcontent-%COMP%]:last-of-type, mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}mat-cell[_ngcontent-%COMP%], mat-footer-cell[_ngcontent-%COMP%], mat-header-cell[_ngcontent-%COMP%]{flex:1;display:flex;align-items:center;overflow:hidden;word-wrap:break-word;min-height:inherit}table.mat-table[_ngcontent-%COMP%]{border-spacing:0}tr.mat-header-row[_ngcontent-%COMP%]{height:56px}tr.mat-footer-row[_ngcontent-%COMP%], tr.mat-row[_ngcontent-%COMP%]{height:48px}th.mat-header-cell[_ngcontent-%COMP%]{text-align:left}[dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]{text-align:right}td.mat-cell[_ngcontent-%COMP%], td.mat-footer-cell[_ngcontent-%COMP%], th.mat-header-cell[_ngcontent-%COMP%]{padding:0;border-bottom-width:1px;border-bottom-style:solid}td.mat-cell[_ngcontent-%COMP%]:first-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:first-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:first-of-type{padding-left:0;padding-right:24px}td.mat-cell[_ngcontent-%COMP%]:last-of-type, td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:24px}[dir=rtl][_ngcontent-%COMP%]   td.mat-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   td.mat-footer-cell[_ngcontent-%COMP%]:last-of-type, [dir=rtl][_ngcontent-%COMP%]   th.mat-header-cell[_ngcontent-%COMP%]:last-of-type{padding-right:0;padding-left:24px}"], data: { animation: [
                    animations.trigger('detailExpand', [
                        animations.state('collapsed', animations.style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                        animations.state('expanded', animations.style({ height: '*', visibility: 'visible' })),
                        animations.transition('expanded <=> collapsed', animations.animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                    ]),
                ] } });
        return MaisTableComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaisTableComponent, [{
            type: core.Component,
            args: [{
                    // tslint:disable-next-line: component-selector
                    selector: 'mais-table',
                    templateUrl: './mais-table.component.html',
                    styleUrls: ['./mais-table.component.scss', './mais-table-porting.css'],
                    animations: [
                        animations.trigger('detailExpand', [
                            animations.state('collapsed', animations.style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                            animations.state('expanded', animations.style({ height: '*', visibility: 'visible' })),
                            animations.transition('expanded <=> collapsed', animations.animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                        ]),
                    ],
                }]
        }], function () { return [{ type: core$1.TranslateService }, { type: MaisTableService }, { type: MaisTableRegistryService }, { type: core.ChangeDetectorRef }, { type: core.NgZone }]; }, { data: [{
                type: core.Input
            }], dataProvider: [{
                type: core.Input
            }], columns: [{
                type: core.Input
            }], defaultSortingColumn: [{
                type: core.Input
            }], defaultSortingDirection: [{
                type: core.Input
            }], defaultPageSize: [{
                type: core.Input
            }], possiblePageSize: [{
                type: core.Input
            }], tableId: [{
                type: core.Input
            }], dropConnectedTo: [{
                type: core.Input
            }], paginationMode: [{
                type: core.Input
            }], enablePagination: [{
                type: core.Input
            }], enableSelection: [{
                type: core.Input
            }], enableContextFiltering: [{
                type: core.Input
            }], enableSelectAll: [{
                type: core.Input
            }], enableMultiSelect: [{
                type: core.Input
            }], enableFiltering: [{
                type: core.Input
            }], enableColumnsSelection: [{
                type: core.Input
            }], enableContextActions: [{
                type: core.Input
            }], enableHeaderActions: [{
                type: core.Input
            }], enableRowExpansion: [{
                type: core.Input
            }], enablePageSizeSelect: [{
                type: core.Input
            }], enableMultipleRowExpansion: [{
                type: core.Input
            }], enableItemTracking: [{
                type: core.Input
            }], enableStorePersistence: [{
                type: core.Input
            }], enableDrag: [{
                type: core.Input
            }], enableDrop: [{
                type: core.Input
            }], contextActions: [{
                type: core.Input
            }], headerActions: [{
                type: core.Input
            }], contextFilters: [{
                type: core.Input
            }], contextFilteringPrompt: [{
                type: core.Input
            }], actionStatusProvider: [{
                type: core.Input
            }], expandableStatusProvider: [{
                type: core.Input
            }], storeAdapter: [{
                type: core.Input
            }], itemIdentifier: [{
                type: core.Input
            }], refreshStrategy: [{
                type: core.Input
            }], refreshInterval: [{
                type: core.Input
            }], refreshEmitter: [{
                type: core.Input
            }], refreshIntervalInBackground: [{
                type: core.Input
            }], refreshOnPushInBackground: [{
                type: core.Input
            }], pageChange: [{
                type: core.Output
            }], sortChange: [{
                type: core.Output
            }], selectionChange: [{
                type: core.Output
            }], filteringColumnsChange: [{
                type: core.Output
            }], visibleColumnsChange: [{
                type: core.Output
            }], contextFiltersChange: [{
                type: core.Output
            }], statusChange: [{
                type: core.Output
            }], itemDragged: [{
                type: core.Output
            }], itemDropped: [{
                type: core.Output
            }], action: [{
                type: core.Output
            }], cellTemplate: [{
                type: core.ContentChild,
                args: ['cellTemplate']
            }], actionsCaptionTemplate: [{
                type: core.ContentChild,
                args: ['actionsCaptionTemplate']
            }], rowDetailTemplate: [{
                type: core.ContentChild,
                args: ['rowDetailTemplate']
            }], dragTemplate: [{
                type: core.ContentChild,
                args: ['dragTemplate']
            }], dropTemplate: [{
                type: core.ContentChild,
                args: ['dropTemplate']
            }], table: [{
                type: core.ViewChild,
                args: ['#renderedMatTable']
            }] }); })();

    var MaisTableModule = /** @class */ (function () {
        function MaisTableModule() {
        }
        /** @nocollapse */ MaisTableModule.ɵmod = core["ɵɵdefineNgModule"]({ type: MaisTableModule });
        /** @nocollapse */ MaisTableModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function MaisTableModule_Factory(t) { return new (t || MaisTableModule)(); }, providers: [], imports: [[
                    common.CommonModule,
                    angularFontawesome.FontAwesomeModule,
                    ngBootstrap.NgbModule,
                    core$1.TranslateModule,
                    table$1.CdkTableModule,
                    table.MatTableModule,
                    dragDrop.DragDropModule,
                ]] });
        return MaisTableModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](MaisTableModule, { declarations: [MaisTableComponent], imports: [common.CommonModule,
            angularFontawesome.FontAwesomeModule,
            ngBootstrap.NgbModule,
            core$1.TranslateModule,
            table$1.CdkTableModule,
            table.MatTableModule,
            dragDrop.DragDropModule], exports: [MaisTableComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaisTableModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        MaisTableComponent,
                    ],
                    imports: [
                        common.CommonModule,
                        angularFontawesome.FontAwesomeModule,
                        ngBootstrap.NgbModule,
                        core$1.TranslateModule,
                        table$1.CdkTableModule,
                        table.MatTableModule,
                        dragDrop.DragDropModule,
                    ],
                    exports: [
                        MaisTableComponent
                    ],
                    providers: []
                }]
        }], null, null); })();

    exports.COLUMN_DEFAULTS = COLUMN_DEFAULTS;
    exports.MaisTableComponent = MaisTableComponent;
    exports.MaisTableModule = MaisTableModule;
    exports.MaisTableRegistryService = MaisTableRegistryService;
    exports.MaisTableService = MaisTableService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ngx-mais-table.umd.js.map
