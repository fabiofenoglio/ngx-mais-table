import { Component } from '@angular/core';

@Component({
  selector: 'mais-table-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MaisTableLibrary';
}
